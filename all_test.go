// Copyright 2023 The libqbe-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libqbe // import "modernc.org/libqbe"

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"testing"

	"github.com/pmezard/go-difflib/difflib"
	_ "modernc.org/ccgo/v3/lib"
	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/gc/v3"
	"modernc.org/libc"
)

var (
	goos       = runtime.GOOS
	goarch     = runtime.GOARCH
	gomaxprocs = runtime.GOMAXPROCS(-1)
	cc         string
	re         *regexp.Regexp

	oRE  = flag.String("re", "", "")
	oTrc = flag.Bool("trc", false, "")

	targets = []string{
		"amd64_apple",
		"amd64_sysv",
		"arm64",
		"arm64_apple",
		"rv64",
	}
)

func TestMain(m *testing.M) {
	flag.Parse()
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}
	rc := m.Run()
	os.Exit(rc)
}

func test0(tls *libc.TLS, temp, target, path, driver string, runArgs []string, run bool) (asm, out []byte, err error) {
	r, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}

	defer r.Close()

	w := bytes.NewBuffer(nil)
	if err = Main(target, path, r, w, nil); err != nil {
		return nil, nil, err
	}

	asm = w.Bytes()
	if !run {
		return asm, nil, nil
	}

	ofn := filepath.Join(temp, "out.s")
	if err = os.WriteFile(ofn, asm, 0660); err != nil {
		return nil, nil, err
	}

	bin := filepath.Join(temp, "a.out")
	os.Remove(bin)
	compileArgs := []string{"-o", bin, ofn}
	if driver != "" {
		compileArgs = append(compileArgs, driver)
	}
	switch fmt.Sprintf("%s/%s", goos, goarch) {
	case
		"freebsd/amd64",
		"freebsd/arm64",
		"netbsd/amd64",
		"openbsd/amd64":

		compileArgs = append(compileArgs, "-lpthread")
	}
	if out, err = exec.Command(cc, compileArgs...).CombinedOutput(); err != nil {
		s, _ := os.ReadFile(ofn)
		var d []byte
		if driver != "" {
			d, _ = os.ReadFile(driver)
		}
		return nil, nil, fmt.Errorf("compiling %s:\n---- %s\n%s\n---- %s\n%s\n----\nout=%s FAIL err=%v", ofn, ofn, s, driver, d, out, err)
	}

	out, err = exec.Command(bin, runArgs...).CombinedOutput()
	return asm, out, err
}

func TestCompile(t *testing.T) {
	tempDir := t.TempDir()
	tls := libc.NewTLS()
	blacklist := map[string]struct{}{}
	dir := filepath.Join("internal", "test")
	for _, target := range targets {
		t.Run(target, func(t *testing.T) { testCompile(t, tls, blacklist, tempDir, dir, target) })
	}
}

func testCompile(t *testing.T, tls *libc.TLS, blacklist map[string]struct{}, tempDir, dir, target string) {
	var files, ok int
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		base := filepath.Base(path)

		if info.IsDir() || strings.HasPrefix(base, "_") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := blacklist[base]; ok {
				return nil
			}
		}

		files++
		switch filepath.Ext(path) {
		case ".ssa":
			if *oTrc {
				fmt.Printf("%s target=%s\n", path, target)
			}

			wantFn := filepath.Join("testdata", target, base+".s")
			want, err := os.ReadFile(wantFn)
			if err != nil {
				return err
			}

			asm, _, err := test0(tls, tempDir, target, path, "", nil, false)
			if err != nil {
				t.Error(err)
				return nil
			}

			g, e := string(asm), string(want)
			if g != e {
				diff := difflib.UnifiedDiff{
					A:        difflib.SplitLines(e),
					B:        difflib.SplitLines(g),
					FromFile: wantFn + " - want",
					ToFile:   base + ".s - got",
					Context:  3,
				}
				text, _ := difflib.GetUnifiedDiffString(diff)
				t.Errorf("%v: FAIL output differs target=%v:\n%s\n---- asm want\n%s---- asm got\n%s", path, target, text, want, asm)
				return nil
			}

			ok++
			return nil
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("files %v, ok %v", files, ok)
}
