.text
.balign 16
func:
	endbr64
	subq $16, %rsp
	movl $1, 0(%rsp)
	movl 0(%rsp), %eax
	movl %eax, 4(%rsp)
	movl $2, 0(%rsp)
	movq 0(%rsp), %rax
	addq $16, %rsp
	ret
.type func, @function
.size func, .-func
/* end function func */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	callq func
	movq %rax, -8(%rbp)
	movl -4(%rbp), %eax
	cmpl $1, %eax
	jz .Lbb4
	callq abort
.Lbb4:
	movl $0, %eax
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
