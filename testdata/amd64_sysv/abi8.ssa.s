.data
.balign 8
ctoqbestr:
	.ascii "c->qbe(%d)"
	.byte 0
/* end data */

.data
.balign 8
emptystr:
	.byte 0
/* end data */

.text
.balign 16
.globl qfn0
qfn0:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movss 16(%rbp), %xmm8
	movss %xmm8, -16(%rbp)
	movl $0, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	movss -16(%rbp), %xmm0
	callq ps
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn0, @function
.size qfn0, .-qfn0
/* end function qfn0 */

.text
.balign 16
.globl qfn1
qfn1:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss %xmm0, -16(%rbp)
	movq %rsi, -24(%rbp)
	movl $1, %esi
	movl %edi, %ebx
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	movl %ebx, %edi
	callq pw
	movss -16(%rbp), %xmm0
	callq ps
	leaq -24(%rbp), %rdi
	callq pfi1
	leaq emptystr(%rip), %rdi
	callq puts
	popq %rbx
	leave
	ret
.type qfn1, @function
.size qfn1, .-qfn1
/* end function qfn1 */

.text
.balign 16
.globl qfn2
qfn2:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss %xmm0, -16(%rbp)
	movq %rsi, -24(%rbp)
	movl $2, %esi
	movl %edi, %ebx
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	movl %ebx, %edi
	callq pw
	leaq -24(%rbp), %rdi
	callq pfi2
	movss -16(%rbp), %xmm0
	callq ps
	leaq emptystr(%rip), %rdi
	callq puts
	popq %rbx
	leave
	ret
.type qfn2, @function
.size qfn2, .-qfn2
/* end function qfn2 */

.text
.balign 16
.globl qfn3
qfn3:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss %xmm0, -16(%rbp)
	movq %rsi, -24(%rbp)
	movl $3, %esi
	movl %edi, %ebx
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	movl %ebx, %edi
	callq pw
	movss -16(%rbp), %xmm0
	callq ps
	leaq -24(%rbp), %rdi
	callq pfi3
	leaq emptystr(%rip), %rdi
	callq puts
	popq %rbx
	leave
	ret
.type qfn3, @function
.size qfn3, .-qfn3
/* end function qfn3 */

.text
.balign 16
.globl qfn4
qfn4:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %xmm0, -8(%rbp)
	movl $4, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	leaq -8(%rbp), %rdi
	callq pss
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn4, @function
.size qfn4, .-qfn4
/* end function qfn4 */

.text
.balign 16
.globl qfn5
qfn5:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss 16(%rbp), %xmm8
	movss %xmm8, -16(%rbp)
	movsd %xmm7, %xmm0
	movq %xmm0, -24(%rbp)
	movl $5, %esi
	movq %rdi, %rbx
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	movq %rbx, %rdi
	movq %rdi, %rbx
	leaq -24(%rbp), %rdi
	callq pss
	movss -16(%rbp), %xmm0
	callq ps
	movq %rbx, %rdi
	callq pl
	leaq emptystr(%rip), %rdi
	callq puts
	popq %rbx
	leave
	ret
.type qfn5, @function
.size qfn5, .-qfn5
/* end function qfn5 */

.text
.balign 16
.globl qfn6
qfn6:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %rdi, -16(%rbp)
	movq %rsi, -8(%rbp)
	movl $6, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	leaq -16(%rbp), %rdi
	callq plb
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn6, @function
.size qfn6, .-qfn6
/* end function qfn6 */

.text
.balign 16
.globl qfn7
qfn7:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $7, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	leaq 24(%rbp), %rdi
	callq plb
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn7, @function
.size qfn7, .-qfn7
/* end function qfn7 */

.text
.balign 16
.globl qfn8
qfn8:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $8, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	leaq 32(%rbp), %rdi
	callq plb
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn8, @function
.size qfn8, .-qfn8
/* end function qfn8 */

.text
.balign 16
.globl qfn9
qfn9:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $9, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	leaq 16(%rbp), %rdi
	callq pbig
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn9, @function
.size qfn9, .-qfn9
/* end function qfn9 */

.text
.balign 16
.globl qfn10
qfn10:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $24, %rsp
	pushq %rbx
	movq 56(%rbp), %rbx
	movss %xmm0, -16(%rbp)
	movq %rbx, %rdi
	movl $10, %esi
	movq %rdi, %rbx
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	movq %rbx, %rdi
	movq %rdi, %rbx
	leaq 32(%rbp), %rdi
	callq pbig
	movss -16(%rbp), %xmm0
	callq ps
	movq %rbx, %rdi
	callq pl
	leaq emptystr(%rip), %rdi
	callq puts
	popq %rbx
	leave
	ret
.type qfn10, @function
.size qfn10, .-qfn10
/* end function qfn10 */

.text
.balign 16
.globl qfn11
qfn11:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $11, %esi
	leaq ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq printf
	leaq 16(%rbp), %rdi
	callq pddd
	leaq emptystr(%rip), %rdi
	callq puts
	leave
	ret
.type qfn11, @function
.size qfn11, .-qfn11
/* end function qfn11 */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %rsp, %rax
	movq $1092511334, 0(%rax)
	movss ".Lfp1"(%rip), %xmm7
	movss ".Lfp1"(%rip), %xmm6
	movss ".Lfp1"(%rip), %xmm5
	movss ".Lfp1"(%rip), %xmm4
	movss ".Lfp1"(%rip), %xmm3
	movss ".Lfp1"(%rip), %xmm2
	movss ".Lfp1"(%rip), %xmm1
	movss ".Lfp1"(%rip), %xmm0
	callq cfn0
	subq $-16, %rsp
	movq fi1(%rip), %rsi
	movss ".Lfp2"(%rip), %xmm0
	movl $1, %edi
	callq cfn1
	movss ".Lfp3"(%rip), %xmm0
	movq fi2(%rip), %rsi
	movl $1, %edi
	callq cfn2
	movq fi3(%rip), %rsi
	movss ".Lfp2"(%rip), %xmm0
	movl $1, %edi
	callq cfn3
	movsd ss(%rip), %xmm0
	callq cfn4
	subq $16, %rsp
	movq %rsp, %rax
	movq $1092511334, 0(%rax)
	movl $10, %edi
	movsd ss(%rip), %xmm7
	movsd ".Lfp1"(%rip), %xmm6
	movsd ".Lfp1"(%rip), %xmm5
	movsd ".Lfp1"(%rip), %xmm4
	movsd ".Lfp1"(%rip), %xmm3
	movsd ".Lfp1"(%rip), %xmm2
	movsd ".Lfp1"(%rip), %xmm1
	movsd ".Lfp1"(%rip), %xmm0
	callq cfn5
	subq $-16, %rsp
	movq lb(%rip), %rdi
	leaq lb(%rip), %rax
	addq $8, %rax
	movq (%rax), %rsi
	callq cfn6
	subq $32, %rsp
	movq %rsp, %rax
	leaq lb(%rip), %rcx
	addq $0, %rcx
	movq (%rcx), %rcx
	movq %rcx, 8(%rax)
	leaq lb(%rip), %rcx
	addq $8, %rcx
	movq (%rcx), %rcx
	movq %rcx, 16(%rax)
	movq $0, 0(%rax)
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl $0, %edi
	callq cfn7
	subq $-32, %rsp
	subq $32, %rsp
	movq %rsp, %rax
	leaq lb(%rip), %rcx
	addq $0, %rcx
	movq (%rcx), %rcx
	movq %rcx, 16(%rax)
	leaq lb(%rip), %rcx
	addq $8, %rcx
	movq (%rcx), %rcx
	movq %rcx, 24(%rax)
	movq $0, 8(%rax)
	movq $0, 0(%rax)
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl $0, %edi
	callq cfn8
	subq $-32, %rsp
	subq $32, %rsp
	movq %rsp, %rcx
	leaq big(%rip), %rax
	addq $0, %rax
	movzbl (%rax), %eax
	movb %al, 0(%rcx)
	leaq big(%rip), %rax
	addq $1, %rax
	movq (%rax), %rax
	movq %rax, 1(%rcx)
	leaq big(%rip), %rax
	addq $9, %rax
	movq (%rax), %rax
	movq %rax, 9(%rcx)
	callq cfn9
	subq $-32, %rsp
	subq $48, %rsp
	movq %rsp, %rax
	movq $11, 40(%rax)
	leaq big(%rip), %rcx
	addq $0, %rcx
	movzbl (%rcx), %ecx
	movb %cl, 16(%rax)
	leaq big(%rip), %rcx
	addq $1, %rcx
	movq (%rcx), %rcx
	movq %rcx, 17(%rax)
	leaq big(%rip), %rcx
	addq $9, %rcx
	movq (%rcx), %rcx
	movq %rcx, 25(%rax)
	movq $0, 8(%rax)
	movq $0, 0(%rax)
	movss ".Lfp0"(%rip), %xmm0
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl $0, %edi
	callq cfn10
	subq $-48, %rsp
	subq $32, %rsp
	movq %rsp, %rcx
	leaq ddd(%rip), %rax
	addq $0, %rax
	movq (%rax), %rax
	movq %rax, 0(%rcx)
	leaq ddd(%rip), %rax
	addq $8, %rax
	movq (%rax), %rax
	movq %rax, 8(%rcx)
	leaq ddd(%rip), %rax
	addq $16, %rax
	movq (%rax), %rax
	movq %rax, 16(%rcx)
	callq cfn11
	subq $-32, %rsp
	movl $0, %eax
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 0 /* 0.000000 */

.section .rodata
.p2align 2
.Lfp0:
	.int 1092721050 /* 10.100000 */

.section .rodata
.p2align 2
.Lfp2:
	.int 1074580685 /* 2.200000 */

.section .rodata
.p2align 2
.Lfp3:
	.int 1079194419 /* 3.300000 */

.section .note.GNU-stack,"",@progbits
