.data
.balign 8
z:
	.int 0
/* end data */

.text
.balign 16
.globl test
test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $24, %rsp
	pushq %rbx
	movl z(%rip), %eax
	movl %eax, %ebx
	addl %eax, %ebx
	movslq %ebx, %rcx
	movq $4, -16(%rbp)
	movl $5, -4(%rbp)
	leaq F(%rip), %rax
	addq %rcx, %rax
	subq $16, %rsp
	movq %rsp, %rcx
	movq $6, 0(%rcx)
	movq -16(%rbp), %r8
	movq -8(%rbp), %r9
	movl $3, %ecx
	movl $2, %edx
	movl $1, %esi
	movl %ebx, %edi
	callq *%rax
	subq $-16, %rsp
	addl %ebx, %eax
	movl %eax, a(%rip)
	popq %rbx
	leave
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
