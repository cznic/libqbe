.text
.balign 16
test:
	endbr64
	movl $100, %eax
	movl $0, %edx
.Lbb2:
	negl %edx
	addl $1, %edx
	jnz .Lbb5
	movl $10, %ecx
.Lbb4:
	movl %ecx, %esi
	subl $1, %ecx
	cmpl $0, %esi
	jz .Lbb2
	jmp .Lbb4
.Lbb5:
	subl $10, %eax
	jnz .Lbb2
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
