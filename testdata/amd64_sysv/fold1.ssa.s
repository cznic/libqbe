.text
.balign 16
.globl f1
f1:
	endbr64
	movl $4294967295, %eax
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	endbr64
	movl $4294967264, %eax
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	endbr64
	movl $4294967291, %eax
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	endbr64
	movl $0, %eax
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.text
.balign 16
.globl f5
f5:
	endbr64
	movl $1, %eax
	ret
.type f5, @function
.size f5, .-f5
/* end function f5 */

.text
.balign 16
.globl f6
f6:
	endbr64
	movl $0, %eax
	ret
.type f6, @function
.size f6, .-f6
/* end function f6 */

.section .note.GNU-stack,"",@progbits
