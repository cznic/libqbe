.data
.balign 8
arr:
	.byte 10
	.byte -60
	.byte 10
	.byte 100
	.byte 200
	.byte 0
/* end data */

.text
.balign 16
.globl test
test:
	endbr64
	leaq arr(%rip), %rax
	movq %rax, %rcx
	movl $-1, %eax
.Lbb2:
	movzbl (%rcx), %edx
	addq $1, %rcx
	cmpl $0, %edx
	jz .Lbb5
	cmpl %edx, %eax
	jg .Lbb2
	movl %edx, %eax
	jmp .Lbb2
.Lbb5:
	movl %eax, a(%rip)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
