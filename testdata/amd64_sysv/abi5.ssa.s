.data
.balign 8
fmt1:
	.ascii "t1: %s\n"
	.byte 0
/* end data */

.data
.balign 8
fmt2:
	.ascii "t2: %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt3:
	.ascii "t3: %f %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt4:
	.ascii "t4: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmt5:
	.ascii "t5: %f %lld\n"
	.byte 0
/* end data */

.data
.balign 8
fmt6:
	.ascii "t6: %s\n"
	.byte 0
/* end data */

.data
.balign 8
fmt7:
	.ascii "t7: %f %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmt8:
	.ascii "t8: %d %d %d %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt9:
	.ascii "t9: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmta:
	.ascii "ta: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmtb:
	.ascii "tb: %d %d %f\n"
	.byte 0
/* end data */

.text
.balign 16
.globl test
test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $144, %rsp
	leaq -72(%rbp), %rdi
	callq t1
	movq %rax, %rsi
	leaq fmt1(%rip), %rdi
	movl $0, %eax
	callq printf
	callq t2
	movq %rax, -80(%rbp)
	movl -80(%rbp), %esi
	leaq fmt2(%rip), %rdi
	movl $0, %eax
	callq printf
	callq t3
	movq %rax, -88(%rbp)
	movss -88(%rbp), %xmm0
	movl -84(%rbp), %esi
	cvtss2sd %xmm0, %xmm0
	leaq fmt3(%rip), %rdi
	movl $1, %eax
	callq printf
	callq t4
	movq %rax, -16(%rbp)
	movq %xmm0, -8(%rbp)
	movl -16(%rbp), %esi
	movsd -8(%rbp), %xmm0
	leaq fmt4(%rip), %rdi
	movl $1, %eax
	callq printf
	callq t5
	movq %xmm0, -32(%rbp)
	movq %rax, -24(%rbp)
	movss -32(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	movq -24(%rbp), %rsi
	leaq fmt5(%rip), %rdi
	movl $1, %eax
	callq printf
	callq t6
	movq %rax, %rcx
	movq %rdx, %rax
	movq %rcx, -104(%rbp)
	movq %rax, -96(%rbp)
	leaq -104(%rbp), %rsi
	leaq fmt6(%rip), %rdi
	movl $0, %eax
	callq printf
	callq t7
	movsd %xmm0, %xmm15
	movsd %xmm1, %xmm0
	movsd %xmm15, %xmm1
	movq %xmm1, -48(%rbp)
	movq %xmm0, -40(%rbp)
	movss -48(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	movsd -40(%rbp), %xmm1
	leaq fmt7(%rip), %rdi
	movl $2, %eax
	callq printf
	callq t8
	movq %rax, %rcx
	movq %rdx, %rax
	movq %rcx, -120(%rbp)
	movq %rax, -112(%rbp)
	movl -120(%rbp), %esi
	movl -116(%rbp), %edx
	movl -112(%rbp), %ecx
	movl -108(%rbp), %r8d
	leaq fmt8(%rip), %rdi
	movl $0, %eax
	callq printf
	callq t9
	movq %rax, -128(%rbp)
	movl -128(%rbp), %esi
	movss -124(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	leaq fmt9(%rip), %rdi
	movl $1, %eax
	callq printf
	callq ta
	movq %rax, -136(%rbp)
	movsbl -136(%rbp), %esi
	movss -132(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	leaq fmta(%rip), %rdi
	movl $1, %eax
	callq printf
	callq tb
	movq %rax, -144(%rbp)
	movsbl -144(%rbp), %esi
	movsbl -143(%rbp), %edx
	movss -140(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	leaq fmtb(%rip), %rdi
	movl $1, %eax
	callq printf
	leave
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
