.text
.balign 16
g:
	endbr64
	ret
.type g, @function
.size g, .-g
/* end function g */

.text
.balign 16
f:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $8, %rsp
	pushq %rbx
	movl %edi, %ebx
	callq g
	movl %ebx, %eax
	subq $16, %rsp
	movq %rsp, %rdx
	movq $180388626474, %rcx
	movq %rcx, (%rdx)
	movq $180388626474, %rcx
	movq %rcx, 8(%rdx)
	movq %rbp, %rsp
	subq $16, %rsp
	popq %rbx
	leave
	ret
.type f, @function
.size f, .-f
/* end function f */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $8, %rsp
	pushq %rbx
	movl $0, %edi
	callq f
	movl %eax, %ebx
	movl $0, %edi
	callq f
	movl %ebx, %eax
	popq %rbx
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
