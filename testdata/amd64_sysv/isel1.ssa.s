.text
.balign 16
.globl f
f:
	endbr64
	imulq $8, %rdi, %rcx
	leaq a(%rip), %rax
	addq %rcx, %rax
	imulq $4, %rsi, %rcx
	addq %rcx, %rax
	movl (%rax), %eax
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
