.text
.balign 16
mandel:
	endbr64
	movsd %xmm1, %xmm3
	subsd ".Lfp1"(%rip), %xmm3
	movsd ".Lfp0"(%rip), %xmm2
	movsd ".Lfp0"(%rip), %xmm1
	movl $0, %eax
.Lbb2:
	movsd %xmm1, %xmm4
	addl $1, %eax
	movsd %xmm2, %xmm1
	mulsd %xmm4, %xmm2
	movsd %xmm4, %xmm5
	mulsd %xmm4, %xmm5
	movsd %xmm1, %xmm4
	mulsd %xmm1, %xmm4
	movsd %xmm5, %xmm1
	subsd %xmm4, %xmm1
	addsd %xmm3, %xmm1
	addsd %xmm2, %xmm2
	addsd %xmm0, %xmm2
	addsd %xmm5, %xmm4
	ucomisd ".Lfp2"(%rip), %xmm4
	ja .Lbb5
	cmpl $1000, %eax
	jle .Lbb2
	movl $0, %eax
.Lbb5:
	ret
.type mandel, @function
.size mandel, .-mandel
/* end function mandel */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movsd ".Lfp3"(%rip), %xmm1
.Lbb8:
	movsd %xmm1, -16(%rbp)
	movsd ".Lfp3"(%rip), %xmm0
.Lbb10:
	movsd %xmm0, -8(%rbp)
	callq mandel
	cmpl $0, %eax
	jnz .Lbb12
	movl $42, %edi
	callq putchar
	movsd -8(%rbp), %xmm0
	movsd -16(%rbp), %xmm1
	jmp .Lbb13
.Lbb12:
	movl $32, %edi
	callq putchar
	movsd -8(%rbp), %xmm0
	movsd -16(%rbp), %xmm1
.Lbb13:
	addsd ".Lfp5"(%rip), %xmm0
	ucomisd ".Lfp4"(%rip), %xmm0
	jbe .Lbb10
	movl $10, %edi
	callq putchar
	movsd -16(%rbp), %xmm1
	addsd ".Lfp5"(%rip), %xmm1
	ucomisd ".Lfp4"(%rip), %xmm1
	jbe .Lbb8
	movl $0, %eax
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 0
	.int 0 /* 0.000000 */

.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 1071644672 /* 0.500000 */

.section .rodata
.p2align 3
.Lfp2:
	.int 0
	.int 1076887552 /* 16.000000 */

.section .rodata
.p2align 3
.Lfp3:
	.int 0
	.int -1074790400 /* -1.000000 */

.section .rodata
.p2align 3
.Lfp4:
	.int 0
	.int 1072693248 /* 1.000000 */

.section .rodata
.p2align 3
.Lfp5:
	.int -755914244
	.int 1067475533 /* 0.032000 */

.section .note.GNU-stack,"",@progbits
