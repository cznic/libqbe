.text
.balign 16
.globl chk
chk:
	endbr64
	movl $0, %ecx
	movl $0, %eax
.Lbb2:
	movl glo1(%rip), %r9d
	cmpl %r9d, %ecx
	jge .Lbb12
	movq glo3(%rip), %rdx
	movslq %edi, %r8
	movq (%rdx, %r8, 8), %r8
	movslq %ecx, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
	movslq %ecx, %r8
	movq (%rdx, %r8, 8), %r8
	movslq %esi, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
	movl %edi, %r8d
	addl %ecx, %r8d
	cmpl %r9d, %r8d
	setl %r8b
	movzbl %r8b, %r8d
	movl %esi, %r10d
	addl %ecx, %r10d
	cmpl %r9d, %r10d
	setl %r10b
	movzbl %r10b, %r10d
	testl %r8d, %r10d
	jz .Lbb5
	movl %edi, %r8d
	addl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %r8
	movl %esi, %r10d
	addl %ecx, %r10d
	movslq %r10d, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
.Lbb5:
	movl %edi, %r8d
	addl %ecx, %r8d
	cmpl %r9d, %r8d
	setl %r8b
	movzbl %r8b, %r8d
	movl %esi, %r10d
	subl %ecx, %r10d
	cmpl $0, %r10d
	setge %r10b
	movzbl %r10b, %r10d
	testl %r8d, %r10d
	jz .Lbb7
	movl %edi, %r8d
	addl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %r8
	movl %esi, %r10d
	subl %ecx, %r10d
	movslq %r10d, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
.Lbb7:
	movl %edi, %r8d
	subl %ecx, %r8d
	cmpl $0, %r8d
	setge %r8b
	movzbl %r8b, %r8d
	movl %esi, %r10d
	addl %ecx, %r10d
	cmpl %r9d, %r10d
	setl %r9b
	movzbl %r9b, %r9d
	testl %r8d, %r9d
	jz .Lbb9
	movl %edi, %r8d
	subl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %r8
	movl %esi, %r9d
	addl %ecx, %r9d
	movslq %r9d, %r9
	movl (%r8, %r9, 4), %r8d
	addl %r8d, %eax
.Lbb9:
	movl %edi, %r8d
	subl %ecx, %r8d
	cmpl $0, %r8d
	setge %r8b
	movzbl %r8b, %r8d
	movl %esi, %r9d
	subl %ecx, %r9d
	cmpl $0, %r9d
	setge %r9b
	movzbl %r9b, %r9d
	testl %r8d, %r9d
	jz .Lbb11
	movl %edi, %r8d
	subl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %rdx
	movl %esi, %r8d
	subl %ecx, %r8d
	movslq %r8d, %r8
	movl (%rdx, %r8, 4), %edx
	addl %edx, %eax
.Lbb11:
	addl $1, %ecx
	jmp .Lbb2
.Lbb12:
	ret
.type chk, @function
.size chk, .-chk
/* end function chk */

.text
.balign 16
.globl go
go:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	pushq %rbx
	pushq %r12
	movl glo1(%rip), %eax
	cmpl %eax, %edi
	jz .Lbb20
	movl $0, %ebx
.Lbb15:
	movl glo1(%rip), %eax
	cmpl %eax, %ebx
	jge .Lbb19
	movl %edi, %esi
	movl %edi, %r12d
	movl %ebx, %edi
	callq chk
	movl %r12d, %edi
	cmpl $0, %eax
	jnz .Lbb18
	movq glo3(%rip), %rax
	movslq %ebx, %rcx
	movq (%rax, %rcx, 8), %rcx
	movslq %edi, %rdx
	movl (%rcx, %rdx, 4), %eax
	addl $1, %eax
	movl %eax, (%rcx, %rdx, 4)
	movl %edi, %r12d
	addl $1, %edi
	callq go
	movl %r12d, %edi
	movq glo3(%rip), %rax
	movslq %ebx, %rcx
	movq (%rax, %rcx, 8), %rcx
	movslq %edi, %rdx
	movl (%rcx, %rdx, 4), %eax
	subl $1, %eax
	movl %eax, (%rcx, %rdx, 4)
.Lbb18:
	addl $1, %ebx
	jmp .Lbb15
.Lbb19:
	movl $0, %eax
	jmp .Lbb21
.Lbb20:
	movl glo2(%rip), %eax
	addl $1, %eax
	movl %eax, glo2(%rip)
	movl $0, %eax
.Lbb21:
	popq %r12
	popq %rbx
	leave
	ret
.type go, @function
.size go, .-go
/* end function go */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $8, %rsp
	pushq %rbx
	movl $8, glo1(%rip)
	movl $8, %esi
	movl $8, %edi
	callq calloc
	movq %rax, glo3(%rip)
	movl $0, %ebx
.Lbb24:
	movl glo1(%rip), %edi
	cmpl %edi, %ebx
	jge .Lbb26
	movl $4, %esi
	callq calloc
	movq glo3(%rip), %rcx
	movslq %ebx, %rdx
	movq %rax, (%rcx, %rdx, 8)
	addl $1, %ebx
	jmp .Lbb24
.Lbb26:
	movl $0, %edi
	callq go
	movl glo2(%rip), %eax
	cmpl $92, %eax
	setnz %al
	movzbl %al, %eax
	popq %rbx
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.data
.balign 8
glo1:
	.int 0
/* end data */

.data
.balign 8
glo2:
	.int 0
/* end data */

.data
.balign 8
glo3:
	.quad 0
/* end data */

.section .note.GNU-stack,"",@progbits
