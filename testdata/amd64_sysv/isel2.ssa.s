.text
.balign 16
.globl lt
lt:
	endbr64
	ucomisd %xmm0, %xmm1
	seta %al
	movzbl %al, %eax
	ret
.type lt, @function
.size lt, .-lt
/* end function lt */

.text
.balign 16
.globl le
le:
	endbr64
	ucomisd %xmm0, %xmm1
	setae %al
	movzbl %al, %eax
	ret
.type le, @function
.size le, .-le
/* end function le */

.text
.balign 16
.globl gt
gt:
	endbr64
	ucomisd %xmm1, %xmm0
	seta %al
	movzbl %al, %eax
	ret
.type gt, @function
.size gt, .-gt
/* end function gt */

.text
.balign 16
.globl ge
ge:
	endbr64
	ucomisd %xmm1, %xmm0
	setae %al
	movzbl %al, %eax
	ret
.type ge, @function
.size ge, .-ge
/* end function ge */

.text
.balign 16
.globl eq1
eq1:
	endbr64
	ucomisd %xmm1, %xmm0
	setz %al
	movzbl %al, %eax
	setnp %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	ret
.type eq1, @function
.size eq1, .-eq1
/* end function eq1 */

.text
.balign 16
.globl eq2
eq2:
	endbr64
	ucomisd %xmm1, %xmm0
	setz %al
	movzbl %al, %eax
	setnp %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	jnz .Lbb12
	movl $0, %eax
	jmp .Lbb13
.Lbb12:
	movl $1, %eax
.Lbb13:
	ret
.type eq2, @function
.size eq2, .-eq2
/* end function eq2 */

.text
.balign 16
.globl eq3
eq3:
	endbr64
	ucomisd %xmm1, %xmm0
	setz %al
	movzbl %al, %eax
	setnp %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	jnz .Lbb16
	movl $0, %eax
.Lbb16:
	ret
.type eq3, @function
.size eq3, .-eq3
/* end function eq3 */

.text
.balign 16
.globl ne1
ne1:
	endbr64
	ucomisd %xmm1, %xmm0
	setnz %al
	movzbl %al, %eax
	setp %cl
	movzbl %cl, %ecx
	orl %ecx, %eax
	ret
.type ne1, @function
.size ne1, .-ne1
/* end function ne1 */

.text
.balign 16
.globl ne2
ne2:
	endbr64
	ucomisd %xmm1, %xmm0
	setnz %al
	movzbl %al, %eax
	setp %cl
	movzbl %cl, %ecx
	orl %ecx, %eax
	jnz .Lbb21
	movl $0, %eax
	jmp .Lbb22
.Lbb21:
	movl $1, %eax
.Lbb22:
	ret
.type ne2, @function
.size ne2, .-ne2
/* end function ne2 */

.text
.balign 16
.globl ne3
ne3:
	endbr64
	ucomisd %xmm1, %xmm0
	setnz %al
	movzbl %al, %eax
	setp %cl
	movzbl %cl, %ecx
	orl %ecx, %eax
	jnz .Lbb25
	movl $0, %eax
.Lbb25:
	ret
.type ne3, @function
.size ne3, .-ne3
/* end function ne3 */

.text
.balign 16
.globl o
o:
	endbr64
	ucomisd %xmm1, %xmm0
	setnp %al
	movzbl %al, %eax
	ret
.type o, @function
.size o, .-o
/* end function o */

.text
.balign 16
.globl uo
uo:
	endbr64
	ucomisd %xmm1, %xmm0
	setp %al
	movzbl %al, %eax
	ret
.type uo, @function
.size uo, .-uo
/* end function uo */

.section .note.GNU-stack,"",@progbits
