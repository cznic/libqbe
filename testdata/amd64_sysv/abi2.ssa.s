.text
.balign 16
.globl sum
sum:
	endbr64
	subq $16, %rsp
	movq %rdi, 0(%rsp)
	movq %xmm0, 8(%rsp)
	movss 0(%rsp), %xmm0
	movss 8(%rsp), %xmm1
	addss %xmm1, %xmm0
	addq $16, %rsp
	ret
.type sum, @function
.size sum, .-sum
/* end function sum */

.section .note.GNU-stack,"",@progbits
