.text
.balign 16
epar:
	endbr64
	addq %rdi, %rax
	ret
.type epar, @function
.size epar, .-epar
/* end function epar */

.text
.balign 16
.globl earg
earg:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movq %rdi, %rax
	movq %rsi, %rdi
	callq epar
	movq %rax, %rdi
	movl $113, %eax
	callq labs
	leave
	ret
.type earg, @function
.size earg, .-earg
/* end function earg */

.section .note.GNU-stack,"",@progbits
