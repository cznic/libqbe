.text
.balign 16
.globl test
test:
	endbr64
	movl $100, %ecx
	movl $0, %eax
.Lbb2:
	addl %ecx, %eax
	subl $1, %ecx
	jnz .Lbb2
	movl %eax, a(%rip)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
