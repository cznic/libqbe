.text
.balign 16
.globl main
main:
	endbr64
	subq $16, %rsp
	movl $4, 0(%rsp)
	movl $5, 4(%rsp)
	leaq 0(%rsp), %rax
.Lbb1:
	movl (%rax), %eax
	cmpl $5, %eax
	leaq 4(%rsp), %rax
	jnz .Lbb1
	movl $0, %eax
	addq $16, %rsp
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
