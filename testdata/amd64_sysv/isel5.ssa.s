.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movsd ".Lfp0"(%rip), %xmm0
	leaq fmt(%rip), %rdi
	movl $1, %eax
	callq printf
	movl $0, %eax
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.data
.balign 8
fmt:
	.ascii "%.06f\n"
	.byte 0
/* end data */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 858993459
	.int 1072902963 /* 1.200000 */

.section .note.GNU-stack,"",@progbits
