.text
.balign 16
alpha:
	endbr64
	movq %rdi, %rax
	addq %rdx, %rax
.Lbb1:
	movb %sil, (%rdi)
	movq %rdi, %rcx
	addq $1, %rdi
	addl $1, %esi
	cmpq %rax, %rcx
	jnz .Lbb1
	movb $0, (%rax)
	ret
.type alpha, @function
.size alpha, .-alpha
/* end function alpha */

.text
.balign 16
.globl test
test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movq %rdi, %rbx
	movl $16, %edx
	movl $65, %esi
	leaq -20(%rbp), %rdi
	callq alpha
	movq %rbx, %rax
	movzbl -20(%rbp), %ecx
	movb %cl, 0(%rax)
	movq -19(%rbp), %rcx
	movq %rcx, 1(%rax)
	movq -11(%rbp), %rcx
	movq %rcx, 9(%rax)
	popq %rbx
	leave
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
