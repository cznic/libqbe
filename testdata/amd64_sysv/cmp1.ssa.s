.text
.balign 16
.globl test
test:
	endbr64
	cmpl $1, %edi
	seta %al
	movzbl %al, %eax
	jbe .Lbb2
	movl $1, %eax
.Lbb2:
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
