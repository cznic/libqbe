.text
.balign 16
.globl strcmp
strcmp:
	endbr64
.Lbb1:
	movsbl (%rdi), %eax
	cmpl $0, %eax
	jz .Lbb5
	movsbl (%rsi), %ecx
	cmpl $0, %ecx
	jz .Lbb5
	cmpl %ecx, %eax
	jnz .Lbb5
	addq $1, %rdi
	addq $1, %rsi
	jmp .Lbb1
.Lbb5:
	movzbl %al, %eax
	movzbl (%rsi), %ecx
	subl %ecx, %eax
	ret
.type strcmp, @function
.size strcmp, .-strcmp
/* end function strcmp */

.section .note.GNU-stack,"",@progbits
