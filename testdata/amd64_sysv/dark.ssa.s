.data
.balign 8
ret:
	.quad 0
/* end data */

.text
.balign 16
.globl test
test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl a(%rip), %eax
	addl $1, %eax
	movl %eax, a(%rip)
	movq ret(%rip), %rcx
	movq 8(%rbp), %rax
	movq %rax, ret(%rip)
	cmpq %rax, %rcx
	jz .Lbb2
	callq test
.Lbb2:
	leave
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
