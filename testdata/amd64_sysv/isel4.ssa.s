.text
.balign 16
.globl f0
f0:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
.type f0, @function
.size f0, .-f0
/* end function f0 */

.text
.balign 16
.globl f1
f1:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	endbr64
	imulq $4, %rsi, %rax
	movslq 8(%rdi, %rax, 1), %rax
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.section .note.GNU-stack,"",@progbits
