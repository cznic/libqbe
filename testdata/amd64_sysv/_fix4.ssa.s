.text
.balign 16
test:
	endbr64
	movl $3, %ecx
	movl $2, %esi
.Lbb2:
	cmpl $10000, %esi
	jz .Lbb9
	addl $2, %ecx
	movl $3, %edi
.Lbb5:
	movl %edi, %eax
	imull %edi, %eax
	cmpl %ecx, %eax
	jg .Lbb8
	movl %ecx, %eax
	cltd
	idivl %edi
	movl %edx, %eax
	cmpl $0, %eax
	jz .Lbb2
	addl $2, %edi
	jmp .Lbb5
.Lbb8:
	addl $1, %esi
	jmp .Lbb2
.Lbb9:
	movl %ecx, a(%rip)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
