.text
.balign 16
.globl test
test:
	endbr64
	subq $24, %rsp
	leaq 0(%rsp), %rax
	addq $8, %rax
	movl $16, %ecx
	cltd
	idivl %ecx
	movl %edx, %eax
	movl %eax, 8(%rsp)
	movl %eax, a(%rip)
	addq $24, %rsp
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
