.text
.balign 16
alpha:
	endbr64
	movq %rdi, %rax
	addq %rdx, %rax
.Lbb1:
	movb %sil, (%rdi)
	movq %rdi, %rcx
	addq $1, %rdi
	addl $1, %esi
	cmpq %rax, %rcx
	jnz .Lbb1
	movb $0, (%rax)
	ret
.type alpha, @function
.size alpha, .-alpha
/* end function alpha */

.text
.balign 16
.globl test
test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $48, %rsp
	movl $16, %edx
	movl $65, %esi
	leaq -40(%rbp), %rdi
	callq alpha
	movl $16, %edx
	movl $97, %esi
	leaq -20(%rbp), %rdi
	callq alpha
	subq $80, %rsp
	movq %rsp, %rcx
	movzbl -20(%rbp), %eax
	movb %al, 48(%rcx)
	movq -19(%rbp), %rax
	movq %rax, 49(%rcx)
	movq -11(%rbp), %rax
	movq %rax, 57(%rcx)
	movq $9, 40(%rcx)
	movq $8, 32(%rcx)
	movq $7, 24(%rcx)
	movzbl -40(%rbp), %eax
	movb %al, 0(%rcx)
	movq -39(%rbp), %rax
	movq %rax, 1(%rcx)
	movq -31(%rbp), %rax
	movq %rax, 9(%rcx)
	movl $6, %r9d
	movl $5, %r8d
	movl $4, %ecx
	movl $3, %edx
	movl $2, %esi
	movl $1, %edi
	callq fcb
	subq $-80, %rsp
	leave
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
