.text
.balign 16
.globl t0
t0:
	endbr64
	movl $128, %edx
	movl $256, %eax
.Lbb2:
	movl %edx, %ecx
	movl %eax, %edx
	shrl $1, %edx
	jz .Lbb4
	movl %ecx, %eax
	jmp .Lbb2
.Lbb4:
	ret
.type t0, @function
.size t0, .-t0
/* end function t0 */

.text
.balign 16
.globl t1
t1:
	endbr64
	movl $256, %eax
	movl $128, %edx
.Lbb7:
	movl %edx, %ecx
	movl %eax, %edx
	shrl $1, %edx
	jz .Lbb9
	movl %ecx, %eax
	jmp .Lbb7
.Lbb9:
	ret
.type t1, @function
.size t1, .-t1
/* end function t1 */

.section .note.GNU-stack,"",@progbits
