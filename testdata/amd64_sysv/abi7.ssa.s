.text
.balign 16
.globl test
test:
	endbr64
	movq s(%rip), %rax
	leaq s(%rip), %rcx
	addq $8, %rcx
	movq (%rcx), %rdx
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
