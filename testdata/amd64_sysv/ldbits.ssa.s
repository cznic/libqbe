.text
.balign 16
.globl tests
tests:
	endbr64
	movl $1, a(%rip)
	movl $2, a(%rip)
	movl $3, a(%rip)
	movl $0, a(%rip)
	ret
.type tests, @function
.size tests, .-tests
/* end function tests */

.section .note.GNU-stack,"",@progbits
