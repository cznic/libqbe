.text
.balign 16
.globl fneg
fneg:
	endbr64
	movq %xmm0, %rax
	xorl $2147483648, %eax
	movq %rax, %xmm0
	ret
.type fneg, @function
.size fneg, .-fneg
/* end function fneg */

.text
.balign 16
.globl ftrunc
ftrunc:
	endbr64
	cvttsd2sil %xmm0, %eax
	cvtsi2sd %eax, %xmm0
	ret
.type ftrunc, @function
.size ftrunc, .-ftrunc
/* end function ftrunc */

.text
.balign 16
.globl wtos
wtos:
	endbr64
	movl %edi, %eax
	cvtsi2ss %rax, %xmm0
	ret
.type wtos, @function
.size wtos, .-wtos
/* end function wtos */

.text
.balign 16
.globl wtod
wtod:
	endbr64
	movl %edi, %eax
	cvtsi2sd %rax, %xmm0
	ret
.type wtod, @function
.size wtod, .-wtod
/* end function wtod */

.text
.balign 16
.globl ltos
ltos:
	endbr64
	movq %rdi, %rax
	andq $1, %rax
	movq %rdi, %rdx
	shrq $63, %rdx
	movl %edx, %ecx
	movq %rdi, %rsi
	shrq %cl, %rsi
	orq %rsi, %rax
	cvtsi2ss %rax, %xmm0
	movq %xmm0, %rax
	movl %edx, %ecx
	shll $23, %ecx
	addl %ecx, %eax
	movq %rax, %xmm0
	ret
.type ltos, @function
.size ltos, .-ltos
/* end function ltos */

.text
.balign 16
.globl ltod
ltod:
	endbr64
	movq %rdi, %rax
	andq $1, %rax
	movq %rdi, %rdx
	shrq $63, %rdx
	movl %edx, %ecx
	movq %rdi, %rsi
	shrq %cl, %rsi
	orq %rsi, %rax
	cvtsi2sd %rax, %xmm0
	movq %xmm0, %rax
	movq %rdx, %rcx
	shlq $52, %rcx
	addq %rcx, %rax
	movq %rax, %xmm0
	ret
.type ltod, @function
.size ltod, .-ltod
/* end function ltod */

.text
.balign 16
.globl stow
stow:
	endbr64
	cvttss2siq %xmm0, %rax
	ret
.type stow, @function
.size stow, .-stow
/* end function stow */

.text
.balign 16
.globl dtow
dtow:
	endbr64
	cvttsd2siq %xmm0, %rax
	ret
.type dtow, @function
.size dtow, .-dtow
/* end function dtow */

.text
.balign 16
.globl stol
stol:
	endbr64
	cvttss2siq %xmm0, %rax
	movq %rax, %rdx
	sarq $63, %rdx
	addss ".Lfp0"(%rip), %xmm0
	cvttss2siq %xmm0, %rcx
	andq %rdx, %rcx
	orq %rcx, %rax
	ret
.type stol, @function
.size stol, .-stol
/* end function stol */

.text
.balign 16
.globl dtol
dtol:
	endbr64
	cvttsd2siq %xmm0, %rax
	movq %rax, %rdx
	sarq $63, %rdx
	addsd ".Lfp1"(%rip), %xmm0
	cvttsd2siq %xmm0, %rcx
	andq %rdx, %rcx
	orq %rcx, %rax
	ret
.type dtol, @function
.size dtol, .-dtol
/* end function dtol */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int -1008730112 /* -9223372036854775808.000000 */

.section .rodata
.p2align 2
.Lfp0:
	.int -553648128 /* -9223372036854775808.000000 */

.section .note.GNU-stack,"",@progbits
