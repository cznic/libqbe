.text
.balign 16
.globl test
test:
	endbr64
	movl $747, %esi
	movl $380, %ecx
.Lbb2:
	cmpl $0, %ecx
	jz .Lbb5
	movl %esi, %eax
	cltd
	idivl %ecx
	movl %edx, %esi
	xchgl %ecx, %esi
	jmp .Lbb2
.Lbb5:
	movl %esi, %eax
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
