.data
.balign 8
dfmt:
	.ascii "double: %g\n"
	.byte 0
/* end data */

.text
.balign 16
.globl f
f:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $64, %rsp
	movsd %xmm7, -16(%rbp)
	movsd %xmm4, -8(%rbp)
	movq %xmm5, -64(%rbp)
	movq %xmm6, -56(%rbp)
	movq %xmm2, -48(%rbp)
	movq %xmm3, -40(%rbp)
	movq %xmm0, -32(%rbp)
	movq %xmm1, -24(%rbp)
	movsd -32(%rbp), %xmm0
	movsd -24(%rbp), %xmm1
	callq phfa3
	movsd -48(%rbp), %xmm0
	movsd -40(%rbp), %xmm1
	callq phfa3
	movsd -64(%rbp), %xmm0
	movsd -56(%rbp), %xmm1
	callq phfa3
	movsd -8(%rbp), %xmm0
	leaq dfmt(%rip), %rdi
	movl $1, %eax
	callq printf
	movsd -16(%rbp), %xmm0
	leaq dfmt(%rip), %rdi
	movl $1, %eax
	callq printf
	leave
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
