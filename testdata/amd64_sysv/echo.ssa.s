.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	pushq %rbx
	pushq %r12
	movq $1663398693, -8(%rbp)
	movq %rsi, %rbx
	addq $8, %rbx
	movl %edi, %r12d
	subl $1, %r12d
.Lbb1:
	cmpl $0, %r12d
	jz .Lbb6
	cmpl $1, %r12d
	jz .Lbb4
	movl $32, %edx
	jmp .Lbb5
.Lbb4:
	movl $10, %edx
.Lbb5:
	movq (%rbx), %rsi
	leaq -8(%rbp), %rdi
	movl $0, %eax
	callq printf
	addq $8, %rbx
	subl $1, %r12d
	jmp .Lbb1
.Lbb6:
	movl $0, %eax
	popq %r12
	popq %rbx
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
