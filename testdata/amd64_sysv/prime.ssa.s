.text
.balign 16
.globl test
test:
	endbr64
	movl $13, %r8d
	movl $5, %edi
.Lbb2:
	movl %r8d, %esi
	movl %esi, %r8d
	addl $2, %r8d
	movl $3, %ecx
.Lbb4:
	movl %esi, %eax
	cltd
	idivl %ecx
	movl %edx, %eax
	cmpl $0, %eax
	jz .Lbb2
	addl $2, %ecx
	movl %ecx, %eax
	imull %ecx, %eax
	cmpl %esi, %eax
	jle .Lbb4
	addl $1, %edi
	cmpl $10001, %edi
	jnz .Lbb2
	movl %esi, a(%rip)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
