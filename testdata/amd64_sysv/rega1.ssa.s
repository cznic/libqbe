.text
.balign 16
.globl f
f:
	endbr64
	movq %rdi, %rax
	movq %rax, %rdi
	movl $42, %eax
.Lbb2:
	subq $1, %rdi
	cmpl $0, %eax
	jnz .Lbb5
	cmpl $0, %edi
	jz .Lbb7
	movq %rdi, %rax
	jmp .Lbb2
.Lbb5:
	cmpl $0, %edi
	jz .Lbb7
	movq %rdi, %rax
	jmp .Lbb2
.Lbb7:
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
