.text
.balign 16
.globl slt
slt:
	endbr64
	cmpl %esi, %edi
	setl %al
	movzbl %al, %eax
	ret
.type slt, @function
.size slt, .-slt
/* end function slt */

.text
.balign 16
.globl sle
sle:
	endbr64
	cmpl %esi, %edi
	setle %al
	movzbl %al, %eax
	ret
.type sle, @function
.size sle, .-sle
/* end function sle */

.text
.balign 16
.globl sgt
sgt:
	endbr64
	cmpl %esi, %edi
	setg %al
	movzbl %al, %eax
	ret
.type sgt, @function
.size sgt, .-sgt
/* end function sgt */

.text
.balign 16
.globl sge
sge:
	endbr64
	cmpl %esi, %edi
	setge %al
	movzbl %al, %eax
	ret
.type sge, @function
.size sge, .-sge
/* end function sge */

.text
.balign 16
.globl ult
ult:
	endbr64
	cmpl %esi, %edi
	setb %al
	movzbl %al, %eax
	ret
.type ult, @function
.size ult, .-ult
/* end function ult */

.text
.balign 16
.globl ule
ule:
	endbr64
	cmpl %esi, %edi
	setbe %al
	movzbl %al, %eax
	ret
.type ule, @function
.size ule, .-ule
/* end function ule */

.text
.balign 16
.globl ugt
ugt:
	endbr64
	cmpl %esi, %edi
	seta %al
	movzbl %al, %eax
	ret
.type ugt, @function
.size ugt, .-ugt
/* end function ugt */

.text
.balign 16
.globl uge
uge:
	endbr64
	cmpl %esi, %edi
	setae %al
	movzbl %al, %eax
	ret
.type uge, @function
.size uge, .-uge
/* end function uge */

.text
.balign 16
.globl eq
eq:
	endbr64
	cmpl %esi, %edi
	setz %al
	movzbl %al, %eax
	ret
.type eq, @function
.size eq, .-eq
/* end function eq */

.text
.balign 16
.globl ne
ne:
	endbr64
	cmpl %esi, %edi
	setnz %al
	movzbl %al, %eax
	ret
.type ne, @function
.size ne, .-ne
/* end function ne */

.section .note.GNU-stack,"",@progbits
