.text
.balign 16
rand:
	endbr64
	movl $0, %eax
	ret
.type rand, @function
.size rand, .-rand
/* end function rand */

.text
.balign 16
chk:
	endbr64
	cmpl $1, %edi
	setz %al
	movzbl %al, %eax
	cmpl $0, %esi
	setz %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	xorl $1, %eax
	ret
.type chk, @function
.size chk, .-chk
/* end function chk */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movl $1, -8(%rbp)
	movl $0, -4(%rbp)
	callq rand
	cmpl $0, %eax
	movl $0, %esi
	movl $1, %edi
	callq chk
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
