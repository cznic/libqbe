.section .tdata,"awT"
.balign 4
i:
	.int 42
/* end data */

.data
.balign 1
fmti:
	.ascii "i%d==%d\n"
	.byte 0
/* end data */

.section .tdata,"awT"
.balign 8
x:
	.int 1
	.int 2
	.int 3
	.int 4
/* end data */

.data
.balign 1
fmtx:
	.ascii "*(x+%d)==%d\n"
	.byte 0
/* end data */

.text
.balign 16
.globl main
main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movl $0, %ecx
	leaq thread(%rip), %rdx
	movl $0, %esi
	leaq -16(%rbp), %rdi
	callq pthread_create
	movq -16(%rbp), %rdi
	leaq -8(%rbp), %rsi
	callq pthread_join
	movl %fs:i@tpoff, %edx
	movl $0, %esi
	leaq fmti(%rip), %rdi
	movl $0, %eax
	callq printf
	movl -8(%rbp), %edx
	movl $1, %esi
	leaq fmti(%rip), %rdi
	movl $0, %eax
	callq printf
	callq xaddr
	movl (%rax), %edx
	movl $0, %esi
	leaq fmtx(%rip), %rdi
	movl $0, %eax
	callq printf
	callq xaddroff4
	movl (%rax), %edx
	movl $4, %esi
	leaq fmtx(%rip), %rdi
	movl $0, %eax
	callq printf
	movl $8, %edi
	callq xaddroff
	movl (%rax), %edx
	movl $8, %esi
	leaq fmtx(%rip), %rdi
	movl $0, %eax
	callq printf
	movl $3, %edi
	callq xvalcnt
	movq %rax, %rdx
	movl $12, %esi
	leaq fmtx(%rip), %rdi
	movl $0, %eax
	callq printf
	movl $0, %eax
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.text
.balign 16
thread:
	endbr64
	movb $24, %fs:i@tpoff+3
	movslq %fs:i@tpoff, %rax
	ret
.type thread, @function
.size thread, .-thread
/* end function thread */

.text
.balign 16
xaddr:
	endbr64
	movq %fs:0, %rax
	leaq x@tpoff(%rax), %rax
	ret
.type xaddr, @function
.size xaddr, .-xaddr
/* end function xaddr */

.text
.balign 16
xaddroff4:
	endbr64
	movq %fs:0, %rax
	leaq x@tpoff+4(%rax), %rax
	ret
.type xaddroff4, @function
.size xaddroff4, .-xaddroff4
/* end function xaddroff4 */

.text
.balign 16
xaddroff:
	endbr64
	movq %fs:0, %rax
	leaq x@tpoff(%rax), %rax
	addq %rdi, %rax
	ret
.type xaddroff, @function
.size xaddroff, .-xaddroff
/* end function xaddroff */

.text
.balign 16
xvalcnt:
	endbr64
	movq %fs:0, %rax
	leaq x@tpoff(%rax), %rax
	movl (%rax, %rdi, 4), %eax
	ret
.type xvalcnt, @function
.size xvalcnt, .-xvalcnt
/* end function xvalcnt */

.section .note.GNU-stack,"",@progbits
