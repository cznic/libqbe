.text
.balign 16
.globl test
test:
	endbr64
	subq $16, %rsp
	leaq 0(%rsp), %rcx
	leaq 8(%rsp), %rax
	cmpq %rax, %rcx
	setnz %al
	movzbl %al, %eax
	addq $16, %rsp
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
