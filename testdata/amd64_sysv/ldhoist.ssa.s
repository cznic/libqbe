.text
.balign 16
.globl f
f:
	endbr64
	movl $0, %eax
.Lbb2:
	subl $1, %edi
	cmpl $0, %edi
	jl .Lbb4
	movl (%rsi), %eax
	jmp .Lbb2
.Lbb4:
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
