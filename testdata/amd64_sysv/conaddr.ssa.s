.text
.balign 16
.globl f0
f0:
	endbr64
	leaq a(%rip), %rax
	movzbl (%rax, %rdi, 1), %eax
	ret
.type f0, @function
.size f0, .-f0
/* end function f0 */

.text
.balign 16
.globl f1
f1:
	endbr64
	movzbl 10(%rdi), %eax
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	endbr64
	imulq $2, %rsi, %rax
	movq %rdi, %rcx
	addq %rax, %rcx
	leaq a(%rip), %rax
	addq %rcx, %rax
	movzbl (%rax), %eax
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	endbr64
	leaq a(%rip), %rax
	addq %rdi, %rax
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	endbr64
	leaq p(%rip), %rax
	movq %rax, p(%rip)
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.text
.balign 16
.globl writeto0
writeto0:
	endbr64
	movq $0, 0
	ret
.type writeto0, @function
.size writeto0, .-writeto0
/* end function writeto0 */

.section .note.GNU-stack,"",@progbits
