.text
.balign 16
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 100
	li t0, 0
.L1:
	li t2, 10
	slt t2, t2, t1
	xor t2, t2, 1
	bnez t2, .L3
	subw t0, t0, t1
	j .L4
.L3:
	addw t0, t0, t1
	li t2, 1
	subw t1, t1, t2
.L4:
	li t2, 1
	subw t1, t1, t2
	bnez t1, .L1
	mv a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
