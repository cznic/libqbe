.text
.balign 16
.globl chacha20_rounds_qbe
chacha20_rounds_qbe:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	sd s2, 8(sp)
	sd s3, 16(sp)
	sd s4, 24(sp)
	mv t1, a0
	lw s3, 0(a1)
	add t0, a1, 4
	lw s2, 0(t0)
	add t0, t0, 4
	lw s1, 0(t0)
	add t0, t0, 4
	lw a7, 0(t0)
	add t0, t0, 4
	lw a6, 0(t0)
	add t0, t0, 4
	lw a5, 0(t0)
	add t0, t0, 4
	lw a4, 0(t0)
	add t0, t0, 4
	lw a3, 0(t0)
	add t0, t0, 4
	lw a2, 0(t0)
	add t0, t0, 4
	lw a1, 0(t0)
	add t0, t0, 4
	lw a0, 0(t0)
	add t0, t0, 4
	lw t5, 0(t0)
	add t0, t0, 4
	lw t4, 0(t0)
	add t0, t0, 4
	lw t3, 0(t0)
	add t0, t0, 4
	lw t2, 0(t0)
	add t0, t0, 4
	lw t0, 0(t0)
	addw s3, s3, a6
	xor s4, t4, s3
	sllw t4, s4, 16
	srlw s4, s4, 16
	xor t4, t4, s4
	addw a2, a2, t4
	xor s4, a6, a2
	sllw a6, s4, 12
	srlw s4, s4, 20
	xor a6, a6, s4
	addw s3, s3, a6
	xor s4, t4, s3
	sllw t4, s4, 8
	srlw s4, s4, 24
	xor t4, t4, s4
	addw a2, a2, t4
	xor s4, a6, a2
	sllw a6, s4, 7
	srlw s4, s4, 25
	xor a6, a6, s4
	addw s2, s2, a5
	xor s4, t3, s2
	sllw t3, s4, 16
	srlw s4, s4, 16
	xor t3, t3, s4
	addw a1, a1, t3
	xor s4, a5, a1
	sllw a5, s4, 12
	srlw s4, s4, 20
	xor a5, a5, s4
	addw s2, s2, a5
	xor s4, t3, s2
	sllw t3, s4, 8
	srlw s4, s4, 24
	xor t3, t3, s4
	addw a1, a1, t3
	xor s4, a5, a1
	sllw a5, s4, 7
	srlw s4, s4, 25
	xor a5, a5, s4
	addw s1, s1, a4
	xor s4, t2, s1
	sllw t2, s4, 16
	srlw s4, s4, 16
	xor t2, t2, s4
	addw a0, a0, t2
	xor s4, a4, a0
	sllw a4, s4, 12
	srlw s4, s4, 20
	xor a4, a4, s4
	addw s1, s1, a4
	xor s4, t2, s1
	sllw t2, s4, 8
	srlw s4, s4, 24
	xor t2, t2, s4
	addw a0, a0, t2
	xor s4, a4, a0
	sllw a4, s4, 7
	srlw s4, s4, 25
	xor a4, a4, s4
	addw a7, a7, a3
	xor s4, t0, a7
	sllw t0, s4, 16
	srlw s4, s4, 16
	xor t0, t0, s4
	addw t5, t5, t0
	xor s4, a3, t5
	sllw a3, s4, 12
	srlw s4, s4, 20
	xor a3, a3, s4
	addw a7, a7, a3
	xor s4, t0, a7
	sllw t0, s4, 8
	srlw s4, s4, 24
	xor t0, t0, s4
	addw t5, t5, t0
	xor s4, a3, t5
	sllw a3, s4, 7
	srlw s4, s4, 25
	xor a3, a3, s4
	addw s3, s3, a5
	xor s4, t0, s3
	sllw t0, s4, 16
	srlw s4, s4, 16
	xor t0, t0, s4
	addw a0, a0, t0
	xor s4, a5, a0
	sllw a5, s4, 12
	srlw s4, s4, 20
	xor a5, a5, s4
	addw s3, s3, a5
	xor s4, t0, s3
	sllw t0, s4, 8
	srlw s4, s4, 24
	xor t0, t0, s4
	addw a0, a0, t0
	xor s4, a5, a0
	sllw a5, s4, 7
	srlw s4, s4, 25
	xor a5, a5, s4
	addw s2, s2, a4
	xor s4, t4, s2
	sllw t4, s4, 16
	srlw s4, s4, 16
	xor t4, t4, s4
	addw t5, t5, t4
	xor s4, a4, t5
	sllw a4, s4, 12
	srlw s4, s4, 20
	xor a4, a4, s4
	addw s2, s2, a4
	xor s4, t4, s2
	sllw t4, s4, 8
	srlw s4, s4, 24
	xor t4, t4, s4
	addw t5, t5, t4
	xor s4, a4, t5
	sllw a4, s4, 7
	srlw s4, s4, 25
	xor a4, a4, s4
	addw s1, s1, a3
	xor s4, t3, s1
	sllw t3, s4, 16
	srlw s4, s4, 16
	xor t3, t3, s4
	addw a2, a2, t3
	xor s4, a3, a2
	sllw a3, s4, 12
	srlw s4, s4, 20
	xor a3, a3, s4
	addw s1, s1, a3
	xor s4, t3, s1
	sllw t3, s4, 8
	srlw s4, s4, 24
	xor t3, t3, s4
	addw a2, a2, t3
	xor s4, a3, a2
	sllw a3, s4, 7
	srlw s4, s4, 25
	xor a3, a3, s4
	addw a7, a7, a6
	xor s4, t2, a7
	sllw t2, s4, 16
	srlw s4, s4, 16
	xor t2, t2, s4
	addw a1, a1, t2
	xor s4, a6, a1
	sllw a6, s4, 12
	srlw s4, s4, 20
	xor a6, a6, s4
	addw a7, a7, a6
	xor s4, t2, a7
	sllw t2, s4, 8
	srlw s4, s4, 24
	xor t2, t2, s4
	addw a1, a1, t2
	xor s4, a6, a1
	sllw a6, s4, 7
	srlw s4, s4, 25
	xor a6, a6, s4
	sw s3, 0(t1)
	add t1, t1, 4
	sw s2, 0(t1)
	add t1, t1, 4
	sw s1, 0(t1)
	add t1, t1, 4
	sw a7, 0(t1)
	add t1, t1, 4
	sw a6, 0(t1)
	add t1, t1, 4
	sw a5, 0(t1)
	add t1, t1, 4
	sw a4, 0(t1)
	add t1, t1, 4
	sw a3, 0(t1)
	add t1, t1, 4
	sw a2, 0(t1)
	add t1, t1, 4
	sw a1, 0(t1)
	add t1, t1, 4
	sw a0, 0(t1)
	add t1, t1, 4
	sw t5, 0(t1)
	add t1, t1, 4
	sw t4, 0(t1)
	add t1, t1, 4
	sw t3, 0(t1)
	add t1, t1, 4
	sw t2, 0(t1)
	add t1, t1, 4
	sw t0, 0(t1)
	ld s1, 0(sp)
	ld s2, 8(sp)
	ld s3, 16(sp)
	ld s4, 24(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type chacha20_rounds_qbe, @function
.size chacha20_rounds_qbe, .-chacha20_rounds_qbe
/* end function chacha20_rounds_qbe */

.section .note.GNU-stack,"",@progbits
