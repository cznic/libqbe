.text
.balign 16
.globl t0
t0:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 128
	li t0, 256
	mv t6, t1
	mv t1, t0
	mv t0, t6
.L2:
	mv t2, t0
	srlw t0, t1, 1
	beqz t0, .L4
	mv t1, t2
	j .L2
.L4:
	mv a0, t1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type t0, @function
.size t0, .-t0
/* end function t0 */

.text
.balign 16
.globl t1
t1:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 256
	li t0, 128
.L7:
	mv t2, t0
	srlw t0, t1, 1
	beqz t0, .L9
	mv t1, t2
	j .L7
.L9:
	mv a0, t1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type t1, @function
.size t1, .-t1
/* end function t1 */

.section .note.GNU-stack,"",@progbits
