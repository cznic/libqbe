.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 13
	li t0, 5
	mv t2, t0
.L2:
	addw t3, t1, 2
	li t0, 3
.L3:
	remw t4, t1, t0
	beqz t4, .L9
	li t4, 2
	addw t0, t4, t0
	mulw t4, t0, t0
	slt t4, t1, t4
	beqz t4, .L3
	li t0, 1
	addw t2, t0, t2
	li t0, 10001
	xor t0, t0, t2
	seqz t0, t0
	bnez t0, .L7
	mv t1, t3
	j .L2
.L7:
	sw t1, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.L9:
	mv t1, t3
	j .L2
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
