.text
.balign 16
.globl tests
tests:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 1
	sw t0, a, t6
	li t0, 2
	sw t0, a, t6
	li t0, 3
	sw t0, a, t6
	li t0, 0
	sw t0, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type tests, @function
.size tests, .-tests
/* end function tests */

.section .note.GNU-stack,"",@progbits
