.text
.balign 16
.globl f0
f0:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	add t0, a1, 2
	li t1, 4
	mul t0, t0, t1
	add t0, a0, t0
	lw t0, 0(t0)
	sext.w a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f0, @function
.size f0, .-f0
/* end function f0 */

.text
.balign 16
.globl f1
f1:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 1
	add t0, t0, a1
	add t0, t0, 1
	li t1, 4
	mul t0, t0, t1
	add t0, t0, a0
	lw t0, 0(t0)
	sext.w a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 4
	mul t1, a1, t0
	li t0, 8
	add t0, t0, t1
	add t0, a0, t0
	lw t0, 0(t0)
	sext.w a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 4
	mul t1, a1, t0
	li t0, 4
	add t1, t0, t1
	li t0, 4
	add t0, t0, t1
	add t0, a0, t0
	lw t0, 0(t0)
	sext.w a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 1
	add t0, t0, a1
	li t1, 4
	mul t1, t0, t1
	li t0, 4
	add t0, t0, t1
	add t0, t0, a0
	lw t0, 0(t0)
	sext.w a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.section .note.GNU-stack,"",@progbits
