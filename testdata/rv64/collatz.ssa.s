.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	li t6, 4016
	sub sp, sp, t6
	li t1, 0
	li t0, 1
	mv t6, t1
	mv t1, t0
	mv t0, t6
.L2:
	slt t2, t1, 1000
	mv t3, t0
	li t0, 0
	beqz t2, .L14
	mv t2, t1
.L4:
	xor t4, t1, 1
	snez t4, t4
	beqz t4, .L11
	slt t4, t1, t2
	bnez t4, .L9
	addw t0, t0, 1
	and t4, t1, 1
	bnez t4, .L8
	srlw t1, t1, 1
	j .L4
.L8:
	li t4, 3
	mulw t1, t4, t1
	addw t1, t1, 1
	j .L4
.L9:
	mv t6, t1
	mv t1, t2
	mv t2, t6
	sext.w t2, t2
	li t4, 4
	mul t2, t2, t4
	li t4, -4000
	add t4, fp, t4
	add t2, t2, t4
	lw t2, 0(t2)
	addw t0, t0, t2
	j .L12
.L11:
	mv t1, t2
.L12:
	sext.w t2, t1
	li t4, 4
	mul t2, t2, t4
	li t4, -4000
	add t4, fp, t4
	add t2, t2, t4
	sw t0, 0(t2)
	mv t2, t1
	li t1, 1
	addw t1, t1, t2
	slt t2, t0, t3
	xor t2, t2, 1
	bnez t2, .L2
	mv t0, t3
	j .L2
.L14:
	mv t0, t3
	sw t0, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
