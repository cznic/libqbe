.data
.balign 8
z:
	.int 0
/* end data */

.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	lw t0, z
	addw s1, t0, t0
	sext.w t1, s1
	add t0, fp, -16
	add t2, t0, 12
	li t0, 4
	sd t0, -16(fp)
	li t0, 5
	sw t0, 0(t2)
	la t0, F
	add t0, t0, t1
	li a6, 6
	add t1, fp, -16
	add t1, t1, 8
	ld a5, 0(t1)
	add t1, fp, -16
	add t1, t1, 0
	ld a4, 0(t1)
	li a3, 3
	li a2, 2
	li a1, 1
	mv a0, s1
	jalr t0
	mv t0, a0
	addw t0, s1, t0
	sw t0, a, t6
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
