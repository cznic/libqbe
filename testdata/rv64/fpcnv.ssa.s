.text
.balign 16
.globl fneg
fneg:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fmv.x.w t1, fa0
	li t0, -2147483648
	xor t0, t0, t1
	fmv.w.x fa0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type fneg, @function
.size fneg, .-fneg
/* end function fneg */

.text
.balign 16
.globl ftrunc
ftrunc:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.w.d t0, fa0, rtz
	fcvt.d.w fa0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ftrunc, @function
.size ftrunc, .-ftrunc
/* end function ftrunc */

.text
.balign 16
.globl wtos
wtos:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.s.wu fa0, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type wtos, @function
.size wtos, .-wtos
/* end function wtos */

.text
.balign 16
.globl wtod
wtod:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.d.wu fa0, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type wtod, @function
.size wtod, .-wtod
/* end function wtod */

.text
.balign 16
.globl ltos
ltos:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.s.lu fa0, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ltos, @function
.size ltos, .-ltos
/* end function ltos */

.text
.balign 16
.globl ltod
ltod:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.d.lu fa0, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ltod, @function
.size ltod, .-ltod
/* end function ltod */

.text
.balign 16
.globl stow
stow:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.wu.s a0, fa0, rtz
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type stow, @function
.size stow, .-stow
/* end function stow */

.text
.balign 16
.globl dtow
dtow:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.wu.d a0, fa0, rtz
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type dtow, @function
.size dtow, .-dtow
/* end function dtow */

.text
.balign 16
.globl stol
stol:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.lu.s a0, fa0, rtz
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type stol, @function
.size stol, .-stol
/* end function stol */

.text
.balign 16
.globl dtol
dtol:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fcvt.lu.d a0, fa0, rtz
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type dtol, @function
.size dtol, .-dtol
/* end function dtol */

.section .note.GNU-stack,"",@progbits
