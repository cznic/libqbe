.text
.balign 16
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
