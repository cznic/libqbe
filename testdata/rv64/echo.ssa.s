.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	sd s2, 8(sp)
	li t0, 1663398693
	sd t0, -8(fp)
	add s1, a1, 8
	li t0, 1
	subw s2, a0, t0
.L1:
	xor t0, s2, 0
	seqz t0, t0
	bnez t0, .L6
	xor t0, s2, 1
	seqz t0, t0
	bnez t0, .L4
	li t0, 32
	j .L5
.L4:
	li t0, 10
.L5:
	mv a2, t0
	ld a1, 0(s1)
	add a0, fp, -8
	call printf
	add s1, s1, 8
	li t0, 1
	subw s2, s2, t0
	j .L1
.L6:
	li a0, 0
	ld s1, 0(sp)
	ld s2, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
