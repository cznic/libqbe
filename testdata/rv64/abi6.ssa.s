.data
.balign 8
dfmt:
	.ascii "double: %g\n"
	.byte 0
/* end data */

.text
.balign 16
.globl f
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	fsd fs0, 0(sp)
	fsd fs1, 8(sp)
	fmv.d fs0, fa1
	fmv.d fs1, fa0
	add t0, fp, -16
	add t0, t0, 8
	sd a5, 0(t0)
	add t0, fp, -16
	add t0, t0, 0
	sd a4, 0(t0)
	add t0, fp, -32
	add t0, t0, 8
	sd a3, 0(t0)
	add t0, fp, -32
	add t0, t0, 0
	sd a2, 0(t0)
	add t0, fp, -48
	add t0, t0, 8
	sd a1, 0(t0)
	add t0, fp, -48
	add t0, t0, 0
	sd a0, 0(t0)
	add t0, fp, -48
	add t0, t0, 8
	ld a1, 0(t0)
	add t0, fp, -48
	add t0, t0, 0
	ld a0, 0(t0)
	call phfa3
	add t0, fp, -32
	add t0, t0, 8
	ld a1, 0(t0)
	add t0, fp, -32
	add t0, t0, 0
	ld a0, 0(t0)
	call phfa3
	add t0, fp, -16
	add t0, t0, 8
	ld a1, 0(t0)
	add t0, fp, -16
	add t0, t0, 0
	ld a0, 0(t0)
	call phfa3
	fmv.d fa0, fs1
	fmv.x.d a1, fa0
	la a0, dfmt
	call printf
	fmv.d fa1, fs0
	fmv.x.d a1, fa1
	la a0, dfmt
	call printf
	fld fs0, 0(sp)
	fld fs1, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
