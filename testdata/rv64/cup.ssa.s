.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, -1988
.L1:
	li t1, 1
	add t0, t1, t0
	slt t1, t0, 1991
	xor t1, t1, 1
	beqz t1, .L1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
