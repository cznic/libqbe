.data
.balign 8
arr:
	.byte 10
	.byte -60
	.byte 10
	.byte 100
	.byte 200
	.byte 0
/* end data */

.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	la t1, arr
	li t0, -1
.L1:
	lbu t2, 0(t1)
	li t3, 1
	add t1, t3, t1
	beqz t2, .L4
	slt t3, t2, t0
	xor t3, t3, 1
	beqz t3, .L1
	mv t0, t2
	j .L1
.L4:
	sw t0, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
