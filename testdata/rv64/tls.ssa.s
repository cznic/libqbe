.section .tdata,"awT"
.balign 4
i:
	.int 42
/* end data */

.data
.balign 1
fmti:
	.ascii "i%d==%d\n"
	.byte 0
/* end data */

.section .tdata,"awT"
.balign 8
x:
	.int 1
	.int 2
	.int 3
	.int 4
/* end data */

.data
.balign 1
fmtx:
	.ascii "*(x+%d)==%d\n"
	.byte 0
/* end data */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	li a3, 0
	la a2, thread
	li a1, 0
	add a0, fp, -16
	call pthread_create
	ld a0, -16(fp)
	add a1, fp, -8
	call pthread_join
	lui t6, %tprel_hi(i)
	add t6, t6, tp, %tprel_add(i)
	addi t6, t6, %tprel_lo(i)
	lw a2, 0(t6)
	li a1, 0
	la a0, fmti
	call printf
	lw a2, -8(fp)
	li a1, 1
	la a0, fmti
	call printf
	call xaddr
	mv t0, a0
	lw a2, 0(t0)
	li a1, 0
	la a0, fmtx
	call printf
	call xaddroff4
	mv t0, a0
	lw a2, 0(t0)
	li a1, 4
	la a0, fmtx
	call printf
	li a0, 8
	call xaddroff
	mv t0, a0
	lw a2, 0(t0)
	li a1, 8
	la a0, fmtx
	call printf
	li a0, 3
	call xvalcnt
	mv t0, a0
	sext.w a2, t0
	li a1, 12
	la a0, fmtx
	call printf
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.text
.balign 16
thread:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 24
	lui t6, %tprel_hi(i)+3
	add t6, t6, tp, %tprel_add(i)+3
	addi t6, t6, %tprel_lo(i)+3
	sb t0, 0(t6)
	lui t6, %tprel_hi(i)
	add t6, t6, tp, %tprel_add(i)
	addi t6, t6, %tprel_lo(i)
	lw a0, 0(t6)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type thread, @function
.size thread, .-thread
/* end function thread */

.text
.balign 16
xaddr:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	lui a0, %tprel_hi(x)
	add a0, a0, tp, %tprel_add(x)
	addi a0, a0, %tprel_lo(x)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type xaddr, @function
.size xaddr, .-xaddr
/* end function xaddr */

.text
.balign 16
xaddroff4:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	lui a0, %tprel_hi(x)+4
	add a0, a0, tp, %tprel_add(x)+4
	addi a0, a0, %tprel_lo(x)+4
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type xaddroff4, @function
.size xaddroff4, .-xaddroff4
/* end function xaddroff4 */

.text
.balign 16
xaddroff:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	lui t0, %tprel_hi(x)
	add t0, t0, tp, %tprel_add(x)
	addi t0, t0, %tprel_lo(x)
	add a0, t0, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type xaddroff, @function
.size xaddroff, .-xaddroff
/* end function xaddroff */

.text
.balign 16
xvalcnt:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 4
	mul t1, t0, a0
	lui t0, %tprel_hi(x)
	add t0, t0, tp, %tprel_add(x)
	addi t0, t0, %tprel_lo(x)
	add t0, t0, t1
	lw a0, 0(t0)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type xvalcnt, @function
.size xvalcnt, .-xvalcnt
/* end function xvalcnt */

.section .note.GNU-stack,"",@progbits
