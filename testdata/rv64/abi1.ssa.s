.text
.balign 16
alpha:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	add t0, a0, a2
.L1:
	sb a1, 0(a0)
	mv t1, a0
	add a0, a0, 1
	addw a1, a1, 1
	xor t1, t1, t0
	seqz t1, t1
	beqz t1, .L1
	li t1, 0
	sb t1, 0(t0)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type alpha, @function
.size alpha, .-alpha
/* end function alpha */

.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -112
	li a2, 16
	li a1, 65
	add a0, fp, -40
	call alpha
	li a2, 16
	li a1, 97
	add a0, fp, -20
	call alpha
	li t0, 32
	sub sp, sp, t0
	mv t0, sp
	add t2, t0, 16
	add t1, fp, -64
	sd t1, 0(t2)
	add t2, t0, 8
	li t1, 9
	sext.w t1, t1
	sd t1, 0(t2)
	add t1, t0, 0
	li t0, 8
	sext.w t0, t0
	sd t0, 0(t1)
	add t0, fp, -20
	add t0, t0, 0
	lbu t0, 0(t0)
	add t1, fp, -64
	add t1, t1, 0
	sb t0, 0(t1)
	add t0, fp, -20
	add t0, t0, 1
	ld t0, 0(t0)
	add t1, fp, -64
	add t1, t1, 1
	sd t0, 0(t1)
	add t0, fp, -20
	add t0, t0, 9
	ld t0, 0(t0)
	add t1, fp, -64
	add t1, t1, 9
	sd t0, 0(t1)
	add t0, fp, -40
	add t0, t0, 0
	lbu t0, 0(t0)
	add t1, fp, -88
	add t1, t1, 0
	sb t0, 0(t1)
	add t0, fp, -40
	add t0, t0, 1
	ld t0, 0(t0)
	add t1, fp, -88
	add t1, t1, 1
	sd t0, 0(t1)
	add t0, fp, -40
	add t0, t0, 9
	ld t0, 0(t0)
	add t1, fp, -88
	add t1, t1, 9
	sd t0, 0(t1)
	li a7, 7
	li a6, 6
	li a5, 5
	li a4, 4
	li a3, 3
	li a2, 2
	li a1, 1
	add a0, fp, -88
	call fcb
	li t0, -32
	sub sp, sp, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
