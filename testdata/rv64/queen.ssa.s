.text
.balign 16
.globl chk
chk:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 0
	li t0, 0
.L1:
	lw t5, glo1
	slt t2, t1, t5
	beqz t2, .L11
	ld t2, glo3
	sext.w t4, a0
	li t3, 8
	mul t3, t3, t4
	add t3, t2, t3
	ld t3, 0(t3)
	sext.w a2, t1
	li t4, 4
	mul t4, t4, a2
	add t3, t3, t4
	lw t3, 0(t3)
	addw t0, t0, t3
	sext.w t4, t1
	li t3, 8
	mul t3, t3, t4
	add t3, t2, t3
	ld t3, 0(t3)
	sext.w a2, a1
	li t4, 4
	mul t4, t4, a2
	add t3, t3, t4
	lw t3, 0(t3)
	addw t0, t0, t3
	addw t3, a0, t1
	slt t3, t3, t5
	addw t4, a1, t1
	slt t4, t4, t5
	and t3, t3, t4
	beqz t3, .L4
	addw t3, a0, t1
	sext.w t4, t3
	li t3, 8
	mul t3, t3, t4
	add t3, t2, t3
	ld t3, 0(t3)
	addw t4, a1, t1
	sext.w a2, t4
	li t4, 4
	mul t4, t4, a2
	add t3, t3, t4
	lw t3, 0(t3)
	addw t0, t0, t3
.L4:
	addw t3, a0, t1
	slt t3, t3, t5
	subw t4, a1, t1
	slt t4, t4, 0
	xor t4, t4, 1
	and t3, t3, t4
	beqz t3, .L6
	addw t3, a0, t1
	sext.w t4, t3
	li t3, 8
	mul t3, t3, t4
	add t3, t2, t3
	ld t3, 0(t3)
	subw t4, a1, t1
	sext.w a2, t4
	li t4, 4
	mul t4, t4, a2
	add t3, t3, t4
	lw t3, 0(t3)
	addw t0, t0, t3
.L6:
	subw t3, a0, t1
	slt t3, t3, 0
	xor t3, t3, 1
	addw t4, a1, t1
	slt t4, t4, t5
	and t3, t3, t4
	beqz t3, .L8
	subw t3, a0, t1
	sext.w t4, t3
	li t3, 8
	mul t3, t3, t4
	add t3, t2, t3
	ld t3, 0(t3)
	addw t4, a1, t1
	sext.w t5, t4
	li t4, 4
	mul t4, t4, t5
	add t3, t3, t4
	lw t3, 0(t3)
	addw t0, t0, t3
.L8:
	subw t3, a0, t1
	slt t3, t3, 0
	xor t3, t3, 1
	subw t4, a1, t1
	slt t4, t4, 0
	xor t4, t4, 1
	and t3, t3, t4
	beqz t3, .L10
	subw t3, a0, t1
	sext.w t4, t3
	li t3, 8
	mul t3, t3, t4
	add t2, t2, t3
	ld t2, 0(t2)
	subw t3, a1, t1
	sext.w t4, t3
	li t3, 4
	mul t3, t3, t4
	add t2, t2, t3
	lw t2, 0(t2)
	addw t0, t0, t2
.L10:
	addw t1, t1, 1
	j .L1
.L11:
	mv a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type chk, @function
.size chk, .-chk
/* end function chk */

.text
.balign 16
.globl go
go:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	sd s1, 0(sp)
	sd s2, 8(sp)
	lw t0, glo1
	xor t0, a0, t0
	seqz t0, t0
	bnez t0, .L21
	li t0, 0
	mv s1, t0
.L16:
	lw t0, glo1
	slt t0, s1, t0
	beqz t0, .L20
	mv a1, a0
	mv s2, a0
	mv a0, s1
	call chk
	mv t0, a0
	mv a0, s2
	xor t0, t0, 0
	seqz t0, t0
	beqz t0, .L19
	ld t0, glo3
	sext.w t2, s1
	li t1, 8
	mul t1, t1, t2
	add t0, t0, t1
	ld t0, 0(t0)
	sext.w t2, a0
	li t1, 4
	mul t1, t1, t2
	add t1, t0, t1
	lw t0, 0(t1)
	addw t0, t0, 1
	sw t0, 0(t1)
	mv s2, a0
	addw a0, a0, 1
	call go
	mv a0, s2
	ld t0, glo3
	sext.w t2, s1
	li t1, 8
	mul t1, t1, t2
	add t0, t0, t1
	ld t0, 0(t0)
	sext.w t2, a0
	li t1, 4
	mul t1, t1, t2
	add t1, t0, t1
	lw t0, 0(t1)
	li t2, 1
	subw t0, t0, t2
	sw t0, 0(t1)
.L19:
	addw s1, s1, 1
	j .L16
.L20:
	li a0, 0
	j .L22
.L21:
	lw t0, glo2
	addw t0, t0, 1
	sw t0, glo2, t6
	li a0, 0
.L22:
	ld s1, 0(sp)
	ld s2, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type go, @function
.size go, .-go
/* end function go */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	sd s1, 0(sp)
	li t0, 8
	sw t0, glo1, t6
	li a1, 8
	li a0, 8
	call calloc
	mv t0, a0
	sd t0, glo3, t6
	li t0, 0
	mv s1, t0
.L25:
	lw a0, glo1
	slt t0, s1, a0
	beqz t0, .L27
	li a1, 4
	call calloc
	mv t0, a0
	ld t1, glo3
	sext.w t3, s1
	li t2, 8
	mul t2, t2, t3
	add t1, t1, t2
	sd t0, 0(t1)
	addw s1, s1, 1
	j .L25
.L27:
	li a0, 0
	call go
	lw t0, glo2
	xor t0, t0, 92
	snez a0, t0
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.data
.balign 8
glo1:
	.int 0
/* end data */

.data
.balign 8
glo2:
	.int 0
/* end data */

.data
.balign 8
glo3:
	.quad 0
/* end data */

.section .note.GNU-stack,"",@progbits
