.text
.balign 16
.globl blit
blit:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	li t0, 287454020
	sw t0, -12(fp)
	add t0, fp, -12
	add t1, t0, 4
	li t0, 1432778632
	sw t0, 0(t1)
	add t0, fp, -12
	add t1, t0, 8
	li t0, -1716864052
	sw t0, 0(t1)
	add t0, fp, -12
	add t1, t0, 1
	add t0, fp, -12
	add t0, t0, 10
	lbu t0, 0(t0)
	add t2, t1, 10
	sb t0, 0(t2)
	add t0, fp, -12
	add t0, t0, 8
	lhu t0, 0(t0)
	add t2, t1, 8
	sh t0, 0(t2)
	add t0, fp, -12
	add t0, t0, 0
	ld t0, 0(t0)
	add t1, t1, 0
	sd t0, 0(t1)
	li t0, 221
	sb t0, -12(fp)
	add t0, fp, -12
	add t0, t0, 8
	ld a1, 0(t0)
	add t0, fp, -12
	add t0, t0, 0
	ld a0, 0(t0)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type blit, @function
.size blit, .-blit
/* end function blit */

.section .note.GNU-stack,"",@progbits
