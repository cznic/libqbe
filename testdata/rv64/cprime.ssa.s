.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t2, 5
	li t1, 11
	li t0, 12
	mv t3, t2
	mv t6, t1
	mv t1, t0
	mv t0, t6
.L2:
	xor t2, t3, 201
	snez t2, t2
	beqz t2, .L19
	li t2, 2
	remw t2, t1, t2
	xor t2, t2, 0
	seqz t4, t2
	li t2, 1
	beqz t4, .L5
	li t2, 0
.L5:
	li t4, 3
	mv t6, t0
	mv t0, t4
	mv t4, t6
	mv t6, t1
	mv t1, t0
	mv t0, t6
.L7:
	slt t5, t1, t0
	beqz t5, .L13
	remw t5, t0, t1
	xor t5, t5, 0
	seqz t5, t5
	bnez t5, .L10
	addw t1, t1, 2
	j .L7
.L10:
	mv t1, t0
	mv t0, t4
	mv t4, t0
	li t0, 0
	mv t2, t0
	j .L14
.L13:
	mv t1, t0
.L14:
	bnez t2, .L16
	mv t0, t4
	j .L18
.L16:
	addw t3, t3, 1
	mv t0, t1
.L18:
	addw t1, t1, 1
	j .L2
.L19:
	xor t0, t0, 1229
	snez t0, t0
	bnez t0, .L21
	li a0, 0
	j .L22
.L21:
	li a0, 1
.L22:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
