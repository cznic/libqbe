.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	add t0, fp, -4
	add t1, t0, 1
	li t0, 0
	sb t0, 0(t1)
	li t0, 0
	mv s1, t0
.L2:
	addw t0, s1, 48
	sb t0, -4(fp)
	add a0, fp, -4
	call puts
	addw s1, s1, 1
	li t0, 9
	slt t0, t0, s1
	xor t0, t0, 1
	bnez t0, .L2
	li a0, 0
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
