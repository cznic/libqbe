.data
.balign 8
ctoqbestr:
	.ascii "c->qbe(%d)"
	.byte 0
/* end data */

.data
.balign 8
emptystr:
	.byte 0
/* end data */

.text
.balign 16
.globl qfn0
qfn0:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	fsd fs0, 0(sp)
	fmv.w.x fs0, a0
	li a1, 0
	la a0, ctoqbestr
	call printf
	fmv.s fa0, fs0
	call ps
	la a0, emptystr
	call puts
	fld fs0, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn0, @function
.size qfn0, .-qfn0
/* end function qfn0 */

.text
.balign 16
.globl qfn1
qfn1:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	fsd fs0, 8(sp)
	fmv.s fs0, fa0
	add t0, fp, -8
	add t0, t0, 4
	fsw fa1, 0(t0)
	add t0, fp, -8
	add t0, t0, 0
	sw a1, 0(t0)
	li a1, 1
	mv s1, a0
	la a0, ctoqbestr
	call printf
	mv a0, s1
	call pw
	fmv.s fa0, fs0
	call ps
	add a0, fp, -8
	call pfi1
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	fld fs0, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn1, @function
.size qfn1, .-qfn1
/* end function qfn1 */

.text
.balign 16
.globl qfn2
qfn2:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	fsd fs0, 8(sp)
	fmv.s fs0, fa1
	add t0, fp, -8
	add t0, t0, 4
	sw a1, 0(t0)
	add t0, fp, -8
	add t0, t0, 0
	fsw fa0, 0(t0)
	li a1, 2
	mv s1, a0
	la a0, ctoqbestr
	call printf
	mv a0, s1
	call pw
	add a0, fp, -8
	call pfi2
	fmv.s fa0, fs0
	call ps
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	fld fs0, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn2, @function
.size qfn2, .-qfn2
/* end function qfn2 */

.text
.balign 16
.globl qfn3
qfn3:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	fsd fs0, 8(sp)
	fmv.s fs0, fa0
	add t0, fp, -8
	add t0, t0, 0
	sd a1, 0(t0)
	li a1, 3
	mv s1, a0
	la a0, ctoqbestr
	call printf
	mv a0, s1
	call pw
	fmv.s fa0, fs0
	call ps
	add a0, fp, -8
	call pfi3
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	fld fs0, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn3, @function
.size qfn3, .-qfn3
/* end function qfn3 */

.text
.balign 16
.globl qfn4
qfn4:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	add t0, fp, -8
	add t0, t0, 4
	fsw fa1, 0(t0)
	add t0, fp, -8
	add t0, t0, 0
	fsw fa0, 0(t0)
	li a1, 4
	la a0, ctoqbestr
	call printf
	add a0, fp, -8
	call pss
	la a0, emptystr
	call puts
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn4, @function
.size qfn4, .-qfn4
/* end function qfn4 */

.text
.balign 16
.globl qfn5
qfn5:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	fsd fs0, 8(sp)
	fmv.s fs0, fa7
	mv t0, a0
	mv a0, a1
	add t1, fp, -8
	add t1, t1, 0
	sd t0, 0(t1)
	li a1, 5
	mv s1, a0
	la a0, ctoqbestr
	call printf
	mv a0, s1
	mv s1, a0
	add a0, fp, -8
	call pss
	fmv.s fa0, fs0
	call ps
	mv a0, s1
	call pl
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	fld fs0, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn5, @function
.size qfn5, .-qfn5
/* end function qfn5 */

.text
.balign 16
.globl qfn6
qfn6:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	add t0, fp, -16
	add t0, t0, 8
	sd a1, 0(t0)
	add t0, fp, -16
	add t0, t0, 0
	sd a0, 0(t0)
	li a1, 6
	la a0, ctoqbestr
	call printf
	add a0, fp, -16
	call plb
	la a0, emptystr
	call puts
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn6, @function
.size qfn6, .-qfn6
/* end function qfn6 */

.text
.balign 16
.globl qfn7
qfn7:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	ld t1, 16(fp)
	mv t0, a7
	add t2, fp, -16
	add t2, t2, 8
	sd t1, 0(t2)
	add t1, fp, -16
	add t1, t1, 0
	sd t0, 0(t1)
	li a1, 7
	la a0, ctoqbestr
	call printf
	add a0, fp, -16
	call plb
	la a0, emptystr
	call puts
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn7, @function
.size qfn7, .-qfn7
/* end function qfn7 */

.text
.balign 16
.globl qfn8
qfn8:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a1, 8
	la a0, ctoqbestr
	call printf
	add a0, fp, 16
	call plb
	la a0, emptystr
	call puts
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn8, @function
.size qfn8, .-qfn8
/* end function qfn8 */

.text
.balign 16
.globl qfn9
qfn9:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	sd s1, 0(sp)
	li a1, 9
	mv s1, a0
	la a0, ctoqbestr
	call printf
	mv a0, s1
	call pbig
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn9, @function
.size qfn9, .-qfn9
/* end function qfn9 */

.text
.balign 16
.globl qfn10
qfn10:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	sd s2, 8(sp)
	fsd fs0, 16(sp)
	ld s1, 24(fp)
	fmv.s fs0, fa0
	ld s2, 16(fp)
	mv a0, s2
	li a1, 10
	mv s2, a0
	la a0, ctoqbestr
	call printf
	mv a0, s2
	call pbig
	fmv.s fa0, fs0
	call ps
	mv a0, s1
	call pl
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	ld s2, 8(sp)
	fld fs0, 16(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn10, @function
.size qfn10, .-qfn10
/* end function qfn10 */

.text
.balign 16
.globl qfn11
qfn11:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	sd s1, 0(sp)
	li a1, 11
	mv s1, a0
	la a0, ctoqbestr
	call printf
	mv a0, s1
	call pddd
	la a0, emptystr
	call puts
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qfn11, @function
.size qfn11, .-qfn11
/* end function qfn11 */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -96
	flw ft0, ".Lfp2", t6
	fmv.x.w a0, ft0
	flw fa7, ".Lfp1", t6
	flw fa6, ".Lfp1", t6
	flw fa5, ".Lfp1", t6
	flw fa4, ".Lfp1", t6
	flw fa3, ".Lfp1", t6
	flw fa2, ".Lfp1", t6
	flw fa1, ".Lfp1", t6
	flw fa0, ".Lfp1", t6
	call cfn0
	la t0, fi1
	add t0, t0, 4
	flw fa1, 0(t0)
	la t0, fi1
	add t0, t0, 0
	lw a1, 0(t0)
	flw fa0, ".Lfp3", t6
	li a0, 1
	call cfn1
	flw fa1, ".Lfp4", t6
	la t0, fi2
	add t0, t0, 4
	lw a1, 0(t0)
	la t0, fi2
	add t0, t0, 0
	flw fa0, 0(t0)
	li a0, 1
	call cfn2
	la t0, fi3
	add t0, t0, 0
	ld a1, 0(t0)
	flw fa0, ".Lfp3", t6
	li a0, 1
	call cfn3
	la t0, ss
	add t0, t0, 4
	flw fa1, 0(t0)
	la t0, ss
	add t0, t0, 0
	flw fa0, 0(t0)
	call cfn4
	li a1, 10
	flw fa7, ".Lfp2", t6
	la t0, ss
	add t0, t0, 0
	ld a0, 0(t0)
	fld fa6, ".Lfp1", t6
	fld fa5, ".Lfp1", t6
	fld fa4, ".Lfp1", t6
	fld fa3, ".Lfp1", t6
	fld fa2, ".Lfp1", t6
	fld fa1, ".Lfp1", t6
	fld fa0, ".Lfp1", t6
	call cfn5
	la t0, lb
	add t0, t0, 8
	ld a1, 0(t0)
	la t0, lb
	add t0, t0, 0
	ld a0, 0(t0)
	call cfn6
	li t0, 16
	sub sp, sp, t0
	mv t1, sp
	la t0, lb
	add t0, t0, 8
	ld t0, 0(t0)
	add t1, t1, 0
	sd t0, 0(t1)
	la t0, lb
	add t0, t0, 0
	ld a7, 0(t0)
	li a6, 0
	li a5, 0
	li a4, 0
	li a3, 0
	li a2, 0
	li a1, 0
	li a0, 0
	call cfn7
	li t0, -16
	sub sp, sp, t0
	li t0, 16
	sub sp, sp, t0
	mv t1, sp
	la t0, lb
	add t0, t0, 8
	ld t0, 0(t0)
	add t2, t1, 8
	sd t0, 0(t2)
	ld t0, lb
	add t1, t1, 0
	sd t0, 0(t1)
	li a7, 0
	li a6, 0
	li a5, 0
	li a4, 0
	li a3, 0
	li a2, 0
	li a1, 0
	li a0, 0
	call cfn8
	li t0, -16
	sub sp, sp, t0
	la t0, big
	add t0, t0, 0
	lbu t0, 0(t0)
	add t1, fp, -48
	add t1, t1, 0
	sb t0, 0(t1)
	la t0, big
	add t0, t0, 1
	ld t0, 0(t0)
	add t1, fp, -48
	add t1, t1, 1
	sd t0, 0(t1)
	la t0, big
	add t0, t0, 9
	ld t0, 0(t0)
	add t1, fp, -48
	add t1, t1, 9
	sd t0, 0(t1)
	add a0, fp, -48
	call cfn9
	li t0, 16
	sub sp, sp, t0
	mv t0, sp
	add t2, t0, 8
	li t1, 11
	sd t1, 0(t2)
	add t1, t0, 0
	add t0, fp, -72
	sd t0, 0(t1)
	la t0, big
	add t0, t0, 0
	lbu t0, 0(t0)
	add t1, fp, -72
	add t1, t1, 0
	sb t0, 0(t1)
	la t0, big
	add t0, t0, 1
	ld t0, 0(t0)
	add t1, fp, -72
	add t1, t1, 1
	sd t0, 0(t1)
	la t0, big
	add t0, t0, 9
	ld t0, 0(t0)
	add t1, fp, -72
	add t1, t1, 9
	sd t0, 0(t1)
	flw fa0, ".Lfp0", t6
	li a7, 0
	li a6, 0
	li a5, 0
	li a4, 0
	li a3, 0
	li a2, 0
	li a1, 0
	li a0, 0
	call cfn10
	li t0, -16
	sub sp, sp, t0
	la t0, ddd
	add t0, t0, 0
	ld t0, 0(t0)
	add t1, fp, -24
	add t1, t1, 0
	sd t0, 0(t1)
	la t0, ddd
	add t0, t0, 8
	ld t0, 0(t0)
	add t1, fp, -24
	add t1, t1, 8
	sd t0, 0(t1)
	la t0, ddd
	add t0, t0, 16
	ld t0, 0(t0)
	add t1, fp, -24
	add t1, t1, 16
	sd t0, 0(t1)
	add a0, fp, -24
	call cfn11
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 0 /* 0.000000 */

.section .rodata
.p2align 2
.Lfp0:
	.int 1092721050 /* 10.100000 */

.section .rodata
.p2align 2
.Lfp2:
	.int 1092511334 /* 9.900000 */

.section .rodata
.p2align 2
.Lfp3:
	.int 1074580685 /* 2.200000 */

.section .rodata
.p2align 2
.Lfp4:
	.int 1079194419 /* 3.300000 */

.section .note.GNU-stack,"",@progbits
