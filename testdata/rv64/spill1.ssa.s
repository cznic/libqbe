.text
.balign 16
.globl f
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	sd s1, 0(sp)
	sd s2, 8(sp)
	sd s3, 16(sp)
	sd s4, 24(sp)
	li s3, 0
	li s2, 0
	li s1, 0
	li a7, 0
	li a6, 0
	li a5, 0
	li a4, 0
	li a3, 0
	li a2, 0
	li a1, 0
	li t5, 0
	li t4, 0
	li t3, 0
	li t2, 0
	li t1, 0
	li t0, 0
.L1:
	li s4, 1
	addw t0, s4, t0
	li s4, 2
	addw t1, s4, t1
	li s4, 3
	addw t2, s4, t2
	li s4, 4
	addw t3, s4, t3
	li s4, 5
	addw t4, s4, t4
	li s4, 6
	addw t5, s4, t5
	li s4, 7
	addw a1, s4, a1
	li s4, 8
	addw a2, s4, a2
	li s4, 9
	addw a3, s4, a3
	li s4, 10
	addw a4, s4, a4
	li s4, 11
	addw a5, s4, a5
	li s4, 12
	addw a6, s4, a6
	li s4, 13
	addw a7, s4, a7
	li s4, 14
	addw s1, s4, s1
	li s4, 15
	addw s2, s4, s2
	li s4, 16
	addw s3, s4, s3
	li s4, 1
	subw a0, a0, s4
	bnez a0, .L1
	addw t0, t0, 0
	addw t0, t1, t0
	addw t0, t2, t0
	addw t0, t3, t0
	addw t0, t4, t0
	addw t0, t5, t0
	addw t0, a1, t0
	addw t0, a2, t0
	addw t0, a3, t0
	addw t0, a4, t0
	addw t0, a5, t0
	addw t0, a6, t0
	addw t0, a7, t0
	addw t0, s1, t0
	addw t0, s2, t0
	addw a0, s3, t0
	ld s1, 0(sp)
	ld s2, 8(sp)
	ld s3, 16(sp)
	ld s4, 24(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
