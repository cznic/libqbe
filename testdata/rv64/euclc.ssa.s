.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 747
	li t0, 380
.L1:
	beqz t0, .L4
	remw t1, t1, t0
	mv t6, t0
	mv t0, t1
	mv t1, t6
	j .L1
.L4:
	mv a0, t1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
