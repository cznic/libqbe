.text
.balign 16
mandel:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fld ft0, ".Lfp1", t6
	fsub.d ft2, fa1, ft0
	fld ft1, ".Lfp0", t6
	fld ft0, ".Lfp0", t6
	li t0, 0
.L1:
	fmv.d ft3, ft0
	li t1, 1
	addw t0, t1, t0
	fmv.d ft0, ft1
	fmul.d ft1, ft3, ft1
	fmul.d ft4, ft3, ft3
	fmul.d ft3, ft0, ft0
	fsub.d ft0, ft4, ft3
	fadd.d ft0, ft0, ft2
	fadd.d ft1, ft1, ft1
	fadd.d ft1, ft1, fa0
	fadd.d ft3, ft3, ft4
	fld ft4, ".Lfp2", t6
	fgt.d t1, ft3, ft4
	bnez t1, .L4
	li t1, 1000
	slt t1, t1, t0
	beqz t1, .L1
	li a0, 0
	j .L5
.L4:
	mv a0, t0
.L5:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type mandel, @function
.size mandel, .-mandel
/* end function mandel */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	fsd fs0, 0(sp)
	fsd fs1, 8(sp)
	fld ft0, ".Lfp3", t6
	fmv.d fs1, ft0
.L8:
	fld ft0, ".Lfp3", t6
	fmv.d fs0, ft0
.L10:
	fmv.d fa1, fs1
	fmv.d fa0, fs0
	call mandel
	mv t0, a0
	bnez t0, .L12
	li a0, 42
	call putchar
	j .L13
.L12:
	li a0, 32
	call putchar
.L13:
	fld ft0, ".Lfp5", t6
	fadd.d fs0, fs0, ft0
	fld ft0, ".Lfp4", t6
	fgt.d t0, fs0, ft0
	beqz t0, .L10
	li a0, 10
	call putchar
	fld ft0, ".Lfp5", t6
	fadd.d fs1, fs1, ft0
	fld ft0, ".Lfp4", t6
	fgt.d t0, fs1, ft0
	beqz t0, .L8
	li a0, 0
	fld fs0, 0(sp)
	fld fs1, 8(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 0
	.int 0 /* 0.000000 */

.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 1071644672 /* 0.500000 */

.section .rodata
.p2align 3
.Lfp2:
	.int 0
	.int 1076887552 /* 16.000000 */

.section .rodata
.p2align 3
.Lfp3:
	.int 0
	.int -1074790400 /* -1.000000 */

.section .rodata
.p2align 3
.Lfp4:
	.int 0
	.int 1072693248 /* 1.000000 */

.section .rodata
.p2align 3
.Lfp5:
	.int -755914244
	.int 1067475533 /* 0.032000 */

.section .note.GNU-stack,"",@progbits
