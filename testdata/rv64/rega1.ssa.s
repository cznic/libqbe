.text
.balign 16
.globl f
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 42
.L1:
	li t1, 1
	sub a0, a0, t1
	sext.w t1, t0
	bnez t1, .L4
	sext.w t1, a0
	beqz t1, .L6
	mv t0, a0
	j .L1
.L4:
	sext.w t1, a0
	beqz t1, .L6
	mv t0, a0
	j .L1
.L6:
	mv a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
