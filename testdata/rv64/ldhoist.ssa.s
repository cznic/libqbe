.text
.balign 16
.globl f
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 0
.L1:
	li t1, 1
	subw a0, a0, t1
	slt t1, a0, 0
	xor t1, t1, 1
	beqz t1, .L3
	lw t0, 0(a1)
	j .L1
.L3:
	mv a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
