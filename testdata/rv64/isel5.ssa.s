.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fld ft0, ".Lfp0", t6
	fmv.x.d a1, ft0
	la a0, fmt
	call printf
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.data
.balign 8
fmt:
	.ascii "%.06f\n"
	.byte 0
/* end data */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 858993459
	.int 1072902963 /* 1.200000 */

.section .note.GNU-stack,"",@progbits
