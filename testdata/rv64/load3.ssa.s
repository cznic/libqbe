.text
.balign 16
rand:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type rand, @function
.size rand, .-rand
/* end function rand */

.text
.balign 16
chk:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	xor t0, a0, 1
	seqz t0, t0
	xor t1, a1, 0
	seqz t1, t1
	and t0, t0, t1
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type chk, @function
.size chk, .-chk
/* end function chk */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	li t0, 1
	sw t0, -8(fp)
	add t1, fp, -8
	li t0, 4
	add t1, t0, t1
	li t0, 0
	sw t0, 0(t1)
	call rand
	mv t0, a0
	li a1, 0
	li a0, 1
	call chk
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
