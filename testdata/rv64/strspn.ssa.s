.text
.balign 16
.globl strspn_
strspn_:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	mv t0, a1
	li t1, 0
	mv t2, t1
.L2:
	mv t1, a0
	add a0, a0, 1
	lb t1, 0(t1)
	xor t3, t1, 0
	snez t3, t3
	beqz t3, .L10
	mv a1, t0
.L4:
	lb t3, 0(a1)
	xor t4, t3, 0
	snez t4, t4
	beqz t4, .L7
	xor t4, t3, t1
	snez t4, t4
	beqz t4, .L7
	add a1, a1, 1
	j .L4
.L7:
	xor t1, t3, 0
	snez t1, t1
	beqz t1, .L9
	addw t2, t2, 1
	j .L2
.L9:
	mv a0, t2
	j .L11
.L10:
	mv a0, t2
.L11:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type strspn_, @function
.size strspn_, .-strspn_
/* end function strspn_ */

.section .note.GNU-stack,"",@progbits
