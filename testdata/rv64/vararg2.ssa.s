.text
.balign 16
.globl qbeprint0
qbeprint0:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, a0
	add t0, fp, 24
	sd t0, -32(fp)
.L1:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L5
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L4
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L1
.L4:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L1
.L5:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint0, @function
.size qbeprint0, .-qbeprint0
/* end function qbeprint0 */

.text
.balign 16
.globl qbecall0
qbecall0:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	add t0, fp, 24
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall0, @function
.size qbecall0, .-qbecall0
/* end function qbecall0 */

.text
.balign 16
.globl qbeprint1
qbeprint1:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, a1
	add t0, fp, 32
	sd t0, -32(fp)
.L10:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L14
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L13
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L10
.L13:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L10
.L14:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint1, @function
.size qbeprint1, .-qbeprint1
/* end function qbeprint1 */

.text
.balign 16
.globl qbecall1
qbecall1:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	mv a0, a1
	add t0, fp, 32
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall1, @function
.size qbecall1, .-qbecall1
/* end function qbecall1 */

.text
.balign 16
.globl qbeprint2
qbeprint2:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, a0
	add t0, fp, 24
	sd t0, -32(fp)
.L19:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L23
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L22
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L19
.L22:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L19
.L23:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint2, @function
.size qbeprint2, .-qbeprint2
/* end function qbeprint2 */

.text
.balign 16
.globl qbecall2
qbecall2:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	add t0, fp, 24
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall2, @function
.size qbecall2, .-qbecall2
/* end function qbecall2 */

.text
.balign 16
.globl qbeprint3
qbeprint3:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, a4
	add t0, fp, 56
	sd t0, -32(fp)
.L28:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L32
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L31
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L28
.L31:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L28
.L32:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint3, @function
.size qbeprint3, .-qbeprint3
/* end function qbeprint3 */

.text
.balign 16
.globl qbecall3
qbecall3:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	mv a0, a4
	add t0, fp, 56
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall3, @function
.size qbecall3, .-qbecall3
/* end function qbecall3 */

.text
.balign 16
.globl qbeprint4
qbeprint4:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, a0
	add t0, fp, 24
	sd t0, -32(fp)
.L37:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L41
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L40
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L37
.L40:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L37
.L41:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint4, @function
.size qbeprint4, .-qbeprint4
/* end function qbeprint4 */

.text
.balign 16
.globl qbecall4
qbecall4:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	add t0, fp, 24
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall4, @function
.size qbecall4, .-qbecall4
/* end function qbecall4 */

.text
.balign 16
.globl qbeprint5
qbeprint5:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, a5
	add t0, fp, 64
	sd t0, -32(fp)
.L46:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L50
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L49
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L46
.L49:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L46
.L50:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint5, @function
.size qbeprint5, .-qbeprint5
/* end function qbeprint5 */

.text
.balign 16
.globl qbecall5
qbecall5:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	mv a0, a5
	add t0, fp, 64
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall5, @function
.size qbecall5, .-qbecall5
/* end function qbecall5 */

.text
.balign 16
.globl qbeprint6
qbeprint6:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	ld t1, 112(fp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, t1
	add t0, fp, 120
	sd t0, -32(fp)
.L55:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L59
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L58
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L55
.L58:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L55
.L59:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint6, @function
.size qbeprint6, .-qbeprint6
/* end function qbeprint6 */

.text
.balign 16
.globl qbecall6
qbecall6:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	ld t0, 112(fp)
	mv a0, t0
	add t0, fp, 120
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall6, @function
.size qbecall6, .-qbecall6
/* end function qbecall6 */

.text
.balign 16
.globl qbeprint7
qbeprint7:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -80
	sd s1, 0(sp)
	ld t1, 88(fp)
	li t0, 2122789
	sw t0, -40(fp)
	li t0, 2123557
	sw t0, -44(fp)
	li t0, 0
	sw t0, -36(fp)
	li t0, 1
	add s1, t0, t1
	add t0, fp, 96
	sd t0, -32(fp)
.L64:
	lb t0, 0(s1)
	li t1, 3
	add s1, t1, s1
	beqz t0, .L68
	xor t0, t0, 103
	seqz t0, t0
	bnez t0, .L67
	ld t0, -32(fp)
	lw a1, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add a0, fp, -40
	call printf
	j .L64
.L67:
	ld t0, -32(fp)
	fld ft0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	fmv.x.d a1, ft0
	add a0, fp, -44
	call printf
	j .L64
.L68:
	add a0, fp, -36
	call puts
	ld s1, 0(sp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbeprint7, @function
.size qbeprint7, .-qbeprint7
/* end function qbeprint7 */

.text
.balign 16
.globl qbecall7
qbecall7:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	ld t0, 88(fp)
	mv a0, t0
	add t0, fp, 96
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type qbecall7, @function
.size qbecall7, .-qbecall7
/* end function qbecall7 */

.section .note.GNU-stack,"",@progbits
