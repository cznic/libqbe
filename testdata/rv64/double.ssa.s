.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 0
	fld ft0, ".Lfp0", t6
.L1:
	fadd.d ft0, ft0, ft0
	addw t0, t0, 1
	fld ft1, ".Lfp1", t6
	fle.d t1, ft0, ft1
	bnez t1, .L1
	sw t0, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 0
	.int 1016070144 /* 0.000000 */

.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 1072693248 /* 1.000000 */

.section .note.GNU-stack,"",@progbits
