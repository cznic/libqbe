.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	li t0, 4
	sw t0, -8(fp)
	li t0, 5
	sw t0, -4(fp)
	add t0, fp, -8
.L1:
	lw t0, 0(t0)
	xor t0, t0, 5
	seqz t1, t0
	add t0, fp, -4
	beqz t1, .L1
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
