.text
.balign 16
.globl f
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	li t0, 0
	sw t0, -8(fp)
	bnez a0, .L2
	add t0, fp, -4
	j .L3
.L2:
	add t0, fp, -8
.L3:
	mv t1, t0
	li t0, 1
	sw t0, 0(t1)
	lw a0, -8(fp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
