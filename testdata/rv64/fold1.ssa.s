.text
.balign 16
.globl f1
f1:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, -1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, -32
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, -5
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.text
.balign 16
.globl f5
f5:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f5, @function
.size f5, .-f5
/* end function f5 */

.text
.balign 16
.globl f6
f6:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f6, @function
.size f6, .-f6
/* end function f6 */

.section .note.GNU-stack,"",@progbits
