.text
.balign 16
.globl f
f:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	add t0, fp, 24
	sd t0, -32(fp)
	ld t0, -32(fp)
	add t0, t0, 8
	sd t0, -32(fp)
	ld t0, -32(fp)
	fld fa0, 0(t0)
	add t0, t0, 8
	sd t0, -32(fp)
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.text
.balign 16
.globl g
g:
	add sp, sp, -64
	sd a0, 0(sp)
	sd a1, 8(sp)
	sd a2, 16(sp)
	sd a3, 24(sp)
	sd a4, 32(sp)
	sd a5, 40(sp)
	sd a6, 48(sp)
	sd a7, 56(sp)
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	add t0, fp, 24
	sd t0, -32(fp)
	add a1, fp, -32
	call print
	add sp, fp, 80
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type g, @function
.size g, .-g
/* end function g */

.section .note.GNU-stack,"",@progbits
