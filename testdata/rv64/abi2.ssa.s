.text
.balign 16
.globl sum
sum:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	add t0, fp, -16
	add t0, t0, 8
	sd a1, 0(t0)
	add t0, fp, -16
	add t0, t0, 0
	sd a0, 0(t0)
	flw ft0, -16(fp)
	add t1, fp, -16
	li t0, 8
	add t0, t0, t1
	flw ft1, 0(t0)
	fadd.s fa0, ft0, ft1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type sum, @function
.size sum, .-sum
/* end function sum */

.section .note.GNU-stack,"",@progbits
