.text
.balign 16
epar:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	add a0, t5, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type epar, @function
.size epar, .-epar
/* end function epar */

.text
.balign 16
.globl earg
earg:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	mv t5, a0
	mv a0, a1
	call epar
	li t5, 113
	call labs
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type earg, @function
.size earg, .-earg
/* end function earg */

.section .note.GNU-stack,"",@progbits
