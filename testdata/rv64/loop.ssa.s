.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 100
	li t0, 0
	mv t6, t1
	mv t1, t0
	mv t0, t6
.L2:
	addw t1, t1, t0
	li t2, 1
	subw t0, t0, t2
	bnez t0, .L2
	sw t1, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
