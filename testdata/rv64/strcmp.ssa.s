.text
.balign 16
.globl strcmp
strcmp:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
.L1:
	lb t0, 0(a0)
	xor t1, t0, 0
	snez t1, t1
	beqz t1, .L5
	lb t1, 0(a1)
	xor t2, t1, 0
	snez t2, t2
	beqz t2, .L5
	xor t1, t0, t1
	seqz t1, t1
	beqz t1, .L5
	add a0, a0, 1
	add a1, a1, 1
	j .L1
.L5:
	zext.b t0, t0
	lbu t1, 0(a1)
	subw a0, t0, t1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type strcmp, @function
.size strcmp, .-strcmp
/* end function strcmp */

.section .note.GNU-stack,"",@progbits
