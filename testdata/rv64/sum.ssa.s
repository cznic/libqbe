.text
.balign 16
.globl sum
sum:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 0
.L1:
	li t2, 1
	mv t1, a1
	subw a1, a1, t2
	li t2, 0
	slt t1, t2, t1
	xor t1, t1, 1
	bnez t1, .L3
	sext.w t2, a1
	li t1, 4
	mul t1, t1, t2
	add t1, t1, a0
	lw t1, 0(t1)
	addw t0, t1, t0
	j .L1
.L3:
	mv a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type sum, @function
.size sum, .-sum
/* end function sum */

.section .note.GNU-stack,"",@progbits
