.text
.balign 16
.globl f
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t0, 8
	mul t1, a0, t0
	la t0, a
	add t0, t0, t1
	li t1, 4
	mul t1, a1, t1
	add t0, t0, t1
	lw a0, 0(t0)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
