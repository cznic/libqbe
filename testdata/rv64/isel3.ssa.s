.text
.balign 16
.globl slt
slt:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	slt a0, a0, a1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type slt, @function
.size slt, .-slt
/* end function slt */

.text
.balign 16
.globl sle
sle:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	slt t0, a1, a0
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type sle, @function
.size sle, .-sle
/* end function sle */

.text
.balign 16
.globl sgt
sgt:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	slt a0, a1, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type sgt, @function
.size sgt, .-sgt
/* end function sgt */

.text
.balign 16
.globl sge
sge:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	slt t0, a0, a1
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type sge, @function
.size sge, .-sge
/* end function sge */

.text
.balign 16
.globl ult
ult:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	sltu a0, a0, a1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ult, @function
.size ult, .-ult
/* end function ult */

.text
.balign 16
.globl ule
ule:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	sltu t0, a1, a0
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ule, @function
.size ule, .-ule
/* end function ule */

.text
.balign 16
.globl ugt
ugt:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	sltu a0, a1, a0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ugt, @function
.size ugt, .-ugt
/* end function ugt */

.text
.balign 16
.globl uge
uge:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	sltu t0, a0, a1
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type uge, @function
.size uge, .-uge
/* end function uge */

.text
.balign 16
.globl eq
eq:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	xor t0, a0, a1
	seqz a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type eq, @function
.size eq, .-eq
/* end function eq */

.text
.balign 16
.globl ne
ne:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	xor t0, a0, a1
	snez a0, t0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ne, @function
.size ne, .-ne
/* end function ne */

.section .note.GNU-stack,"",@progbits
