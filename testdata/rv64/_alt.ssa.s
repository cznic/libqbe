.text
.balign 16
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 100
	li t0, 0
	mv t2, t0
	mv t0, t1
.L2:
	li t1, 1
	subw t2, t1, t2
	li t1, 10
	bnez t2, .L4
.L3:
	mv t3, t1
	li t1, 1
	subw t1, t3, t1
	beqz t3, .L2
	j .L3
.L4:
	li t1, 10
	subw t1, t0, t1
	beqz t1, .L6
	mv t0, t1
	j .L2
.L6:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
