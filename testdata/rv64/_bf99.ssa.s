.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	li t6, 4128
	sub sp, sp, t6
	sd s1, 0(sp)
	li a2, 4096
	li a1, 0
	li a0, -4096
	add a0, fp, a0
	call memset
	li t0, -4096
	add t0, fp, t0
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 7
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L1:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L3
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 5
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L1
.L3:
	add t1, t1, 7
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L4:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L6
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 5
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L4
.L6:
	add t1, t1, 10
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 9
	sd t1, 0(t0)
	add t0, t0, 16
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L7:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L9
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L7
.L9:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 14
	sd t1, 0(t0)
.L10:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L12
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 7
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L10
.L12:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L13:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L15
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L13
.L15:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L16:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L18
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L16
.L18:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L19:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L21
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L19
.L21:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L22:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L24
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 9
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L22
.L24:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L25:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L27
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L25
.L27:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L28:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L30
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L28
.L30:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L31:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L33
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L31
.L33:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L34:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L36
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L34
.L36:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L37:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L39
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L37
.L39:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L40:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L42
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L40
.L42:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 14
	sd t1, 0(t0)
.L43:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L45
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 7
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L43
.L45:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L46:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L48
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L46
.L48:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L49:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L51
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L49
.L51:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L52:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L54
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L52
.L54:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L55:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L57
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L55
.L57:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L58:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L60
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L58
.L60:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L61:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L63
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L61
.L63:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L64:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L66
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L64
.L66:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L67:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L69
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L67
.L69:
	add t1, t1, 4
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L70:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L72
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L70
.L72:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L73:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L75
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L73
.L75:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L76:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L78
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L76
.L78:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 13
	sd t1, 0(t0)
.L79:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L81
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 9
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L79
.L81:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L82:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L84
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L82
.L84:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L85:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L87
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 9
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L85
.L87:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L88:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L90
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 9
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L88
.L90:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 5
	sd t1, 0(t0)
.L91:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L93
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 2
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L91
.L93:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L94:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L96
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L94
.L96:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L97:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L99
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L97
.L99:
	add t1, t1, 3
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 13
	sd t1, 0(t0)
.L100:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L102
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L100
.L102:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L103:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L105
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L103
.L105:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L106:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L108
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L106
.L108:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L109:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L111
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L109
.L111:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L112:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L114
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L112
.L114:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L115:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L117
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L115
.L117:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L118:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L120
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L118
.L120:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L121:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L123
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L121
.L123:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L124:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L126
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L124
.L126:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 13
	sd t1, 0(t0)
.L127:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L129
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 9
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L127
.L129:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L130:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L132
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L130
.L132:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L133:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L135
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L133
.L135:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L136:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L138
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L136
.L138:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L139:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L141
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L139
.L141:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L142:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L144
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L142
.L144:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L145:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L147
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L145
.L147:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L148:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L150
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L148
.L150:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L151:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L153
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L151
.L153:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L154:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L156
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L154
.L156:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L157:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L159
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L157
.L159:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L160:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L162
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L160
.L162:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 13
	sd t1, 0(t0)
.L163:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L165
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L163
.L165:
	add t1, t1, 2
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L166:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L168
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L166
.L168:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 8
	sd t1, 0(t0)
.L169:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L171
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 4
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L169
.L171:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 12
	sd t1, 0(t0)
.L172:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L174
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 8
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L172
.L174:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 19
	sd t1, 0(t0)
.L175:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L177
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 6
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L175
.L177:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L178:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L180
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L178
.L180:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 13
	sd t1, 0(t0)
.L181:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L183
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 9
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L181
.L183:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 11
	sd t1, 0(t0)
.L184:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L186
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L184
.L186:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
.L187:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L189
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 10
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L187
.L189:
	add t0, t0, 8
	ld t1, 0(t0)
	add t1, t1, 5
	sd t1, 0(t0)
.L190:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L192
	add t0, t0, -8
	ld t2, 0(t0)
	add t2, t2, 2
	sd t2, 0(t0)
	add t0, t0, 8
	add t1, t1, -1
	sd t1, 0(t0)
	j .L190
.L192:
	add t1, t1, 13
	sd t1, 0(t0)
.L193:
	ld t1, 0(t0)
	sext.w t1, t1
	beqz t1, .L195
	add t0, t0, -8
	j .L193
.L195:
	add t0, t0, 32
.L196:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L224
	add t0, t0, -8
.L198:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L214
.L199:
	ld t1, 0(t0)
	sext.w t1, t1
	beqz t1, .L201
	add t0, t0, 8
	j .L199
.L201:
	add s1, t0, -16
	ld a0, 0(s1)
	call putchar
	ld a0, 0(s1)
	call putchar
.L202:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L204
	add s1, s1, -8
	j .L202
.L204:
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L205:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L207
	add s1, s1, -8
	j .L205
.L207:
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 104
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L208:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L210
	add s1, s1, -8
	j .L208
.L210:
	add t0, s1, 16
	ld t1, 0(t0)
	add t1, t1, -1
	sd t1, 0(t0)
	add s1, t0, -8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L211:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L213
	add s1, s1, -8
	j .L211
.L213:
	add t0, s1, 24
	ld t1, 0(t0)
	add t1, t1, -1
	sd t1, 0(t0)
	j .L198
.L214:
	add t1, t1, 10
	sd t1, 0(t0)
	add t0, t0, -8
	ld t1, 0(t0)
	add t1, t1, 10
	sd t1, 0(t0)
	add t0, t0, -8
	ld t1, 0(t0)
	add t1, t1, -1
	sd t1, 0(t0)
.L215:
	ld t1, 0(t0)
	sext.w t1, t1
	beqz t1, .L217
	add t0, t0, 8
	j .L215
.L217:
	add s1, t0, -8
	ld a0, 0(s1)
	call putchar
.L218:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L220
	add s1, s1, -8
	j .L218
.L220:
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L221:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L223
	add s1, s1, -8
	j .L221
.L223:
	add t0, s1, 32
	ld t1, 0(t0)
	add t1, t1, -1
	sd t1, 0(t0)
	j .L196
.L224:
	add t1, t1, 1
	sd t1, 0(t0)
	add t0, t0, -8
	ld t1, 0(t0)
	add t1, t1, -2
	sd t1, 0(t0)
.L225:
	ld t1, 0(t0)
	sext.w t2, t1
	beqz t2, .L241
.L226:
	ld t1, 0(t0)
	sext.w t1, t1
	beqz t1, .L228
	add t0, t0, 8
	j .L226
.L228:
	add s1, t0, -16
	ld a0, 0(s1)
	call putchar
	ld a0, 0(s1)
	call putchar
.L229:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L231
	add s1, s1, -8
	j .L229
.L231:
	add t0, s1, 8
	add s1, t0, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L232:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L234
	add s1, s1, -8
	j .L232
.L234:
	add t0, s1, 8
	add s1, t0, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 104
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L235:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L237
	add s1, s1, -8
	j .L235
.L237:
	add s1, s1, 16
	ld t0, 0(s1)
	add a0, t0, -1
	sd a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L238:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L240
	add s1, s1, -8
	j .L238
.L240:
	add t0, s1, 24
	ld t1, 0(t0)
	add t1, t1, -1
	sd t1, 0(t0)
	j .L225
.L241:
	add t1, t1, 1
	sd t1, 0(t0)
.L242:
	ld t1, 0(t0)
	sext.w t1, t1
	beqz t1, .L244
	add t0, t0, 8
	j .L242
.L244:
	add s1, t0, -8
	ld a0, 0(s1)
	call putchar
.L245:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L247
	add s1, s1, -8
	j .L245
.L247:
	add t0, s1, 8
	add s1, t0, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 16
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, -32
	ld a0, 0(s1)
	call putchar
.L248:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L250
	add s1, s1, 8
	j .L248
.L250:
	add s1, s1, -16
	ld a0, 0(s1)
	call putchar
	ld a0, 0(s1)
	call putchar
.L251:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L253
	add s1, s1, -8
	j .L251
.L253:
	add t0, s1, 8
	add s1, t0, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 16
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L254:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L256
	add s1, s1, -8
	j .L254
.L256:
	add t0, s1, 8
	add s1, t0, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 24
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 16
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 104
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
.L257:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L259
	add s1, s1, 8
	j .L257
.L259:
	add t0, s1, -8
	add s1, t0, -24
	ld a0, 0(s1)
	call putchar
	add s1, s1, -16
	ld a0, 0(s1)
	call putchar
	add s1, s1, -24
	ld a0, 0(s1)
	call putchar
.L260:
	ld t0, 0(s1)
	sext.w t0, t0
	beqz t0, .L262
	add s1, s1, -8
	j .L260
.L262:
	add t0, s1, 32
	add s1, t0, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add s1, s1, 8
	ld a0, 0(s1)
	call putchar
	add t0, s1, 8
	ld a0, 0(t0)
	call putchar
	li a0, 0
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
