.text
.balign 16
alpha:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	add t0, a0, a2
.L1:
	sb a1, 0(a0)
	mv t1, a0
	add a0, a0, 1
	addw a1, a1, 1
	xor t1, t1, t0
	seqz t1, t1
	beqz t1, .L1
	li t1, 0
	sb t1, 0(t0)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type alpha, @function
.size alpha, .-alpha
/* end function alpha */

.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -64
	sd s1, 0(sp)
	li a2, 16
	li a1, 65
	mv s1, a0
	add a0, fp, -20
	call alpha
	mv a0, s1
	add t0, fp, -20
	add t0, t0, 0
	lbu t0, 0(t0)
	add t1, a0, 0
	sb t0, 0(t1)
	add t0, fp, -20
	add t0, t0, 1
	ld t0, 0(t0)
	add t1, a0, 1
	sd t0, 0(t1)
	add t0, fp, -20
	add t0, t0, 9
	ld t0, 0(t0)
	add t1, a0, 9
	sd t0, 0(t1)
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
