.text
.balign 16
func:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	li t0, 1
	sd t0, -24(fp)
	add t0, fp, -24
	add t1, t0, 8
	li t0, 2
	sd t0, 0(t1)
	add t0, fp, -24
	add t1, t0, 8
	add t0, fp, -24
	add t0, t0, 8
	ld t0, 0(t0)
	add t2, t1, 8
	sd t0, 0(t2)
	add t0, fp, -24
	add t0, t0, 0
	ld t0, 0(t0)
	add t1, t1, 0
	sd t0, 0(t1)
	add t0, fp, -24
	add t0, t0, 0
	ld t0, 0(t0)
	add t1, a0, 0
	sd t0, 0(t1)
	add t0, fp, -24
	add t0, t0, 8
	ld t0, 0(t0)
	add t1, a0, 8
	sd t0, 0(t1)
	add t0, fp, -24
	add t0, t0, 16
	ld t0, 0(t0)
	add t1, a0, 16
	sd t0, 0(t1)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type func, @function
.size func, .-func
/* end function func */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -48
	add a0, fp, -24
	call func
	add t0, fp, -24
	add t0, t0, 16
	ld t0, 0(t0)
	xor t0, t0, 2
	seqz t0, t0
	bnez t0, .L6
	call abort
.L6:
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
