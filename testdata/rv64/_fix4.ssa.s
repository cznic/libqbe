.text
.balign 16
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	li t1, 3
	li t0, 2
	mv t6, t1
	mv t1, t0
	mv t0, t6
.L2:
	li t2, 10000
	xor t2, t1, t2
	seqz t2, t2
	bnez t2, .L8
	addw t0, t0, 2
	li t2, 3
.L4:
	mulw t3, t2, t2
	slt t3, t0, t3
	bnez t3, .L7
	remw t3, t0, t2
	beqz t3, .L2
	addw t2, t2, 2
	j .L4
.L7:
	addw t1, t1, 1
	j .L2
.L8:
	sw t0, a, t6
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
