.data
.balign 8
ret:
	.quad 0
/* end data */

.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	lw t1, a
	li t0, 1
	addw t0, t0, t1
	sw t0, a, t6
	ld t0, ret
	add t1, a0, -8
	ld t1, 0(t1)
	sd t1, ret, t6
	xor t0, t0, t1
	seqz t0, t0
	bnez t0, .L2
	call test
.L2:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
