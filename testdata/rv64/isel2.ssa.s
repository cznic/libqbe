.text
.balign 16
.globl lt
lt:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	flt.d a0, fa0, fa1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type lt, @function
.size lt, .-lt
/* end function lt */

.text
.balign 16
.globl le
le:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fle.d a0, fa0, fa1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type le, @function
.size le, .-le
/* end function le */

.text
.balign 16
.globl gt
gt:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fgt.d a0, fa0, fa1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type gt, @function
.size gt, .-gt
/* end function gt */

.text
.balign 16
.globl ge
ge:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	fge.d a0, fa0, fa1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ge, @function
.size ge, .-ge
/* end function ge */

.text
.balign 16
.globl eq1
eq1:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d a0, fa0, fa1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type eq1, @function
.size eq1, .-eq1
/* end function eq1 */

.text
.balign 16
.globl eq2
eq2:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d t0, fa0, fa1
	bnez t0, .L12
	li a0, 0
	j .L13
.L12:
	li a0, 1
.L13:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type eq2, @function
.size eq2, .-eq2
/* end function eq2 */

.text
.balign 16
.globl eq3
eq3:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d a0, fa0, fa1
	bnez a0, .L16
	li a0, 0
.L16:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type eq3, @function
.size eq3, .-eq3
/* end function eq3 */

.text
.balign 16
.globl ne1
ne1:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d t0, fa0, fa1
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ne1, @function
.size ne1, .-ne1
/* end function ne1 */

.text
.balign 16
.globl ne2
ne2:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d t0, fa0, fa1
	xor t0, t0, 1
	bnez t0, .L21
	li a0, 0
	j .L22
.L21:
	li a0, 1
.L22:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ne2, @function
.size ne2, .-ne2
/* end function ne2 */

.text
.balign 16
.globl ne3
ne3:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d t0, fa0, fa1
	xor a0, t0, 1
	bnez a0, .L25
	li a0, 0
.L25:
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type ne3, @function
.size ne3, .-ne3
/* end function ne3 */

.text
.balign 16
.globl o
o:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d t1, fa1, fa1
	feq.d t0, fa0, fa0
	and a0, t0, t1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type o, @function
.size o, .-o
/* end function o */

.text
.balign 16
.globl uo
uo:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	feq.d t1, fa1, fa1
	feq.d t0, fa0, fa0
	and t0, t0, t1
	xor a0, t0, 1
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type uo, @function
.size uo, .-uo
/* end function uo */

.section .note.GNU-stack,"",@progbits
