.text
.balign 16
g:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -16
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type g, @function
.size g, .-g
/* end function g */

.text
.balign 16
f:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	sd s1, 0(sp)
	mv s1, a0
	call g
	mv a0, s1
	li t0, 16
	sub sp, sp, t0
	mv t1, sp
	li t0, 180388626474
	sd t0, 0(t1)
	li t0, 8
	add t1, t0, t1
	li t0, 180388626474
	sd t0, 0(t1)
	add sp, fp, -16
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type f, @function
.size f, .-f
/* end function f */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	sd s1, 0(sp)
	li a0, 0
	call f
	mv s1, a0
	li a0, 0
	call f
	mv a0, s1
	ld s1, 0(sp)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
