.data
.balign 8
fmt1:
	.ascii "t1: %s\n"
	.byte 0
/* end data */

.data
.balign 8
fmt2:
	.ascii "t2: %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt3:
	.ascii "t3: %f %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt4:
	.ascii "t4: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmt5:
	.ascii "t5: %f %lld\n"
	.byte 0
/* end data */

.data
.balign 8
fmt6:
	.ascii "t6: %s\n"
	.byte 0
/* end data */

.data
.balign 8
fmt7:
	.ascii "t7: %f %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmt8:
	.ascii "t8: %d %d %d %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt9:
	.ascii "t9: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmta:
	.ascii "ta: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmtb:
	.ascii "tb: %d %d %f\n"
	.byte 0
/* end data */

.text
.balign 16
.globl test
test:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -160
	add a0, fp, -72
	call t1
	add a1, fp, -72
	la a0, fmt1
	call printf
	call t2
	mv t0, a0
	add t1, fp, -80
	add t1, t1, 0
	sd t0, 0(t1)
	lw a1, -80(fp)
	la a0, fmt2
	call printf
	call t3
	mv t0, a0
	fmv.s ft0, fa0
	add t1, fp, -88
	add t1, t1, 4
	sw t0, 0(t1)
	add t0, fp, -88
	add t0, t0, 0
	fsw ft0, 0(t0)
	flw ft0, -88(fp)
	add t0, fp, -88
	add t0, t0, 4
	lw a2, 0(t0)
	fcvt.d.s ft0, ft0
	fmv.x.d a1, ft0
	la a0, fmt3
	call printf
	call t4
	fmv.d ft0, fa0
	mv t0, a0
	add t1, fp, -16
	add t1, t1, 8
	fsd ft0, 0(t1)
	add t1, fp, -16
	add t1, t1, 0
	sw t0, 0(t1)
	lw a1, -16(fp)
	add t1, fp, -16
	li t0, 8
	add t0, t0, t1
	fld ft0, 0(t0)
	fmv.x.d a2, ft0
	la a0, fmt4
	call printf
	call t5
	mv t0, a0
	fmv.s ft0, fa0
	add t1, fp, -32
	add t1, t1, 8
	sd t0, 0(t1)
	add t0, fp, -32
	add t0, t0, 0
	fsw ft0, 0(t0)
	flw ft0, -32(fp)
	fcvt.d.s ft0, ft0
	add t0, fp, -32
	add t0, t0, 8
	ld a2, 0(t0)
	fmv.x.d a1, ft0
	la a0, fmt5
	call printf
	call t6
	mv t1, a1
	mv t0, a0
	add t2, fp, -104
	add t2, t2, 8
	sd t1, 0(t2)
	add t1, fp, -104
	add t1, t1, 0
	sd t0, 0(t1)
	add a1, fp, -104
	la a0, fmt6
	call printf
	call t7
	fmv.d ft1, fa1
	fmv.s ft0, fa0
	add t0, fp, -48
	add t0, t0, 8
	fsd ft1, 0(t0)
	add t0, fp, -48
	add t0, t0, 0
	fsw ft0, 0(t0)
	flw ft0, -48(fp)
	fcvt.d.s ft0, ft0
	add t0, fp, -48
	add t0, t0, 8
	fld ft1, 0(t0)
	fmv.x.d a2, ft1
	fmv.x.d a1, ft0
	la a0, fmt7
	call printf
	call t8
	mv t1, a1
	mv t0, a0
	add t2, fp, -120
	add t2, t2, 8
	sd t1, 0(t2)
	add t1, fp, -120
	add t1, t1, 0
	sd t0, 0(t1)
	add t1, fp, -120
	li t0, 4
	add t2, t0, t1
	li t0, 4
	add t1, t0, t2
	li t0, 4
	add t0, t0, t1
	lw a1, -120(fp)
	lw a2, 0(t2)
	lw a3, 0(t1)
	lw a4, 0(t0)
	la a0, fmt8
	call printf
	call t9
	mv t0, a0
	add t1, fp, -128
	add t1, t1, 0
	sd t0, 0(t1)
	add t1, fp, -128
	li t0, 4
	add t0, t0, t1
	lw a1, -128(fp)
	flw ft0, 0(t0)
	fcvt.d.s ft0, ft0
	fmv.x.d a2, ft0
	la a0, fmt9
	call printf
	call ta
	fmv.s ft0, fa0
	mv t0, a0
	add t1, fp, -136
	add t1, t1, 4
	fsw ft0, 0(t1)
	add t1, fp, -136
	add t1, t1, 0
	sw t0, 0(t1)
	add t1, fp, -136
	li t0, 4
	add t0, t0, t1
	lb a1, -136(fp)
	flw ft0, 0(t0)
	fcvt.d.s ft0, ft0
	fmv.x.d a2, ft0
	la a0, fmta
	call printf
	call tb
	mv t0, a0
	add t1, fp, -144
	add t1, t1, 0
	sd t0, 0(t1)
	add t1, fp, -144
	li t0, 1
	add t1, t0, t1
	add t2, fp, -144
	li t0, 4
	add t0, t0, t2
	lb a1, -144(fp)
	lb a2, 0(t1)
	flw ft0, 0(t0)
	fcvt.d.s ft0, ft0
	fmv.x.d a3, ft0
	la a0, fmtb
	call printf
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
