.text
.balign 16
func:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	li t0, 1
	sw t0, -8(fp)
	add t0, fp, -8
	add t1, t0, 4
	add t0, fp, -8
	add t0, t0, 0
	lw t0, 0(t0)
	add t1, t1, 0
	sw t0, 0(t1)
	li t0, 2
	sw t0, -8(fp)
	add t0, fp, -8
	add t0, t0, 0
	ld a0, 0(t0)
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type func, @function
.size func, .-func
/* end function func */

.text
.balign 16
.globl main
main:
	sd fp, -16(sp)
	sd ra, -8(sp)
	add fp, sp, -16
	add sp, sp, -32
	call func
	mv t0, a0
	add t1, fp, -8
	add t1, t1, 0
	sd t0, 0(t1)
	add t0, fp, -8
	add t0, t0, 4
	lw t0, 0(t0)
	xor t0, t0, 1
	seqz t0, t0
	bnez t0, .L4
	call abort
.L4:
	li a0, 0
	add sp, fp, 16
	ld ra, 8(fp)
	ld fp, 0(fp)
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
