.text
.balign 16
.globl fneg
fneg:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fmov	w1, s0
	mov	w0, #-2147483648
	eor	w0, w0, w1
	fmov	s0, w0
	ldp	x29, x30, [sp], 16
	ret
.type fneg, @function
.size fneg, .-fneg
/* end function fneg */

.text
.balign 16
.globl ftrunc
ftrunc:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzs	w0, d0
	scvtf	d0, w0
	ldp	x29, x30, [sp], 16
	ret
.type ftrunc, @function
.size ftrunc, .-ftrunc
/* end function ftrunc */

.text
.balign 16
.globl wtos
wtos:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	s0, w0
	ldp	x29, x30, [sp], 16
	ret
.type wtos, @function
.size wtos, .-wtos
/* end function wtos */

.text
.balign 16
.globl wtod
wtod:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	d0, w0
	ldp	x29, x30, [sp], 16
	ret
.type wtod, @function
.size wtod, .-wtod
/* end function wtod */

.text
.balign 16
.globl ltos
ltos:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	s0, x0
	ldp	x29, x30, [sp], 16
	ret
.type ltos, @function
.size ltos, .-ltos
/* end function ltos */

.text
.balign 16
.globl ltod
ltod:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	d0, x0
	ldp	x29, x30, [sp], 16
	ret
.type ltod, @function
.size ltod, .-ltod
/* end function ltod */

.text
.balign 16
.globl stow
stow:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	w0, s0
	ldp	x29, x30, [sp], 16
	ret
.type stow, @function
.size stow, .-stow
/* end function stow */

.text
.balign 16
.globl dtow
dtow:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	w0, d0
	ldp	x29, x30, [sp], 16
	ret
.type dtow, @function
.size dtow, .-dtow
/* end function dtow */

.text
.balign 16
.globl stol
stol:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	x0, s0
	ldp	x29, x30, [sp], 16
	ret
.type stol, @function
.size stol, .-stol
/* end function stol */

.text
.balign 16
.globl dtol
dtol:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	x0, d0
	ldp	x29, x30, [sp], 16
	ret
.type dtol, @function
.size dtol, .-dtol
/* end function dtol */

.section .note.GNU-stack,"",@progbits
