.text
.balign 16
.globl main
main:
	hint	#34
	mov	x16, #4112
	sub	sp, sp, x16
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	str	x19, [x29, 4120]
	mov	x2, #4096
	mov	x1, #0
	add	x0, x29, #16
	bl	memset
	add	x0, x29, #16
	ldr	x0, [x0]
	mov	x1, #13
	add	x0, x0, x1
	add	x1, x29, #16
	str	x0, [x1]
	add	x0, x29, #16
.L1:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L3
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	b	.L1
.L3:
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #6
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-3
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L4:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L11
.L5:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L7
	mov	x1, #72
	add	x0, x0, x1
	b	.L5
.L7:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L8:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L10
	mov	x1, #-72
	add	x0, x0, x1
	b	.L8
.L10:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L4
.L11:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L12:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L17
	mov	x1, #64
	add	x0, x0, x1
.L14:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L16
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L14
.L16:
	mov	x1, #8
	add	x0, x0, x1
	b	.L12
.L17:
	mov	x1, #-72
	add	x0, x0, x1
.L18:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L20
	mov	x1, #-72
	add	x0, x0, x1
	b	.L18
.L20:
	mov	x1, #64
	add	x0, x0, x1
.L21:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L23
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L21
.L23:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
.L24:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L29
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L26:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L28
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L26
.L28:
	mov	x1, #72
	add	x0, x0, x1
	b	.L24
.L29:
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #208
	add	x0, x0, x1
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-136
	add	x0, x0, x1
.L30:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L32
	mov	x1, #-72
	add	x0, x0, x1
	b	.L30
.L32:
	mov	x1, #24
	add	x0, x0, x1
.L33:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L35
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L33
.L35:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L36:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2016
	mov	x1, #48
	add	x0, x0, x1
.L38:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L43
	mov	x1, #56
	add	x0, x0, x1
.L40:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L42
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L40
.L42:
	mov	x1, #16
	add	x0, x0, x1
	b	.L38
.L43:
	mov	x1, #-72
	add	x0, x0, x1
.L44:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L46
	mov	x1, #-72
	add	x0, x0, x1
	b	.L44
.L46:
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #40
	add	x0, x0, x1
.L47:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L49
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L47
.L49:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #4
	add	x1, x1, x2
	str	x1, [x0]
.L50:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L55
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L52:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L54
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L52
.L54:
	mov	x1, #72
	add	x0, x0, x1
	b	.L50
.L55:
	mov	x2, #48
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #-48
	add	x0, x0, x2
	mov	x2, #7
	add	x1, x1, x2
	str	x1, [x0]
.L56:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L61
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L58:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L60
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L58
.L60:
	mov	x1, #72
	add	x0, x0, x1
	b	.L56
.L61:
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-128
	add	x0, x0, x1
.L62:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L64
	mov	x1, #-72
	add	x0, x0, x1
	b	.L62
.L64:
	mov	x1, #24
	add	x1, x0, x1
.L65:
	ldr	x0, [x1]
	cmp	w0, #0
	beq	.L1821
.L66:
	cmp	w0, #0
	beq	.L68
	mov	x2, #-1
	add	x0, x0, x2
	str	x0, [x1]
	b	.L66
.L68:
	mov	x0, #48
	add	x0, x1, x0
.L69:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L77
	mov	x1, #40
	add	x0, x0, x1
	mov	x1, #16
	add	x0, x0, x1
.L71:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L73
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L71
.L73:
	mov	x1, #-48
	add	x0, x0, x1
.L74:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L76
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L74
.L76:
	mov	x1, #64
	add	x0, x0, x1
	b	.L69
.L77:
	mov	x1, #-72
	add	x0, x0, x1
.L78:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L80
	mov	x1, #-72
	add	x0, x0, x1
	b	.L78
.L80:
	mov	x1, #72
	add	x0, x0, x1
.L81:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L89
	mov	x1, #64
	add	x0, x0, x1
.L83:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L85
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L83
.L85:
	mov	x1, #-56
	add	x0, x0, x1
.L86:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L88
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L86
.L88:
	mov	x1, #64
	add	x0, x0, x1
	b	.L81
.L89:
	mov	x1, #-72
	add	x0, x0, x1
.L90:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L92
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
	b	.L90
.L92:
	mov	x1, #56
	add	x0, x0, x1
.L93:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L95
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L93
.L95:
	mov	x1, #-56
	add	x0, x0, x1
.L96:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L98
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	b	.L96
.L98:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L99:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L133
.L100:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L102
	mov	x1, #72
	add	x0, x0, x1
	b	.L100
.L102:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L103:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L105
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L103
.L105:
	mov	x1, #8
	add	x0, x0, x1
.L106:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L108
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L106
.L108:
	mov	x1, #8
	add	x0, x0, x1
.L109:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L111
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L109
.L111:
	mov	x1, #8
	add	x0, x0, x1
.L112:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L114
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L112
.L114:
	mov	x1, #8
	add	x0, x0, x1
.L115:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L117
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L115
.L117:
	mov	x1, #8
	add	x0, x0, x1
.L118:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L120
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L118
.L120:
	mov	x1, #8
	add	x0, x0, x1
.L121:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L123
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L121
.L123:
	mov	x1, #8
	add	x0, x0, x1
.L124:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L126
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L124
.L126:
	mov	x1, #8
	add	x0, x0, x1
.L127:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L129
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L127
.L129:
	mov	x1, #-72
	add	x0, x0, x1
.L130:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L132
	mov	x1, #-72
	add	x0, x0, x1
	b	.L130
.L132:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L99
.L133:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L134:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L136
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L134
.L136:
	mov	x1, #-72
	add	x0, x0, x1
.L137:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L139
	mov	x1, #-72
	add	x0, x0, x1
	b	.L137
.L139:
	mov	x1, #72
	add	x0, x0, x1
.L140:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L171
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L142:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L144
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L142
.L144:
	mov	x1, #-32
	add	x0, x0, x1
.L145:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L158
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
.L147:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L155
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L149:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L151
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L149
.L151:
	mov	x1, #-16
	add	x0, x0, x1
.L152:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L154
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L152
.L154:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L147
.L155:
	mov	x1, #-64
	add	x0, x0, x1
.L156:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L145
	mov	x1, #-72
	add	x0, x0, x1
	b	.L156
.L158:
	mov	x1, #72
	add	x0, x0, x1
.L159:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L161
	mov	x1, #72
	add	x0, x0, x1
	b	.L159
.L161:
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
.L162:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L167
	mov	x1, #8
	add	x0, x0, x1
.L164:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L166
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L164
.L166:
	mov	x1, #-80
	add	x0, x0, x1
	b	.L162
.L167:
	mov	x1, #8
	add	x0, x0, x1
.L168:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L170
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L168
.L170:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L140
.L171:
	mov	x1, #-72
	add	x0, x0, x1
.L172:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L189
	mov	x2, #8
	add	x0, x0, x2
.L174:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L176
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L174
.L176:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L177:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L185
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L179:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L181
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L179
.L181:
	mov	x1, #-8
	add	x0, x0, x1
.L182:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L184
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L182
.L184:
	mov	x1, #32
	add	x0, x0, x1
	b	.L177
.L185:
	mov	x1, #-24
	add	x0, x0, x1
.L186:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L188
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L186
.L188:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L172
.L189:
	mov	x1, #40
	add	x0, x0, x1
	mov	x1, #32
	add	x0, x0, x1
.L190:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L192
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L190
.L192:
	mov	x1, #-72
	add	x0, x0, x1
.L193:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L195
	mov	x1, #-72
	add	x0, x0, x1
	b	.L193
.L195:
	mov	x1, #72
	add	x0, x0, x1
.L196:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L227
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L198:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L200
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L198
.L200:
	mov	x1, #-40
	add	x0, x0, x1
.L201:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L214
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
.L203:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L211
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
.L205:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L207
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L205
.L207:
	mov	x1, #-24
	add	x0, x0, x1
.L208:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L210
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L208
.L210:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L203
.L211:
	mov	x1, #-64
	add	x0, x0, x1
.L212:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L201
	mov	x1, #-72
	add	x0, x0, x1
	b	.L212
.L214:
	mov	x1, #72
	add	x0, x0, x1
.L215:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L217
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #56
	add	x0, x0, x1
	b	.L215
.L217:
	mov	x1, #-72
	add	x0, x0, x1
.L218:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L223
	mov	x1, #16
	add	x0, x0, x1
.L220:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L222
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L220
.L222:
	mov	x1, #-88
	add	x0, x0, x1
	b	.L218
.L223:
	mov	x1, #16
	add	x0, x0, x1
.L224:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L226
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L224
.L226:
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L196
.L227:
	mov	x1, #-72
	add	x0, x0, x1
.L228:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L245
	mov	x2, #8
	add	x0, x0, x2
.L230:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L232
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L230
.L232:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L233:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L241
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L235:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L237
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L235
.L237:
	mov	x1, #-8
	add	x0, x0, x1
.L238:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L240
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L238
.L240:
	mov	x1, #32
	add	x0, x0, x1
	b	.L233
.L241:
	mov	x1, #-24
	add	x0, x0, x1
.L242:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L244
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	mov	x1, #-8
	add	x0, x0, x1
	b	.L242
.L244:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L228
.L245:
	mov	x1, #72
	add	x0, x0, x1
.L246:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L251
	mov	x1, #32
	add	x0, x0, x1
.L248:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L250
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-288
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #104
	add	x0, x0, x1
	mov	x1, #184
	add	x0, x0, x1
	b	.L248
.L250:
	mov	x1, #40
	add	x0, x0, x1
	b	.L246
.L251:
	mov	x1, #-72
	add	x0, x0, x1
.L252:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L254
	mov	x1, #-72
	add	x0, x0, x1
	b	.L252
.L254:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L255:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L262
.L256:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L258
	mov	x1, #32
	add	x0, x0, x1
	mov	x1, #40
	add	x0, x0, x1
	b	.L256
.L258:
	mov	x1, #-72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
.L259:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L261
	mov	x1, #-72
	add	x0, x0, x1
	b	.L259
.L261:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L255
.L262:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #168
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L263:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L265
	mov	x1, #-48
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
	b	.L263
.L265:
	mov	x1, #72
	add	x0, x0, x1
.L266:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L317
	mov	x1, #24
	add	x0, x0, x1
.L268:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L270
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L268
.L270:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L271:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L287
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L273:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L275
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L273
.L275:
	mov	x1, #-32
	add	x0, x0, x1
.L276:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L271
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
.L278:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L280
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
	b	.L278
.L280:
	mov	x1, #32
	add	x0, x0, x1
.L281:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L283
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L281
.L283:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L284:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L286
	mov	x1, #72
	add	x0, x0, x1
	b	.L284
.L286:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L276
.L287:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L288:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L290
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L288
.L290:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L291:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L310
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L293:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L295
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L293
.L295:
	mov	x1, #-24
	add	x0, x0, x1
.L296:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L291
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-96
	add	x0, x0, x1
.L298:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L300
	mov	x1, #-72
	add	x0, x0, x1
	b	.L298
.L300:
	mov	x1, #24
	add	x0, x0, x1
.L301:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L303
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L301
.L303:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L304:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L306
	mov	x1, #72
	add	x0, x0, x1
	b	.L304
.L306:
	mov	x1, #8
	add	x0, x0, x1
.L307:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L309
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L307
.L309:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L296
.L310:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L311:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L316
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L313:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L315
	mov	x1, #72
	add	x0, x0, x1
	b	.L313
.L315:
	mov	x1, #-48
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
	b	.L311
.L316:
	mov	x1, #64
	add	x0, x0, x1
	b	.L266
.L317:
	mov	x1, #-72
	add	x0, x0, x1
.L318:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L320
	mov	x1, #-72
	add	x0, x0, x1
	b	.L318
.L320:
	mov	x1, #-56
	add	x0, x0, x1
.L321:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L323
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L321
.L323:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
	mov	x2, #7
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L324:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L326
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L324
.L326:
	mov	x1, #-32
	add	x0, x0, x1
.L327:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L332
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L329:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L331
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L329
.L331:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L327
.L332:
	mov	x1, #16
	add	x0, x0, x1
.L333:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1575
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L335:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L339
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L337:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L335
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L337
.L339:
	mov	x1, #8
	add	x0, x0, x1
.L340:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L345
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L342:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L344
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L342
.L344:
	mov	x1, #24
	add	x0, x0, x1
	b	.L340
.L345:
	mov	x1, #104
	add	x0, x0, x1
.L346:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L357
	mov	x1, #16
	add	x0, x0, x1
.L348:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L350
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L348
.L350:
	mov	x1, #8
	add	x0, x0, x1
.L351:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L353
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L351
.L353:
	mov	x1, #8
	add	x0, x0, x1
.L354:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L356
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L354
.L356:
	mov	x1, #40
	add	x0, x0, x1
	b	.L346
.L357:
	mov	x1, #-72
	add	x0, x0, x1
.L358:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L360
	mov	x1, #-72
	add	x0, x0, x1
	b	.L358
.L360:
	mov	x1, #24
	add	x0, x0, x1
.L361:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L363
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L361
.L363:
	mov	x1, #48
	add	x0, x0, x1
.L364:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L372
	mov	x1, #40
	add	x0, x0, x1
.L366:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L368
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L366
.L368:
	mov	x1, #-32
	add	x0, x0, x1
.L369:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L371
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L369
.L371:
	mov	x1, #64
	add	x0, x0, x1
	b	.L364
.L372:
	mov	x1, #-72
	add	x0, x0, x1
.L373:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L375
	mov	x1, #-72
	add	x0, x0, x1
	b	.L373
.L375:
	mov	x1, #72
	add	x0, x0, x1
.L376:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L381
	mov	x1, #16
	add	x0, x0, x1
.L378:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L380
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L378
.L380:
	mov	x1, #56
	add	x0, x0, x1
	b	.L376
.L381:
	mov	x1, #-72
	add	x0, x0, x1
.L382:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L384
	mov	x1, #-72
	add	x0, x0, x1
	b	.L382
.L384:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L385:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L419
.L386:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L388
	mov	x1, #72
	add	x0, x0, x1
	b	.L386
.L388:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L389:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L391
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L389
.L391:
	mov	x1, #8
	add	x0, x0, x1
.L392:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L394
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L392
.L394:
	mov	x1, #8
	add	x0, x0, x1
.L395:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L397
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L395
.L397:
	mov	x1, #8
	add	x0, x0, x1
.L398:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L400
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L398
.L400:
	mov	x1, #8
	add	x0, x0, x1
.L401:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L403
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L401
.L403:
	mov	x1, #8
	add	x0, x0, x1
.L404:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L406
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L404
.L406:
	mov	x1, #8
	add	x0, x0, x1
.L407:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L409
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L407
.L409:
	mov	x1, #8
	add	x0, x0, x1
.L410:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L412
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L410
.L412:
	mov	x1, #8
	add	x0, x0, x1
.L413:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L415
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L413
.L415:
	mov	x1, #-72
	add	x0, x0, x1
.L416:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L418
	mov	x1, #-72
	add	x0, x0, x1
	b	.L416
.L418:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L385
.L419:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L420:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L422
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L420
.L422:
	mov	x1, #-24
	add	x0, x0, x1
	mov	x1, #-48
	add	x0, x0, x1
.L423:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L425
	mov	x1, #-72
	add	x0, x0, x1
	b	.L423
.L425:
	mov	x1, #72
	add	x0, x0, x1
.L426:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L457
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L428:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L430
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L428
.L430:
	mov	x1, #-40
	add	x0, x0, x1
.L431:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L444
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
.L433:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L441
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L435:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L437
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L435
.L437:
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-8
	add	x0, x0, x1
.L438:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L440
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L438
.L440:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L433
.L441:
	mov	x1, #-64
	add	x0, x0, x1
.L442:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L431
	mov	x1, #-72
	add	x0, x0, x1
	b	.L442
.L444:
	mov	x1, #72
	add	x0, x0, x1
.L445:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L447
	mov	x1, #72
	add	x0, x0, x1
	b	.L445
.L447:
	mov	x1, #-72
	add	x0, x0, x1
.L448:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L453
	mov	x1, #8
	add	x0, x0, x1
.L450:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L452
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L450
.L452:
	mov	x1, #-80
	add	x0, x0, x1
	b	.L448
.L453:
	mov	x1, #8
	add	x0, x0, x1
.L454:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L456
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L454
.L456:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L426
.L457:
	mov	x1, #-72
	add	x0, x0, x1
.L458:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L475
	mov	x2, #8
	add	x0, x0, x2
.L460:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L462
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L460
.L462:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
.L463:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L471
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L465:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L467
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L465
.L467:
	mov	x1, #-8
	add	x0, x0, x1
.L468:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L470
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L468
.L470:
	mov	x1, #24
	add	x0, x0, x1
	b	.L463
.L471:
	mov	x1, #-16
	add	x0, x0, x1
.L472:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L474
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L472
.L474:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L458
.L475:
	mov	x1, #72
	add	x0, x0, x1
.L476:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L484
	mov	x1, #48
	add	x0, x0, x1
.L478:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L480
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L478
.L480:
	mov	x1, #-40
	add	x0, x0, x1
.L481:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L483
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L481
.L483:
	mov	x1, #64
	add	x0, x0, x1
	b	.L476
.L484:
	mov	x1, #-72
	add	x0, x0, x1
.L485:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L487
	mov	x1, #-72
	add	x0, x0, x1
	b	.L485
.L487:
	mov	x1, #72
	add	x0, x0, x1
.L488:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L490
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L488
.L490:
	mov	x1, #-72
	add	x0, x0, x1
.L491:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L493
	mov	x1, #-72
	add	x0, x0, x1
	b	.L491
.L493:
	mov	x1, #72
	add	x0, x0, x1
.L494:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L525
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L496:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L498
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L496
.L498:
	mov	x1, #-40
	add	x0, x0, x1
.L499:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L512
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
.L501:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L509
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L503:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L505
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L503
.L505:
	mov	x1, #-16
	add	x0, x0, x1
.L506:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L508
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L506
.L508:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L501
.L509:
	mov	x1, #-64
	add	x0, x0, x1
.L510:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L499
	mov	x1, #-72
	add	x0, x0, x1
	b	.L510
.L512:
	mov	x1, #72
	add	x0, x0, x1
.L513:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L515
	mov	x1, #72
	add	x0, x0, x1
	b	.L513
.L515:
	mov	x1, #-72
	add	x0, x0, x1
.L516:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L521
	mov	x1, #8
	add	x0, x0, x1
.L518:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L520
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L518
.L520:
	mov	x1, #-80
	add	x0, x0, x1
	b	.L516
.L521:
	mov	x1, #8
	add	x0, x0, x1
.L522:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L524
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L522
.L524:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L494
.L525:
	mov	x1, #-72
	add	x0, x0, x1
.L526:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L543
	mov	x2, #8
	add	x0, x0, x2
.L528:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L530
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L528
.L530:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L531:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L539
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L533:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L535
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L533
.L535:
	mov	x1, #-8
	add	x0, x0, x1
.L536:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L538
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L536
.L538:
	mov	x1, #32
	add	x0, x0, x1
	b	.L531
.L539:
	mov	x1, #-24
	add	x0, x0, x1
.L540:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L542
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L540
.L542:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L526
.L543:
	mov	x1, #72
	add	x0, x0, x1
.L544:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L549
	mov	x1, #32
	add	x0, x0, x1
.L546:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L548
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-288
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #288
	add	x0, x0, x1
	b	.L546
.L548:
	mov	x1, #40
	add	x0, x0, x1
	b	.L544
.L549:
	mov	x1, #-72
	add	x0, x0, x1
.L550:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L552
	mov	x1, #-72
	add	x0, x0, x1
	b	.L550
.L552:
	mov	x1, #72
	add	x0, x0, x1
.L553:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L558
	mov	x1, #24
	add	x0, x0, x1
.L555:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L557
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-288
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	mov	x1, #280
	add	x0, x0, x1
	b	.L555
.L557:
	mov	x1, #48
	add	x0, x0, x1
	b	.L553
.L558:
	mov	x1, #-72
	add	x0, x0, x1
.L559:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L561
	mov	x1, #-72
	add	x0, x0, x1
	b	.L559
.L561:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
	mov	x2, #7
	add	x1, x1, x2
	str	x1, [x0]
.L562:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L569
.L563:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L565
	mov	x1, #72
	add	x0, x0, x1
	b	.L563
.L565:
	mov	x1, #-72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
.L566:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L568
	mov	x1, #-72
	add	x0, x0, x1
	b	.L566
.L568:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L562
.L569:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L570:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L578
	mov	x1, #64
	add	x0, x0, x1
.L572:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L574
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L572
.L574:
	mov	x1, #-56
	add	x0, x0, x1
.L575:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L577
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L575
.L577:
	mov	x1, #64
	add	x0, x0, x1
	b	.L570
.L578:
	mov	x1, #-72
	add	x0, x0, x1
.L579:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L581
	mov	x1, #-72
	add	x0, x0, x1
	b	.L579
.L581:
	mov	x1, #72
	add	x0, x0, x1
.L582:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L587
	mov	x1, #48
	add	x0, x0, x1
.L584:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L586
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L584
.L586:
	mov	x1, #24
	add	x0, x0, x1
	b	.L582
.L587:
	mov	x1, #-72
	add	x0, x0, x1
.L588:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L590
	mov	x1, #-72
	add	x0, x0, x1
	b	.L588
.L590:
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L591:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L593
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L591
.L593:
	mov	x1, #8
	add	x0, x0, x1
.L594:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L602
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
.L596:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L598
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L596
.L598:
	mov	x1, #40
	add	x0, x0, x1
.L599:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L601
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L599
.L601:
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	b	.L594
.L602:
	mov	x1, #-8
	add	x0, x0, x1
.L603:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L605
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L603
.L605:
	mov	x1, #-40
	add	x0, x0, x1
.L606:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L608
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	b	.L606
.L608:
	mov	x2, #48
	add	x0, x0, x2
.L609:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L611
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L609
.L611:
	mov	x2, #-48
	add	x0, x0, x2
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L612:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L614
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L612
.L614:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L615:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L752
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L617:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L668
	mov	x1, #16
	add	x0, x0, x1
.L619:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L621
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L619
.L621:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L622:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L638
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L624:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L626
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L624
.L626:
	mov	x1, #-24
	add	x0, x0, x1
.L627:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L622
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-96
	add	x0, x0, x1
.L629:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L631
	mov	x1, #-72
	add	x0, x0, x1
	b	.L629
.L631:
	mov	x1, #24
	add	x0, x0, x1
.L632:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L634
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L632
.L634:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L635:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L637
	mov	x1, #72
	add	x0, x0, x1
	b	.L635
.L637:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L627
.L638:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
.L639:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L641
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L639
.L641:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L642:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L661
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L644:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L646
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L644
.L646:
	mov	x1, #-16
	add	x0, x0, x1
.L647:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L642
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-88
	add	x0, x0, x1
.L649:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L651
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
	b	.L649
.L651:
	mov	x1, #32
	add	x0, x0, x1
.L652:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L654
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L652
.L654:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L655:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L657
	mov	x1, #72
	add	x0, x0, x1
	b	.L655
.L657:
	mov	x1, #8
	add	x0, x0, x1
.L658:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L660
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L658
.L660:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L647
.L661:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L662:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L667
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L664:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L666
	mov	x1, #72
	add	x0, x0, x1
	b	.L664
.L666:
	mov	x1, #-64
	add	x0, x0, x1
	b	.L662
.L667:
	mov	x1, #64
	add	x0, x0, x1
	b	.L617
.L668:
	mov	x1, #-72
	add	x0, x0, x1
.L669:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L671
	mov	x1, #-72
	add	x0, x0, x1
	b	.L669
.L671:
	mov	x1, #32
	add	x0, x0, x1
.L672:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L674
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L672
.L674:
	mov	x1, #-32
	add	x0, x0, x1
.L675:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L713
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L677:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L685
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L679:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L681
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L679
.L681:
	mov	x1, #-16
	add	x0, x0, x1
.L682:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L684
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L682
.L684:
	mov	x1, #64
	add	x0, x0, x1
	b	.L677
.L685:
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L686:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L709
	mov	x1, #8
	add	x0, x0, x1
.L688:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L699
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L690:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L695
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
.L692:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L694
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L692
.L694:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L690
.L695:
	mov	x1, #8
	add	x0, x0, x1
.L696:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L698
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L696
.L698:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L688
.L699:
	mov	x1, #8
	add	x0, x0, x1
.L700:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L705
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L702:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L704
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L702
.L704:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L700
.L705:
	mov	x1, #8
	add	x0, x0, x1
.L706:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L708
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L706
.L708:
	mov	x1, #-16
	add	x0, x0, x1
	mov	x1, #-80
	add	x0, x0, x1
	b	.L686
.L709:
	mov	x1, #32
	add	x0, x0, x1
.L710:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L712
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L710
.L712:
	mov	x1, #-32
	add	x0, x0, x1
	b	.L675
.L713:
	mov	x1, #24
	add	x0, x0, x1
.L714:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L716
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L714
.L716:
	mov	x1, #-24
	add	x0, x0, x1
.L717:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L615
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L719:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L727
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L721:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L723
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	b	.L721
.L723:
	mov	x1, #-8
	add	x0, x0, x1
.L724:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L726
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L724
.L726:
	mov	x1, #64
	add	x0, x0, x1
	b	.L719
.L727:
	mov	x1, #-24
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L728:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L751
	mov	x1, #8
	add	x0, x0, x1
.L730:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L741
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L732:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L737
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
.L734:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L736
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L734
.L736:
	mov	x1, #8
	add	x0, x0, x1
	b	.L732
.L737:
	mov	x1, #-8
	add	x0, x0, x1
.L738:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L740
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
	b	.L738
.L740:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L730
.L741:
	mov	x1, #16
	add	x0, x0, x1
.L742:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L747
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L744:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L746
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
	b	.L744
.L746:
	mov	x1, #8
	add	x0, x0, x1
	b	.L742
.L747:
	mov	x1, #-8
	add	x0, x0, x1
.L748:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L750
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L748
.L750:
	mov	x1, #-88
	add	x0, x0, x1
	b	.L728
.L751:
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	b	.L717
.L752:
	mov	x1, #32
	add	x0, x0, x1
.L753:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L755
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L753
.L755:
	mov	x1, #-32
	add	x0, x0, x1
.L756:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L784
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L758:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L760
	mov	x1, #72
	add	x0, x0, x1
	b	.L758
.L760:
	mov	x1, #-72
	add	x0, x0, x1
.L761:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L756
	mov	x1, #8
	add	x0, x0, x1
.L763:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L774
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L765:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L770
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
.L767:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L769
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L767
.L769:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L765
.L770:
	mov	x1, #8
	add	x0, x0, x1
.L771:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L773
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L771
.L773:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L763
.L774:
	mov	x1, #8
	add	x0, x0, x1
.L775:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L780
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L777:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L779
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L777
.L779:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L775
.L780:
	mov	x1, #8
	add	x0, x0, x1
.L781:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L783
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L781
.L783:
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
	b	.L761
.L784:
	mov	x1, #8
	add	x0, x0, x1
.L785:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L787
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L785
.L787:
	mov	x1, #16
	add	x0, x0, x1
.L788:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L790
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L788
.L790:
	mov	x1, #8
	add	x0, x0, x1
.L791:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L793
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L791
.L793:
	mov	x1, #40
	add	x0, x0, x1
.L794:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L802
	mov	x1, #16
	add	x0, x0, x1
.L796:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L798
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L796
.L798:
	mov	x1, #8
	add	x0, x0, x1
.L799:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L801
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L799
.L801:
	mov	x1, #48
	add	x0, x0, x1
	b	.L794
.L802:
	mov	x1, #-72
	add	x0, x0, x1
.L803:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L805
	mov	x1, #-72
	add	x0, x0, x1
	b	.L803
.L805:
	mov	x1, #72
	add	x0, x0, x1
.L806:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L814
	mov	x1, #40
	add	x0, x0, x1
.L808:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L810
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L808
.L810:
	mov	x1, #-32
	add	x0, x0, x1
.L811:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L813
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L811
.L813:
	mov	x1, #64
	add	x0, x0, x1
	b	.L806
.L814:
	mov	x1, #-72
	add	x0, x0, x1
.L815:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L817
	mov	x1, #-72
	add	x0, x0, x1
	b	.L815
.L817:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L818:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L852
.L819:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L821
	mov	x1, #72
	add	x0, x0, x1
	b	.L819
.L821:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L822:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L824
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L822
.L824:
	mov	x1, #8
	add	x0, x0, x1
.L825:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L827
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L825
.L827:
	mov	x1, #8
	add	x0, x0, x1
.L828:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L830
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L828
.L830:
	mov	x1, #8
	add	x0, x0, x1
.L831:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L833
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L831
.L833:
	mov	x1, #8
	add	x0, x0, x1
.L834:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L836
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L834
.L836:
	mov	x1, #8
	add	x0, x0, x1
.L837:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L839
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L837
.L839:
	mov	x1, #8
	add	x0, x0, x1
.L840:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L842
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L840
.L842:
	mov	x1, #8
	add	x0, x0, x1
.L843:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L845
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L843
.L845:
	mov	x1, #8
	add	x0, x0, x1
.L846:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L848
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L846
.L848:
	mov	x1, #-72
	add	x0, x0, x1
.L849:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L851
	mov	x1, #-72
	add	x0, x0, x1
	b	.L849
.L851:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L818
.L852:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L853:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L855
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L853
.L855:
	mov	x1, #-72
	add	x0, x0, x1
.L856:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L858
	mov	x1, #-72
	add	x0, x0, x1
	b	.L856
.L858:
	mov	x1, #72
	add	x0, x0, x1
.L859:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L890
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L861:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L863
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L861
.L863:
	mov	x1, #-32
	add	x0, x0, x1
.L864:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L877
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
.L866:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L874
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L868:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L870
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L868
.L870:
	mov	x1, #-16
	add	x0, x0, x1
.L871:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L873
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L871
.L873:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L866
.L874:
	mov	x1, #-64
	add	x0, x0, x1
.L875:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L864
	mov	x1, #-72
	add	x0, x0, x1
	b	.L875
.L877:
	mov	x1, #72
	add	x0, x0, x1
.L878:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L880
	mov	x1, #72
	add	x0, x0, x1
	b	.L878
.L880:
	mov	x1, #-64
	add	x0, x0, x1
	mov	x1, #-8
	add	x0, x0, x1
.L881:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L886
	mov	x1, #8
	add	x0, x0, x1
.L883:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L885
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L883
.L885:
	mov	x1, #-80
	add	x0, x0, x1
	b	.L881
.L886:
	mov	x1, #8
	add	x0, x0, x1
.L887:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L889
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L887
.L889:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L859
.L890:
	mov	x1, #-72
	add	x0, x0, x1
.L891:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L908
	mov	x2, #8
	add	x0, x0, x2
.L893:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L895
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L893
.L895:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
.L896:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L904
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L898:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L900
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L898
.L900:
	mov	x1, #-8
	add	x0, x0, x1
.L901:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L903
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L901
.L903:
	mov	x1, #24
	add	x0, x0, x1
	b	.L896
.L904:
	mov	x1, #-16
	add	x0, x0, x1
.L905:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L907
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L905
.L907:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L891
.L908:
	mov	x1, #72
	add	x0, x0, x1
.L909:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L914
	mov	x1, #24
	add	x0, x0, x1
.L911:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L913
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-288
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #288
	add	x0, x0, x1
	b	.L911
.L913:
	mov	x1, #8
	add	x0, x0, x1
	mov	x1, #40
	add	x0, x0, x1
	b	.L909
.L914:
	mov	x1, #-72
	add	x0, x0, x1
.L915:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L917
	mov	x1, #-72
	add	x0, x0, x1
	b	.L915
.L917:
	mov	x1, #40
	add	x0, x0, x1
.L918:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L920
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L918
.L920:
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L921:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L928
.L922:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L924
	mov	x1, #72
	add	x0, x0, x1
	b	.L922
.L924:
	mov	x1, #-72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
.L925:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L927
	mov	x1, #-72
	add	x0, x0, x1
	b	.L925
.L927:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L921
.L928:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L929:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L980
	mov	x1, #24
	add	x0, x0, x1
.L931:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L933
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L931
.L933:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L934:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L950
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L936:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L938
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L936
.L938:
	mov	x1, #-32
	add	x0, x0, x1
.L939:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L934
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-64
	add	x0, x0, x1
.L941:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L943
	mov	x1, #-72
	add	x0, x0, x1
	b	.L941
.L943:
	mov	x1, #32
	add	x0, x0, x1
.L944:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L946
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L944
.L946:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L947:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L949
	mov	x1, #72
	add	x0, x0, x1
	b	.L947
.L949:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L939
.L950:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L951:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L953
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L951
.L953:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L954:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L973
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L956:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L958
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L956
.L958:
	mov	x1, #-24
	add	x0, x0, x1
.L959:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L954
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-96
	add	x0, x0, x1
.L961:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L963
	mov	x1, #-72
	add	x0, x0, x1
	b	.L961
.L963:
	mov	x1, #24
	add	x0, x0, x1
.L964:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L966
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L964
.L966:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L967:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L969
	mov	x1, #72
	add	x0, x0, x1
	b	.L967
.L969:
	mov	x1, #8
	add	x0, x0, x1
.L970:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L972
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L970
.L972:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L959
.L973:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L974:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L979
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L976:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L978
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #56
	add	x0, x0, x1
	b	.L976
.L978:
	mov	x1, #-64
	add	x0, x0, x1
	b	.L974
.L979:
	mov	x1, #64
	add	x0, x0, x1
	b	.L929
.L980:
	mov	x1, #-72
	add	x0, x0, x1
.L981:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L983
	mov	x1, #-72
	add	x0, x0, x1
	b	.L981
.L983:
	mov	x1, #24
	add	x0, x0, x1
.L984:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L986
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L984
.L986:
	mov	x1, #-24
	add	x0, x0, x1
.L987:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1021
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L989:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L997
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
.L991:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L993
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L991
.L993:
	mov	x1, #-24
	add	x0, x0, x1
.L994:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L996
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L994
.L996:
	mov	x1, #64
	add	x0, x0, x1
	b	.L989
.L997:
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L998:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L987
	mov	x1, #8
	add	x0, x0, x1
.L1000:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1011
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1002:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1007
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #96
	add	x0, x0, x1
.L1004:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1006
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1004
.L1006:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1002
.L1007:
	mov	x1, #8
	add	x0, x0, x1
.L1008:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1010
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #96
	add	x0, x0, x1
	b	.L1008
.L1010:
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1000
.L1011:
	mov	x1, #16
	add	x0, x0, x1
.L1012:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1017
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1014:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1016
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #96
	add	x0, x0, x1
	b	.L1014
.L1016:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1012
.L1017:
	mov	x1, #8
	add	x0, x0, x1
.L1018:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1020
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1018
.L1020:
	mov	x1, #-104
	add	x0, x0, x1
	b	.L998
.L1021:
	mov	x1, #32
	add	x0, x0, x1
.L1022:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1024
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1022
.L1024:
	mov	x1, #-32
	add	x0, x0, x1
.L1025:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1060
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1027:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1035
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1029:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1031
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1029
.L1031:
	mov	x1, #-16
	add	x0, x0, x1
.L1032:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1034
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1032
.L1034:
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #48
	add	x0, x0, x1
	b	.L1027
.L1035:
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1036:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1059
	mov	x1, #8
	add	x0, x0, x1
.L1038:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1049
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1040:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1045
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
.L1042:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1044
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	b	.L1042
.L1044:
	mov	x1, #8
	add	x0, x0, x1
	b	.L1040
.L1045:
	mov	x1, #-8
	add	x0, x0, x1
.L1046:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1048
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	mov	x1, #56
	add	x0, x0, x1
	b	.L1046
.L1048:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1038
.L1049:
	mov	x1, #24
	add	x0, x0, x1
.L1050:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1055
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1052:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1054
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-80
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L1052
.L1054:
	mov	x1, #8
	add	x0, x0, x1
	b	.L1050
.L1055:
	mov	x1, #-8
	add	x0, x0, x1
.L1056:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1058
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	b	.L1056
.L1058:
	mov	x1, #-96
	add	x0, x0, x1
	b	.L1036
.L1059:
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	b	.L1025
.L1060:
	mov	x1, #72
	add	x0, x0, x1
.L1061:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1072
	mov	x1, #24
	add	x0, x0, x1
.L1063:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1065
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1063
.L1065:
	mov	x1, #8
	add	x0, x0, x1
.L1066:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1068
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1066
.L1068:
	mov	x1, #8
	add	x0, x0, x1
.L1069:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1071
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1069
.L1071:
	mov	x1, #32
	add	x0, x0, x1
	b	.L1061
.L1072:
	mov	x1, #-72
	add	x0, x0, x1
.L1073:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1075
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1073
.L1075:
	mov	x1, #24
	add	x0, x0, x1
.L1076:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1078
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1076
.L1078:
	mov	x1, #8
	add	x0, x0, x1
.L1079:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1081
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1079
.L1081:
	mov	x1, #40
	add	x0, x0, x1
.L1082:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1090
	mov	x1, #56
	add	x0, x0, x1
.L1084:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1086
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L1084
.L1086:
	mov	x1, #-48
	add	x0, x0, x1
.L1087:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1089
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1087
.L1089:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1082
.L1090:
	mov	x1, #-72
	add	x0, x0, x1
.L1091:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1093
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1091
.L1093:
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1094:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1096
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	mov	x1, #8
	add	x0, x0, x1
	b	.L1094
.L1096:
	mov	x1, #16
	add	x0, x0, x1
.L1097:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1105
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
.L1099:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1101
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L1099
.L1101:
	mov	x1, #40
	add	x0, x0, x1
.L1102:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1104
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L1102
.L1104:
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1097
.L1105:
	mov	x1, #-16
	add	x0, x0, x1
.L1106:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1108
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1106
.L1108:
	mov	x1, #-40
	add	x0, x0, x1
.L1109:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1111
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1109
.L1111:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1112:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1114
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1112
.L1114:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L1115:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1260
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1117:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1168
	mov	x1, #24
	add	x0, x0, x1
.L1119:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1121
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1119
.L1121:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L1122:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1138
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1124:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1126
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1124
.L1126:
	mov	x1, #-16
	add	x0, x0, x1
.L1127:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1122
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	mov	x1, #-72
	add	x0, x0, x1
.L1129:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1131
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1129
.L1131:
	mov	x1, #32
	add	x0, x0, x1
.L1132:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1134
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1132
.L1134:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1135:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1137
	mov	x1, #72
	add	x0, x0, x1
	b	.L1135
.L1137:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1127
.L1138:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1139:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1141
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1139
.L1141:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1142:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1161
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1144:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1146
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1144
.L1146:
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
.L1147:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1142
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-96
	add	x0, x0, x1
.L1149:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1151
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1149
.L1151:
	mov	x1, #24
	add	x0, x0, x1
.L1152:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1154
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1152
.L1154:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L1155:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1157
	mov	x1, #72
	add	x0, x0, x1
	b	.L1155
.L1157:
	mov	x1, #8
	add	x0, x0, x1
.L1158:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1160
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1158
.L1160:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1147
.L1161:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1162:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1167
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1164:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1166
	mov	x1, #72
	add	x0, x0, x1
	b	.L1164
.L1166:
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-56
	add	x0, x0, x1
	b	.L1162
.L1167:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1117
.L1168:
	mov	x1, #-72
	add	x0, x0, x1
.L1169:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1171
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1169
.L1171:
	mov	x1, #24
	add	x0, x0, x1
.L1172:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1174
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1172
.L1174:
	mov	x1, #-24
	add	x0, x0, x1
.L1175:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1218
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L1177:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1185
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1179:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1181
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	b	.L1179
.L1181:
	mov	x1, #-8
	add	x0, x0, x1
.L1182:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1184
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1182
.L1184:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1177
.L1185:
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1186:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1209
	mov	x1, #8
	add	x0, x0, x1
.L1188:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1199
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1190:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1195
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
.L1192:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1194
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1192
.L1194:
	mov	x1, #8
	add	x0, x0, x1
	b	.L1190
.L1195:
	mov	x1, #-8
	add	x0, x0, x1
.L1196:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1198
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
	b	.L1196
.L1198:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1188
.L1199:
	mov	x1, #16
	add	x0, x0, x1
.L1200:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1205
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L1202:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1204
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #80
	add	x0, x0, x1
	b	.L1202
.L1204:
	mov	x1, #8
	add	x0, x0, x1
	b	.L1200
.L1205:
	mov	x1, #-8
	add	x0, x0, x1
.L1206:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1208
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1206
.L1208:
	mov	x1, #-88
	add	x0, x0, x1
	b	.L1186
.L1209:
	mov	x1, #40
	add	x0, x0, x1
.L1210:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1212
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1210
.L1212:
	mov	x1, #16
	add	x0, x0, x1
.L1213:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1215
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1213
.L1215:
	mov	x1, #-56
	add	x0, x0, x1
.L1216:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1175
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	b	.L1216
.L1218:
	mov	x1, #32
	add	x0, x0, x1
.L1219:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1221
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	mov	x1, #24
	add	x0, x0, x1
	b	.L1219
.L1221:
	mov	x1, #-32
	add	x0, x0, x1
.L1222:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1256
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1224:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1232
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1226:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1228
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1226
.L1228:
	mov	x1, #-16
	add	x0, x0, x1
.L1229:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1231
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1229
.L1231:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1224
.L1232:
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1233:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1222
	mov	x1, #8
	add	x0, x0, x1
.L1235:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1246
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L1237:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1242
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
.L1239:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1241
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1239
.L1241:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1237
.L1242:
	mov	x1, #8
	add	x0, x0, x1
.L1243:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1245
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L1243
.L1245:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1235
.L1246:
	mov	x1, #8
	add	x0, x0, x1
.L1247:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1252
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1249:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1251
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L1249
.L1251:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1247
.L1252:
	mov	x1, #8
	add	x0, x0, x1
.L1253:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1255
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1253
.L1255:
	mov	x1, #-96
	add	x0, x0, x1
	b	.L1233
.L1256:
	mov	x1, #32
	add	x0, x0, x1
.L1257:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1259
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1257
.L1259:
	mov	x1, #-32
	add	x0, x0, x1
	b	.L1115
.L1260:
	mov	x1, #32
	add	x0, x0, x1
.L1261:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1263
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #16
	add	x0, x0, x1
	b	.L1261
.L1263:
	mov	x1, #-32
	add	x0, x0, x1
.L1264:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1301
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1266:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1268
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1266
.L1268:
	mov	x1, #16
	add	x0, x0, x1
.L1269:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1271
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1269
.L1271:
	mov	x1, #-56
	add	x0, x0, x1
.L1272:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1274
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	b	.L1272
.L1274:
	mov	x1, #72
	add	x0, x0, x1
.L1275:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1277
	mov	x1, #48
	add	x0, x0, x1
	mov	x1, #24
	add	x0, x0, x1
	b	.L1275
.L1277:
	mov	x1, #-72
	add	x0, x0, x1
.L1278:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1264
	mov	x1, #8
	add	x0, x0, x1
.L1280:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1291
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L1282:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1287
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
.L1284:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1286
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1284
.L1286:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1282
.L1287:
	mov	x1, #8
	add	x0, x0, x1
.L1288:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1290
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L1288
.L1290:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1280
.L1291:
	mov	x1, #8
	add	x0, x0, x1
.L1292:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1297
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1294:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1296
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #88
	add	x0, x0, x1
	b	.L1294
.L1296:
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1292
.L1297:
	mov	x1, #8
	add	x0, x0, x1
.L1298:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1300
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1298
.L1300:
	mov	x1, #-64
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
	b	.L1278
.L1301:
	mov	x1, #72
	add	x0, x0, x1
.L1302:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1310
	mov	x1, #16
	add	x0, x0, x1
.L1304:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1306
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1304
.L1306:
	mov	x1, #8
	add	x0, x0, x1
.L1307:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1309
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1307
.L1309:
	mov	x1, #48
	add	x0, x0, x1
	b	.L1302
.L1310:
	mov	x1, #-72
	add	x0, x0, x1
.L1311:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1313
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1311
.L1313:
	mov	x1, #24
	add	x0, x0, x1
.L1314:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1316
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1314
.L1316:
	mov	x1, #8
	add	x0, x0, x1
.L1317:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1319
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1317
.L1319:
	mov	x1, #40
	add	x0, x0, x1
.L1320:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1328
	mov	x1, #40
	add	x0, x0, x1
.L1322:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1324
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1322
.L1324:
	mov	x1, #-32
	add	x0, x0, x1
.L1325:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1327
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1325
.L1327:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1320
.L1328:
	mov	x1, #-72
	add	x0, x0, x1
.L1329:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1331
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1329
.L1331:
	mov	x1, #72
	add	x0, x0, x1
.L1332:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1340
	mov	x1, #48
	add	x0, x0, x1
.L1334:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1336
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L1334
.L1336:
	mov	x1, #-40
	add	x0, x0, x1
.L1337:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1339
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1337
.L1339:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1332
.L1340:
	mov	x1, #-72
	add	x0, x0, x1
.L1341:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1343
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1341
.L1343:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L1344:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1378
.L1345:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1347
	mov	x1, #32
	add	x0, x0, x1
	mov	x1, #40
	add	x0, x0, x1
	b	.L1345
.L1347:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1348:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1350
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1348
.L1350:
	mov	x1, #8
	add	x0, x0, x1
.L1351:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1353
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1351
.L1353:
	mov	x1, #8
	add	x0, x0, x1
.L1354:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1356
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1354
.L1356:
	mov	x1, #8
	add	x0, x0, x1
.L1357:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1359
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1357
.L1359:
	mov	x1, #8
	add	x0, x0, x1
.L1360:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1362
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1360
.L1362:
	mov	x1, #8
	add	x0, x0, x1
.L1363:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1365
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1363
.L1365:
	mov	x1, #8
	add	x0, x0, x1
.L1366:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1368
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1366
.L1368:
	mov	x1, #8
	add	x0, x0, x1
.L1369:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1371
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1369
.L1371:
	mov	x1, #8
	add	x0, x0, x1
.L1372:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1374
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1372
.L1374:
	mov	x1, #-72
	add	x0, x0, x1
.L1375:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1377
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1375
.L1377:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1344
.L1378:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L1379:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1381
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #48
	add	x0, x0, x1
	b	.L1379
.L1381:
	mov	x1, #-72
	add	x0, x0, x1
.L1382:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1384
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1382
.L1384:
	mov	x1, #72
	add	x0, x0, x1
.L1385:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1416
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1387:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1389
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1387
.L1389:
	mov	x1, #-32
	add	x0, x0, x1
.L1390:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1403
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
.L1392:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1400
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1394:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1396
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1394
.L1396:
	mov	x1, #-16
	add	x0, x0, x1
.L1397:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1399
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L1397
.L1399:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L1392
.L1400:
	mov	x1, #-64
	add	x0, x0, x1
.L1401:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1390
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1401
.L1403:
	mov	x1, #72
	add	x0, x0, x1
.L1404:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1406
	mov	x1, #72
	add	x0, x0, x1
	b	.L1404
.L1406:
	mov	x1, #-72
	add	x0, x0, x1
.L1407:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1412
	mov	x1, #8
	add	x0, x0, x1
.L1409:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1411
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1409
.L1411:
	mov	x1, #-80
	add	x0, x0, x1
	b	.L1407
.L1412:
	mov	x1, #8
	add	x0, x0, x1
.L1413:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1415
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1413
.L1415:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1385
.L1416:
	mov	x1, #-72
	add	x0, x0, x1
.L1417:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1434
	mov	x2, #8
	add	x0, x0, x2
.L1419:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1421
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1419
.L1421:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1422:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1430
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1424:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1426
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L1424
.L1426:
	mov	x1, #-8
	add	x0, x0, x1
.L1427:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1429
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1427
.L1429:
	mov	x1, #32
	add	x0, x0, x1
	b	.L1422
.L1430:
	mov	x1, #-24
	add	x0, x0, x1
.L1431:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1433
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1431
.L1433:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1417
.L1434:
	mov	x1, #72
	add	x0, x0, x1
.L1435:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1437
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1435
.L1437:
	mov	x1, #-72
	add	x0, x0, x1
.L1438:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1440
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1438
.L1440:
	mov	x1, #72
	add	x0, x0, x1
.L1441:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1472
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1443:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1445
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L1443
.L1445:
	mov	x1, #-40
	add	x0, x0, x1
.L1446:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1459
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
.L1448:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1456
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
.L1450:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1452
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1450
.L1452:
	mov	x1, #-24
	add	x0, x0, x1
.L1453:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1455
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L1453
.L1455:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	b	.L1448
.L1456:
	mov	x1, #-64
	add	x0, x0, x1
.L1457:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1446
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1457
.L1459:
	mov	x1, #72
	add	x0, x0, x1
.L1460:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1462
	mov	x1, #48
	add	x0, x0, x1
	mov	x1, #24
	add	x0, x0, x1
	b	.L1460
.L1462:
	mov	x1, #-72
	add	x0, x0, x1
.L1463:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1468
	mov	x1, #16
	add	x0, x0, x1
.L1465:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1467
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1465
.L1467:
	mov	x1, #-88
	add	x0, x0, x1
	b	.L1463
.L1468:
	mov	x1, #16
	add	x0, x0, x1
.L1469:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1471
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1469
.L1471:
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	mov	x1, #40
	add	x0, x0, x1
	b	.L1441
.L1472:
	mov	x1, #-72
	add	x0, x0, x1
.L1473:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1490
	mov	x2, #8
	add	x0, x0, x2
.L1475:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1477
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1475
.L1477:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1478:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1486
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1480:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1482
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L1480
.L1482:
	mov	x1, #-8
	add	x0, x0, x1
.L1483:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1485
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1483
.L1485:
	mov	x1, #32
	add	x0, x0, x1
	b	.L1478
.L1486:
	mov	x1, #-24
	add	x0, x0, x1
.L1487:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1489
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1487
.L1489:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1473
.L1490:
	mov	x1, #72
	add	x0, x0, x1
.L1491:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1496
	mov	x1, #32
	add	x0, x0, x1
.L1493:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1495
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-288
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #136
	add	x0, x0, x1
	mov	x1, #152
	add	x0, x0, x1
	b	.L1493
.L1495:
	mov	x1, #40
	add	x0, x0, x1
	b	.L1491
.L1496:
	mov	x1, #-72
	add	x0, x0, x1
.L1497:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1499
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1497
.L1499:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #15
	add	x1, x1, x2
	str	x1, [x0]
.L1500:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1507
.L1501:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1503
	mov	x1, #64
	add	x0, x0, x1
	mov	x1, #8
	add	x0, x0, x1
	b	.L1501
.L1503:
	mov	x1, #-72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
.L1504:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1506
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1504
.L1506:
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1500
.L1507:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #168
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L1508:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1510
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1508
.L1510:
	mov	x1, #72
	add	x0, x0, x1
.L1511:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1562
	mov	x1, #24
	add	x0, x0, x1
.L1513:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1515
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1513
.L1515:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
.L1516:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1532
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1518:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1520
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1518
.L1520:
	mov	x1, #-32
	add	x0, x0, x1
.L1521:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1516
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-104
	add	x0, x0, x1
.L1523:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1525
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1523
.L1525:
	mov	x1, #32
	add	x0, x0, x1
.L1526:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1528
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1526
.L1528:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1529:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1531
	mov	x1, #72
	add	x0, x0, x1
	b	.L1529
.L1531:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1521
.L1532:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1533:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1535
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1533
.L1535:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L1536:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1555
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1538:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1540
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1538
.L1540:
	mov	x1, #-24
	add	x0, x0, x1
.L1541:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1536
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-88
	add	x0, x0, x1
.L1543:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1545
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1543
.L1545:
	mov	x1, #24
	add	x0, x0, x1
.L1546:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1548
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1546
.L1548:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L1549:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1551
	mov	x1, #72
	add	x0, x0, x1
	b	.L1549
.L1551:
	mov	x1, #8
	add	x0, x0, x1
.L1552:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1554
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1552
.L1554:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1541
.L1555:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1556:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1561
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1558:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1560
	mov	x1, #72
	add	x0, x0, x1
	b	.L1558
.L1560:
	mov	x1, #-64
	add	x0, x0, x1
	b	.L1556
.L1561:
	mov	x1, #8
	add	x0, x0, x1
	mov	x1, #56
	add	x0, x0, x1
	b	.L1511
.L1562:
	mov	x1, #-72
	add	x0, x0, x1
.L1563:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1565
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1563
.L1565:
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1566:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1568
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1566
.L1568:
	mov	x1, #-32
	add	x0, x0, x1
.L1569:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1574
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1571:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1573
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1571
.L1573:
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1569
.L1574:
	mov	x1, #16
	add	x0, x0, x1
	b	.L333
.L1575:
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1576:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1578
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1576
.L1578:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L1579:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1581
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #16
	add	x0, x19, x0
	b	.L1579
.L1581:
	mov	x1, #32
	add	x0, x0, x1
.L1582:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1584
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #56
	add	x0, x19, x0
	b	.L1582
.L1584:
	mov	x2, #-24
	add	x0, x0, x2
.L1585:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1587
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1585
.L1587:
	mov	x2, #8
	add	x0, x0, x2
.L1588:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1590
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1588
.L1590:
	mov	x2, #8
	add	x0, x0, x2
.L1591:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1593
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1591
.L1593:
	mov	x2, #8
	add	x0, x0, x2
.L1594:
	cmp	w1, #0
	beq	.L1596
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1594
.L1596:
	mov	x1, #8
	add	x0, x0, x1
.L1597:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1599
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1597
.L1599:
	mov	x1, #8
	add	x0, x0, x1
.L1600:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1602
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1600
.L1602:
	mov	x1, #24
	add	x0, x0, x1
.L1603:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1623
	mov	x1, #8
	add	x0, x0, x1
.L1605:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1607
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1605
.L1607:
	mov	x1, #8
	add	x0, x0, x1
.L1608:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1610
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1608
.L1610:
	mov	x1, #8
	add	x0, x0, x1
.L1611:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1613
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1611
.L1613:
	mov	x1, #8
	add	x0, x0, x1
.L1614:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1616
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1614
.L1616:
	mov	x1, #8
	add	x0, x0, x1
.L1617:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1619
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1617
.L1619:
	mov	x1, #8
	add	x0, x0, x1
.L1620:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1622
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1620
.L1622:
	mov	x1, #24
	add	x0, x0, x1
	b	.L1603
.L1623:
	mov	x1, #-72
	add	x0, x0, x1
.L1624:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1626
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1624
.L1626:
	mov	x1, #72
	add	x0, x0, x1
.L1627:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1632
	mov	x1, #40
	add	x0, x0, x1
.L1629:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1631
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1629
.L1631:
	mov	x1, #32
	add	x0, x0, x1
	b	.L1627
.L1632:
	mov	x1, #-72
	add	x0, x0, x1
.L1633:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1635
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1633
.L1635:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L1636:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1641
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L1638:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1640
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1638
.L1640:
	mov	x1, #72
	add	x0, x0, x1
	b	.L1636
.L1641:
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	mov	x1, #-48
	add	x0, x0, x1
.L1642:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1644
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1642
.L1644:
	mov	x1, #56
	add	x0, x0, x1
.L1645:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1647
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1645
.L1647:
	mov	x1, #-56
	add	x0, x0, x1
.L1648:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1670
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x1, x0, x1
	ldr	x0, [x1]
	mov	x2, #1
	add	x0, x0, x2
	str	x0, [x1]
.L1650:
	cmp	w0, #0
	beq	.L1652
	mov	x2, #-1
	add	x0, x0, x2
	str	x0, [x1]
	b	.L1650
.L1652:
	mov	x0, #16
	add	x0, x1, x0
.L1653:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1655
	mov	x1, #72
	add	x0, x0, x1
	b	.L1653
.L1655:
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
.L1656:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1648
	mov	x1, #56
	add	x0, x0, x1
.L1658:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1660
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L1658
.L1660:
	mov	x1, #-48
	add	x0, x0, x1
.L1661:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1669
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
.L1663:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1665
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1663
.L1665:
	mov	x1, #56
	add	x0, x0, x1
.L1666:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1668
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1666
.L1668:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1661
.L1669:
	mov	x1, #-32
	add	x0, x0, x1
	mov	x1, #-48
	add	x0, x0, x1
	b	.L1656
.L1670:
	mov	x1, #56
	add	x0, x0, x1
.L1671:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1673
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1671
.L1673:
	mov	x1, #-56
	add	x0, x0, x1
.L1674:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1715
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1676:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1684
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
.L1678:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1680
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #32
	add	x0, x0, x1
	b	.L1678
.L1680:
	mov	x1, #-32
	add	x0, x0, x1
.L1681:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1683
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
	b	.L1681
.L1683:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1676
.L1684:
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
.L1685:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1690
	mov	x1, #40
	add	x0, x0, x1
.L1687:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1689
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1687
.L1689:
	mov	x1, #-112
	add	x0, x0, x1
	b	.L1685
.L1690:
	mov	x1, #72
	add	x0, x0, x1
.L1691:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1693
	mov	x1, #72
	add	x0, x0, x1
	b	.L1691
.L1693:
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
.L1694:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1711
	mov	x2, #8
	add	x0, x0, x2
.L1696:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1698
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1696
.L1698:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
.L1699:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1707
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1701:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1703
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1701
.L1703:
	mov	x1, #-8
	add	x0, x0, x1
.L1704:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1706
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1704
.L1706:
	mov	x1, #56
	add	x0, x0, x1
	b	.L1699
.L1707:
	mov	x1, #-48
	add	x0, x0, x1
.L1708:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1710
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	b	.L1708
.L1710:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1694
.L1711:
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-32
	add	x0, x0, x1
.L1712:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1714
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1712
.L1714:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1674
.L1715:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
.L1716:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1718
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1716
.L1718:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
.L1719:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1820
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1721:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1726
	mov	x1, #16
	add	x0, x0, x1
	mov	x1, #24
	add	x0, x0, x1
.L1723:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1725
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1723
.L1725:
	mov	x1, #32
	add	x0, x0, x1
	b	.L1721
.L1726:
	mov	x1, #-72
	add	x0, x0, x1
.L1727:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1744
	mov	x2, #8
	add	x0, x0, x2
.L1729:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1731
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1729
.L1731:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
.L1732:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1740
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1734:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1736
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #24
	add	x0, x0, x1
	b	.L1734
.L1736:
	mov	x1, #-8
	add	x0, x0, x1
.L1737:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1739
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1737
.L1739:
	mov	x1, #56
	add	x0, x0, x1
	b	.L1732
.L1740:
	mov	x1, #-16
	add	x0, x0, x1
	mov	x1, #-32
	add	x0, x0, x1
.L1741:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1743
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	b	.L1741
.L1743:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1727
.L1744:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
.L1745:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1750
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L1747:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1749
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1747
.L1749:
	mov	x1, #72
	add	x0, x0, x1
	b	.L1745
.L1750:
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
.L1751:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1753
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1751
.L1753:
	mov	x1, #72
	add	x0, x0, x1
.L1754:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1805
	mov	x1, #40
	add	x0, x0, x1
.L1756:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1758
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L1756
.L1758:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
.L1759:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1775
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1761:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1763
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1761
.L1763:
	mov	x1, #-32
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
.L1764:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1759
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-128
	add	x0, x0, x1
.L1766:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1768
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1766
.L1768:
	mov	x1, #32
	add	x0, x0, x1
.L1769:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1771
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1769
.L1771:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1772:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1774
	mov	x1, #72
	add	x0, x0, x1
	b	.L1772
.L1774:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1764
.L1775:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
.L1776:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1778
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	b	.L1776
.L1778:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
.L1779:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1798
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1781:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1783
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L1781
.L1783:
	mov	x1, #-40
	add	x0, x0, x1
.L1784:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1779
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-112
	add	x0, x0, x1
.L1786:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1788
	mov	x1, #-24
	add	x0, x0, x1
	mov	x1, #-48
	add	x0, x0, x1
	b	.L1786
.L1788:
	mov	x1, #24
	add	x0, x0, x1
.L1789:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1791
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1789
.L1791:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L1792:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1794
	mov	x1, #72
	add	x0, x0, x1
	b	.L1792
.L1794:
	mov	x1, #8
	add	x0, x0, x1
.L1795:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1797
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1795
.L1797:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1784
.L1798:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1799:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1804
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1801:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1803
	mov	x1, #72
	add	x0, x0, x1
	b	.L1801
.L1803:
	mov	x1, #-64
	add	x0, x0, x1
	b	.L1799
.L1804:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1754
.L1805:
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
.L1806:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1808
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1806
.L1808:
	mov	x1, #32
	add	x0, x0, x1
.L1809:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1811
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1809
.L1811:
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
.L1812:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1817
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L1814:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1816
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1814
.L1816:
	mov	x1, #72
	add	x0, x0, x1
	b	.L1812
.L1817:
	mov	x1, #32
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
.L1818:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1719
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1818
.L1820:
	mov	x1, #24
	add	x1, x0, x1
	b	.L65
.L1821:
	mov	x0, #-32
	add	x19, x1, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #80
	add	x0, x19, x0
.L1822:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1827
	mov	x1, #48
	add	x0, x0, x1
.L1824:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1826
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1824
.L1826:
	mov	x1, #24
	add	x0, x0, x1
	b	.L1822
.L1827:
	mov	x1, #-72
	add	x0, x0, x1
.L1828:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1830
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1828
.L1830:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L1831:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1836
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L1833:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1835
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1833
.L1835:
	mov	x1, #72
	add	x0, x0, x1
	b	.L1831
.L1836:
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-120
	add	x0, x0, x1
.L1837:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1839
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1837
.L1839:
	mov	x1, #64
	add	x0, x0, x1
.L1840:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1842
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1840
.L1842:
	mov	x1, #-64
	add	x0, x0, x1
.L1843:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1865
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x1, x0, x1
	ldr	x0, [x1]
	mov	x2, #1
	add	x0, x0, x2
	str	x0, [x1]
.L1845:
	cmp	w0, #0
	beq	.L1847
	mov	x2, #-1
	add	x0, x0, x2
	str	x0, [x1]
	b	.L1845
.L1847:
	mov	x0, #8
	add	x0, x1, x0
.L1848:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1850
	mov	x1, #72
	add	x0, x0, x1
	b	.L1848
.L1850:
	mov	x1, #-72
	add	x0, x0, x1
.L1851:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1843
	mov	x1, #64
	add	x0, x0, x1
.L1853:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1855
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	mov	x1, #8
	add	x0, x0, x1
	b	.L1853
.L1855:
	mov	x1, #-56
	add	x0, x0, x1
.L1856:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1864
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
.L1858:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1860
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1858
.L1860:
	mov	x1, #64
	add	x0, x0, x1
.L1861:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1863
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1861
.L1863:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1856
.L1864:
	mov	x1, #-80
	add	x0, x0, x1
	b	.L1851
.L1865:
	mov	x1, #64
	add	x0, x0, x1
.L1866:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1868
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1866
.L1868:
	mov	x1, #-64
	add	x0, x0, x1
.L1869:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1910
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1871:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1879
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1873:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1875
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	b	.L1873
.L1875:
	mov	x1, #-40
	add	x0, x0, x1
.L1876:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1878
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
	b	.L1876
.L1878:
	mov	x1, #48
	add	x0, x0, x1
	mov	x1, #16
	add	x0, x0, x1
	b	.L1871
.L1879:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
.L1880:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1885
	mov	x1, #48
	add	x0, x0, x1
.L1882:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1884
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1882
.L1884:
	mov	x1, #-120
	add	x0, x0, x1
	b	.L1880
.L1885:
	mov	x1, #72
	add	x0, x0, x1
.L1886:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1888
	mov	x1, #72
	add	x0, x0, x1
	b	.L1886
.L1888:
	mov	x1, #-72
	add	x0, x0, x1
.L1889:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1906
	mov	x2, #8
	add	x0, x0, x2
.L1891:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1893
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1891
.L1893:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
.L1894:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1902
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1896:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1898
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1896
.L1898:
	mov	x1, #-8
	add	x0, x0, x1
.L1899:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1901
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1899
.L1901:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1894
.L1902:
	mov	x1, #-56
	add	x0, x0, x1
.L1903:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1905
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	b	.L1903
.L1905:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1889
.L1906:
	mov	x1, #64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-40
	add	x0, x0, x1
.L1907:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1909
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1907
.L1909:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-24
	add	x0, x0, x1
	b	.L1869
.L1910:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
.L1911:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1913
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1911
.L1913:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
.L1914:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2015
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1916:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1921
	mov	x1, #24
	add	x0, x0, x1
	mov	x1, #24
	add	x0, x0, x1
.L1918:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1920
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1918
.L1920:
	mov	x1, #24
	add	x0, x0, x1
	b	.L1916
.L1921:
	mov	x1, #-72
	add	x0, x0, x1
.L1922:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1939
	mov	x2, #8
	add	x0, x0, x2
.L1924:
	ldr	x2, [x0]
	cmp	w2, #0
	beq	.L1926
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	b	.L1924
.L1926:
	mov	x2, #-8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
.L1927:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1935
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1929:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1931
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #-1
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	b	.L1929
.L1931:
	mov	x1, #-8
	add	x0, x0, x1
.L1932:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1934
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1932
.L1934:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1927
.L1935:
	mov	x1, #-16
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
.L1936:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1938
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #56
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-56
	add	x0, x0, x1
	b	.L1936
.L1938:
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1922
.L1939:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
.L1940:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1945
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L1942:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1944
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1942
.L1944:
	mov	x1, #72
	add	x0, x0, x1
	b	.L1940
.L1945:
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #216
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
.L1946:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1948
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1946
.L1948:
	mov	x1, #72
	add	x0, x0, x1
.L1949:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2000
	mov	x1, #48
	add	x0, x0, x1
.L1951:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1953
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L1951
.L1953:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
.L1954:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1970
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
.L1956:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1958
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1956
.L1958:
	mov	x1, #-64
	add	x0, x0, x1
.L1959:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1954
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-136
	add	x0, x0, x1
.L1961:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1963
	mov	x1, #-56
	add	x0, x0, x1
	mov	x1, #-16
	add	x0, x0, x1
	b	.L1961
.L1963:
	mov	x1, #32
	add	x0, x0, x1
.L1964:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1966
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1964
.L1966:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #40
	add	x0, x0, x1
.L1967:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1969
	mov	x1, #72
	add	x0, x0, x1
	b	.L1967
.L1969:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1959
.L1970:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
.L1971:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1973
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	b	.L1971
.L1973:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-64
	add	x0, x0, x1
.L1974:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1993
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #64
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-16
	add	x0, x0, x1
.L1976:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1978
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	b	.L1976
.L1978:
	mov	x1, #-48
	add	x0, x0, x1
.L1979:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1974
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-120
	add	x0, x0, x1
.L1981:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1983
	mov	x1, #-72
	add	x0, x0, x1
	b	.L1981
.L1983:
	mov	x1, #24
	add	x0, x0, x1
.L1984:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1986
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1984
.L1986:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #48
	add	x0, x0, x1
.L1987:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1989
	mov	x1, #48
	add	x0, x0, x1
	mov	x1, #24
	add	x0, x0, x1
	b	.L1987
.L1989:
	mov	x1, #8
	add	x0, x0, x1
.L1990:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1992
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1990
.L1992:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	b	.L1979
.L1993:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
.L1994:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1999
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
.L1996:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1998
	mov	x1, #72
	add	x0, x0, x1
	b	.L1996
.L1998:
	mov	x1, #-64
	add	x0, x0, x1
	b	.L1994
.L1999:
	mov	x1, #64
	add	x0, x0, x1
	b	.L1949
.L2000:
	mov	x1, #-72
	add	x0, x0, x1
.L2001:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2003
	mov	x1, #-72
	add	x0, x0, x1
	b	.L2001
.L2003:
	mov	x1, #32
	add	x0, x0, x1
.L2004:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2006
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L2004
.L2006:
	mov	x1, #-24
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #4
	add	x1, x1, x2
	str	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L2007:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2012
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L2009:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L2011
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #72
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-72
	add	x0, x0, x1
	b	.L2009
.L2011:
	mov	x1, #72
	add	x0, x0, x1
	b	.L2007
.L2012:
	mov	x1, #40
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #216
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-48
	add	x0, x0, x1
.L2013:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L1914
	mov	x1, #-32
	add	x0, x0, x1
	mov	x1, #-40
	add	x0, x0, x1
	b	.L2013
.L2015:
	mov	x1, #24
	add	x0, x0, x1
	b	.L36
.L2016:
	mov	w0, #0
	ldr	x19, [x29, 4120]
	ldp	x29, x30, [sp], 16
	mov	x16, #4112
	add	sp, sp, x16
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
