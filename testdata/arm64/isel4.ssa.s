.text
.balign 16
.globl f0
f0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #2
	add	x1, x1, x2
	mov	x2, #4
	mul	x1, x1, x2
	add	x0, x0, x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f0, @function
.size f0, .-f0
/* end function f0 */

.text
.balign 16
.globl f1
f1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #1
	add	x1, x2, x1
	mov	x2, #1
	add	x1, x1, x2
	mov	x2, #4
	mul	x1, x1, x2
	add	x0, x1, x0
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #4
	mul	x2, x1, x2
	mov	x1, #8
	add	x1, x1, x2
	add	x0, x0, x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #4
	mul	x2, x1, x2
	mov	x1, #4
	add	x2, x1, x2
	mov	x1, #4
	add	x1, x1, x2
	add	x0, x0, x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #1
	add	x1, x2, x1
	mov	x2, #4
	mul	x2, x1, x2
	mov	x1, #4
	add	x1, x1, x2
	add	x0, x1, x0
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.section .note.GNU-stack,"",@progbits
