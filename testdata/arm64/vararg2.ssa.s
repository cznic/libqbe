.text
.balign 16
.globl qbeprint0
qbeprint0:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w1, #25637
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #20
	mov	w1, #26405
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #28
	mov	w1, #0
	str	w1, [x2]
	mov	x1, #1
	add	x19, x1, x0
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-128
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-56
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L1:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L11
	cmp	w0, #103
	beq	.L7
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L5
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L6
.L5:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L6:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L1
.L7:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L9
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L10
.L9:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L10:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L1
.L11:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint0, @function
.size qbeprint0, .-qbeprint0
/* end function qbeprint0 */

.text
.balign 16
.globl qbecall0
qbecall0:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-128
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-56
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall0, @function
.size qbecall0, .-qbecall0
/* end function qbecall0 */

.text
.balign 16
.globl qbeprint1
qbeprint1:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x2]
	add	x2, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x2]
	add	x2, x29, #28
	mov	w0, #0
	str	w0, [x2]
	mov	x0, #1
	add	x19, x0, x1
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-128
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-48
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L16:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L26
	cmp	w0, #103
	beq	.L22
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L20
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L21
.L20:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L21:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L16
.L22:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L24
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L25
.L24:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L25:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L16
.L26:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint1, @function
.size qbeprint1, .-qbeprint1
/* end function qbeprint1 */

.text
.balign 16
.globl qbecall1
qbecall1:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x0, x1
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-128
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-48
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall1, @function
.size qbecall1, .-qbecall1
/* end function qbecall1 */

.text
.balign 16
.globl qbeprint2
qbeprint2:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w1, #25637
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #20
	mov	w1, #26405
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #28
	mov	w1, #0
	str	w1, [x2]
	mov	x1, #1
	add	x19, x1, x0
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-112
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-56
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L31:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L41
	cmp	w0, #103
	beq	.L37
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L35
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L36
.L35:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L36:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L31
.L37:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L39
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L40
.L39:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L40:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L31
.L41:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint2, @function
.size qbeprint2, .-qbeprint2
/* end function qbeprint2 */

.text
.balign 16
.globl qbecall2
qbecall2:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-112
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-56
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall2, @function
.size qbecall2, .-qbecall2
/* end function qbecall2 */

.text
.balign 16
.globl qbeprint3
qbeprint3:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x4
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-128
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-24
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L46:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L56
	cmp	w0, #103
	beq	.L52
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L50
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L51
.L50:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L51:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L46
.L52:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L54
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L55
.L54:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L55:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L46
.L56:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint3, @function
.size qbeprint3, .-qbeprint3
/* end function qbeprint3 */

.text
.balign 16
.globl qbecall3
qbecall3:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x0, x4
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-128
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-24
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall3, @function
.size qbecall3, .-qbecall3
/* end function qbecall3 */

.text
.balign 16
.globl qbeprint4
qbeprint4:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w1, #25637
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #20
	mov	w1, #26405
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #28
	mov	w1, #0
	str	w1, [x2]
	mov	x1, #1
	add	x19, x1, x0
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-32
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-56
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L61:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L71
	cmp	w0, #103
	beq	.L67
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L65
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L66
.L65:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L66:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L61
.L67:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L69
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L70
.L69:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L70:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L61
.L71:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint4, @function
.size qbeprint4, .-qbeprint4
/* end function qbeprint4 */

.text
.balign 16
.globl qbecall4
qbecall4:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-32
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-56
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall4, @function
.size qbecall4, .-qbecall4
/* end function qbecall4 */

.text
.balign 16
.globl qbeprint5
qbeprint5:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x5
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-16
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-16
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L76:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L86
	cmp	w0, #103
	beq	.L82
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L80
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L81
.L80:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L81:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L76
.L82:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L84
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L85
.L84:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L85:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L76
.L86:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint5, @function
.size qbeprint5, .-qbeprint5
/* end function qbeprint5 */

.text
.balign 16
.globl qbecall5
qbecall5:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x0, x5
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-16
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-16
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall5, @function
.size qbecall5, .-qbecall5
/* end function qbecall5 */

.text
.balign 16
.globl qbeprint6
qbeprint6:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	ldr	x8, [x29, 304]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x8
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #0
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #0
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #232
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L91:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L101
	cmp	w0, #103
	beq	.L97
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L95
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L96
.L95:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L96:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L91
.L97:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L99
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L100
.L99:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L100:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L91
.L101:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint6, @function
.size qbeprint6, .-qbeprint6
/* end function qbeprint6 */

.text
.balign 16
.globl qbecall6
qbecall6:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	ldr	x8, [x29, 272]
	mov	x0, x8
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #0
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #0
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #232
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall6, @function
.size qbecall6, .-qbecall6
/* end function qbecall6 */

.text
.balign 16
.globl qbeprint7
qbeprint7:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	ldr	x8, [x29, 280]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x8
	mov	x1, #28
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #-128
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #32
	add	x1, x0, x1
	mov	w0, #0
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #32
	add	x2, x0, x1
	add	x0, x29, #80
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #32
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #208
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
.L106:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	.L116
	cmp	w0, #103
	beq	.L112
	mov	x1, #24
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L110
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L111
.L110:
	mov	x2, #8
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L111:
	ldr	w1, [x0]
	add	x0, x29, #24
	bl	printf
	b	.L106
.L112:
	mov	x1, #28
	add	x0, x29, #32
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L114
	add	x0, x29, #32
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #32
	str	x1, [x2]
	b	.L115
.L114:
	mov	x2, #16
	add	x0, x29, #32
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #32
	add	x2, x2, x3
	str	w1, [x2]
.L115:
	ldr	d0, [x0]
	add	x0, x29, #20
	bl	printf
	b	.L106
.L116:
	add	x0, x29, #28
	bl	puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 272
	ret
.type qbeprint7, @function
.size qbeprint7, .-qbeprint7
/* end function qbeprint7 */

.text
.balign 16
.globl qbecall7
qbecall7:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	ldr	x8, [x29, 248]
	mov	x0, x8
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-128
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #0
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #208
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type qbecall7, @function
.size qbecall7, .-qbecall7
/* end function qbecall7 */

.section .note.GNU-stack,"",@progbits
