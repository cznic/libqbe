.data
.balign 8
arr:
	.byte 10
	.byte -60
	.byte 10
	.byte 100
	.byte 200
	.byte 0
/* end data */

.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, arr
	add	x1, x1, #:lo12:arr
	mov	w0, #-1
.L2:
	ldrb	w2, [x1]
	mov	x3, #1
	add	x1, x3, x1
	cmp	w2, #0
	beq	.L5
	cmp	w0, w2
	bgt	.L2
	mov	w0, w2
	b	.L2
.L5:
	adrp	x1, a
	add	x1, x1, #:lo12:a
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
