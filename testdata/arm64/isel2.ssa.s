.text
.balign 16
.globl lt
lt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, mi
	ldp	x29, x30, [sp], 16
	ret
.type lt, @function
.size lt, .-lt
/* end function lt */

.text
.balign 16
.globl le
le:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ls
	ldp	x29, x30, [sp], 16
	ret
.type le, @function
.size le, .-le
/* end function le */

.text
.balign 16
.globl gt
gt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, gt
	ldp	x29, x30, [sp], 16
	ret
.type gt, @function
.size gt, .-gt
/* end function gt */

.text
.balign 16
.globl ge
ge:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ge
	ldp	x29, x30, [sp], 16
	ret
.type ge, @function
.size ge, .-ge
/* end function ge */

.text
.balign 16
.globl eq1
eq1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, eq
	ldp	x29, x30, [sp], 16
	ret
.type eq1, @function
.size eq1, .-eq1
/* end function eq1 */

.text
.balign 16
.globl eq2
eq2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	beq	.L12
	mov	w0, #0
	b	.L13
.L12:
	mov	w0, #1
.L13:
	ldp	x29, x30, [sp], 16
	ret
.type eq2, @function
.size eq2, .-eq2
/* end function eq2 */

.text
.balign 16
.globl eq3
eq3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, eq
	cmp	w0, #0
	bne	.L16
	mov	w0, #0
.L16:
	ldp	x29, x30, [sp], 16
	ret
.type eq3, @function
.size eq3, .-eq3
/* end function eq3 */

.text
.balign 16
.globl ne1
ne1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ne
	ldp	x29, x30, [sp], 16
	ret
.type ne1, @function
.size ne1, .-ne1
/* end function ne1 */

.text
.balign 16
.globl ne2
ne2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	bne	.L21
	mov	w0, #0
	b	.L22
.L21:
	mov	w0, #1
.L22:
	ldp	x29, x30, [sp], 16
	ret
.type ne2, @function
.size ne2, .-ne2
/* end function ne2 */

.text
.balign 16
.globl ne3
ne3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ne
	cmp	w0, #0
	bne	.L25
	mov	w0, #0
.L25:
	ldp	x29, x30, [sp], 16
	ret
.type ne3, @function
.size ne3, .-ne3
/* end function ne3 */

.text
.balign 16
.globl o
o:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, vc
	ldp	x29, x30, [sp], 16
	ret
.type o, @function
.size o, .-o
/* end function o */

.text
.balign 16
.globl uo
uo:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, vs
	ldp	x29, x30, [sp], 16
	ret
.type uo, @function
.size uo, .-uo
/* end function uo */

.section .note.GNU-stack,"",@progbits
