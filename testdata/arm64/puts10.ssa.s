.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	mov	x1, #1
	add	x0, x29, #28
	add	x1, x0, x1
	mov	w0, #0
	strb	w0, [x1]
	mov	w19, #0
.L2:
	mov	w0, #48
	add	w0, w19, w0
	add	x1, x29, #28
	strb	w0, [x1]
	add	x0, x29, #28
	bl	puts
	mov	w0, #1
	add	w19, w19, w0
	cmp	w19, #9
	ble	.L2
	mov	w0, #0
	ldr	x19, [x29, 40]
	ldp	x29, x30, [sp], 48
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
