.text
.balign 16
.globl f
f:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	add	x2, x29, #24
	mov	w1, #0
	str	w1, [x2]
	cmp	w0, #0
	bne	.L2
	add	x0, x29, #28
	b	.L3
.L2:
	add	x0, x29, #24
.L3:
	mov	x1, x0
	mov	w0, #1
	str	w0, [x1]
	add	x0, x29, #24
	ldr	w0, [x0]
	ldp	x29, x30, [sp], 32
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
