.text
.balign 16
.globl blit
blit:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	add	x1, x29, #20
	mov	w0, #13124
	movk	w0, #0x1122, lsl #16
	str	w0, [x1]
	mov	x1, #4
	add	x0, x29, #20
	add	x1, x0, x1
	mov	w0, #30600
	movk	w0, #0x5566, lsl #16
	str	w0, [x1]
	mov	x1, #8
	add	x0, x29, #20
	add	x1, x0, x1
	mov	w0, #48076
	movk	w0, #0x99aa, lsl #16
	str	w0, [x1]
	mov	x1, #1
	add	x0, x29, #20
	add	x1, x0, x1
	mov	x2, #10
	add	x0, x29, #20
	add	x0, x0, x2
	ldrb	w0, [x0]
	mov	x2, #10
	add	x2, x1, x2
	strb	w0, [x2]
	mov	x2, #8
	add	x0, x29, #20
	add	x0, x0, x2
	ldrh	w0, [x0]
	mov	x2, #8
	add	x2, x1, x2
	strh	w0, [x2]
	mov	x2, #0
	add	x0, x29, #20
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #0
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #20
	mov	w0, #221
	strb	w0, [x1]
	mov	x1, #8
	add	x0, x29, #20
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #0
	add	x0, x29, #20
	add	x0, x0, x2
	ldr	x0, [x0]
	ldp	x29, x30, [sp], 32
	ret
.type blit, @function
.size blit, .-blit
/* end function blit */

.section .note.GNU-stack,"",@progbits
