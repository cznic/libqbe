.section .tdata,"awT"
.balign 4
i:
	.int 42
/* end data */

.data
.balign 1
fmti:
	.ascii "i%d==%d\n"
	.byte 0
/* end data */

.section .tdata,"awT"
.balign 8
x:
	.int 1
	.int 2
	.int 3
	.int 4
/* end data */

.data
.balign 1
fmtx:
	.ascii "*(x+%d)==%d\n"
	.byte 0
/* end data */

.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	mov	x3, #0
	adrp	x2, thread
	add	x2, x2, #:lo12:thread
	mov	x1, #0
	add	x0, x29, #16
	bl	pthread_create
	add	x0, x29, #16
	ldr	x0, [x0]
	add	x1, x29, #24
	bl	pthread_join
	mrs	x0, tpidr_el0
	add	x0, x0, #:tprel_hi12:i, lsl #12
	add	x0, x0, #:tprel_lo12_nc:i
	ldr	w2, [x0]
	mov	w1, #0
	adrp	x0, fmti
	add	x0, x0, #:lo12:fmti
	bl	printf
	add	x0, x29, #24
	ldr	w2, [x0]
	mov	w1, #1
	adrp	x0, fmti
	add	x0, x0, #:lo12:fmti
	bl	printf
	bl	xaddr
	ldr	w2, [x0]
	mov	w1, #0
	adrp	x0, fmtx
	add	x0, x0, #:lo12:fmtx
	bl	printf
	bl	xaddroff4
	ldr	w2, [x0]
	mov	w1, #4
	adrp	x0, fmtx
	add	x0, x0, #:lo12:fmtx
	bl	printf
	mov	x0, #8
	bl	xaddroff
	ldr	w2, [x0]
	mov	w1, #8
	adrp	x0, fmtx
	add	x0, x0, #:lo12:fmtx
	bl	printf
	mov	x0, #3
	bl	xvalcnt
	mov	x2, x0
	mov	w1, #12
	adrp	x0, fmtx
	add	x0, x0, #:lo12:fmtx
	bl	printf
	mov	w0, #0
	ldp	x29, x30, [sp], 32
	ret
.type main, @function
.size main, .-main
/* end function main */

.text
.balign 16
thread:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mrs	x1, tpidr_el0
	add	x1, x1, #:tprel_hi12:i+3, lsl #12
	add	x1, x1, #:tprel_lo12_nc:i+3
	mov	w0, #24
	strb	w0, [x1]
	mrs	x0, tpidr_el0
	add	x0, x0, #:tprel_hi12:i, lsl #12
	add	x0, x0, #:tprel_lo12_nc:i
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type thread, @function
.size thread, .-thread
/* end function thread */

.text
.balign 16
xaddr:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mrs	x0, tpidr_el0
	add	x0, x0, #:tprel_hi12:x, lsl #12
	add	x0, x0, #:tprel_lo12_nc:x
	ldp	x29, x30, [sp], 16
	ret
.type xaddr, @function
.size xaddr, .-xaddr
/* end function xaddr */

.text
.balign 16
xaddroff4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mrs	x0, tpidr_el0
	add	x0, x0, #:tprel_hi12:x+4, lsl #12
	add	x0, x0, #:tprel_lo12_nc:x+4
	ldp	x29, x30, [sp], 16
	ret
.type xaddroff4, @function
.size xaddroff4, .-xaddroff4
/* end function xaddroff4 */

.text
.balign 16
xaddroff:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mrs	x1, tpidr_el0
	add	x1, x1, #:tprel_hi12:x, lsl #12
	add	x1, x1, #:tprel_lo12_nc:x
	add	x0, x1, x0
	ldp	x29, x30, [sp], 16
	ret
.type xaddroff, @function
.size xaddroff, .-xaddroff
/* end function xaddroff */

.text
.balign 16
xvalcnt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #4
	mul	x1, x1, x0
	mrs	x0, tpidr_el0
	add	x0, x0, #:tprel_hi12:x, lsl #12
	add	x0, x0, #:tprel_lo12_nc:x
	add	x0, x0, x1
	ldr	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type xvalcnt, @function
.size xvalcnt, .-xvalcnt
/* end function xvalcnt */

.section .note.GNU-stack,"",@progbits
