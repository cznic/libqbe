.data
.balign 8
fmt1:
	.ascii "t1: %s\n"
	.byte 0
/* end data */

.data
.balign 8
fmt2:
	.ascii "t2: %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt3:
	.ascii "t3: %f %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt4:
	.ascii "t4: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmt5:
	.ascii "t5: %f %lld\n"
	.byte 0
/* end data */

.data
.balign 8
fmt6:
	.ascii "t6: %s\n"
	.byte 0
/* end data */

.data
.balign 8
fmt7:
	.ascii "t7: %f %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmt8:
	.ascii "t8: %d %d %d %d\n"
	.byte 0
/* end data */

.data
.balign 8
fmt9:
	.ascii "t9: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmta:
	.ascii "ta: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
fmtb:
	.ascii "tb: %d %d %f\n"
	.byte 0
/* end data */

.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -160]!
	mov	x29, sp
	add	x8, x29, #92
	bl	t1
	add	x1, x29, #92
	adrp	x0, fmt1
	add	x0, x0, #:lo12:fmt1
	bl	printf
	bl	t2
	mov	x2, #0
	add	x1, x29, #84
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #84
	ldr	w1, [x0]
	adrp	x0, fmt2
	add	x0, x0, #:lo12:fmt2
	bl	printf
	bl	t3
	mov	x2, #0
	add	x1, x29, #76
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #76
	ldr	s0, [x0]
	mov	x1, #4
	add	x0, x29, #76
	add	x0, x0, x1
	ldr	w1, [x0]
	fcvt	d0, s0
	adrp	x0, fmt3
	add	x0, x0, #:lo12:fmt3
	bl	printf
	bl	t4
	mov	x3, #8
	add	x2, x29, #144
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #144
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #144
	ldr	w1, [x0]
	add	x2, x29, #144
	mov	x0, #8
	add	x0, x0, x2
	ldr	d0, [x0]
	adrp	x0, fmt4
	add	x0, x0, #:lo12:fmt4
	bl	printf
	bl	t5
	mov	x3, #8
	add	x2, x29, #128
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #128
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #128
	ldr	s0, [x0]
	fcvt	d0, s0
	mov	x1, #8
	add	x0, x29, #128
	add	x0, x0, x1
	ldr	x1, [x0]
	adrp	x0, fmt5
	add	x0, x0, #:lo12:fmt5
	bl	printf
	bl	t6
	mov	x3, #8
	add	x2, x29, #60
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #60
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #60
	adrp	x0, fmt6
	add	x0, x0, #:lo12:fmt6
	bl	printf
	bl	t7
	mov	x3, #8
	add	x2, x29, #112
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #112
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #112
	ldr	s0, [x0]
	fcvt	d0, s0
	mov	x1, #8
	add	x0, x29, #112
	add	x0, x0, x1
	ldr	d1, [x0]
	adrp	x0, fmt7
	add	x0, x0, #:lo12:fmt7
	bl	printf
	bl	t8
	mov	x3, #8
	add	x2, x29, #44
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #44
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #44
	mov	x0, #4
	add	x2, x0, x1
	mov	x0, #4
	add	x3, x0, x2
	mov	x0, #4
	add	x0, x0, x3
	add	x1, x29, #44
	ldr	w1, [x1]
	ldr	w2, [x2]
	ldr	w3, [x3]
	ldr	w4, [x0]
	adrp	x0, fmt8
	add	x0, x0, #:lo12:fmt8
	bl	printf
	bl	t9
	mov	x2, #0
	add	x1, x29, #36
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #36
	mov	x0, #4
	add	x0, x0, x1
	add	x1, x29, #36
	ldr	w1, [x1]
	ldr	s0, [x0]
	fcvt	d0, s0
	adrp	x0, fmt9
	add	x0, x0, #:lo12:fmt9
	bl	printf
	bl	ta
	mov	x2, #0
	add	x1, x29, #28
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #28
	mov	x0, #4
	add	x0, x0, x1
	add	x1, x29, #28
	ldrsb	w1, [x1]
	ldr	s0, [x0]
	fcvt	d0, s0
	adrp	x0, fmta
	add	x0, x0, #:lo12:fmta
	bl	printf
	bl	tb
	mov	x2, #0
	add	x1, x29, #20
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #20
	mov	x0, #1
	add	x2, x0, x1
	add	x1, x29, #20
	mov	x0, #4
	add	x0, x0, x1
	add	x1, x29, #20
	ldrsb	w1, [x1]
	ldrsb	w2, [x2]
	ldr	s0, [x0]
	fcvt	d0, s0
	adrp	x0, fmtb
	add	x0, x0, #:lo12:fmtb
	bl	printf
	ldp	x29, x30, [sp], 160
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
