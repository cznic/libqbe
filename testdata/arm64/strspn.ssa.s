.text
.balign 16
.globl strspn_
strspn_:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, x1
	mov	w4, #0
.L2:
	mov	x1, x0
	mov	x0, #1
	add	x0, x1, x0
	ldrsb	w3, [x1]
	cmp	w3, #0
	beq	.L10
	mov	x1, x2
.L4:
	ldrsb	w5, [x1]
	cmp	w5, #0
	beq	.L7
	cmp	w5, w3
	beq	.L7
	mov	x5, #1
	add	x1, x1, x5
	b	.L4
.L7:
	cmp	w5, #0
	beq	.L9
	mov	w1, #1
	add	w4, w4, w1
	b	.L2
.L9:
	mov	w0, w4
	b	.L11
.L10:
	mov	w0, w4
.L11:
	ldp	x29, x30, [sp], 16
	ret
.type strspn_, @function
.size strspn_, .-strspn_
/* end function strspn_ */

.section .note.GNU-stack,"",@progbits
