.text
.balign 16
.globl f
f:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	add	x1, x29, #20
	mov	x0, #4
	add	x19, x0, x1
	add	x1, x29, #20
	mov	w0, #30600
	movk	w0, #0x5566, lsl #16
	str	w0, [x1]
	mov	w0, #34884
	movk	w0, #0x1122, lsl #16
	bl	px
	add	x1, x29, #20
	mov	w0, #30600
	movk	w0, #0x5566, lsl #16
	str	w0, [x1]
	mov	w0, #34884
	movk	w0, #0x1177, lsl #16
	bl	px
	add	x1, x29, #20
	mov	w0, #30600
	movk	w0, #0x5566, lsl #16
	str	w0, [x1]
	mov	w0, #34884
	movk	w0, #0x6677, lsl #16
	bl	px
	add	x1, x29, #20
	mov	w0, #30600
	movk	w0, #0x5566, lsl #16
	str	w0, [x1]
	mov	w0, #21862
	movk	w0, #0x1122, lsl #16
	bl	px
	add	x1, x29, #20
	mov	w0, #0
	str	w0, [x1]
	mov	w0, #30600
	movk	w0, #0x5566, lsl #16
	str	w0, [x19]
	mov	w0, #34816
	movk	w0, #0x6677, lsl #16
	bl	px
	ldr	x19, [x29, 40]
	ldp	x29, x30, [sp], 48
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
