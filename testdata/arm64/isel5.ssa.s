.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, ".Lfp0"
	add	x0, x0, #:lo12:".Lfp0"
	ldr	d0, [x0]
	adrp	x0, fmt
	add	x0, x0, #:lo12:fmt
	bl	printf
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
.type main, @function
.size main, .-main
/* end function main */

.data
.balign 8
fmt:
	.ascii "%.06f\n"
	.byte 0
/* end data */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 858993459
	.int 1072902963 /* 1.200000 */

.section .note.GNU-stack,"",@progbits
