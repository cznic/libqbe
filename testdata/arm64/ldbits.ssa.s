.text
.balign 16
.globl tests
tests:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, a
	add	x1, x1, #:lo12:a
	mov	w0, #1
	str	w0, [x1]
	adrp	x1, a
	add	x1, x1, #:lo12:a
	mov	w0, #2
	str	w0, [x1]
	adrp	x1, a
	add	x1, x1, #:lo12:a
	mov	w0, #3
	str	w0, [x1]
	adrp	x1, a
	add	x1, x1, #:lo12:a
	mov	w0, #0
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
.type tests, @function
.size tests, .-tests
/* end function tests */

.section .note.GNU-stack,"",@progbits
