.text
.balign 16
.globl f1
f1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #-1
	ldp	x29, x30, [sp], 16
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #-32
	ldp	x29, x30, [sp], 16
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #-5
	ldp	x29, x30, [sp], 16
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.text
.balign 16
.globl f5
f5:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #1
	ldp	x29, x30, [sp], 16
	ret
.type f5, @function
.size f5, .-f5
/* end function f5 */

.text
.balign 16
.globl f6
f6:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
.type f6, @function
.size f6, .-f6
/* end function f6 */

.section .note.GNU-stack,"",@progbits
