.text
.balign 16
g:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ldp	x29, x30, [sp], 16
	ret
.type g, @function
.size g, .-g
/* end function g */

.text
.balign 16
f:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	mov	w19, w0
	bl	g
	mov	w0, w19
	mov	x1, #16
	sub	sp, sp, x1
	mov	x2, sp
	mov	x1, #42
	movk	x1, #0x0, lsl #16
	movk	x1, #0x2a, lsl #32
	str	x1, [x2]
	mov	x1, #8
	add	x2, x1, x2
	mov	x1, #42
	movk	x1, #0x0, lsl #16
	movk	x1, #0x2a, lsl #32
	str	x1, [x2]
	ldr	x19, [x29, 24]
	mov sp, x29
	ldp	x29, x30, [sp], 32
	ret
.type f, @function
.size f, .-f
/* end function f */

.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	mov	w0, #0
	bl	f
	mov	w19, w0
	mov	w0, #0
	bl	f
	mov	w0, w19
	ldr	x19, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
