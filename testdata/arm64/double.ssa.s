.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, ".Lfp0"
	add	x0, x0, #:lo12:".Lfp0"
	ldr	d0, [x0]
	mov	w0, #0
.L2:
	fadd	d0, d0, d0
	mov	w1, #1
	add	w0, w0, w1
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d1, [x1]
	fcmpe	d0, d1
	bls	.L2
	adrp	x1, a
	add	x1, x1, #:lo12:a
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 0
	.int 1016070144 /* 0.000000 */

.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 1072693248 /* 1.000000 */

.section .note.GNU-stack,"",@progbits
