.text
.balign 16
func:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x1, x29, #24
	mov	x0, #1
	str	x0, [x1]
	mov	x1, #8
	add	x0, x29, #24
	add	x1, x0, x1
	mov	x0, #2
	str	x0, [x1]
	mov	x1, #8
	add	x0, x29, #24
	add	x1, x0, x1
	mov	x2, #8
	add	x0, x29, #24
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #8
	add	x2, x1, x2
	str	x0, [x2]
	mov	x2, #0
	add	x0, x29, #24
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #0
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #0
	add	x0, x29, #24
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x1, #0
	add	x1, x8, x1
	str	x0, [x1]
	mov	x1, #8
	add	x0, x29, #24
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x8, x1
	str	x0, [x1]
	mov	x1, #16
	add	x0, x29, #24
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x1, #16
	add	x1, x8, x1
	str	x0, [x1]
	ldp	x29, x30, [sp], 48
	ret
.type func, @function
.size func, .-func
/* end function func */

.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x8, x29, #24
	bl	func
	mov	x1, #16
	add	x0, x29, #24
	add	x0, x0, x1
	ldr	x0, [x0]
	cmp	x0, #2
	beq	.L6
	bl	abort
.L6:
	mov	w0, #0
	ldp	x29, x30, [sp], 48
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
