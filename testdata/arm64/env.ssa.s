.text
.balign 16
epar:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	add	x0, x9, x0
	ldp	x29, x30, [sp], 16
	ret
.type epar, @function
.size epar, .-epar
/* end function epar */

.text
.balign 16
.globl earg
earg:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x9, x0
	mov	x0, x1
	bl	epar
	mov	x9, #113
	bl	labs
	ldp	x29, x30, [sp], 16
	ret
.type earg, @function
.size earg, .-earg
/* end function earg */

.section .note.GNU-stack,"",@progbits
