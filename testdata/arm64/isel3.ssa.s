.text
.balign 16
.globl slt
slt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, lt
	ldp	x29, x30, [sp], 16
	ret
.type slt, @function
.size slt, .-slt
/* end function slt */

.text
.balign 16
.globl sle
sle:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, le
	ldp	x29, x30, [sp], 16
	ret
.type sle, @function
.size sle, .-sle
/* end function sle */

.text
.balign 16
.globl sgt
sgt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, gt
	ldp	x29, x30, [sp], 16
	ret
.type sgt, @function
.size sgt, .-sgt
/* end function sgt */

.text
.balign 16
.globl sge
sge:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, ge
	ldp	x29, x30, [sp], 16
	ret
.type sge, @function
.size sge, .-sge
/* end function sge */

.text
.balign 16
.globl ult
ult:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, cc
	ldp	x29, x30, [sp], 16
	ret
.type ult, @function
.size ult, .-ult
/* end function ult */

.text
.balign 16
.globl ule
ule:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, ls
	ldp	x29, x30, [sp], 16
	ret
.type ule, @function
.size ule, .-ule
/* end function ule */

.text
.balign 16
.globl ugt
ugt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, hi
	ldp	x29, x30, [sp], 16
	ret
.type ugt, @function
.size ugt, .-ugt
/* end function ugt */

.text
.balign 16
.globl uge
uge:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, cs
	ldp	x29, x30, [sp], 16
	ret
.type uge, @function
.size uge, .-uge
/* end function uge */

.text
.balign 16
.globl eq
eq:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, eq
	ldp	x29, x30, [sp], 16
	ret
.type eq, @function
.size eq, .-eq
/* end function eq */

.text
.balign 16
.globl ne
ne:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, ne
	ldp	x29, x30, [sp], 16
	ret
.type ne, @function
.size ne, .-ne
/* end function ne */

.section .note.GNU-stack,"",@progbits
