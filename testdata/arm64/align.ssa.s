.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	mov	x1, #8
	add	x0, x29, #16
	add	x1, x0, x1
	mov	w0, #16
	sdiv	w18, w1, w0
	msub	w0, w18, w0, w1
	str	w0, [x1]
	adrp	x1, a
	add	x1, x1, #:lo12:a
	str	w0, [x1]
	ldp	x29, x30, [sp], 32
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
