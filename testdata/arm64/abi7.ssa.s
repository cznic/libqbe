.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #8
	adrp	x0, s
	add	x0, x0, #:lo12:s
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #0
	adrp	x0, s
	add	x0, x0, #:lo12:s
	add	x0, x0, x2
	ldr	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
