.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	add	x1, x29, #24
	mov	w0, #4
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #5
	str	w0, [x1]
	add	x0, x29, #24
.L1:
	ldr	w0, [x0]
	cmp	w0, #5
	add	x0, x29, #28
	bne	.L1
	mov	w0, #0
	ldp	x29, x30, [sp], 32
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
