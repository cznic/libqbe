.text
.balign 16
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, a
	add	x0, x0, #:lo12:a
	ldr	w0, [x0]
.L1:
	cmp	w0, w0
	ble	.L1
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
