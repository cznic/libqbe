.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w3, #5
	mov	w4, #11
	mov	w1, #12
.L2:
	mov	w0, w1
	cmp	w3, #201
	beq	.L17
	mov	w1, #2
	sdiv	w18, w0, w1
	msub	w1, w18, w1, w0
	cmp	w1, #0
	beq	.L5
	mov	w2, #1
	b	.L6
.L5:
	mov	w2, #0
.L6:
	mov	w1, #3
.L7:
	cmp	w1, w0
	bge	.L11
	sdiv	w18, w0, w1
	msub	w5, w18, w1, w0
	cmp	w5, #0
	beq	.L10
	mov	w5, #2
	add	w1, w1, w5
	b	.L7
.L10:
	mov	w2, #0
.L11:
	cmp	w2, #0
	bne	.L13
	mov	w1, w0
	mov	w0, w4
	b	.L15
.L13:
	mov	w1, w0
	mov	w0, #1
	add	w3, w3, w0
	mov	w0, w1
.L15:
	mov	w2, #1
	add	w1, w1, w2
	mov	w4, w0
	b	.L2
.L17:
	cmp	w4, #1229
	bne	.L19
	mov	w0, #0
	b	.L20
.L19:
	mov	w0, #1
.L20:
	ldp	x29, x30, [sp], 16
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
