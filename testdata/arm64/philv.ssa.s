.text
.balign 16
.globl t0
t0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #128
	mov	w2, #256
.L2:
	mov	w1, w2
	mov	w2, w0
	mov	w0, #1
	lsr	w0, w1, w0
	cmp	w0, #0
	bne	.L2
	mov	w0, w1
	ldp	x29, x30, [sp], 16
	ret
.type t0, @function
.size t0, .-t0
/* end function t0 */

.text
.balign 16
.globl t1
t1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w2, #256
	mov	w0, #128
.L7:
	mov	w1, w2
	mov	w2, w0
	mov	w0, #1
	lsr	w0, w1, w0
	cmp	w0, #0
	bne	.L7
	mov	w0, w1
	ldp	x29, x30, [sp], 16
	ret
.type t1, @function
.size t1, .-t1
/* end function t1 */

.section .note.GNU-stack,"",@progbits
