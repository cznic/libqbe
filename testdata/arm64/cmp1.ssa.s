.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, #1
	cset	w0, hi
	cmp	w0, #0
	beq	.L2
	mov	w0, #1
.L2:
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
