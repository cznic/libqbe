.data
.balign 8
dfmt:
	.ascii "double: %g\n"
	.byte 0
/* end data */

.text
.balign 16
.globl f
f:
	hint	#34
	stp	x29, x30, [sp, -64]!
	mov	x29, sp
	str	d8, [x29, 56]
	str	d9, [x29, 48]
	ldr	d8, [x29, 80]
	fmov	d31, d6
	fmov	d6, d0
	fmov	d0, d31
	fmov	d31, d5
	fmov	d5, d6
	fmov	d6, d31
	fmov	d31, d4
	fmov	d4, d5
	fmov	d5, d31
	fmov	d31, d3
	fmov	d3, d4
	fmov	d4, d31
	fmov	d31, d2
	fmov	d2, d3
	fmov	d3, d31
	fmov	d31, d1
	fmov	d1, d2
	fmov	d2, d31
	mov	x1, #8
	add	x0, x29, #32
	add	x0, x0, x1
	str	s6, [x0]
	mov	x1, #4
	add	x0, x29, #32
	add	x0, x0, x1
	str	s5, [x0]
	mov	x1, #0
	add	x0, x29, #32
	add	x0, x0, x1
	str	s4, [x0]
	mov	x1, #8
	add	x0, x29, #16
	add	x0, x0, x1
	str	s3, [x0]
	mov	x1, #4
	add	x0, x29, #16
	add	x0, x0, x1
	str	s2, [x0]
	mov	x1, #0
	add	x0, x29, #16
	add	x0, x0, x1
	str	s1, [x0]
	mov	x1, #8
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	s2, [x0]
	mov	x1, #4
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	add	x0, x29, #16
	add	x0, x0, x1
	fmov	d9, d0
	ldr	s0, [x0]
	bl	phfa3
	fmov	d0, d9
	mov	x1, #8
	add	x0, x29, #32
	add	x0, x0, x1
	ldr	s2, [x0]
	mov	x1, #4
	add	x0, x29, #32
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	add	x0, x29, #32
	add	x0, x0, x1
	fmov	d9, d0
	ldr	s0, [x0]
	bl	phfa3
	fmov	d0, d9
	mov	x1, #8
	add	x0, x29, #64
	add	x0, x0, x1
	ldr	s2, [x0]
	mov	x1, #4
	add	x0, x29, #64
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	add	x0, x29, #64
	add	x0, x0, x1
	fmov	d9, d0
	ldr	s0, [x0]
	bl	phfa3
	fmov	d0, d9
	adrp	x0, dfmt
	add	x0, x0, #:lo12:dfmt
	bl	printf
	fmov	d0, d8
	adrp	x0, dfmt
	add	x0, x0, #:lo12:dfmt
	bl	printf
	ldr	d8, [x29, 56]
	ldr	d9, [x29, 48]
	ldp	x29, x30, [sp], 64
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
