.text
.balign 16
.globl f0
f0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, a
	add	x1, x1, #:lo12:a
	add	x0, x1, x0
	ldrb	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f0, @function
.size f0, .-f0
/* end function f0 */

.text
.balign 16
.globl f1
f1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #10
	add	x0, x1, x0
	ldrb	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f1, @function
.size f1, .-f1
/* end function f1 */

.text
.balign 16
.globl f2
f2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #2
	mul	x1, x1, x2
	add	x1, x0, x1
	adrp	x0, a
	add	x0, x0, #:lo12:a
	add	x0, x0, x1
	ldrb	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f2, @function
.size f2, .-f2
/* end function f2 */

.text
.balign 16
.globl f3
f3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, a
	add	x1, x1, #:lo12:a
	add	x0, x0, x1
	ldp	x29, x30, [sp], 16
	ret
.type f3, @function
.size f3, .-f3
/* end function f3 */

.text
.balign 16
.globl f4
f4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, p
	add	x1, x1, #:lo12:p
	adrp	x0, p
	add	x0, x0, #:lo12:p
	str	x0, [x1]
	ldp	x29, x30, [sp], 16
	ret
.type f4, @function
.size f4, .-f4
/* end function f4 */

.text
.balign 16
.globl writeto0
writeto0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #0
	mov	x0, #0
	str	x0, [x1]
	ldp	x29, x30, [sp], 16
	ret
.type writeto0, @function
.size writeto0, .-writeto0
/* end function writeto0 */

.section .note.GNU-stack,"",@progbits
