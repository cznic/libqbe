.data
.balign 8
ret:
	.quad 0
/* end data */

.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, x0
	adrp	x0, a
	add	x0, x0, #:lo12:a
	ldr	w2, [x0]
	mov	w0, #1
	add	w0, w0, w2
	adrp	x2, a
	add	x2, x2, #:lo12:a
	str	w0, [x2]
	adrp	x0, ret
	add	x0, x0, #:lo12:ret
	ldr	x0, [x0]
	mov	x2, #-8
	add	x1, x1, x2
	ldr	x1, [x1]
	adrp	x2, ret
	add	x2, x2, #:lo12:ret
	str	x1, [x2]
	cmp	x0, x1
	beq	.L2
	bl	test
.L2:
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
