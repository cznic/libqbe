.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #100
	mov	w1, #0
.L2:
	add	w1, w1, w0
	mov	w2, #1
	sub	w0, w0, w2
	cmp	w0, #0
	bne	.L2
	adrp	x0, a
	add	x0, x0, #:lo12:a
	str	w1, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
