.text
.balign 16
.globl main
main:
	hint	#34
	mov	x16, #4112
	sub	sp, sp, x16
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	str	x19, [x29, 4120]
	mov	x2, #4096
	mov	x1, #0
	add	x0, x29, #16
	bl	memset
	mov	x1, #8
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #7
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L1:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L3
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #5
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L1
.L3:
	mov	x2, #7
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L4:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L6
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #5
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L4
.L6:
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #9
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #16
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L7:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L9
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L7
.L9:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #14
	add	x1, x1, x2
	str	x1, [x0]
.L10:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L12
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #7
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L10
.L12:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L13:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L15
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L13
.L15:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L16:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L18
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L16
.L18:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L19:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L21
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L19
.L21:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L22:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L24
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #9
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L22
.L24:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L25:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L27
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L25
.L27:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L28:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L30
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L28
.L30:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L31:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L33
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L31
.L33:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L34:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L36
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L34
.L36:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L37:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L39
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L37
.L39:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L40:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L42
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L40
.L42:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #14
	add	x1, x1, x2
	str	x1, [x0]
.L43:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L45
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #7
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L43
.L45:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L46:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L48
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L46
.L48:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L49:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L51
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L49
.L51:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L52:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L54
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L52
.L54:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L55:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L57
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L55
.L57:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L58:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L60
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L58
.L60:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L61:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L63
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L61
.L63:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L64:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L66
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L64
.L66:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L67:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L69
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L67
.L69:
	mov	x2, #4
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L70:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L72
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L70
.L72:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L73:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L75
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L73
.L75:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L76:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L78
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L76
.L78:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #13
	add	x1, x1, x2
	str	x1, [x0]
.L79:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L81
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #9
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L79
.L81:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L82:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L84
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L82
.L84:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L85:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L87
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #9
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L85
.L87:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L88:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L90
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #9
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L88
.L90:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
.L91:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L93
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #2
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L91
.L93:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L94:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L96
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L94
.L96:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L97:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L99
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L97
.L99:
	mov	x2, #3
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #13
	add	x1, x1, x2
	str	x1, [x0]
.L100:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L102
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L100
.L102:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L103:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L105
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L103
.L105:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L106:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L108
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L106
.L108:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L109:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L111
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L109
.L111:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L112:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L114
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L112
.L114:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L115:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L117
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L115
.L117:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L118:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L120
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L118
.L120:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L121:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L123
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L121
.L123:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L124:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L126
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L124
.L126:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #13
	add	x1, x1, x2
	str	x1, [x0]
.L127:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L129
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #9
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L127
.L129:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L130:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L132
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L130
.L132:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L133:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L135
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L133
.L135:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L136:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L138
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L136
.L138:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L139:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L141
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L139
.L141:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L142:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L144
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L142
.L144:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L145:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L147
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L145
.L147:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L148:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L150
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L148
.L150:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L151:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L153
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L151
.L153:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L154:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L156
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L154
.L156:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L157:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L159
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L157
.L159:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L160:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L162
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L160
.L162:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #13
	add	x1, x1, x2
	str	x1, [x0]
.L163:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L165
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L163
.L165:
	mov	x2, #2
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L166:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L168
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L166
.L168:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x1, [x0]
.L169:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L171
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #4
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L169
.L171:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #12
	add	x1, x1, x2
	str	x1, [x0]
.L172:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L174
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #8
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L172
.L174:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #19
	add	x1, x1, x2
	str	x1, [x0]
.L175:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L177
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #6
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L175
.L177:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L178:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L180
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L178
.L180:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #13
	add	x1, x1, x2
	str	x1, [x0]
.L181:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L183
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #9
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L181
.L183:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #11
	add	x1, x1, x2
	str	x1, [x0]
.L184:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L186
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L184
.L186:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
.L187:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L189
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #10
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L187
.L189:
	mov	x1, #8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #5
	add	x1, x1, x2
	str	x1, [x0]
.L190:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L192
	mov	x2, #-8
	add	x0, x0, x2
	ldr	x2, [x0]
	mov	x3, #2
	add	x2, x2, x3
	str	x2, [x0]
	mov	x2, #8
	add	x0, x0, x2
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L190
.L192:
	mov	x2, #13
	add	x1, x1, x2
	str	x1, [x0]
.L193:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L195
	mov	x1, #-8
	add	x0, x0, x1
	b	.L193
.L195:
	mov	x1, #32
	add	x0, x0, x1
.L196:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L224
	mov	x1, #-8
	add	x0, x0, x1
.L198:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L214
.L199:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L201
	mov	x1, #8
	add	x0, x0, x1
	b	.L199
.L201:
	mov	x1, #-16
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	ldr	x0, [x19]
	bl	putchar
.L202:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L204
	mov	x0, #-8
	add	x19, x19, x0
	b	.L202
.L204:
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L205:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L207
	mov	x0, #-8
	add	x19, x19, x0
	b	.L205
.L207:
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #104
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L208:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L210
	mov	x0, #-8
	add	x19, x19, x0
	b	.L208
.L210:
	mov	x0, #16
	add	x0, x19, x0
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L211:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L213
	mov	x0, #-8
	add	x19, x19, x0
	b	.L211
.L213:
	mov	x0, #24
	add	x0, x19, x0
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L198
.L214:
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #10
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
.L215:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L217
	mov	x1, #8
	add	x0, x0, x1
	b	.L215
.L217:
	mov	x1, #-8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
.L218:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L220
	mov	x0, #-8
	add	x19, x19, x0
	b	.L218
.L220:
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L221:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L223
	mov	x0, #-8
	add	x19, x19, x0
	b	.L221
.L223:
	mov	x0, #32
	add	x0, x19, x0
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L196
.L224:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
	mov	x1, #-8
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #-2
	add	x1, x1, x2
	str	x1, [x0]
.L225:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L241
.L226:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L228
	mov	x1, #8
	add	x0, x0, x1
	b	.L226
.L228:
	mov	x1, #-16
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	ldr	x0, [x19]
	bl	putchar
.L229:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L231
	mov	x0, #-8
	add	x19, x19, x0
	b	.L229
.L231:
	mov	x0, #8
	add	x0, x19, x0
	mov	x1, #8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L232:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L234
	mov	x0, #-8
	add	x19, x19, x0
	b	.L232
.L234:
	mov	x0, #8
	add	x0, x19, x0
	mov	x1, #8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #104
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L235:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L237
	mov	x0, #-8
	add	x19, x19, x0
	b	.L235
.L237:
	mov	x0, #16
	add	x19, x19, x0
	ldr	x0, [x19]
	mov	x1, #-1
	add	x0, x0, x1
	str	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L238:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L240
	mov	x0, #-8
	add	x19, x19, x0
	b	.L238
.L240:
	mov	x0, #24
	add	x0, x19, x0
	ldr	x1, [x0]
	mov	x2, #-1
	add	x1, x1, x2
	str	x1, [x0]
	b	.L225
.L241:
	mov	x2, #1
	add	x1, x1, x2
	str	x1, [x0]
.L242:
	ldr	x1, [x0]
	cmp	w1, #0
	beq	.L244
	mov	x1, #8
	add	x0, x0, x1
	b	.L242
.L244:
	mov	x1, #-8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
.L245:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L247
	mov	x0, #-8
	add	x19, x19, x0
	b	.L245
.L247:
	mov	x0, #8
	add	x0, x19, x0
	mov	x1, #8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #16
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #-32
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L248:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L250
	mov	x0, #8
	add	x19, x19, x0
	b	.L248
.L250:
	mov	x0, #-16
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	ldr	x0, [x19]
	bl	putchar
.L251:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L253
	mov	x0, #-8
	add	x19, x19, x0
	b	.L251
.L253:
	mov	x0, #8
	add	x0, x19, x0
	mov	x1, #8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #16
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L254:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L256
	mov	x0, #-8
	add	x19, x19, x0
	b	.L254
.L256:
	mov	x0, #8
	add	x0, x19, x0
	mov	x1, #8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #16
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #104
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L257:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L259
	mov	x0, #8
	add	x19, x19, x0
	b	.L257
.L259:
	mov	x0, #-8
	add	x0, x19, x0
	mov	x1, #-24
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #-16
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #-24
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
.L260:
	ldr	x0, [x19]
	cmp	w0, #0
	beq	.L262
	mov	x0, #-8
	add	x19, x19, x0
	b	.L260
.L262:
	mov	x0, #32
	add	x0, x19, x0
	mov	x1, #8
	add	x19, x0, x1
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x19, x19, x0
	ldr	x0, [x19]
	bl	putchar
	mov	x0, #8
	add	x0, x19, x0
	ldr	x0, [x0]
	bl	putchar
	mov	w0, #0
	ldr	x19, [x29, 4120]
	ldp	x29, x30, [sp], 16
	mov	x16, #4112
	add	sp, sp, x16
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
