.text
.balign 16
.globl f
f:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w2, #0
.L2:
	mov	w3, #1
	sub	w0, w0, w3
	cmp	w0, #0
	blt	.L4
	ldr	w2, [x1]
	b	.L2
.L4:
	mov	w0, w2
	ldp	x29, x30, [sp], 16
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
