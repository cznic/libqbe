.text
.balign 16
mandel:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	d2, [x0]
	fsub	d3, d1, d2
	adrp	x0, ".Lfp0"
	add	x0, x0, #:lo12:".Lfp0"
	ldr	d2, [x0]
	adrp	x0, ".Lfp0"
	add	x0, x0, #:lo12:".Lfp0"
	ldr	d1, [x0]
	mov	w0, #0
.L2:
	fmov	d4, d1
	mov	w1, #1
	add	w0, w1, w0
	fmov	d1, d2
	fmul	d2, d4, d2
	fmul	d5, d4, d4
	fmul	d4, d1, d1
	fsub	d1, d5, d4
	fadd	d1, d1, d3
	fadd	d2, d2, d2
	fadd	d2, d2, d0
	fadd	d4, d4, d5
	adrp	x1, ".Lfp2"
	add	x1, x1, #:lo12:".Lfp2"
	ldr	d5, [x1]
	fcmpe	d4, d5
	bgt	.L5
	cmp	w0, #1000
	ble	.L2
	mov	w0, #0
.L5:
	ldp	x29, x30, [sp], 16
	ret
.type mandel, @function
.size mandel, .-mandel
/* end function mandel */

.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	d8, [x29, 24]
	str	d9, [x29, 16]
	adrp	x0, ".Lfp3"
	add	x0, x0, #:lo12:".Lfp3"
	ldr	d0, [x0]
	fmov	d9, d0
.L8:
	adrp	x0, ".Lfp3"
	add	x0, x0, #:lo12:".Lfp3"
	ldr	d0, [x0]
	fmov	d8, d0
.L10:
	fmov	d1, d9
	fmov	d0, d8
	bl	mandel
	cmp	w0, #0
	bne	.L12
	mov	w0, #42
	bl	putchar
	b	.L13
.L12:
	mov	w0, #32
	bl	putchar
.L13:
	adrp	x0, ".Lfp5"
	add	x0, x0, #:lo12:".Lfp5"
	ldr	d0, [x0]
	fadd	d8, d8, d0
	adrp	x0, ".Lfp4"
	add	x0, x0, #:lo12:".Lfp4"
	ldr	d0, [x0]
	fcmpe	d8, d0
	bls	.L10
	mov	w0, #10
	bl	putchar
	adrp	x0, ".Lfp5"
	add	x0, x0, #:lo12:".Lfp5"
	ldr	d0, [x0]
	fadd	d9, d9, d0
	adrp	x0, ".Lfp4"
	add	x0, x0, #:lo12:".Lfp4"
	ldr	d0, [x0]
	fcmpe	d9, d0
	bls	.L8
	mov	w0, #0
	ldr	d8, [x29, 24]
	ldr	d9, [x29, 16]
	ldp	x29, x30, [sp], 32
	ret
.type main, @function
.size main, .-main
/* end function main */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp0:
	.int 0
	.int 0 /* 0.000000 */

.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 1071644672 /* 0.500000 */

.section .rodata
.p2align 3
.Lfp2:
	.int 0
	.int 1076887552 /* 16.000000 */

.section .rodata
.p2align 3
.Lfp3:
	.int 0
	.int -1074790400 /* -1.000000 */

.section .rodata
.p2align 3
.Lfp4:
	.int 0
	.int 1072693248 /* 1.000000 */

.section .rodata
.p2align 3
.Lfp5:
	.int -755914244
	.int 1067475533 /* 0.032000 */

.section .note.GNU-stack,"",@progbits
