.text
.balign 16
.globl f
f:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x1, #28
	add	x0, x29, #16
	add	x1, x0, x1
	mov	w0, #-128
	str	w0, [x1]
	mov	x1, #24
	add	x0, x29, #16
	add	x1, x0, x1
	mov	w0, #-56
	str	w0, [x1]
	mov	x1, #16
	add	x0, x29, #16
	add	x2, x0, x1
	add	x0, x29, #48
	mov	x1, #192
	add	x1, x0, x1
	str	x1, [x2]
	mov	x2, #8
	add	x1, x29, #16
	add	x2, x1, x2
	mov	x1, #64
	add	x1, x0, x1
	str	x1, [x2]
	mov	x1, #192
	add	x0, x0, x1
	add	x1, x29, #16
	str	x0, [x1]
	mov	x1, #24
	add	x0, x29, #16
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L2
	add	x0, x29, #16
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #16
	str	x1, [x2]
	b	.L3
.L2:
	mov	x2, #8
	add	x0, x29, #16
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #8
	add	w1, w1, w2
	mov	x3, #24
	add	x2, x29, #16
	add	x2, x2, x3
	str	w1, [x2]
.L3:
	mov	x1, #28
	add	x0, x29, #16
	add	x0, x0, x1
	ldrsw	x1, [x0]
	cmp	w1, #0
	blt	.L5
	add	x0, x29, #16
	ldr	x0, [x0]
	mov	x1, #8
	add	x1, x0, x1
	add	x2, x29, #16
	str	x1, [x2]
	b	.L6
.L5:
	mov	x2, #16
	add	x0, x29, #16
	add	x0, x0, x2
	ldr	x0, [x0]
	add	x0, x0, x1
	mov	w2, #16
	add	w1, w1, w2
	mov	x3, #28
	add	x2, x29, #16
	add	x2, x2, x3
	str	w1, [x2]
.L6:
	ldr	d0, [x0]
	ldp	x29, x30, [sp], 240
	ret
.type f, @function
.size f, .-f
/* end function f */

.text
.balign 16
.globl g
g:
	hint	#34
	str	q7, [sp, -16]!
	str	q6, [sp, -16]!
	str	q5, [sp, -16]!
	str	q4, [sp, -16]!
	str	q3, [sp, -16]!
	str	q2, [sp, -16]!
	str	q1, [sp, -16]!
	str	q0, [sp, -16]!
	stp	x6, x7, [sp, -16]!
	stp	x4, x5, [sp, -16]!
	stp	x2, x3, [sp, -16]!
	stp	x0, x1, [sp, -16]!
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x2, #28
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-128
	str	w1, [x2]
	mov	x2, #24
	add	x1, x29, #16
	add	x2, x1, x2
	mov	w1, #-56
	str	w1, [x2]
	mov	x2, #16
	add	x1, x29, #16
	add	x3, x1, x2
	add	x1, x29, #48
	mov	x2, #192
	add	x2, x1, x2
	str	x2, [x3]
	mov	x3, #8
	add	x2, x29, #16
	add	x3, x2, x3
	mov	x2, #64
	add	x2, x1, x2
	str	x2, [x3]
	mov	x2, #192
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	print
	ldp	x29, x30, [sp], 240
	ret
.type g, @function
.size g, .-g
/* end function g */

.section .note.GNU-stack,"",@progbits
