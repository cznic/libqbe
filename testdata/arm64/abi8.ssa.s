.data
.balign 8
ctoqbestr:
	.ascii "c->qbe(%d)"
	.byte 0
/* end data */

.data
.balign 8
emptystr:
	.byte 0
/* end data */

.text
.balign 16
.globl qfn0
qfn0:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	d8, [x29, 24]
	ldr	s8, [x29, 32]
	mov	w1, #0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	fmov	s0, s8
	bl	ps
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	d8, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
.type qfn0, @function
.size qfn0, .-qfn0
/* end function qfn0 */

.text
.balign 16
.globl qfn1
qfn1:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	str	d8, [x29, 32]
	fmov	s8, s0
	mov	x3, #0
	add	x2, x29, #24
	add	x2, x2, x3
	str	x1, [x2]
	mov	w1, #1
	mov	w19, w0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	mov	w0, w19
	bl	pw
	fmov	s0, s8
	bl	ps
	add	x0, x29, #24
	bl	pfi1
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	x19, [x29, 40]
	ldr	d8, [x29, 32]
	ldp	x29, x30, [sp], 48
	ret
.type qfn1, @function
.size qfn1, .-qfn1
/* end function qfn1 */

.text
.balign 16
.globl qfn2
qfn2:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	str	d8, [x29, 32]
	fmov	s8, s0
	mov	x3, #0
	add	x2, x29, #24
	add	x2, x2, x3
	str	x1, [x2]
	mov	w1, #2
	mov	w19, w0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	mov	w0, w19
	bl	pw
	add	x0, x29, #24
	bl	pfi2
	fmov	s0, s8
	bl	ps
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	x19, [x29, 40]
	ldr	d8, [x29, 32]
	ldp	x29, x30, [sp], 48
	ret
.type qfn2, @function
.size qfn2, .-qfn2
/* end function qfn2 */

.text
.balign 16
.globl qfn3
qfn3:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	str	d8, [x29, 32]
	fmov	s8, s0
	mov	x3, #0
	add	x2, x29, #24
	add	x2, x2, x3
	str	x1, [x2]
	mov	w1, #3
	mov	w19, w0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	mov	w0, w19
	bl	pw
	fmov	s0, s8
	bl	ps
	add	x0, x29, #24
	bl	pfi3
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	x19, [x29, 40]
	ldr	d8, [x29, 32]
	ldp	x29, x30, [sp], 48
	ret
.type qfn3, @function
.size qfn3, .-qfn3
/* end function qfn3 */

.text
.balign 16
.globl qfn4
qfn4:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	mov	x1, #4
	add	x0, x29, #24
	add	x0, x0, x1
	str	s1, [x0]
	mov	x1, #0
	add	x0, x29, #24
	add	x0, x0, x1
	str	s0, [x0]
	mov	w1, #4
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	add	x0, x29, #24
	bl	pss
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldp	x29, x30, [sp], 32
	ret
.type qfn4, @function
.size qfn4, .-qfn4
/* end function qfn4 */

.text
.balign 16
.globl qfn5
qfn5:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	str	d8, [x29, 16]
	ldr	s8, [x29, 40]
	mov	w1, #5
	mov	x19, x0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	mov	x0, x19
	mov	x19, x0
	add	x0, x29, #32
	bl	pss
	fmov	s0, s8
	bl	ps
	mov	x0, x19
	bl	pl
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	x19, [x29, 24]
	ldr	d8, [x29, 16]
	ldp	x29, x30, [sp], 32
	ret
.type qfn5, @function
.size qfn5, .-qfn5
/* end function qfn5 */

.text
.balign 16
.globl qfn6
qfn6:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	mov	x3, #8
	add	x2, x29, #16
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #16
	add	x1, x1, x2
	str	x0, [x1]
	mov	w1, #6
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	add	x0, x29, #16
	bl	plb
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldp	x29, x30, [sp], 32
	ret
.type qfn6, @function
.size qfn6, .-qfn6
/* end function qfn6 */

.text
.balign 16
.globl qfn7
qfn7:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w1, #7
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	add	x0, x29, #16
	bl	plb
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldp	x29, x30, [sp], 16
	ret
.type qfn7, @function
.size qfn7, .-qfn7
/* end function qfn7 */

.text
.balign 16
.globl qfn8
qfn8:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w1, #8
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	add	x0, x29, #16
	bl	plb
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldp	x29, x30, [sp], 16
	ret
.type qfn8, @function
.size qfn8, .-qfn8
/* end function qfn8 */

.text
.balign 16
.globl qfn9
qfn9:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	mov	w1, #9
	mov	x19, x0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	mov	x0, x19
	bl	pbig
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	x19, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
.type qfn9, @function
.size qfn9, .-qfn9
/* end function qfn9 */

.text
.balign 16
.globl qfn10
qfn10:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	str	x20, [x29, 32]
	str	d8, [x29, 24]
	ldr	x19, [x29, 56]
	fmov	s8, s0
	ldr	x20, [x29, 48]
	mov	x0, x20
	mov	w1, #10
	mov	x20, x0
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	mov	x0, x20
	bl	pbig
	fmov	s0, s8
	bl	ps
	mov	x0, x19
	bl	pl
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldr	x19, [x29, 40]
	ldr	x20, [x29, 32]
	ldr	d8, [x29, 24]
	ldp	x29, x30, [sp], 48
	ret
.type qfn10, @function
.size qfn10, .-qfn10
/* end function qfn10 */

.text
.balign 16
.globl qfn11
qfn11:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x1, #16
	add	x0, x29, #24
	add	x0, x0, x1
	str	d2, [x0]
	mov	x1, #8
	add	x0, x29, #24
	add	x0, x0, x1
	str	d1, [x0]
	mov	x1, #0
	add	x0, x29, #24
	add	x0, x0, x1
	str	d0, [x0]
	mov	w1, #11
	adrp	x0, ctoqbestr
	add	x0, x0, #:lo12:ctoqbestr
	bl	printf
	add	x0, x29, #24
	bl	pddd
	adrp	x0, emptystr
	add	x0, x0, #:lo12:emptystr
	bl	puts
	ldp	x29, x30, [sp], 48
	ret
.type qfn11, @function
.size qfn11, .-qfn11
/* end function qfn11 */

.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -64]!
	mov	x29, sp
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	adrp	x1, ".Lfp2"
	add	x1, x1, #:lo12:".Lfp2"
	ldr	s0, [x1]
	str	s0, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s7, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s6, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s5, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s4, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s3, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s2, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s1, [x0]
	adrp	x0, ".Lfp1"
	add	x0, x0, #:lo12:".Lfp1"
	ldr	s0, [x0]
	bl	cfn0
	mov	x0, #16
	add	sp, sp, x0
	mov	x1, #0
	adrp	x0, fi1
	add	x0, x0, #:lo12:fi1
	add	x0, x0, x1
	ldr	x1, [x0]
	adrp	x0, ".Lfp3"
	add	x0, x0, #:lo12:".Lfp3"
	ldr	s0, [x0]
	mov	w0, #1
	bl	cfn1
	adrp	x0, ".Lfp4"
	add	x0, x0, #:lo12:".Lfp4"
	ldr	s0, [x0]
	mov	x1, #0
	adrp	x0, fi2
	add	x0, x0, #:lo12:fi2
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	w0, #1
	bl	cfn2
	mov	x1, #0
	adrp	x0, fi3
	add	x0, x0, #:lo12:fi3
	add	x0, x0, x1
	ldr	x1, [x0]
	adrp	x0, ".Lfp3"
	add	x0, x0, #:lo12:".Lfp3"
	ldr	s0, [x0]
	mov	w0, #1
	bl	cfn3
	mov	x1, #4
	adrp	x0, ss
	add	x0, x0, #:lo12:ss
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	adrp	x0, ss
	add	x0, x0, #:lo12:ss
	add	x0, x0, x1
	ldr	s0, [x0]
	bl	cfn4
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #8
	add	x0, sp, x0
	adrp	x1, ".Lfp2"
	add	x1, x1, #:lo12:".Lfp2"
	ldr	s0, [x1]
	str	s0, [x0]
	mov	x0, #0
	add	x1, sp, x0
	mov	x2, #0
	adrp	x0, ss
	add	x0, x0, #:lo12:ss
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #0
	add	x1, x1, x2
	str	x0, [x1]
	mov	x0, #10
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d6, [x1]
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d5, [x1]
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d4, [x1]
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d3, [x1]
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d2, [x1]
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d1, [x1]
	adrp	x1, ".Lfp1"
	add	x1, x1, #:lo12:".Lfp1"
	ldr	d0, [x1]
	bl	cfn5
	mov	x0, #16
	add	sp, sp, x0
	mov	x1, #8
	adrp	x0, lb
	add	x0, x0, #:lo12:lb
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #0
	adrp	x0, lb
	add	x0, x0, #:lo12:lb
	add	x0, x0, x2
	ldr	x0, [x0]
	bl	cfn6
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x1, sp, x0
	mov	x2, #0
	adrp	x0, lb
	add	x0, x0, #:lo12:lb
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #0
	add	x2, x1, x2
	str	x0, [x2]
	mov	x2, #8
	adrp	x0, lb
	add	x0, x0, #:lo12:lb
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x0, [x1]
	mov	w6, #0
	mov	w5, #0
	mov	w4, #0
	mov	w3, #0
	mov	w2, #0
	mov	w1, #0
	mov	w0, #0
	bl	cfn7
	mov	x0, #16
	add	sp, sp, x0
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x1, sp, x0
	mov	x2, #0
	adrp	x0, lb
	add	x0, x0, #:lo12:lb
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #0
	add	x2, x1, x2
	str	x0, [x2]
	mov	x2, #8
	adrp	x0, lb
	add	x0, x0, #:lo12:lb
	add	x0, x0, x2
	ldr	x0, [x0]
	mov	x2, #8
	add	x1, x1, x2
	str	x0, [x1]
	mov	w7, #0
	mov	w6, #0
	mov	w5, #0
	mov	w4, #0
	mov	w3, #0
	mov	w2, #0
	mov	w1, #0
	mov	w0, #0
	bl	cfn8
	mov	x0, #16
	add	sp, sp, x0
	mov	x1, #0
	adrp	x0, big
	add	x0, x0, #:lo12:big
	add	x0, x0, x1
	ldrb	w0, [x0]
	mov	x2, #0
	add	x1, x29, #44
	add	x1, x1, x2
	strb	w0, [x1]
	mov	x1, #1
	adrp	x0, big
	add	x0, x0, #:lo12:big
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #1
	add	x1, x29, #44
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #9
	adrp	x0, big
	add	x0, x0, #:lo12:big
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #9
	add	x1, x29, #44
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #44
	bl	cfn9
	mov	x1, #0
	adrp	x0, big
	add	x0, x0, #:lo12:big
	add	x0, x0, x1
	ldrb	w0, [x0]
	mov	x2, #0
	add	x1, x29, #24
	add	x1, x1, x2
	strb	w0, [x1]
	mov	x1, #1
	adrp	x0, big
	add	x0, x0, #:lo12:big
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #1
	add	x1, x29, #24
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #9
	adrp	x0, big
	add	x0, x0, #:lo12:big
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #9
	add	x1, x29, #24
	add	x1, x1, x2
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #8
	add	x1, sp, x0
	mov	x0, #11
	str	x0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	add	x0, x29, #24
	str	x0, [x1]
	adrp	x0, ".Lfp0"
	add	x0, x0, #:lo12:".Lfp0"
	ldr	s0, [x0]
	mov	w7, #0
	mov	w6, #0
	mov	w5, #0
	mov	w4, #0
	mov	w3, #0
	mov	w2, #0
	mov	w1, #0
	mov	w0, #0
	bl	cfn10
	mov	x0, #16
	add	sp, sp, x0
	mov	x1, #16
	adrp	x0, ddd
	add	x0, x0, #:lo12:ddd
	add	x0, x0, x1
	ldr	d2, [x0]
	mov	x1, #8
	adrp	x0, ddd
	add	x0, x0, #:lo12:ddd
	add	x0, x0, x1
	ldr	d1, [x0]
	mov	x1, #0
	adrp	x0, ddd
	add	x0, x0, #:lo12:ddd
	add	x0, x0, x1
	ldr	d0, [x0]
	bl	cfn11
	mov	w0, #0
	ldp	x29, x30, [sp], 64
	ret
.type main, @function
.size main, .-main
/* end function main */

/* floating point constants */
.section .rodata
.p2align 3
.Lfp1:
	.int 0
	.int 0 /* 0.000000 */

.section .rodata
.p2align 2
.Lfp0:
	.int 1092721050 /* 10.100000 */

.section .rodata
.p2align 2
.Lfp2:
	.int 1092511334 /* 9.900000 */

.section .rodata
.p2align 2
.Lfp3:
	.int 1074580685 /* 2.200000 */

.section .rodata
.p2align 2
.Lfp4:
	.int 1079194419 /* 3.300000 */

.section .note.GNU-stack,"",@progbits
