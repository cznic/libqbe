.text
.balign 16
.globl f
f:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #8
	mul	x2, x0, x2
	adrp	x0, a
	add	x0, x0, #:lo12:a
	add	x0, x0, x2
	mov	x2, #4
	mul	x1, x1, x2
	add	x0, x0, x1
	ldr	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
.type f, @function
.size f, .-f
/* end function f */

.section .note.GNU-stack,"",@progbits
