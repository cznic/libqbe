.text
.balign 16
alpha:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	add	x2, x0, x2
.L1:
	strb	w1, [x0]
	mov	x3, x0
	mov	x0, #1
	add	x0, x3, x0
	mov	w4, #1
	add	w1, w1, w4
	cmp	x3, x2
	bne	.L1
	mov	w0, #0
	strb	w0, [x2]
	ldp	x29, x30, [sp], 16
	ret
.type alpha, @function
.size alpha, .-alpha
/* end function alpha */

.text
.balign 16
.globl test
test:
	hint	#34
	stp	x29, x30, [sp, -96]!
	mov	x29, sp
	mov	x2, #16
	mov	w1, #65
	add	x0, x29, #56
	bl	alpha
	mov	x2, #16
	mov	w1, #97
	add	x0, x29, #76
	bl	alpha
	mov	x1, #0
	add	x0, x29, #76
	add	x0, x0, x1
	ldrb	w0, [x0]
	mov	x2, #0
	add	x1, x29, #36
	add	x1, x1, x2
	strb	w0, [x1]
	mov	x1, #1
	add	x0, x29, #76
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #1
	add	x1, x29, #36
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #9
	add	x0, x29, #76
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #9
	add	x1, x29, #36
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #0
	add	x0, x29, #56
	add	x0, x0, x1
	ldrb	w0, [x0]
	mov	x2, #0
	add	x1, x29, #16
	add	x1, x1, x2
	strb	w0, [x1]
	mov	x1, #1
	add	x0, x29, #56
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #1
	add	x1, x29, #16
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #9
	add	x0, x29, #56
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x2, #9
	add	x1, x29, #16
	add	x1, x1, x2
	str	x0, [x1]
	mov	x0, #32
	sub	sp, sp, x0
	mov	x0, #16
	add	x1, sp, x0
	add	x0, x29, #36
	str	x0, [x1]
	mov	x0, #8
	add	x1, sp, x0
	mov	w0, #9
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #8
	str	w0, [x1]
	mov	w7, #7
	mov	w6, #6
	mov	w5, #5
	mov	w4, #4
	mov	w3, #3
	mov	w2, #2
	mov	w1, #1
	add	x0, x29, #16
	bl	fcb
	mov	x0, #32
	add	sp, sp, x0
	ldp	x29, x30, [sp], 96
	ret
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
