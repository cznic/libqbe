.text
.balign 16
test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
.L1:
	b	.L1
.type test, @function
.size test, .-test
/* end function test */

.section .note.GNU-stack,"",@progbits
