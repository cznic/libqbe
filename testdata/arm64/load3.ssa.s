.text
.balign 16
rand:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
.type rand, @function
.size rand, .-rand
/* end function rand */

.text
.balign 16
chk:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, #1
	cset	w0, eq
	cmp	w1, #0
	cset	w1, eq
	and	w0, w0, w1
	mov	w1, #1
	eor	w0, w0, w1
	ldp	x29, x30, [sp], 16
	ret
.type chk, @function
.size chk, .-chk
/* end function chk */

.text
.balign 16
.globl main
main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	add	x1, x29, #24
	mov	w0, #1
	str	w0, [x1]
	add	x1, x29, #24
	mov	x0, #4
	add	x1, x0, x1
	mov	w0, #0
	str	w0, [x1]
	bl	rand
	cmp	w0, #0
	mov	w1, #0
	mov	w0, #1
	bl	chk
	ldp	x29, x30, [sp], 32
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits
