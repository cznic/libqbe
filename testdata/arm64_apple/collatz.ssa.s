.text
.balign 4
.globl _test
_test:
	hint	#34
	sub	sp, sp, #4000
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w3, #0
	mov	w1, #1
L2:
	cmp	w1, #1000
	bge	L14
	mov	w2, w1
	mov	w0, #0
L4:
	cmp	w1, #1
	beq	L11
	cmp	w1, w2
	blt	L9
	mov	w4, #1
	add	w0, w0, w4
	mov	w4, #1
	and	w4, w1, w4
	cmp	w4, #0
	bne	L8
	mov	w4, #1
	lsr	w1, w1, w4
	b	L4
L8:
	mov	w4, #3
	mul	w1, w4, w1
	mov	w4, #1
	add	w1, w1, w4
	b	L4
L9:
	mov	w18, w1
	mov	w1, w2
	mov	w2, w18
	sxtw	x2, w2
	mov	x4, #4
	mul	x2, x2, x4
	add	x4, x29, #16
	add	x2, x2, x4
	ldr	w2, [x2]
	add	w0, w0, w2
	b	L12
L11:
	mov	w1, w2
L12:
	sxtw	x2, w1
	mov	x4, #4
	mul	x2, x2, x4
	add	x4, x29, #16
	add	x2, x2, x4
	str	w0, [x2]
	mov	w2, w1
	mov	w1, #1
	add	w1, w1, w2
	cmp	w3, w0
	bgt	L2
	mov	w3, w0
	b	L2
L14:
	adrp	x0, _a@page
	add	x0, x0, _a@pageoff
	str	w3, [x0]
	ldp	x29, x30, [sp], 16
	add	sp, sp, #4000
	ret
/* end function test */

