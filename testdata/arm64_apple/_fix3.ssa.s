.text
.balign 4
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w1, #100
	mov	w0, #0
L2:
	cmp	w1, #10
	ble	L4
	sub	w0, w0, w1
	b	L5
L4:
	add	w0, w0, w1
	mov	w2, #1
	sub	w1, w1, w2
L5:
	mov	w2, #1
	sub	w1, w1, w2
	cmp	w1, #0
	bne	L2
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

