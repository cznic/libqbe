.text
.balign 4
.globl _tests
_tests:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	mov	w0, #1
	str	w0, [x1]
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	mov	w0, #2
	str	w0, [x1]
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	mov	w0, #3
	str	w0, [x1]
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	mov	w0, #0
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function tests */

