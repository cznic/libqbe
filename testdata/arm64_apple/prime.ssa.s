.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w3, #13
	mov	w2, #5
L2:
	mov	w1, w3
	mov	w0, #2
	add	w3, w1, w0
	mov	w0, #3
L4:
	sdiv	w18, w1, w0
	msub	w4, w18, w0, w1
	cmp	w4, #0
	beq	L2
	mov	w4, #2
	add	w0, w4, w0
	mul	w4, w0, w0
	cmp	w4, w1
	ble	L4
	mov	w0, #1
	add	w2, w0, w2
	mov	w0, #10001
	cmp	w2, w0
	bne	L2
	adrp	x0, _a@page
	add	x0, x0, _a@pageoff
	str	w1, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

