.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #100
	mov	w1, #0
L2:
	add	w1, w1, w0
	mov	w2, #1
	sub	w0, w0, w2
	cmp	w0, #0
	bne	L2
	adrp	x0, _a@page
	add	x0, x0, _a@pageoff
	str	w1, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

