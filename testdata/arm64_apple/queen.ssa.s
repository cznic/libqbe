.text
.balign 4
.globl _chk
_chk:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w3, #0
	mov	w2, #0
L2:
	adrp	x4, _glo1@page
	add	x4, x4, _glo1@pageoff
	ldr	w7, [x4]
	cmp	w3, w7
	bge	L12
	adrp	x4, _glo3@page
	add	x4, x4, _glo3@pageoff
	ldr	x4, [x4]
	sxtw	x6, w0
	mov	x5, #8
	mul	x5, x5, x6
	add	x5, x4, x5
	ldr	x5, [x5]
	sxtw	x8, w3
	mov	x6, #4
	mul	x6, x6, x8
	add	x5, x5, x6
	ldr	w5, [x5]
	add	w2, w2, w5
	sxtw	x6, w3
	mov	x5, #8
	mul	x5, x5, x6
	add	x5, x4, x5
	ldr	x5, [x5]
	sxtw	x8, w1
	mov	x6, #4
	mul	x6, x6, x8
	add	x5, x5, x6
	ldr	w5, [x5]
	add	w2, w2, w5
	add	w5, w0, w3
	cmp	w5, w7
	cset	w5, lt
	add	w6, w1, w3
	cmp	w6, w7
	cset	w6, lt
	and	w5, w5, w6
	cmp	w5, #0
	beq	L5
	add	w5, w0, w3
	sxtw	x6, w5
	mov	x5, #8
	mul	x5, x5, x6
	add	x5, x4, x5
	ldr	x5, [x5]
	add	w6, w1, w3
	sxtw	x8, w6
	mov	x6, #4
	mul	x6, x6, x8
	add	x5, x5, x6
	ldr	w5, [x5]
	add	w2, w2, w5
L5:
	add	w5, w0, w3
	cmp	w5, w7
	cset	w5, lt
	sub	w6, w1, w3
	cmp	w6, #0
	cset	w6, ge
	and	w5, w5, w6
	cmp	w5, #0
	beq	L7
	add	w5, w0, w3
	sxtw	x6, w5
	mov	x5, #8
	mul	x5, x5, x6
	add	x5, x4, x5
	ldr	x5, [x5]
	sub	w6, w1, w3
	sxtw	x8, w6
	mov	x6, #4
	mul	x6, x6, x8
	add	x5, x5, x6
	ldr	w5, [x5]
	add	w2, w2, w5
L7:
	sub	w5, w0, w3
	cmp	w5, #0
	cset	w5, ge
	add	w6, w1, w3
	cmp	w6, w7
	cset	w6, lt
	and	w5, w5, w6
	cmp	w5, #0
	beq	L9
	sub	w5, w0, w3
	sxtw	x6, w5
	mov	x5, #8
	mul	x5, x5, x6
	add	x5, x4, x5
	ldr	x5, [x5]
	add	w6, w1, w3
	sxtw	x7, w6
	mov	x6, #4
	mul	x6, x6, x7
	add	x5, x5, x6
	ldr	w5, [x5]
	add	w2, w2, w5
L9:
	sub	w5, w0, w3
	cmp	w5, #0
	cset	w5, ge
	sub	w6, w1, w3
	cmp	w6, #0
	cset	w6, ge
	and	w5, w5, w6
	cmp	w5, #0
	beq	L11
	sub	w5, w0, w3
	sxtw	x6, w5
	mov	x5, #8
	mul	x5, x5, x6
	add	x4, x4, x5
	ldr	x4, [x4]
	sub	w5, w1, w3
	sxtw	x6, w5
	mov	x5, #4
	mul	x5, x5, x6
	add	x4, x4, x5
	ldr	w4, [x4]
	add	w2, w2, w4
L11:
	mov	w4, #1
	add	w3, w3, w4
	b	L2
L12:
	mov	w0, w2
	ldp	x29, x30, [sp], 16
	ret
/* end function chk */

.text
.balign 4
.globl _go
_go:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	str	x20, [x29, 16]
	adrp	x1, _glo1@page
	add	x1, x1, _glo1@pageoff
	ldr	w1, [x1]
	cmp	w0, w1
	beq	L21
	mov	w19, #0
L16:
	adrp	x1, _glo1@page
	add	x1, x1, _glo1@pageoff
	ldr	w1, [x1]
	cmp	w19, w1
	bge	L20
	mov	w1, w0
	mov	w20, w0
	mov	w0, w19
	bl	_chk
	mov	w1, w0
	mov	w0, w20
	cmp	w1, #0
	bne	L19
	adrp	x1, _glo3@page
	add	x1, x1, _glo3@pageoff
	ldr	x1, [x1]
	sxtw	x3, w19
	mov	x2, #8
	mul	x2, x2, x3
	add	x1, x1, x2
	ldr	x1, [x1]
	sxtw	x3, w0
	mov	x2, #4
	mul	x2, x2, x3
	add	x2, x1, x2
	ldr	w1, [x2]
	mov	w3, #1
	add	w1, w1, w3
	str	w1, [x2]
	mov	w20, w0
	mov	w0, #1
	add	w0, w20, w0
	bl	_go
	mov	w0, w20
	adrp	x1, _glo3@page
	add	x1, x1, _glo3@pageoff
	ldr	x1, [x1]
	sxtw	x3, w19
	mov	x2, #8
	mul	x2, x2, x3
	add	x1, x1, x2
	ldr	x1, [x1]
	sxtw	x3, w0
	mov	x2, #4
	mul	x2, x2, x3
	add	x2, x1, x2
	ldr	w1, [x2]
	mov	w3, #1
	sub	w1, w1, w3
	str	w1, [x2]
L19:
	mov	w1, #1
	add	w19, w19, w1
	b	L16
L20:
	mov	w0, #0
	b	L22
L21:
	adrp	x0, _glo2@page
	add	x0, x0, _glo2@pageoff
	ldr	w0, [x0]
	mov	w1, #1
	add	w0, w0, w1
	adrp	x1, _glo2@page
	add	x1, x1, _glo2@pageoff
	str	w0, [x1]
	mov	w0, #0
L22:
	ldr	x19, [x29, 24]
	ldr	x20, [x29, 16]
	ldp	x29, x30, [sp], 32
	ret
/* end function go */

.text
.balign 4
.globl _main
_main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	adrp	x1, _glo1@page
	add	x1, x1, _glo1@pageoff
	mov	w0, #8
	str	w0, [x1]
	mov	w1, #8
	mov	w0, #8
	bl	_calloc
	adrp	x1, _glo3@page
	add	x1, x1, _glo3@pageoff
	str	x0, [x1]
	mov	w19, #0
L25:
	adrp	x0, _glo1@page
	add	x0, x0, _glo1@pageoff
	ldr	w0, [x0]
	cmp	w19, w0
	bge	L27
	mov	w1, #4
	bl	_calloc
	adrp	x1, _glo3@page
	add	x1, x1, _glo3@pageoff
	ldr	x1, [x1]
	sxtw	x3, w19
	mov	x2, #8
	mul	x2, x2, x3
	add	x1, x1, x2
	str	x0, [x1]
	mov	w0, #1
	add	w19, w19, w0
	b	L25
L27:
	mov	w0, #0
	bl	_go
	adrp	x0, _glo2@page
	add	x0, x0, _glo2@pageoff
	ldr	w0, [x0]
	cmp	w0, #92
	cset	w0, ne
	ldr	x19, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
/* end function main */

.data
.balign 8
_glo1:
	.int 0
/* end data */

.data
.balign 8
_glo2:
	.int 0
/* end data */

.data
.balign 8
_glo3:
	.quad 0
/* end data */

