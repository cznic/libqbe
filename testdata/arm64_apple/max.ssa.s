.data
.balign 8
_arr:
	.byte 10
	.byte -60
	.byte 10
	.byte 100
	.byte 200
	.byte 0
/* end data */

.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, _arr@page
	add	x1, x1, _arr@pageoff
	mov	w0, #-1
L2:
	ldrb	w2, [x1]
	mov	x3, #1
	add	x1, x3, x1
	cmp	w2, #0
	beq	L5
	cmp	w0, w2
	bgt	L2
	mov	w0, w2
	b	L2
L5:
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

