.text
.balign 4
.globl _main
_main:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	adrp	x1, "Lfp0"@page
	add	x1, x1, "Lfp0"@pageoff
	ldr	d0, [x1]
	str	d0, [x0]
	adrp	x0, _fmt@page
	add	x0, x0, _fmt@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
/* end function main */

.data
.balign 8
_fmt:
	.ascii "%.06f\n"
	.byte 0
/* end data */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp0:
	.int 858993459
	.int 1072902963 /* 1.200000 */

