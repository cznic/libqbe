.section __DATA,__thread_vars,thread_local_variables
_i:
	.quad __tlv_bootstrap
	.quad 0
	.quad _i$tlv$init

.section __DATA,__thread_data,thread_local_regular
.balign 4
_i$tlv$init:
	.int 42
/* end data */

.data
.balign 1
_fmti:
	.ascii "i%d==%d\n"
	.byte 0
/* end data */

.section __DATA,__thread_vars,thread_local_variables
_x:
	.quad __tlv_bootstrap
	.quad 0
	.quad _x$tlv$init

.section __DATA,__thread_data,thread_local_regular
.balign 8
_x$tlv$init:
	.int 1
	.int 2
	.int 3
	.int 4
/* end data */

.data
.balign 1
_fmtx:
	.ascii "*(x+%d)==%d\n"
	.byte 0
/* end data */

.text
.balign 4
.globl _main
_main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	mov	x3, #0
	adrp	x2, _thread@page
	add	x2, x2, _thread@pageoff
	mov	x1, #0
	add	x0, x29, #16
	bl	_pthread_create
	add	x0, x29, #16
	ldr	x0, [x0]
	add	x1, x29, #24
	bl	_pthread_join
	adrp	x0, _i@tlvppage
	ldr	x0, [x0, _i@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	ldr	w0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #0
	str	w0, [x1]
	adrp	x0, _fmti@page
	add	x0, x0, _fmti@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	add	x0, x29, #24
	ldr	w0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #1
	str	w0, [x1]
	adrp	x0, _fmti@page
	add	x0, x0, _fmti@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_xaddr
	ldr	w0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #0
	str	w0, [x1]
	adrp	x0, _fmtx@page
	add	x0, x0, _fmtx@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_xaddroff4
	ldr	w0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #4
	str	w0, [x1]
	adrp	x0, _fmtx@page
	add	x0, x0, _fmtx@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	mov	x0, #8
	bl	_xaddroff
	ldr	w0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #8
	str	w0, [x1]
	adrp	x0, _fmtx@page
	add	x0, x0, _fmtx@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	mov	x0, #3
	bl	_xvalcnt
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x1, sp, x0
	mov	w0, #12
	str	w0, [x1]
	adrp	x0, _fmtx@page
	add	x0, x0, _fmtx@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	mov	w0, #0
	ldp	x29, x30, [sp], 32
	ret
/* end function main */

.text
.balign 4
_thread:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, _i@tlvppage
	ldr	x0, [x0, _i@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	add	x1, x0, #3
	mov	w0, #24
	strb	w0, [x1]
	adrp	x0, _i@tlvppage
	ldr	x0, [x0, _i@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function thread */

.text
.balign 4
_xaddr:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, _x@tlvppage
	ldr	x0, [x0, _x@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	ldp	x29, x30, [sp], 16
	ret
/* end function xaddr */

.text
.balign 4
_xaddroff4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, _x@tlvppage
	ldr	x0, [x0, _x@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	add	x0, x0, #4
	ldp	x29, x30, [sp], 16
	ret
/* end function xaddroff4 */

.text
.balign 4
_xaddroff:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	mov	x19, x0
	adrp	x0, _x@tlvppage
	ldr	x0, [x0, _x@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	mov	x1, x0
	mov	x0, x19
	add	x0, x1, x0
	ldr	x19, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
/* end function xaddroff */

.text
.balign 4
_xvalcnt:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	mov	x1, #4
	mul	x19, x1, x0
	adrp	x0, _x@tlvppage
	ldr	x0, [x0, _x@tlvppageoff]
	ldr	x1, [x0]
	blr	x1
	add	x0, x0, x19
	ldr	w0, [x0]
	ldr	x19, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
/* end function xvalcnt */

