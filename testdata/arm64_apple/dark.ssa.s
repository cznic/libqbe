.data
.balign 8
_ret:
	.quad 0
/* end data */

.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, x0
	adrp	x0, _a@page
	add	x0, x0, _a@pageoff
	ldr	w2, [x0]
	mov	w0, #1
	add	w0, w0, w2
	adrp	x2, _a@page
	add	x2, x2, _a@pageoff
	str	w0, [x2]
	adrp	x0, _ret@page
	add	x0, x0, _ret@pageoff
	ldr	x0, [x0]
	mov	x2, #-8
	add	x1, x1, x2
	ldr	x1, [x1]
	adrp	x2, _ret@page
	add	x2, x2, _ret@pageoff
	str	x1, [x2]
	cmp	x0, x1
	beq	L2
	bl	_test
L2:
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

