.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	add	x1, x29, #24
	add	x0, x29, #16
	cmp	x0, x1
	cset	w0, ne
	ldp	x29, x30, [sp], 32
	ret
/* end function test */

