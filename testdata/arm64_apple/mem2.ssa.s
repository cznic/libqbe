.text
.balign 4
_func:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	add	x1, x29, #24
	mov	w0, #1
	str	w0, [x1]
	mov	x1, #4
	add	x0, x29, #24
	add	x1, x0, x1
	mov	x2, #0
	add	x0, x29, #24
	add	x0, x0, x2
	ldr	w0, [x0]
	mov	x2, #0
	add	x1, x1, x2
	str	w0, [x1]
	add	x1, x29, #24
	mov	w0, #2
	str	w0, [x1]
	mov	x1, #0
	add	x0, x29, #24
	add	x0, x0, x1
	ldr	x0, [x0]
	ldp	x29, x30, [sp], 32
	ret
/* end function func */

.text
.balign 4
.globl _main
_main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	bl	_func
	mov	x2, #0
	add	x1, x29, #24
	add	x1, x1, x2
	str	x0, [x1]
	mov	x1, #4
	add	x0, x29, #24
	add	x0, x0, x1
	ldr	w0, [x0]
	cmp	w0, #1
	beq	L4
	bl	_abort
L4:
	mov	w0, #0
	ldp	x29, x30, [sp], 32
	ret
/* end function main */

