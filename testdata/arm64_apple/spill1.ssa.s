.text
.balign 4
.globl _f
_f:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w16, #0
	mov	w15, #0
	mov	w14, #0
	mov	w13, #0
	mov	w12, #0
	mov	w11, #0
	mov	w10, #0
	mov	w9, #0
	mov	w8, #0
	mov	w7, #0
	mov	w6, #0
	mov	w5, #0
	mov	w4, #0
	mov	w3, #0
	mov	w2, #0
	mov	w1, #0
L2:
	mov	w17, #1
	add	w1, w17, w1
	mov	w17, #2
	add	w2, w17, w2
	mov	w17, #3
	add	w3, w17, w3
	mov	w17, #4
	add	w4, w17, w4
	mov	w17, #5
	add	w5, w17, w5
	mov	w17, #6
	add	w6, w17, w6
	mov	w17, #7
	add	w7, w17, w7
	mov	w17, #8
	add	w8, w17, w8
	mov	w17, #9
	add	w9, w17, w9
	mov	w17, #10
	add	w10, w17, w10
	mov	w17, #11
	add	w11, w17, w11
	mov	w17, #12
	add	w12, w17, w12
	mov	w17, #13
	add	w13, w17, w13
	mov	w17, #14
	add	w14, w17, w14
	mov	w17, #15
	add	w15, w17, w15
	mov	w17, #16
	add	w16, w17, w16
	mov	w17, #1
	sub	w0, w0, w17
	cmp	w0, #0
	bne	L2
	mov	w0, #0
	add	w0, w1, w0
	add	w0, w2, w0
	add	w0, w3, w0
	add	w0, w4, w0
	add	w0, w5, w0
	add	w0, w6, w0
	add	w0, w7, w0
	add	w0, w8, w0
	add	w0, w9, w0
	add	w0, w10, w0
	add	w0, w11, w0
	add	w0, w12, w0
	add	w0, w13, w0
	add	w0, w14, w0
	add	w0, w15, w0
	add	w0, w16, w0
	ldp	x29, x30, [sp], 16
	ret
/* end function f */

