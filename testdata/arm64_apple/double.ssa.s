.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, "Lfp0"@page
	add	x0, x0, "Lfp0"@pageoff
	ldr	d0, [x0]
	mov	w0, #0
L2:
	fadd	d0, d0, d0
	mov	w1, #1
	add	w0, w0, w1
	adrp	x1, "Lfp1"@page
	add	x1, x1, "Lfp1"@pageoff
	ldr	d1, [x1]
	fcmpe	d0, d1
	bls	L2
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp0:
	.int 0
	.int 1016070144 /* 0.000000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp1:
	.int 0
	.int 1072693248 /* 1.000000 */

