.text
.balign 4
.globl _fneg
_fneg:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fmov	w1, s0
	mov	w0, #-2147483648
	eor	w0, w0, w1
	fmov	s0, w0
	ldp	x29, x30, [sp], 16
	ret
/* end function fneg */

.text
.balign 4
.globl _ftrunc
_ftrunc:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzs	w0, d0
	scvtf	d0, w0
	ldp	x29, x30, [sp], 16
	ret
/* end function ftrunc */

.text
.balign 4
.globl _wtos
_wtos:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	s0, w0
	ldp	x29, x30, [sp], 16
	ret
/* end function wtos */

.text
.balign 4
.globl _wtod
_wtod:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	d0, w0
	ldp	x29, x30, [sp], 16
	ret
/* end function wtod */

.text
.balign 4
.globl _ltos
_ltos:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	s0, x0
	ldp	x29, x30, [sp], 16
	ret
/* end function ltos */

.text
.balign 4
.globl _ltod
_ltod:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	ucvtf	d0, x0
	ldp	x29, x30, [sp], 16
	ret
/* end function ltod */

.text
.balign 4
.globl _stow
_stow:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	w0, s0
	ldp	x29, x30, [sp], 16
	ret
/* end function stow */

.text
.balign 4
.globl _dtow
_dtow:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	w0, d0
	ldp	x29, x30, [sp], 16
	ret
/* end function dtow */

.text
.balign 4
.globl _stol
_stol:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	x0, s0
	ldp	x29, x30, [sp], 16
	ret
/* end function stol */

.text
.balign 4
.globl _dtol
_dtol:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcvtzu	x0, d0
	ldp	x29, x30, [sp], 16
	ret
/* end function dtol */

