.text
.balign 4
.globl _t0
_t0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #128
	mov	w2, #256
L2:
	mov	w1, w2
	mov	w2, w0
	mov	w0, #1
	lsr	w0, w1, w0
	cmp	w0, #0
	bne	L2
	mov	w0, w1
	ldp	x29, x30, [sp], 16
	ret
/* end function t0 */

.text
.balign 4
.globl _t1
_t1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w2, #256
	mov	w0, #128
L7:
	mov	w1, w2
	mov	w2, w0
	mov	w0, #1
	lsr	w0, w1, w0
	cmp	w0, #0
	bne	L7
	mov	w0, w1
	ldp	x29, x30, [sp], 16
	ret
/* end function t1 */

