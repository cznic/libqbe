.text
.balign 4
.globl _sum
_sum:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w2, #0
L2:
	mov	w3, w1
	mov	w1, #1
	sub	w1, w3, w1
	cmp	w3, #0
	ble	L4
	sxtw	x4, w1
	mov	x3, #4
	mul	x3, x3, x4
	add	x3, x3, x0
	ldr	w3, [x3]
	add	w2, w3, w2
	b	L2
L4:
	mov	w0, w2
	ldp	x29, x30, [sp], 16
	ret
/* end function sum */

