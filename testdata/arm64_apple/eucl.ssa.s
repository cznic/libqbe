.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w1, #747
	mov	w0, #380
L2:
	sdiv	w18, w1, w0
	msub	w1, w18, w0, w1
	cmp	w1, #0
	beq	L4
	mov	w18, w0
	mov	w0, w1
	mov	w1, w18
	b	L2
L4:
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

