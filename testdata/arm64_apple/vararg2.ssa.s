.text
.balign 4
.globl _qbeprint0
_qbeprint0:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w1, #25637
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #20
	mov	w1, #26405
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #28
	mov	w1, #0
	str	w1, [x2]
	mov	x1, #1
	add	x19, x1, x0
	add	x0, x29, #80
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L1:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L5
	cmp	w0, #103
	beq	L4
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L1
L4:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L1
L5:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint0 */

.text
.balign 4
.globl _qbecall0
_qbecall0:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall0 */

.text
.balign 4
.globl _qbeprint1
_qbeprint1:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x2]
	add	x2, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x2]
	add	x2, x29, #28
	mov	w0, #0
	str	w0, [x2]
	mov	x0, #1
	add	x19, x0, x1
	add	x0, x29, #80
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L10:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L14
	cmp	w0, #103
	beq	L13
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L10
L13:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L10
L14:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint1 */

.text
.balign 4
.globl _qbecall1
_qbecall1:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x0, x1
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall1 */

.text
.balign 4
.globl _qbeprint2
_qbeprint2:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w1, #25637
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #20
	mov	w1, #26405
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #28
	mov	w1, #0
	str	w1, [x2]
	mov	x1, #1
	add	x19, x1, x0
	add	x0, x29, #80
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L19:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L23
	cmp	w0, #103
	beq	L22
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L19
L22:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L19
L23:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint2 */

.text
.balign 4
.globl _qbecall2
_qbecall2:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall2 */

.text
.balign 4
.globl _qbeprint3
_qbeprint3:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x4
	add	x0, x29, #80
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L28:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L32
	cmp	w0, #103
	beq	L31
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L28
L31:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L28
L32:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint3 */

.text
.balign 4
.globl _qbecall3
_qbecall3:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x0, x4
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall3 */

.text
.balign 4
.globl _qbeprint4
_qbeprint4:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x2, x29, #24
	mov	w1, #25637
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #20
	mov	w1, #26405
	movk	w1, #0x20, lsl #16
	str	w1, [x2]
	add	x2, x29, #28
	mov	w1, #0
	str	w1, [x2]
	mov	x1, #1
	add	x19, x1, x0
	add	x0, x29, #80
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L37:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L41
	cmp	w0, #103
	beq	L40
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L37
L40:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L37
L41:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint4 */

.text
.balign 4
.globl _qbecall4
_qbecall4:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall4 */

.text
.balign 4
.globl _qbeprint5
_qbeprint5:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x5
	add	x0, x29, #80
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L46:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L50
	cmp	w0, #103
	beq	L49
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L46
L49:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L46
L50:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint5 */

.text
.balign 4
.globl _qbecall5
_qbecall5:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	mov	x0, x5
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall5 */

.text
.balign 4
.globl _qbeprint6
_qbeprint6:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	ldr	x8, [x29, 104]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x8
	add	x0, x29, #80
	mov	x1, #32
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L55:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L59
	cmp	w0, #103
	beq	L58
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L55
L58:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L55
L59:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint6 */

.text
.balign 4
.globl _qbecall6
_qbecall6:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	ldr	x8, [x29, 72]
	mov	x0, x8
	add	x1, x29, #48
	mov	x2, #32
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall6 */

.text
.balign 4
.globl _qbeprint7
_qbeprint7:
	hint	#34
	stp	x29, x30, [sp, -80]!
	mov	x29, sp
	str	x19, [x29, 72]
	ldr	x8, [x29, 88]
	add	x1, x29, #24
	mov	w0, #25637
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #20
	mov	w0, #26405
	movk	w0, #0x20, lsl #16
	str	w0, [x1]
	add	x1, x29, #28
	mov	w0, #0
	str	w0, [x1]
	mov	x0, #1
	add	x19, x0, x8
	add	x0, x29, #80
	mov	x1, #16
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
L64:
	ldrsb	w0, [x19]
	mov	x1, #3
	add	x19, x1, x19
	cmp	w0, #0
	beq	L68
	cmp	w0, #103
	beq	L67
	add	x0, x29, #32
	ldr	x1, [x0]
	ldr	w0, [x1]
	mov	x2, #8
	add	x1, x1, x2
	add	x2, x29, #32
	str	x1, [x2]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	add	x0, x29, #24
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L64
L67:
	add	x0, x29, #32
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #32
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	add	x0, x29, #20
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	b	L64
L68:
	add	x0, x29, #28
	bl	_puts
	ldr	x19, [x29, 72]
	ldp	x29, x30, [sp], 80
	ret
/* end function qbeprint7 */

.text
.balign 4
.globl _qbecall7
_qbecall7:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	ldr	x8, [x29, 56]
	mov	x0, x8
	add	x1, x29, #48
	mov	x2, #16
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function qbecall7 */

