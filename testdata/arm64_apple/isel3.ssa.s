.text
.balign 4
.globl _slt
_slt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, lt
	ldp	x29, x30, [sp], 16
	ret
/* end function slt */

.text
.balign 4
.globl _sle
_sle:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, le
	ldp	x29, x30, [sp], 16
	ret
/* end function sle */

.text
.balign 4
.globl _sgt
_sgt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, gt
	ldp	x29, x30, [sp], 16
	ret
/* end function sgt */

.text
.balign 4
.globl _sge
_sge:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, ge
	ldp	x29, x30, [sp], 16
	ret
/* end function sge */

.text
.balign 4
.globl _ult
_ult:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, cc
	ldp	x29, x30, [sp], 16
	ret
/* end function ult */

.text
.balign 4
.globl _ule
_ule:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, ls
	ldp	x29, x30, [sp], 16
	ret
/* end function ule */

.text
.balign 4
.globl _ugt
_ugt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, hi
	ldp	x29, x30, [sp], 16
	ret
/* end function ugt */

.text
.balign 4
.globl _uge
_uge:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, cs
	ldp	x29, x30, [sp], 16
	ret
/* end function uge */

.text
.balign 4
.globl _eq
_eq:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, eq
	ldp	x29, x30, [sp], 16
	ret
/* end function eq */

.text
.balign 4
.globl _ne
_ne:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	cmp	w0, w1
	cset	w0, ne
	ldp	x29, x30, [sp], 16
	ret
/* end function ne */

