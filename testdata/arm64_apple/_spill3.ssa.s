.text
.balign 4
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, _a@page
	add	x0, x0, _a@pageoff
	ldr	w0, [x0]
L1:
	cmp	w0, w0
	ble	L1
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

