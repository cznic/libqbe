.text
.balign 4
_alpha:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	add	x2, x0, x2
L1:
	strb	w1, [x0]
	mov	x3, x0
	mov	x0, #1
	add	x0, x3, x0
	mov	w4, #1
	add	w1, w1, w4
	cmp	x3, x2
	bne	L1
	mov	w0, #0
	strb	w0, [x2]
	ldp	x29, x30, [sp], 16
	ret
/* end function alpha */

.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -64]!
	mov	x29, sp
	str	x19, [x29, 56]
	mov	x19, x8
	mov	x2, #16
	mov	w1, #65
	add	x0, x29, #28
	bl	_alpha
	mov	x8, x19
	mov	x1, #0
	add	x0, x29, #28
	add	x0, x0, x1
	ldrb	w0, [x0]
	mov	x1, #0
	add	x1, x8, x1
	strb	w0, [x1]
	mov	x1, #1
	add	x0, x29, #28
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x1, #1
	add	x1, x8, x1
	str	x0, [x1]
	mov	x1, #9
	add	x0, x29, #28
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x1, #9
	add	x1, x8, x1
	str	x0, [x1]
	ldr	x19, [x29, 56]
	ldp	x29, x30, [sp], 64
	ret
/* end function test */

