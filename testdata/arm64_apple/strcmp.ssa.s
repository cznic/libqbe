.text
.balign 4
.globl _strcmp
_strcmp:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
L1:
	ldrsb	w2, [x0]
	cmp	w2, #0
	beq	L5
	ldrsb	w3, [x1]
	cmp	w3, #0
	beq	L5
	cmp	w2, w3
	bne	L5
	mov	x2, #1
	add	x0, x0, x2
	mov	x2, #1
	add	x1, x1, x2
	b	L1
L5:
	uxtb	w0, w2
	ldrb	w1, [x1]
	sub	w0, w0, w1
	ldp	x29, x30, [sp], 16
	ret
/* end function strcmp */

