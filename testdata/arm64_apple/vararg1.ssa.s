.text
.balign 4
.globl _f
_f:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x0, x29, #48
	mov	x1, #0
	add	x0, x0, x1
	add	x1, x29, #16
	str	x0, [x1]
	add	x0, x29, #16
	ldr	x0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #16
	str	x0, [x1]
	add	x0, x29, #16
	ldr	x0, [x0]
	ldr	d0, [x0]
	mov	x1, #8
	add	x0, x0, x1
	add	x1, x29, #16
	str	x0, [x1]
	ldp	x29, x30, [sp], 48
	ret
/* end function f */

.text
.balign 4
.globl _g
_g:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	add	x1, x29, #48
	mov	x2, #0
	add	x1, x1, x2
	add	x2, x29, #16
	str	x1, [x2]
	add	x1, x29, #16
	bl	_print
	ldp	x29, x30, [sp], 48
	ret
/* end function g */

