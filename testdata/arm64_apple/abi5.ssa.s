.data
.balign 8
_fmt1:
	.ascii "t1: %s\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt2:
	.ascii "t2: %d\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt3:
	.ascii "t3: %f %d\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt4:
	.ascii "t4: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt5:
	.ascii "t5: %f %lld\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt6:
	.ascii "t6: %s\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt7:
	.ascii "t7: %f %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt8:
	.ascii "t8: %d %d %d %d\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt9:
	.ascii "t9: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmta:
	.ascii "ta: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmtb:
	.ascii "tb: %d %d %f\n"
	.byte 0
/* end data */

.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -160]!
	mov	x29, sp
	add	x8, x29, #92
	bl	_t1
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x1, sp, x0
	add	x0, x29, #92
	str	x0, [x1]
	adrp	x0, _fmt1@page
	add	x0, x0, _fmt1@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t2
	mov	x2, #0
	add	x1, x29, #84
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #84
	ldr	w0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	adrp	x0, _fmt2@page
	add	x0, x0, _fmt2@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t3
	mov	x2, #0
	add	x1, x29, #76
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #76
	ldr	s0, [x0]
	mov	x1, #4
	add	x0, x29, #76
	add	x0, x0, x1
	ldr	w0, [x0]
	fcvt	d0, s0
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	w0, [x1]
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	adrp	x0, _fmt3@page
	add	x0, x0, _fmt3@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t4
	mov	x3, #8
	add	x2, x29, #144
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #144
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #144
	ldr	w0, [x0]
	add	x2, x29, #144
	mov	x1, #8
	add	x1, x1, x2
	ldr	d0, [x1]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	d0, [x1]
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	adrp	x0, _fmt4@page
	add	x0, x0, _fmt4@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t5
	mov	x3, #8
	add	x2, x29, #128
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #128
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #128
	ldr	s0, [x0]
	fcvt	d0, s0
	mov	x1, #8
	add	x0, x29, #128
	add	x0, x0, x1
	ldr	x0, [x0]
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	x0, [x1]
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	adrp	x0, _fmt5@page
	add	x0, x0, _fmt5@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t6
	mov	x3, #8
	add	x2, x29, #60
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #60
	add	x1, x1, x2
	str	x0, [x1]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x1, sp, x0
	add	x0, x29, #60
	str	x0, [x1]
	adrp	x0, _fmt6@page
	add	x0, x0, _fmt6@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t7
	mov	x3, #8
	add	x2, x29, #112
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #112
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #112
	ldr	s0, [x0]
	fcvt	d0, s0
	mov	x1, #8
	add	x0, x29, #112
	add	x0, x0, x1
	ldr	d1, [x0]
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #8
	add	x0, sp, x0
	str	d1, [x0]
	mov	x0, #0
	add	x0, sp, x0
	str	d0, [x0]
	adrp	x0, _fmt7@page
	add	x0, x0, _fmt7@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_t8
	mov	x3, #8
	add	x2, x29, #44
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #44
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #44
	mov	x0, #4
	add	x1, x0, x1
	mov	x0, #4
	add	x2, x0, x1
	mov	x0, #4
	add	x3, x0, x2
	add	x0, x29, #44
	ldr	w0, [x0]
	ldr	w1, [x1]
	ldr	w2, [x2]
	ldr	w3, [x3]
	mov	x4, #32
	sub	sp, sp, x4
	mov	x4, #24
	add	x4, sp, x4
	str	w3, [x4]
	mov	x3, #16
	add	x3, sp, x3
	str	w2, [x3]
	mov	x2, #8
	add	x2, sp, x2
	str	w1, [x2]
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	adrp	x0, _fmt8@page
	add	x0, x0, _fmt8@pageoff
	bl	_printf
	mov	x0, #32
	add	sp, sp, x0
	bl	_t9
	mov	x2, #0
	add	x1, x29, #36
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #36
	mov	x0, #4
	add	x1, x0, x1
	add	x0, x29, #36
	ldr	w0, [x0]
	ldr	s0, [x1]
	fcvt	d0, s0
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	d0, [x1]
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	adrp	x0, _fmt9@page
	add	x0, x0, _fmt9@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_ta
	mov	x2, #0
	add	x1, x29, #28
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #28
	mov	x0, #4
	add	x1, x0, x1
	add	x0, x29, #28
	ldrsb	w0, [x0]
	ldr	s0, [x1]
	fcvt	d0, s0
	mov	x1, #16
	sub	sp, sp, x1
	mov	x1, #8
	add	x1, sp, x1
	str	d0, [x1]
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	adrp	x0, _fmta@page
	add	x0, x0, _fmta@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	bl	_tb
	mov	x2, #0
	add	x1, x29, #20
	add	x1, x1, x2
	str	x0, [x1]
	add	x1, x29, #20
	mov	x0, #1
	add	x1, x0, x1
	add	x2, x29, #20
	mov	x0, #4
	add	x2, x0, x2
	add	x0, x29, #20
	ldrsb	w0, [x0]
	ldrsb	w1, [x1]
	ldr	s0, [x2]
	fcvt	d0, s0
	mov	x2, #32
	sub	sp, sp, x2
	mov	x2, #16
	add	x2, sp, x2
	str	d0, [x2]
	mov	x2, #8
	add	x2, sp, x2
	str	w1, [x2]
	mov	x1, #0
	add	x1, sp, x1
	str	w0, [x1]
	adrp	x0, _fmtb@page
	add	x0, x0, _fmtb@pageoff
	bl	_printf
	mov	x0, #32
	add	sp, sp, x0
	ldp	x29, x30, [sp], 160
	ret
/* end function test */

