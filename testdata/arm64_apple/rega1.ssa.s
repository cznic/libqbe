.text
.balign 4
.globl _f
_f:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #42
L2:
	mov	x2, #1
	sub	x0, x0, x2
	cmp	w1, #0
	bne	L5
	cmp	w0, #0
	beq	L7
	mov	x1, x0
	b	L2
L5:
	cmp	w0, #0
	beq	L7
	mov	x1, x0
	b	L2
L7:
	mov	x0, x1
	ldp	x29, x30, [sp], 16
	ret
/* end function f */

