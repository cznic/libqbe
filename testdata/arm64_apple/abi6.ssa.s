.data
.balign 8
_dfmt:
	.ascii "double: %g\n"
	.byte 0
/* end data */

.text
.balign 4
.globl _f
_f:
	hint	#34
	stp	x29, x30, [sp, -64]!
	mov	x29, sp
	str	d8, [x29, 56]
	str	d9, [x29, 48]
	ldr	d8, [x29, 80]
	fmov	d9, d6
	mov	x1, #8
	add	x0, x29, #32
	add	x0, x0, x1
	str	s5, [x0]
	mov	x1, #4
	add	x0, x29, #32
	add	x0, x0, x1
	str	s4, [x0]
	mov	x1, #0
	add	x0, x29, #32
	add	x0, x0, x1
	str	s3, [x0]
	mov	x1, #8
	add	x0, x29, #16
	add	x0, x0, x1
	str	s2, [x0]
	mov	x1, #4
	add	x0, x29, #16
	add	x0, x0, x1
	str	s1, [x0]
	mov	x1, #0
	add	x0, x29, #16
	add	x0, x0, x1
	str	s0, [x0]
	mov	x1, #8
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	s2, [x0]
	mov	x1, #4
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	s0, [x0]
	bl	_phfa3
	mov	x1, #8
	add	x0, x29, #32
	add	x0, x0, x1
	ldr	s2, [x0]
	mov	x1, #4
	add	x0, x29, #32
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	add	x0, x29, #32
	add	x0, x0, x1
	ldr	s0, [x0]
	bl	_phfa3
	mov	x1, #8
	add	x0, x29, #64
	add	x0, x0, x1
	ldr	s2, [x0]
	mov	x1, #4
	add	x0, x29, #64
	add	x0, x0, x1
	ldr	s1, [x0]
	mov	x1, #0
	add	x0, x29, #64
	add	x0, x0, x1
	ldr	s0, [x0]
	bl	_phfa3
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d9, [x0]
	adrp	x0, _dfmt@page
	add	x0, x0, _dfmt@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	mov	x0, #16
	sub	sp, sp, x0
	mov	x0, #0
	add	x0, sp, x0
	str	d8, [x0]
	adrp	x0, _dfmt@page
	add	x0, x0, _dfmt@pageoff
	bl	_printf
	mov	x0, #16
	add	sp, sp, x0
	ldr	d8, [x29, 56]
	ldr	d9, [x29, 48]
	ldp	x29, x30, [sp], 64
	ret
/* end function f */

