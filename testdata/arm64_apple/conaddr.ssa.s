.text
.balign 4
.globl _f0
_f0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	add	x0, x1, x0
	ldrb	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f0 */

.text
.balign 4
.globl _f1
_f1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #10
	add	x0, x1, x0
	ldrb	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f1 */

.text
.balign 4
.globl _f2
_f2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #2
	mul	x1, x1, x2
	add	x1, x0, x1
	adrp	x0, _a@page
	add	x0, x0, _a@pageoff
	add	x0, x0, x1
	ldrb	w0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f2 */

.text
.balign 4
.globl _f3
_f3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	add	x0, x0, x1
	ldp	x29, x30, [sp], 16
	ret
/* end function f3 */

.text
.balign 4
.globl _f4
_f4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x1, _p@page
	add	x1, x1, _p@pageoff
	adrp	x0, _p@page
	add	x0, x0, _p@pageoff
	str	x0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function f4 */

.text
.balign 4
.globl _writeto0
_writeto0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #0
	mov	x0, #0
	str	x0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function writeto0 */

