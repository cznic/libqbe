.data
.balign 8
_z:
	.int 0
/* end data */

.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -48]!
	mov	x29, sp
	str	x19, [x29, 40]
	adrp	x0, _z@page
	add	x0, x0, _z@pageoff
	ldr	w0, [x0]
	add	w19, w0, w0
	sxtw	x1, w19
	mov	x2, #12
	add	x0, x29, #16
	add	x2, x0, x2
	add	x3, x29, #16
	mov	x0, #4
	str	x0, [x3]
	mov	w0, #5
	str	w0, [x2]
	adrp	x0, _F@page
	add	x0, x0, _F@pageoff
	add	x7, x0, x1
	mov	w6, #6
	mov	x1, #8
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	x5, [x0]
	mov	x1, #0
	add	x0, x29, #16
	add	x0, x0, x1
	ldr	x4, [x0]
	mov	w3, #3
	mov	w2, #2
	mov	w1, #1
	mov	w0, w19
	blr	x7
	add	w0, w19, w0
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	str	w0, [x1]
	ldr	x19, [x29, 40]
	ldp	x29, x30, [sp], 48
	ret
/* end function test */

