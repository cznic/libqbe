.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x1, #8
	adrp	x0, _s@page
	add	x0, x0, _s@pageoff
	add	x0, x0, x1
	ldr	x1, [x0]
	mov	x2, #0
	adrp	x0, _s@page
	add	x0, x0, _s@pageoff
	add	x0, x0, x2
	ldr	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

