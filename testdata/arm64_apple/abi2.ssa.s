.text
.balign 4
.globl _sum
_sum:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	mov	x3, #8
	add	x2, x29, #16
	add	x2, x2, x3
	str	x1, [x2]
	mov	x2, #0
	add	x1, x29, #16
	add	x1, x1, x2
	str	x0, [x1]
	add	x0, x29, #16
	ldr	s0, [x0]
	add	x1, x29, #16
	mov	x0, #8
	add	x0, x0, x1
	ldr	s1, [x0]
	fadd	s0, s0, s1
	ldp	x29, x30, [sp], 32
	ret
/* end function sum */

