.text
.balign 4
.globl _chacha20_rounds_qbe
_chacha20_rounds_qbe:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	x19, [x29, 24]
	mov	x18, x1
	mov	x1, x0
	mov	x0, x18
	ldr	w16, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w15, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w14, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w13, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w12, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w11, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w10, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w9, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w8, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w7, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w6, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w5, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w4, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w3, [x0]
	mov	x2, #4
	add	x0, x0, x2
	ldr	w2, [x0]
	mov	x17, #4
	add	x0, x0, x17
	ldr	w0, [x0]
	add	w16, w16, w12
	eor	w17, w4, w16
	mov	w4, #16
	lsl	w4, w17, w4
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w4, w4, w17
	add	w8, w8, w4
	eor	w17, w12, w8
	mov	w12, #12
	lsl	w12, w17, w12
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w12, w12, w17
	add	w16, w16, w12
	eor	w17, w4, w16
	mov	w4, #8
	lsl	w4, w17, w4
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w4, w4, w17
	add	w8, w8, w4
	eor	w17, w12, w8
	mov	w12, #7
	lsl	w12, w17, w12
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w12, w12, w17
	add	w15, w15, w11
	eor	w17, w3, w15
	mov	w3, #16
	lsl	w3, w17, w3
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w3, w3, w17
	add	w7, w7, w3
	eor	w17, w11, w7
	mov	w11, #12
	lsl	w11, w17, w11
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w11, w11, w17
	add	w15, w15, w11
	eor	w17, w3, w15
	mov	w3, #8
	lsl	w3, w17, w3
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w3, w3, w17
	add	w7, w7, w3
	eor	w17, w11, w7
	mov	w11, #7
	lsl	w11, w17, w11
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w11, w11, w17
	add	w14, w14, w10
	eor	w17, w2, w14
	mov	w2, #16
	lsl	w2, w17, w2
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w2, w2, w17
	add	w6, w6, w2
	eor	w17, w10, w6
	mov	w10, #12
	lsl	w10, w17, w10
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w10, w10, w17
	add	w14, w14, w10
	eor	w17, w2, w14
	mov	w2, #8
	lsl	w2, w17, w2
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w2, w2, w17
	add	w6, w6, w2
	eor	w17, w10, w6
	mov	w10, #7
	lsl	w10, w17, w10
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w10, w10, w17
	add	w13, w13, w9
	eor	w17, w0, w13
	mov	w0, #16
	lsl	w0, w17, w0
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w0, w0, w17
	add	w5, w5, w0
	eor	w17, w9, w5
	mov	w9, #12
	lsl	w9, w17, w9
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w9, w9, w17
	add	w13, w13, w9
	eor	w17, w0, w13
	mov	w0, #8
	lsl	w0, w17, w0
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w0, w0, w17
	add	w5, w5, w0
	eor	w17, w9, w5
	mov	w9, #7
	lsl	w9, w17, w9
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w9, w9, w17
	add	w16, w16, w11
	eor	w17, w0, w16
	mov	w0, #16
	lsl	w0, w17, w0
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w0, w0, w17
	add	w6, w6, w0
	eor	w17, w11, w6
	mov	w11, #12
	lsl	w11, w17, w11
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w11, w11, w17
	add	w16, w16, w11
	eor	w17, w0, w16
	mov	w0, #8
	lsl	w0, w17, w0
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w0, w0, w17
	add	w6, w6, w0
	eor	w17, w11, w6
	mov	w11, #7
	lsl	w11, w17, w11
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w11, w11, w17
	add	w15, w15, w10
	eor	w17, w4, w15
	mov	w4, #16
	lsl	w4, w17, w4
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w4, w4, w17
	add	w5, w5, w4
	eor	w17, w10, w5
	mov	w10, #12
	lsl	w10, w17, w10
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w10, w10, w17
	add	w15, w15, w10
	eor	w17, w4, w15
	mov	w4, #8
	lsl	w4, w17, w4
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w4, w4, w17
	add	w5, w5, w4
	eor	w17, w10, w5
	mov	w10, #7
	lsl	w10, w17, w10
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w10, w10, w17
	add	w14, w14, w9
	eor	w17, w3, w14
	mov	w3, #16
	lsl	w3, w17, w3
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w3, w3, w17
	add	w8, w8, w3
	eor	w17, w9, w8
	mov	w9, #12
	lsl	w9, w17, w9
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w9, w9, w17
	add	w14, w14, w9
	eor	w17, w3, w14
	mov	w3, #8
	lsl	w3, w17, w3
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w3, w3, w17
	add	w8, w8, w3
	eor	w17, w9, w8
	mov	w9, #7
	lsl	w9, w17, w9
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w9, w9, w17
	add	w13, w13, w12
	eor	w17, w2, w13
	mov	w2, #16
	lsl	w2, w17, w2
	mov	w19, #16
	lsr	w17, w17, w19
	eor	w2, w2, w17
	add	w7, w7, w2
	eor	w17, w12, w7
	mov	w12, #12
	lsl	w12, w17, w12
	mov	w19, #20
	lsr	w17, w17, w19
	eor	w12, w12, w17
	add	w13, w13, w12
	eor	w17, w2, w13
	mov	w2, #8
	lsl	w2, w17, w2
	mov	w19, #24
	lsr	w17, w17, w19
	eor	w2, w2, w17
	add	w7, w7, w2
	eor	w17, w12, w7
	mov	w12, #7
	lsl	w12, w17, w12
	mov	w19, #25
	lsr	w17, w17, w19
	eor	w12, w12, w17
	str	w16, [x1]
	mov	x16, #4
	add	x1, x1, x16
	str	w15, [x1]
	mov	x15, #4
	add	x1, x1, x15
	str	w14, [x1]
	mov	x14, #4
	add	x1, x1, x14
	str	w13, [x1]
	mov	x13, #4
	add	x1, x1, x13
	str	w12, [x1]
	mov	x12, #4
	add	x1, x1, x12
	str	w11, [x1]
	mov	x11, #4
	add	x1, x1, x11
	str	w10, [x1]
	mov	x10, #4
	add	x1, x1, x10
	str	w9, [x1]
	mov	x9, #4
	add	x1, x1, x9
	str	w8, [x1]
	mov	x8, #4
	add	x1, x1, x8
	str	w7, [x1]
	mov	x7, #4
	add	x1, x1, x7
	str	w6, [x1]
	mov	x6, #4
	add	x1, x1, x6
	str	w5, [x1]
	mov	x5, #4
	add	x1, x1, x5
	str	w4, [x1]
	mov	x4, #4
	add	x1, x1, x4
	str	w3, [x1]
	mov	x3, #4
	add	x1, x1, x3
	str	w2, [x1]
	mov	x2, #4
	add	x1, x1, x2
	str	w0, [x1]
	ldr	x19, [x29, 24]
	ldp	x29, x30, [sp], 32
	ret
/* end function chacha20_rounds_qbe */

