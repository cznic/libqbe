.text
.balign 4
.globl _f0
_f0:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #2
	add	x1, x1, x2
	mov	x2, #4
	mul	x1, x1, x2
	add	x0, x0, x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f0 */

.text
.balign 4
.globl _f1
_f1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #1
	add	x1, x2, x1
	mov	x2, #1
	add	x1, x1, x2
	mov	x2, #4
	mul	x1, x1, x2
	add	x0, x1, x0
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f1 */

.text
.balign 4
.globl _f2
_f2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #4
	mul	x2, x1, x2
	mov	x1, #8
	add	x1, x1, x2
	add	x0, x0, x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f2 */

.text
.balign 4
.globl _f3
_f3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #4
	mul	x2, x1, x2
	mov	x1, #4
	add	x2, x1, x2
	mov	x1, #4
	add	x1, x1, x2
	add	x0, x0, x1
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f3 */

.text
.balign 4
.globl _f4
_f4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x2, #1
	add	x1, x2, x1
	mov	x2, #4
	mul	x2, x1, x2
	mov	x1, #4
	add	x1, x1, x2
	add	x0, x1, x0
	ldrsw	x0, [x0]
	ldp	x29, x30, [sp], 16
	ret
/* end function f4 */

