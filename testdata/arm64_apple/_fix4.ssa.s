.text
.balign 4
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #3
	mov	w1, #2
L2:
	mov	w2, #10000
	cmp	w1, w2
	beq	L9
	mov	w2, #2
	add	w0, w0, w2
	mov	w2, #3
L5:
	mul	w3, w2, w2
	cmp	w3, w0
	bgt	L8
	sdiv	w18, w0, w2
	msub	w3, w18, w2, w0
	cmp	w3, #0
	beq	L2
	mov	w3, #2
	add	w2, w2, w3
	b	L5
L8:
	mov	w2, #1
	add	w1, w1, w2
	b	L2
L9:
	adrp	x1, _a@page
	add	x1, x1, _a@pageoff
	str	w0, [x1]
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

