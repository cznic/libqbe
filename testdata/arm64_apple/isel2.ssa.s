.text
.balign 4
.globl _lt
_lt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, mi
	ldp	x29, x30, [sp], 16
	ret
/* end function lt */

.text
.balign 4
.globl _le
_le:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ls
	ldp	x29, x30, [sp], 16
	ret
/* end function le */

.text
.balign 4
.globl _gt
_gt:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, gt
	ldp	x29, x30, [sp], 16
	ret
/* end function gt */

.text
.balign 4
.globl _ge
_ge:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ge
	ldp	x29, x30, [sp], 16
	ret
/* end function ge */

.text
.balign 4
.globl _eq1
_eq1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, eq
	ldp	x29, x30, [sp], 16
	ret
/* end function eq1 */

.text
.balign 4
.globl _eq2
_eq2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	beq	L12
	mov	w0, #0
	b	L13
L12:
	mov	w0, #1
L13:
	ldp	x29, x30, [sp], 16
	ret
/* end function eq2 */

.text
.balign 4
.globl _eq3
_eq3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, eq
	cmp	w0, #0
	bne	L16
	mov	w0, #0
L16:
	ldp	x29, x30, [sp], 16
	ret
/* end function eq3 */

.text
.balign 4
.globl _ne1
_ne1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ne
	ldp	x29, x30, [sp], 16
	ret
/* end function ne1 */

.text
.balign 4
.globl _ne2
_ne2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	bne	L21
	mov	w0, #0
	b	L22
L21:
	mov	w0, #1
L22:
	ldp	x29, x30, [sp], 16
	ret
/* end function ne2 */

.text
.balign 4
.globl _ne3
_ne3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, ne
	cmp	w0, #0
	bne	L25
	mov	w0, #0
L25:
	ldp	x29, x30, [sp], 16
	ret
/* end function ne3 */

.text
.balign 4
.globl _o
_o:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, vc
	ldp	x29, x30, [sp], 16
	ret
/* end function o */

.text
.balign 4
.globl _uo
_uo:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	fcmpe	d0, d1
	cset	w0, vs
	ldp	x29, x30, [sp], 16
	ret
/* end function uo */

