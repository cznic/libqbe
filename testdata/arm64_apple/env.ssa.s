.text
.balign 4
_epar:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	add	x0, x9, x0
	ldp	x29, x30, [sp], 16
	ret
/* end function epar */

.text
.balign 4
.globl _earg
_earg:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x9, x0
	mov	x0, x1
	bl	_epar
	mov	x9, #113
	bl	_labs
	ldp	x29, x30, [sp], 16
	ret
/* end function earg */

