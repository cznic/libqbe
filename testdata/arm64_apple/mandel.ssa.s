.text
.balign 4
_mandel:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	adrp	x0, "Lfp1"@page
	add	x0, x0, "Lfp1"@pageoff
	ldr	d2, [x0]
	fsub	d3, d1, d2
	adrp	x0, "Lfp0"@page
	add	x0, x0, "Lfp0"@pageoff
	ldr	d2, [x0]
	adrp	x0, "Lfp0"@page
	add	x0, x0, "Lfp0"@pageoff
	ldr	d1, [x0]
	mov	w0, #0
L2:
	fmov	d4, d1
	mov	w1, #1
	add	w0, w1, w0
	fmov	d1, d2
	fmul	d2, d4, d2
	fmul	d5, d4, d4
	fmul	d4, d1, d1
	fsub	d1, d5, d4
	fadd	d1, d1, d3
	fadd	d2, d2, d2
	fadd	d2, d2, d0
	fadd	d4, d4, d5
	adrp	x1, "Lfp2"@page
	add	x1, x1, "Lfp2"@pageoff
	ldr	d5, [x1]
	fcmpe	d4, d5
	bgt	L5
	cmp	w0, #1000
	ble	L2
	mov	w0, #0
L5:
	ldp	x29, x30, [sp], 16
	ret
/* end function mandel */

.text
.balign 4
.globl _main
_main:
	hint	#34
	stp	x29, x30, [sp, -32]!
	mov	x29, sp
	str	d8, [x29, 24]
	str	d9, [x29, 16]
	adrp	x0, "Lfp3"@page
	add	x0, x0, "Lfp3"@pageoff
	ldr	d0, [x0]
	fmov	d9, d0
L8:
	adrp	x0, "Lfp3"@page
	add	x0, x0, "Lfp3"@pageoff
	ldr	d0, [x0]
	fmov	d8, d0
L10:
	fmov	d1, d9
	fmov	d0, d8
	bl	_mandel
	cmp	w0, #0
	bne	L12
	mov	w0, #42
	bl	_putchar
	b	L13
L12:
	mov	w0, #32
	bl	_putchar
L13:
	adrp	x0, "Lfp5"@page
	add	x0, x0, "Lfp5"@pageoff
	ldr	d0, [x0]
	fadd	d8, d8, d0
	adrp	x0, "Lfp4"@page
	add	x0, x0, "Lfp4"@pageoff
	ldr	d0, [x0]
	fcmpe	d8, d0
	bls	L10
	mov	w0, #10
	bl	_putchar
	adrp	x0, "Lfp5"@page
	add	x0, x0, "Lfp5"@pageoff
	ldr	d0, [x0]
	fadd	d9, d9, d0
	adrp	x0, "Lfp4"@page
	add	x0, x0, "Lfp4"@pageoff
	ldr	d0, [x0]
	fcmpe	d9, d0
	bls	L8
	mov	w0, #0
	ldr	d8, [x29, 24]
	ldr	d9, [x29, 16]
	ldp	x29, x30, [sp], 32
	ret
/* end function main */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp0:
	.int 0
	.int 0 /* 0.000000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp1:
	.int 0
	.int 1071644672 /* 0.500000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp2:
	.int 0
	.int 1076887552 /* 16.000000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp3:
	.int 0
	.int -1074790400 /* -1.000000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp4:
	.int 0
	.int 1072693248 /* 1.000000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp5:
	.int -755914244
	.int 1067475533 /* 0.032000 */

