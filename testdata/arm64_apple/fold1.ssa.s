.text
.balign 4
.globl _f1
_f1:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #-1
	ldp	x29, x30, [sp], 16
	ret
/* end function f1 */

.text
.balign 4
.globl _f2
_f2:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #-32
	ldp	x29, x30, [sp], 16
	ret
/* end function f2 */

.text
.balign 4
.globl _f3
_f3:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #-5
	ldp	x29, x30, [sp], 16
	ret
/* end function f3 */

.text
.balign 4
.globl _f4
_f4:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
/* end function f4 */

.text
.balign 4
.globl _f5
_f5:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #1
	ldp	x29, x30, [sp], 16
	ret
/* end function f5 */

.text
.balign 4
.globl _f6
_f6:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	w0, #0
	ldp	x29, x30, [sp], 16
	ret
/* end function f6 */

