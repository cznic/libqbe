.text
.balign 4
.globl _test
_test:
	hint	#34
	stp	x29, x30, [sp, -16]!
	mov	x29, sp
	mov	x0, #-1988
L2:
	mov	x1, #1
	add	x0, x1, x0
	cmp	x0, #1991
	blt	L2
	ldp	x29, x30, [sp], 16
	ret
/* end function test */

