.data
.balign 8
_ctoqbestr:
	.ascii "c->qbe(%d)"
	.byte 0
/* end data */

.data
.balign 8
_emptystr:
	.byte 0
/* end data */

.text
.balign 16
.globl _qfn0
_qfn0:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movss 16(%rbp), %xmm8
	movss %xmm8, -16(%rbp)
	movl $0, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	movss -16(%rbp), %xmm0
	callq _ps
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn0 */

.text
.balign 16
.globl _qfn1
_qfn1:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss %xmm0, -16(%rbp)
	movq %rsi, -24(%rbp)
	movl $1, %esi
	movl %edi, %ebx
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl %ebx, %edi
	callq _pw
	movss -16(%rbp), %xmm0
	callq _ps
	leaq -24(%rbp), %rdi
	callq _pfi1
	leaq _emptystr(%rip), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qfn1 */

.text
.balign 16
.globl _qfn2
_qfn2:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss %xmm0, -16(%rbp)
	movq %rsi, -24(%rbp)
	movl $2, %esi
	movl %edi, %ebx
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl %ebx, %edi
	callq _pw
	leaq -24(%rbp), %rdi
	callq _pfi2
	movss -16(%rbp), %xmm0
	callq _ps
	leaq _emptystr(%rip), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qfn2 */

.text
.balign 16
.globl _qfn3
_qfn3:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss %xmm0, -16(%rbp)
	movq %rsi, -24(%rbp)
	movl $3, %esi
	movl %edi, %ebx
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl %ebx, %edi
	callq _pw
	movss -16(%rbp), %xmm0
	callq _ps
	leaq -24(%rbp), %rdi
	callq _pfi3
	leaq _emptystr(%rip), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qfn3 */

.text
.balign 16
.globl _qfn4
_qfn4:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %xmm0, -8(%rbp)
	movl $4, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	leaq -8(%rbp), %rdi
	callq _pss
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn4 */

.text
.balign 16
.globl _qfn5
_qfn5:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	pushq %rbx
	movss 16(%rbp), %xmm8
	movss %xmm8, -16(%rbp)
	movsd %xmm7, %xmm0
	movq %xmm0, -24(%rbp)
	movl $5, %esi
	movq %rdi, %rbx
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	movq %rbx, %rdi
	movq %rdi, %rbx
	leaq -24(%rbp), %rdi
	callq _pss
	movss -16(%rbp), %xmm0
	callq _ps
	movq %rbx, %rdi
	callq _pl
	leaq _emptystr(%rip), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qfn5 */

.text
.balign 16
.globl _qfn6
_qfn6:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %rdi, -16(%rbp)
	movq %rsi, -8(%rbp)
	movl $6, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	leaq -16(%rbp), %rdi
	callq _plb
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn6 */

.text
.balign 16
.globl _qfn7
_qfn7:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $7, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	leaq 24(%rbp), %rdi
	callq _plb
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn7 */

.text
.balign 16
.globl _qfn8
_qfn8:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $8, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	leaq 32(%rbp), %rdi
	callq _plb
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn8 */

.text
.balign 16
.globl _qfn9
_qfn9:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $9, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	leaq 16(%rbp), %rdi
	callq _pbig
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn9 */

.text
.balign 16
.globl _qfn10
_qfn10:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $24, %rsp
	pushq %rbx
	movq 56(%rbp), %rbx
	movss %xmm0, -16(%rbp)
	movq %rbx, %rdi
	movl $10, %esi
	movq %rdi, %rbx
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	movq %rbx, %rdi
	movq %rdi, %rbx
	leaq 32(%rbp), %rdi
	callq _pbig
	movss -16(%rbp), %xmm0
	callq _ps
	movq %rbx, %rdi
	callq _pl
	leaq _emptystr(%rip), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qfn10 */

.text
.balign 16
.globl _qfn11
_qfn11:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl $11, %esi
	leaq _ctoqbestr(%rip), %rdi
	movl $0, %eax
	callq _printf
	leaq 16(%rbp), %rdi
	callq _pddd
	leaq _emptystr(%rip), %rdi
	callq _puts
	leave
	ret
/* end function qfn11 */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %rsp, %rax
	movq $1092511334, 0(%rax)
	movss "Lfp1"(%rip), %xmm7
	movss "Lfp1"(%rip), %xmm6
	movss "Lfp1"(%rip), %xmm5
	movss "Lfp1"(%rip), %xmm4
	movss "Lfp1"(%rip), %xmm3
	movss "Lfp1"(%rip), %xmm2
	movss "Lfp1"(%rip), %xmm1
	movss "Lfp1"(%rip), %xmm0
	callq _cfn0
	subq $-16, %rsp
	movq _fi1(%rip), %rsi
	movss "Lfp2"(%rip), %xmm0
	movl $1, %edi
	callq _cfn1
	movss "Lfp3"(%rip), %xmm0
	movq _fi2(%rip), %rsi
	movl $1, %edi
	callq _cfn2
	movq _fi3(%rip), %rsi
	movss "Lfp2"(%rip), %xmm0
	movl $1, %edi
	callq _cfn3
	movsd _ss(%rip), %xmm0
	callq _cfn4
	subq $16, %rsp
	movq %rsp, %rax
	movq $1092511334, 0(%rax)
	movl $10, %edi
	movsd _ss(%rip), %xmm7
	movsd "Lfp1"(%rip), %xmm6
	movsd "Lfp1"(%rip), %xmm5
	movsd "Lfp1"(%rip), %xmm4
	movsd "Lfp1"(%rip), %xmm3
	movsd "Lfp1"(%rip), %xmm2
	movsd "Lfp1"(%rip), %xmm1
	movsd "Lfp1"(%rip), %xmm0
	callq _cfn5
	subq $-16, %rsp
	movq _lb(%rip), %rdi
	leaq _lb(%rip), %rax
	addq $8, %rax
	movq (%rax), %rsi
	callq _cfn6
	subq $32, %rsp
	movq %rsp, %rax
	leaq _lb(%rip), %rcx
	addq $0, %rcx
	movq (%rcx), %rcx
	movq %rcx, 8(%rax)
	leaq _lb(%rip), %rcx
	addq $8, %rcx
	movq (%rcx), %rcx
	movq %rcx, 16(%rax)
	movq $0, 0(%rax)
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl $0, %edi
	callq _cfn7
	subq $-32, %rsp
	subq $32, %rsp
	movq %rsp, %rax
	leaq _lb(%rip), %rcx
	addq $0, %rcx
	movq (%rcx), %rcx
	movq %rcx, 16(%rax)
	leaq _lb(%rip), %rcx
	addq $8, %rcx
	movq (%rcx), %rcx
	movq %rcx, 24(%rax)
	movq $0, 8(%rax)
	movq $0, 0(%rax)
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl $0, %edi
	callq _cfn8
	subq $-32, %rsp
	subq $32, %rsp
	movq %rsp, %rcx
	leaq _big(%rip), %rax
	addq $0, %rax
	movzbl (%rax), %eax
	movb %al, 0(%rcx)
	leaq _big(%rip), %rax
	addq $1, %rax
	movq (%rax), %rax
	movq %rax, 1(%rcx)
	leaq _big(%rip), %rax
	addq $9, %rax
	movq (%rax), %rax
	movq %rax, 9(%rcx)
	callq _cfn9
	subq $-32, %rsp
	subq $48, %rsp
	movq %rsp, %rax
	movq $11, 40(%rax)
	leaq _big(%rip), %rcx
	addq $0, %rcx
	movzbl (%rcx), %ecx
	movb %cl, 16(%rax)
	leaq _big(%rip), %rcx
	addq $1, %rcx
	movq (%rcx), %rcx
	movq %rcx, 17(%rax)
	leaq _big(%rip), %rcx
	addq $9, %rcx
	movq (%rcx), %rcx
	movq %rcx, 25(%rax)
	movq $0, 8(%rax)
	movq $0, 0(%rax)
	movss "Lfp0"(%rip), %xmm0
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl $0, %edi
	callq _cfn10
	subq $-48, %rsp
	subq $32, %rsp
	movq %rsp, %rcx
	leaq _ddd(%rip), %rax
	addq $0, %rax
	movq (%rax), %rax
	movq %rax, 0(%rcx)
	leaq _ddd(%rip), %rax
	addq $8, %rax
	movq (%rax), %rax
	movq %rax, 8(%rcx)
	leaq _ddd(%rip), %rax
	addq $16, %rax
	movq (%rax), %rax
	movq %rax, 16(%rcx)
	callq _cfn11
	subq $-32, %rsp
	movl $0, %eax
	leave
	ret
/* end function main */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp1:
	.int 0
	.int 0 /* 0.000000 */

.section __TEXT,__literal4,4byte_literals
.p2align 2
Lfp0:
	.int 1092721050 /* 10.100000 */

.section __TEXT,__literal4,4byte_literals
.p2align 2
Lfp2:
	.int 1074580685 /* 2.200000 */

.section __TEXT,__literal4,4byte_literals
.p2align 2
Lfp3:
	.int 1079194419 /* 3.300000 */

