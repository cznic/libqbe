.text
.balign 16
.globl _chk
_chk:
	endbr64
	movl $0, %ecx
	movl $0, %eax
Lbb2:
	movl _glo1(%rip), %r9d
	cmpl %r9d, %ecx
	jge Lbb12
	movq _glo3(%rip), %rdx
	movslq %edi, %r8
	movq (%rdx, %r8, 8), %r8
	movslq %ecx, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
	movslq %ecx, %r8
	movq (%rdx, %r8, 8), %r8
	movslq %esi, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
	movl %edi, %r8d
	addl %ecx, %r8d
	cmpl %r9d, %r8d
	setl %r8b
	movzbl %r8b, %r8d
	movl %esi, %r10d
	addl %ecx, %r10d
	cmpl %r9d, %r10d
	setl %r10b
	movzbl %r10b, %r10d
	testl %r8d, %r10d
	jz Lbb5
	movl %edi, %r8d
	addl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %r8
	movl %esi, %r10d
	addl %ecx, %r10d
	movslq %r10d, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
Lbb5:
	movl %edi, %r8d
	addl %ecx, %r8d
	cmpl %r9d, %r8d
	setl %r8b
	movzbl %r8b, %r8d
	movl %esi, %r10d
	subl %ecx, %r10d
	cmpl $0, %r10d
	setge %r10b
	movzbl %r10b, %r10d
	testl %r8d, %r10d
	jz Lbb7
	movl %edi, %r8d
	addl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %r8
	movl %esi, %r10d
	subl %ecx, %r10d
	movslq %r10d, %r10
	movl (%r8, %r10, 4), %r8d
	addl %r8d, %eax
Lbb7:
	movl %edi, %r8d
	subl %ecx, %r8d
	cmpl $0, %r8d
	setge %r8b
	movzbl %r8b, %r8d
	movl %esi, %r10d
	addl %ecx, %r10d
	cmpl %r9d, %r10d
	setl %r9b
	movzbl %r9b, %r9d
	testl %r8d, %r9d
	jz Lbb9
	movl %edi, %r8d
	subl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %r8
	movl %esi, %r9d
	addl %ecx, %r9d
	movslq %r9d, %r9
	movl (%r8, %r9, 4), %r8d
	addl %r8d, %eax
Lbb9:
	movl %edi, %r8d
	subl %ecx, %r8d
	cmpl $0, %r8d
	setge %r8b
	movzbl %r8b, %r8d
	movl %esi, %r9d
	subl %ecx, %r9d
	cmpl $0, %r9d
	setge %r9b
	movzbl %r9b, %r9d
	testl %r8d, %r9d
	jz Lbb11
	movl %edi, %r8d
	subl %ecx, %r8d
	movslq %r8d, %r8
	movq (%rdx, %r8, 8), %rdx
	movl %esi, %r8d
	subl %ecx, %r8d
	movslq %r8d, %r8
	movl (%rdx, %r8, 4), %edx
	addl %edx, %eax
Lbb11:
	addl $1, %ecx
	jmp Lbb2
Lbb12:
	ret
/* end function chk */

.text
.balign 16
.globl _go
_go:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	pushq %rbx
	pushq %r12
	movl _glo1(%rip), %eax
	cmpl %eax, %edi
	jz Lbb20
	movl $0, %ebx
Lbb15:
	movl _glo1(%rip), %eax
	cmpl %eax, %ebx
	jge Lbb19
	movl %edi, %esi
	movl %edi, %r12d
	movl %ebx, %edi
	callq _chk
	movl %r12d, %edi
	cmpl $0, %eax
	jnz Lbb18
	movq _glo3(%rip), %rax
	movslq %ebx, %rcx
	movq (%rax, %rcx, 8), %rcx
	movslq %edi, %rdx
	movl (%rcx, %rdx, 4), %eax
	addl $1, %eax
	movl %eax, (%rcx, %rdx, 4)
	movl %edi, %r12d
	addl $1, %edi
	callq _go
	movl %r12d, %edi
	movq _glo3(%rip), %rax
	movslq %ebx, %rcx
	movq (%rax, %rcx, 8), %rcx
	movslq %edi, %rdx
	movl (%rcx, %rdx, 4), %eax
	subl $1, %eax
	movl %eax, (%rcx, %rdx, 4)
Lbb18:
	addl $1, %ebx
	jmp Lbb15
Lbb19:
	movl $0, %eax
	jmp Lbb21
Lbb20:
	movl _glo2(%rip), %eax
	addl $1, %eax
	movl %eax, _glo2(%rip)
	movl $0, %eax
Lbb21:
	popq %r12
	popq %rbx
	leave
	ret
/* end function go */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $8, %rsp
	pushq %rbx
	movl $8, _glo1(%rip)
	movl $8, %esi
	movl $8, %edi
	callq _calloc
	movq %rax, _glo3(%rip)
	movl $0, %ebx
Lbb24:
	movl _glo1(%rip), %edi
	cmpl %edi, %ebx
	jge Lbb26
	movl $4, %esi
	callq _calloc
	movq _glo3(%rip), %rcx
	movslq %ebx, %rdx
	movq %rax, (%rcx, %rdx, 8)
	addl $1, %ebx
	jmp Lbb24
Lbb26:
	movl $0, %edi
	callq _go
	movl _glo2(%rip), %eax
	cmpl $92, %eax
	setnz %al
	movzbl %al, %eax
	popq %rbx
	leave
	ret
/* end function main */

.data
.balign 8
_glo1:
	.int 0
/* end data */

.data
.balign 8
_glo2:
	.int 0
/* end data */

.data
.balign 8
_glo3:
	.quad 0
/* end data */

