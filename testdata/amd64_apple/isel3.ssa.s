.text
.balign 16
.globl _slt
_slt:
	endbr64
	cmpl %esi, %edi
	setl %al
	movzbl %al, %eax
	ret
/* end function slt */

.text
.balign 16
.globl _sle
_sle:
	endbr64
	cmpl %esi, %edi
	setle %al
	movzbl %al, %eax
	ret
/* end function sle */

.text
.balign 16
.globl _sgt
_sgt:
	endbr64
	cmpl %esi, %edi
	setg %al
	movzbl %al, %eax
	ret
/* end function sgt */

.text
.balign 16
.globl _sge
_sge:
	endbr64
	cmpl %esi, %edi
	setge %al
	movzbl %al, %eax
	ret
/* end function sge */

.text
.balign 16
.globl _ult
_ult:
	endbr64
	cmpl %esi, %edi
	setb %al
	movzbl %al, %eax
	ret
/* end function ult */

.text
.balign 16
.globl _ule
_ule:
	endbr64
	cmpl %esi, %edi
	setbe %al
	movzbl %al, %eax
	ret
/* end function ule */

.text
.balign 16
.globl _ugt
_ugt:
	endbr64
	cmpl %esi, %edi
	seta %al
	movzbl %al, %eax
	ret
/* end function ugt */

.text
.balign 16
.globl _uge
_uge:
	endbr64
	cmpl %esi, %edi
	setae %al
	movzbl %al, %eax
	ret
/* end function uge */

.text
.balign 16
.globl _eq
_eq:
	endbr64
	cmpl %esi, %edi
	setz %al
	movzbl %al, %eax
	ret
/* end function eq */

.text
.balign 16
.globl _ne
_ne:
	endbr64
	cmpl %esi, %edi
	setnz %al
	movzbl %al, %eax
	ret
/* end function ne */

