.data
.balign 8
_ret:
	.quad 0
/* end data */

.text
.balign 16
.globl _test
_test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movl _a(%rip), %eax
	addl $1, %eax
	movl %eax, _a(%rip)
	movq _ret(%rip), %rcx
	movq 8(%rbp), %rax
	movq %rax, _ret(%rip)
	cmpq %rax, %rcx
	jz Lbb2
	callq _test
Lbb2:
	leave
	ret
/* end function test */

