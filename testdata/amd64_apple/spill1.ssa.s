.text
.balign 16
.globl _f
_f:
	endbr64
	subq $48, %rsp
	pushq %rbx
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %r15
	movl %edi, %r14d
	movl $0, 68(%rsp)
	movl $0, 72(%rsp)
	movl $0, 76(%rsp)
	movl $0, 80(%rsp)
	movl $0, %r13d
	movl $0, %r12d
	movl $0, %ebx
	movl $0, %r11d
	movl $0, %r10d
	movl $0, %r9d
	movl $0, %r8d
	movl $0, %edi
	movl $0, %esi
	movl $0, %edx
	movl $0, %ecx
	movl $0, %eax
Lbb2:
	addl $1, %eax
	addl $2, %ecx
	addl $3, %edx
	addl $4, %esi
	addl $5, %edi
	addl $6, %r8d
	addl $7, %r9d
	addl $8, %r10d
	addl $9, %r11d
	addl $10, %ebx
	addl $11, %r12d
	addl $12, %r13d
	movl $13, %r15d
	addl 80(%rsp), %r15d
	movl %r15d, 64(%rsp)
	movl $14, %r15d
	addl 76(%rsp), %r15d
	movl %r15d, 56(%rsp)
	movl $15, %r15d
	addl 72(%rsp), %r15d
	movl %r15d, 44(%rsp)
	movl $16, %r15d
	addl 68(%rsp), %r15d
	movl %r15d, 48(%rsp)
	subl $1, %r14d
	movl %r14d, 60(%rsp)
	movl 56(%rsp), %r15d
	movl 64(%rsp), %r14d
	jz Lbb4
	movss 48(%rsp), %xmm15
	movss %xmm15, 68(%rsp)
	movss 44(%rsp), %xmm15
	movss %xmm15, 72(%rsp)
	movl %r15d, 76(%rsp)
	movl %r14d, 80(%rsp)
	movl 60(%rsp), %r14d
	jmp Lbb2
Lbb4:
	xchgl %ecx, %eax
	addl $0, %ecx
	movl %ecx, 52(%rsp)
	movl 56(%rsp), %ecx
	addl 52(%rsp), %eax
	movl %eax, 40(%rsp)
	movl 48(%rsp), %r15d
	movl 44(%rsp), %eax
	addl 40(%rsp), %edx
	addl %esi, %edx
	addl %edi, %edx
	addl %r8d, %edx
	addl %r9d, %edx
	addl %r10d, %edx
	addl %r11d, %edx
	addl %ebx, %edx
	addl %r12d, %edx
	addl %r13d, %edx
	addl %r14d, %edx
	addl %edx, %ecx
	addl %ecx, %eax
	addl %r15d, %eax
	popq %r15
	popq %r14
	popq %r13
	popq %r12
	popq %rbx
	addq $48, %rsp
	ret
/* end function f */

