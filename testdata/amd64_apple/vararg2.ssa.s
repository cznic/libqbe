.text
.balign 16
.globl _qbeprint0
_qbeprint0:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %rdi, %rbx
	movl $8, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb1:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb11
	cmpl $103, %eax
	jz Lbb7
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb5
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb6
Lbb5:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb6:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb1
Lbb7:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb9
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb10
Lbb9:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb10:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb1
Lbb11:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint0 */

.text
.balign 16
.globl _qbecall0
_qbecall0:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movl $8, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall0 */

.text
.balign 16
.globl _qbeprint1
_qbeprint1:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %rsi, %rbx
	movl $16, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb16:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb26
	cmpl $103, %eax
	jz Lbb22
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb20
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb21
Lbb20:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb21:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb16
Lbb22:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb24
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb25
Lbb24:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb25:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb16
Lbb26:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint1 */

.text
.balign 16
.globl _qbecall1
_qbecall1:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movq %rsi, %rdi
	movl $16, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall1 */

.text
.balign 16
.globl _qbeprint2
_qbeprint2:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %rdi, %rbx
	movl $8, -208(%rbp)
	movl $64, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb31:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb41
	cmpl $103, %eax
	jz Lbb37
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb35
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb36
Lbb35:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb36:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb31
Lbb37:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb39
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb40
Lbb39:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb40:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb31
Lbb41:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint2 */

.text
.balign 16
.globl _qbecall2
_qbecall2:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movl $8, -208(%rbp)
	movl $64, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall2 */

.text
.balign 16
.globl _qbeprint3
_qbeprint3:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %r8, %rbx
	movl $40, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb46:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb56
	cmpl $103, %eax
	jz Lbb52
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb50
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb51
Lbb50:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb51:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb46
Lbb52:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb54
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb55
Lbb54:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb55:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb46
Lbb56:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint3 */

.text
.balign 16
.globl _qbecall3
_qbecall3:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movq %r8, %rdi
	movl $40, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall3 */

.text
.balign 16
.globl _qbeprint4
_qbeprint4:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %rdi, %rbx
	movl $8, -208(%rbp)
	movl $144, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb61:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb71
	cmpl $103, %eax
	jz Lbb67
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb65
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb66
Lbb65:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb66:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb61
Lbb67:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb69
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb70
Lbb69:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb70:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb61
Lbb71:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint4 */

.text
.balign 16
.globl _qbecall4
_qbecall4:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movl $8, -208(%rbp)
	movl $144, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall4 */

.text
.balign 16
.globl _qbeprint5
_qbeprint5:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %r9, %rbx
	movl $48, -208(%rbp)
	movl $160, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb76:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb86
	cmpl $103, %eax
	jz Lbb82
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb80
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb81
Lbb80:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb81:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb76
Lbb82:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb84
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb85
Lbb84:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb85:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb76
Lbb86:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint5 */

.text
.balign 16
.globl _qbecall5
_qbecall5:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movq %r9, %rdi
	movl $48, -208(%rbp)
	movl $160, -204(%rbp)
	movq %rbp, %rax
	addq $16, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall5 */

.text
.balign 16
.globl _qbeprint6
_qbeprint6:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movq 64(%rbp), %rax
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %rax, %rbx
	movl $48, -208(%rbp)
	movl $176, -204(%rbp)
	movq %rbp, %rax
	addq $72, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb91:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb101
	cmpl $103, %eax
	jz Lbb97
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb95
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb96
Lbb95:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb96:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb91
Lbb97:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb99
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb100
Lbb99:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb100:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb91
Lbb101:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint6 */

.text
.balign 16
.globl _qbecall6
_qbecall6:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movq 64(%rbp), %rax
	movq %rax, %rdi
	movl $48, -208(%rbp)
	movl $176, -204(%rbp)
	movq %rbp, %rax
	addq $72, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall6 */

.text
.balign 16
.globl _qbeprint7
_qbeprint7:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $232, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	pushq %rbx
	movq 40(%rbp), %rax
	movl $2122789, -216(%rbp)
	movl $2123557, -220(%rbp)
	movl $0, -212(%rbp)
	movl $1, %ebx
	addq %rax, %rbx
	movl $48, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $48, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
Lbb106:
	movsbl (%rbx), %eax
	addq $3, %rbx
	cmpl $0, %eax
	jz Lbb116
	cmpl $103, %eax
	jz Lbb112
	movslq -208(%rbp), %rcx
	cmpl $48, %ecx
	jb Lbb110
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb111
Lbb110:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $8, %ecx
	movl %ecx, -208(%rbp)
Lbb111:
	movl (%rax), %esi
	leaq -216(%rbp), %rdi
	movl $0, %eax
	callq _printf
	jmp Lbb106
Lbb112:
	movslq -204(%rbp), %rcx
	cmpl $176, %ecx
	jb Lbb114
	movq -200(%rbp), %rax
	movq %rax, %rcx
	addq $8, %rcx
	movq %rcx, -200(%rbp)
	jmp Lbb115
Lbb114:
	movq -192(%rbp), %rax
	addq %rcx, %rax
	addl $16, %ecx
	movl %ecx, -204(%rbp)
Lbb115:
	movsd (%rax), %xmm0
	leaq -220(%rbp), %rdi
	movl $1, %eax
	callq _printf
	jmp Lbb106
Lbb116:
	leaq -212(%rbp), %rdi
	callq _puts
	popq %rbx
	leave
	ret
/* end function qbeprint7 */

.text
.balign 16
.globl _qbecall7
_qbecall7:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $208, %rsp
	movq %rdi, -176(%rbp)
	movq %rsi, -168(%rbp)
	movq %rdx, -160(%rbp)
	movq %rcx, -152(%rbp)
	movq %r8, -144(%rbp)
	movq %r9, -136(%rbp)
	movaps %xmm0, -128(%rbp)
	movaps %xmm1, -112(%rbp)
	movaps %xmm2, -96(%rbp)
	movaps %xmm3, -80(%rbp)
	movaps %xmm4, -64(%rbp)
	movaps %xmm5, -48(%rbp)
	movaps %xmm6, -32(%rbp)
	movaps %xmm7, -16(%rbp)
	movq 40(%rbp), %rax
	movq %rax, %rdi
	movl $48, -208(%rbp)
	movl $48, -204(%rbp)
	movq %rbp, %rax
	addq $48, %rax
	movq %rax, -200(%rbp)
	movq %rbp, %rax
	addq $-176, %rax
	movq %rax, -192(%rbp)
	leaq -208(%rbp), %rsi
	callq _print
	leave
	ret
/* end function qbecall7 */

