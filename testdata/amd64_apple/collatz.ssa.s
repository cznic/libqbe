.text
.balign 16
.globl _test
_test:
	endbr64
	subq $4000, %rsp
	movl $0, %esi
	movl $1, %ecx
Lbb2:
	cmpl $1000, %ecx
	jge Lbb14
	movl %ecx, %edx
	movl $0, %eax
Lbb4:
	cmpl $1, %ecx
	jz Lbb11
	cmpl %edx, %ecx
	jl Lbb9
	addl $1, %eax
	testl $1, %ecx
	jnz Lbb8
	shrl $1, %ecx
	jmp Lbb4
Lbb8:
	imull $3, %ecx, %ecx
	addl $1, %ecx
	jmp Lbb4
Lbb9:
	xchgl %ecx, %edx
	movslq %edx, %rdx
	movl 0(%rsp, %rdx, 4), %edx
	addl %edx, %eax
	jmp Lbb12
Lbb11:
	movl %edx, %ecx
Lbb12:
	movslq %ecx, %rdx
	movl %eax, 0(%rsp, %rdx, 4)
	movl %ecx, %edx
	addl $1, %ecx
	cmpl %eax, %esi
	jg Lbb2
	movl %eax, %esi
	jmp Lbb2
Lbb14:
	movl %esi, _a(%rip)
	addq $4000, %rsp
	ret
/* end function test */

