.text
.balign 16
_g:
	endbr64
	ret
/* end function g */

.text
.balign 16
_f:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $8, %rsp
	pushq %rbx
	movl %edi, %ebx
	callq _g
	movl %ebx, %eax
	subq $16, %rsp
	movq %rsp, %rdx
	movq $180388626474, %rcx
	movq %rcx, (%rdx)
	movq $180388626474, %rcx
	movq %rcx, 8(%rdx)
	movq %rbp, %rsp
	subq $16, %rsp
	popq %rbx
	leave
	ret
/* end function f */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $8, %rsp
	pushq %rbx
	movl $0, %edi
	callq _f
	movl %eax, %ebx
	movl $0, %edi
	callq _f
	movl %ebx, %eax
	popq %rbx
	leave
	ret
/* end function main */

