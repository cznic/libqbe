.text
.balign 16
.globl _f
_f:
	endbr64
	imulq $8, %rdi, %rcx
	leaq _a(%rip), %rax
	addq %rcx, %rax
	imulq $4, %rsi, %rcx
	addq %rcx, %rax
	movl (%rax), %eax
	ret
/* end function f */

