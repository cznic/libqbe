.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movsd "Lfp0"(%rip), %xmm0
	leaq _fmt(%rip), %rdi
	movl $1, %eax
	callq _printf
	movl $0, %eax
	leave
	ret
/* end function main */

.data
.balign 8
_fmt:
	.ascii "%.06f\n"
	.byte 0
/* end data */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp0:
	.int 858993459
	.int 1072902963 /* 1.200000 */

