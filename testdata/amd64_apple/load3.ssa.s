.text
.balign 16
_rand:
	endbr64
	movl $0, %eax
	ret
/* end function rand */

.text
.balign 16
_chk:
	endbr64
	cmpl $1, %edi
	setz %al
	movzbl %al, %eax
	cmpl $0, %esi
	setz %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	xorl $1, %eax
	ret
/* end function chk */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movl $1, -8(%rbp)
	movl $0, -4(%rbp)
	callq _rand
	cmpl $0, %eax
	movl $0, %esi
	movl $1, %edi
	callq _chk
	leave
	ret
/* end function main */

