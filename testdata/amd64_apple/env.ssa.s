.text
.balign 16
_epar:
	endbr64
	addq %rdi, %rax
	ret
/* end function epar */

.text
.balign 16
.globl _earg
_earg:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	movq %rdi, %rax
	movq %rsi, %rdi
	callq _epar
	movq %rax, %rdi
	movl $113, %eax
	callq _labs
	leave
	ret
/* end function earg */

