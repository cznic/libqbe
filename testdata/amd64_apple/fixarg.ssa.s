.text
.balign 16
.globl _test
_test:
	endbr64
	subq $16, %rsp
	leaq 0(%rsp), %rcx
	leaq 8(%rsp), %rax
	cmpq %rax, %rcx
	setnz %al
	movzbl %al, %eax
	addq $16, %rsp
	ret
/* end function test */

