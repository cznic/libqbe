.text
.balign 16
.globl _fneg
_fneg:
	endbr64
	movq %xmm0, %rax
	xorl $2147483648, %eax
	movq %rax, %xmm0
	ret
/* end function fneg */

.text
.balign 16
.globl _ftrunc
_ftrunc:
	endbr64
	cvttsd2sil %xmm0, %eax
	cvtsi2sd %eax, %xmm0
	ret
/* end function ftrunc */

.text
.balign 16
.globl _wtos
_wtos:
	endbr64
	movl %edi, %eax
	cvtsi2ss %rax, %xmm0
	ret
/* end function wtos */

.text
.balign 16
.globl _wtod
_wtod:
	endbr64
	movl %edi, %eax
	cvtsi2sd %rax, %xmm0
	ret
/* end function wtod */

.text
.balign 16
.globl _ltos
_ltos:
	endbr64
	movq %rdi, %rax
	andq $1, %rax
	movq %rdi, %rdx
	shrq $63, %rdx
	movl %edx, %ecx
	movq %rdi, %rsi
	shrq %cl, %rsi
	orq %rsi, %rax
	cvtsi2ss %rax, %xmm0
	movq %xmm0, %rax
	movl %edx, %ecx
	shll $23, %ecx
	addl %ecx, %eax
	movq %rax, %xmm0
	ret
/* end function ltos */

.text
.balign 16
.globl _ltod
_ltod:
	endbr64
	movq %rdi, %rax
	andq $1, %rax
	movq %rdi, %rdx
	shrq $63, %rdx
	movl %edx, %ecx
	movq %rdi, %rsi
	shrq %cl, %rsi
	orq %rsi, %rax
	cvtsi2sd %rax, %xmm0
	movq %xmm0, %rax
	movq %rdx, %rcx
	shlq $52, %rcx
	addq %rcx, %rax
	movq %rax, %xmm0
	ret
/* end function ltod */

.text
.balign 16
.globl _stow
_stow:
	endbr64
	cvttss2siq %xmm0, %rax
	ret
/* end function stow */

.text
.balign 16
.globl _dtow
_dtow:
	endbr64
	cvttsd2siq %xmm0, %rax
	ret
/* end function dtow */

.text
.balign 16
.globl _stol
_stol:
	endbr64
	cvttss2siq %xmm0, %rax
	movq %rax, %rdx
	sarq $63, %rdx
	addss "Lfp0"(%rip), %xmm0
	cvttss2siq %xmm0, %rcx
	andq %rdx, %rcx
	orq %rcx, %rax
	ret
/* end function stol */

.text
.balign 16
.globl _dtol
_dtol:
	endbr64
	cvttsd2siq %xmm0, %rax
	movq %rax, %rdx
	sarq $63, %rdx
	addsd "Lfp1"(%rip), %xmm0
	cvttsd2siq %xmm0, %rcx
	andq %rdx, %rcx
	orq %rcx, %rax
	ret
/* end function dtol */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp1:
	.int 0
	.int -1008730112 /* -9223372036854775808.000000 */

.section __TEXT,__literal4,4byte_literals
.p2align 2
Lfp0:
	.int -553648128 /* -9223372036854775808.000000 */

