.text
.balign 16
.globl _sum
_sum:
	endbr64
	movl $0, %eax
Lbb2:
	movl %esi, %ecx
	subl $1, %esi
	cmpl $0, %ecx
	jle Lbb4
	movslq %esi, %rcx
	movl (%rdi, %rcx, 4), %ecx
	addl %ecx, %eax
	jmp Lbb2
Lbb4:
	ret
/* end function sum */

