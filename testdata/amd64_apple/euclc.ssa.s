.text
.balign 16
.globl _test
_test:
	endbr64
	movl $747, %esi
	movl $380, %ecx
Lbb2:
	cmpl $0, %ecx
	jz Lbb5
	movl %esi, %eax
	cltd
	idivl %ecx
	movl %edx, %esi
	xchgl %ecx, %esi
	jmp Lbb2
Lbb5:
	movl %esi, %eax
	ret
/* end function test */

