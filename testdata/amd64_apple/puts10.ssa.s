.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $24, %rsp
	pushq %rbx
	movb $0, -3(%rbp)
	movl $0, %ebx
Lbb2:
	movl %ebx, %eax
	addl $48, %eax
	movb %al, -4(%rbp)
	leaq -4(%rbp), %rdi
	callq _puts
	addl $1, %ebx
	cmpl $9, %ebx
	jle Lbb2
	movl $0, %eax
	popq %rbx
	leave
	ret
/* end function main */

