.text
.balign 16
.globl _main
_main:
	endbr64
	subq $16, %rsp
	movl $4, 0(%rsp)
	movl $5, 4(%rsp)
	leaq 0(%rsp), %rax
Lbb1:
	movl (%rax), %eax
	cmpl $5, %eax
	leaq 4(%rsp), %rax
	jnz Lbb1
	movl $0, %eax
	addq $16, %rsp
	ret
/* end function main */

