.text
.balign 16
.globl _main
_main:
	endbr64
	movl $5, %r8d
	movl $11, %ecx
	movl $12, %esi
Lbb2:
	cmpl $201, %r8d
	jz Lbb18
	movl %ecx, %r9d
	movl $2, %ecx
	movl %esi, %eax
	cltd
	idivl %ecx
	movl %edx, %eax
	cmpl $0, %eax
	jz Lbb5
	movl $1, %edi
	jmp Lbb6
Lbb5:
	movl $0, %edi
Lbb6:
	movl %esi, %ecx
	movl $3, %esi
Lbb7:
	cmpl %ecx, %esi
	jge Lbb12
	movl %ecx, %eax
	cltd
	idivl %esi
	movl %edx, %eax
	cmpl $0, %eax
	jz Lbb10
	addl $2, %esi
	jmp Lbb7
Lbb10:
	movl %ecx, %esi
	movl $0, %edi
	jmp Lbb13
Lbb12:
	movl %ecx, %esi
Lbb13:
	cmpl $0, %edi
	jnz Lbb15
	movl %r9d, %ecx
	jmp Lbb17
Lbb15:
	addl $1, %r8d
	movl %esi, %ecx
Lbb17:
	addl $1, %esi
	jmp Lbb2
Lbb18:
	cmpl $1229, %ecx
	jnz Lbb20
	movl $0, %eax
	jmp Lbb21
Lbb20:
	movl $1, %eax
Lbb21:
	ret
/* end function main */

