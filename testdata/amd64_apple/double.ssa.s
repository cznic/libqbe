.text
.balign 16
.globl _test
_test:
	endbr64
	movl $0, %eax
	movsd "Lfp0"(%rip), %xmm0
Lbb2:
	addsd %xmm0, %xmm0
	addl $1, %eax
	movsd "Lfp1"(%rip), %xmm1
	ucomisd %xmm0, %xmm1
	jae Lbb2
	movl %eax, _a(%rip)
	ret
/* end function test */

/* floating point constants */
.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp0:
	.int 0
	.int 1016070144 /* 0.000000 */

.section __TEXT,__literal8,8byte_literals
.p2align 3
Lfp1:
	.int 0
	.int 1072693248 /* 1.000000 */

