.text
.balign 16
.globl _f1
_f1:
	endbr64
	movl $4294967295, %eax
	ret
/* end function f1 */

.text
.balign 16
.globl _f2
_f2:
	endbr64
	movl $4294967264, %eax
	ret
/* end function f2 */

.text
.balign 16
.globl _f3
_f3:
	endbr64
	movl $4294967291, %eax
	ret
/* end function f3 */

.text
.balign 16
.globl _f4
_f4:
	endbr64
	movl $0, %eax
	ret
/* end function f4 */

.text
.balign 16
.globl _f5
_f5:
	endbr64
	movl $1, %eax
	ret
/* end function f5 */

.text
.balign 16
.globl _f6
_f6:
	endbr64
	movl $0, %eax
	ret
/* end function f6 */

