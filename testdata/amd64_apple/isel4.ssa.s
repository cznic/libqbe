.text
.balign 16
.globl _f0
_f0:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
/* end function f0 */

.text
.balign 16
.globl _f1
_f1:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
/* end function f1 */

.text
.balign 16
.globl _f2
_f2:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
/* end function f2 */

.text
.balign 16
.globl _f3
_f3:
	endbr64
	imulq $4, %rsi, %rax
	movslq 8(%rdi, %rax, 1), %rax
	ret
/* end function f3 */

.text
.balign 16
.globl _f4
_f4:
	endbr64
	movslq 8(%rdi, %rsi, 4), %rax
	ret
/* end function f4 */

