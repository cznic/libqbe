.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $4096, %rsp
	pushq %rbx
	pushq %r12
	movl $4096, %edx
	movl $0, %esi
	leaq -4096(%rbp), %rdi
	callq _memset
	leaq -4096(%rbp), %rax
	addq $8, %rax
	movq -4088(%rbp), %rcx
	addq $7, %rcx
	movq %rcx, -4088(%rbp)
	movq %rax, %rcx
	addq $8, %rcx
	movq -4080(%rbp), %rax
	addq $10, %rax
	movq %rax, -4080(%rbp)
	movq %rcx, %rax
Lbb2:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb4
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $5, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb2
Lbb4:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $7, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rcx)
Lbb6:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb8
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $5, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb6
Lbb8:
	movq %rdx, %rcx
	addq $10, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $9, %rdx
	movq %rdx, 8(%rax)
	addq $16, %rcx
	movq 24(%rax), %rdx
	addq $8, %rdx
	movq %rdx, 24(%rax)
	movq %rcx, %rax
Lbb10:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb12
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb10
Lbb12:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $14, %rdx
	movq %rdx, 8(%rcx)
Lbb14:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb16
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $7, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb14
Lbb16:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb18:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb20
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb18
Lbb20:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $2, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rcx)
Lbb22:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb24
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb22
Lbb24:
	movq %rdx, %rcx
	addq $2, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb26:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb28
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb26
Lbb28:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rcx)
Lbb30:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb32
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $9, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb30
Lbb32:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb34:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb36
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb34
Lbb36:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rcx)
Lbb38:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb40
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb38
Lbb40:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb42:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb44
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb42
Lbb44:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb46:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb48
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb46
Lbb48:
	movq %rdx, %rcx
	addq $2, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb50:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb52
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb50
Lbb52:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rcx)
Lbb54:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb56
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb54
Lbb56:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $14, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb58:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb60
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $7, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb58
Lbb60:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rcx)
Lbb62:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb64
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb62
Lbb64:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb66:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb68
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb66
Lbb68:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rcx)
Lbb70:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb72
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb70
Lbb72:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb74:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb76
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb74
Lbb76:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb78:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb80
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb78
Lbb80:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb82:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb84
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb82
Lbb84:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rcx)
Lbb86:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb88
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb86
Lbb88:
	movq %rdx, %rcx
	addq $2, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb90:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb92
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb90
Lbb92:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $4, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rcx)
Lbb94:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb96
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb94
Lbb96:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb98:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb100
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb98
Lbb100:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rcx)
Lbb102:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb104
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb102
Lbb104:
	movq %rdx, %rcx
	addq $2, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $13, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb106:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb108
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $9, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb106
Lbb108:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rcx)
Lbb110:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb112
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb110
Lbb112:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb114:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb116
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $9, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb114
Lbb116:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rcx)
Lbb118:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb120
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $9, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb118
Lbb120:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $5, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb122:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb124
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $2, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb122
Lbb124:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $2, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rcx)
Lbb126:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb128
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb126
Lbb128:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb130:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb132
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb130
Lbb132:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $3, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $13, %rdx
	movq %rdx, 8(%rcx)
Lbb134:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb136
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb134
Lbb136:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb138:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb140
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb138
Lbb140:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rcx)
Lbb142:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb144
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb142
Lbb144:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb146:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb148
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb146
Lbb148:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb150:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb152
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb150
Lbb152:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb154:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb156
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb154
Lbb156:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rcx)
Lbb158:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb160
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb158
Lbb160:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb162:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb164
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb162
Lbb164:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb166:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb168
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb166
Lbb168:
	movq %rdx, %rcx
	addq $2, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $13, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb170:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb172
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $9, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb170
Lbb172:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb174:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb176
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb174
Lbb176:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb178:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb180
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb178
Lbb180:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rcx)
Lbb182:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb184
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb182
Lbb184:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb186:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb188
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb186
Lbb188:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rcx)
Lbb190:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb192
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb190
Lbb192:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb194:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb196
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb194
Lbb196:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $2, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb198:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb200
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb198
Lbb200:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb202:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb204
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb202
Lbb204:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rcx)
Lbb206:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb208
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb206
Lbb208:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb210:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb212
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb210
Lbb212:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rcx)
Lbb214:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb216
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb214
Lbb216:
	movq %rdx, %rcx
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $13, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb218:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb220
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb218
Lbb220:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $2, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rcx)
Lbb222:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb224
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb222
Lbb224:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $8, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb226:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb228
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $4, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb226
Lbb228:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $12, %rdx
	movq %rdx, 8(%rcx)
Lbb230:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb232
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $8, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb230
Lbb232:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $19, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb234:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb236
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $6, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb234
Lbb236:
	movq %rax, %rcx
	movq %rdx, %rax
	addq $1, %rax
	movq %rax, (%rcx)
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb238:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb240
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb238
Lbb240:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $13, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb242:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb244
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $9, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb242
Lbb244:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $11, %rdx
	movq %rdx, 8(%rcx)
Lbb246:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb248
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb246
Lbb248:
	movq %rax, %rcx
	addq $8, %rcx
	movq 8(%rax), %rdx
	addq $10, %rdx
	movq %rdx, 8(%rax)
	movq %rcx, %rax
Lbb250:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb252
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $10, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb250
Lbb252:
	movq %rax, %rcx
	movq %rcx, %rax
	addq $8, %rax
	movq 8(%rcx), %rdx
	addq $5, %rdx
	movq %rdx, 8(%rcx)
Lbb254:
	movq (%rax), %rdx
	cmpl $0, %edx
	jz Lbb256
	movq %rax, %rcx
	addq $-8, %rax
	movq -8(%rcx), %rsi
	addq $2, %rsi
	movq %rsi, -8(%rcx)
	addq $8, %rax
	addq $-1, %rdx
	movq %rdx, 0(%rcx)
	jmp Lbb254
Lbb256:
	movq %rdx, %rcx
	addq $13, %rcx
	movq %rcx, (%rax)
Lbb257:
	movq (%rax), %rcx
	cmpl $0, %ecx
	jz Lbb259
	addq $-8, %rax
	jmp Lbb257
Lbb259:
	addq $32, %rax
Lbb260:
	movq (%rax), %rcx
	cmpl $0, %ecx
	jz Lbb294
	movq %rax, %rbx
	addq $-8, %rbx
Lbb262:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb282
Lbb263:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb265
	addq $8, %rbx
	jmp Lbb263
Lbb265:
	movq %rbx, %r12
	addq $-16, %rbx
	movq -16(%r12), %rdi
	callq _putchar
	movq -16(%r12), %rdi
	callq _putchar
Lbb266:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb268
	addq $-8, %rbx
	jmp Lbb266
Lbb268:
	movq %rbx, %r12
	addq $8, %r12
	movq 8(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 16(%rbx), %rdi
	callq _putchar
	addq $24, %r12
	movq 40(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 48(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 56(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 64(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 72(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 80(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 88(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 96(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 104(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 112(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 120(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 128(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 136(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 144(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 152(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 160(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 168(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 176(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 184(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 192(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 200(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 208(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 216(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 224(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 232(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 240(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 248(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 256(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 264(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb270:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb272
	addq $-8, %rbx
	jmp Lbb270
Lbb272:
	movq %rbx, %r12
	movq %r12, %rbx
	addq $8, %rbx
	movq 8(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 16(%r12), %rdi
	callq _putchar
	addq $24, %rbx
	movq 40(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 48(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 56(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 64(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 72(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 80(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 88(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 96(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 104(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 112(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 120(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 128(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 136(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 144(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 152(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 160(%r12), %rdi
	callq _putchar
	addq $104, %rbx
	movq 264(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 272(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 280(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 288(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 296(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 304(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 312(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 320(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 328(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 336(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 344(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 352(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 360(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 368(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 376(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 384(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 392(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 400(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 408(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 416(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 424(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 432(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 440(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 448(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 456(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 464(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 472(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 480(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 488(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 496(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 504(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 512(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 520(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 528(%r12), %rdi
	callq _putchar
Lbb274:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb276
	addq $-8, %rbx
	jmp Lbb274
Lbb276:
	movq %rbx, %rax
	addq $16, %rax
	movq 16(%rbx), %rcx
	addq $-1, %rcx
	movq %rcx, 16(%rbx)
	movq %rax, %r12
	addq $-8, %r12
	movq 8(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 16(%rbx), %rdi
	callq _putchar
	addq $24, %r12
	movq 40(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 48(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 56(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 64(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 72(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 80(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 88(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 96(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 104(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 112(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 120(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 128(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 136(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 144(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 152(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 160(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 168(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 176(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 184(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 192(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 200(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 208(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 216(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 224(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 232(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 240(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 248(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 256(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb278:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb280
	addq $-8, %rbx
	jmp Lbb278
Lbb280:
	movq %rbx, %r12
	movq %r12, %rbx
	addq $24, %rbx
	movq 24(%r12), %rax
	addq $-1, %rax
	movq %rax, 24(%r12)
	jmp Lbb262
Lbb282:
	addq $10, %rax
	movq %rax, (%rbx)
	movq %rbx, %rax
	addq $-8, %rax
	movq -8(%rbx), %rcx
	addq $10, %rcx
	movq %rcx, -8(%rbx)
	addq $-8, %rax
	movq -16(%rbx), %rcx
	addq $-1, %rcx
	movq %rcx, -16(%rbx)
Lbb283:
	movq (%rax), %rcx
	cmpl $0, %ecx
	jz Lbb285
	addq $8, %rax
	jmp Lbb283
Lbb285:
	movq %rax, %r12
	addq $-8, %r12
	movq -8(%rax), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb287:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb289
	addq $-8, %rbx
	jmp Lbb287
Lbb289:
	movq %rbx, %r12
	movq %r12, %rbx
	addq $8, %rbx
	movq 8(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 16(%r12), %rdi
	callq _putchar
	addq $24, %rbx
	movq 40(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 48(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 56(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 64(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 72(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 80(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 88(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 96(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 104(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 112(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 120(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 128(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 136(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 144(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 152(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 160(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 168(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 176(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 184(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 192(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 200(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 208(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 216(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 224(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 232(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 240(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 248(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 256(%r12), %rdi
	callq _putchar
Lbb291:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb293
	addq $-8, %rbx
	jmp Lbb291
Lbb293:
	movq %rbx, %rax
	addq $32, %rax
	movq 32(%rbx), %rcx
	addq $-1, %rcx
	movq %rcx, 32(%rbx)
	jmp Lbb260
Lbb294:
	addq $1, %rcx
	movq %rcx, (%rax)
	movq %rax, %rbx
	addq $-8, %rbx
	movq -8(%rax), %rcx
	addq $-2, %rcx
	movq %rcx, -8(%rax)
Lbb295:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb315
Lbb296:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb298
	addq $8, %rbx
	jmp Lbb296
Lbb298:
	movq %rbx, %r12
	addq $-16, %rbx
	movq -16(%r12), %rdi
	callq _putchar
	movq -16(%r12), %rdi
	callq _putchar
Lbb299:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb301
	addq $-8, %rbx
	jmp Lbb299
Lbb301:
	movq %rbx, %rax
	addq $8, %rax
	movq %rax, %r12
	addq $8, %r12
	movq 16(%rbx), %rdi
	callq _putchar
	addq $24, %r12
	movq 40(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 48(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 56(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 64(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 72(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 80(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 88(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 96(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 104(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 112(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 120(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 128(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 136(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 144(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 152(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 160(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 168(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 176(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 184(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 192(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 200(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 208(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 216(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 224(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 232(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 240(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 248(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 256(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 264(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb303:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb305
	addq $-8, %rbx
	jmp Lbb303
Lbb305:
	movq %rbx, %r12
	movq %r12, %rax
	addq $8, %rax
	movq %rax, %rbx
	addq $8, %rbx
	movq 16(%r12), %rdi
	callq _putchar
	addq $24, %rbx
	movq 40(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 48(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 56(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 64(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 72(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 80(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 88(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 96(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 104(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 112(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 120(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 128(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 136(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 144(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 152(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 160(%r12), %rdi
	callq _putchar
	addq $104, %rbx
	movq 264(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 272(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 280(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 288(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 296(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 304(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 312(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 320(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 328(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 336(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 344(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 352(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 360(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 368(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 376(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 384(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 392(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 400(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 408(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 416(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 424(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 432(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 440(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 448(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 456(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 464(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 472(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 480(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 488(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 496(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 504(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 512(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 520(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 528(%r12), %rdi
	callq _putchar
Lbb307:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb309
	addq $-8, %rbx
	jmp Lbb307
Lbb309:
	movq %rbx, %r12
	addq $16, %r12
	movq 16(%rbx), %rax
	movq %rax, %rdi
	addq $-1, %rdi
	movq %rdi, 16(%rbx)
	callq _putchar
	addq $24, %r12
	movq 40(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 48(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 56(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 64(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 72(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 80(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 88(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 96(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 104(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 112(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 120(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 128(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 136(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 144(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 152(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 160(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 168(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 176(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 184(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 192(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 200(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 208(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 216(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 224(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 232(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 240(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 248(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 256(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb311:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb313
	addq $-8, %rbx
	jmp Lbb311
Lbb313:
	movq %rbx, %r12
	movq %r12, %rbx
	addq $24, %rbx
	movq 24(%r12), %rax
	addq $-1, %rax
	movq %rax, 24(%r12)
	jmp Lbb295
Lbb315:
	addq $1, %rax
	movq %rax, (%rbx)
Lbb316:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb318
	addq $8, %rbx
	jmp Lbb316
Lbb318:
	movq %rbx, %r12
	addq $-8, %r12
	movq -8(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb320:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb322
	addq $-8, %rbx
	jmp Lbb320
Lbb322:
	movq %rbx, %r12
	movq %r12, %rax
	addq $8, %rax
	movq %rax, %rbx
	addq $8, %rbx
	movq 16(%r12), %rdi
	callq _putchar
	addq $24, %rbx
	movq 40(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 48(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 56(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 64(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 72(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 80(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 88(%r12), %rdi
	callq _putchar
	addq $16, %rbx
	movq 104(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 112(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 120(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 128(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 136(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 144(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 152(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 160(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 168(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 176(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 184(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 192(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 200(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 208(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 216(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 224(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 232(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 240(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 248(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 256(%r12), %rdi
	callq _putchar
	addq $-32, %rbx
	movq 224(%r12), %rdi
	callq _putchar
Lbb324:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb326
	addq $8, %rbx
	jmp Lbb324
Lbb326:
	movq %rbx, %r12
	addq $-16, %r12
	movq -16(%rbx), %rdi
	callq _putchar
	movq -16(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb328:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb330
	addq $-8, %rbx
	jmp Lbb328
Lbb330:
	movq %rbx, %r12
	movq %r12, %rax
	addq $8, %rax
	movq %rax, %rbx
	addq $8, %rbx
	movq 16(%r12), %rdi
	callq _putchar
	addq $24, %rbx
	movq 40(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 48(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 56(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 64(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 72(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 80(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 88(%r12), %rdi
	callq _putchar
	addq $16, %rbx
	movq 104(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 112(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 120(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 128(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 136(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 144(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 152(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 160(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 168(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 176(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 184(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 192(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 200(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 208(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 216(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 224(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 232(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 240(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 248(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 256(%r12), %rdi
	callq _putchar
	addq $8, %rbx
	movq 264(%r12), %rdi
	callq _putchar
Lbb332:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb334
	addq $-8, %rbx
	jmp Lbb332
Lbb334:
	movq %rbx, %rax
	addq $8, %rax
	movq %rax, %r12
	addq $8, %r12
	movq 16(%rbx), %rdi
	callq _putchar
	addq $24, %r12
	movq 40(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 48(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 56(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 64(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 72(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 80(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 88(%rbx), %rdi
	callq _putchar
	addq $16, %r12
	movq 104(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 112(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 120(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 128(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 136(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 144(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 152(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 160(%rbx), %rdi
	callq _putchar
	addq $104, %r12
	movq 264(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 272(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 280(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 288(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 296(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 304(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 312(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 320(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 328(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 336(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 344(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 352(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 360(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 368(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 376(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 384(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 392(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 400(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 408(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 416(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 424(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 432(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 440(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 448(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 456(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 464(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 472(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 480(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 488(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 496(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 504(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 512(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 520(%rbx), %rdi
	callq _putchar
	addq $8, %r12
	movq 528(%rbx), %rdi
	callq _putchar
	movq %r12, %rbx
Lbb336:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb338
	addq $8, %rbx
	jmp Lbb336
Lbb338:
	movq %rbx, %r12
	movq %r12, %rax
	addq $-8, %rax
	movq %rax, %rbx
	addq $-24, %rbx
	movq -32(%r12), %rdi
	callq _putchar
	addq $-16, %rbx
	movq -48(%r12), %rdi
	callq _putchar
	addq $-24, %rbx
	movq -72(%r12), %rdi
	callq _putchar
Lbb340:
	movq (%rbx), %rax
	cmpl $0, %eax
	jz Lbb342
	addq $-8, %rbx
	jmp Lbb340
Lbb342:
	movq 40(%rbx), %rdi
	callq _putchar
	movq 48(%rbx), %rdi
	callq _putchar
	movq 56(%rbx), %rdi
	callq _putchar
	movq 64(%rbx), %rdi
	callq _putchar
	movq 72(%rbx), %rdi
	callq _putchar
	movq 80(%rbx), %rdi
	callq _putchar
	movq 88(%rbx), %rdi
	callq _putchar
	movq 96(%rbx), %rdi
	callq _putchar
	movq 104(%rbx), %rdi
	callq _putchar
	movq 112(%rbx), %rdi
	callq _putchar
	movq 120(%rbx), %rdi
	callq _putchar
	movq 128(%rbx), %rdi
	callq _putchar
	movq 136(%rbx), %rdi
	callq _putchar
	movq 144(%rbx), %rdi
	callq _putchar
	movq 152(%rbx), %rdi
	callq _putchar
	movq 160(%rbx), %rdi
	callq _putchar
	movq 168(%rbx), %rdi
	callq _putchar
	movq 176(%rbx), %rdi
	callq _putchar
	movq 184(%rbx), %rdi
	callq _putchar
	movq 192(%rbx), %rdi
	callq _putchar
	movq 200(%rbx), %rdi
	callq _putchar
	movq 208(%rbx), %rdi
	callq _putchar
	movq 216(%rbx), %rdi
	callq _putchar
	movq 224(%rbx), %rdi
	callq _putchar
	movq 232(%rbx), %rdi
	callq _putchar
	movq 240(%rbx), %rdi
	callq _putchar
	movq 248(%rbx), %rdi
	callq _putchar
	movq 256(%rbx), %rdi
	callq _putchar
	movq 264(%rbx), %rdi
	callq _putchar
	movl $0, %eax
	popq %r12
	popq %rbx
	leave
	ret
/* end function main */

