.text
.balign 16
.globl _blit
_blit:
	endbr64
	subq $16, %rsp
	movl $287454020, 0(%rsp)
	movl $1432778632, 4(%rsp)
	movl $2578103244, 8(%rsp)
	movzbl 10(%rsp), %eax
	movb %al, 11(%rsp)
	movzwl 8(%rsp), %eax
	movw %ax, 9(%rsp)
	movq 0(%rsp), %rax
	movq %rax, 1(%rsp)
	movb $221, 0(%rsp)
	movq 0(%rsp), %rax
	movq 8(%rsp), %rdx
	addq $16, %rsp
	ret
/* end function blit */

