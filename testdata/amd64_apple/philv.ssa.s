.text
.balign 16
.globl _t0
_t0:
	endbr64
	movl $128, %edx
	movl $256, %eax
Lbb2:
	movl %edx, %ecx
	movl %eax, %edx
	shrl $1, %edx
	jz Lbb4
	movl %ecx, %eax
	jmp Lbb2
Lbb4:
	ret
/* end function t0 */

.text
.balign 16
.globl _t1
_t1:
	endbr64
	movl $256, %eax
	movl $128, %edx
Lbb7:
	movl %edx, %ecx
	movl %eax, %edx
	shrl $1, %edx
	jz Lbb9
	movl %ecx, %eax
	jmp Lbb7
Lbb9:
	ret
/* end function t1 */

