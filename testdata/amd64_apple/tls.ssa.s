.section __DATA,__thread_vars,thread_local_variables
_i:
	.quad __tlv_bootstrap
	.quad 0
	.quad _i$tlv$init

.section __DATA,__thread_data,thread_local_regular
.balign 4
_i$tlv$init:
	.int 42
/* end data */

.data
.balign 1
_fmti:
	.ascii "i%d==%d\n"
	.byte 0
/* end data */

.section __DATA,__thread_vars,thread_local_variables
_x:
	.quad __tlv_bootstrap
	.quad 0
	.quad _x$tlv$init

.section __DATA,__thread_data,thread_local_regular
.balign 8
_x$tlv$init:
	.int 1
	.int 2
	.int 3
	.int 4
/* end data */

.data
.balign 1
_fmtx:
	.ascii "*(x+%d)==%d\n"
	.byte 0
/* end data */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movl $0, %ecx
	leaq _thread(%rip), %rdx
	movl $0, %esi
	leaq -16(%rbp), %rdi
	callq _pthread_create
	movq -16(%rbp), %rdi
	leaq -8(%rbp), %rsi
	callq _pthread_join
	movq _i@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	movl (%rax), %edx
	movl $0, %esi
	leaq _fmti(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl -8(%rbp), %edx
	movl $1, %esi
	leaq _fmti(%rip), %rdi
	movl $0, %eax
	callq _printf
	callq _xaddr
	movl (%rax), %edx
	movl $0, %esi
	leaq _fmtx(%rip), %rdi
	movl $0, %eax
	callq _printf
	callq _xaddroff4
	movl (%rax), %edx
	movl $4, %esi
	leaq _fmtx(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl $8, %edi
	callq _xaddroff
	movl (%rax), %edx
	movl $8, %esi
	leaq _fmtx(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl $3, %edi
	callq _xvalcnt
	movq %rax, %rdx
	movl $12, %esi
	leaq _fmtx(%rip), %rdi
	movl $0, %eax
	callq _printf
	movl $0, %eax
	leave
	ret
/* end function main */

.text
.balign 16
_thread:
	endbr64
	movq _i@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	addq $3, %rax
	movb $24, (%rax)
	movq _i@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	movslq (%rax), %rax
	ret
/* end function thread */

.text
.balign 16
_xaddr:
	endbr64
	movq _x@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	ret
/* end function xaddr */

.text
.balign 16
_xaddroff4:
	endbr64
	movq _x@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	addq $4, %rax
	ret
/* end function xaddroff4 */

.text
.balign 16
_xaddroff:
	endbr64
	pushq %rbx
	movq %rdi, %rbx
	movq _x@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	movq %rbx, %rdi
	addq %rdi, %rax
	popq %rbx
	ret
/* end function xaddroff */

.text
.balign 16
_xvalcnt:
	endbr64
	pushq %rbx
	movq %rdi, %rbx
	movq _x@TLVP(%rip), %rdi
	movq (%rdi), %rax
	callq *%rax
	movq %rbx, %rdi
	movl (%rax, %rdi, 4), %eax
	popq %rbx
	ret
/* end function xvalcnt */

