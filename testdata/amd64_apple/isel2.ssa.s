.text
.balign 16
.globl _lt
_lt:
	endbr64
	ucomisd %xmm0, %xmm1
	seta %al
	movzbl %al, %eax
	ret
/* end function lt */

.text
.balign 16
.globl _le
_le:
	endbr64
	ucomisd %xmm0, %xmm1
	setae %al
	movzbl %al, %eax
	ret
/* end function le */

.text
.balign 16
.globl _gt
_gt:
	endbr64
	ucomisd %xmm1, %xmm0
	seta %al
	movzbl %al, %eax
	ret
/* end function gt */

.text
.balign 16
.globl _ge
_ge:
	endbr64
	ucomisd %xmm1, %xmm0
	setae %al
	movzbl %al, %eax
	ret
/* end function ge */

.text
.balign 16
.globl _eq1
_eq1:
	endbr64
	ucomisd %xmm1, %xmm0
	setz %al
	movzbl %al, %eax
	setnp %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	ret
/* end function eq1 */

.text
.balign 16
.globl _eq2
_eq2:
	endbr64
	ucomisd %xmm1, %xmm0
	setz %al
	movzbl %al, %eax
	setnp %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	jnz Lbb12
	movl $0, %eax
	jmp Lbb13
Lbb12:
	movl $1, %eax
Lbb13:
	ret
/* end function eq2 */

.text
.balign 16
.globl _eq3
_eq3:
	endbr64
	ucomisd %xmm1, %xmm0
	setz %al
	movzbl %al, %eax
	setnp %cl
	movzbl %cl, %ecx
	andl %ecx, %eax
	jnz Lbb16
	movl $0, %eax
Lbb16:
	ret
/* end function eq3 */

.text
.balign 16
.globl _ne1
_ne1:
	endbr64
	ucomisd %xmm1, %xmm0
	setnz %al
	movzbl %al, %eax
	setp %cl
	movzbl %cl, %ecx
	orl %ecx, %eax
	ret
/* end function ne1 */

.text
.balign 16
.globl _ne2
_ne2:
	endbr64
	ucomisd %xmm1, %xmm0
	setnz %al
	movzbl %al, %eax
	setp %cl
	movzbl %cl, %ecx
	orl %ecx, %eax
	jnz Lbb21
	movl $0, %eax
	jmp Lbb22
Lbb21:
	movl $1, %eax
Lbb22:
	ret
/* end function ne2 */

.text
.balign 16
.globl _ne3
_ne3:
	endbr64
	ucomisd %xmm1, %xmm0
	setnz %al
	movzbl %al, %eax
	setp %cl
	movzbl %cl, %ecx
	orl %ecx, %eax
	jnz Lbb25
	movl $0, %eax
Lbb25:
	ret
/* end function ne3 */

.text
.balign 16
.globl _o
_o:
	endbr64
	ucomisd %xmm1, %xmm0
	setnp %al
	movzbl %al, %eax
	ret
/* end function o */

.text
.balign 16
.globl _uo
_uo:
	endbr64
	ucomisd %xmm1, %xmm0
	setp %al
	movzbl %al, %eax
	ret
/* end function uo */

