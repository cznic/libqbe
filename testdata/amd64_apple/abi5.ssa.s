.data
.balign 8
_fmt1:
	.ascii "t1: %s\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt2:
	.ascii "t2: %d\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt3:
	.ascii "t3: %f %d\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt4:
	.ascii "t4: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt5:
	.ascii "t5: %f %lld\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt6:
	.ascii "t6: %s\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt7:
	.ascii "t7: %f %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt8:
	.ascii "t8: %d %d %d %d\n"
	.byte 0
/* end data */

.data
.balign 8
_fmt9:
	.ascii "t9: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmta:
	.ascii "ta: %d %f\n"
	.byte 0
/* end data */

.data
.balign 8
_fmtb:
	.ascii "tb: %d %d %f\n"
	.byte 0
/* end data */

.text
.balign 16
.globl _test
_test:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $144, %rsp
	leaq -72(%rbp), %rdi
	callq _t1
	movq %rax, %rsi
	leaq _fmt1(%rip), %rdi
	movl $0, %eax
	callq _printf
	callq _t2
	movq %rax, -80(%rbp)
	movl -80(%rbp), %esi
	leaq _fmt2(%rip), %rdi
	movl $0, %eax
	callq _printf
	callq _t3
	movq %rax, -88(%rbp)
	movss -88(%rbp), %xmm0
	movl -84(%rbp), %esi
	cvtss2sd %xmm0, %xmm0
	leaq _fmt3(%rip), %rdi
	movl $1, %eax
	callq _printf
	callq _t4
	movq %rax, -16(%rbp)
	movq %xmm0, -8(%rbp)
	movl -16(%rbp), %esi
	movsd -8(%rbp), %xmm0
	leaq _fmt4(%rip), %rdi
	movl $1, %eax
	callq _printf
	callq _t5
	movq %xmm0, -32(%rbp)
	movq %rax, -24(%rbp)
	movss -32(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	movq -24(%rbp), %rsi
	leaq _fmt5(%rip), %rdi
	movl $1, %eax
	callq _printf
	callq _t6
	movq %rax, %rcx
	movq %rdx, %rax
	movq %rcx, -104(%rbp)
	movq %rax, -96(%rbp)
	leaq -104(%rbp), %rsi
	leaq _fmt6(%rip), %rdi
	movl $0, %eax
	callq _printf
	callq _t7
	movsd %xmm0, %xmm15
	movsd %xmm1, %xmm0
	movsd %xmm15, %xmm1
	movq %xmm1, -48(%rbp)
	movq %xmm0, -40(%rbp)
	movss -48(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	movsd -40(%rbp), %xmm1
	leaq _fmt7(%rip), %rdi
	movl $2, %eax
	callq _printf
	callq _t8
	movq %rax, %rcx
	movq %rdx, %rax
	movq %rcx, -120(%rbp)
	movq %rax, -112(%rbp)
	movl -120(%rbp), %esi
	movl -116(%rbp), %edx
	movl -112(%rbp), %ecx
	movl -108(%rbp), %r8d
	leaq _fmt8(%rip), %rdi
	movl $0, %eax
	callq _printf
	callq _t9
	movq %rax, -128(%rbp)
	movl -128(%rbp), %esi
	movss -124(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	leaq _fmt9(%rip), %rdi
	movl $1, %eax
	callq _printf
	callq _ta
	movq %rax, -136(%rbp)
	movsbl -136(%rbp), %esi
	movss -132(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	leaq _fmta(%rip), %rdi
	movl $1, %eax
	callq _printf
	callq _tb
	movq %rax, -144(%rbp)
	movsbl -144(%rbp), %esi
	movsbl -143(%rbp), %edx
	movss -140(%rbp), %xmm0
	cvtss2sd %xmm0, %xmm0
	leaq _fmtb(%rip), %rdi
	movl $1, %eax
	callq _printf
	leave
	ret
/* end function test */

