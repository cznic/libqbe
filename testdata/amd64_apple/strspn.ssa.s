.text
.balign 16
.globl _strspn_
_strspn_:
	endbr64
	movq %rsi, %rax
	movl $0, %edx
Lbb2:
	movq %rdi, %rcx
	addq $1, %rdi
	movsbl (%rcx), %ecx
	cmpl $0, %ecx
	jz Lbb10
	movq %rax, %rsi
Lbb4:
	movsbl (%rsi), %r8d
	cmpl $0, %r8d
	jz Lbb7
	cmpl %ecx, %r8d
	jz Lbb7
	addq $1, %rsi
	jmp Lbb4
Lbb7:
	cmpl $0, %r8d
	jz Lbb9
	addl $1, %edx
	jmp Lbb2
Lbb9:
	movl %edx, %eax
	jmp Lbb11
Lbb10:
	movl %edx, %eax
Lbb11:
	ret
/* end function strspn_ */

