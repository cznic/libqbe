.text
.balign 16
_func:
	endbr64
	subq $16, %rsp
	movl $1, 0(%rsp)
	movl 0(%rsp), %eax
	movl %eax, 4(%rsp)
	movl $2, 0(%rsp)
	movq 0(%rsp), %rax
	addq $16, %rsp
	ret
/* end function func */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	callq _func
	movq %rax, -8(%rbp)
	movl -4(%rbp), %eax
	cmpl $1, %eax
	jz Lbb4
	callq _abort
Lbb4:
	movl $0, %eax
	leave
	ret
/* end function main */

