.text
.balign 16
.globl _test
_test:
	endbr64
	movl $747, %esi
	movl $380, %ecx
Lbb2:
	movl %esi, %eax
	cltd
	idivl %ecx
	movl %edx, %esi
	cmpl $0, %esi
	jz Lbb4
	xchgl %ecx, %esi
	jmp Lbb2
Lbb4:
	movl %ecx, _a(%rip)
	ret
/* end function test */

