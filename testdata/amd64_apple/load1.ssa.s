.text
.balign 16
.globl _f
_f:
	endbr64
	subq $16, %rsp
	movl $0, 0(%rsp)
	cmpl $0, %edi
	jnz Lbb2
	leaq 4(%rsp), %rax
	jmp Lbb3
Lbb2:
	leaq 0(%rsp), %rax
Lbb3:
	movl $1, (%rax)
	movl 0(%rsp), %eax
	addq $16, %rsp
	ret
/* end function f */

