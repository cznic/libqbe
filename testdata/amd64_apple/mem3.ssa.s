.text
.balign 16
_func:
	endbr64
	subq $32, %rsp
	movq %rdi, %rax
	movq $1, 0(%rsp)
	movq $2, 8(%rsp)
	movq 8(%rsp), %rcx
	movq %rcx, 16(%rsp)
	movq 0(%rsp), %rcx
	movq %rcx, 8(%rsp)
	movq 0(%rsp), %rcx
	movq %rcx, 0(%rax)
	movq 8(%rsp), %rcx
	movq %rcx, 8(%rax)
	movq 16(%rsp), %rcx
	movq %rcx, 16(%rax)
	addq $32, %rsp
	ret
/* end function func */

.text
.balign 16
.globl _main
_main:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	subq $32, %rsp
	leaq -24(%rbp), %rdi
	callq _func
	movq 16(%rax), %rax
	cmpq $2, %rax
	jz Lbb6
	callq _abort
Lbb6:
	movl $0, %eax
	leave
	ret
/* end function main */

