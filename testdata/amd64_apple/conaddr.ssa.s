.text
.balign 16
.globl _f0
_f0:
	endbr64
	leaq _a(%rip), %rax
	movzbl (%rax, %rdi, 1), %eax
	ret
/* end function f0 */

.text
.balign 16
.globl _f1
_f1:
	endbr64
	movzbl 10(%rdi), %eax
	ret
/* end function f1 */

.text
.balign 16
.globl _f2
_f2:
	endbr64
	imulq $2, %rsi, %rax
	movq %rdi, %rcx
	addq %rax, %rcx
	leaq _a(%rip), %rax
	addq %rcx, %rax
	movzbl (%rax), %eax
	ret
/* end function f2 */

.text
.balign 16
.globl _f3
_f3:
	endbr64
	leaq _a(%rip), %rax
	addq %rdi, %rax
	ret
/* end function f3 */

.text
.balign 16
.globl _f4
_f4:
	endbr64
	leaq _p(%rip), %rax
	movq %rax, _p(%rip)
	ret
/* end function f4 */

.text
.balign 16
.globl _writeto0
_writeto0:
	endbr64
	movq $0, 0
	ret
/* end function writeto0 */

