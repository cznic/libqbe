// Copyright 2023 The libpqbe-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"slices"
	//"strconv"
	"strings"

	"modernc.org/cc/v4"
	util "modernc.org/ccgo/v3/lib"
	ccgo "modernc.org/ccgo/v4/lib"
	"modernc.org/gc/v3"
)

const (
	archivePath = "c9x.me.tar.gz"
)

var (
	goos    = runtime.GOOS
	goarch  = runtime.GOARCH
	j       = "1" // strconv.Itoa(runtime.GOMAXPROCS(-1))
	targets = []string{
		"amd64_apple",
		"amd64_sysv",
		"arm64",
		"arm64_apple",
		"rv64",
	}
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	f.Close()

	extractedArchivePath := "c9x.me/qbe"
	tempDir := os.Getenv("GO_GENERATE_DIR")
	dev := os.Getenv("GO_GENERATE_DEV") != ""
	switch {
	case tempDir != "":
		util.MustShell(true, "sh", "-c", fmt.Sprintf("rm -rf %s", filepath.Join(tempDir, extractedArchivePath)))
	default:
		var err error
		if tempDir, err = os.MkdirTemp("", "libqbe-generate"); err != nil {
			fail(1, "creating temp dir: %v\n", err)
		}

		defer func() {
			switch os.Getenv("GO_GENERATE_KEEP") {
			case "":
				os.RemoveAll(tempDir)
			default:
				fmt.Printf("%s: temporary directory kept\n", tempDir)
			}
		}()
	}
	libRoot := filepath.Join(tempDir, extractedArchivePath)
	makeRoot := libRoot
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "extractedArchivePath %s\n", extractedArchivePath)
	fmt.Fprintf(os.Stderr, "tempDir %s\n", tempDir)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)

	tests := filepath.Join("internal", "test")
	os.RemoveAll(tests)
	os.MkdirAll(tests, 0770)
	if tests, err = filepath.Abs(tests); err != nil {
		fail(1, "%s\n", err)
	}

	util.MustShell(true, "tar", "xzf", archivePath, "-C", tempDir)
	util.MustShell(true, "find", tempDir, "-name", "*.o", "-delete")
	util.MustShell(true, "find", tempDir, "-name", "*.go", "-delete")
	util.MustShell(true, "find", tempDir, "-name", "*.ago", "-delete")
	util.MustShell(true, "sh", "-c", fmt.Sprintf("cp %s* LICENSE-QBE", filepath.Join(makeRoot, "LICENSE")))
	mustCopyDir(libRoot, filepath.Join("internal", "overlay", extractedArchivePath, "all"), nil, true)
	mustCopyDir(libRoot, filepath.Join("internal", "overlay", extractedArchivePath, goarch), nil, true)
	result := filepath.Join("qbe.go")
	util.MustInDir(true, makeRoot, func() (err error) {
		util.MustShell(true, "sh", "-c", "go mod init example.com/libqbe; go get modernc.org/libc@latest")
		if dev {
			util.MustShell(true, "sh", "-c", "go work init ; go work use $GOPATH/src/modernc.org/libc")
		}
		args := []string{os.Args[0]}
		if dev {
			args = append(
				args,
				"-absolute-paths",
				"-keep-object-files",
				"-positions",
			)
		}
		args = append(args,
			"--prefix-enumerator=_",
			"--prefix-external=x_",
			"--prefix-macro=m_",
			"--prefix-static-internal=s_",
			"--prefix-static-none=s_",
			"--prefix-tagged-enum=_",
			"--prefix-tagged-struct=_",
			"--prefix-tagged-union=_",
			"--prefix-typename=_",
			"--prefix-undefined=_",
			"-import", "io,runtime",
			"-D__CCGO__",
			"-extended-errors",
		)
		switch fmt.Sprintf("%s/%s", goos, goarch) {
		case
			"freebsd/amd64",
			"freebsd/arm64":

			args = append(args, "-D_XLOCALE_INLINE=extern __inline")
		}
		switch {
		case dev:
			args = append(args, "-UNDEBUG")
		default:
			args = append(args, "-DNDEBUG")
		}
		if s := cc.LongDouble64Flag(goos, goarch); s != "" {
			args = append(args, s)
		}

		switch fmt.Sprintf("%s/%s", goos, goarch) {
		case
			"darwin/amd64",
			"darwin/arm64",
			"freebsd/amd64",
			"freebsd/arm64",
			"linux/amd64",
			"linux/arm64",
			"linux/riscv64":

			os.Remove("config.h")
			if err := ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "clean", "check", "-j", j), os.Stdout, os.Stderr, nil).Exec(); err != nil {
				return err
			}
		default:
			if err := ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "qbe", "-j", j), os.Stdout, os.Stderr, nil).Exec(); err != nil {
				return err
			}
		}

		filepath.Walk("test", func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				fail(1, "%s\n", err)
			}

			if info.IsDir() || !strings.HasSuffix(path, ".ssa") {
				return nil
			}

			dir, nm := filepath.Split(path)
			dir = tests
			if err := os.MkdirAll(dir, 0770); err != nil {
				fail(1, "%s\n", err)
			}

			util.MustCopyFile(false, filepath.Join(dir, nm), path, nil)
			return nil
		})

		util.MustShell(true, "gofmt", "-l", "-s", "-w", ".")
		// sed meta `$+.*\^`
		util.MustShell(
			true, "sed", "-i.bak",
			"-e", `s/\<T__\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/t__\1/g`,
			"-e", `s/\<x___\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/X__\1/g`,
			"-e", `s/package main/package libqbe/`,
			"-e", `s/libc\.Start/\/\/ libc.Start/`,
			"-e", `s/(\*(\*func(\*libc\.TLS/(*(*func(*libc.TLS, *_QBE/g`,
			"-e", `s/libc\.Xqsort(tls/qsort(tls, qbe/g`,
			"-e", `s/libc\.Xcalloc(tls/calloc(tls, qbe/g`,
			"-e", `s/libc\.Xfree(tls/free(tls, qbe/g`,
			"-e", `s/libc\.Xfgetc(tls/fgetc(tls, qbe/g`,
			"-e", `s/libc\.Xfprintf(tls/fprintf(tls, qbe/g`,
			"-e", `s/libc\.Xfputc(tls/fputc(tls, qbe/g`,
			"-e", `s/libc\.Xfputs(tls/fputs(tls, qbe/g`,
			"-e", `s/libc\.Xfscanf(tls/fscanf(tls, qbe/g`,
			"-e", `s/libc\.Xungetc(tls/ungetc(tls, qbe/g`,
			"-e", `s/iqlibc.qbe.x___builtin_vsnprintf(/libc.Xvsnprintf(/g`,
			"-e", `s/_fscanf(tls/fscanf(tls, qbe/g`,

			result,
		)
		return nil
	})

	fn := fmt.Sprintf("ccgo_%s_%s.go", goos, goarch)
	util.MustCopyFile(false, fn, filepath.Join(makeRoot, result), nil)
	util.MustShell(true, "gofmt", "-l", "-s", "-w", ".")
	if err := main2lib(fn); err != nil {
		fail(1, "%s\n", err)
	}

	util.Shell("sh", "-c", "./unconvert.sh")
	genTestData(makeRoot)
	util.Shell("sh", "-c", "rm *.bak")
	util.MustShell(true, "gofmt", "-l", "-s", "-w", ".")
	util.MustShell(true, "go", "test", "-run", "@")
	util.Shell("git", "status")
}

func mustCopyDir(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool, srcNotExistsOk bool) (files int, bytes int64) {
	file, bytes, err := copyDir(dst, src, canOverwrite, srcNotExistsOk)
	if err != nil {
		fail(1, "%s\n", err)
	}

	return file, bytes
}

func copyDir(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool, srcNotExistsOk bool) (files int, bytes int64, rerr error) {
	dst = filepath.FromSlash(dst)
	src = filepath.FromSlash(src)
	si, err := os.Stat(src)
	if err != nil {
		if os.IsNotExist(err) && srcNotExistsOk {
			err = nil
		}
		return 0, 0, err
	}

	if !si.IsDir() {
		return 0, 0, fmt.Errorf("cannot copy a file: %s", src)
	}

	return files, bytes, filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.Mode()&os.ModeSymlink != 0 {
			target, err := filepath.EvalSymlinks(path)
			if err != nil {
				return fmt.Errorf("cannot evaluate symlink %s: %v", path, err)
			}

			if info, err = os.Stat(target); err != nil {
				return fmt.Errorf("cannot stat %s: %v", target, err)
			}

			if info.IsDir() {
				rel, err := filepath.Rel(src, path)
				if err != nil {
					return err
				}

				dst2 := filepath.Join(dst, rel)
				if err := os.MkdirAll(dst2, 0770); err != nil {
					return err
				}

				f, b, err := copyDir(dst2, target, canOverwrite, srcNotExistsOk)
				files += f
				bytes += b
				return err
			}

			path = target
		}

		rel, err := filepath.Rel(src, path)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return os.MkdirAll(filepath.Join(dst, rel), 0770)
		}

		n, err := copyFile(filepath.Join(dst, rel), path, canOverwrite)
		if err != nil {
			return err
		}

		files++
		bytes += n
		return nil
	})
}

func mustCopyFile(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool) int64 {
	n, err := copyFile(dst, src, canOverwrite)
	if err != nil {
		fail(1, "%s\n", err)
	}

	return n
}

func copyFile(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool) (n int64, rerr error) {
	src = filepath.FromSlash(src)
	si, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if si.IsDir() {
		return 0, fmt.Errorf("cannot copy a directory: %s", src)
	}

	dst = filepath.FromSlash(dst)
	if si.Size() == 0 {
		return 0, os.Remove(dst)
	}

	dstDir := filepath.Dir(dst)
	di, err := os.Stat(dstDir)
	switch {
	case err != nil:
		if !os.IsNotExist(err) {
			return 0, err
		}

		if err := os.MkdirAll(dstDir, 0770); err != nil {
			return 0, err
		}
	case err == nil:
		if !di.IsDir() {
			return 0, fmt.Errorf("cannot create directory, file exists: %s", dst)
		}
	}

	di, err = os.Stat(dst)
	switch {
	case err != nil && !os.IsNotExist(err):
		return 0, err
	case err == nil:
		if di.IsDir() {
			return 0, fmt.Errorf("cannot overwite a directory: %s", dst)
		}

		if canOverwrite != nil && !canOverwrite(dst, di) {
			return 0, fmt.Errorf("cannot overwite: %s", dst)
		}
	}

	s, err := os.Open(src)
	if err != nil {
		return 0, err
	}

	defer s.Close()
	r := bufio.NewReader(s)

	d, err := os.Create(dst)

	defer func() {
		if err := d.Close(); err != nil && rerr == nil {
			rerr = err
			return
		}

		if err := os.Chmod(dst, si.Mode()); err != nil && rerr == nil {
			rerr = err
			return
		}

		if err := os.Chtimes(dst, si.ModTime(), si.ModTime()); err != nil && rerr == nil {
			rerr = err
			return
		}
	}()

	w := bufio.NewWriter(d)

	defer func() {
		if err := w.Flush(); err != nil && rerr == nil {
			rerr = err
		}
	}()

	return io.Copy(w, r)
}

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
		if strings.HasPrefix(fns, "func") {
			num := true
			for _, c := range fns[len("func"):] {
				if c < '0' || c > '9' {
					num = false
					break
				}
			}
			if num {
				return origin(skip + 2)
			}
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s\n\tTODO %s", origin(2), s)
	// fmt.Fprintf(os.Stderr, "%s\n", r)
	// os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", r)
	os.Stderr.Sync()
	return r
}

func main2lib(fn string) (err error) {
	b, err := os.ReadFile(fn)
	if err != nil {
		return err
	}

	ast, err := gc.ParseFile(fn, b)
	if err != nil {
		return err
	}

	buf := bytes.NewBuffer(nil)
	w := func(s string, args ...interface{}) {
		fmt.Fprintf(buf, s, args...)
	}

	src := ast.SourceFile
	w("%s\n", src.PackageClause.Source(true))
	if src.ImportDeclList != nil {
		w("%s\n", src.ImportDeclList.Source(true))
	}

	vars := map[*gc.VarDeclNode]string{}
	xvars := map[string]*gc.VarDeclNode{}
	funcs := map[string]struct{}{}
	initFuncs := map[string]*gc.FunctionDeclNode{}
	initNum := 0
	for l := src.TopLevelDeclList; l != nil; l = l.List {
		switch x := l.TopLevelDecl.(type) {
		case *gc.FunctionDeclNode:
			switch nm := x.FunctionName.IDENT.Src(); {
			case nm == "__ccgo_fp":
				// nop
			case nm == "main":
				// nop
			case nm == "init":
				// nop
			default:
				funcs[nm] = struct{}{}
			}
		}
	}
	var a []string
	// for k := range funcs {
	// 	a = append(a, k)
	// }
	// slices.Sort(a)
	// for _, k := range a {
	// 	trc("FUNC %s", k)
	// }
	for l := src.TopLevelDeclList; l != nil; l = l.List {
		switch x := l.TopLevelDecl.(type) {
		case *gc.VarDeclNode:
			if x.LPAREN.IsValid() {
				w("%s\n\n", x.Source(true))
				break
			}

			switch y := x.VarSpec.(type) {
			case *gc.VarSpecNode:
				nm := y.IDENT.Src()
				if !strings.HasPrefix(nm, "x_") && !strings.HasPrefix(nm, "s_") {
					w("%s\n\n", x.Source(true))
					break
				}

				vars[x] = nm
				xvars[nm] = x
			default:
				w("%s\n\n", x.Source(true))
			}
		case *gc.ConstDeclNode:
			w("%s\n\n", x.Source(true))
		case *gc.TypeDeclNode:
			w("%s\n\n", x.Source(true))
		case *gc.FunctionDeclNode:
			switch nm := x.FunctionName.IDENT.Src(); {
			case nm == "__ccgo_fp":
				w("%s\n\n", x.Source(true))
				continue
			case nm == "main":
				// nop
			case nm == "init":
				s := x.Source(true)
				initNum++
				nm := fmt.Sprintf("%s%v", nm, initNum)
				initFuncs[nm] = x
				ix := strings.Index(s, "func init")
				// func init
				head := s[:ix]
				tail := s[ix+9:]
				tail = rename(tail, funcs)
				w("%sfunc (qbe *_QBE) init%v%s\n\n", head, initNum, tail)
			default:
				s := x.Source(true)
				ix := strings.Index(s, "{")
				// func foo(tls *libc.TLS... {
				head := s[:ix]
				head = strings.Replace(head, "(tls *libc.TLS", "(tls *libc.TLS, qbe *_QBE", 1)
				tail := s[ix:]
				tail = rename(tail, funcs)
				w("%s%s\n\n", head, tail)

			}
		default:
			panic(todo("%T", x))
		}
	}

	w("\n\ntype _QBE struct{\n")
	w("mallocs map[uintptr]struct{}\n")
	w("stderr io.Writer\n")
	w("r io.ByteScanner\n")
	w("w io.Writer\n")
	a = a[:0]
	for k := range xvars {
		a = append(a, k)
	}
	slices.Sort(a)
	initializers := map[string]gc.Node{}
	for _, nm := range a {
		vd := xvars[nm]
		vs := vd.VarSpec.(*gc.VarSpecNode)
		switch {
		case vs.TypeNode != nil:
			w("%s %s\n", nm, vs.TypeNode.Source(true))
			if vs.ExpressionList != nil {
				panic(todo(""))
			}
		default:
			switch x := vs.ExpressionList.Expression.(type) {
			case *gc.CompositeLitNode:
				initializers[nm] = x
				w("%s %s\n", nm, x.LiteralType.Source(true))
			case *gc.PrimaryExprNode:
				initializers[nm] = x
				w("%s %s\n", nm, primaryExprType(x))
			case *gc.OperandNode:
				if x.OperandName != nil {
					initializers[nm] = x
					w("%s %s\n", nm, x.OperandName.Source(true))
					break
				}

				if x.TypeArgs != nil {
					panic(todo("%T\n%s", x, x.Source(true)))
				}

				if x.LiteralValue != nil {
					panic(todo("%T\n%s", x, x.Source(true)))
				}

				panic(todo(""))
			default:
				panic(todo("%T\n%s", x, x.Source(true)))
			}

		}
	}
	w("}\n\n")
	w("func newQBE(pinner *runtime.Pinner) (qbe *_QBE) {\n")
	w("qbe = &_QBE{mallocs: map[uintptr]struct{}{}}\n")
	w("pinner.Pin(qbe)\n")
	a = a[:0]
	for k := range initializers {
		a = append(a, k)
	}
	slices.Sort(a)
	for _, nm := range a {
		w("qbe.%s = %s\n", nm, rename(initializers[nm].Source(true), funcs))
	}
	a = a[:0]
	for k := range initFuncs {
		a = append(a, k)
	}
	slices.Sort(a)
	for _, nm := range a {
		w("qbe.%s()\n", nm)
	}
	w("return qbe")
	w("}\n")

	if err := os.WriteFile(fn, buf.Bytes(), 0660); err != nil {
		return err
	}

	if out, err := util.Shell(
		"sed", "-i.bak",
		"-e", `s/x_err(tls/_err(tls/g`,
		"-e", `s/func _err(/func x_err(/g`,
		"-e", `s/x_die_(tls/_die_(tls/g`,
		"-e", `s/func _die_(/func x_die_(/g`,

		fn,
	); err != nil {
		return fmt.Errorf("out=%s FAIL err=%v", out, err)
	}

	return nil
}

func primaryExprType(n *gc.PrimaryExprNode) string {
	if n.Postfix != nil {
		return n.PrimaryExpr.Source(true)
	}

	switch x := n.PrimaryExpr.(type) {
	default:
		panic(todo("%T", x))
	}
}

var (
	re  = regexp.MustCompile(`\b[sx]_[a-zA-Z0-9_]+\b`)
	re2 = regexp.MustCompile(`\b[sx]_[a-zA-Z0-9_]+\b\(tls`)
	re3 = regexp.MustCompile(`}\)\)\)\(tls\b`)
)

func rename(s string, funcs map[string]struct{}) string {
	s = re.ReplaceAllStringFunc(s, func(s string) string {
		if _, ok := funcs[s]; !ok {
			return "qbe." + s
		}

		return s
	})
	s = re2.ReplaceAllStringFunc(s, func(s string) string {
		return s + ", qbe"
	})
	return re3.ReplaceAllStringFunc(s, func(s string) string {
		return s + ", qbe"
	})
}

func genTestData(makeRoot string) {
	qbe := filepath.Join(makeRoot, "qbe")
	for _, target := range targets {
		oDir := filepath.Join("testdata", target)
		os.RemoveAll(oDir)
		os.MkdirAll(oDir, 0770)
		m, err := filepath.Glob(filepath.Join("internal", "test", "*.ssa"))
		if err != nil {
			fail(1, "glob *.ssa: err=%v", err)
		}

		for _, in := range m {
			out := filepath.Join(oDir, filepath.Base(in)+".s")
			util.MustShell(true, qbe, "-t", target, "-o", out, in)
		}
	}
}
