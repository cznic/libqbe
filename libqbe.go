// Copyright 2023 The libqbe-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libqbe is a ccgo/v4 version of the [QBE command] in a library form.
//
// QBE compiles SSA source code to assembler source code. See the [QBE
// Intermediate Language] manual for the full specification.
//
// # QBE vs LLVM
//
// (Copied verbatim from [QBE vs LLVM].)
//
// Both QBE and LLVM are compiler backends using an SSA representation. This
// document will explain why LLVM does not make QBE a redundant project.
// Obviously, everything following is biased, because written by me.
//
// # Scope
//
// QBE is a much smaller scale project with different goals than LLVM.
//
//   - QBE is for amateur language designers.
//
// It does not address all the problems faced when conceiving an industry-grade
// language. If you are toying with some language ideas, using LLVM will be
// like hauling your backpack with a truck, but using QBE will feel more like
// riding a bicycle.
//
//   - QBE is about the first 70%, not the last 30%.
//
// It attempts to pinpoint, in the extremely vast compilation literature, the
// optimizations that get you 70% of the performance in 10% of the code of full
// blown compilers.
//
// For example, copy propagation on SSA form is implemented in 160 lines of
// code in QBE!
//
//   - QBE is extremely hackable.
//
// First, it is, and will remain, a small project (less than 8 kloc). Second,
// it is programmed in non-fancy C99 without any dependencies. Third, it is
// able to dump the IL and debug information in a uniform format after each
// pass.
//
// On my Core 2 Duo machine, QBE compiles in half a second (without optimizations).
//
// # Features
//
// LLVM is definitely more packed with features, but there are a few things
// provided in QBE to consider.
//
//   - LLVM does NOT provide full C compatibility for you.
//
// In more technical terms, any language that provides good C compatibility and
// uses LLVM as a backend needs to reimplement large chunks of the ABI in its
// frontend! This well known issue in the LLVM community causes a great deal of
// duplication and bugs.
//
// Implementing a complete C ABI (with struct arguments and returns) is
// incredibly tricky, and not really a lot of fun. QBE provides you with IL
// operations to call in (and be called by) C with no pain. Moreover the ABI
// implementation in QBE has been thoroughly tested by fuzzing and manual
// tests.
//
//   - LLVM IL is more cluttered with memory operations.
//
// Implementing SSA construction is hard. To save its users from having to
// implement it, LLVM provides stack slots. This means that one increment of a
// variable v will be composed of three LLVM instructions: one load, one add,
// and one store.
//
// QBE provides simple non-SSA temporaries, so incrementing v is simply done
// with one instruction %v =w add %v, 1.
//
// This could seem cosmetic, but dividing the size of the IL by three makes it
// easier for the frontend writers to spot bugs in the generated code.
//
//   - LLVM IL is more cluttered with type annotations and casts.
//
// For the sake of advanced optimizations and correctness, LLVM has complex IL
// types. However, only a few types are really first class and many operations
// of source languages require casts to be compiled.
//
// Because QBE makes a much lighter use of types, the IL is more readable and
// shorter. It can of course be argued back that the correctness of QBE is
// jeopardized, but remember that, in practice, the large amount of casts
// necessary in LLVM IL is undermining the overall effectiveness of the type
// system.
//
// (End of the verbatim copy from [QBE vs LLVM].)
//
// # Phi
//
// (Copied verbatim from [Phi].)
//
//	PHI := %IDENT '=' BASETY 'phi' ( @IDENT VAL ),
//
// First and foremost, phi instructions are NOT necessary when writing a
// frontend to QBE. One solution to avoid having to deal with SSA form is to
// use stack allocated variables for all source program variables and perform
// assignments and lookups using Memory operations. This is what LLVM users
// typically do.
//
// Another solution is to simply emit code that is not in SSA form! Contrary to
// LLVM, QBE is able to fixup programs not in SSA form without requiring the
// boilerplate of loading and storing in memory. For example, the following
// program will be correctly compiled by QBE.
//
//	@start
//	        %x =w copy 100
//	        %s =w copy 0
//	@loop
//	        %s =w add %s, %x
//	        %x =w sub %x, 1
//	        jnz %x, @loop, @end
//	@end
//	        ret %s
//
// Now, if you want to know what phi instructions are and how to use them in
// QBE, you can read the following.
//
// Phi instructions are specific to SSA form. In SSA form values can only be
// assigned once, without phi instructions, this requirement is too strong to
// represent many programs. For example consider the following C program.
//
//	int f(int x) {
//	        int y;
//	        if (x)
//	                y = 1;
//	        else
//	                y = 2;
//	        return y;
//	}
//
// The variable y is assigned twice, the solution to translate it in SSA form
// is to insert a phi instruction.
//
//	@ifstmt
//	        jnz %x, @ift, @iff
//	@ift
//	        jmp @retstmt
//	@iff
//	        jmp @retstmt
//	@retstmt
//	        %y =w phi @ift 1, @iff 2
//	        ret %y
//
// Phi instructions return one of their arguments depending on where the
// control came from. In the example, %y is set to 1 if the @ift branch is
// taken, or it is set to 2 otherwise.
//
// An important remark about phi instructions is that QBE assumes that if a
// variable is defined by a phi it respects all the SSA invariants. So it is
// critical to not use phi instructions unless you know exactly what you are
// doing.
//
// (End of the verbatim copy from [Phi].)
//
// # Supported hosts
//
//	GOOS/GOARCH
//	-----------
//	darwin/amd64
//	darwin/arm64
//	freebsd/amd64
//	freebsd/arm64
//	linux/386
//	linux/amd64
//	linux/arm
//	linux/arm64
//	linux/loong64
//	linux/ppc64le
//	linux/riscv64
//
// The linux/s390x host seems to be currently (upstream@327736b3) broken by
// unsupported endianess.
//
// # Supported targets
//
//	GOOS/GOARCH     QBE 'target' argument
//	-----------     ---------------------
//	darwin/amd64    amd64_apple
//	darwin/arm64    arm64_apple
//	freebsd/amd64   amd64_sysv
//	freebsd/arm64   arm64
//	linux/amd64     amd64_sysv
//	linux/arm64     arm64
//	linux/riscv64	rv64
//
// All supported hosts can cross compile for any of the supported targets.
//
// # Example
//
//	~/src/modernc.org/libqbe$ go test -run TestExec0 -v
//	=== RUN   TestExec0
//	    exec_test.go:51: using C compiler at /usr/bin/cc
//	    exec_test.go:73:
//	        ---- qbe
//	        function w $add(w %a, w %b) {              # Define a function add
//	        @start
//	        	%c =w add %a, %b                   # Adds the 2 arguments
//	        	ret %c                             # Return the result
//	        }
//
//	        export function w $main() {                # Main function
//	        @start
//	        	%r =w call $add(w 1, w 1)          # Call add(1, 1)
//	        	call $printf(l $fmt, ..., w %r)    # Show the result
//	        	ret 0
//	        }
//	        data $fmt = { b "One and one make %d!\n", b 0 }
//
//	        ---- asm
//	        .text
//	        add:
//	        	pushq %rbp
//	        	movq %rsp, %rbp
//	        	movl %edi, %eax
//	        	addl %esi, %eax
//	        	leave
//	        	ret
//	        .type add, @function
//	        .size add, .-add
//	        /* end function add */
//
//	        .text
//	        .globl main
//	        main:
//	        	pushq %rbp
//	        	movq %rsp, %rbp
//	        	movl $1, %esi
//	        	movl $1, %edi
//	        	callq add
//	        	movl %eax, %esi
//	        	leaq fmt(%rip), %rdi
//	        	movl $0, %eax
//	        	callq printf
//	        	movl $0, %eax
//	        	leave
//	        	ret
//	        .type main, @function
//	        .size main, .-main
//	        /* end function main */
//
//	        .data
//	        .balign 8
//	        fmt:
//	        	.ascii "One and one make %d!\n"
//	        	.byte 0
//	        /* end data */
//
//	        .section .note.GNU-stack,"",@progbits
//
//	        ---- output
//	        One and one make 2!
//	--- PASS: TestExec0 (0.03s)
//	PASS
//	ok  	modernc.org/libqbe	0.045s
//	~/src/modernc.org/libqbe$
//
// [Phi]: https://c9x.me/compile/doc/il.html#Phi
// [QBE Intermediate Language]: https://c9x.me/compile/doc/il.html
// [QBE command]: https://c9x.me/compile/
// [QBE vs LLVM]: https://c9x.me/compile/doc/llvm.html
package libqbe // import "modernc.org/libqbe"

import (
	"bufio"
	"fmt"
	"io"
	"runtime"
	"sort"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libc/stdio"
	"modernc.org/libc/sys/types"
)

func qsort(t *libc.TLS, qbe *_QBE, base uintptr, nmemb, size types.Size_t, compar uintptr) {
	sort.Sort(&sorter{
		qbe:  qbe,
		len:  int(nmemb),
		base: base,
		sz:   uintptr(size),
		f: (*struct {
			f func(*libc.TLS, *_QBE, uintptr, uintptr) int32
		})(unsafe.Pointer(&struct{ uintptr }{compar})).f,
		t: t,
	})
}

type sorter struct {
	qbe  *_QBE
	len  int
	base uintptr
	sz   uintptr
	f    func(*libc.TLS, *_QBE, uintptr, uintptr) int32
	t    *libc.TLS
}

func (s *sorter) Len() int { return s.len }

func (s *sorter) Less(i, j int) bool {
	return s.f(s.t, s.qbe, s.base+uintptr(i)*s.sz, s.base+uintptr(j)*s.sz) < 0
}

func (s *sorter) Swap(i, j int) {
	p := s.base + uintptr(i)*s.sz
	q := s.base + uintptr(j)*s.sz
	for i := 0; i < int(s.sz); i++ {
		*(*byte)(unsafe.Pointer(p)), *(*byte)(unsafe.Pointer(q)) = *(*byte)(unsafe.Pointer(q)), *(*byte)(unsafe.Pointer(p))
		p++
		q++
	}
}

func _err(tls *libc.TLS, qbe *_QBE, s uintptr, va uintptr) {
	const sz = 1000
	p := libc.Xmalloc(tls, sz)
	if p == 0 {
		panic("OOM")
	}

	defer libc.Xfree(tls, p)

	n := libc.Xvsnprintf(tls, p, sz, s, va)
	panic(fmt.Errorf("%s:%d: %s", libc.GoString(qbe.s_inpath), qbe.s_lnum, libc.GoBytes(p, int(n))))
}

func _die_(tls *libc.TLS, qbe *_QBE, file uintptr, s uintptr, va uintptr) {
	const sz = 1000
	p := libc.Xmalloc(tls, sz)
	if p == 0 {
		panic("OOM")
	}

	defer libc.Xfree(tls, p)

	n := libc.Xvsnprintf(tls, p, sz, s, va)
	panic(fmt.Errorf("%s: dying: %s", libc.GoString(file), libc.GoBytes(p, int(n))))
}

func calloc(t *libc.TLS, qbe *_QBE, n, size types.Size_t) (r uintptr) {
	if r = libc.Xcalloc(t, n, size); r != 0 {
		qbe.mallocs[r] = struct{}{}
	}
	return r
}

func free(t *libc.TLS, qbe *_QBE, p uintptr) {
	if p != 0 {
		delete(qbe.mallocs, p)
		libc.Xfree(t, p)
	}
}

func fgetc(t *libc.TLS, qbe *_QBE, stream uintptr) (r int32) {
	switch stream {
	case libc.Xstdin:
		c, err := qbe.r.ReadByte()
		if err != nil {
			return stdio.EOF
		}

		return int32(c)
	default:
		panic(todo("stream=%#0x", stream))
	}
}

func fscanf(t *libc.TLS, qbe *_QBE, stream, format, va uintptr) (r int32) {
	switch stream {
	case libc.Xstdin:
		return scanf(qbe.r, format, va)
	default:
		panic(todo("stream=%#0x format=%q va=%#0x", stream, libc.GoString(format), va))
	}
}

func ungetc(t *libc.TLS, qbe *_QBE, c int32, stream uintptr) int32 {
	switch stream {
	case libc.Xstdin:
		qbe.r.UnreadByte()
		return c
	default:
		panic(todo("stream=%#0x", stream))
	}
}

func fprintf(t *libc.TLS, qbe *_QBE, stream, format, args uintptr) int32 {
	b := printf(format, args)
	switch stream {
	case libc.Xstdout:
		n, err := qbe.w.Write(b)
		if err != nil {
			return -1
		}

		return int32(n)
	case libc.Xstderr:
		n, err := qbe.stderr.Write(b)
		if err != nil {
			return -1
		}

		return int32(n)
	default:
		panic(todo("stream=%#0x format=%q", stream, libc.GoString(format)))
	}
}

func fputc(t *libc.TLS, qbe *_QBE, c int32, stream uintptr) int32 {
	switch stream {
	case libc.Xstdout:
		_, err := qbe.w.Write([]byte{byte(c)})
		if err != nil {
			return -1
		}

		return int32(byte(c))
	case libc.Xstderr:
		_, err := qbe.stderr.Write([]byte{byte(c)})
		if err != nil {
			return -1
		}

		return int32(byte(c))
	default:
		panic(todo("c=%#U stream=%#0x", c, stream))
	}
}

func fputs(t *libc.TLS, qbe *_QBE, s, stream uintptr) int32 {
	switch stream {
	case libc.Xstdout:
		s := libc.GoString(s)
		n, err := qbe.w.Write([]byte(s))
		if err != nil {
			return stdio.EOF
		}

		return int32(n)
	default:
		panic(todo("s=%q stream=%#0x", libc.GoString(s), stream))
	}
}

// Options configure main.
//
// No options are defined at the moment.
type Options struct{}

// Main compiles QBE SSA source code from 'r', pretending it comes from file
// 'name', and writes the resulting assembly source code to 'w'.
//
// The 'target' argument selects the resulting assembly ABI and it must be one
// of
//
//	amd64_apple
//	amd64_sysv
//	arm64
//	arm64_apple
//	rv64
//
// Passing nil 'opts' is ok.
func Main(target, name string, r io.Reader, w io.Writer, opts *Options) (err error) {
	switch target {
	case
		"amd64_apple",
		"amd64_sysv",
		"arm64",
		"arm64_apple",
		"rv64":

		// ok
	default:
		return fmt.Errorf("unknown/unsupported target: %q", target)
	}

	tls := libc.NewTLS()

	defer tls.Close()

	infn, err := libc.CString(name)
	if err != nil {
		return err
	}

	defer libc.Xfree(tls, infn)

	cTarget, err := libc.CString(target)
	if err != nil {
		return err
	}

	defer libc.Xfree(tls, cTarget)

	var pinner runtime.Pinner
	q := newQBE(&pinner)

	defer pinner.Unpin()

	defer func() {
		for k := range q.mallocs {
			libc.Xfree(tls, k)
		}
		switch x := recover().(type) {
		case nil:
			// ok
		case error:
			if err == nil {
				err = x
			}
		default:
			if err == nil {
				err = fmt.Errorf("%v", x)
			}
		}
	}()

	switch x, ok := r.(io.ByteScanner); {
	case ok:
		q.r = x
	default:
		q.r = bufio.NewReader(r)
	}
	q.w = w
	if rc := x_main(tls, q, infn, cTarget); rc != 0 {
		return fmt.Errorf("FAIL rc=%v", rc)
	}

	return nil
}

// DefaultTarget returns the QBE target string for given goos/goarch. It
// defaults to "amd64_sysv" on unsupported goos/goarch combinations.
func DefaultTarget(goos, goarch string) string {
	switch fmt.Sprintf("%s/%s", goos, goarch) {
	case "darwin/amd64":
		return "amd64_apple"
	case "darwin/arm64":
		return "arm64_apple"
	case "freebsd/arm64":
		return "arm64"
	case "linux/arm64":
		return "arm64"
	case "linux/riscv64":
		return "rv64"
	default:
		return "amd64_sysv"
	}
}
