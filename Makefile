# Copyright 2023 The libqbe-go Authors. All rights reserved.
# Use of the source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	all clean dbg dev edit editor generate test work

DIR = /tmp/libqbe

all: editor
	golint 2>&1
	staticcheck 2>&1

build_all_targets:
	./build_all_targets.sh
	echo done

clean:
	rm -f log-* cpu.test mem.test *.out go.work*
	go clean

edit:
	@touch log
	@if [ -f "Session.vim" ]; then novim -S & else novim -p Makefile go.mod builder.json all_test.go generator.go libqbe.go & fi

editor:
	gofmt -l -s -w . 2>&1 | tee log-editor
	go test -c -o /dev/null 2>&1 | tee -a log-editor
	go build -v  -o /dev/null ./... 2>&1 | tee -a log-editor
	go build -o /dev/null generator*.go

generate:
	mkdir -p $(DIR) || true
	rm -rf $(DIR)/*
	echo -n > log-generate
	echo -n > log-generate-errors
	GO_GENERATE_DIR=$(DIR) go run generator*.go 2> log-generate-errors | tee log-generate
	cat log-generate-errors
	go build -v ./...
	# go install github.com/mdempsky/unconvert@latest
	grep 'TRC\|TODO\|ERRORF\|FAIL\|# PASS' log-generate || true
	grep 'TRC\|TODO\|ERRORF\|FAIL' log-generate-errors || true
	go build -v ./...  | tee -a log-generate
	git status
	# grep -o 'libc\.X[a-zA-Z0-9_]\+' ccgo*.go | sort -u | tee -a log-generate

dev:
	mkdir -p $(DIR) || true
	rm -rf $(DIR)/*
	echo -n > /tmp/ccgo.log
	echo -n > log-generate
	echo -n > log-generate-errors
	date 2>&1 | tee -a log-generate
	# GO_GENERATE_DIR=$(DIR) GO_GENERATE_DEV=1 go run -tags=ccgo.dmesg,ccgo.assert generator*.go 2>&1 | tee -a log-generate
	GO_GENERATE_DIR=$(DIR) GO_GENERATE_DEV=1 go run -tags=ccgo.assert generator*.go 2>&1 | tee -a log-generate
	date 2>&1 | tee -a log-generate
	grep 'TRC\|TODO\|ERRORF\|FAIL' log-generate || true
	grep 'TRC\|TODO\|ERRORF\|FAIL' log-generate-errors || true
	grep 'TRC\|TODO\|ERRORF\|FAIL' /tmp/ccgo.log || true
	go build -v ./...  | tee -a log-generate
	git status
	# grep -o 'libc\.X[a-zA-Z0-9_]\+' ccgo*.go | sort -u | tee -a log-generate

test:
	go test -v -timeout 24h 2>&1 | tee log-test

work:
	rm -f go.work*
	go work init
	go work use .
	go work use ../cc/v4
	go work use ../ccgo/v3
	go work use ../ccgo/v4
	go work use ../gc/v3
	go work use ../libc

dbg:
	make dev
	mv /tmp/libqbe/c9x.me/qbe/qbe ~/bin
	qbe -dpmncafailsr /tmp/libqbe/c9x.me/qbe/test/puts10.ssa 2>&1 | tee log-a
	go test -run TestCompile/amd64_sysv -re puts10.ssa 2>&1 | tee log-b
	
