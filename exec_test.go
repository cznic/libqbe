// Copyright 2023 The libqbe-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build (amd64 && (linux || darwin || freebsd || netbsd || openbsd)) || (arm64 && (linux || darwin || freebsd)) || (riscv64 && linux)

package libqbe // import "modernc.org/libqbe"

import (
	"bytes"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	"github.com/pmezard/go-difflib/difflib"
	cc4 "modernc.org/cc/v4"
	"modernc.org/libc"
)

func init() {
	cfg, err := cc4.NewConfig(goos, goarch)
	if err != nil {
		panic(err)
	}

	cc = cfg.CC
}

func TestExec0(t *testing.T) {
	t.Logf("using C compiler at %s", cc)
	temp := t.TempDir()
	tls := libc.NewTLS()

	defer tls.Close()

	err := filepath.WalkDir(filepath.Join("testdata"), func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() || !strings.HasSuffix(path, ".ssa") {
			return nil
		}

		asm, out, err := test0(tls, temp, DefaultTarget(goos, goarch), path, "", nil, true)
		if err != nil {
			t.Error(err)
			return nil
		}

		b, _ := os.ReadFile(path)
		t.Logf("\n---- qbe\n%s\n---- asm\n%s\n---- output\n%s", b, asm, out)
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func TestExec(t *testing.T) {
	t.Logf("using C compiler at %s", cc)
	tempDir := t.TempDir()
	tls := libc.NewTLS()
	blacklist := map[string]struct{}{}
	var files, skip, ok int
	dir := filepath.Join("internal", "test")
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		base := filepath.Base(path)

		if info.IsDir() || strings.HasPrefix(base, "_") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := blacklist[base]; ok {
				return nil
			}
		}

		files++
		b, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		x := bytes.IndexByte(b, '\n')
		l0 := string(b[:x])
		if strings.HasPrefix(l0, "# skip") {
			a := strings.Fields(l0)
			m := map[string]struct{}{}
			for _, v := range a[2:] {
				m[v] = struct{}{}
			}
			switch fmt.Sprintf("%s/%s", runtime.GOOS, runtime.GOARCH) {
			case
				"darwin/arm64",
				"freebsd/arm64",
				"linux/arm64":

				if _, ok := m["arm64"]; ok {
					skip++
					return nil
				}
			case "linux/riscv64":
				if _, ok := m["rv64"]; ok {
					skip++
					return nil
				}
			}
		}

		switch filepath.Ext(path) {
		case ".ssa":
			if *oTrc {
				fmt.Printf("%s\n", path)
			}

			driver, want := extractDriverAndOutput(b)
			driverPath := filepath.Join(tempDir, "driver.c")
			switch {
			case len(driver) == 0:
				driverPath = ""
			default:
				if err := os.WriteFile(driverPath, []byte(driver), 0660); err != nil {
					t.Logf("driver=%s", driver)
					return fmt.Errorf("failed to write driver.c: %v", err)
				}
			}

			_, out, err := test0(tls, tempDir, DefaultTarget(goos, goarch), path, driverPath, []string{"a", "b", "c"}, true)
			if err != nil {
				t.Error(err)
				return nil
			}

			if len(out) != 0 && len(want) == 0 {
				t.Errorf("%s: unexpected output: %s", path, out)
				return nil
			}

			if len(want) != 0 {
				g, e := string(out), want
				if strings.HasSuffix(g, "\n") {
					g = g[:len(g)-1]
				}
				if g != e {
					diff := difflib.UnifiedDiff{
						A:        difflib.SplitLines(e),
						B:        difflib.SplitLines(g),
						FromFile: path + ".expect",
						ToFile:   path + ".got",
						Context:  3,
					}
					text, _ := difflib.GetUnifiedDiffString(diff)
					t.Errorf("%v: output differs:\n%s\n---- x.c\n%s", path, text, out)
					return nil
				}
			}

			ok++
			return nil
		}
		return nil
	})
	t.Logf("files %v, skip %v, ok %v", files, skip, ok)
	if err != nil {
		t.Fatal(err)
	}
}

func extractDriverAndOutput(b []byte) (d, o string) {
	a := strings.Split(strings.TrimSpace(string(b)), "\n")
	var driver, output []string
	var p *[]string
	for _, line := range a {
		if !strings.HasPrefix(line, "#") {
			continue
		}

		if strings.HasSuffix(line, "#") {
			line = line[:len(line)-1]
		}
		switch {
		case strings.HasPrefix(line, "# >>> driver"):
			p = &driver
		case strings.HasPrefix(line, "# >>> output"):
			p = &output
		case strings.HasPrefix(line, "# <<<"):
			p = nil
		default:
			if p != nil {
				*p = append(*p, line[2:])
			}
		}
	}
	return strings.Join(driver, "\n"), strings.Join(output, "\n")
}
