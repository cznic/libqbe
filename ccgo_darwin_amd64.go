// Code generated for darwin/amd64 by 'cc --prefix-enumerator=_ --prefix-external=x_ --prefix-macro=m_ --prefix-static-internal=s_ --prefix-static-none=s_ --prefix-tagged-enum=_ --prefix-tagged-struct=_ --prefix-tagged-union=_ --prefix-typename=_ --prefix-undefined=_ -import io,runtime -D__CCGO__ -extended-errors -DNDEBUG -mlong-double-64 main.o.go util.o.go parse.o.go abi.o.go cfg.o.go mem.o.go ssa.o.go alias.o.go load.o.go copy.o.go fold.o.go simpl.o.go live.o.go spill.o.go rega.o.go emit.o.go amd64/targ.o.go amd64/sysv.o.go amd64/isel.o.go amd64/emit.o.go arm64/targ.o.go arm64/abi.o.go arm64/isel.o.go arm64/emit.o.go rv64/targ.o.go rv64/abi.o.go rv64/isel.o.go rv64/emit.o.go -o qbe.go', DO NOT EDIT.

//go:build darwin && amd64

package libqbe

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"

	"io"
	"runtime"
)

var _ reflect.Type

var _ unsafe.Pointer

const m___INT_MAX__ = 2147483647

const m___SCHAR_MAX__ = 127

type ___predefined_size_t = uint64

type ___predefined_ptrdiff_t = int64

type ___uint32_t = uint32

type ___darwin_ct_rune_t = int32

type _int64_t = int64

type _uint64_t = uint64

type _int32_t = int32

type _uint32_t = uint32

type _va_list = uintptr

type _size_t = uint64

type _uchar = uint8

type _uint = uint32

type _ulong = uint64

type _bits = uint64

type _BSet = struct {
	Fnt _uint
	Ft  uintptr
}

type _Ref = struct {
	F__ccgo0 uint32
}

type _Op = struct {
	Fname     uintptr
	Fargcls   [2][4]int16
	F__ccgo24 uint8
}

type _Ins = struct {
	F__ccgo0 uint32
	Fto      _Ref
	Farg     [2]_Ref
}

type _Phi = struct {
	Fto   _Ref
	Farg  uintptr
	Fblk  uintptr
	Fnarg _uint
	Fcls  int32
	Flink uintptr
}

type _Blk = struct {
	Fphi  uintptr
	Fins  uintptr
	Fnins _uint
	Fjmp  struct {
		Ftype1 int16
		Farg   _Ref
	}
	Fs1    uintptr
	Fs2    uintptr
	Flink  uintptr
	Fid    _uint
	Fvisit _uint
	Fidom  uintptr
	Fdom   uintptr
	Fdlink uintptr
	Ffron  uintptr
	Fnfron _uint
	Fpred  uintptr
	Fnpred _uint
	Fin    [1]_BSet
	Fout   [1]_BSet
	Fgen   [1]_BSet
	Fnlive [2]int32
	Floop  int32
	Fname  [80]int8
}

type _Use = struct {
	Ftype1 int32
	Fbid   _uint
	Fu     struct {
		Fphi [0]uintptr
		Fins uintptr
	}
}

const _UPhi = 1

const _UIns = 2

const _UJmp = 3

type _Sym = struct {
	Ftype1 int32
	Fid    _uint32_t
}

const _SGlo = 0

const _SThr = 1

type _Num = struct {
	Fn  _uchar
	Fnl _uchar
	Fnr _uchar
	Fl  _Ref
	Fr  _Ref
}

type _Alias = struct {
	Ftype1  int32
	Fbase   int32
	Foffset _int64_t
	Fu      struct {
		Floc [0]struct {
			Fsz int32
			Fm  _bits
		}
		Fsym         _Sym
		F__ccgo_pad2 [8]byte
	}
	Fslot uintptr
}

const _ABot = 0

const _ALoc = 1

const _ACon = 2

const _AEsc = 3

const _ASym = 4

const _AUnk = 6

type _Tmp = struct {
	Fname [80]int8
	Fdef  uintptr
	Fuse  uintptr
	Fndef _uint
	Fnuse _uint
	Fbid  _uint
	Fcost _uint
	Fslot int32
	Fcls  int16
	Fhint struct {
		Fr int32
		Fw int32
		Fm _bits
	}
	Fphi   int32
	Falias _Alias
	Fwidth int32
	Fvisit int32
}

const _WFull = 0

const _Wsb = 1

const _Wub = 2

const _Wsh = 3

const _Wuh = 4

const _Wsw = 5

const _Wuw = 6

type _Con = struct {
	Ftype1 int32
	Fsym   _Sym
	Fbits  struct {
		Fd [0]float64
		Fs [0]float32
		Fi _int64_t
	}
	Fflt int8
}

const _CUndef = 0

const _CBits = 1

const _CAddr = 2

type _Mem = struct {
	Foffset _Con
	Fbase   _Ref
	Findex  _Ref
	Fscale  int32
}

type _Fn = struct {
	Fstart    uintptr
	Ftmp      uintptr
	Fcon      uintptr
	Fmem      uintptr
	Fntmp     int32
	Fncon     int32
	Fnmem     int32
	Fnblk     _uint
	Fretty    int32
	Fretr     _Ref
	Frpo      uintptr
	Freg      _bits
	Fslot     int32
	Fsalign   int32
	Fvararg   int8
	Fdynalloc int8
	Fleaf     int8
	Fname     [80]int8
	Flnk      _Lnk
}

type _Typ = struct {
	Fname    [80]int8
	Fisdark  int8
	Fisunion int8
	Falign   int32
	Fsize    _uint64_t
	Fnunion  _uint
	Ffields  uintptr
}

type _Field = struct {
	Ftype1 int32
	Flen1  _uint
}

const _FEnd = 0

const _Fb = 1

const _Fh = 2

const _Fw = 3

const _Fl = 4

const _Fs = 5

const _Fd = 6

const _FPad = 7

const _FTyp = 8

type _Dat = struct {
	Ftype1 int32
	Fname  uintptr
	Flnk   uintptr
	Fu     struct {
		Ffltd [0]float64
		Fflts [0]float32
		Fstr  [0]uintptr
		Fref  [0]struct {
			Fname uintptr
			Foff  _int64_t
		}
		Fnum         _int64_t
		F__ccgo_pad5 [8]byte
	}
	Fisref int8
	Fisstr int8
}

const _DStart = 0

const _DEnd = 1

const _DB = 2

const _DH = 3

const _DW = 4

const _DL = 5

const _DZ = 6

type _Lnk = struct {
	Fexport int8
	Fthread int8
	Fcommon int8
	Falign  int8
	Fsec    uintptr
	Fsecf   uintptr
}

type _Target = struct {
	Fname    [16]int8
	Fapple   int8
	Fgpr0    int32
	Fngpr    int32
	Ffpr0    int32
	Fnfpr    int32
	Frglob   _bits
	Fnrglob  int32
	Frsave   uintptr
	Fnrsave  [2]int32
	Fretregs uintptr
	Fargregs uintptr
	Fmemargs uintptr
	Fabi0    uintptr
	Fabi1    uintptr
	Fisel    uintptr
	Femitfn  uintptr
	Femitfin uintptr
	Fasloc   [4]int8
	Fassym   [4]int8
}

const _NString = 80

const _NIns = 1048576

const _NField = 32

const _NBit = 64

const _Tmp0 = 64

const _RTmp = 0

const _RCon = 1

const _RInt = 2

const _RType = 3

const _RSlot = 4

const _RCall = 5

const _RMem = 6

const _Cieq = 0

const _Cine = 1

const _Cisge = 2

const _Cisgt = 3

const _Cisle = 4

const _Cislt = 5

const _Ciuge = 6

const _Ciugt = 7

const _Ciule = 8

const _Ciult = 9

const _NCmpI = 10

const _Cfeq = 0

const _Cfge = 1

const _Cfgt = 2

const _Cfle = 3

const _Cflt = 4

const _Cfne = 5

const _Cfo = 6

const _Cfuo = 7

const _NCmp = 18

const _Oadd = 1

const _Osub = 2

const _Oneg = 3

const _Odiv = 4

const _Orem = 5

const _Oudiv = 6

const _Ourem = 7

const _Omul = 8

const _Oand = 9

const _Oor = 10

const _Oxor = 11

const _Osar = 12

const _Oshr = 13

const _Oshl = 14

const _Ocsltl = 30

const _Ocultl = 34

const _Oceqs = 35

const _Ocges = 36

const _Ocgts = 37

const _Ocles = 38

const _Oclts = 39

const _Oceqd = 43

const _Ocged = 44

const _Ocgtd = 45

const _Ocled = 46

const _Ocltd = 47

const _Ostoreb = 51

const _Ostoreh = 52

const _Ostorew = 53

const _Ostorel = 54

const _Ostores = 55

const _Ostored = 56

const _Oloadsb = 57

const _Oloadub = 58

const _Oloadsh = 59

const _Oloaduh = 60

const _Oloadsw = 61

const _Oloaduw = 62

const _Oload = 63

const _Oextsb = 64

const _Oextub = 65

const _Oextsh = 66

const _Oextuh = 67

const _Oextsw = 68

const _Oextuw = 69

const _Oexts = 70

const _Otruncd = 71

const _Ostosi = 72

const _Ostoui = 73

const _Odtosi = 74

const _Odtoui = 75

const _Oswtof = 76

const _Ouwtof = 77

const _Osltof = 78

const _Oultof = 79

const _Ocast = 80

const _Oalloc4 = 81

const _Oalloc8 = 82

const _Oalloc16 = 83

const _Ovaarg = 84

const _Ovastart = 85

const _Ocopy = 86

const _Odbgloc = 87

const _Onop = 88

const _Oaddr = 89

const _Oblit0 = 90

const _Oblit1 = 91

const _Oswap = 92

const _Osign = 93

const _Osalloc = 94

const _Oxidiv = 95

const _Oxdiv = 96

const _Oxcmp = 97

const _Oxtest = 98

const _Oacmp = 99

const _Oacmn = 100

const _Oafcmp = 101

const _Oreqz = 102

const _Ornez = 103

const _Opar = 104

const _Oparsb = 105

const _Oparub = 106

const _Oparsh = 107

const _Oparuh = 108

const _Oparc = 109

const _Opare = 110

const _Oarg = 111

const _Oargsb = 112

const _Oargub = 113

const _Oargsh = 114

const _Oarguh = 115

const _Oargc = 116

const _Oarge = 117

const _Oargv = 118

const _Ocall = 119

const _Oflagfo = 136

const _Oflagfuo = 137

const _NOp = 138

const _Jxxx = 0

const _Jretw = 1

const _Jretl = 2

const _Jrets = 3

const _Jretd = 4

const _Jretsb = 5

const _Jretub = 6

const _Jretsh = 7

const _Jretuh = 8

const _Jretc = 9

const _Jret0 = 10

const _Jjmp = 11

const _Jjnz = 12

const _Jjfine = 14

const _Jhlt = 31

const _Ocmpw = 15

const _Ocmpw1 = 24

const _Ocmpl = 25

const _Ocmpl1 = 34

const _Ocmps = 35

const _Ocmps1 = 42

const _Ocmpd = 43

const _Ocmpd1 = 50

const _Oalloc = 81

const _Oalloc1 = 83

const _Oflag = 120

const _NPubOp = 88

const _Jjf = 13

const _Kx = -1

const _Kw = 0

const _Kl = 1

const _Ks = 2

const _Kd = 3

const _NoAlias = 0

const _MayAlias = 1

const _MustAlias = 2

type _Addr = struct {
	Foffset _Con
	Fbase   _Ref
	Findex  _Ref
	Fscale  int32
}

type _Pool = int32

const _PHeap = 0

const _PFn = 1

func s_data(tls *libc.TLS, qbe *_QBE, d uintptr) {
	if qbe.s_dbg != 0 {
		return
	}
	x_emitdat(tls, qbe, d, qbe.s_outf)
	if (*_Dat)(unsafe.Pointer(d)).Ftype1 == int32(_DEnd) {
		fputs(tls, qbe, __ccgo_ts, qbe.s_outf)
		x_freeall(tls, qbe)
	}
}

func s_func(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var n _uint
	_ = n
	if qbe.s_dbg != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+17, libc.VaList(bp+8, fn+83))
	}
	if qbe.x_debug[int32('P')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+39, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
	(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fabi0})))(tls, qbe, fn)
	x_fillrpo(tls, qbe, fn)
	x_fillpreds(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	x_promote(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	x_ssa(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	x_ssacheck(tls, qbe, fn)
	x_fillalias(tls, qbe, fn)
	x_loadopt(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	x_fillalias(tls, qbe, fn)
	x_coalesce(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	x_ssacheck(tls, qbe, fn)
	x_copy(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	x_fold(tls, qbe, fn)
	(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fabi1})))(tls, qbe, fn)
	x_simpl(tls, qbe, fn)
	x_fillpreds(tls, qbe, fn)
	x_filluse(tls, qbe, fn)
	(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fisel})))(tls, qbe, fn)
	x_fillrpo(tls, qbe, fn)
	x_filllive(tls, qbe, fn)
	x_fillloop(tls, qbe, fn)
	x_fillcost(tls, qbe, fn)
	x_spill(tls, qbe, fn)
	x_rega(tls, qbe, fn)
	x_fillrpo(tls, qbe, fn)
	x_simpljmp(tls, qbe, fn)
	x_fillpreds(tls, qbe, fn)
	x_fillrpo(tls, qbe, fn)
	n = uint32(0)
	for {
		if n == (*_Fn)(unsafe.Pointer(fn)).Fnblk-uint32(1) {
			(*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8)))).Flink = uintptr(0)
			break
		} else {
			(*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8)))).Flink = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n+uint32(1))*8))
		}
		goto _1
	_1:
		;
		n++
	}
	if !(qbe.s_dbg != 0) {
		(*(*func(*libc.TLS, *_QBE, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Femitfn})))(tls, qbe, fn, qbe.s_outf)
		fprintf(tls, qbe, qbe.s_outf, __ccgo_ts+58, libc.VaList(bp+8, fn+83))
	} else {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+82, 0)
	}
	x_freeall(tls, qbe)
}

func s_dbgfile(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	x_emitdbgfile(tls, qbe, fn, qbe.s_outf)
}

func x_main(tls *libc.TLS, qbe *_QBE, f uintptr, target uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var c int32
	var hf, inf, sep, t uintptr
	_, _, _, _, _ = c, hf, inf, sep, t
	qbe.x_T = qbe.x_T_amd64_apple
	qbe.s_outf = libc.X__stdoutp
	t = uintptr(unsafe.Pointer(&qbe.s_tlist))
	for {
		if !(*(*uintptr)(unsafe.Pointer(t)) != 0) {
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+84, libc.VaList(bp+8, target))
			return int32(1)
		}
		if libc.Xstrcmp(tls, target, *(*uintptr)(unsafe.Pointer(t))) == 0 {
			qbe.x_T = *(*_Target)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(t))))
			break
		}
		goto _1
	_1:
		;
		t += 8
	}
	inf = libc.X__stdinp
	x_parse(tls, qbe, inf, f, __ccgo_fp(s_dbgfile), __ccgo_fp(s_data), __ccgo_fp(s_func))
	(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Femitfin})))(tls, qbe, qbe.s_outf)
	return 0
}

type _Vec = struct {
	Fmag  _ulong
	Fpool _Pool
	Fesz  _size_t
	Fcap1 _ulong
}

type _Bucket = struct {
	Fnstr _uint
	Fstr  uintptr
}

const _VMin = 2

const _VMag = 212581022

const _NPtr = 256

const _IBits = 12

const _IMask = 4095

func x_hash(tls *libc.TLS, qbe *_QBE, s uintptr) (r _uint32_t) {
	var h _uint32_t
	_ = h
	h = uint32(0)
	for {
		if !(*(*int8)(unsafe.Pointer(s)) != 0) {
			break
		}
		h = libc.Uint32FromInt8(*(*int8)(unsafe.Pointer(s))) + uint32(17)*h
		goto _1
	_1:
		;
		s++
	}
	return h
}

func x_die_(tls *libc.TLS, qbe *_QBE, file uintptr, s uintptr, va uintptr) {
}

func x_emalloc(tls *libc.TLS, qbe *_QBE, n _size_t) (r uintptr) {
	var p uintptr
	_ = p
	p = calloc(tls, qbe, uint64(1), n)
	if !(p != 0) {
		_die_(tls, qbe, __ccgo_ts+105, __ccgo_ts+112, 0)
	}
	return p
}

func x_alloc(tls *libc.TLS, qbe *_QBE, n _size_t) (r uintptr) {
	var pp, v1 uintptr
	var v2 int32
	_, _, _ = pp, v1, v2
	if n == uint64(0) {
		return uintptr(0)
	}
	if qbe.s_nptr >= int32(_NPtr) {
		pp = x_emalloc(tls, qbe, uint64(_NPtr)*libc.Uint64FromInt64(8))
		*(*uintptr)(unsafe.Pointer(pp)) = qbe.s_pool
		qbe.s_pool = pp
		qbe.s_nptr = int32(1)
	}
	v1 = x_emalloc(tls, qbe, n)
	v2 = qbe.s_nptr
	qbe.s_nptr++
	*(*uintptr)(unsafe.Pointer(qbe.s_pool + uintptr(v2)*8)) = v1
	return v1
}

func x_freeall(tls *libc.TLS, qbe *_QBE) {
	var pp uintptr
	_ = pp
	for {
		pp = qbe.s_pool + 1*8
		for {
			if !(pp < qbe.s_pool+uintptr(qbe.s_nptr)*8) {
				break
			}
			free(tls, qbe, *(*uintptr)(unsafe.Pointer(pp)))
			goto _2
		_2:
			;
			pp += 8
		}
		pp = *(*uintptr)(unsafe.Pointer(qbe.s_pool))
		if !(pp != 0) {
			break
		}
		free(tls, qbe, qbe.s_pool)
		qbe.s_pool = pp
		qbe.s_nptr = int32(_NPtr)
		goto _1
	_1:
	}
	qbe.s_nptr = int32(1)
}

func x_vnew(tls *libc.TLS, qbe *_QBE, len1 _ulong, esz _size_t, pool _Pool) (r uintptr) {
	var cap1 _ulong
	var f, v, v2 uintptr
	_, _, _, _ = cap1, f, v, v2
	cap1 = uint64(_VMin)
	for {
		if !(cap1 < len1) {
			break
		}
		goto _1
	_1:
		;
		cap1 *= uint64(2)
	}
	if pool == int32(_PHeap) {
		v2 = __ccgo_fp(x_emalloc)
	} else {
		v2 = __ccgo_fp(x_alloc)
	}
	f = v2
	v = (*(*func(*libc.TLS, *_QBE, _size_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{f})))(tls, qbe, cap1*esz+uint64(32))
	(*_Vec)(unsafe.Pointer(v)).Fmag = uint64(_VMag)
	(*_Vec)(unsafe.Pointer(v)).Fcap1 = cap1
	(*_Vec)(unsafe.Pointer(v)).Fesz = esz
	(*_Vec)(unsafe.Pointer(v)).Fpool = pool
	return v + uintptr(1)*32
}

func x_vfree(tls *libc.TLS, qbe *_QBE, p uintptr) {
	var v uintptr
	_ = v
	v = p - uintptr(1)*32
	if (*_Vec)(unsafe.Pointer(v)).Fpool == int32(_PHeap) {
		(*_Vec)(unsafe.Pointer(v)).Fmag = uint64(0)
		free(tls, qbe, v)
	}
}

func x_vgrow(tls *libc.TLS, qbe *_QBE, vp uintptr, len1 _ulong) {
	var v, v1 uintptr
	_, _ = v, v1
	v = *(*uintptr)(unsafe.Pointer(vp)) - uintptr(1)*32
	if (*_Vec)(unsafe.Pointer(v)).Fcap1 >= len1 {
		return
	}
	v1 = x_vnew(tls, qbe, len1, (*_Vec)(unsafe.Pointer(v)).Fesz, (*_Vec)(unsafe.Pointer(v)).Fpool)
	libc.X__builtin___memcpy_chk(tls, v1, v+uintptr(1)*32, (*_Vec)(unsafe.Pointer(v)).Fcap1*(*_Vec)(unsafe.Pointer(v)).Fesz, ^___predefined_size_t(0))
	x_vfree(tls, qbe, v+uintptr(1)*32)
	*(*uintptr)(unsafe.Pointer(vp)) = v1
}

func x_strf(tls *libc.TLS, qbe *_QBE, str uintptr, s uintptr, va uintptr) {
	var ap _va_list
	_ = ap
	ap = va
	libc.X__builtin___vsnprintf_chk(tls, str, uint64(_NString), 0, ^___predefined_size_t(0), s, ap)
	_ = ap
}

func x_intern(tls *libc.TLS, qbe *_QBE, s uintptr) (r _uint32_t) {
	var b uintptr
	var h _uint32_t
	var i, n _uint
	_, _, _, _ = b, h, i, n
	h = x_hash(tls, qbe, s) & uint32(_IMask)
	b = uintptr(unsafe.Pointer(&qbe.s_itbl)) + uintptr(h)*16
	n = (*_Bucket)(unsafe.Pointer(b)).Fnstr
	i = uint32(0)
	for {
		if !(i < n) {
			break
		}
		if libc.Xstrcmp(tls, s, *(*uintptr)(unsafe.Pointer((*_Bucket)(unsafe.Pointer(b)).Fstr + uintptr(i)*8))) == 0 {
			return h + i<<int32(_IBits)
		}
		goto _1
	_1:
		;
		i++
	}
	if n == libc.Uint32FromInt32(libc.Int32FromInt32(1)<<(libc.Int32FromInt32(32)-int32(_IBits))) {
		_die_(tls, qbe, __ccgo_ts+105, __ccgo_ts+135, 0)
	}
	if n == uint32(0) {
		(*_Bucket)(unsafe.Pointer(b)).Fstr = x_vnew(tls, qbe, uint64(1), uint64(8), int32(_PHeap))
	} else {
		if n&(n-uint32(1)) == uint32(0) {
			x_vgrow(tls, qbe, b+8, uint64(n+n))
		}
	}
	*(*uintptr)(unsafe.Pointer((*_Bucket)(unsafe.Pointer(b)).Fstr + uintptr(n)*8)) = x_emalloc(tls, qbe, libc.Xstrlen(tls, s)+uint64(1))
	(*_Bucket)(unsafe.Pointer(b)).Fnstr = n + uint32(1)
	libc.X__builtin___strcpy_chk(tls, *(*uintptr)(unsafe.Pointer((*_Bucket)(unsafe.Pointer(b)).Fstr + uintptr(n)*8)), s, ^___predefined_size_t(0))
	return h + n<<int32(_IBits)
}

func x_str(tls *libc.TLS, qbe *_QBE, id _uint32_t) (r uintptr) {
	return *(*uintptr)(unsafe.Pointer(qbe.s_itbl[id&uint32(_IMask)].Fstr + uintptr(id>>int32(_IBits))*8))
}

func x_isreg(tls *libc.TLS, qbe *_QBE, _r _Ref) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	;
	return libc.BoolInt32(v6 == int32(_RTmp) && int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) < int32(_Tmp0))
}

func x_iscmp(tls *libc.TLS, qbe *_QBE, op int32, pk uintptr, pc uintptr) (r int32) {
	if int32(_Ocmpw) <= op && op <= int32(_Ocmpw1) {
		*(*int32)(unsafe.Pointer(pc)) = op - int32(_Ocmpw)
		*(*int32)(unsafe.Pointer(pk)) = int32(_Kw)
	} else {
		if int32(_Ocmpl) <= op && op <= int32(_Ocmpl1) {
			*(*int32)(unsafe.Pointer(pc)) = op - int32(_Ocmpl)
			*(*int32)(unsafe.Pointer(pk)) = int32(_Kl)
		} else {
			if int32(_Ocmps) <= op && op <= int32(_Ocmps1) {
				*(*int32)(unsafe.Pointer(pc)) = int32(_NCmpI) + op - int32(_Ocmps)
				*(*int32)(unsafe.Pointer(pk)) = int32(_Ks)
			} else {
				if int32(_Ocmpd) <= op && op <= int32(_Ocmpd1) {
					*(*int32)(unsafe.Pointer(pc)) = int32(_NCmpI) + op - int32(_Ocmpd)
					*(*int32)(unsafe.Pointer(pk)) = int32(_Kd)
				} else {
					return 0
				}
			}
		}
	}
	return int32(1)
}

func x_argcls(tls *libc.TLS, qbe *_QBE, i uintptr, n int32) (r int32) {
	return int32(*(*int16)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_optab)) + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))*32 + 8 + uintptr(n)*8 + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30))*2)))
}

func x_emit(tls *libc.TLS, qbe *_QBE, op int32, k int32, to _Ref, arg0 _Ref, arg1 _Ref) {
	var v1 uintptr
	_ = v1
	if qbe.x_curi == uintptr(unsafe.Pointer(&qbe.x_insb)) {
		_die_(tls, qbe, __ccgo_ts+105, __ccgo_ts+160, 0)
	}
	qbe.x_curi -= 16
	v1 = qbe.x_curi
	*(*_Ins)(unsafe.Pointer(v1)) = _Ins{
		F__ccgo0: libc.Uint32FromInt32(op)&0x3fffffff<<0 | libc.Uint32FromInt32(k)&0x3<<30,
		Fto:      to,
		Farg: [2]_Ref{
			0: arg0,
			1: arg1,
		},
	}
}

func x_emiti(tls *libc.TLS, qbe *_QBE, _i _Ins) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ins)(unsafe.Pointer(bp)) = _i
	x_emit(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0), int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), (*(*_Ins)(unsafe.Pointer(bp))).Fto, *(*_Ref)(unsafe.Pointer(bp + 8)), *(*_Ref)(unsafe.Pointer(bp + 8 + 1*4)))
}

func x_idup(tls *libc.TLS, qbe *_QBE, pd uintptr, s uintptr, n _ulong) {
	*(*uintptr)(unsafe.Pointer(pd)) = x_alloc(tls, qbe, n*uint64(16))
	if n != 0 {
		libc.X__builtin___memcpy_chk(tls, *(*uintptr)(unsafe.Pointer(pd)), s, n*uint64(16), ^___predefined_size_t(0))
	}
}

func x_icpy(tls *libc.TLS, qbe *_QBE, d uintptr, s uintptr, n _ulong) (r uintptr) {
	if n != 0 {
		libc.X__builtin___memcpy_chk(tls, d, s, n*uint64(16), ^___predefined_size_t(0))
	}
	return d + uintptr(n)*16
}

func x_cmpneg(tls *libc.TLS, qbe *_QBE, c int32) (r int32) {
	return *(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.s_cmptab)) + uintptr(c)*8))
}

func x_cmpop(tls *libc.TLS, qbe *_QBE, c int32) (r int32) {
	return *(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.s_cmptab)) + uintptr(c)*8 + 1*4))
}

func x_clsmerge(tls *libc.TLS, qbe *_QBE, pk uintptr, k int16) (r int32) {
	var k1 int16
	_ = k1
	k1 = *(*int16)(unsafe.Pointer(pk))
	if int32(k1) == int32(_Kx) {
		*(*int16)(unsafe.Pointer(pk)) = k
		return 0
	}
	if int32(k1) == int32(_Kw) && int32(k) == int32(_Kl) || int32(k1) == int32(_Kl) && int32(k) == int32(_Kw) {
		*(*int16)(unsafe.Pointer(pk)) = int16(_Kw)
		return 0
	}
	return libc.BoolInt32(int32(k1) != int32(k))
}

func x_phicls(tls *libc.TLS, qbe *_QBE, t int32, tmp uintptr) (r int32) {
	var t1 int32
	_ = t1
	t1 = (*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fphi
	if !(t1 != 0) {
		return t
	}
	t1 = x_phicls(tls, qbe, t1, tmp)
	(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fphi = t1
	return t1
}

func x_newtmp(tls *libc.TLS, qbe *_QBE, prfx uintptr, k int32, fn uintptr) (r _Ref) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var t, v1, v3 int32
	var v2 uintptr
	_, _, _, _ = t, v1, v2, v3
	v2 = fn + 32
	v1 = *(*int32)(unsafe.Pointer(v2))
	*(*int32)(unsafe.Pointer(v2))++
	t = v1
	x_vgrow(tls, qbe, fn+8, libc.Uint64FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
	libc.X__builtin___memset_chk(tls, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(t)*192, 0, uint64(192), ^___predefined_size_t(0))
	if prfx != 0 {
		qbe.s_n++
		v3 = qbe.s_n
		x_strf(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(t)*192, __ccgo_ts+188, libc.VaList(bp+8, prfx, v3))
	}
	(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fcls = int16(k)
	(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fslot = -int32(1)
	(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fnuse = libc.Uint32FromInt32(+libc.Int32FromInt32(1))
	(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fndef = libc.Uint32FromInt32(+libc.Int32FromInt32(1))
	return _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
	}
}

func x_chuse(tls *libc.TLS, qbe *_QBE, _r _Ref, du int32, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))).Fnuse += libc.Uint32FromInt32(du)
	}
}

func x_symeq(tls *libc.TLS, qbe *_QBE, s0 _Sym, s1 _Sym) (r int32) {
	return libc.BoolInt32(s0.Ftype1 == s1.Ftype1 && s0.Fid == s1.Fid)
}

func x_newcon(tls *libc.TLS, qbe *_QBE, c0 uintptr, fn uintptr) (r _Ref) {
	var c1, v3 uintptr
	var i, v2 int32
	_, _, _, _ = c1, i, v2, v3
	i = int32(1)
	for {
		if !(i < (*_Fn)(unsafe.Pointer(fn)).Fncon) {
			break
		}
		c1 = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(i)*32
		if (*_Con)(unsafe.Pointer(c0)).Ftype1 == (*_Con)(unsafe.Pointer(c1)).Ftype1 && x_symeq(tls, qbe, (*_Con)(unsafe.Pointer(c0)).Fsym, (*_Con)(unsafe.Pointer(c1)).Fsym) != 0 && *(*_int64_t)(unsafe.Pointer(c0 + 16)) == *(*_int64_t)(unsafe.Pointer(c1 + 16)) {
			return _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(i)&0x1fffffff<<3,
			}
		}
		goto _1
	_1:
		;
		i++
	}
	v3 = fn + 36
	*(*int32)(unsafe.Pointer(v3))++
	v2 = *(*int32)(unsafe.Pointer(v3))
	x_vgrow(tls, qbe, fn+16, libc.Uint64FromInt32(v2))
	*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(i)*32)) = *(*_Con)(unsafe.Pointer(c0))
	return _Ref{
		F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(i)&0x1fffffff<<3,
	}
}

func x_getcon(tls *libc.TLS, qbe *_QBE, val _int64_t, fn uintptr) (r _Ref) {
	var c, v2 int32
	var v3 uintptr
	_, _, _ = c, v2, v3
	c = int32(1)
	for {
		if !(c < (*_Fn)(unsafe.Pointer(fn)).Fncon) {
			break
		}
		if (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(c)*32))).Ftype1 == int32(_CBits) && *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(c)*32 + 16)) == val {
			return _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(c)&0x1fffffff<<3,
			}
		}
		goto _1
	_1:
		;
		c++
	}
	v3 = fn + 36
	*(*int32)(unsafe.Pointer(v3))++
	v2 = *(*int32)(unsafe.Pointer(v3))
	x_vgrow(tls, qbe, fn+16, libc.Uint64FromInt32(v2))
	*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(c)*32)) = _Con{
		Ftype1: int32(_CBits),
		Fbits: *(*struct {
			Fd [0]float64
			Fs [0]float32
			Fi _int64_t
		})(unsafe.Pointer(&struct{ f _int64_t }{f: val})),
	}
	return _Ref{
		F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(c)&0x1fffffff<<3,
	}
}

func x_addcon(tls *libc.TLS, qbe *_QBE, c0 uintptr, c1 uintptr, m int32) (r int32) {
	if m != int32(1) && (*_Con)(unsafe.Pointer(c1)).Ftype1 == int32(_CAddr) {
		return 0
	}
	if (*_Con)(unsafe.Pointer(c0)).Ftype1 == int32(_CUndef) {
		*(*_Con)(unsafe.Pointer(c0)) = *(*_Con)(unsafe.Pointer(c1))
		*(*_int64_t)(unsafe.Pointer(c0 + 16)) *= int64(m)
	} else {
		if (*_Con)(unsafe.Pointer(c1)).Ftype1 == int32(_CAddr) {
			if (*_Con)(unsafe.Pointer(c0)).Ftype1 == int32(_CAddr) {
				return 0
			}
			(*_Con)(unsafe.Pointer(c0)).Ftype1 = int32(_CAddr)
			(*_Con)(unsafe.Pointer(c0)).Fsym = (*_Con)(unsafe.Pointer(c1)).Fsym
		}
		*(*_int64_t)(unsafe.Pointer(c0 + 16)) += *(*_int64_t)(unsafe.Pointer(c1 + 16)) * int64(m)
	}
	return int32(1)
}

func x_salloc(tls *libc.TLS, qbe *_QBE, _rt _Ref, _rs _Ref, fn uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _rt
	*(*_Ref)(unsafe.Pointer(bp + 16)) = _rs
	var r0, r1, v1, v2, v3 _Ref
	var sz _int64_t
	var v4, v6 int32
	_, _, _, _, _, _, _, _ = r0, r1, sz, v1, v2, v3, v4, v6
	/* we need to make sure
	 * the stack remains aligned
	 * (rsp = 0) mod 16
	 */
	(*_Fn)(unsafe.Pointer(fn)).Fdynalloc = int8(1)
	v1 = *(*_Ref)(unsafe.Pointer(bp + 16))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RCon) {
		sz = *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*32 + 16))
		if sz < 0 || sz >= int64(libc.Int32FromInt32(m___INT_MAX__)-libc.Int32FromInt32(15)) {
			_err(tls, qbe, __ccgo_ts+194, libc.VaList(bp+32, sz))
		}
		sz = (sz + int64(15)) & int64(-int32(16))
		x_emit(tls, qbe, int32(_Osalloc), int32(_Kl), *(*_Ref)(unsafe.Pointer(bp + 12)), x_getcon(tls, qbe, sz, fn), _Ref{})
	} else {
		/* r0 = (r + 15) & -16 */
		r0 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
		r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Osalloc), int32(_Kl), *(*_Ref)(unsafe.Pointer(bp + 12)), r0, _Ref{})
		x_emit(tls, qbe, int32(_Oand), int32(_Kl), r0, r1, x_getcon(tls, qbe, int64(-int32(16)), fn))
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(bp + 16)), x_getcon(tls, qbe, int64(15), fn))
		if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*192))).Fslot != -int32(1) {
			_err(tls, qbe, __ccgo_ts+223, libc.VaList(bp+32, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*192, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))
		}
	}
}

func x_bsinit(tls *libc.TLS, qbe *_QBE, bs uintptr, n _uint) {
	n = (n + uint32(_NBit) - uint32(1)) / uint32(_NBit)
	(*_BSet)(unsafe.Pointer(bs)).Fnt = n
	(*_BSet)(unsafe.Pointer(bs)).Ft = x_alloc(tls, qbe, uint64(n)*uint64(8))
}

func s_popcnt(tls *libc.TLS, qbe *_QBE, b _bits) (r _uint) {
	b = b&uint64(0x5555555555555555) + b>>libc.Int32FromInt32(1)&uint64(0x5555555555555555)
	b = b&uint64(0x3333333333333333) + b>>libc.Int32FromInt32(2)&uint64(0x3333333333333333)
	b = b&uint64(0x0f0f0f0f0f0f0f0f) + b>>libc.Int32FromInt32(4)&uint64(0x0f0f0f0f0f0f0f0f)
	b += b >> libc.Int32FromInt32(8)
	b += b >> libc.Int32FromInt32(16)
	b += b >> libc.Int32FromInt32(32)
	return uint32(b & uint64(0xff))
}

func s_firstbit(tls *libc.TLS, qbe *_QBE, b _bits) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var n int32
	_ = n
	n = 0
	if !(b&libc.Uint64FromUint32(0xffffffff) != 0) {
		n += int32(32)
		b >>= uint64(32)
	}
	if !(b&libc.Uint64FromInt32(0xffff) != 0) {
		n += int32(16)
		b >>= uint64(16)
	}
	if !(b&libc.Uint64FromInt32(0xff) != 0) {
		n += int32(8)
		b >>= uint64(8)
	}
	if !(b&libc.Uint64FromInt32(0xf) != 0) {
		n += int32(4)
		b >>= uint64(4)
	}
	*(*[16]int8)(unsafe.Pointer(bp)) = [16]int8{
		0:  int8(4),
		2:  int8(1),
		4:  int8(2),
		6:  int8(1),
		8:  int8(3),
		10: int8(1),
		12: int8(2),
		14: int8(1),
	}
	n += int32(*(*int8)(unsafe.Pointer(bp + uintptr(b&uint64(0xf)))))
	return n
}

func x_bscount(tls *libc.TLS, qbe *_QBE, bs uintptr) (r _uint) {
	var i, n _uint
	_, _ = i, n
	n = uint32(0)
	i = uint32(0)
	for {
		if !(i < (*_BSet)(unsafe.Pointer(bs)).Fnt) {
			break
		}
		n += s_popcnt(tls, qbe, *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bs)).Ft + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	return n
}

func x_bsset(tls *libc.TLS, qbe *_QBE, bs uintptr, elt _uint) {
	*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bs)).Ft + uintptr(elt/uint32(_NBit))*8)) |= libc.Uint64FromInt32(1) << (elt % uint32(_NBit))
}

func x_bsclr(tls *libc.TLS, qbe *_QBE, bs uintptr, elt _uint) {
	*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bs)).Ft + uintptr(elt/uint32(_NBit))*8)) &= ^(libc.Uint64FromInt32(1) << (elt % uint32(_NBit)))
}

func x_bscopy(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) {
	var i _uint
	_ = i
	i = uint32(0)
	for {
		if !(i < (*_BSet)(unsafe.Pointer(a)).Fnt) {
			break
		}
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(a)).Ft + uintptr(i)*8)) = *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b)).Ft + uintptr(i)*8))
		goto _1
	_1:
		;
		i++
	}
}

func x_bsunion(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) {
	var i _uint
	_ = i
	i = uint32(0)
	for {
		if !(i < (*_BSet)(unsafe.Pointer(a)).Fnt) {
			break
		}
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(a)).Ft + uintptr(i)*8)) |= *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b)).Ft + uintptr(i)*8))
		goto _1
	_1:
		;
		i++
	}
}

func x_bsinter(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) {
	var i _uint
	_ = i
	i = uint32(0)
	for {
		if !(i < (*_BSet)(unsafe.Pointer(a)).Fnt) {
			break
		}
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(a)).Ft + uintptr(i)*8)) &= *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b)).Ft + uintptr(i)*8))
		goto _1
	_1:
		;
		i++
	}
}

func x_bsdiff(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) {
	var i _uint
	_ = i
	i = uint32(0)
	for {
		if !(i < (*_BSet)(unsafe.Pointer(a)).Fnt) {
			break
		}
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(a)).Ft + uintptr(i)*8)) &= ^*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b)).Ft + uintptr(i)*8))
		goto _1
	_1:
		;
		i++
	}
}

func x_bsequal(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) (r int32) {
	var i _uint
	_ = i
	i = uint32(0)
	for {
		if !(i < (*_BSet)(unsafe.Pointer(a)).Fnt) {
			break
		}
		if *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(a)).Ft + uintptr(i)*8)) != *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b)).Ft + uintptr(i)*8)) {
			return 0
		}
		goto _1
	_1:
		;
		i++
	}
	return int32(1)
}

func x_bszero(tls *libc.TLS, qbe *_QBE, bs uintptr) {
	libc.X__builtin___memset_chk(tls, (*_BSet)(unsafe.Pointer(bs)).Ft, 0, uint64((*_BSet)(unsafe.Pointer(bs)).Fnt)*uint64(8), ^___predefined_size_t(0))
}

// C documentation
//
//	/* iterates on a bitset, use as follows
//	 *
//	 * 	for (i=0; bsiter(set, &i); i++)
//	 * 		use(i);
//	 *
//	 */
func x_bsiter(tls *libc.TLS, qbe *_QBE, bs uintptr, elt uintptr) (r int32) {
	var b _bits
	var i, t _uint
	_, _, _ = b, i, t
	i = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(elt)))
	t = i / uint32(_NBit)
	if t >= (*_BSet)(unsafe.Pointer(bs)).Fnt {
		return 0
	}
	b = *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bs)).Ft + uintptr(t)*8))
	b &= ^(libc.Uint64FromInt32(1)<<(i%uint32(_NBit)) - libc.Uint64FromInt32(1))
	for !(b != 0) {
		t++
		if t >= (*_BSet)(unsafe.Pointer(bs)).Fnt {
			return 0
		}
		b = *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bs)).Ft + uintptr(t)*8))
	}
	*(*int32)(unsafe.Pointer(elt)) = libc.Int32FromUint32(uint32(_NBit)*t + libc.Uint32FromInt32(s_firstbit(tls, qbe, b)))
	return int32(1)
}

func x_dumpts(tls *libc.TLS, qbe *_QBE, bs uintptr, tmp uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var _ /* t at bp+0 */ int32
	fprintf(tls, qbe, f, __ccgo_ts+261, 0)
	*(*int32)(unsafe.Pointer(bp)) = int32(_Tmp0)
	for {
		if !(x_bsiter(tls, qbe, bs, bp) != 0) {
			break
		}
		fprintf(tls, qbe, f, __ccgo_ts+263, libc.VaList(bp+16, tmp+uintptr(*(*int32)(unsafe.Pointer(bp)))*192))
		goto _1
	_1:
		;
		*(*int32)(unsafe.Pointer(bp))++
	}
	fprintf(tls, qbe, f, __ccgo_ts+267, 0)
}

func x_runmatch(tls *libc.TLS, qbe *_QBE, code uintptr, tn uintptr, _ref _Ref, var1 uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	*(*_Ref)(unsafe.Pointer(bp)) = _ref
	var bc, i, n, nl, nr, v1 int32
	var pc, s, stk, v2, v3, v4, v5, v7, v8 uintptr
	var _ /* stkbuf at bp+4 */ [20]_Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = bc, i, n, nl, nr, pc, s, stk, v1, v2, v3, v4, v5, v7, v8
	stk = bp + 4
	pc = code
	for {
		v1 = libc.Int32FromUint8(*(*_uchar)(unsafe.Pointer(pc)))
		bc = v1
		if !(v1 != 0) {
			break
		}
		switch bc {
		case int32(1): /* pushsym */
			fallthrough
		case int32(2): /* push */
			nl = libc.Int32FromUint8((*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fnl)
			nr = libc.Int32FromUint8((*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fnr)
			if bc == int32(1) && nl > nr {
				v2 = stk
				stk += 4
				*(*_Ref)(unsafe.Pointer(v2)) = (*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fl
				*(*_Ref)(unsafe.Pointer(bp)) = (*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fr
			} else {
				v3 = stk
				stk += 4
				*(*_Ref)(unsafe.Pointer(v3)) = (*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fr
				*(*_Ref)(unsafe.Pointer(bp)) = (*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fl
			}
			pc++
		case int32(3): /* set */
			pc++
			v4 = pc
			*(*_Ref)(unsafe.Pointer(var1 + uintptr(*(*_uchar)(unsafe.Pointer(v4)))*4)) = *(*_Ref)(unsafe.Pointer(bp))
			if libc.Int32FromUint8(*(*_uchar)(unsafe.Pointer(pc + libc.UintptrFromInt32(1)))) == 0 {
				return
			}
			/* fall through */
			fallthrough
		case int32(4): /* pop */
			stk -= 4
			v5 = stk
			*(*_Ref)(unsafe.Pointer(bp)) = *(*_Ref)(unsafe.Pointer(v5))
			pc++
		case int32(5): /* switch */
			n = libc.Int32FromUint8((*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*12))).Fn)
			s = pc + uintptr(1)
			v7 = s
			s++
			i = libc.Int32FromUint8(*(*_uchar)(unsafe.Pointer(v7)))
			for {
				if !(i > 0) {
					break
				}
				v8 = s
				s++
				if n == libc.Int32FromUint8(*(*_uchar)(unsafe.Pointer(v8))) {
					break
				}
				goto _6
			_6:
				;
				i--
				s++
			}
			pc += uintptr(*(*_uchar)(unsafe.Pointer(s)))
		default: /* jump */
			pc = code + uintptr(bc-libc.Int32FromInt32(10))
			break
		}
	}
}

const _Ksb = 4

const /* matches Oarg/Opar/Jret */
_Kub = 5

const _Ksh = 6

const _Kuh = 7

const _Kc = 8

const _K0 = 9

const _Ke = -2

const /* erroneous mode */
_Km = 1

type _PState = int32

const _PLbl = 1

const _PPhi = 2

const _PIns = 3

const _PEnd = 4

const _Txxx = 0

const

/* aliases */
_Tloadw = 88

const _Tloadl = 89

const _Tloadd = 91

const _Talloc1 = 92

const _Talloc2 = 93

const _Tblit = 94

const _Tcall = 95

const _Tenv = 96

const _Tphi = 97

const _Tjmp = 98

const _Tjnz = 99

const _Tret = 100

const _Thlt = 101

const _Texport = 102

const _Tthread = 103

const _Tcommon = 104

const _Tfunc = 105

const _Ttype = 106

const _Tdata = 107

const _Tsection = 108

const _Talign = 109

const _Tdbgfile = 110

const _Tl = 111

const _Tw = 112

const _Tsh = 113

const _Tuh = 114

const _Th = 115

const _Tsb = 116

const _Tub = 117

const _Tb = 118

const _Td = 119

const _Ts = 120

const _Tz = 121

const _Tint = 122

const _Tflts = 123

const _Tfltd = 124

const _Ttmp = 125

const _Tlbl = 126

const _Tglo = 127

const _Ttyp = 128

const _Tstr = 129

const _Tplus = 130

const _Teq = 131

const _Tcomma = 132

const _Tlparen = 133

const _Trparen = 134

const _Tlbrace = 135

const _Trbrace = 136

const _Tnl = 137

const _Tdots = 138

const _Teof = 139

const _Ntok = 140

const _NPred = 63

const _TMask = 16383

const /* for temps hash */
_BMask = 8191

const /* for blocks hash */

_K = 11183273

const /* found using tools/lexh.c */
_M = 23

func x_err(tls *libc.TLS, qbe *_QBE, s uintptr, va uintptr) {
}

func s_lexinit(tls *libc.TLS, qbe *_QBE) {
	var h int64
	var i int32
	_, _ = h, i
	if qbe.s_done != 0 {
		return
	}
	i = 0
	for {
		if !(i < int32(_NPubOp)) {
			break
		}
		if qbe.x_optab[i].Fname != 0 {
			qbe.s_kwmap[i] = qbe.x_optab[i].Fname
		}
		goto _1
	_1:
		;
		i++
	}
	i = 0
	for {
		if !(i < int32(_Ntok)) {
			break
		}
		if qbe.s_kwmap[i] != 0 {
			h = libc.Int64FromUint32(x_hash(tls, qbe, qbe.s_kwmap[i]) * uint32(_K) >> int32(_M))
			qbe.s_lexh[h] = libc.Uint8FromInt32(i)
		}
		goto _2
	_2:
		;
		i++
	}
	qbe.s_done = int32(1)
}

func s_getint(tls *libc.TLS, qbe *_QBE) (r _int64_t) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var c, m int32
	var _ /* n at bp+0 */ _uint64_t
	_, _ = c, m
	*(*_uint64_t)(unsafe.Pointer(bp)) = uint64(0)
	c = fgetc(tls, qbe, qbe.s_inf)
	m = libc.BoolInt32(c == int32('-'))
	if m != 0 {
		c = fgetc(tls, qbe, qbe.s_inf)
	}
	for cond := true; cond; cond = int32('0') <= c && c <= int32('9') {
		*(*_uint64_t)(unsafe.Pointer(bp)) = uint64(10)**(*_uint64_t)(unsafe.Pointer(bp)) + libc.Uint64FromInt32(c-libc.Int32FromUint8('0'))
		c = fgetc(tls, qbe, qbe.s_inf)
	}
	ungetc(tls, qbe, c, qbe.s_inf)
	if m != 0 {
		*(*_uint64_t)(unsafe.Pointer(bp)) = uint64(1) + ^*(*_uint64_t)(unsafe.Pointer(bp))
	}
	return *(*_int64_t)(unsafe.Pointer(bp))
}

func s_lex(tls *libc.TLS, qbe *_QBE) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c, esc, i, t, v1, v28, v31, v32, v37, v40, v44, v46, v47, v49, v5, v53, v55, v56, v58, v63, v66, v7, v8 int32
	var v3, v34, v35, v42, v51, v60, v61 ___darwin_ct_rune_t
	var v4, v43, v52 uint64
	var v64 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, esc, i, t, v1, v28, v3, v31, v32, v34, v35, v37, v4, v40, v42, v43, v44, v46, v47, v49, v5, v51, v52, v53, v55, v56, v58, v60, v61, v63, v64, v66, v7, v8
	for {
		c = fgetc(tls, qbe, qbe.s_inf)
		goto _10
	_10:
		;
		v3 = c
		v4 = uint64(0x00020000)
		v8 = libc.BoolInt32(v3 & ^libc.Int32FromInt32(0x7F) == 0)
		goto _9
	_9:
		if v8 != 0 {
			v7 = libc.BoolInt32(!!(uint64(*(*___uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v3)*4)))&v4 != 0))
		} else {
			v7 = libc.BoolInt32(!!(libc.X__maskrune(tls, v3, v4) != 0))
		}
		v5 = v7
		goto _6
	_6:
		v1 = v5
		goto _2
	_2:
		;
		if !(v1 != 0) {
			break
		}
	}
	t = int32(_Txxx)
	qbe.s_tokval.Fchr = int8(c)
	switch c {
	case -int32(1):
		goto _11
	case int32(','):
		goto _12
	case int32('('):
		goto _13
	case int32(')'):
		goto _14
	case int32('{'):
		goto _15
	case int32('}'):
		goto _16
	case int32('='):
		goto _17
	case int32('+'):
		goto _18
	case int32('s'):
		goto _19
	case int32('d'):
		goto _20
	case int32('%'):
		goto _21
	case int32('@'):
		goto _22
	case int32('$'):
		goto _23
	case int32(':'):
		goto _24
	case int32('#'):
		goto _25
	case int32('\n'):
		goto _26
	}
	goto _27
_11:
	;
	return int32(_Teof)
_12:
	;
	return int32(_Tcomma)
_13:
	;
	return int32(_Tlparen)
_14:
	;
	return int32(_Trparen)
_15:
	;
	return int32(_Tlbrace)
_16:
	;
	return int32(_Trbrace)
_17:
	;
	return int32(_Teq)
_18:
	;
	return int32(_Tplus)
_19:
	;
	if fscanf(tls, qbe, qbe.s_inf, __ccgo_ts+1242, libc.VaList(bp+8, uintptr(unsafe.Pointer(&qbe.s_tokval))+16)) != int32(1) {
		goto _27
	}
	return int32(_Tflts)
_20:
	;
	if fscanf(tls, qbe, qbe.s_inf, __ccgo_ts+1246, libc.VaList(bp+8, uintptr(unsafe.Pointer(&qbe.s_tokval))+8)) != int32(1) {
		goto _27
	}
	return int32(_Tfltd)
_21:
	;
	t = int32(_Ttmp)
	c = fgetc(tls, qbe, qbe.s_inf)
	goto Alpha
_22:
	;
	t = int32(_Tlbl)
	c = fgetc(tls, qbe, qbe.s_inf)
	goto Alpha
_23:
	;
	t = int32(_Tglo)
	v28 = fgetc(tls, qbe, qbe.s_inf)
	c = v28
	if v28 == int32('"') {
		goto Quoted
	}
	goto Alpha
_24:
	;
	t = int32(_Ttyp)
	c = fgetc(tls, qbe, qbe.s_inf)
	goto Alpha
_25:
	;
_30:
	;
	v31 = fgetc(tls, qbe, qbe.s_inf)
	c = v31
	if !(v31 != int32('\n') && c != -int32(1)) {
		goto _29
	}
	goto _30
_29:
	;
	/* fall through */
_26:
	;
	qbe.s_lnum++
	return int32(_Tnl)
_27:
	;
	v34 = c
	if v34 < 0 || v34 >= libc.Int32FromInt32(1)<<libc.Int32FromInt32(8) {
		v37 = 0
	} else {
		v37 = libc.BoolInt32(!!(uint64(*(*___uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v34)*4)))&uint64(0x00000400) != 0))
	}
	v35 = v37
	goto _36
_36:
	v32 = v35
	goto _33
_33:
	;
	if v32 != 0 || c == int32('-') {
		ungetc(tls, qbe, c, qbe.s_inf)
		qbe.s_tokval.Fnum = s_getint(tls, qbe)
		return int32(_Tint)
	}
	if !(c == int32('"')) {
		goto _38
	}
	t = int32(_Tstr)
	goto Quoted
Quoted:
	;
	qbe.s_tokval.Fstr = x_vnew(tls, qbe, uint64(2), uint64(1), int32(_PFn))
	*(*int8)(unsafe.Pointer(qbe.s_tokval.Fstr)) = int8(c)
	esc = 0
	i = int32(1)
	for {
		c = fgetc(tls, qbe, qbe.s_inf)
		if c == -int32(1) {
			_err(tls, qbe, __ccgo_ts+1251, 0)
		}
		x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_tokval))+32, libc.Uint64FromInt32(i+int32(2)))
		*(*int8)(unsafe.Pointer(qbe.s_tokval.Fstr + uintptr(i))) = int8(c)
		if c == int32('"') && !(esc != 0) {
			*(*int8)(unsafe.Pointer(qbe.s_tokval.Fstr + uintptr(i+int32(1)))) = 0
			return t
		}
		esc = libc.BoolInt32(c == int32('\\') && !(esc != 0))
		goto _39
	_39:
		;
		i++
	}
_38:
	;
	goto Alpha
Alpha:
	;
	v42 = c
	v43 = uint64(0x00000100)
	v47 = libc.BoolInt32(v42 & ^libc.Int32FromInt32(0x7F) == 0)
	goto _48
_48:
	if v47 != 0 {
		v46 = libc.BoolInt32(!!(uint64(*(*___uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v42)*4)))&v43 != 0))
	} else {
		v46 = libc.BoolInt32(!!(libc.X__maskrune(tls, v42, v43) != 0))
	}
	v44 = v46
	goto _45
_45:
	v40 = v44
	goto _41
_41:
	;
	if !(v40 != 0) && c != int32('.') && c != int32('_') {
		_err(tls, qbe, __ccgo_ts+1271, libc.VaList(bp+8, c, c))
	}
	i = 0
	for {
		if i >= int32(_NString)-libc.Int32FromInt32(1) {
			_err(tls, qbe, __ccgo_ts+1297, 0)
		}
		v66 = i
		i++
		qbe.s_tok[v66] = int8(c)
		c = fgetc(tls, qbe, qbe.s_inf)
		goto _65
	_65:
		;
		v51 = c
		v52 = uint64(0x00000100)
		v56 = libc.BoolInt32(v51 & ^libc.Int32FromInt32(0x7F) == 0)
		goto _57
	_57:
		if v56 != 0 {
			v55 = libc.BoolInt32(!!(uint64(*(*___uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v51)*4)))&v52 != 0))
		} else {
			v55 = libc.BoolInt32(!!(libc.X__maskrune(tls, v51, v52) != 0))
		}
		v53 = v55
		goto _54
	_54:
		v49 = v53
		goto _50
	_50:
		;
		if v64 = v49 != 0 || c == int32('$') || c == int32('.') || c == int32('_'); !v64 {
			v60 = c
			if v60 < 0 || v60 >= libc.Int32FromInt32(1)<<libc.Int32FromInt32(8) {
				v63 = 0
			} else {
				v63 = libc.BoolInt32(!!(uint64(*(*___uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v60)*4)))&uint64(0x00000400) != 0))
			}
			v61 = v63
			goto _62
		_62:
			v58 = v61
			goto _59
		_59:
		}
		if !(v64 || v58 != 0) {
			break
		}
	}
	qbe.s_tok[i] = 0
	ungetc(tls, qbe, c, qbe.s_inf)
	qbe.s_tokval.Fstr = uintptr(unsafe.Pointer(&qbe.s_tok))
	if t != int32(_Txxx) {
		return t
	}
	t = libc.Int32FromUint8(qbe.s_lexh[x_hash(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_tok)))*uint32(_K)>>int32(_M)])
	if t == int32(_Txxx) || libc.Xstrcmp(tls, qbe.s_kwmap[t], uintptr(unsafe.Pointer(&qbe.s_tok))) != 0 {
		_err(tls, qbe, __ccgo_ts+1317, libc.VaList(bp+8, uintptr(unsafe.Pointer(&qbe.s_tok))))
		return int32(_Txxx)
	}
	return t
}

func s_peek(tls *libc.TLS, qbe *_QBE) (r int32) {
	if qbe.s_thead == int32(_Txxx) {
		qbe.s_thead = s_lex(tls, qbe)
	}
	return qbe.s_thead
}

func s_next(tls *libc.TLS, qbe *_QBE) (r int32) {
	var t int32
	_ = t
	t = s_peek(tls, qbe)
	qbe.s_thead = int32(_Txxx)
	return t
}

func s_nextnl(tls *libc.TLS, qbe *_QBE) (r int32) {
	var t, v1 int32
	_, _ = t, v1
	for {
		v1 = s_next(tls, qbe)
		t = v1
		if !(v1 == int32(_Tnl)) {
			break
		}
	}
	return t
}

func s_expect(tls *libc.TLS, qbe *_QBE, t int32) {
	bp := tls.Alloc(160)
	defer tls.Free(160)
	var s1, s2, v1, v2 uintptr
	var t1 int32
	var _ /* buf at bp+0 */ [128]int8
	_, _, _, _, _ = s1, s2, t1, v1, v2
	t1 = s_next(tls, qbe)
	if t == t1 {
		return
	}
	if qbe.s_ttoa[t] != 0 {
		v1 = qbe.s_ttoa[t]
	} else {
		v1 = __ccgo_ts + 1362
	}
	s1 = v1
	if qbe.s_ttoa[t1] != 0 {
		v2 = qbe.s_ttoa[t1]
	} else {
		v2 = __ccgo_ts + 1362
	}
	s2 = v2
	libc.X__builtin___sprintf_chk(tls, bp, 0, ^___predefined_size_t(0), __ccgo_ts+1365, libc.VaList(bp+136, s1, s2))
	_err(tls, qbe, bp, 0)
}

func s_tmpref(tls *libc.TLS, qbe *_QBE, v uintptr) (r _Ref) {
	var i, t, v1 int32
	_, _, _ = i, t, v1
	if qbe.s_tmphcap/int32(2) <= (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fntmp-int32(_Tmp0) {
		free(tls, qbe, qbe.s_tmph)
		if qbe.s_tmphcap != 0 {
			v1 = qbe.s_tmphcap * int32(2)
		} else {
			v1 = int32(_TMask) + libc.Int32FromInt32(1)
		}
		qbe.s_tmphcap = v1
		qbe.s_tmph = x_emalloc(tls, qbe, libc.Uint64FromInt32(qbe.s_tmphcap)*uint64(4))
		t = int32(_Tmp0)
		for {
			if !(t < (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fntmp) {
				break
			}
			i = libc.Int32FromUint32(x_hash(tls, qbe, (*_Fn)(unsafe.Pointer(qbe.s_curf)).Ftmp+uintptr(t)*192) & libc.Uint32FromInt32(qbe.s_tmphcap-libc.Int32FromInt32(1)))
			for {
				if !(*(*int32)(unsafe.Pointer(qbe.s_tmph + uintptr(i)*4)) != 0) {
					break
				}
				goto _3
			_3:
				;
				i = (i + int32(1)) & (qbe.s_tmphcap - int32(1))
			}
			*(*int32)(unsafe.Pointer(qbe.s_tmph + uintptr(i)*4)) = t
			goto _2
		_2:
			;
			t++
		}
	}
	i = libc.Int32FromUint32(x_hash(tls, qbe, v) & libc.Uint32FromInt32(qbe.s_tmphcap-libc.Int32FromInt32(1)))
	for {
		if !(*(*int32)(unsafe.Pointer(qbe.s_tmph + uintptr(i)*4)) != 0) {
			break
		}
		t = *(*int32)(unsafe.Pointer(qbe.s_tmph + uintptr(i)*4))
		if libc.Xstrcmp(tls, (*_Fn)(unsafe.Pointer(qbe.s_curf)).Ftmp+uintptr(t)*192, v) == 0 {
			return _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
			}
		}
		goto _4
	_4:
		;
		i = (i + int32(1)) & (qbe.s_tmphcap - int32(1))
	}
	t = (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fntmp
	*(*int32)(unsafe.Pointer(qbe.s_tmph + uintptr(i)*4)) = t
	x_newtmp(tls, qbe, uintptr(0), int32(_Kx), qbe.s_curf)
	libc.X__builtin___strcpy_chk(tls, (*_Fn)(unsafe.Pointer(qbe.s_curf)).Ftmp+uintptr(t)*192, v, ^___predefined_size_t(0))
	return _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
	}
}

func s_parseref(tls *libc.TLS, qbe *_QBE) (r _Ref) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var _ /* c at bp+0 */ _Con
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(32), ^___predefined_size_t(0))
	switch s_next(tls, qbe) {
	default:
		return _Ref{}
	case int32(_Ttmp):
		return s_tmpref(tls, qbe, qbe.s_tokval.Fstr)
	case int32(_Tint):
		(*(*_Con)(unsafe.Pointer(bp))).Ftype1 = int32(_CBits)
		*(*_int64_t)(unsafe.Pointer(bp + 16)) = qbe.s_tokval.Fnum
	case int32(_Tflts):
		(*(*_Con)(unsafe.Pointer(bp))).Ftype1 = int32(_CBits)
		*(*float32)(unsafe.Pointer(bp + 16)) = qbe.s_tokval.Fflts
		(*(*_Con)(unsafe.Pointer(bp))).Fflt = int8(1)
	case int32(_Tfltd):
		(*(*_Con)(unsafe.Pointer(bp))).Ftype1 = int32(_CBits)
		*(*float64)(unsafe.Pointer(bp + 16)) = qbe.s_tokval.Ffltd
		(*(*_Con)(unsafe.Pointer(bp))).Fflt = int8(2)
	case int32(_Tthread):
		(*(*_Con)(unsafe.Pointer(bp))).Fsym.Ftype1 = int32(_SThr)
		s_expect(tls, qbe, int32(_Tglo))
		/* fall through */
		fallthrough
	case int32(_Tglo):
		(*(*_Con)(unsafe.Pointer(bp))).Ftype1 = int32(_CAddr)
		(*(*_Con)(unsafe.Pointer(bp))).Fsym.Fid = x_intern(tls, qbe, qbe.s_tokval.Fstr)
		break
	}
	return x_newcon(tls, qbe, bp, qbe.s_curf)
}

func s_findtyp(tls *libc.TLS, qbe *_QBE, i int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var v1 int32
	_ = v1
	for {
		i--
		v1 = i
		if !(v1 >= 0) {
			break
		}
		if libc.Xstrcmp(tls, qbe.s_tokval.Fstr, qbe.x_typ+uintptr(i)*112) == 0 {
			return i
		}
	}
	_err(tls, qbe, __ccgo_ts+1393, libc.VaList(bp+8, qbe.s_tokval.Fstr))
	return r
}

func s_parsecls(tls *libc.TLS, qbe *_QBE, tyn uintptr) (r int32) {
	switch s_next(tls, qbe) {
	default:
		_err(tls, qbe, __ccgo_ts+1412, 0)
		fallthrough
	case int32(_Ttyp):
		*(*int32)(unsafe.Pointer(tyn)) = s_findtyp(tls, qbe, libc.Int32FromUint32(qbe.s_ntyp))
		return int32(_Kc)
	case int32(_Tsb):
		return int32(_Ksb)
	case int32(_Tub):
		return int32(_Kub)
	case int32(_Tsh):
		return int32(_Ksh)
	case int32(_Tuh):
		return int32(_Kuh)
	case int32(_Tw):
		return int32(_Kw)
	case int32(_Tl):
		return int32(_Kl)
	case int32(_Ts):
		return int32(_Ks)
	case int32(_Td):
		return int32(_Kd)
	}
	return r
}

func s_parserefl(tls *libc.TLS, qbe *_QBE, arg int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var env, hasenv, k, vararg, v10, v3, v8 int32
	var r1, v1, v2, v5, v6, v7 _Ref
	var v12 bool
	var _ /* ty at bp+12 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = env, hasenv, k, r1, vararg, v1, v10, v12, v2, v3, v5, v6, v7, v8
	hasenv = 0
	vararg = 0
	s_expect(tls, qbe, int32(_Tlparen))
	for s_peek(tls, qbe) != int32(_Trparen) {
		if (int64(qbe.x_curi)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))))/16 >= int64(_NIns) {
			_err(tls, qbe, __ccgo_ts+1436, 0)
		}
		if !(arg != 0) && vararg != 0 {
			_err(tls, qbe, __ccgo_ts+1458, 0)
		}
		switch s_peek(tls, qbe) {
		case int32(_Tdots):
			if vararg != 0 {
				_err(tls, qbe, __ccgo_ts+1492, 0)
			}
			vararg = int32(1)
			if arg != 0 {
				*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
					F__ccgo0: uint32(_Oargv) & 0x3fffffff << 0,
				}
				qbe.x_curi += 16
			}
			s_next(tls, qbe)
			goto Next
		case int32(_Tenv):
			if hasenv != 0 {
				_err(tls, qbe, __ccgo_ts+1515, 0)
			}
			hasenv = int32(1)
			env = int32(1)
			s_next(tls, qbe)
			k = int32(_Kl)
		default:
			env = 0
			k = s_parsecls(tls, qbe, bp+12)
			break
		}
		r1 = s_parseref(tls, qbe)
		v1 = r1
		v2 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v1
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
		v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _4
	_4:
		if v3 != 0 {
			_err(tls, qbe, __ccgo_ts+1544, 0)
		}
		if v12 = !(arg != 0); v12 {
			v5 = r1
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v5
			v6 = v5
			v7 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v6
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v7
			v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _9
		_9:
			if v8 != 0 {
				v10 = -int32(1)
				goto _11
			}
			v10 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _11
		_11:
		}
		if v12 && v10 != int32(_RTmp) {
			_err(tls, qbe, __ccgo_ts+1561, 0)
		}
		if env != 0 {
			if arg != 0 {
				*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
					F__ccgo0: uint32(_Oarge)&0x3fffffff<<0 | libc.Uint32FromInt32(k)&0x3<<30,
					Fto:      _Ref{},
					Farg: [2]_Ref{
						0: r1,
					},
				}
			} else {
				*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
					F__ccgo0: uint32(_Opare)&0x3fffffff<<0 | libc.Uint32FromInt32(k)&0x3<<30,
					Fto:      r1,
					Farg: [2]_Ref{
						0: {},
					},
				}
			}
		} else {
			if k == int32(_Kc) {
				if arg != 0 {
					*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
						F__ccgo0: uint32(_Oargc)&0x3fffffff<<0 | uint32(_Kl)&0x3<<30,
						Fto:      _Ref{},
						Farg: [2]_Ref{
							0: {
								F__ccgo0: uint32(_RType)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 12)))&0x1fffffff<<3,
							},
							1: r1,
						},
					}
				} else {
					*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
						F__ccgo0: uint32(_Oparc)&0x3fffffff<<0 | uint32(_Kl)&0x3<<30,
						Fto:      r1,
						Farg: [2]_Ref{
							0: {
								F__ccgo0: uint32(_RType)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 12)))&0x1fffffff<<3,
							},
						},
					}
				}
			} else {
				if k >= int32(_Ksb) {
					if arg != 0 {
						*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
							F__ccgo0: libc.Uint32FromInt32(int32(_Oargsb)+(k-int32(_Ksb)))&0x3fffffff<<0 | uint32(_Kw)&0x3<<30,
							Fto:      _Ref{},
							Farg: [2]_Ref{
								0: r1,
							},
						}
					} else {
						*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
							F__ccgo0: libc.Uint32FromInt32(int32(_Oparsb)+(k-int32(_Ksb)))&0x3fffffff<<0 | uint32(_Kw)&0x3<<30,
							Fto:      r1,
							Farg: [2]_Ref{
								0: {},
							},
						}
					}
				} else {
					if arg != 0 {
						*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
							F__ccgo0: uint32(_Oarg)&0x3fffffff<<0 | libc.Uint32FromInt32(k)&0x3<<30,
							Fto:      _Ref{},
							Farg: [2]_Ref{
								0: r1,
							},
						}
					} else {
						*(*_Ins)(unsafe.Pointer(qbe.x_curi)) = _Ins{
							F__ccgo0: uint32(_Opar)&0x3fffffff<<0 | libc.Uint32FromInt32(k)&0x3<<30,
							Fto:      r1,
							Farg: [2]_Ref{
								0: {},
							},
						}
					}
				}
			}
		}
		qbe.x_curi += 16
		goto Next
	Next:
		;
		if s_peek(tls, qbe) == int32(_Trparen) {
			break
		}
		s_expect(tls, qbe, int32(_Tcomma))
	}
	s_expect(tls, qbe, int32(_Trparen))
	return vararg
}

func s_findblk(tls *libc.TLS, qbe *_QBE, name uintptr) (r uintptr) {
	var b uintptr
	var h _uint32_t
	var v2 int32
	_, _, _ = b, h, v2
	h = x_hash(tls, qbe, name) & uint32(_BMask)
	b = qbe.s_blkh[h]
	for {
		if !(b != 0) {
			break
		}
		if libc.Xstrcmp(tls, b+180, name) == 0 {
			return b
		}
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Fdlink
	}
	b = x_newblk(tls, qbe)
	v2 = qbe.s_nblk
	qbe.s_nblk++
	(*_Blk)(unsafe.Pointer(b)).Fid = libc.Uint32FromInt32(v2)
	libc.X__builtin___strcpy_chk(tls, b+180, name, ^___predefined_size_t(0))
	(*_Blk)(unsafe.Pointer(b)).Fdlink = qbe.s_blkh[h]
	qbe.s_blkh[h] = b
	return b
}

func s_closeblk(tls *libc.TLS, qbe *_QBE) {
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fnins = libc.Uint32FromInt64((int64(qbe.x_curi) - ___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb)))) / 16)
	x_idup(tls, qbe, qbe.s_curb+8, uintptr(unsafe.Pointer(&qbe.x_insb)), uint64((*_Blk)(unsafe.Pointer(qbe.s_curb)).Fnins))
	qbe.s_blink = qbe.s_curb + 48
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb))
}

func s_parseline(tls *libc.TLS, qbe *_QBE, ps _PState) (r _PState) {
	bp := tls.Alloc(800)
	defer tls.Free(800)
	var b1, c, phi uintptr
	var i, k, op, t, v17, v21, v26, v35, v37, v39, v42 int32
	var r2, v15, v16, v19, v20, v24, v25, v32, v33, v34 _Ref
	var v41, v44 bool
	var _ /* arg at bp+16 */ [63]_Ref
	var _ /* blk at bp+272 */ [63]uintptr
	var _ /* ty at bp+776 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, c, i, k, op, phi, r2, t, v15, v16, v17, v19, v20, v21, v24, v25, v26, v32, v33, v34, v35, v37, v39, v41, v42, v44
	*(*[63]_Ref)(unsafe.Pointer(bp + 16)) = [63]_Ref{
		0: {},
	}
	t = s_nextnl(tls, qbe)
	if ps == int32(_PLbl) && t != int32(_Tlbl) && t != int32(_Trbrace) {
		_err(tls, qbe, __ccgo_ts+1588, 0)
	}
	switch t {
	case int32(_Ttmp):
		goto _1
	case int32(_Ovastart):
		goto _2
	case int32(_Tcall):
		goto _3
	case int32(_Tblit):
		goto _4
	default:
		goto _5
	case int32(_Trbrace):
		goto _6
	case int32(_Tlbl):
		goto _7
	case int32(_Tret):
		goto _8
	case int32(_Tjmp):
		goto _9
	case int32(_Tjnz):
		goto _10
	case int32(_Thlt):
		goto _11
	case int32(_Odbgloc):
		goto _12
	}
	goto _13
_1:
	;
	r2 = s_tmpref(tls, qbe, qbe.s_tokval.Fstr)
	s_expect(tls, qbe, int32(_Teq))
	k = s_parsecls(tls, qbe, bp+776)
	op = s_next(tls, qbe)
	goto _13
_5:
	;
	if !(libc.Uint32FromInt32(t)-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb))) {
		goto _14
	}
_4:
	;
_3:
	;
_2:
	;
	/* operations without result */
	r2 = _Ref{}
	k = int32(_Kw)
	op = t
	goto _13
_14:
	;
	_err(tls, qbe, __ccgo_ts+1608, 0)
_6:
	;
	return int32(_PEnd)
_7:
	;
	b1 = s_findblk(tls, qbe, qbe.s_tokval.Fstr)
	if qbe.s_curb != 0 && int32((*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1) == int32(_Jxxx) {
		s_closeblk(tls, qbe)
		(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1 = int16(_Jjmp)
		(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fs1 = b1
	}
	if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) != int32(_Jxxx) {
		_err(tls, qbe, __ccgo_ts+1644, libc.VaList(bp+792, b1+180))
	}
	*(*uintptr)(unsafe.Pointer(qbe.s_blink)) = b1
	qbe.s_curb = b1
	qbe.s_plink = qbe.s_curb
	s_expect(tls, qbe, int32(_Tnl))
	return int32(_PPhi)
_8:
	;
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1 = int16(int32(_Jretw) + qbe.s_rcls)
	if s_peek(tls, qbe) == int32(_Tnl) {
		(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1 = int16(_Jret0)
	} else {
		if qbe.s_rcls != int32(_K0) {
			r2 = s_parseref(tls, qbe)
			v15 = r2
			v16 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v15
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v16
			v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _18
		_18:
			if v17 != 0 {
				_err(tls, qbe, __ccgo_ts+1678, 0)
			}
			(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Farg = r2
		}
	}
	goto Close
_9:
	;
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1 = int16(_Jjmp)
	goto Jump
_10:
	;
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1 = int16(_Jjnz)
	r2 = s_parseref(tls, qbe)
	v19 = r2
	v20 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v19
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v20
	v21 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _22
_22:
	if v21 != 0 {
		_err(tls, qbe, __ccgo_ts+1699, 0)
	}
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Farg = r2
	s_expect(tls, qbe, int32(_Tcomma))
	goto Jump
Jump:
	;
	s_expect(tls, qbe, int32(_Tlbl))
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fs1 = s_findblk(tls, qbe, qbe.s_tokval.Fstr)
	if int32((*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1) != int32(_Jjmp) {
		s_expect(tls, qbe, int32(_Tcomma))
		s_expect(tls, qbe, int32(_Tlbl))
		(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fs2 = s_findblk(tls, qbe, qbe.s_tokval.Fstr)
	}
	if (*_Blk)(unsafe.Pointer(qbe.s_curb)).Fs1 == (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fstart || (*_Blk)(unsafe.Pointer(qbe.s_curb)).Fs2 == (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fstart {
		_err(tls, qbe, __ccgo_ts+1729, 0)
	}
	goto Close
_11:
	;
	(*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1 = int16(_Jhlt)
	goto Close
Close:
	;
	s_expect(tls, qbe, int32(_Tnl))
	s_closeblk(tls, qbe)
	return int32(_PLbl)
_12:
	;
	op = t
	k = int32(_Kw)
	r2 = _Ref{}
	s_expect(tls, qbe, int32(_Tint))
	(*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[0] = _Ref{
		F__ccgo0: uint32(_RInt)&0x7<<0 | libc.Uint32FromInt64(qbe.s_tokval.Fnum&libc.Int64FromInt32(0x1fffffff))&0x1fffffff<<3,
	}
	if int64(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3)) != qbe.s_tokval.Fnum {
		_err(tls, qbe, __ccgo_ts+1761, 0)
	}
	if s_peek(tls, qbe) == int32(_Tcomma) {
		s_next(tls, qbe)
		s_expect(tls, qbe, int32(_Tint))
		(*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[int32(1)] = _Ref{
			F__ccgo0: uint32(_RInt)&0x7<<0 | libc.Uint32FromInt64(qbe.s_tokval.Fnum&libc.Int64FromInt32(0x1fffffff))&0x1fffffff<<3,
		}
		if int64(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 1*4 + 0))&0xfffffff8>>3)) != qbe.s_tokval.Fnum {
			_err(tls, qbe, __ccgo_ts+1781, 0)
		}
	} else {
		(*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[int32(1)] = _Ref{
			F__ccgo0: uint32(_RInt)&0x7<<0 | libc.Uint32FromInt32(libc.Int32FromInt32(0)&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
		}
	}
	goto Ins
_13:
	;
	if op == int32(_Tcall) {
		(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fleaf = 0
		(*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[0] = s_parseref(tls, qbe)
		s_parserefl(tls, qbe, int32(1))
		op = int32(_Ocall)
		s_expect(tls, qbe, int32(_Tnl))
		if k == int32(_Kc) {
			k = int32(_Kl)
			(*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[int32(1)] = _Ref{
				F__ccgo0: uint32(_RType)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 776)))&0x1fffffff<<3,
			}
		}
		if k >= int32(_Ksb) {
			k = int32(_Kw)
		}
		goto Ins
	}
	if op == int32(_Tloadw) {
		op = int32(_Oloadsw)
	}
	if op >= int32(_Tloadl) && op <= int32(_Tloadd) {
		op = int32(_Oload)
	}
	if op == int32(_Talloc1) || op == int32(_Talloc2) {
		op = int32(_Oalloc)
	}
	if op == int32(_Ovastart) && !((*_Fn)(unsafe.Pointer(qbe.s_curf)).Fvararg != 0) {
		_err(tls, qbe, __ccgo_ts+1803, 0)
	}
	if k >= int32(_Ksb) {
		_err(tls, qbe, __ccgo_ts+1847, 0)
	}
	i = 0
	if s_peek(tls, qbe) != int32(_Tnl) {
		for {
			if i == int32(_NPred) {
				_err(tls, qbe, __ccgo_ts+1880, 0)
			}
			if op == int32(_Tphi) {
				s_expect(tls, qbe, int32(_Tlbl))
				(*(*[63]uintptr)(unsafe.Pointer(bp + 272)))[i] = s_findblk(tls, qbe, qbe.s_tokval.Fstr)
			}
			(*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[i] = s_parseref(tls, qbe)
			v24 = (*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[i]
			v25 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v24
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v25
			v26 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _27
		_27:
			if v26 != 0 {
				_err(tls, qbe, __ccgo_ts+1899, 0)
			}
			i++
			t = s_peek(tls, qbe)
			if t == int32(_Tnl) {
				break
			}
			if t != int32(_Tcomma) {
				_err(tls, qbe, __ccgo_ts+1928, 0)
			}
			s_next(tls, qbe)
			goto _23
		_23:
		}
	}
	s_next(tls, qbe)
	switch op {
	case int32(_Tphi):
		goto _28
	case int32(_Tblit):
		goto _29
	default:
		goto _30
	}
	goto _31
_28:
	;
	if ps != int32(_PPhi) || qbe.s_curb == (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fstart {
		_err(tls, qbe, __ccgo_ts+1954, 0)
	}
	phi = x_alloc(tls, qbe, uint64(40))
	(*_Phi)(unsafe.Pointer(phi)).Fto = r2
	(*_Phi)(unsafe.Pointer(phi)).Fcls = k
	(*_Phi)(unsafe.Pointer(phi)).Farg = x_vnew(tls, qbe, libc.Uint64FromInt32(i), uint64(4), int32(_PFn))
	libc.X__builtin___memcpy_chk(tls, (*_Phi)(unsafe.Pointer(phi)).Farg, bp+16, libc.Uint64FromInt32(i)*uint64(4), ^___predefined_size_t(0))
	(*_Phi)(unsafe.Pointer(phi)).Fblk = x_vnew(tls, qbe, libc.Uint64FromInt32(i), uint64(8), int32(_PFn))
	libc.X__builtin___memcpy_chk(tls, (*_Phi)(unsafe.Pointer(phi)).Fblk, bp+272, libc.Uint64FromInt32(i)*uint64(8), ^___predefined_size_t(0))
	(*_Phi)(unsafe.Pointer(phi)).Fnarg = libc.Uint32FromInt32(i)
	*(*uintptr)(unsafe.Pointer(qbe.s_plink)) = phi
	qbe.s_plink = phi + 32
	return int32(_PPhi)
_29:
	;
	if (int64(qbe.x_curi)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))))/16 >= int64(int32(_NIns)-libc.Int32FromInt32(1)) {
		_err(tls, qbe, __ccgo_ts+1436, 0)
	}
	libc.X__builtin___memset_chk(tls, qbe.x_curi, 0, libc.Uint64FromInt32(2)*libc.Uint64FromInt64(16), ^___predefined_size_t(0))
	libc.SetBitFieldPtr32Uint32(qbe.x_curi+0, uint32(_Oblit0), 0, 0x3fffffff)
	*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8)) = (*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[0]
	*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8 + 1*4)) = (*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[int32(1)]
	qbe.x_curi += 16
	v32 = (*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[int32(2)]
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v32
	v33 = v32
	v34 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v33
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v34
	v35 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _36
_36:
	if v35 != 0 {
		v37 = -int32(1)
		goto _38
	}
	v37 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _38
_38:
	if v37 != int32(_RCon) {
		_err(tls, qbe, __ccgo_ts+1981, 0)
	}
	c = (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 2*4 + 0))&0xfffffff8>>3))*32
	r2 = _Ref{
		F__ccgo0: uint32(_RInt)&0x7<<0 | libc.Uint32FromInt64(*(*_int64_t)(unsafe.Pointer(c + 16))&libc.Int64FromInt32(0x1fffffff))&0x1fffffff<<3,
	}
	if v41 = (*_Con)(unsafe.Pointer(c)).Ftype1 != int32(_CBits); !v41 {
		*(*_Ref)(unsafe.Pointer(bp + 12)) = r2
		v39 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
		goto _40
	_40:
	}
	if v44 = v41 || v39 < 0; !v44 {
		*(*_Ref)(unsafe.Pointer(bp + 12)) = r2
		v42 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
		goto _43
	_43:
	}
	if v44 || int64(v42) != *(*_int64_t)(unsafe.Pointer(c + 16)) {
		_err(tls, qbe, __ccgo_ts+2008, 0)
	}
	libc.SetBitFieldPtr32Uint32(qbe.x_curi+0, uint32(_Oblit1), 0, 0x3fffffff)
	*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8)) = r2
	qbe.x_curi += 16
	return int32(_PIns)
_30:
	;
	if op >= int32(_NPubOp) {
		_err(tls, qbe, __ccgo_ts+2026, 0)
	}
	goto Ins
Ins:
	;
	if (int64(qbe.x_curi)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))))/16 >= int64(_NIns) {
		_err(tls, qbe, __ccgo_ts+1436, 0)
	}
	libc.SetBitFieldPtr32Uint32(qbe.x_curi+0, libc.Uint32FromInt32(op), 0, 0x3fffffff)
	libc.SetBitFieldPtr32Uint32(qbe.x_curi+0, libc.Uint32FromInt32(k), 30, 0xc0000000)
	(*_Ins)(unsafe.Pointer(qbe.x_curi)).Fto = r2
	*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8)) = (*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[0]
	*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8 + 1*4)) = (*(*[63]_Ref)(unsafe.Pointer(bp + 16)))[int32(1)]
	qbe.x_curi += 16
	return int32(_PIns)
_31:
	;
	return r
}

func s_usecheck(tls *libc.TLS, qbe *_QBE, _r _Ref, k int32, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	;
	return libc.BoolInt32(v6 != int32(_RTmp) || int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))).Fcls) == k || int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))).Fcls) == int32(_Kl) && k == int32(_Kw))
}

func s_typecheck(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var b1, i, p, t, v36, v44, v45 uintptr
	var k, v18, v25, v27, v32, v34, v40, v42, v7, v9 int32
	var n, v17 _uint
	var v22, v23, v24, v29, v30, v31, v37, v38, v39, v4, v5, v6 _Ref
	var _ /* pb at bp+16 */ [1]_BSet
	var _ /* ppb at bp+32 */ [1]_BSet
	var _ /* r at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, i, k, n, p, t, v17, v18, v22, v23, v24, v25, v27, v29, v30, v31, v32, v34, v36, v37, v38, v39, v4, v40, v42, v44, v45, v5, v6, v7, v9
	x_fillpreds(tls, qbe, fn)
	x_bsinit(tls, qbe, bp+16, (*_Fn)(unsafe.Pointer(fn)).Fnblk)
	x_bsinit(tls, qbe, bp+32, (*_Fn)(unsafe.Pointer(fn)).Fnblk)
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*192))).Fcls = int16((*_Phi)(unsafe.Pointer(p)).Fcls)
			goto _2
		_2:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			v4 = (*_Ins)(unsafe.Pointer(i)).Fto
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v4
			v5 = v4
			v6 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v5
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
			v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _8
		_8:
			if v7 != 0 {
				v9 = -int32(1)
				goto _10
			}
			v9 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _10
		_10:
			if v9 == int32(_RTmp) {
				t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192
				if x_clsmerge(tls, qbe, t+116, int16(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30))) != 0 {
					_err(tls, qbe, __ccgo_ts+2046, libc.VaList(bp+56, t))
				}
			}
			goto _3
		_3:
			;
			i += 16
		}
		goto _1
	_1:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
_13:
	;
	if !(b1 != 0) {
		goto _11
	}
	x_bszero(tls, qbe, bp+16)
	n = uint32(0)
	for {
		if !(n < (*_Blk)(unsafe.Pointer(b1)).Fnpred) {
			break
		}
		x_bsset(tls, qbe, bp+16, (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b1)).Fpred + uintptr(n)*8)))).Fid)
		goto _14
	_14:
		;
		n++
	}
	p = (*_Blk)(unsafe.Pointer(b1)).Fphi
	for {
		if !(p != 0) {
			break
		}
		x_bszero(tls, qbe, bp+32)
		t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*192
		n = uint32(0)
		for {
			if !(n < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
				break
			}
			k = int32((*_Tmp)(unsafe.Pointer(t)).Fcls)
			v17 = (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8)))).Fid
			v18 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp+32)).Ft + uintptr(v17/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v17%uint32(_NBit))) != uint64(0))
			goto _19
		_19:
			if v18 != 0 {
				_err(tls, qbe, __ccgo_ts+2093, libc.VaList(bp+56, *(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8))+180, t))
			}
			if !(s_usecheck(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(n)*4)), k, fn) != 0) {
				_err(tls, qbe, __ccgo_ts+2130, libc.VaList(bp+56, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(n)*4 + 0))&0xfffffff8>>3))*192, t))
			}
			x_bsset(tls, qbe, bp+32, (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8)))).Fid)
			goto _16
		_16:
			;
			n++
		}
		if !(x_bsequal(tls, qbe, bp+16, bp+32) != 0) {
			_err(tls, qbe, __ccgo_ts+2172, libc.VaList(bp+56, t))
		}
		goto _15
	_15:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
	i = (*_Blk)(unsafe.Pointer(b1)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
			break
		}
		n = uint32(0)
		for {
			if !(n < uint32(2)) {
				break
			}
			k = int32(*(*int16)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_optab)) + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))*32 + 8 + uintptr(n)*8 + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30))*2)))
			*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(n)*4))
			t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192
			if k == int32(_Ke) {
				_err(tls, qbe, __ccgo_ts+2209, libc.VaList(bp+56, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname))
			}
			v22 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v22
			v23 = v22
			v24 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v23
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v24
			v25 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _26
		_26:
			if v25 != 0 {
				v27 = -int32(1)
				goto _28
			}
			v27 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _28
		_28:
			if v27 == int32(_RType) {
				goto _21
			}
			v29 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v29
			v30 = v29
			v31 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v30
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v31
			v32 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _33
		_33:
			if v32 != 0 {
				v34 = -int32(1)
				goto _35
			}
			v34 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _35
		_35:
			;
			if v34 != -int32(1) && k == int32(_Kx) {
				if n == uint32(1) {
					v36 = __ccgo_ts + 2240
				} else {
					v36 = __ccgo_ts + 2247
				}
				_err(tls, qbe, __ccgo_ts+2253, libc.VaList(bp+56, v36, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname))
			}
			v37 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v37
			v38 = v37
			v39 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v38
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v39
			v40 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _41
		_41:
			if v40 != 0 {
				v42 = -int32(1)
				goto _43
			}
			v42 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _43
		_43:
			;
			if v42 == -int32(1) && k != int32(_Kx) {
				if n == uint32(1) {
					v44 = __ccgo_ts + 2240
				} else {
					v44 = __ccgo_ts + 2247
				}
				_err(tls, qbe, __ccgo_ts+2282, libc.VaList(bp+56, v44, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname))
			}
			if !(s_usecheck(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), k, fn) != 0) {
				if n == uint32(1) {
					v45 = __ccgo_ts + 2240
				} else {
					v45 = __ccgo_ts + 2247
				}
				_err(tls, qbe, __ccgo_ts+2307, libc.VaList(bp+56, v45, t, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname))
			}
			goto _21
		_21:
			;
			n++
		}
		goto _20
	_20:
		;
		i += 16
	}
	*(*_Ref)(unsafe.Pointer(bp + 12)) = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
	if libc.Uint32FromInt16((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1)-uint32(_Jretw) <= libc.Uint32FromInt32(int32(_Jret0)-int32(_Jretw)) {
		if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jretc) {
			k = int32(_Kl)
		} else {
			if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) >= int32(_Jretsb) {
				k = int32(_Kw)
			} else {
				k = int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) - int32(_Jretw)
			}
		}
		if !(s_usecheck(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), k, fn) != 0) {
			goto JErr
		}
	}
	if !(int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jjnz) && !(s_usecheck(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), int32(_Kw), fn) != 0)) {
		goto _46
	}
	goto JErr
JErr:
	;
	_err(tls, qbe, __ccgo_ts+2346, libc.VaList(bp+56, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192, b1+180))
_46:
	;
	if (*_Blk)(unsafe.Pointer(b1)).Fs1 != 0 && int32((*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b1)).Fs1)).Fjmp.Ftype1) == int32(_Jxxx) {
		_err(tls, qbe, __ccgo_ts+2395, libc.VaList(bp+56, (*_Blk)(unsafe.Pointer(b1)).Fs1+180))
	}
	if (*_Blk)(unsafe.Pointer(b1)).Fs2 != 0 && int32((*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b1)).Fs2)).Fjmp.Ftype1) == int32(_Jxxx) {
		_err(tls, qbe, __ccgo_ts+2395, libc.VaList(bp+56, (*_Blk)(unsafe.Pointer(b1)).Fs2+180))
	}
	goto _12
_12:
	;
	b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	goto _13
	goto _11
_11:
}

func s_parsefn(tls *libc.TLS, qbe *_QBE, lnk uintptr) (r uintptr) {
	var b uintptr
	var i int32
	var ps _PState
	_, _, _ = b, i, ps
	qbe.s_curb = uintptr(0)
	qbe.s_nblk = 0
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb))
	qbe.s_curf = x_alloc(tls, qbe, uint64(192))
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fntmp = 0
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fncon = int32(2)
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Ftmp = x_vnew(tls, qbe, libc.Uint64FromInt32((*_Fn)(unsafe.Pointer(qbe.s_curf)).Fntmp), uint64(192), int32(_PFn))
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fcon = x_vnew(tls, qbe, libc.Uint64FromInt32((*_Fn)(unsafe.Pointer(qbe.s_curf)).Fncon), uint64(32), int32(_PFn))
	i = 0
	for {
		if !(i < int32(_Tmp0)) {
			break
		}
		if qbe.x_T.Ffpr0 <= i && i < qbe.x_T.Ffpr0+qbe.x_T.Fnfpr {
			x_newtmp(tls, qbe, uintptr(0), int32(_Kd), qbe.s_curf)
		} else {
			x_newtmp(tls, qbe, uintptr(0), int32(_Kl), qbe.s_curf)
		}
		goto _1
	_1:
		;
		i++
	}
	(*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(qbe.s_curf)).Fcon))).Ftype1 = int32(_CBits)
	*(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(qbe.s_curf)).Fcon + 16)) = libc.Int64FromUint32(0xdeaddead) /* UNDEF */
	(*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(qbe.s_curf)).Fcon + 1*32))).Ftype1 = int32(_CBits)
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Flnk = *(*_Lnk)(unsafe.Pointer(lnk))
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fleaf = int8(1)
	qbe.s_blink = qbe.s_curf
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fretty = int32(_Kx)
	if s_peek(tls, qbe) != int32(_Tglo) {
		qbe.s_rcls = s_parsecls(tls, qbe, qbe.s_curf+48)
	} else {
		qbe.s_rcls = int32(_K0)
	}
	if s_next(tls, qbe) != int32(_Tglo) {
		_err(tls, qbe, __ccgo_ts+2423, 0)
	}
	libc.X__builtin___strncpy_chk(tls, qbe.s_curf+83, qbe.s_tokval.Fstr, libc.Uint64FromInt32(int32(_NString)-libc.Int32FromInt32(1)), ^___predefined_size_t(0))
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fvararg = int8(s_parserefl(tls, qbe, 0))
	if s_nextnl(tls, qbe) != int32(_Tlbrace) {
		_err(tls, qbe, __ccgo_ts+2446, 0)
	}
	ps = int32(_PLbl)
	for cond := true; cond; cond = ps != int32(_PEnd) {
		ps = s_parseline(tls, qbe, ps)
	}
	if !(qbe.s_curb != 0) {
		_err(tls, qbe, __ccgo_ts+2478, 0)
	}
	if int32((*_Blk)(unsafe.Pointer(qbe.s_curb)).Fjmp.Ftype1) == int32(_Jxxx) {
		_err(tls, qbe, __ccgo_ts+2493, 0)
	}
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fmem = x_vnew(tls, qbe, uint64(0), uint64(48), int32(_PFn))
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fnmem = 0
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Fnblk = libc.Uint32FromInt32(qbe.s_nblk)
	(*_Fn)(unsafe.Pointer(qbe.s_curf)).Frpo = uintptr(0)
	b = (*_Fn)(unsafe.Pointer(qbe.s_curf)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fdlink = uintptr(0)
		goto _2
	_2:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	} /* was trashed by findblk() */
	i = 0
	for {
		if !(i < int32(_BMask)+libc.Int32FromInt32(1)) {
			break
		}
		qbe.s_blkh[i] = uintptr(0)
		goto _3
	_3:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, qbe.s_tmph, 0, libc.Uint64FromInt32(qbe.s_tmphcap)*uint64(4), ^___predefined_size_t(0))
	s_typecheck(tls, qbe, qbe.s_curf)
	return qbe.s_curf
}

func s_parsefields(tls *libc.TLS, qbe *_QBE, fld uintptr, ty uintptr, t int32) {
	var a, al, c, n, type1 int32
	var s, sz _uint64_t
	var ty1 uintptr
	_, _, _, _, _, _, _, _ = a, al, c, n, s, sz, ty1, type1
	n = 0
	sz = uint64(0)
	al = (*_Typ)(unsafe.Pointer(ty)).Falign
	for t != int32(_Trbrace) {
		ty1 = uintptr(0)
		switch t {
		default:
			_err(tls, qbe, __ccgo_ts+2516, 0)
			fallthrough
		case int32(_Td):
			type1 = int32(_Fd)
			s = uint64(8)
			a = int32(3)
		case int32(_Tl):
			type1 = int32(_Fl)
			s = uint64(8)
			a = int32(3)
		case int32(_Ts):
			type1 = int32(_Fs)
			s = uint64(4)
			a = int32(2)
		case int32(_Tw):
			type1 = int32(_Fw)
			s = uint64(4)
			a = int32(2)
		case int32(_Th):
			type1 = int32(_Fh)
			s = uint64(2)
			a = int32(1)
		case int32(_Tb):
			type1 = int32(_Fb)
			s = uint64(1)
			a = 0
		case int32(_Ttyp):
			type1 = int32(_FTyp)
			ty1 = qbe.x_typ + uintptr(s_findtyp(tls, qbe, libc.Int32FromUint32(qbe.s_ntyp-uint32(1))))*112
			s = (*_Typ)(unsafe.Pointer(ty1)).Fsize
			a = (*_Typ)(unsafe.Pointer(ty1)).Falign
			break
		}
		if a > al {
			al = a
		}
		a = int32(1)<<a - int32(1)
		a = libc.Int32FromUint64((sz+libc.Uint64FromInt32(a))&libc.Uint64FromInt32(^a) - sz)
		if a != 0 {
			if n < int32(_NField) {
				/* padding */
				(*(*_Field)(unsafe.Pointer(fld + uintptr(n)*8))).Ftype1 = int32(_FPad)
				(*(*_Field)(unsafe.Pointer(fld + uintptr(n)*8))).Flen1 = libc.Uint32FromInt32(a)
				n++
			}
		}
		t = s_nextnl(tls, qbe)
		if t == int32(_Tint) {
			c = int32(qbe.s_tokval.Fnum)
			t = s_nextnl(tls, qbe)
		} else {
			c = int32(1)
		}
		sz += libc.Uint64FromInt32(a) + libc.Uint64FromInt32(c)*s
		if type1 == int32(_FTyp) {
			s = libc.Uint64FromInt64((int64(ty1) - int64(qbe.x_typ)) / 112)
		}
		for {
			if !(c > 0 && n < int32(_NField)) {
				break
			}
			(*(*_Field)(unsafe.Pointer(fld + uintptr(n)*8))).Ftype1 = type1
			(*(*_Field)(unsafe.Pointer(fld + uintptr(n)*8))).Flen1 = uint32(s)
			goto _1
		_1:
			;
			c--
			n++
		}
		if t != int32(_Tcomma) {
			break
		}
		t = s_nextnl(tls, qbe)
	}
	if t != int32(_Trbrace) {
		_err(tls, qbe, __ccgo_ts+2546, 0)
	}
	(*(*_Field)(unsafe.Pointer(fld + uintptr(n)*8))).Ftype1 = int32(_FEnd)
	a = int32(1) << al
	if sz < (*_Typ)(unsafe.Pointer(ty)).Fsize {
		sz = (*_Typ)(unsafe.Pointer(ty)).Fsize
	}
	(*_Typ)(unsafe.Pointer(ty)).Fsize = (sz + libc.Uint64FromInt32(a) - uint64(1)) & libc.Uint64FromInt32(-a)
	(*_Typ)(unsafe.Pointer(ty)).Falign = al
}

func s_parsetyp(tls *libc.TLS, qbe *_QBE) {
	var al, t int32
	var n, v1, v4, v5 _uint
	var ty, p3 uintptr
	_, _, _, _, _, _, _, _ = al, n, t, ty, v1, v4, v5, p3
	/* be careful if extending the syntax
	 * to handle nested types, any pointer
	 * held to typ[] might be invalidated!
	 */
	x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.x_typ)), uint64(qbe.s_ntyp+uint32(1)))
	v1 = qbe.s_ntyp
	qbe.s_ntyp++
	ty = qbe.x_typ + uintptr(v1)*112
	(*_Typ)(unsafe.Pointer(ty)).Fisdark = 0
	(*_Typ)(unsafe.Pointer(ty)).Fisunion = 0
	(*_Typ)(unsafe.Pointer(ty)).Falign = -int32(1)
	(*_Typ)(unsafe.Pointer(ty)).Fsize = uint64(0)
	if s_nextnl(tls, qbe) != int32(_Ttyp) || s_nextnl(tls, qbe) != int32(_Teq) {
		_err(tls, qbe, __ccgo_ts+2562, 0)
	}
	libc.X__builtin___strcpy_chk(tls, ty, qbe.s_tokval.Fstr, ^___predefined_size_t(0))
	t = s_nextnl(tls, qbe)
	if t == int32(_Talign) {
		if s_nextnl(tls, qbe) != int32(_Tint) {
			_err(tls, qbe, __ccgo_ts+2592, 0)
		}
		al = 0
		for {
			p3 = uintptr(unsafe.Pointer(&qbe.s_tokval)) + 24
			*(*_int64_t)(unsafe.Pointer(p3)) /= int64(2)
			if !(*(*_int64_t)(unsafe.Pointer(p3)) != 0) {
				break
			}
			goto _2
		_2:
			;
			al++
		}
		(*_Typ)(unsafe.Pointer(ty)).Falign = al
		t = s_nextnl(tls, qbe)
	}
	if t != int32(_Tlbrace) {
		_err(tls, qbe, __ccgo_ts+2611, 0)
	}
	t = s_nextnl(tls, qbe)
	if t == int32(_Tint) {
		(*_Typ)(unsafe.Pointer(ty)).Fisdark = int8(1)
		(*_Typ)(unsafe.Pointer(ty)).Fsize = libc.Uint64FromInt64(qbe.s_tokval.Fnum)
		if (*_Typ)(unsafe.Pointer(ty)).Falign == -int32(1) {
			_err(tls, qbe, __ccgo_ts+2639, 0)
		}
		if s_nextnl(tls, qbe) != int32(_Trbrace) {
			_err(tls, qbe, __ccgo_ts+2665, 0)
		}
		return
	}
	n = uint32(0)
	(*_Typ)(unsafe.Pointer(ty)).Ffields = x_vnew(tls, qbe, uint64(1), uint64(264), int32(_PHeap))
	if t == int32(_Tlbrace) {
		(*_Typ)(unsafe.Pointer(ty)).Fisunion = int8(1)
		for cond := true; cond; cond = t != int32(_Trbrace) {
			if t != int32(_Tlbrace) {
				_err(tls, qbe, __ccgo_ts+2676, 0)
			}
			x_vgrow(tls, qbe, ty+104, uint64(n+uint32(1)))
			v4 = n
			n++
			s_parsefields(tls, qbe, (*_Typ)(unsafe.Pointer(ty)).Ffields+uintptr(v4)*264, ty, s_nextnl(tls, qbe))
			t = s_nextnl(tls, qbe)
		}
	} else {
		v5 = n
		n++
		s_parsefields(tls, qbe, (*_Typ)(unsafe.Pointer(ty)).Ffields+uintptr(v5)*264, ty, t)
	}
	(*_Typ)(unsafe.Pointer(ty)).Fnunion = n
}

func s_parsedatref(tls *libc.TLS, qbe *_QBE, d uintptr) {
	var t int32
	_ = t
	(*_Dat)(unsafe.Pointer(d)).Fisref = int8(1)
	(*(*struct {
		Fname uintptr
		Foff  _int64_t
	})(unsafe.Pointer(d + 24))).Fname = qbe.s_tokval.Fstr
	(*(*struct {
		Fname uintptr
		Foff  _int64_t
	})(unsafe.Pointer(d + 24))).Foff = 0
	t = s_peek(tls, qbe)
	if t == int32(_Tplus) {
		s_next(tls, qbe)
		if s_next(tls, qbe) != int32(_Tint) {
			_err(tls, qbe, __ccgo_ts+2697, 0)
		}
		(*(*struct {
			Fname uintptr
			Foff  _int64_t
		})(unsafe.Pointer(d + 24))).Foff = qbe.s_tokval.Fnum
	}
}

func s_parsedatstr(tls *libc.TLS, qbe *_QBE, d uintptr) {
	(*_Dat)(unsafe.Pointer(d)).Fisstr = int8(1)
	*(*uintptr)(unsafe.Pointer(&(*_Dat)(unsafe.Pointer(d)).Fu)) = qbe.s_tokval.Fstr
}

func s_parsedat(tls *libc.TLS, qbe *_QBE, cb uintptr, lnk uintptr) {
	bp := tls.Alloc(144)
	defer tls.Free(144)
	var t int32
	var _ /* d at bp+80 */ _Dat
	var _ /* name at bp+0 */ [80]int8
	_ = t
	*(*[80]int8)(unsafe.Pointer(bp)) = [80]int8{}
	if s_nextnl(tls, qbe) != int32(_Tglo) || s_nextnl(tls, qbe) != int32(_Teq) {
		_err(tls, qbe, __ccgo_ts+2731, 0)
	}
	libc.X__builtin___strncpy_chk(tls, bp, qbe.s_tokval.Fstr, libc.Uint64FromInt32(int32(_NString)-libc.Int32FromInt32(1)), ^___predefined_size_t(0))
	t = s_nextnl(tls, qbe)
	(*_Lnk)(unsafe.Pointer(lnk)).Falign = int8(8)
	if t == int32(_Talign) {
		if s_nextnl(tls, qbe) != int32(_Tint) {
			_err(tls, qbe, __ccgo_ts+2592, 0)
		}
		if qbe.s_tokval.Fnum <= 0 || qbe.s_tokval.Fnum > int64(m___SCHAR_MAX__) || qbe.s_tokval.Fnum&(qbe.s_tokval.Fnum-int64(1)) != 0 {
			_err(tls, qbe, __ccgo_ts+2758, 0)
		}
		(*_Lnk)(unsafe.Pointer(lnk)).Falign = int8(qbe.s_tokval.Fnum)
		t = s_nextnl(tls, qbe)
	}
	(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DStart)
	(*(*_Dat)(unsafe.Pointer(bp + 80))).Fname = bp
	(*(*_Dat)(unsafe.Pointer(bp + 80))).Flnk = lnk
	(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{cb})))(tls, qbe, bp+80)
	if t != int32(_Tlbrace) {
		_err(tls, qbe, __ccgo_ts+2776, 0)
	}
	for {
		switch s_nextnl(tls, qbe) {
		default:
			_err(tls, qbe, __ccgo_ts+2809, libc.VaList(bp+136, int32(qbe.s_tokval.Fchr)))
			fallthrough
		case int32(_Trbrace):
			goto Done
		case int32(_Tl):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DL)
		case int32(_Tw):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DW)
		case int32(_Th):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DH)
		case int32(_Tb):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DB)
		case int32(_Ts):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DW)
		case int32(_Td):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DL)
		case int32(_Tz):
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DZ)
			break
		}
		t = s_nextnl(tls, qbe)
		for cond := true; cond; cond = t == int32(_Tint) || t == int32(_Tflts) || t == int32(_Tfltd) || t == int32(_Tstr) || t == int32(_Tglo) {
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Fisstr = 0
			(*(*_Dat)(unsafe.Pointer(bp + 80))).Fisref = 0
			libc.X__builtin___memset_chk(tls, bp+80+24, 0, uint64(16), ^___predefined_size_t(0))
			if t == int32(_Tflts) {
				*(*float32)(unsafe.Pointer(bp + 80 + 24)) = qbe.s_tokval.Fflts
			} else {
				if t == int32(_Tfltd) {
					*(*float64)(unsafe.Pointer(bp + 80 + 24)) = qbe.s_tokval.Ffltd
				} else {
					if t == int32(_Tint) {
						*(*_int64_t)(unsafe.Pointer(bp + 80 + 24)) = qbe.s_tokval.Fnum
					} else {
						if t == int32(_Tglo) {
							s_parsedatref(tls, qbe, bp+80)
						} else {
							if t == int32(_Tstr) {
								s_parsedatstr(tls, qbe, bp+80)
							} else {
								_err(tls, qbe, __ccgo_ts+2843, 0)
							}
						}
					}
				}
			}
			(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{cb})))(tls, qbe, bp+80)
			t = s_nextnl(tls, qbe)
		}
		if t == int32(_Trbrace) {
			break
		}
		if t != int32(_Tcomma) {
			_err(tls, qbe, __ccgo_ts+2546, 0)
		}
		goto _1
	_1:
	}
	goto Done
Done:
	;
	(*(*_Dat)(unsafe.Pointer(bp + 80))).Ftype1 = int32(_DEnd)
	(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{cb})))(tls, qbe, bp+80)
}

func s_parselnk(tls *libc.TLS, qbe *_QBE, lnk uintptr) (r int32) {
	var haslnk, t, v2 int32
	_, _, _ = haslnk, t, v2
	haslnk = 0
	for {
		v2 = s_nextnl(tls, qbe)
		t = v2
		switch v2 {
		case int32(_Texport):
			(*_Lnk)(unsafe.Pointer(lnk)).Fexport = int8(1)
		case int32(_Tthread):
			(*_Lnk)(unsafe.Pointer(lnk)).Fthread = int8(1)
		case int32(_Tcommon):
			(*_Lnk)(unsafe.Pointer(lnk)).Fcommon = int8(1)
		case int32(_Tsection):
			if (*_Lnk)(unsafe.Pointer(lnk)).Fsec != 0 {
				_err(tls, qbe, __ccgo_ts+2869, 0)
			}
			if s_next(tls, qbe) != int32(_Tstr) {
				_err(tls, qbe, __ccgo_ts+2894, 0)
			}
			(*_Lnk)(unsafe.Pointer(lnk)).Fsec = qbe.s_tokval.Fstr
			if s_peek(tls, qbe) == int32(_Tstr) {
				s_next(tls, qbe)
				(*_Lnk)(unsafe.Pointer(lnk)).Fsecf = qbe.s_tokval.Fstr
			}
		default:
			if t == int32(_Tfunc) && (*_Lnk)(unsafe.Pointer(lnk)).Fthread != 0 {
				_err(tls, qbe, __ccgo_ts+2918, 0)
			}
			if haslnk != 0 && t != int32(_Tdata) && t != int32(_Tfunc) {
				_err(tls, qbe, __ccgo_ts+2952, 0)
			}
			return t
		}
		goto _1
	_1:
		;
		haslnk = int32(1)
	}
	return r
}

func x_parse(tls *libc.TLS, qbe *_QBE, f uintptr, path uintptr, dbgfile uintptr, data uintptr, func1 uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var n _uint
	var _ /* lnk at bp+0 */ _Lnk
	_ = n
	s_lexinit(tls, qbe)
	qbe.s_inf = f
	qbe.s_inpath = path
	qbe.s_lnum = int32(1)
	qbe.s_thead = int32(_Txxx)
	qbe.s_ntyp = uint32(0)
	qbe.x_typ = x_vnew(tls, qbe, uint64(0), uint64(112), int32(_PHeap))
	for {
		*(*_Lnk)(unsafe.Pointer(bp)) = _Lnk{}
		switch s_parselnk(tls, qbe, bp) {
		default:
			goto _2
		case int32(_Tdbgfile):
			goto _3
		case int32(_Tfunc):
			goto _4
		case int32(_Tdata):
			goto _5
		case int32(_Ttype):
			goto _6
		case int32(_Teof):
			goto _7
		}
		goto _8
	_2:
		;
		_err(tls, qbe, __ccgo_ts+2988, 0)
	_3:
		;
		s_expect(tls, qbe, int32(_Tstr))
		(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{dbgfile})))(tls, qbe, qbe.s_tokval.Fstr)
		goto _8
	_4:
		;
		(*(*_Lnk)(unsafe.Pointer(bp))).Falign = int8(16)
		(*(*func(*libc.TLS, *_QBE, uintptr))(unsafe.Pointer(&struct{ uintptr }{func1})))(tls, qbe, s_parsefn(tls, qbe, bp))
		goto _8
	_5:
		;
		s_parsedat(tls, qbe, data, bp)
		goto _8
	_6:
		;
		s_parsetyp(tls, qbe)
		goto _8
	_7:
		;
		n = uint32(0)
	_11:
		;
		if !(n < qbe.s_ntyp) {
			goto _9
		}
		if (*(*_Typ)(unsafe.Pointer(qbe.x_typ + uintptr(n)*112))).Fnunion != 0 {
			x_vfree(tls, qbe, (*(*_Typ)(unsafe.Pointer(qbe.x_typ + uintptr(n)*112))).Ffields)
		}
		goto _10
	_10:
		;
		n++
		goto _11
		goto _9
	_9:
		;
		x_vfree(tls, qbe, qbe.x_typ)
		return
	_8:
		;
		goto _1
	_1:
	}
}

func s_printcon(tls *libc.TLS, qbe *_QBE, c uintptr, f uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	switch (*_Con)(unsafe.Pointer(c)).Ftype1 {
	case int32(_CUndef):
	case int32(_CAddr):
		if (*_Con)(unsafe.Pointer(c)).Fsym.Ftype1 == int32(_SThr) {
			fprintf(tls, qbe, f, __ccgo_ts+3018, 0)
		}
		fprintf(tls, qbe, f, __ccgo_ts+3026, libc.VaList(bp+8, x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid)))
		if *(*_int64_t)(unsafe.Pointer(c + 16)) != 0 {
			fprintf(tls, qbe, f, __ccgo_ts+3030, libc.VaList(bp+8, *(*_int64_t)(unsafe.Pointer(c + 16))))
		}
	case int32(_CBits):
		if int32((*_Con)(unsafe.Pointer(c)).Fflt) == int32(1) {
			fprintf(tls, qbe, f, __ccgo_ts+3036, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(c)).Fbits)))))
		} else {
			if int32((*_Con)(unsafe.Pointer(c)).Fflt) == int32(2) {
				fprintf(tls, qbe, f, __ccgo_ts+3041, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(c + 16))))
			} else {
				fprintf(tls, qbe, f, __ccgo_ts+3047, libc.VaList(bp+8, *(*_int64_t)(unsafe.Pointer(c + 16))))
			}
		}
		break
	}
}

func x_printref(tls *libc.TLS, qbe *_QBE, _r _Ref, fn uintptr, f uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	*(*_Ref)(unsafe.Pointer(bp + 16)) = _r
	var i, v10, v12, v16, v20, v22, v4, v6 int32
	var m uintptr
	var v1, v14, v15, v18, v19, v2, v3, v8, v9 _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = i, m, v1, v10, v12, v14, v15, v16, v18, v19, v2, v20, v22, v3, v4, v6, v8, v9
	v1 = *(*_Ref)(unsafe.Pointer(bp + 16))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	switch v6 {
	case int32(_RTmp):
		if int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3) < int32(_Tmp0) {
			fprintf(tls, qbe, f, __ccgo_ts+3052, libc.VaList(bp+32, int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3)))
		} else {
			fprintf(tls, qbe, f, __ccgo_ts+3056, libc.VaList(bp+32, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*192))
		}
	case int32(_RCon):
		v8 = *(*_Ref)(unsafe.Pointer(bp + 16))
		v9 = _Ref{
			F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(0)&0x1fffffff<<3,
		}
		*(*_Ref)(unsafe.Pointer(bp)) = v8
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
		v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _11
	_11:
		if v10 != 0 {
			fprintf(tls, qbe, f, __ccgo_ts+3061, 0)
		} else {
			s_printcon(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*32, f)
		}
	case int32(_RSlot):
		*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(bp + 16))
		v12 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
		goto _13
	_13:
		fprintf(tls, qbe, f, __ccgo_ts+3067, libc.VaList(bp+32, v12))
	case int32(_RCall):
		fprintf(tls, qbe, f, __ccgo_ts+3071, libc.VaList(bp+32, int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3)))
	case int32(_RType):
		fprintf(tls, qbe, f, __ccgo_ts+3076, libc.VaList(bp+32, qbe.x_typ+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*112))
	case int32(_RMem):
		i = 0
		m = (*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*48
		fputc(tls, qbe, int32('['), f)
		if (*_Mem)(unsafe.Pointer(m)).Foffset.Ftype1 != int32(_CUndef) {
			s_printcon(tls, qbe, m, f)
			i = int32(1)
		}
		v14 = (*_Mem)(unsafe.Pointer(m)).Fbase
		v15 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v14
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v15
		v16 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _17
	_17:
		if !(v16 != 0) {
			if i != 0 {
				fprintf(tls, qbe, f, __ccgo_ts+3080, 0)
			}
			x_printref(tls, qbe, (*_Mem)(unsafe.Pointer(m)).Fbase, fn, f)
			i = int32(1)
		}
		v18 = (*_Mem)(unsafe.Pointer(m)).Findex
		v19 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v18
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v19
		v20 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _21
	_21:
		if !(v20 != 0) {
			if i != 0 {
				fprintf(tls, qbe, f, __ccgo_ts+3080, 0)
			}
			fprintf(tls, qbe, f, __ccgo_ts+3084, libc.VaList(bp+32, (*_Mem)(unsafe.Pointer(m)).Fscale))
			x_printref(tls, qbe, (*_Mem)(unsafe.Pointer(m)).Findex, fn, f)
		}
		fputc(tls, qbe, int32(']'), f)
	case int32(_RInt):
		*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(bp + 16))
		v22 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
		goto _23
	_23:
		fprintf(tls, qbe, f, __ccgo_ts+3090, libc.VaList(bp+32, v22))
	case -int32(1):
		fprintf(tls, qbe, f, __ccgo_ts+3093, 0)
		break
	}
}

func x_printfn(tls *libc.TLS, qbe *_QBE, fn uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var b1, i, p uintptr
	var n _uint
	var v10, v13, v14, v17, v18, v21, v22, v5, v6, v9 _Ref
	var v11, v15, v19, v23, v7 int32
	var v25 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, i, n, p, v10, v11, v13, v14, v15, v17, v18, v19, v21, v22, v23, v25, v5, v6, v7, v9
	fprintf(tls, qbe, f, __ccgo_ts+3264, libc.VaList(bp+16, fn+83))
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		fprintf(tls, qbe, f, __ccgo_ts+3282, libc.VaList(bp+16, b1+180))
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			fprintf(tls, qbe, f, __ccgo_ts+3287, 0)
			x_printref(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Fto, fn, f)
			fprintf(tls, qbe, f, __ccgo_ts+3289, libc.VaList(bp+16, int32(qbe.s_ktoc[(*_Phi)(unsafe.Pointer(p)).Fcls])))
			n = uint32(0)
			for {
				fprintf(tls, qbe, f, __ccgo_ts+3299, libc.VaList(bp+16, *(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8))+180))
				x_printref(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(n)*4)), fn, f)
				if n == (*_Phi)(unsafe.Pointer(p)).Fnarg-uint32(1) {
					fprintf(tls, qbe, f, __ccgo_ts+82, 0)
					break
				} else {
					fprintf(tls, qbe, f, __ccgo_ts+3304, 0)
				}
				goto _3
			_3:
				;
				n++
			}
			goto _2
		_2:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			fprintf(tls, qbe, f, __ccgo_ts+3287, 0)
			v5 = (*_Ins)(unsafe.Pointer(i)).Fto
			v6 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v5
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
			v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _8
		_8:
			if !(v7 != 0) {
				x_printref(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, fn, f)
				fprintf(tls, qbe, f, __ccgo_ts+3307, libc.VaList(bp+16, int32(qbe.s_ktoc[int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)])))
			}
			fprintf(tls, qbe, f, __ccgo_ts+3313, libc.VaList(bp+16, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname))
			v9 = (*_Ins)(unsafe.Pointer(i)).Fto
			v10 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v9
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
			v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _12
		_12:
			if v11 != 0 {
				switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
				case int32(_Oarg):
					fallthrough
				case int32(_Oswap):
					fallthrough
				case int32(_Oxcmp):
					fallthrough
				case int32(_Oacmp):
					fallthrough
				case int32(_Oacmn):
					fallthrough
				case int32(_Oafcmp):
					fallthrough
				case int32(_Oxtest):
					fallthrough
				case int32(_Oxdiv):
					fallthrough
				case int32(_Oxidiv):
					fputc(tls, qbe, int32(qbe.s_ktoc[int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)]), f)
				}
			}
			v13 = *(*_Ref)(unsafe.Pointer(i + 8))
			v14 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v13
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v14
			v15 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _16
		_16:
			if !(v15 != 0) {
				fprintf(tls, qbe, f, __ccgo_ts+3316, 0)
				x_printref(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), fn, f)
			}
			v17 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
			v18 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v17
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v18
			v19 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _20
		_20:
			if !(v19 != 0) {
				fprintf(tls, qbe, f, __ccgo_ts+3304, 0)
				x_printref(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), fn, f)
			}
			fprintf(tls, qbe, f, __ccgo_ts+82, 0)
			goto _4
		_4:
			;
			i += 16
		}
		switch int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) {
		case int32(_Jret0):
			fallthrough
		case int32(_Jretsb):
			fallthrough
		case int32(_Jretub):
			fallthrough
		case int32(_Jretsh):
			fallthrough
		case int32(_Jretuh):
			fallthrough
		case int32(_Jretw):
			fallthrough
		case int32(_Jretl):
			fallthrough
		case int32(_Jrets):
			fallthrough
		case int32(_Jretd):
			fallthrough
		case int32(_Jretc):
			fprintf(tls, qbe, f, __ccgo_ts+3318, libc.VaList(bp+16, qbe.s_jtoa[(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1]))
			if v25 = int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) != int32(_Jret0); !v25 {
				v21 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
				v22 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v21
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v22
				v23 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _24
			_24:
			}
			if v25 || !(v23 != 0) {
				fprintf(tls, qbe, f, __ccgo_ts+3316, 0)
				x_printref(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, fn, f)
			}
			if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jretc) {
				fprintf(tls, qbe, f, __ccgo_ts+3322, libc.VaList(bp+16, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112))
			}
			fprintf(tls, qbe, f, __ccgo_ts+82, 0)
		case int32(_Jhlt):
			fprintf(tls, qbe, f, __ccgo_ts+3328, 0)
		case int32(_Jjmp):
			if (*_Blk)(unsafe.Pointer(b1)).Fs1 != (*_Blk)(unsafe.Pointer(b1)).Flink {
				fprintf(tls, qbe, f, __ccgo_ts+3334, libc.VaList(bp+16, (*_Blk)(unsafe.Pointer(b1)).Fs1+180))
			}
		default:
			fprintf(tls, qbe, f, __ccgo_ts+3344, libc.VaList(bp+16, qbe.s_jtoa[(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1]))
			if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jjnz) {
				x_printref(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, fn, f)
				fprintf(tls, qbe, f, __ccgo_ts+3304, 0)
			}
			fprintf(tls, qbe, f, __ccgo_ts+3349, libc.VaList(bp+16, (*_Blk)(unsafe.Pointer(b1)).Fs1+180, (*_Blk)(unsafe.Pointer(b1)).Fs2+180))
			break
		}
		goto _1
	_1:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	fprintf(tls, qbe, f, __ccgo_ts+3359, 0)
}

// C documentation
//
//	/* eliminate sub-word abi op
//	 * variants for targets that
//	 * treat char/short/... as
//	 * words with arbitrary high
//	 * bits
//	 */
func x_elimsb(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	var b, i uintptr
	_, _ = b, i
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		i = (*_Blk)(unsafe.Pointer(b)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
				break
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oargsb) <= libc.Uint32FromInt32(int32(_Oarguh)-int32(_Oargsb)) {
				libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oarg), 0, 0x3fffffff)
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oparsb) <= libc.Uint32FromInt32(int32(_Oparuh)-int32(_Oparsb)) {
				libc.SetBitFieldPtr32Uint32(i+0, uint32(_Opar), 0, 0x3fffffff)
			}
			goto _2
		_2:
			;
			i += 16
		}
		if libc.Uint32FromInt16((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)-uint32(_Jretsb) <= libc.Uint32FromInt32(int32(_Jretuh)-int32(_Jretsb)) {
			(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jretw)
		}
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
}

func x_newblk(tls *libc.TLS, qbe *_QBE) (r uintptr) {
	var b uintptr
	_ = b
	b = x_alloc(tls, qbe, uint64(264))
	*(*_Blk)(unsafe.Pointer(b)) = qbe.s_z
	return b
}

func x_edgedel(tls *libc.TLS, qbe *_QBE, bs uintptr, pbd uintptr) {
	var a _uint
	var bd, p uintptr
	var mult int32
	_, _, _, _ = a, bd, mult, p
	bd = *(*uintptr)(unsafe.Pointer(pbd))
	mult = int32(1) + libc.BoolInt32((*_Blk)(unsafe.Pointer(bs)).Fs1 == (*_Blk)(unsafe.Pointer(bs)).Fs2)
	*(*uintptr)(unsafe.Pointer(pbd)) = uintptr(0)
	if !(bd != 0) || mult > int32(1) {
		return
	}
	p = (*_Blk)(unsafe.Pointer(bd)).Fphi
	for {
		if !(p != 0) {
			break
		}
		a = uint32(0)
		for {
			if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)) != bs) {
				break
			}
			goto _2
		_2:
			;
			a++
		}
		(*_Phi)(unsafe.Pointer(p)).Fnarg--
		libc.X__builtin___memmove_chk(tls, (*_Phi)(unsafe.Pointer(p)).Fblk+uintptr(a)*8, (*_Phi)(unsafe.Pointer(p)).Fblk+uintptr(a+uint32(1))*8, uint64(8)*uint64((*_Phi)(unsafe.Pointer(p)).Fnarg-a), ^___predefined_size_t(0))
		libc.X__builtin___memmove_chk(tls, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(a)*4, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(a+uint32(1))*4, uint64(4)*uint64((*_Phi)(unsafe.Pointer(p)).Fnarg-a), ^___predefined_size_t(0))
		goto _1
	_1:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
	if (*_Blk)(unsafe.Pointer(bd)).Fnpred != uint32(0) {
		a = uint32(0)
		for {
			if !(*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(bd)).Fpred + uintptr(a)*8)) != bs) {
				break
			}
			goto _3
		_3:
			;
			a++
		}
		(*_Blk)(unsafe.Pointer(bd)).Fnpred--
		libc.X__builtin___memmove_chk(tls, (*_Blk)(unsafe.Pointer(bd)).Fpred+uintptr(a)*8, (*_Blk)(unsafe.Pointer(bd)).Fpred+uintptr(a+uint32(1))*8, uint64(8)*uint64((*_Blk)(unsafe.Pointer(bd)).Fnpred-a), ^___predefined_size_t(0))
	}
}

func s_addpred(tls *libc.TLS, qbe *_QBE, bp uintptr, bc uintptr) {
	var v1 _uint
	var v2 uintptr
	_, _ = v1, v2
	if !((*_Blk)(unsafe.Pointer(bc)).Fpred != 0) {
		(*_Blk)(unsafe.Pointer(bc)).Fpred = x_alloc(tls, qbe, uint64((*_Blk)(unsafe.Pointer(bc)).Fnpred)*uint64(8))
		(*_Blk)(unsafe.Pointer(bc)).Fvisit = uint32(0)
	}
	v2 = bc + 60
	v1 = *(*_uint)(unsafe.Pointer(v2))
	*(*_uint)(unsafe.Pointer(v2))++
	*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(bc)).Fpred + uintptr(v1)*8)) = bp
}

// C documentation
//
//	/* fill predecessors information in blocks */
func x_fillpreds(tls *libc.TLS, qbe *_QBE, f uintptr) {
	var b uintptr
	_ = b
	b = (*_Fn)(unsafe.Pointer(f)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fnpred = uint32(0)
		(*_Blk)(unsafe.Pointer(b)).Fpred = uintptr(0)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	b = (*_Fn)(unsafe.Pointer(f)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 {
			(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs1)).Fnpred++
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs2 != 0 && (*_Blk)(unsafe.Pointer(b)).Fs2 != (*_Blk)(unsafe.Pointer(b)).Fs1 {
			(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs2)).Fnpred++
		}
		goto _2
	_2:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	b = (*_Fn)(unsafe.Pointer(f)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 {
			s_addpred(tls, qbe, b, (*_Blk)(unsafe.Pointer(b)).Fs1)
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs2 != 0 && (*_Blk)(unsafe.Pointer(b)).Fs2 != (*_Blk)(unsafe.Pointer(b)).Fs1 {
			s_addpred(tls, qbe, b, (*_Blk)(unsafe.Pointer(b)).Fs2)
		}
		goto _3
	_3:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
}

func s_rporec(tls *libc.TLS, qbe *_QBE, b uintptr, x _uint) (r int32) {
	var s1, s2 uintptr
	_, _ = s1, s2
	if !(b != 0) || (*_Blk)(unsafe.Pointer(b)).Fid != -libc.Uint32FromUint32(1) {
		return libc.Int32FromUint32(x)
	}
	(*_Blk)(unsafe.Pointer(b)).Fid = uint32(1)
	s1 = (*_Blk)(unsafe.Pointer(b)).Fs1
	s2 = (*_Blk)(unsafe.Pointer(b)).Fs2
	if s1 != 0 && s2 != 0 && (*_Blk)(unsafe.Pointer(s1)).Floop > (*_Blk)(unsafe.Pointer(s2)).Floop {
		s1 = (*_Blk)(unsafe.Pointer(b)).Fs2
		s2 = (*_Blk)(unsafe.Pointer(b)).Fs1
	}
	x = libc.Uint32FromInt32(s_rporec(tls, qbe, s1, x))
	x = libc.Uint32FromInt32(s_rporec(tls, qbe, s2, x))
	(*_Blk)(unsafe.Pointer(b)).Fid = x
	return libc.Int32FromUint32(x - uint32(1))
}

// C documentation
//
//	/* fill the rpo information */
func x_fillrpo(tls *libc.TLS, qbe *_QBE, f uintptr) {
	var b, p, v3 uintptr
	var n _uint
	_, _, _, _ = b, n, p, v3
	b = (*_Fn)(unsafe.Pointer(f)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fid = -libc.Uint32FromUint32(1)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	n = libc.Uint32FromInt32(int32(1) + s_rporec(tls, qbe, (*_Fn)(unsafe.Pointer(f)).Fstart, (*_Fn)(unsafe.Pointer(f)).Fnblk-uint32(1)))
	*(*_uint)(unsafe.Pointer(f + 44)) -= n
	(*_Fn)(unsafe.Pointer(f)).Frpo = x_alloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(f)).Fnblk)*uint64(8))
	p = f
	for {
		v3 = *(*uintptr)(unsafe.Pointer(p))
		b = v3
		if !(v3 != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(b)).Fid == -libc.Uint32FromUint32(1) {
			x_edgedel(tls, qbe, b, b+32)
			x_edgedel(tls, qbe, b, b+40)
			*(*uintptr)(unsafe.Pointer(p)) = (*_Blk)(unsafe.Pointer(b)).Flink
		} else {
			*(*_uint)(unsafe.Pointer(b + 56)) -= n
			*(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(f)).Frpo + uintptr((*_Blk)(unsafe.Pointer(b)).Fid)*8)) = b
			p = b + 48
		}
		goto _2
	_2:
	}
}

/* for dominators computation, read
 * "A Simple, Fast Dominance Algorithm"
 * by K. Cooper, T. Harvey, and K. Kennedy.
 */

func s_inter(tls *libc.TLS, qbe *_QBE, b1 uintptr, b2 uintptr) (r uintptr) {
	var bt uintptr
	_ = bt
	if b1 == uintptr(0) {
		return b2
	}
	for b1 != b2 {
		if (*_Blk)(unsafe.Pointer(b1)).Fid < (*_Blk)(unsafe.Pointer(b2)).Fid {
			bt = b1
			b1 = b2
			b2 = bt
		}
		for (*_Blk)(unsafe.Pointer(b1)).Fid > (*_Blk)(unsafe.Pointer(b2)).Fid {
			b1 = (*_Blk)(unsafe.Pointer(b1)).Fidom
		}
	}
	return b1
}

func x_filldom(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	var b, d, v5 uintptr
	var ch int32
	var n, p _uint
	_, _, _, _, _, _ = b, ch, d, n, p, v5
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fidom = uintptr(0)
		(*_Blk)(unsafe.Pointer(b)).Fdom = uintptr(0)
		(*_Blk)(unsafe.Pointer(b)).Fdlink = uintptr(0)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	for cond := true; cond; cond = ch != 0 {
		ch = 0
		n = uint32(1)
		for {
			if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
				break
			}
			b = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
			d = uintptr(0)
			p = uint32(0)
			for {
				if !(p < (*_Blk)(unsafe.Pointer(b)).Fnpred) {
					break
				}
				if (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(p)*8)))).Fidom != 0 || *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(p)*8)) == (*_Fn)(unsafe.Pointer(fn)).Fstart {
					d = s_inter(tls, qbe, d, *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(p)*8)))
				}
				goto _3
			_3:
				;
				p++
			}
			if d != (*_Blk)(unsafe.Pointer(b)).Fidom {
				ch++
				(*_Blk)(unsafe.Pointer(b)).Fidom = d
			}
			goto _2
		_2:
			;
			n++
		}
	}
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		v5 = (*_Blk)(unsafe.Pointer(b)).Fidom
		d = v5
		if v5 != 0 {
			(*_Blk)(unsafe.Pointer(b)).Fdlink = (*_Blk)(unsafe.Pointer(d)).Fdom
			(*_Blk)(unsafe.Pointer(d)).Fdom = b
		}
		goto _4
	_4:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
}

func x_sdom(tls *libc.TLS, qbe *_QBE, b1 uintptr, b2 uintptr) (r int32) {
	if b1 == b2 {
		return 0
	}
	for (*_Blk)(unsafe.Pointer(b2)).Fid > (*_Blk)(unsafe.Pointer(b1)).Fid {
		b2 = (*_Blk)(unsafe.Pointer(b2)).Fidom
	}
	return libc.BoolInt32(b1 == b2)
}

func x_dom(tls *libc.TLS, qbe *_QBE, b1 uintptr, b2 uintptr) (r int32) {
	return libc.BoolInt32(b1 == b2 || x_sdom(tls, qbe, b1, b2) != 0)
}

func s_addfron(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) {
	var n, v2, v4 _uint
	var v3, v5 uintptr
	_, _, _, _, _ = n, v2, v3, v4, v5
	n = uint32(0)
	for {
		if !(n < (*_Blk)(unsafe.Pointer(a)).Fnfron) {
			break
		}
		if *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(a)).Ffron + uintptr(n)*8)) == b {
			return
		}
		goto _1
	_1:
		;
		n++
	}
	if !((*_Blk)(unsafe.Pointer(a)).Fnfron != 0) {
		v3 = a + 96
		*(*_uint)(unsafe.Pointer(v3))++
		v2 = *(*_uint)(unsafe.Pointer(v3))
		(*_Blk)(unsafe.Pointer(a)).Ffron = x_vnew(tls, qbe, uint64(v2), uint64(8), int32(_PFn))
	} else {
		v5 = a + 96
		*(*_uint)(unsafe.Pointer(v5))++
		v4 = *(*_uint)(unsafe.Pointer(v5))
		x_vgrow(tls, qbe, a+88, uint64(v4))
	}
	*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(a)).Ffron + uintptr((*_Blk)(unsafe.Pointer(a)).Fnfron-uint32(1))*8)) = b
}

// C documentation
//
//	/* fill the dominance frontier */
func x_fillfron(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	var a, b uintptr
	_, _ = a, b
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fnfron = uint32(0)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 {
			a = b
			for {
				if !!(x_sdom(tls, qbe, a, (*_Blk)(unsafe.Pointer(b)).Fs1) != 0) {
					break
				}
				s_addfron(tls, qbe, a, (*_Blk)(unsafe.Pointer(b)).Fs1)
				goto _3
			_3:
				;
				a = (*_Blk)(unsafe.Pointer(a)).Fidom
			}
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs2 != 0 {
			a = b
			for {
				if !!(x_sdom(tls, qbe, a, (*_Blk)(unsafe.Pointer(b)).Fs2) != 0) {
					break
				}
				s_addfron(tls, qbe, a, (*_Blk)(unsafe.Pointer(b)).Fs2)
				goto _4
			_4:
				;
				a = (*_Blk)(unsafe.Pointer(a)).Fidom
			}
		}
		goto _2
	_2:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
}

func s_loopmark(tls *libc.TLS, qbe *_QBE, hd uintptr, b uintptr, f uintptr) {
	var p _uint
	_ = p
	if (*_Blk)(unsafe.Pointer(b)).Fid < (*_Blk)(unsafe.Pointer(hd)).Fid || (*_Blk)(unsafe.Pointer(b)).Fvisit == (*_Blk)(unsafe.Pointer(hd)).Fid {
		return
	}
	(*_Blk)(unsafe.Pointer(b)).Fvisit = (*_Blk)(unsafe.Pointer(hd)).Fid
	(*(*func(*libc.TLS, *_QBE, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{f})))(tls, qbe, hd, b)
	p = uint32(0)
	for {
		if !(p < (*_Blk)(unsafe.Pointer(b)).Fnpred) {
			break
		}
		s_loopmark(tls, qbe, hd, *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(p)*8)), f)
		goto _1
	_1:
		;
		p++
	}
}

func x_loopiter(tls *libc.TLS, qbe *_QBE, fn uintptr, f uintptr) {
	var b uintptr
	var n, p _uint
	_, _, _ = b, n, p
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fvisit = -libc.Uint32FromUint32(1)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	n = uint32(0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
			break
		}
		b = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
		p = uint32(0)
		for {
			if !(p < (*_Blk)(unsafe.Pointer(b)).Fnpred) {
				break
			}
			if (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(p)*8)))).Fid >= n {
				s_loopmark(tls, qbe, b, *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(p)*8)), f)
			}
			goto _3
		_3:
			;
			p++
		}
		goto _2
	_2:
		;
		n++
	}
}

func x_multloop(tls *libc.TLS, qbe *_QBE, hd uintptr, b uintptr) {
	_ = hd
	*(*int32)(unsafe.Pointer(b + 176)) *= int32(10)
}

func x_fillloop(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	var b uintptr
	_ = b
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Floop = int32(1)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	x_loopiter(tls, qbe, fn, __ccgo_fp(x_multloop))
}

func s_uffind(tls *libc.TLS, qbe *_QBE, pb uintptr, uf uintptr) {
	var pb1 uintptr
	_ = pb1
	pb1 = uf + uintptr((*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(pb)))).Fid)*8
	if *(*uintptr)(unsafe.Pointer(pb1)) != 0 {
		s_uffind(tls, qbe, pb1, uf)
		*(*uintptr)(unsafe.Pointer(pb)) = *(*uintptr)(unsafe.Pointer(pb1))
	}
}

// C documentation
//
//	/* requires rpo and no phis, breaks cfg */
func x_simpljmp(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	var b, p, ret, uf, v2, v5 uintptr
	var v1 _uint
	_, _, _, _, _, _, _ = b, p, ret, uf, v1, v2, v5
	ret = x_newblk(tls, qbe)
	v2 = fn + 44
	v1 = *(*_uint)(unsafe.Pointer(v2))
	*(*_uint)(unsafe.Pointer(v2))++
	(*_Blk)(unsafe.Pointer(ret)).Fid = v1
	(*_Blk)(unsafe.Pointer(ret)).Fjmp.Ftype1 = int16(_Jret0)
	uf = x_emalloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(8))
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) == int32(_Jret0) {
			(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jjmp)
			(*_Blk)(unsafe.Pointer(b)).Fs1 = ret
		}
		if (*_Blk)(unsafe.Pointer(b)).Fnins == uint32(0) {
			if int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) == int32(_Jjmp) {
				s_uffind(tls, qbe, b+32, uf)
				if (*_Blk)(unsafe.Pointer(b)).Fs1 != b {
					*(*uintptr)(unsafe.Pointer(uf + uintptr((*_Blk)(unsafe.Pointer(b)).Fid)*8)) = (*_Blk)(unsafe.Pointer(b)).Fs1
				}
			}
		}
		goto _3
	_3:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	p = fn
	for {
		v5 = *(*uintptr)(unsafe.Pointer(p))
		b = v5
		if !(v5 != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 {
			s_uffind(tls, qbe, b+32, uf)
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs2 != 0 {
			s_uffind(tls, qbe, b+40, uf)
		}
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 && (*_Blk)(unsafe.Pointer(b)).Fs1 == (*_Blk)(unsafe.Pointer(b)).Fs2 {
			(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jjmp)
			(*_Blk)(unsafe.Pointer(b)).Fs2 = uintptr(0)
		}
		goto _4
	_4:
		;
		p = b + 48
	}
	*(*uintptr)(unsafe.Pointer(p)) = ret
	free(tls, qbe, uf)
}

type _Range = struct {
	Fa int32
	Fb int32
}

type _Store = struct {
	Fip int32
	Fi  uintptr
}

type _Slot = struct {
	Ft   int32
	Fsz  int32
	Fm   _bits
	Fl   _bits
	Fr   _Range
	Fs   uintptr
	Fst  uintptr
	Fnst int32
}

// C documentation
//
//	/* require use, maintains use counts */
func x_promote(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var b1, i, l, t, u, ue uintptr
	var k, s, v5, v9 int32
	var v11 bool
	var v3, v4, v7, v8 _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, i, k, l, s, t, u, ue, v11, v3, v4, v5, v7, v8, v9
	/* promote uniform stack slots to temporaries */
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	i = (*_Blk)(unsafe.Pointer(b1)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
			break
		}
		if int32(_Oalloc) > int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) || int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) > int32(_Oalloc1) {
			goto _1
		}
		/* specific to NAlign == 3 */
		t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192
		if (*_Tmp)(unsafe.Pointer(t)).Fndef != uint32(1) {
			goto Skip
		}
		k = -int32(1)
		s = -int32(1)
		u = (*_Tmp)(unsafe.Pointer(t)).Fuse
		for {
			if !(u < (*_Tmp)(unsafe.Pointer(t)).Fuse+uintptr((*_Tmp)(unsafe.Pointer(t)).Fnuse)*16) {
				break
			}
			if (*_Use)(unsafe.Pointer(u)).Ftype1 != int32(_UIns) {
				goto Skip
			}
			l = *(*uintptr)(unsafe.Pointer(u + 8))
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(l + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) {
				if s == -int32(1) || s == x_loadsz(tls, qbe, l) {
					s = x_loadsz(tls, qbe, l)
					goto _2
				}
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(l + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
				v3 = (*_Ins)(unsafe.Pointer(i)).Fto
				v4 = *(*_Ref)(unsafe.Pointer(l + 8 + 1*4))
				*(*_Ref)(unsafe.Pointer(bp)) = v3
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v4
				v5 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _6
			_6:
				;
				if v11 = v5 != 0; v11 {
					v7 = (*_Ins)(unsafe.Pointer(i)).Fto
					v8 = *(*_Ref)(unsafe.Pointer(l + 8))
					*(*_Ref)(unsafe.Pointer(bp)) = v7
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v8
					v9 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _10
				_10:
				}
				if v11 && !(v9 != 0) {
					if s == -int32(1) || s == x_storesz(tls, qbe, l) {
						if k == -int32(1) || k == int32(*(*int16)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_optab)) + uintptr(int32(*(*uint32)(unsafe.Pointer(l + 0))&0x3fffffff>>0))*32 + 8))) {
							s = x_storesz(tls, qbe, l)
							k = int32(*(*int16)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_optab)) + uintptr(int32(*(*uint32)(unsafe.Pointer(l + 0))&0x3fffffff>>0))*32 + 8)))
							goto _2
						}
					}
				}
			}
			goto Skip
			goto _2
		_2:
			;
			u += 16
		}
		/* get rid of the alloc and replace uses */
		*(*_Ins)(unsafe.Pointer(i)) = _Ins{
			F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
		}
		(*_Tmp)(unsafe.Pointer(t)).Fndef--
		ue = (*_Tmp)(unsafe.Pointer(t)).Fuse + uintptr((*_Tmp)(unsafe.Pointer(t)).Fnuse)*16
		u = (*_Tmp)(unsafe.Pointer(t)).Fuse
		for {
			if !(u != ue) {
				break
			}
			l = *(*uintptr)(unsafe.Pointer(u + 8))
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(l + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
				libc.SetBitFieldPtr32Uint32(l+0, libc.Uint32FromInt32(k), 30, 0xc0000000)
				libc.SetBitFieldPtr32Uint32(l+0, uint32(_Ocopy), 0, 0x3fffffff)
				(*_Ins)(unsafe.Pointer(l)).Fto = *(*_Ref)(unsafe.Pointer(l + 8 + 1*4))
				*(*_Ref)(unsafe.Pointer(l + 8 + 1*4)) = _Ref{}
				(*_Tmp)(unsafe.Pointer(t)).Fnuse--
				(*_Tmp)(unsafe.Pointer(t)).Fndef++
			} else {
				if k == -int32(1) {
					_err(tls, qbe, __ccgo_ts+3362, libc.VaList(bp+16, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(l + 8 + 0))&0xfffffff8>>3))*192))
				}
				/* try to turn loads into copies so we
				 * can eliminate them later */
				switch int32(*(*uint32)(unsafe.Pointer(l + 0)) & 0x3fffffff >> 0) {
				case int32(_Oloaduw):
					goto _13
				case int32(_Oloadsw):
					goto _14
				case int32(_Oload):
					goto _15
				default:
					goto _16
				}
				goto _17
			_14:
				;
			_13:
				;
				if k == int32(_Kl) {
					goto Extend
				}
				/* fall through */
			_15:
				;
				if k>>libc.Int32FromInt32(1) != int32(*(*uint32)(unsafe.Pointer(l + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) {
					libc.SetBitFieldPtr32Uint32(l+0, uint32(_Ocast), 0, 0x3fffffff)
				} else {
					libc.SetBitFieldPtr32Uint32(l+0, uint32(_Ocopy), 0, 0x3fffffff)
				}
				goto _17
			_16:
				;
				goto Extend
			Extend:
				;
				libc.SetBitFieldPtr32Uint32(l+0, libc.Uint32FromInt32(int32(_Oextsb)+(int32(*(*uint32)(unsafe.Pointer(l + 0))&0x3fffffff>>0)-int32(_Oloadsb))), 0, 0x3fffffff)
				goto _17
			_17:
			}
			goto _12
		_12:
			;
			u += 16
		}
		goto Skip
	Skip:
		;
		goto _1
	_1:
		;
		i += 16
	}
	if qbe.x_debug[int32('M')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3400, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

func s_rin(tls *libc.TLS, qbe *_QBE, r _Range, n int32) (r1 int32) {
	return libc.BoolInt32(r.Fa <= n && n < r.Fb)
}

func s_rovlap(tls *libc.TLS, qbe *_QBE, r0 _Range, r1 _Range) (r int32) {
	return libc.BoolInt32(r0.Fb != 0 && r1.Fb != 0 && r0.Fa < r1.Fb && r1.Fa < r0.Fb)
}

func s_radd(tls *libc.TLS, qbe *_QBE, r uintptr, n int32) {
	if !((*_Range)(unsafe.Pointer(r)).Fb != 0) {
		*(*_Range)(unsafe.Pointer(r)) = _Range{
			Fa: n,
			Fb: n + int32(1),
		}
	} else {
		if n < (*_Range)(unsafe.Pointer(r)).Fa {
			(*_Range)(unsafe.Pointer(r)).Fa = n
		} else {
			if n >= (*_Range)(unsafe.Pointer(r)).Fb {
				(*_Range)(unsafe.Pointer(r)).Fb = n + int32(1)
			}
		}
	}
}

func s_slot(tls *libc.TLS, qbe *_QBE, ps uintptr, off uintptr, r _Ref, fn uintptr, sl uintptr) (r1 int32) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var t uintptr
	var _ /* a at bp+0 */ _Alias
	_ = t
	x_getalias(tls, qbe, bp, r, fn)
	if (*(*_Alias)(unsafe.Pointer(bp))).Ftype1 != int32(_ALoc) {
		return 0
	}
	t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr((*(*_Alias)(unsafe.Pointer(bp))).Fbase)*192
	if (*_Tmp)(unsafe.Pointer(t)).Fvisit < 0 {
		return 0
	}
	*(*_int64_t)(unsafe.Pointer(off)) = (*(*_Alias)(unsafe.Pointer(bp))).Foffset
	*(*uintptr)(unsafe.Pointer(ps)) = sl + uintptr((*_Tmp)(unsafe.Pointer(t)).Fvisit)*56
	return int32(1)
}

func s_load(tls *libc.TLS, qbe *_QBE, r _Ref, x _bits, ip int32, fn uintptr, sl uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* off at bp+0 */ _int64_t
	var _ /* s at bp+8 */ uintptr
	if s_slot(tls, qbe, bp+8, bp, r, fn, sl) != 0 {
		*(*_bits)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)) + 16)) |= x << *(*_int64_t)(unsafe.Pointer(bp))
		*(*_bits)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)) + 16)) &= (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fm
		if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fl != 0 {
			s_radd(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 8))+24, ip)
		}
	}
}

func s_store(tls *libc.TLS, qbe *_QBE, r _Ref, x _bits, ip int32, i uintptr, fn uintptr, sl uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var v1 int32
	var v2 uintptr
	var _ /* off at bp+0 */ _int64_t
	var _ /* s at bp+8 */ uintptr
	_, _ = v1, v2
	if s_slot(tls, qbe, bp+8, bp, r, fn, sl) != 0 {
		if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fl != 0 {
			s_radd(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 8))+24, ip)
			*(*_bits)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)) + 16)) &= ^(x << *(*_int64_t)(unsafe.Pointer(bp)))
		} else {
			v2 = *(*uintptr)(unsafe.Pointer(bp + 8)) + 48
			*(*int32)(unsafe.Pointer(v2))++
			v1 = *(*int32)(unsafe.Pointer(v2))
			x_vgrow(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 8))+40, libc.Uint64FromInt32(v1))
			(*(*_Store)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fst + uintptr((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fnst-int32(1))*16))).Fip = ip
			(*(*_Store)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fst + uintptr((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).Fnst-int32(1))*16))).Fi = i
		}
	}
}

func s_scmp(tls *libc.TLS, qbe *_QBE, pa uintptr, pb uintptr) (r int32) {
	var a, b uintptr
	_, _ = a, b
	a = pa
	b = pb
	if (*_Slot)(unsafe.Pointer(a)).Fsz != (*_Slot)(unsafe.Pointer(b)).Fsz {
		return (*_Slot)(unsafe.Pointer(b)).Fsz - (*_Slot)(unsafe.Pointer(a)).Fsz
	}
	return (*_Slot)(unsafe.Pointer(a)).Fr.Fa - (*_Slot)(unsafe.Pointer(b)).Fr.Fa
}

func s_maxrpo(tls *libc.TLS, qbe *_QBE, hd uintptr, b uintptr) {
	if (*_Blk)(unsafe.Pointer(hd)).Floop < libc.Int32FromUint32((*_Blk)(unsafe.Pointer(b)).Fid) {
		(*_Blk)(unsafe.Pointer(hd)).Floop = libc.Int32FromUint32((*_Blk)(unsafe.Pointer(b)).Fid)
	}
}

func x_coalesce(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(144)
	defer tls.Free(144)
	var arg, b1, br, i, ps, t, ts, u, v22, v24 uintptr
	var freed, fused, total _uint
	var ip, m, n, nbl, nsl, sz, v10, v11, v12, v13, v16, v17, v2, v23, v26, v30, v32, v41, v44, v5, v8 int32
	var x _bits
	var v15 uint64
	var v28, v29, v39, v40 _Ref
	var _ /* bl at bp+72 */ uintptr
	var _ /* off0 at bp+80 */ _int64_t
	var _ /* off1 at bp+88 */ _int64_t
	var _ /* r at bp+16 */ _Range
	var _ /* s at bp+24 */ uintptr
	var _ /* s0 at bp+32 */ uintptr
	var _ /* sl at bp+40 */ uintptr
	var _ /* stk at bp+96 */ uintptr
	var _ /* succ at bp+48 */ [3]uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = arg, b1, br, freed, fused, i, ip, m, n, nbl, nsl, ps, sz, t, total, ts, u, x, v10, v11, v12, v13, v15, v16, v17, v2, v22, v23, v24, v26, v28, v29, v30, v32, v39, v40, v41, v44, v5, v8
	/* minimize the stack usage
	 * by coalescing slots
	 */
	nsl = 0
	*(*uintptr)(unsafe.Pointer(bp + 40)) = x_vnew(tls, qbe, uint64(0), uint64(56), int32(_PHeap))
	n = int32(_Tmp0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
			break
		}
		t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(n)*192
		(*_Tmp)(unsafe.Pointer(t)).Fvisit = -int32(1)
		if (*_Tmp)(unsafe.Pointer(t)).Falias.Ftype1 == int32(_ALoc) {
			if (*_Tmp)(unsafe.Pointer(t)).Falias.Fslot == t+144 {
				if (*_Tmp)(unsafe.Pointer(t)).Fbid == (*_Blk)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fstart)).Fid {
					if *(*int32)(unsafe.Pointer(t + 144 + 16)) != -int32(1) {
						(*_Tmp)(unsafe.Pointer(t)).Fvisit = nsl
						nsl++
						v2 = nsl
						x_vgrow(tls, qbe, bp+40, libc.Uint64FromInt32(v2))
						*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 40)) + uintptr(nsl-int32(1))*56
						(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Ft = n
						(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fsz = *(*int32)(unsafe.Pointer(t + 144 + 16))
						(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fm = *(*_bits)(unsafe.Pointer(t + 144 + 16 + 8))
						(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs = uintptr(0)
						(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fst = x_vnew(tls, qbe, uint64(0), uint64(16), int32(_PHeap))
						(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fnst = 0
					}
				}
			}
		}
		goto _1
	_1:
		;
		n++
	}
	/* one-pass liveness analysis */
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b1)).Floop = -int32(1)
		goto _3
	_3:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	x_loopiter(tls, qbe, fn, __ccgo_fp(s_maxrpo))
	nbl = 0
	*(*uintptr)(unsafe.Pointer(bp + 72)) = x_vnew(tls, qbe, uint64(0), uint64(8), int32(_PHeap))
	br = x_emalloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(8))
	ip = libc.Int32FromInt32(m___INT_MAX__) - libc.Int32FromInt32(1)
	n = libc.Int32FromUint32((*_Fn)(unsafe.Pointer(fn)).Fnblk - uint32(1))
	for {
		if !(n >= 0) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
		(*(*[3]uintptr)(unsafe.Pointer(bp + 48)))[0] = (*_Blk)(unsafe.Pointer(b1)).Fs1
		(*(*[3]uintptr)(unsafe.Pointer(bp + 48)))[int32(1)] = (*_Blk)(unsafe.Pointer(b1)).Fs2
		(*(*[3]uintptr)(unsafe.Pointer(bp + 48)))[int32(2)] = uintptr(0)
		v5 = ip
		ip--
		(*(*_Range)(unsafe.Pointer(br + uintptr(n)*8))).Fb = v5
		*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 40))
		for {
			if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
				break
			}
			(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fl = uint64(0)
			ps = bp + 48
			for {
				if !(*(*uintptr)(unsafe.Pointer(ps)) != 0) {
					break
				}
				m = libc.Int32FromUint32((*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(ps)))).Fid)
				if m > n && s_rin(tls, qbe, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr, (*(*_Range)(unsafe.Pointer(br + uintptr(m)*8))).Fa) != 0 {
					(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fl = (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fm
					s_radd(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 24))+24, ip)
				}
				goto _7
			_7:
				;
				ps += 8
			}
			goto _6
		_6:
			;
			*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
		}
		if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jretc) {
			ip--
			v8 = ip
			s_load(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, libc.Uint64FromInt32(-libc.Int32FromInt32(1)), v8, fn, *(*uintptr)(unsafe.Pointer(bp + 40)))
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b1)).Fins) {
				break
			}
			i -= 16
			arg = i + 8
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargc) {
				ip--
				v10 = ip
				s_load(tls, qbe, *(*_Ref)(unsafe.Pointer(arg + 1*4)), libc.Uint64FromInt32(-libc.Int32FromInt32(1)), v10, fn, *(*uintptr)(unsafe.Pointer(bp + 40)))
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) {
				x = libc.Uint64FromInt32(1)<<x_loadsz(tls, qbe, i) - uint64(1)
				ip--
				v11 = ip
				s_load(tls, qbe, *(*_Ref)(unsafe.Pointer(arg)), x, v11, fn, *(*uintptr)(unsafe.Pointer(bp + 40)))
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
				x = libc.Uint64FromInt32(1)<<x_storesz(tls, qbe, i) - uint64(1)
				v12 = ip
				ip--
				s_store(tls, qbe, *(*_Ref)(unsafe.Pointer(arg + 1*4)), x, v12, i, fn, *(*uintptr)(unsafe.Pointer(bp + 40)))
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
				*(*_Ref)(unsafe.Pointer(bp + 8)) = *(*_Ref)(unsafe.Pointer(i + libc.UintptrFromInt32(1)*16 + 8))
				v13 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
				goto _14
			_14:
				sz = libc.Xabs(tls, v13)
				if sz >= int32(_NBit) {
					v15 = libc.Uint64FromInt32(-libc.Int32FromInt32(1))
				} else {
					v15 = libc.Uint64FromInt32(1)<<sz - uint64(1)
				}
				x = v15
				v16 = ip
				ip--
				s_store(tls, qbe, *(*_Ref)(unsafe.Pointer(arg + 1*4)), x, v16, i, fn, *(*uintptr)(unsafe.Pointer(bp + 40)))
				s_load(tls, qbe, *(*_Ref)(unsafe.Pointer(arg)), x, ip, fn, *(*uintptr)(unsafe.Pointer(bp + 40)))
				nbl++
				v17 = nbl
				x_vgrow(tls, qbe, bp+72, libc.Uint64FromInt32(v17))
				*(*uintptr)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 72)) + uintptr(nbl-int32(1))*8)) = i
			}
			goto _9
		_9:
		}
		*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 40))
		for {
			if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
				break
			}
			if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fl != 0 {
				s_radd(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 24))+24, ip)
				if (*_Blk)(unsafe.Pointer(b1)).Floop != -int32(1) {
					s_radd(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 24))+24, (*(*_Range)(unsafe.Pointer(br + uintptr((*_Blk)(unsafe.Pointer(b1)).Floop)*8))).Fb-int32(1))
				}
			}
			goto _18
		_18:
			;
			*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
		}
		(*(*_Range)(unsafe.Pointer(br + uintptr(n)*8))).Fa = ip
		goto _4
	_4:
		;
		n--
	}
	free(tls, qbe, br)
	/* kill dead stores */
	*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 40))
	for {
		if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
			break
		}
		n = 0
		for {
			if !(n < (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fnst) {
				break
			}
			if !(s_rin(tls, qbe, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr, (*(*_Store)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fst + uintptr(n)*16))).Fip) != 0) {
				i = (*(*_Store)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fst + uintptr(n)*16))).Fi
				if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
					*(*_Ins)(unsafe.Pointer(i + libc.UintptrFromInt32(1)*16)) = _Ins{
						F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
					}
				}
				*(*_Ins)(unsafe.Pointer(i)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
			}
			goto _20
		_20:
			;
			n++
		}
		goto _19
	_19:
		;
		*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
	}
	/* kill slots with an empty live range */
	total = uint32(0)
	freed = uint32(0)
	*(*uintptr)(unsafe.Pointer(bp + 96)) = x_vnew(tls, qbe, uint64(0), uint64(4), int32(_PHeap))
	n = 0
	v22 = *(*uintptr)(unsafe.Pointer(bp + 40))
	*(*uintptr)(unsafe.Pointer(bp + 32)) = v22
	*(*uintptr)(unsafe.Pointer(bp + 24)) = v22
	for {
		if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
			break
		}
		total += libc.Uint32FromInt32((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fsz)
		if !((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fb != 0) {
			x_vfree(tls, qbe, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fst)
			n++
			v23 = n
			x_vgrow(tls, qbe, bp+96, libc.Uint64FromInt32(v23))
			*(*int32)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 96)) + uintptr(n-int32(1))*4)) = (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Ft
			freed += libc.Uint32FromInt32((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fsz)
		} else {
			v24 = *(*uintptr)(unsafe.Pointer(bp + 32))
			*(*uintptr)(unsafe.Pointer(bp + 32)) += 56
			*(*_Slot)(unsafe.Pointer(v24)) = *(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24))))
		}
		goto _21
	_21:
		;
		*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
	}
	nsl = int32((int64(*(*uintptr)(unsafe.Pointer(bp + 32))) - int64(*(*uintptr)(unsafe.Pointer(bp + 40)))) / 56)
	if qbe.x_debug[int32('M')] != 0 {
		fputs(tls, qbe, __ccgo_ts+3426, libc.X__stderrp)
		if n != 0 {
			fputs(tls, qbe, __ccgo_ts+3447, libc.X__stderrp)
			m = 0
			for {
				if !(m < n) {
					break
				}
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3455, libc.VaList(bp+112, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(*(*int32)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 96)) + uintptr(m)*4)))*192))
				goto _25
			_25:
				;
				m++
			}
			fputs(tls, qbe, __ccgo_ts+267, libc.X__stderrp)
		}
	}
	for {
		v26 = n
		n--
		if !(v26 != 0) {
			break
		}
		t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(*(*int32)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 96)) + uintptr(n)*4)))*192
		i = (*_Tmp)(unsafe.Pointer(t)).Fdef
		if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) {
			libc.SetBitFieldPtr32Uint32(i+0, uint32(_Ocopy), 0, 0x3fffffff)
			*(*_Ref)(unsafe.Pointer(i + 8)) = _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(0)&0x1fffffff<<3,
			}
			continue
		}
		*(*_Ins)(unsafe.Pointer(i)) = _Ins{
			F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
		}
		u = (*_Tmp)(unsafe.Pointer(t)).Fuse
		for {
			if !(u < (*_Tmp)(unsafe.Pointer(t)).Fuse+uintptr((*_Tmp)(unsafe.Pointer(t)).Fnuse)*16) {
				break
			}
			if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UJmp) {
				b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*_Use)(unsafe.Pointer(u)).Fbid)*8))
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(_Jret0)
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = _Ref{}
				goto _27
			}
			i = *(*uintptr)(unsafe.Pointer(u + 8))
			v28 = (*_Ins)(unsafe.Pointer(i)).Fto
			v29 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v28
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v29
			v30 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _31
		_31:
			if !(v30 != 0) {
				n++
				v32 = n
				x_vgrow(tls, qbe, bp+96, libc.Uint64FromInt32(v32))
				*(*int32)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 96)) + uintptr(n-int32(1))*4)) = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
			} else {
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oarg) <= libc.Uint32FromInt32(int32(_Oargv)-int32(_Oarg)) {
					*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = _Ref{
						F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
					} /* crash */
				} else {
					if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
						*(*_Ins)(unsafe.Pointer(i + libc.UintptrFromInt32(1)*16)) = _Ins{
							F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
						}
					}
					*(*_Ins)(unsafe.Pointer(i)) = _Ins{
						F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
					}
				}
			}
			goto _27
		_27:
			;
			u += 16
		}
	}
	x_vfree(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 96)))
	/* fuse slots by decreasing size */
	qsort(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 40)), libc.Uint64FromInt32(nsl), uint64(56), __ccgo_fp(s_scmp))
	fused = uint32(0)
	n = 0
	for {
		if !(n < nsl) {
			break
		}
		*(*uintptr)(unsafe.Pointer(bp + 32)) = *(*uintptr)(unsafe.Pointer(bp + 40)) + uintptr(n)*56
		if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 32)))).Fs != 0 {
			goto _33
		}
		(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 32)))).Fs = *(*uintptr)(unsafe.Pointer(bp + 32))
		*(*_Range)(unsafe.Pointer(bp + 16)) = (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 32)))).Fr
		*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 32)) + uintptr(1)*56
		for {
			if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
				break
			}
			if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs != 0 || !((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fb != 0) {
				goto Skip
			}
			if s_rovlap(tls, qbe, *(*_Range)(unsafe.Pointer(bp + 16)), (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr) != 0 {
				/* O(n); can be approximated
				 * by 'goto Skip;' if need be
				 */
				m = n
				for {
					if !(*(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(m)*56 < *(*uintptr)(unsafe.Pointer(bp + 24))) {
						break
					}
					if (*(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 40)) + uintptr(m)*56))).Fs == *(*uintptr)(unsafe.Pointer(bp + 32)) {
						if s_rovlap(tls, qbe, (*(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 40)) + uintptr(m)*56))).Fr, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr) != 0 {
							goto Skip
						}
					}
					goto _35
				_35:
					;
					m++
				}
			}
			s_radd(tls, qbe, bp+16, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fa)
			s_radd(tls, qbe, bp+16, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fb-int32(1))
			(*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs = *(*uintptr)(unsafe.Pointer(bp + 32))
			fused += libc.Uint32FromInt32((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fsz)
			goto Skip
		Skip:
			;
			goto _34
		_34:
			;
			*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
		}
		goto _33
	_33:
		;
		n++
	}
	/* substitute fused slots */
	*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 40))
	for {
		if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
			break
		}
		t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Ft)*192
		/* the visit link is stale,
		 * reset it before the slot()
		 * calls below
		 */
		(*_Tmp)(unsafe.Pointer(t)).Fvisit = int32((int64(*(*uintptr)(unsafe.Pointer(bp + 24))) - int64(*(*uintptr)(unsafe.Pointer(bp + 40)))) / 56)
		if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs == *(*uintptr)(unsafe.Pointer(bp + 24)) {
			goto _36
		}
		*(*_Ins)(unsafe.Pointer((*_Tmp)(unsafe.Pointer(t)).Fdef)) = _Ins{
			F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
		}
		ts = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr((*_Slot)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs)).Ft)*192
		if (*_Tmp)(unsafe.Pointer(t)).Fdef < (*_Tmp)(unsafe.Pointer(ts)).Fdef {
			/* make sure the slot we
			 * selected has a def that
			 * dominates its new uses
			 */
			*(*_Ins)(unsafe.Pointer((*_Tmp)(unsafe.Pointer(t)).Fdef)) = *(*_Ins)(unsafe.Pointer((*_Tmp)(unsafe.Pointer(ts)).Fdef))
			*(*_Ins)(unsafe.Pointer((*_Tmp)(unsafe.Pointer(ts)).Fdef)) = _Ins{
				F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
			}
			(*_Tmp)(unsafe.Pointer(ts)).Fdef = (*_Tmp)(unsafe.Pointer(t)).Fdef
		}
		u = (*_Tmp)(unsafe.Pointer(t)).Fuse
		for {
			if !(u < (*_Tmp)(unsafe.Pointer(t)).Fuse+uintptr((*_Tmp)(unsafe.Pointer(t)).Fnuse)*16) {
				break
			}
			if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UJmp) {
				b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*_Use)(unsafe.Pointer(u)).Fbid)*8))
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32((*_Slot)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs)).Ft)&0x1fffffff<<3,
				}
				goto _37
			}
			arg = *(*uintptr)(unsafe.Pointer(u + 8)) + 8
			n = 0
			for {
				if !(n < int32(2)) {
					break
				}
				v39 = *(*_Ref)(unsafe.Pointer(arg + uintptr(n)*4))
				v40 = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Ft)&0x1fffffff<<3,
				}
				*(*_Ref)(unsafe.Pointer(bp)) = v39
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v40
				v41 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _42
			_42:
				if v41 != 0 {
					*(*_Ref)(unsafe.Pointer(arg + uintptr(n)*4)) = _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32((*_Slot)(unsafe.Pointer((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs)).Ft)&0x1fffffff<<3,
					}
				}
				goto _38
			_38:
				;
				n++
			}
			goto _37
		_37:
			;
			u += 16
		}
		goto _36
	_36:
		;
		*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
	}
	/* fix newly overlapping blits */
	n = 0
	for {
		if !(n < nbl) {
			break
		}
		i = *(*uintptr)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 72)) + uintptr(n)*8))
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
			if s_slot(tls, qbe, bp+24, bp+80, *(*_Ref)(unsafe.Pointer(i + 8)), fn, *(*uintptr)(unsafe.Pointer(bp + 40))) != 0 {
				if s_slot(tls, qbe, bp+32, bp+88, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), fn, *(*uintptr)(unsafe.Pointer(bp + 40))) != 0 {
					if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs == (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 32)))).Fs {
						if *(*_int64_t)(unsafe.Pointer(bp + 80)) < *(*_int64_t)(unsafe.Pointer(bp + 88)) {
							*(*_Ref)(unsafe.Pointer(bp + 8)) = *(*_Ref)(unsafe.Pointer(i + libc.UintptrFromInt32(1)*16 + 8))
							v44 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
							goto _45
						_45:
							sz = v44
							*(*_Ref)(unsafe.Pointer(i + libc.UintptrFromInt32(1)*16 + 8)) = _Ref{
								F__ccgo0: uint32(_RInt)&0x7<<0 | libc.Uint32FromInt32(-sz&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
							}
						} else {
							if *(*_int64_t)(unsafe.Pointer(bp + 80)) == *(*_int64_t)(unsafe.Pointer(bp + 88)) {
								*(*_Ins)(unsafe.Pointer(i)) = _Ins{
									F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
								}
								*(*_Ins)(unsafe.Pointer(i + libc.UintptrFromInt32(1)*16)) = _Ins{
									F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
								}
							}
						}
					}
				}
			}
		}
		goto _43
	_43:
		;
		n++
	}
	x_vfree(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 72)))
	if qbe.x_debug[int32('M')] != 0 {
		*(*uintptr)(unsafe.Pointer(bp + 32)) = *(*uintptr)(unsafe.Pointer(bp + 40))
		for {
			if !(*(*uintptr)(unsafe.Pointer(bp + 32)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
				break
			}
			if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 32)))).Fs != *(*uintptr)(unsafe.Pointer(bp + 32)) {
				goto _46
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3461, libc.VaList(bp+112, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 32)))).Fsz))
			*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 32))
			for {
				if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
					break
				}
				if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fs != *(*uintptr)(unsafe.Pointer(bp + 32)) {
					goto _47
				}
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3455, libc.VaList(bp+112, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr((*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Ft)*192))
				if (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fb != 0 {
					fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3477, libc.VaList(bp+112, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fa-ip, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fr.Fb-ip))
				} else {
					fputs(tls, qbe, __ccgo_ts+3485, libc.X__stderrp)
				}
				goto _47
			_47:
				;
				*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
			}
			fputs(tls, qbe, __ccgo_ts+267, libc.X__stderrp)
			goto _46
		_46:
			;
			*(*uintptr)(unsafe.Pointer(bp + 32)) += 56
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3488, libc.VaList(bp+112, freed, fused, total))
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
	*(*uintptr)(unsafe.Pointer(bp + 24)) = *(*uintptr)(unsafe.Pointer(bp + 40))
	for {
		if !(*(*uintptr)(unsafe.Pointer(bp + 24)) < *(*uintptr)(unsafe.Pointer(bp + 40))+uintptr(nsl)*56) {
			break
		}
		x_vfree(tls, qbe, (*_Slot)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))).Fst)
		goto _48
	_48:
		;
		*(*uintptr)(unsafe.Pointer(bp + 24)) += 56
	}
	x_vfree(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 40)))
}

/*===---- __stdarg_va_arg.h - Definitions of va_start, va_arg, va_end-------===
 *
 * Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
 * See https://llvm.org/LICENSE.txt for license information.
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
 *
 *===-----------------------------------------------------------------------===
 */

/* FIXME: This is using the placeholder dates Clang produces for these macros
   in C2x mode; switch to the correct values once they've been published. */
/* Versions before C2x do require the second parameter. */

/*===---- __stdarg___va_copy.h - Definition of __va_copy -------------------===
 *
 * Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
 * See https://llvm.org/LICENSE.txt for license information.
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
 *
 *===-----------------------------------------------------------------------===
 */

/*===---- __stdarg_va_copy.h - Definition of va_copy------------------------===
 *
 * Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
 * See https://llvm.org/LICENSE.txt for license information.
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
 *
 *===-----------------------------------------------------------------------===
 */

func s_adduse(tls *libc.TLS, qbe *_QBE, tmp uintptr, ty int32, b uintptr, va uintptr) {
	var ap _va_list
	var n int32
	var u, v2 uintptr
	var v1 _uint
	_, _, _, _, _ = ap, n, u, v1, v2
	if !((*_Tmp)(unsafe.Pointer(tmp)).Fuse != 0) {
		return
	}
	ap = va
	n = libc.Int32FromUint32((*_Tmp)(unsafe.Pointer(tmp)).Fnuse)
	v2 = tmp + 100
	*(*_uint)(unsafe.Pointer(v2))++
	v1 = *(*_uint)(unsafe.Pointer(v2))
	x_vgrow(tls, qbe, tmp+88, uint64(v1))
	u = (*_Tmp)(unsafe.Pointer(tmp)).Fuse + uintptr(n)*16
	(*_Use)(unsafe.Pointer(u)).Ftype1 = ty
	(*_Use)(unsafe.Pointer(u)).Fbid = (*_Blk)(unsafe.Pointer(b)).Fid
	switch ty {
	case int32(_UPhi):
		*(*uintptr)(unsafe.Pointer(u + 8)) = libc.VaUintptr(&ap)
	case int32(_UIns):
		*(*uintptr)(unsafe.Pointer(u + 8)) = libc.VaUintptr(&ap)
	case int32(_UJmp):
	default:
		_die_(tls, qbe, __ccgo_ts+3526, __ccgo_ts+3532, 0)
	}
	_ = ap
}

// C documentation
//
//	/* fill usage, width, phi, and class information
//	 * must not change .visit fields
//	 */
func x_filluse(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var a1 _uint
	var b1, i, p, tmp uintptr
	var m, t, tp, w, v10, v15, v21, v23, v28, v30, v8 int32
	var v13, v14, v18, v19, v20, v25, v26, v27, v5, v6, v7 _Ref
	var _ /* x at bp+12 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, b1, i, m, p, t, tmp, tp, w, v10, v13, v14, v15, v18, v19, v20, v21, v23, v25, v26, v27, v28, v30, v5, v6, v7, v8
	/* todo, is this the correct file? */
	tmp = (*_Fn)(unsafe.Pointer(fn)).Ftmp
	t = int32(_Tmp0)
	for {
		if !(t < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
			break
		}
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fdef = uintptr(0)
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fbid = -libc.Uint32FromUint32(1)
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fndef = uint32(0)
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fnuse = uint32(0)
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fcls = 0
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fphi = 0
		(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fwidth = int32(_WFull)
		if (*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fuse == uintptr(0) {
			(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fuse = x_vnew(tls, qbe, uint64(0), uint64(16), int32(_PFn))
		}
		goto _1
	_1:
		;
		t++
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			tp = int32(*(*uint32)(unsafe.Pointer(p + 0)) & 0xfffffff8 >> 3)
			(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(tp)*192))).Fbid = (*_Blk)(unsafe.Pointer(b1)).Fid
			(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(tp)*192))).Fndef++
			(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(tp)*192))).Fcls = int16((*_Phi)(unsafe.Pointer(p)).Fcls)
			tp = x_phicls(tls, qbe, tp, (*_Fn)(unsafe.Pointer(fn)).Ftmp)
			a1 = uint32(0)
			for {
				if !(a1 < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
					break
				}
				v5 = *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4))
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v5
				v6 = v5
				v7 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v6
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v7
				v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _9
			_9:
				if v8 != 0 {
					v10 = -int32(1)
					goto _11
				}
				v10 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _11
			_11:
				if v10 == int32(_RTmp) {
					t = int32(*(*uint32)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4 + 0)) & 0xfffffff8 >> 3)
					s_adduse(tls, qbe, tmp+uintptr(t)*192, int32(_UPhi), b1, libc.VaList(bp+24, p))
					t = x_phicls(tls, qbe, t, (*_Fn)(unsafe.Pointer(fn)).Ftmp)
					if t != tp {
						(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fphi = tp
					}
				}
				goto _4
			_4:
				;
				a1++
			}
			goto _3
		_3:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			v13 = (*_Ins)(unsafe.Pointer(i)).Fto
			v14 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v13
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v14
			v15 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _16
		_16:
			if !(v15 != 0) {
				w = int32(_WFull)
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oparsb) <= libc.Uint32FromInt32(int32(_Oparuh)-int32(_Oparsb)) {
					w = int32(_Wsb) + (int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - int32(_Oparsb))
				}
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) && int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Oload) {
					w = int32(_Wsb) + (int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - int32(_Oloadsb))
				}
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oextsb) <= libc.Uint32FromInt32(int32(_Oextuw)-int32(_Oextsb)) {
					w = int32(_Wsb) + (int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - int32(_Oextsb))
				}
				if x_iscmp(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0), bp+12, bp+12) != 0 {
					w = int32(_Wub)
				}
				if w == int32(_Wsw) || w == int32(_Wuw) {
					if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) == int32(_Kw) {
						w = int32(_WFull)
					}
				}
				t = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
				(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fwidth = w
				(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fdef = i
				(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fbid = (*_Blk)(unsafe.Pointer(b1)).Fid
				(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fndef++
				(*(*_Tmp)(unsafe.Pointer(tmp + uintptr(t)*192))).Fcls = int16(int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30))
			}
			m = 0
			for {
				if !(m < int32(2)) {
					break
				}
				v18 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(m)*4))
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v18
				v19 = v18
				v20 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v19
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v20
				v21 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _22
			_22:
				if v21 != 0 {
					v23 = -int32(1)
					goto _24
				}
				v23 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _24
			_24:
				if v23 == int32(_RTmp) {
					t = int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(m)*4 + 0)) & 0xfffffff8 >> 3)
					s_adduse(tls, qbe, tmp+uintptr(t)*192, int32(_UIns), b1, libc.VaList(bp+24, i))
				}
				goto _17
			_17:
				;
				m++
			}
			goto _12
		_12:
			;
			i += 16
		}
		v25 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v25
		v26 = v25
		v27 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v26
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v27
		v28 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _29
	_29:
		if v28 != 0 {
			v30 = -int32(1)
			goto _31
		}
		v30 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _31
	_31:
		if v30 == int32(_RTmp) {
			s_adduse(tls, qbe, tmp+uintptr(int32(*(*uint32)(unsafe.Pointer(b1 + 20 + 4 + 0))&0xfffffff8>>3))*192, int32(_UJmp), b1, 0)
		}
		goto _2
	_2:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
}

func s_refindex(tls *libc.TLS, qbe *_QBE, t int32, fn uintptr) (r _Ref) {
	return x_newtmp(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(t)*192, int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fcls), fn)
}

func s_phiins(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp1 := tls.Alloc(48)
	defer tls.Free(48)
	var a1, b1, be, blist, bp, i, p, use, v28, v38, v41, v51 uintptr
	var defb, n, v22, v25, v3, v40, v42, v45, v48 _uint
	var nt, ok, t, v12, v16, v20, v23, v26, v31, v35, v43, v46, v49, v8 int32
	var r, v10, v11, v14, v15, v18, v19, v29, v30, v33, v34, v6, v7 _Ref
	var v37 bool
	var _ /* defs at bp+24 */ [1]_BSet
	var _ /* k at bp+40 */ int16
	var _ /* u at bp+8 */ [1]_BSet
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, b1, be, blist, bp, defb, i, n, nt, ok, p, r, t, use, v10, v11, v12, v14, v15, v16, v18, v19, v20, v22, v23, v25, v26, v28, v29, v3, v30, v31, v33, v34, v35, v37, v38, v40, v41, v42, v43, v45, v46, v48, v49, v51, v6, v7, v8
	x_bsinit(tls, qbe, bp1+8, (*_Fn)(unsafe.Pointer(fn)).Fnblk)
	x_bsinit(tls, qbe, bp1+24, (*_Fn)(unsafe.Pointer(fn)).Fnblk)
	blist = x_emalloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(8))
	be = blist + uintptr((*_Fn)(unsafe.Pointer(fn)).Fnblk)*8
	nt = (*_Fn)(unsafe.Pointer(fn)).Fntmp
	t = int32(_Tmp0)
	for {
		if !(t < nt) {
			break
		}
		(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fvisit = 0
		if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fphi != 0 {
			goto _1
		}
		if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fndef == uint32(1) {
			ok = int32(1)
			defb = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fbid
			use = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fuse
			n = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fnuse
			for {
				v3 = n
				n--
				if !(v3 != 0) {
					break
				}
				ok &= libc.BoolInt32((*_Use)(unsafe.Pointer(use)).Fbid == defb)
				goto _2
			_2:
				;
				use += 16
			}
			if ok != 0 || defb == (*_Blk)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fstart)).Fid {
				goto _1
			}
		}
		x_bszero(tls, qbe, bp1+8)
		*(*int16)(unsafe.Pointer(bp1 + 40)) = int16(-int32(1))
		bp = be
		b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
		for {
			if !(b1 != 0) {
				break
			}
			(*_Blk)(unsafe.Pointer(b1)).Fvisit = uint32(0)
			r = _Ref{}
			i = (*_Blk)(unsafe.Pointer(b1)).Fins
			for {
				if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
					break
				}
				v6 = r
				v7 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v6
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v7
				v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _9
			_9:
				if !(v8 != 0) {
					v10 = *(*_Ref)(unsafe.Pointer(i + 8))
					v11 = _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
					}
					*(*_Ref)(unsafe.Pointer(bp1)) = v10
					*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v11
					v12 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
					goto _13
				_13:
					if v12 != 0 {
						*(*_Ref)(unsafe.Pointer(i + 8)) = r
					}
					v14 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
					v15 = _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
					}
					*(*_Ref)(unsafe.Pointer(bp1)) = v14
					*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v15
					v16 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
					goto _17
				_17:
					if v16 != 0 {
						*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = r
					}
				}
				v18 = (*_Ins)(unsafe.Pointer(i)).Fto
				v19 = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
				}
				*(*_Ref)(unsafe.Pointer(bp1)) = v18
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v19
				v20 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _21
			_21:
				if v20 != 0 {
					v22 = libc.Uint32FromInt32(t)
					v23 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1+136)).Ft + uintptr(v22/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v22%uint32(_NBit))) != uint64(0))
					goto _24
				_24:
					if !(v23 != 0) {
						r = s_refindex(tls, qbe, t, fn)
						(*_Ins)(unsafe.Pointer(i)).Fto = r
					} else {
						v25 = (*_Blk)(unsafe.Pointer(b1)).Fid
						v26 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+8)).Ft + uintptr(v25/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v25%uint32(_NBit))) != uint64(0))
						goto _27
					_27:
						if !(v26 != 0) {
							x_bsset(tls, qbe, bp1+8, (*_Blk)(unsafe.Pointer(b1)).Fid)
							bp -= 8
							v28 = bp
							*(*uintptr)(unsafe.Pointer(v28)) = b1
						}
						if x_clsmerge(tls, qbe, bp1+40, int16(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30))) != 0 {
							_die_(tls, qbe, __ccgo_ts+3526, __ccgo_ts+3544, 0)
						}
					}
				}
				goto _5
			_5:
				;
				i += 16
			}
			v29 = r
			v30 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp1)) = v29
			*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v30
			v31 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
			goto _32
		_32:
			;
			if v37 = !(v31 != 0); v37 {
				v33 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
				v34 = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
				}
				*(*_Ref)(unsafe.Pointer(bp1)) = v33
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v34
				v35 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _36
			_36:
			}
			if v37 && v35 != 0 {
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = r
			}
			goto _4
		_4:
			;
			b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
		}
		x_bscopy(tls, qbe, bp1+24, bp1+8)
		for bp != be {
			(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fvisit = t
			v38 = bp
			bp += 8
			b1 = *(*uintptr)(unsafe.Pointer(v38))
			x_bsclr(tls, qbe, bp1+8, (*_Blk)(unsafe.Pointer(b1)).Fid)
			n = uint32(0)
			for {
				if !(n < (*_Blk)(unsafe.Pointer(b1)).Fnfron) {
					break
				}
				a1 = *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b1)).Ffron + uintptr(n)*8))
				v41 = a1 + 60
				v40 = *(*_uint)(unsafe.Pointer(v41))
				*(*_uint)(unsafe.Pointer(v41))++
				if v40 == uint32(0) {
					v42 = libc.Uint32FromInt32(t)
					v43 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(a1+120)).Ft + uintptr(v42/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v42%uint32(_NBit))) != uint64(0))
					goto _44
				_44:
					if v43 != 0 {
						p = x_alloc(tls, qbe, uint64(40))
						(*_Phi)(unsafe.Pointer(p)).Fcls = int32(*(*int16)(unsafe.Pointer(bp1 + 40)))
						(*_Phi)(unsafe.Pointer(p)).Fto = _Ref{
							F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
						}
						(*_Phi)(unsafe.Pointer(p)).Flink = (*_Blk)(unsafe.Pointer(a1)).Fphi
						(*_Phi)(unsafe.Pointer(p)).Farg = x_vnew(tls, qbe, uint64(0), uint64(4), int32(_PFn))
						(*_Phi)(unsafe.Pointer(p)).Fblk = x_vnew(tls, qbe, uint64(0), uint64(8), int32(_PFn))
						(*_Blk)(unsafe.Pointer(a1)).Fphi = p
						v45 = (*_Blk)(unsafe.Pointer(a1)).Fid
						v46 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+24)).Ft + uintptr(v45/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v45%uint32(_NBit))) != uint64(0))
						goto _47
					_47:
						if !(v46 != 0) {
							v48 = (*_Blk)(unsafe.Pointer(a1)).Fid
							v49 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+8)).Ft + uintptr(v48/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v48%uint32(_NBit))) != uint64(0))
							goto _50
						_50:
							if !(v49 != 0) {
								x_bsset(tls, qbe, bp1+8, (*_Blk)(unsafe.Pointer(a1)).Fid)
								bp -= 8
								v51 = bp
								*(*uintptr)(unsafe.Pointer(v51)) = a1
							}
						}
					}
				}
				goto _39
			_39:
				;
				n++
			}
		}
		goto _1
	_1:
		;
		t++
	}
	free(tls, qbe, blist)
}

type _Name = struct {
	Fr  _Ref
	Fb  uintptr
	Fup uintptr
}

func s_nnew(tls *libc.TLS, qbe *_QBE, r _Ref, b uintptr, up uintptr) (r1 uintptr) {
	var n uintptr
	_ = n
	if qbe.s_namel != 0 {
		n = qbe.s_namel
		qbe.s_namel = (*_Name)(unsafe.Pointer(n)).Fup
	} else {
		/* could use alloc, here
		 * but namel should be reset
		 */
		n = x_emalloc(tls, qbe, uint64(24))
	}
	(*_Name)(unsafe.Pointer(n)).Fr = r
	(*_Name)(unsafe.Pointer(n)).Fb = b
	(*_Name)(unsafe.Pointer(n)).Fup = up
	return n
}

func s_nfree(tls *libc.TLS, qbe *_QBE, n uintptr) {
	(*_Name)(unsafe.Pointer(n)).Fup = qbe.s_namel
	qbe.s_namel = n
}

func s_rendef(tls *libc.TLS, qbe *_QBE, r uintptr, b1 uintptr, stk uintptr, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var t, v3 int32
	var v1, v2 _Ref
	var _ /* r1 at bp+8 */ _Ref
	_, _, _, _ = t, v1, v2, v3
	t = int32(*(*uint32)(unsafe.Pointer(r + 0)) & 0xfffffff8 >> 3)
	v1 = *(*_Ref)(unsafe.Pointer(r))
	v2 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v1
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
	v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _4
_4:
	;
	if v3 != 0 || !((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fvisit != 0) {
		return
	}
	*(*_Ref)(unsafe.Pointer(bp + 8)) = s_refindex(tls, qbe, t, fn)
	(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3))*192))).Fvisit = t
	*(*uintptr)(unsafe.Pointer(stk + uintptr(t)*8)) = s_nnew(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 8)), b1, *(*uintptr)(unsafe.Pointer(stk + uintptr(t)*8)))
	*(*_Ref)(unsafe.Pointer(r)) = *(*_Ref)(unsafe.Pointer(bp + 8))
}

func s_getstk(tls *libc.TLS, qbe *_QBE, t int32, b uintptr, stk uintptr) (r _Ref) {
	var n, n1 uintptr
	_, _ = n, n1
	n = *(*uintptr)(unsafe.Pointer(stk + uintptr(t)*8))
	for n != 0 && !(x_dom(tls, qbe, (*_Name)(unsafe.Pointer(n)).Fb, b) != 0) {
		n1 = n
		n = (*_Name)(unsafe.Pointer(n)).Fup
		s_nfree(tls, qbe, n1)
	}
	*(*uintptr)(unsafe.Pointer(stk + uintptr(t)*8)) = n
	if !(n != 0) {
		/* uh, oh, warn */
		return _Ref{
			F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(0)&0x1fffffff<<3,
		}
	} else {
		return (*_Name)(unsafe.Pointer(n)).Fr
	}
	return r
}

func s_renblk(tls *libc.TLS, qbe *_QBE, b1 uintptr, stk uintptr, fn uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var i, p, ps, s, v18, v20, v24 uintptr
	var m, t, v14, v16, v22, v7, v9 int32
	var v11, v12, v13, v4, v5, v6 _Ref
	var v23 _uint
	var _ /* succ at bp+16 */ [3]uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = i, m, p, ps, s, t, v11, v12, v13, v14, v16, v18, v20, v22, v23, v24, v4, v5, v6, v7, v9
	p = (*_Blk)(unsafe.Pointer(b1)).Fphi
	for {
		if !(p != 0) {
			break
		}
		s_rendef(tls, qbe, p, b1, stk, fn)
		goto _1
	_1:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
	i = (*_Blk)(unsafe.Pointer(b1)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
			break
		}
		m = 0
		for {
			if !(m < int32(2)) {
				break
			}
			t = int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(m)*4 + 0)) & 0xfffffff8 >> 3)
			v4 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(m)*4))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v4
			v5 = v4
			v6 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v5
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
			v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _8
		_8:
			if v7 != 0 {
				v9 = -int32(1)
				goto _10
			}
			v9 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _10
		_10:
			if v9 == int32(_RTmp) {
				if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fvisit != 0 {
					*(*_Ref)(unsafe.Pointer(i + 8 + uintptr(m)*4)) = s_getstk(tls, qbe, t, b1, stk)
				}
			}
			goto _3
		_3:
			;
			m++
		}
		s_rendef(tls, qbe, i+4, b1, stk, fn)
		goto _2
	_2:
		;
		i += 16
	}
	t = int32(*(*uint32)(unsafe.Pointer(b1 + 20 + 4 + 0)) & 0xfffffff8 >> 3)
	v11 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v11
	v12 = v11
	v13 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v12
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v13
	v14 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _15
_15:
	if v14 != 0 {
		v16 = -int32(1)
		goto _17
	}
	v16 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _17
_17:
	if v16 == int32(_RTmp) {
		if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fvisit != 0 {
			(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = s_getstk(tls, qbe, t, b1, stk)
		}
	}
	(*(*[3]uintptr)(unsafe.Pointer(bp + 16)))[0] = (*_Blk)(unsafe.Pointer(b1)).Fs1
	if (*_Blk)(unsafe.Pointer(b1)).Fs2 == (*_Blk)(unsafe.Pointer(b1)).Fs1 {
		v18 = uintptr(0)
	} else {
		v18 = (*_Blk)(unsafe.Pointer(b1)).Fs2
	}
	(*(*[3]uintptr)(unsafe.Pointer(bp + 16)))[int32(1)] = v18
	(*(*[3]uintptr)(unsafe.Pointer(bp + 16)))[int32(2)] = uintptr(0)
	ps = bp + 16
	for {
		v20 = *(*uintptr)(unsafe.Pointer(ps))
		s = v20
		if !(v20 != 0) {
			break
		}
		p = (*_Blk)(unsafe.Pointer(s)).Fphi
		for {
			if !(p != 0) {
				break
			}
			t = int32(*(*uint32)(unsafe.Pointer(p + 0)) & 0xfffffff8 >> 3)
			v22 = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Fvisit
			t = v22
			if v22 != 0 {
				v24 = p + 24
				v23 = *(*_uint)(unsafe.Pointer(v24))
				*(*_uint)(unsafe.Pointer(v24))++
				m = libc.Int32FromUint32(v23)
				x_vgrow(tls, qbe, p+8, uint64((*_Phi)(unsafe.Pointer(p)).Fnarg))
				x_vgrow(tls, qbe, p+16, uint64((*_Phi)(unsafe.Pointer(p)).Fnarg))
				*(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(m)*4)) = s_getstk(tls, qbe, t, b1, stk)
				*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(m)*8)) = b1
			}
			goto _21
		_21:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		goto _19
	_19:
		;
		ps += 8
	}
	s = (*_Blk)(unsafe.Pointer(b1)).Fdom
	for {
		if !(s != 0) {
			break
		}
		s_renblk(tls, qbe, s, stk, fn)
		goto _25
	_25:
		;
		s = (*_Blk)(unsafe.Pointer(s)).Fdlink
	}
}

// C documentation
//
//	/* require rpo and use */
func x_ssa(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var b, b1, n, stk, v4 uintptr
	var d, nt, v3 int32
	_, _, _, _, _, _, _, _ = b, b1, d, n, nt, stk, v3, v4
	nt = (*_Fn)(unsafe.Pointer(fn)).Fntmp
	stk = x_emalloc(tls, qbe, libc.Uint64FromInt32(nt)*uint64(8))
	d = int32(qbe.x_debug[int32('L')])
	qbe.x_debug[int32('L')] = 0
	x_filldom(tls, qbe, fn)
	if qbe.x_debug[int32('N')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3558, 0)
		b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
		for {
			if !(b1 != 0) {
				break
			}
			if !((*_Blk)(unsafe.Pointer(b1)).Fdom != 0) {
				goto _1
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3574, libc.VaList(bp+8, b1+180))
			b = (*_Blk)(unsafe.Pointer(b1)).Fdom
			for {
				if !(b != 0) {
					break
				}
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+263, libc.VaList(bp+8, b+180))
				goto _2
			_2:
				;
				b = (*_Blk)(unsafe.Pointer(b)).Fdlink
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+82, 0)
			goto _1
		_1:
			;
			b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
		}
	}
	x_fillfron(tls, qbe, fn)
	x_filllive(tls, qbe, fn)
	s_phiins(tls, qbe, fn)
	s_renblk(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Fstart, stk, fn)
	for {
		v3 = nt
		nt--
		if !(v3 != 0) {
			break
		}
		for {
			v4 = *(*uintptr)(unsafe.Pointer(stk + uintptr(nt)*8))
			n = v4
			if !(v4 != 0) {
				break
			}
			*(*uintptr)(unsafe.Pointer(stk + uintptr(nt)*8)) = (*_Name)(unsafe.Pointer(n)).Fup
			s_nfree(tls, qbe, n)
		}
	}
	qbe.x_debug[int32('L')] = int8(d)
	free(tls, qbe, stk)
	if qbe.x_debug[int32('N')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3580, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

func s_phicheck(tls *libc.TLS, qbe *_QBE, p uintptr, b1 uintptr, t _Ref) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var b11 uintptr
	var n _uint
	var v2, v3 _Ref
	var v4 int32
	_, _, _, _, _ = b11, n, v2, v3, v4
	n = uint32(0)
	for {
		if !(n < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
			break
		}
		v2 = *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(n)*4))
		v3 = t
		*(*_Ref)(unsafe.Pointer(bp)) = v2
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
		v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _5
	_5:
		if v4 != 0 {
			b11 = *(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8))
			if b11 != b1 && !(x_sdom(tls, qbe, b1, b11) != 0) {
				return int32(1)
			}
		}
		goto _1
	_1:
		;
		n++
	}
	return 0
}

// C documentation
//
//	/* require use and ssa */
func x_ssacheck(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var b1, bu, i, p, t, u uintptr
	var v11, v9 int32
	var v6, v7, v8 _Ref
	var _ /* r at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _ = b1, bu, i, p, t, u, v11, v6, v7, v8, v9
	t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(_Tmp0)*192
	for {
		if !((int64(t)-int64((*_Fn)(unsafe.Pointer(fn)).Ftmp))/192 < int64((*_Fn)(unsafe.Pointer(fn)).Fntmp)) {
			break
		}
		if (*_Tmp)(unsafe.Pointer(t)).Fndef > uint32(1) {
			_err(tls, qbe, __ccgo_ts+3608, libc.VaList(bp+24, t))
		}
		if (*_Tmp)(unsafe.Pointer(t)).Fnuse > uint32(0) && (*_Tmp)(unsafe.Pointer(t)).Fndef == uint32(0) {
			bu = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*(*_Use)(unsafe.Pointer((*_Tmp)(unsafe.Pointer(t)).Fuse))).Fbid)*8))
			goto Err
		}
		goto _1
	_1:
		;
		t += 192
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			*(*_Ref)(unsafe.Pointer(bp + 12)) = (*_Phi)(unsafe.Pointer(p)).Fto
			t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192
			u = (*_Tmp)(unsafe.Pointer(t)).Fuse
			for {
				if !(u < (*_Tmp)(unsafe.Pointer(t)).Fuse+uintptr((*_Tmp)(unsafe.Pointer(t)).Fnuse)*16) {
					break
				}
				bu = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*_Use)(unsafe.Pointer(u)).Fbid)*8))
				if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UPhi) {
					if s_phicheck(tls, qbe, *(*uintptr)(unsafe.Pointer(u + 8)), b1, *(*_Ref)(unsafe.Pointer(bp + 12))) != 0 {
						goto Err
					}
				} else {
					if bu != b1 && !(x_sdom(tls, qbe, b1, bu) != 0) {
						goto Err
					}
				}
				goto _4
			_4:
				;
				u += 16
			}
			goto _3
		_3:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			v6 = (*_Ins)(unsafe.Pointer(i)).Fto
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v6
			v7 = v6
			v8 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v7
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v8
			v9 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _10
		_10:
			if v9 != 0 {
				v11 = -int32(1)
				goto _12
			}
			v11 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _12
		_12:
			if v11 != int32(_RTmp) {
				goto _5
			}
			*(*_Ref)(unsafe.Pointer(bp + 12)) = (*_Ins)(unsafe.Pointer(i)).Fto
			t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192
			u = (*_Tmp)(unsafe.Pointer(t)).Fuse
			for {
				if !(u < (*_Tmp)(unsafe.Pointer(t)).Fuse+uintptr((*_Tmp)(unsafe.Pointer(t)).Fnuse)*16) {
					break
				}
				bu = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*_Use)(unsafe.Pointer(u)).Fbid)*8))
				if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UPhi) {
					if s_phicheck(tls, qbe, *(*uintptr)(unsafe.Pointer(u + 8)), b1, *(*_Ref)(unsafe.Pointer(bp + 12))) != 0 {
						goto Err
					}
				} else {
					if bu == b1 {
						if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UIns) {
							if *(*uintptr)(unsafe.Pointer(u + 8)) <= i {
								goto Err
							}
						}
					} else {
						if !(x_sdom(tls, qbe, b1, bu) != 0) {
							goto Err
						}
					}
				}
				goto _13
			_13:
				;
				u += 16
			}
			goto _5
		_5:
			;
			i += 16
		}
		goto _2
	_2:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	return
	goto Err
Err:
	;
	if (*_Tmp)(unsafe.Pointer(t)).Fvisit != 0 {
		_die_(tls, qbe, __ccgo_ts+3526, __ccgo_ts+3650, libc.VaList(bp+24, t))
	} else {
		_err(tls, qbe, __ccgo_ts+3678, libc.VaList(bp+24, t, bu+180))
	}
}

func x_getalias(tls *libc.TLS, qbe *_QBE, a1 uintptr, _r _Ref, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var c uintptr
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _ = c, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	switch v6 {
	default:
		_die_(tls, qbe, __ccgo_ts+3722, __ccgo_ts+3532, 0)
		fallthrough
	case int32(_RTmp):
		*(*_Alias)(unsafe.Pointer(a1)) = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))).Falias
		if (*_Alias)(unsafe.Pointer(a1)).Ftype1&int32(1) != 0 {
			(*_Alias)(unsafe.Pointer(a1)).Ftype1 = (*_Alias)(unsafe.Pointer((*_Alias)(unsafe.Pointer(a1)).Fslot)).Ftype1
		}
	case int32(_RCon):
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
		if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CAddr) {
			(*_Alias)(unsafe.Pointer(a1)).Ftype1 = int32(_ASym)
			(*_Alias)(unsafe.Pointer(a1)).Fu.Fsym = (*_Con)(unsafe.Pointer(c)).Fsym
		} else {
			(*_Alias)(unsafe.Pointer(a1)).Ftype1 = int32(_ACon)
		}
		(*_Alias)(unsafe.Pointer(a1)).Foffset = *(*_int64_t)(unsafe.Pointer(c + 16))
		(*_Alias)(unsafe.Pointer(a1)).Fslot = uintptr(0)
		break
	}
}

func x_alias(tls *libc.TLS, qbe *_QBE, p _Ref, op int32, sp int32, q _Ref, sq int32, delta uintptr, fn uintptr) (r int32) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var ovlap int32
	var _ /* ap at bp+0 */ _Alias
	var _ /* aq at bp+40 */ _Alias
	_ = ovlap
	x_getalias(tls, qbe, bp, p, fn)
	x_getalias(tls, qbe, bp+40, q, fn)
	(*(*_Alias)(unsafe.Pointer(bp))).Foffset += int64(op)
	/* when delta is meaningful (ovlap == 1),
	 * we do not overflow int because sp and
	 * sq are bounded by 2^28 */
	*(*int32)(unsafe.Pointer(delta)) = int32((*(*_Alias)(unsafe.Pointer(bp))).Foffset - (*(*_Alias)(unsafe.Pointer(bp + 40))).Foffset)
	ovlap = libc.BoolInt32((*(*_Alias)(unsafe.Pointer(bp))).Foffset < (*(*_Alias)(unsafe.Pointer(bp + 40))).Foffset+int64(sq) && (*(*_Alias)(unsafe.Pointer(bp + 40))).Foffset < (*(*_Alias)(unsafe.Pointer(bp))).Foffset+int64(sp))
	if (*(*_Alias)(unsafe.Pointer(bp))).Ftype1&int32(1) != 0 && (*(*_Alias)(unsafe.Pointer(bp + 40))).Ftype1&int32(1) != 0 {
		/* if both are offsets of the same
		 * stack slot, they alias iif they
		 * overlap */
		if (*(*_Alias)(unsafe.Pointer(bp))).Fbase == (*(*_Alias)(unsafe.Pointer(bp + 40))).Fbase && ovlap != 0 {
			return int32(_MustAlias)
		}
		return int32(_NoAlias)
	}
	if (*(*_Alias)(unsafe.Pointer(bp))).Ftype1 == int32(_ASym) && (*(*_Alias)(unsafe.Pointer(bp + 40))).Ftype1 == int32(_ASym) {
		/* they conservatively alias if the
		 * symbols are different, or they
		 * alias for sure if they overlap */
		if !(x_symeq(tls, qbe, *(*_Sym)(unsafe.Pointer(bp + 16)), *(*_Sym)(unsafe.Pointer(bp + 40 + 16))) != 0) {
			return int32(_MayAlias)
		}
		if ovlap != 0 {
			return int32(_MustAlias)
		}
		return int32(_NoAlias)
	}
	if (*(*_Alias)(unsafe.Pointer(bp))).Ftype1 == int32(_ACon) && (*(*_Alias)(unsafe.Pointer(bp + 40))).Ftype1 == int32(_ACon) || (*(*_Alias)(unsafe.Pointer(bp))).Ftype1 == (*(*_Alias)(unsafe.Pointer(bp + 40))).Ftype1 && (*(*_Alias)(unsafe.Pointer(bp))).Fbase == (*(*_Alias)(unsafe.Pointer(bp + 40))).Fbase {
		/* if they have the same base, we
		 * can rely on the offsets only */
		if ovlap != 0 {
			return int32(_MustAlias)
		}
		return int32(_NoAlias)
	}
	/* if one of the two is unknown
	 * there may be aliasing unless
	 * the other is provably local */
	if (*(*_Alias)(unsafe.Pointer(bp))).Ftype1 == int32(_AUnk) && (*(*_Alias)(unsafe.Pointer(bp + 40))).Ftype1 != int32(_ALoc) {
		return int32(_MayAlias)
	}
	if (*(*_Alias)(unsafe.Pointer(bp + 40))).Ftype1 == int32(_AUnk) && (*(*_Alias)(unsafe.Pointer(bp))).Ftype1 != int32(_ALoc) {
		return int32(_MayAlias)
	}
	return int32(_NoAlias)
}

func x_escapes(tls *libc.TLS, qbe *_QBE, _r _Ref, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var a1 uintptr
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _ = a1, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RTmp) {
		return int32(1)
	}
	a1 = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192 + 144
	return libc.BoolInt32(!((*_Alias)(unsafe.Pointer(a1)).Ftype1&libc.Int32FromInt32(1) != 0) || (*_Alias)(unsafe.Pointer((*_Alias)(unsafe.Pointer(a1)).Fslot)).Ftype1 == int32(_AEsc))
}

func s_esc(tls *libc.TLS, qbe *_QBE, _r _Ref, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var a1 uintptr
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _ = a1, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		a1 = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192 + 144
		if (*_Alias)(unsafe.Pointer(a1)).Ftype1&int32(1) != 0 {
			(*_Alias)(unsafe.Pointer((*_Alias)(unsafe.Pointer(a1)).Fslot)).Ftype1 = int32(_AEsc)
		}
	}
}

func s_store1(tls *libc.TLS, qbe *_QBE, _r _Ref, sz int32, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var a1 uintptr
	var m _bits
	var off _int64_t
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _, _, _ = a1, m, off, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		a1 = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192 + 144
		if (*_Alias)(unsafe.Pointer(a1)).Fslot != 0 {
			off = (*_Alias)(unsafe.Pointer(a1)).Foffset
			if sz >= int32(_NBit) || (off < 0 || off >= int64(_NBit)) {
				m = libc.Uint64FromInt32(-libc.Int32FromInt32(1))
			} else {
				m = (libc.Uint64FromInt32(1)<<sz - uint64(1)) << off
			}
			(*(*struct {
				Fsz int32
				Fm  _bits
			})(unsafe.Pointer((*_Alias)(unsafe.Pointer(a1)).Fslot + 16))).Fm |= m
		}
	}
}

func x_fillalias(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var a1, b1, c, i, p uintptr
	var n _uint
	var sz, t, v12, v14, v18, v20, v7 int32
	var x _int64_t
	var v10, v11, v16, v17, v5, v6, v9 _Ref
	var _ /* a0 at bp+16 */ _Alias
	var _ /* a1 at bp+56 */ _Alias
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, b1, c, i, n, p, sz, t, x, v10, v11, v12, v14, v16, v17, v18, v20, v5, v6, v7, v9
	t = 0
	for {
		if !(t < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
			break
		}
		(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192))).Falias.Ftype1 = int32(_ABot)
		goto _1
	_1:
		;
		t++
	}
	n = uint32(0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			a1 = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*192 + 144
			(*_Alias)(unsafe.Pointer(a1)).Ftype1 = int32(_AUnk)
			(*_Alias)(unsafe.Pointer(a1)).Fbase = int32(*(*uint32)(unsafe.Pointer(p + 0)) & 0xfffffff8 >> 3)
			(*_Alias)(unsafe.Pointer(a1)).Foffset = 0
			(*_Alias)(unsafe.Pointer(a1)).Fslot = uintptr(0)
			goto _3
		_3:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			a1 = uintptr(0)
			v5 = (*_Ins)(unsafe.Pointer(i)).Fto
			v6 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v5
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
			v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _8
		_8:
			if !(v7 != 0) {
				a1 = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192 + 144
				if int32(_Oalloc) <= int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) && int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) <= int32(_Oalloc1) {
					(*_Alias)(unsafe.Pointer(a1)).Ftype1 = int32(_ALoc)
					(*_Alias)(unsafe.Pointer(a1)).Fslot = a1
					(*(*struct {
						Fsz int32
						Fm  _bits
					})(unsafe.Pointer(a1 + 16))).Fsz = -int32(1)
					v9 = *(*_Ref)(unsafe.Pointer(i + 8))
					*(*_Ref)(unsafe.Pointer(bp + 8)) = v9
					v10 = v9
					v11 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp)) = v10
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v11
					v12 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _13
				_13:
					if v12 != 0 {
						v14 = -int32(1)
						goto _15
					}
					v14 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
					goto _15
				_15:
					if v14 == int32(_RCon) {
						c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32
						x = *(*_int64_t)(unsafe.Pointer(c + 16))
						if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CBits) {
							if 0 <= x && x <= int64(_NBit) {
								(*(*struct {
									Fsz int32
									Fm  _bits
								})(unsafe.Pointer(a1 + 16))).Fsz = int32(x)
							}
						}
					}
				} else {
					(*_Alias)(unsafe.Pointer(a1)).Ftype1 = int32(_AUnk)
					(*_Alias)(unsafe.Pointer(a1)).Fslot = uintptr(0)
				}
				(*_Alias)(unsafe.Pointer(a1)).Fbase = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
				(*_Alias)(unsafe.Pointer(a1)).Foffset = 0
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ocopy) {
				x_getalias(tls, qbe, a1, *(*_Ref)(unsafe.Pointer(i + 8)), fn)
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oadd) {
				x_getalias(tls, qbe, bp+16, *(*_Ref)(unsafe.Pointer(i + 8)), fn)
				x_getalias(tls, qbe, bp+56, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), fn)
				if (*(*_Alias)(unsafe.Pointer(bp + 16))).Ftype1 == int32(_ACon) {
					*(*_Alias)(unsafe.Pointer(a1)) = *(*_Alias)(unsafe.Pointer(bp + 56))
					*(*_int64_t)(unsafe.Pointer(a1 + 8)) += (*(*_Alias)(unsafe.Pointer(bp + 16))).Foffset
				} else {
					if (*(*_Alias)(unsafe.Pointer(bp + 56))).Ftype1 == int32(_ACon) {
						*(*_Alias)(unsafe.Pointer(a1)) = *(*_Alias)(unsafe.Pointer(bp + 16))
						*(*_int64_t)(unsafe.Pointer(a1 + 8)) += (*(*_Alias)(unsafe.Pointer(bp + 56))).Foffset
					}
				}
			}
			v16 = (*_Ins)(unsafe.Pointer(i)).Fto
			v17 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v16
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v17
			v18 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _19
		_19:
			;
			if v18 != 0 || (*_Alias)(unsafe.Pointer(a1)).Ftype1 == int32(_AUnk) {
				if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Oblit0) {
					if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb))) {
						s_esc(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), fn)
					}
					if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb))) {
						if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Oargc) {
							s_esc(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), fn)
						}
					}
				}
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
				i += 16
				*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8))
				v20 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
				goto _21
			_21:
				sz = libc.Xabs(tls, v20)
				s_store1(tls, qbe, *(*_Ref)(unsafe.Pointer(i - libc.UintptrFromInt32(1)*16 + 8 + 1*4)), sz, fn)
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
				s_store1(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), x_storesz(tls, qbe, i), fn)
			}
			goto _4
		_4:
			;
			i += 16
		}
		if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) != int32(_Jretc) {
			s_esc(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, fn)
		}
		goto _2
	_2:
		;
		n++
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			n = uint32(0)
			for {
				if !(n < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
					break
				}
				s_esc(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(n)*4)), fn)
				goto _24
			_24:
				;
				n++
			}
			goto _23
		_23:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		goto _22
	_22:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
}

type _Loc = struct {
	Ftype1 int32
	Foff   _uint
	Fblk   uintptr
}

const /* right above the original load */
_LLoad = 1

const /* inserting a load is allowed */
_LNoLoad = 2

type _Slice = struct {
	Fref _Ref
	Foff int32
	Fsz  int16
	Fcls int16
}

type _Insert = struct {
	F__ccgo0 uint32
	Fbid     _uint
	Foff     _uint
	Fnew1    struct {
		Fphi [0]struct {
			Fm _Slice
			Fp uintptr
		}
		Fins         _Ins
		F__ccgo_pad2 [8]byte
	}
}

func x_loadsz(tls *libc.TLS, qbe *_QBE, l uintptr) (r int32) {
	var v1 int32
	_ = v1
	switch int32(*(*uint32)(unsafe.Pointer(l + 0)) & 0x3fffffff >> 0) {
	case int32(_Oloadsb):
		fallthrough
	case int32(_Oloadub):
		return int32(1)
	case int32(_Oloadsh):
		fallthrough
	case int32(_Oloaduh):
		return int32(2)
	case int32(_Oloadsw):
		fallthrough
	case int32(_Oloaduw):
		return int32(4)
	case int32(_Oload):
		if int32(*(*uint32)(unsafe.Pointer(l + 0))&0xc0000000>>30)&int32(1) != 0 {
			v1 = int32(8)
		} else {
			v1 = int32(4)
		}
		return v1
	}
	_die_(tls, qbe, __ccgo_ts+3730, __ccgo_ts+3532, 0)
	return r
}

func x_storesz(tls *libc.TLS, qbe *_QBE, s uintptr) (r int32) {
	switch int32(*(*uint32)(unsafe.Pointer(s + 0)) & 0x3fffffff >> 0) {
	case int32(_Ostoreb):
		return int32(1)
	case int32(_Ostoreh):
		return int32(2)
	case int32(_Ostorew):
		fallthrough
	case int32(_Ostores):
		return int32(4)
	case int32(_Ostorel):
		fallthrough
	case int32(_Ostored):
		return int32(8)
	}
	_die_(tls, qbe, __ccgo_ts+3730, __ccgo_ts+3532, 0)
	return r
}

func s_iins(tls *libc.TLS, qbe *_QBE, cls int32, op int32, a0 _Ref, a1 _Ref, l uintptr) (r _Ref) {
	var ist uintptr
	var v1, v2 _uint
	var v3 _Ref
	_, _, _, _ = ist, v1, v2, v3
	qbe.s_nlog++
	v1 = qbe.s_nlog
	x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_ilog)), uint64(v1))
	ist = qbe.s_ilog + uintptr(qbe.s_nlog-uint32(1))*40
	libc.SetBitFieldPtr32Uint32(ist+0, libc.Uint32FromInt32(0), 0, 0x1)
	v2 = qbe.s_inum
	qbe.s_inum++
	libc.SetBitFieldPtr32Uint32(ist+0, v2, 1, 0xfffffffe)
	(*_Insert)(unsafe.Pointer(ist)).Fbid = (*_Blk)(unsafe.Pointer((*_Loc)(unsafe.Pointer(l)).Fblk)).Fid
	(*_Insert)(unsafe.Pointer(ist)).Foff = (*_Loc)(unsafe.Pointer(l)).Foff
	(*_Insert)(unsafe.Pointer(ist)).Fnew1.Fins = _Ins{
		F__ccgo0: libc.Uint32FromInt32(op)&0x3fffffff<<0 | libc.Uint32FromInt32(cls)&0x3<<30,
		Fto:      _Ref{},
		Farg: [2]_Ref{
			0: a0,
			1: a1,
		},
	}
	v3 = x_newtmp(tls, qbe, __ccgo_ts+3737, cls, qbe.s_curf1)
	(*_Insert)(unsafe.Pointer(ist)).Fnew1.Fins.Fto = v3
	return v3
}

func s_cast(tls *libc.TLS, qbe *_QBE, r1 uintptr, cls int32, l uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var cls0, v4, v6 int32
	var v1, v2, v3 _Ref
	_, _, _, _, _, _ = cls0, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(r1))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RCon) {
		return
	}
	cls0 = int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(qbe.s_curf1)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(r1 + 0))&0xfffffff8>>3))*192))).Fcls)
	if cls0 == cls || cls == int32(_Kw) && cls0 == int32(_Kl) {
		return
	}
	if cls0&int32(1) < cls&int32(1) {
		if cls0 == int32(_Ks) {
			*(*_Ref)(unsafe.Pointer(r1)) = s_iins(tls, qbe, int32(_Kw), int32(_Ocast), *(*_Ref)(unsafe.Pointer(r1)), _Ref{}, l)
		}
		*(*_Ref)(unsafe.Pointer(r1)) = s_iins(tls, qbe, int32(_Kl), int32(_Oextuw), *(*_Ref)(unsafe.Pointer(r1)), _Ref{}, l)
		if cls == int32(_Kd) {
			*(*_Ref)(unsafe.Pointer(r1)) = s_iins(tls, qbe, int32(_Kd), int32(_Ocast), *(*_Ref)(unsafe.Pointer(r1)), _Ref{}, l)
		}
	} else {
		if cls0 == int32(_Kd) && cls != int32(_Kl) {
			*(*_Ref)(unsafe.Pointer(r1)) = s_iins(tls, qbe, int32(_Kl), int32(_Ocast), *(*_Ref)(unsafe.Pointer(r1)), _Ref{}, l)
		}
		if cls0 != int32(_Kd) || cls != int32(_Kw) {
			*(*_Ref)(unsafe.Pointer(r1)) = s_iins(tls, qbe, cls, int32(_Ocast), *(*_Ref)(unsafe.Pointer(r1)), _Ref{}, l)
		}
	}
}

func s_mask(tls *libc.TLS, qbe *_QBE, cls int32, r uintptr, msk _bits, l uintptr) {
	s_cast(tls, qbe, r, cls, l)
	*(*_Ref)(unsafe.Pointer(r)) = s_iins(tls, qbe, cls, int32(_Oand), *(*_Ref)(unsafe.Pointer(r)), x_getcon(tls, qbe, libc.Int64FromUint64(msk), qbe.s_curf1), l)
}

func s_load1(tls *libc.TLS, qbe *_QBE, sl _Slice, msk _bits, l uintptr) (r _Ref) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var a1 uintptr
	var all, cls, ld, v1, v5, v7 int32
	var r11, v2, v3, v4 _Ref
	var _ /* c at bp+56 */ _Con
	var _ /* r at bp+48 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _ = a1, all, cls, ld, r11, v1, v2, v3, v4, v5, v7
	*(*[9]int32)(unsafe.Pointer(bp)) = [9]int32{
		1: int32(_Oloadub),
		2: int32(_Oloaduh),
		4: int32(_Oloaduw),
		8: int32(_Oload),
	}
	ld = *(*int32)(unsafe.Pointer(bp + uintptr(sl.Fsz)*4))
	all = libc.BoolInt32(msk == libc.Uint64FromInt32(1)<<(libc.Int32FromInt32(8)*int32(sl.Fsz)-libc.Int32FromInt32(1))*uint64(2)-uint64(1))
	if all != 0 {
		cls = int32(sl.Fcls)
	} else {
		if int32(sl.Fsz) > int32(4) {
			v1 = int32(_Kl)
		} else {
			v1 = int32(_Kw)
		}
		cls = v1
	}
	*(*_Ref)(unsafe.Pointer(bp + 48)) = sl.Fref
	/* sl.ref might not be live here,
	 * but its alias base ref will be
	 * (see killsl() below) */
	v2 = *(*_Ref)(unsafe.Pointer(bp + 48))
	*(*_Ref)(unsafe.Pointer(bp + 44)) = v2
	v3 = v2
	v4 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp + 36)) = v3
	*(*_Ref)(unsafe.Pointer(bp + 40)) = v4
	v5 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 36 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 40 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 36 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 40 + 0))&0xfffffff8>>3))
	goto _6
_6:
	if v5 != 0 {
		v7 = -int32(1)
		goto _8
	}
	v7 = int32(*(*uint32)(unsafe.Pointer(bp + 44 + 0)) & 0x7 >> 0)
	goto _8
_8:
	if v7 == int32(_RTmp) {
		a1 = (*_Fn)(unsafe.Pointer(qbe.s_curf1)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 48 + 0))&0xfffffff8>>3))*192 + 144
		switch (*_Alias)(unsafe.Pointer(a1)).Ftype1 {
		default:
			_die_(tls, qbe, __ccgo_ts+3730, __ccgo_ts+3532, 0)
			fallthrough
		case int32(_ALoc):
			fallthrough
		case int32(_AEsc):
			fallthrough
		case int32(_AUnk):
			*(*_Ref)(unsafe.Pointer(bp + 48)) = _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32((*_Alias)(unsafe.Pointer(a1)).Fbase)&0x1fffffff<<3,
			}
			if !((*_Alias)(unsafe.Pointer(a1)).Foffset != 0) {
				break
			}
			r11 = x_getcon(tls, qbe, (*_Alias)(unsafe.Pointer(a1)).Foffset, qbe.s_curf1)
			*(*_Ref)(unsafe.Pointer(bp + 48)) = s_iins(tls, qbe, int32(_Kl), int32(_Oadd), *(*_Ref)(unsafe.Pointer(bp + 48)), r11, l)
		case int32(_ACon):
			fallthrough
		case int32(_ASym):
			libc.X__builtin___memset_chk(tls, bp+56, 0, uint64(32), ^___predefined_size_t(0))
			(*(*_Con)(unsafe.Pointer(bp + 56))).Ftype1 = int32(_CAddr)
			(*(*_Con)(unsafe.Pointer(bp + 56))).Fsym = (*_Alias)(unsafe.Pointer(a1)).Fu.Fsym
			*(*_int64_t)(unsafe.Pointer(bp + 56 + 16)) = (*_Alias)(unsafe.Pointer(a1)).Foffset
			*(*_Ref)(unsafe.Pointer(bp + 48)) = x_newcon(tls, qbe, bp+56, qbe.s_curf1)
			break
		}
	}
	*(*_Ref)(unsafe.Pointer(bp + 48)) = s_iins(tls, qbe, cls, ld, *(*_Ref)(unsafe.Pointer(bp + 48)), _Ref{}, l)
	if !(all != 0) {
		s_mask(tls, qbe, cls, bp+48, msk, l)
	}
	return *(*_Ref)(unsafe.Pointer(bp + 48))
}

func s_killsl(tls *libc.TLS, qbe *_QBE, r1 _Ref, _sl _Slice) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	*(*_Slice)(unsafe.Pointer(bp + 12)) = _sl
	var a1 uintptr
	var v1, v2, v3, v8, v9 _Ref
	var v10, v4, v6 int32
	_, _, _, _, _, _, _, _, _ = a1, v1, v10, v2, v3, v4, v6, v8, v9
	v1 = (*(*_Slice)(unsafe.Pointer(bp + 12))).Fref
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RTmp) {
		return 0
	}
	a1 = (*_Fn)(unsafe.Pointer(qbe.s_curf1)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192 + 144
	switch (*_Alias)(unsafe.Pointer(a1)).Ftype1 {
	default:
		_die_(tls, qbe, __ccgo_ts+3730, __ccgo_ts+3532, 0)
		fallthrough
	case int32(_ALoc):
		fallthrough
	case int32(_AEsc):
		fallthrough
	case int32(_AUnk):
		v8 = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32((*_Alias)(unsafe.Pointer(a1)).Fbase)&0x1fffffff<<3,
		}
		v9 = r1
		*(*_Ref)(unsafe.Pointer(bp)) = v8
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
		v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _11
	_11:
		return v10
	case int32(_ACon):
		fallthrough
	case int32(_ASym):
		return 0
	}
	return r
}

// C documentation
//
//	/* returns a ref containing the contents of the slice
//	 * passed as argument, all the bits set to 0 in the
//	 * mask argument are zeroed in the result;
//	 * the returned ref has an integer class when the
//	 * mask does not cover all the bits of the slice,
//	 * otherwise, it has class sl.cls
//	 * the procedure returns R when it fails */
func s_def(tls *libc.TLS, qbe *_QBE, sl _Slice, msk _bits, b1 uintptr, i uintptr, il uintptr) (r _Ref) {
	bp1 := tls.Alloc(48)
	defer tls.Free(48)
	var bp, ist, p, p5 uintptr
	var cls, cls1, ld, op, sz, v12, v17, v2, v22, v28, v3, v8 int32
	var msk1, msks _bits
	var np, oldl, oldt, v24 _uint
	var r11, v10, v11, v15, v16, v20, v21, v26, v27, v6, v7 _Ref
	var _ /* l at bp+32 */ _Loc
	var _ /* off at bp+24 */ int32
	var _ /* r at bp+28 */ _Ref
	var _ /* sl1 at bp+12 */ _Slice
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = bp, cls, cls1, ist, ld, msk1, msks, np, oldl, oldt, op, p, r11, sz, v10, v11, v12, v15, v16, v17, v2, v20, v21, v22, v24, v26, v27, v28, v3, v6, v7, v8, p5
	/* invariants:
	 * -1- b dominates il->blk; so we can use
	 *     temporaries of b in il->blk
	 * -2- if il->type != LNoLoad, then il->blk
	 *     postdominates the original load; so it
	 *     is safe to load in il->blk
	 * -3- if il->type != LNoLoad, then b
	 *     postdominates il->blk (and by 2, the
	 *     original load)
	 */
	oldl = qbe.s_nlog
	oldt = libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(qbe.s_curf1)).Fntmp)
	if !(0 != 0) {
		goto _1
	}
	goto Load
Load:
	;
	(*_Fn)(unsafe.Pointer(qbe.s_curf1)).Fntmp = libc.Int32FromUint32(oldt)
	qbe.s_nlog = oldl
	if (*_Loc)(unsafe.Pointer(il)).Ftype1 != int32(_LLoad) {
		return _Ref{}
	}
	return s_load1(tls, qbe, sl, msk, il)
_1:
	;
	if !(i != 0) {
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
	}
	if int32(sl.Fsz) > int32(4) {
		v2 = int32(_Kl)
	} else {
		v2 = int32(_Kw)
	}
	cls = v2
	msks = libc.Uint64FromInt32(1)<<(libc.Int32FromInt32(8)*int32(sl.Fsz)-libc.Int32FromInt32(1))*libc.Uint64FromInt32(2) - libc.Uint64FromInt32(1)
	for i > (*_Blk)(unsafe.Pointer(b1)).Fins {
		i -= 16
		if s_killsl(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, sl) != 0 || int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ocall) && x_escapes(tls, qbe, sl.Fref, qbe.s_curf1) != 0 {
			goto Load
		}
		ld = libc.BoolInt32(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)))
		if ld != 0 {
			sz = x_loadsz(tls, qbe, i)
			r11 = *(*_Ref)(unsafe.Pointer(i + 8))
			*(*_Ref)(unsafe.Pointer(bp1 + 28)) = (*_Ins)(unsafe.Pointer(i)).Fto
		} else {
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
				sz = x_storesz(tls, qbe, i)
				r11 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
				*(*_Ref)(unsafe.Pointer(bp1 + 28)) = *(*_Ref)(unsafe.Pointer(i + 8))
			} else {
				if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit1) {
					*(*_Ref)(unsafe.Pointer(bp1 + 8)) = *(*_Ref)(unsafe.Pointer(i + 8))
					v3 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
					goto _4
				_4:
					sz = libc.Xabs(tls, v3)
					i -= 16
					r11 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
				} else {
					continue
				}
			}
		}
		switch x_alias(tls, qbe, sl.Fref, sl.Foff, int32(sl.Fsz), r11, sz, bp1+24, qbe.s_curf1) {
		case int32(_MustAlias):
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
				*(*_Slice)(unsafe.Pointer(bp1 + 12)) = sl
				(*(*_Slice)(unsafe.Pointer(bp1 + 12))).Fref = *(*_Ref)(unsafe.Pointer(i + 8))
				if *(*int32)(unsafe.Pointer(bp1 + 24)) >= 0 {
					(*(*_Slice)(unsafe.Pointer(bp1 + 12))).Foff = *(*int32)(unsafe.Pointer(bp1 + 24))
					sz -= *(*int32)(unsafe.Pointer(bp1 + 24))
					*(*int32)(unsafe.Pointer(bp1 + 24)) = 0
				} else {
					(*(*_Slice)(unsafe.Pointer(bp1 + 12))).Foff = 0
					p5 = bp1 + 12 + 8
					*(*int16)(unsafe.Pointer(p5)) = int16(int32(*(*int16)(unsafe.Pointer(p5))) + *(*int32)(unsafe.Pointer(bp1 + 24)))
				}
				if sz > int32((*(*_Slice)(unsafe.Pointer(bp1 + 12))).Fsz) {
					sz = int32((*(*_Slice)(unsafe.Pointer(bp1 + 12))).Fsz)
				}
				(*(*_Slice)(unsafe.Pointer(bp1 + 12))).Fsz = int16(sz)
			}
			if *(*int32)(unsafe.Pointer(bp1 + 24)) < 0 {
				*(*int32)(unsafe.Pointer(bp1 + 24)) = -*(*int32)(unsafe.Pointer(bp1 + 24))
				msk1 = (libc.Uint64FromInt32(1)<<(libc.Int32FromInt32(8)*sz-libc.Int32FromInt32(1))*uint64(2) - uint64(1)) << (int32(8) * *(*int32)(unsafe.Pointer(bp1 + 24))) & msks
				op = int32(_Oshl)
			} else {
				msk1 = (libc.Uint64FromInt32(1)<<(libc.Int32FromInt32(8)*sz-libc.Int32FromInt32(1))*uint64(2) - uint64(1)) >> (int32(8) * *(*int32)(unsafe.Pointer(bp1 + 24))) & msks
				op = int32(_Oshr)
			}
			if msk1&msk == uint64(0) {
				continue
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oblit0) {
				*(*_Ref)(unsafe.Pointer(bp1 + 28)) = s_def(tls, qbe, *(*_Slice)(unsafe.Pointer(bp1 + 12)), libc.Uint64FromInt32(1)<<(libc.Int32FromInt32(8)*sz-libc.Int32FromInt32(1))*libc.Uint64FromInt32(2)-libc.Uint64FromInt32(1), b1, i, il)
				v6 = *(*_Ref)(unsafe.Pointer(bp1 + 28))
				v7 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v6
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v7
				v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _9
			_9:
				if v8 != 0 {
					goto Load
				}
			}
			if *(*int32)(unsafe.Pointer(bp1 + 24)) != 0 {
				cls1 = cls
				if op == int32(_Oshr) && *(*int32)(unsafe.Pointer(bp1 + 24))+int32(sl.Fsz) > int32(4) {
					cls1 = int32(_Kl)
				}
				s_cast(tls, qbe, bp1+28, cls1, il)
				r11 = x_getcon(tls, qbe, int64(int32(8)**(*int32)(unsafe.Pointer(bp1 + 24))), qbe.s_curf1)
				*(*_Ref)(unsafe.Pointer(bp1 + 28)) = s_iins(tls, qbe, cls1, op, *(*_Ref)(unsafe.Pointer(bp1 + 28)), r11, il)
			}
			if msk1&msk != msk1 || *(*int32)(unsafe.Pointer(bp1 + 24))+sz < int32(sl.Fsz) {
				s_mask(tls, qbe, cls, bp1+28, msk1&msk, il)
			}
			if msk & ^msk1 != uint64(0) {
				r11 = s_def(tls, qbe, sl, msk & ^msk1, b1, i, il)
				v10 = r11
				v11 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v10
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v11
				v12 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _13
			_13:
				if v12 != 0 {
					goto Load
				}
				*(*_Ref)(unsafe.Pointer(bp1 + 28)) = s_iins(tls, qbe, cls, int32(_Oor), *(*_Ref)(unsafe.Pointer(bp1 + 28)), r11, il)
			}
			if msk == msks {
				s_cast(tls, qbe, bp1+28, int32(sl.Fcls), il)
			}
			return *(*_Ref)(unsafe.Pointer(bp1 + 28))
		case int32(_MayAlias):
			if ld != 0 {
				continue
			} else {
				goto Load
			}
			fallthrough
		case int32(_NoAlias):
			continue
		default:
			_die_(tls, qbe, __ccgo_ts+3730, __ccgo_ts+3532, 0)
		}
	}
	ist = qbe.s_ilog
	for {
		if !(ist < qbe.s_ilog+uintptr(qbe.s_nlog)*40) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(ist + 0))&0x1>>0) != 0 && (*_Insert)(unsafe.Pointer(ist)).Fbid == (*_Blk)(unsafe.Pointer(b1)).Fid {
			v15 = (*(*struct {
				Fm _Slice
				Fp uintptr
			})(unsafe.Pointer(ist + 16))).Fm.Fref
			v16 = sl.Fref
			*(*_Ref)(unsafe.Pointer(bp1)) = v15
			*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v16
			v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
			goto _18
		_18:
			if v17 != 0 {
				if (*(*struct {
					Fm _Slice
					Fp uintptr
				})(unsafe.Pointer(ist + 16))).Fm.Foff == sl.Foff {
					if int32((*(*struct {
						Fm _Slice
						Fp uintptr
					})(unsafe.Pointer(ist + 16))).Fm.Fsz) == int32(sl.Fsz) {
						*(*_Ref)(unsafe.Pointer(bp1 + 28)) = (*_Phi)(unsafe.Pointer((*(*struct {
							Fm _Slice
							Fp uintptr
						})(unsafe.Pointer(ist + 16))).Fp)).Fto
						if msk != msks {
							s_mask(tls, qbe, cls, bp1+28, msk, il)
						} else {
							s_cast(tls, qbe, bp1+28, int32(sl.Fcls), il)
						}
						return *(*_Ref)(unsafe.Pointer(bp1 + 28))
					}
				}
			}
		}
		goto _14
	_14:
		;
		ist += 40
	}
	p = (*_Blk)(unsafe.Pointer(b1)).Fphi
	for {
		if !(p != 0) {
			break
		}
		if s_killsl(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Fto, sl) != 0 {
			/* scanning predecessors in that
			 * case would be unsafe */
			goto Load
		}
		goto _19
	_19:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
	if (*_Blk)(unsafe.Pointer(b1)).Fnpred == uint32(0) {
		goto Load
	}
	if (*_Blk)(unsafe.Pointer(b1)).Fnpred == uint32(1) {
		bp = *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b1)).Fpred))
		*(*_Loc)(unsafe.Pointer(bp1 + 32)) = *(*_Loc)(unsafe.Pointer(il))
		if (*_Blk)(unsafe.Pointer(bp)).Fs2 != 0 {
			(*(*_Loc)(unsafe.Pointer(bp1 + 32))).Ftype1 = int32(_LNoLoad)
		}
		r11 = s_def(tls, qbe, sl, msk, bp, uintptr(0), bp1+32)
		v20 = r11
		v21 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp1)) = v20
		*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v21
		v22 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
		goto _23
	_23:
		if v22 != 0 {
			goto Load
		}
		return r11
	}
	*(*_Ref)(unsafe.Pointer(bp1 + 28)) = x_newtmp(tls, qbe, __ccgo_ts+3737, int32(sl.Fcls), qbe.s_curf1)
	p = x_alloc(tls, qbe, uint64(40))
	qbe.s_nlog++
	v24 = qbe.s_nlog
	x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_ilog)), uint64(v24))
	ist = qbe.s_ilog + uintptr(qbe.s_nlog-uint32(1))*40
	libc.SetBitFieldPtr32Uint32(ist+0, libc.Uint32FromInt32(1), 0, 0x1)
	(*_Insert)(unsafe.Pointer(ist)).Fbid = (*_Blk)(unsafe.Pointer(b1)).Fid
	(*(*struct {
		Fm _Slice
		Fp uintptr
	})(unsafe.Pointer(ist + 16))).Fm = sl
	(*(*struct {
		Fm _Slice
		Fp uintptr
	})(unsafe.Pointer(ist + 16))).Fp = p
	(*_Phi)(unsafe.Pointer(p)).Fto = *(*_Ref)(unsafe.Pointer(bp1 + 28))
	(*_Phi)(unsafe.Pointer(p)).Fcls = int32(sl.Fcls)
	(*_Phi)(unsafe.Pointer(p)).Fnarg = (*_Blk)(unsafe.Pointer(b1)).Fnpred
	(*_Phi)(unsafe.Pointer(p)).Farg = x_vnew(tls, qbe, uint64((*_Phi)(unsafe.Pointer(p)).Fnarg), uint64(4), int32(_PFn))
	(*_Phi)(unsafe.Pointer(p)).Fblk = x_vnew(tls, qbe, uint64((*_Phi)(unsafe.Pointer(p)).Fnarg), uint64(8), int32(_PFn))
	np = uint32(0)
	for {
		if !(np < (*_Blk)(unsafe.Pointer(b1)).Fnpred) {
			break
		}
		bp = *(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b1)).Fpred + uintptr(np)*8))
		if !((*_Blk)(unsafe.Pointer(bp)).Fs2 != 0) && (*_Loc)(unsafe.Pointer(il)).Ftype1 != int32(_LNoLoad) && (*_Blk)(unsafe.Pointer(bp)).Floop < (*_Blk)(unsafe.Pointer((*_Loc)(unsafe.Pointer(il)).Fblk)).Floop {
			(*(*_Loc)(unsafe.Pointer(bp1 + 32))).Ftype1 = int32(_LLoad)
		} else {
			(*(*_Loc)(unsafe.Pointer(bp1 + 32))).Ftype1 = int32(_LNoLoad)
		}
		(*(*_Loc)(unsafe.Pointer(bp1 + 32))).Fblk = bp
		(*(*_Loc)(unsafe.Pointer(bp1 + 32))).Foff = (*_Blk)(unsafe.Pointer(bp)).Fnins
		r11 = s_def(tls, qbe, sl, msks, bp, uintptr(0), bp1+32)
		v26 = r11
		v27 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp1)) = v26
		*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v27
		v28 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
		goto _29
	_29:
		if v28 != 0 {
			goto Load
		}
		*(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(np)*4)) = r11
		*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(np)*8)) = bp
		goto _25
	_25:
		;
		np++
	}
	if msk != msks {
		s_mask(tls, qbe, cls, bp1+28, msk, il)
	}
	return *(*_Ref)(unsafe.Pointer(bp1 + 28))
}

func s_icmp(tls *libc.TLS, qbe *_QBE, pa uintptr, pb uintptr) (r int32) {
	var a, b uintptr
	var c, v1, v2 int32
	_, _, _, _, _ = a, b, c, v1, v2
	a = pa
	b = pb
	v1 = libc.Int32FromUint32((*_Insert)(unsafe.Pointer(a)).Fbid - (*_Insert)(unsafe.Pointer(b)).Fbid)
	c = v1
	if v1 != 0 {
		return c
	}
	if int32(*(*uint32)(unsafe.Pointer(a + 0))&0x1>>0) != 0 && int32(*(*uint32)(unsafe.Pointer(b + 0))&0x1>>0) != 0 {
		return 0
	}
	if int32(*(*uint32)(unsafe.Pointer(a + 0))&0x1>>0) != 0 {
		return -int32(1)
	}
	if int32(*(*uint32)(unsafe.Pointer(b + 0))&0x1>>0) != 0 {
		return +libc.Int32FromInt32(1)
	}
	v2 = libc.Int32FromUint32((*_Insert)(unsafe.Pointer(a)).Foff - (*_Insert)(unsafe.Pointer(b)).Foff)
	c = v2
	if v2 != 0 {
		return c
	}
	return int32(*(*uint32)(unsafe.Pointer(a + 0))&0xfffffffe>>1) - int32(*(*uint32)(unsafe.Pointer(b + 0))&0xfffffffe>>1)
}

// C documentation
//
//	/* require rpo ssa alias */
func x_loadopt(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var b1, i, ist, v6 uintptr
	var ext, n, ni, nt, v13, v7 _uint
	var sl _Slice
	var sz, v10 int32
	var v12 bool
	var v8, v9 _Ref
	var _ /* ib at bp+8 */ uintptr
	var _ /* l at bp+16 */ _Loc
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, ext, i, ist, n, ni, nt, sl, sz, v10, v12, v13, v6, v7, v8, v9
	qbe.s_curf1 = fn
	qbe.s_ilog = x_vnew(tls, qbe, uint64(0), uint64(40), int32(_PHeap))
	qbe.s_nlog = uint32(0)
	qbe.s_inum = uint32(0)
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb))) {
				goto _2
			}
			sz = x_loadsz(tls, qbe, i)
			sl = _Slice{
				Fref: *(*_Ref)(unsafe.Pointer(i + 8)),
				Fsz:  int16(sz),
				Fcls: int16(int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30)),
			}
			*(*_Loc)(unsafe.Pointer(bp + 16)) = _Loc{
				Foff: libc.Uint32FromInt64((int64(i) - int64((*_Blk)(unsafe.Pointer(b1)).Fins)) / 16),
				Fblk: b1,
			}
			*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = s_def(tls, qbe, sl, libc.Uint64FromInt32(1)<<(libc.Int32FromInt32(8)*sz-libc.Int32FromInt32(1))*libc.Uint64FromInt32(2)-libc.Uint64FromInt32(1), b1, i, bp+16)
			goto _2
		_2:
			;
			i += 16
		}
		goto _1
	_1:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	qsort(tls, qbe, qbe.s_ilog, uint64(qbe.s_nlog), uint64(40), __ccgo_fp(s_icmp))
	x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_ilog)), uint64(qbe.s_nlog+uint32(1)))
	(*(*_Insert)(unsafe.Pointer(qbe.s_ilog + uintptr(qbe.s_nlog)*40))).Fbid = (*_Fn)(unsafe.Pointer(fn)).Fnblk /* add a sentinel */
	*(*uintptr)(unsafe.Pointer(bp + 8)) = x_vnew(tls, qbe, uint64(0), uint64(16), int32(_PHeap))
	ist = qbe.s_ilog
	n = libc.Uint32FromInt32(0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
		for {
			if !((*_Insert)(unsafe.Pointer(ist)).Fbid == n && int32(*(*uint32)(unsafe.Pointer(ist + 0))&0x1>>0) != 0) {
				break
			}
			(*_Phi)(unsafe.Pointer((*(*struct {
				Fm _Slice
				Fp uintptr
			})(unsafe.Pointer(ist + 16))).Fp)).Flink = (*_Blk)(unsafe.Pointer(b1)).Fphi
			(*_Blk)(unsafe.Pointer(b1)).Fphi = (*(*struct {
				Fm _Slice
				Fp uintptr
			})(unsafe.Pointer(ist + 16))).Fp
			goto _4
		_4:
			;
			ist += 40
		}
		ni = uint32(0)
		nt = uint32(0)
		for {
			if (*_Insert)(unsafe.Pointer(ist)).Fbid == n && (*_Insert)(unsafe.Pointer(ist)).Foff == ni {
				v6 = ist
				ist += 40
				i = v6 + 16
			} else {
				if ni == (*_Blk)(unsafe.Pointer(b1)).Fnins {
					break
				}
				v7 = ni
				ni++
				i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr(v7)*16
				if v12 = libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)); v12 {
					v8 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
					v9 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp)) = v8
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
					v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _11
				_11:
				}
				if v12 && !(v10 != 0) {
					ext = libc.Uint32FromInt32(int32(_Oextsb) + int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - int32(_Oloadsb))
					switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
					default:
						_die_(tls, qbe, __ccgo_ts+3730, __ccgo_ts+3532, 0)
						fallthrough
					case int32(_Oloadsb):
						fallthrough
					case int32(_Oloadub):
						fallthrough
					case int32(_Oloadsh):
						fallthrough
					case int32(_Oloaduh):
						libc.SetBitFieldPtr32Uint32(i+0, ext, 0, 0x3fffffff)
					case int32(_Oloadsw):
						fallthrough
					case int32(_Oloaduw):
						if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) == int32(_Kl) {
							libc.SetBitFieldPtr32Uint32(i+0, ext, 0, 0x3fffffff)
							break
						}
						/* fall through */
						fallthrough
					case int32(_Oload):
						libc.SetBitFieldPtr32Uint32(i+0, uint32(_Ocopy), 0, 0x3fffffff)
						break
					}
					*(*_Ref)(unsafe.Pointer(i + 8)) = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
					*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = _Ref{}
				}
			}
			nt++
			v13 = nt
			x_vgrow(tls, qbe, bp+8, uint64(v13))
			*(*_Ins)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)) + uintptr(nt-uint32(1))*16)) = *(*_Ins)(unsafe.Pointer(i))
			goto _5
		_5:
		}
		(*_Blk)(unsafe.Pointer(b1)).Fnins = nt
		x_idup(tls, qbe, b1+8, *(*uintptr)(unsafe.Pointer(bp + 8)), uint64(nt))
		goto _3
	_3:
		;
		n++
	}
	x_vfree(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 8)))
	x_vfree(tls, qbe, qbe.s_ilog)
	if qbe.x_debug[int32('M')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3740, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

func s_iscon(tls *libc.TLS, qbe *_QBE, _r _Ref, bits _int64_t, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	;
	return libc.BoolInt32(v6 == int32(_RCon) && (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32))).Ftype1 == int32(_CBits) && *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32 + 16)) == bits)
}

func s_iscopy(tls *libc.TLS, qbe *_QBE, i uintptr, _r _Ref, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var b1 _bits
	var op, t uintptr
	var v1, v2, v3 _Ref
	var v4, v6 int32
	var v8 bool
	_, _, _, _, _, _, _, _, _ = b1, op, t, v1, v2, v3, v4, v6, v8
	if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ocopy) {
		return int32(1)
	}
	op = uintptr(unsafe.Pointer(&qbe.x_optab)) + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))*32
	if int32(_uint(*(*uint8)(unsafe.Pointer(op + 24))&0x2>>1)) != 0 && int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
		return s_iscon(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), int64(int32(_uint(*(*uint8)(unsafe.Pointer(op + 24))&0x4>>2))), fn)
	}
	if v8 = !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oextsb) <= libc.Uint32FromInt32(int32(_Oextuw)-int32(_Oextsb))); !v8 {
		v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
		v2 = v1
		v3 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v2
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
		v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _5
	_5:
		if v4 != 0 {
			v6 = -int32(1)
			goto _7
		}
		v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _7
	_7:
	}
	if v8 || v6 != int32(_RTmp) {
		return 0
	}
	if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oextsw) || int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oextuw) {
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) == int32(_Kw) {
			return int32(1)
		}
	}
	t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192
	if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) == int32(_Kl) && int32((*_Tmp)(unsafe.Pointer(t)).Fcls) == int32(_Kw) {
		return 0
	}
	b1 = qbe.s_extcpy[(*_Tmp)(unsafe.Pointer(t)).Fwidth]
	return libc.BoolInt32(libc.Uint64FromInt32(1)<<(int32(_Wsb)+(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)-int32(_Oextsb)))&b1 != uint64(0))
}

func s_copyof(tls *libc.TLS, qbe *_QBE, _r _Ref, cpy uintptr) (r _Ref) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3, v8, v9 _Ref
	var v10, v4, v6 int32
	var v12 bool
	_, _, _, _, _, _, _, _, _ = v1, v10, v12, v2, v3, v4, v6, v8, v9
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	;
	if v12 = v6 == int32(_RTmp); v12 {
		v8 = *(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*4))
		v9 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v8
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
		v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _11
	_11:
	}
	if v12 && !(v10 != 0) {
		return *(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*4))
	}
	return *(*_Ref)(unsafe.Pointer(bp + 12))
}

// C documentation
//
//	/* detects a cluster of phis/copies redundant with 'r';
//	 * the algorithm is inspired by Section 3.2 of "Simple
//	 * and Efficient SSA Construction" by Braun M. et al.
//	 */
func s_phisimpl(tls *libc.TLS, qbe *_QBE, p uintptr, r1 _Ref, cpy uintptr, pstk uintptr, ts uintptr, as uintptr, fn uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var a1, nstk, v1, v18, v2 _uint
	var p0, stk, u, u1 uintptr
	var v10, v11, v12, v6, v7 _Ref
	var v13, v15, v3, v8 int32
	var _ /* r1 at bp+72 */ _Ref
	var _ /* t at bp+68 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, nstk, p0, stk, u, u1, v1, v10, v11, v12, v13, v15, v18, v2, v3, v6, v7, v8
	x_bszero(tls, qbe, ts)
	x_bszero(tls, qbe, as)
	*(*_Phi)(unsafe.Pointer(bp)) = _Phi{}
	p0 = bp
	stk = *(*uintptr)(unsafe.Pointer(pstk))
	nstk = uint32(1)
	*(*_Use)(unsafe.Pointer(bp + 40)) = _Use{
		Ftype1: int32(_UPhi),
		Fu: *(*struct {
			Fphi [0]uintptr
			Fins uintptr
		})(unsafe.Pointer(&struct{ f uintptr }{f: p})),
	}
	*(*uintptr)(unsafe.Pointer(stk)) = bp + 40
	for nstk != 0 {
		nstk--
		v1 = nstk
		u = *(*uintptr)(unsafe.Pointer(stk + uintptr(v1)*8))
		if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UIns) && s_iscopy(tls, qbe, *(*uintptr)(unsafe.Pointer(u + 8)), r1, fn) != 0 {
			p = p0
			*(*int32)(unsafe.Pointer(bp + 68)) = int32(*(*uint32)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(u + 8)) + 4 + 0)) & 0xfffffff8 >> 3)
		} else {
			if (*_Use)(unsafe.Pointer(u)).Ftype1 == int32(_UPhi) {
				p = *(*uintptr)(unsafe.Pointer(u + 8))
				*(*int32)(unsafe.Pointer(bp + 68)) = int32(*(*uint32)(unsafe.Pointer(p + 0)) & 0xfffffff8 >> 3)
			} else {
				continue
			}
		}
		v2 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 68)))
		v3 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(ts)).Ft + uintptr(v2/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v2%uint32(_NBit))) != uint64(0))
		goto _4
	_4:
		if v3 != 0 {
			continue
		}
		x_bsset(tls, qbe, ts, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 68))))
		a1 = uint32(0)
		for {
			if !(a1 < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
				break
			}
			*(*_Ref)(unsafe.Pointer(bp + 72)) = s_copyof(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4)), cpy)
			v6 = *(*_Ref)(unsafe.Pointer(bp + 72))
			v7 = r1
			*(*_Ref)(unsafe.Pointer(bp + 56)) = v6
			*(*_Ref)(unsafe.Pointer(bp + 60)) = v7
			v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 56 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 60 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 56 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 60 + 0))&0xfffffff8>>3))
			goto _9
		_9:
			if v8 != 0 {
				goto _5
			}
			v10 = *(*_Ref)(unsafe.Pointer(bp + 72))
			*(*_Ref)(unsafe.Pointer(bp + 64)) = v10
			v11 = v10
			v12 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp + 56)) = v11
			*(*_Ref)(unsafe.Pointer(bp + 60)) = v12
			v13 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 56 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 60 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 56 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 60 + 0))&0xfffffff8>>3))
			goto _14
		_14:
			if v13 != 0 {
				v15 = -int32(1)
				goto _16
			}
			v15 = int32(*(*uint32)(unsafe.Pointer(bp + 64 + 0)) & 0x7 >> 0)
			goto _16
		_16:
			if v15 != int32(_RTmp) {
				return
			}
			x_bsset(tls, qbe, as, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 72 + 0))&0xfffffff8>>3)))
			goto _5
		_5:
			;
			a1++
		}
		u = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(*(*int32)(unsafe.Pointer(bp + 68)))*192))).Fuse
		u1 = u + uintptr((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(*(*int32)(unsafe.Pointer(bp + 68)))*192))).Fnuse)*16
		x_vgrow(tls, qbe, pstk, libc.Uint64FromInt64(libc.Int64FromUint32(nstk)+(int64(u1)-int64(u))/16))
		stk = *(*uintptr)(unsafe.Pointer(pstk))
		for {
			if !(u < u1) {
				break
			}
			v18 = nstk
			nstk++
			*(*uintptr)(unsafe.Pointer(stk + uintptr(v18)*8)) = u
			goto _17
		_17:
			;
			u += 16
		}
	}
	x_bsdiff(tls, qbe, as, ts)
	if !(x_bscount(tls, qbe, as) != 0) {
		*(*int32)(unsafe.Pointer(bp + 68)) = 0
		for {
			if !(x_bsiter(tls, qbe, ts, bp+68) != 0) {
				break
			}
			*(*_Ref)(unsafe.Pointer(cpy + uintptr(*(*int32)(unsafe.Pointer(bp + 68)))*4)) = r1
			goto _19
		_19:
			;
			*(*int32)(unsafe.Pointer(bp + 68))++
		}
	}
}

func s_subst(tls *libc.TLS, qbe *_QBE, pr uintptr, cpy uintptr) {
	*(*_Ref)(unsafe.Pointer(pr)) = s_copyof(tls, qbe, *(*_Ref)(unsafe.Pointer(pr)), cpy)
}

// C documentation
//
//	/* requires use and dom, breaks use */
func x_copy(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var a1, eq, n _uint
	var b1, cpy, i, p, pp, v40 uintptr
	var r11, v12, v13, v17, v18, v21, v22, v26, v27, v28, v3, v34, v35, v4, v41, v42, v47, v48, v52, v53, v56, v57, v8, v9 _Ref
	var t, v10, v14, v19, v23, v29, v31, v36, v43, v49, v5, v54, v58 int32
	var v16, v25 bool
	var _ /* as at bp+32 */ [1]_BSet
	var _ /* r at bp+56 */ _Ref
	var _ /* stk at bp+48 */ uintptr
	var _ /* ts at bp+16 */ [1]_BSet
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, b1, cpy, eq, i, n, p, pp, r11, t, v10, v12, v13, v14, v16, v17, v18, v19, v21, v22, v23, v25, v26, v27, v28, v29, v3, v31, v34, v35, v36, v4, v40, v41, v42, v43, v47, v48, v49, v5, v52, v53, v54, v56, v57, v58, v8, v9
	x_bsinit(tls, qbe, bp+16, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
	x_bsinit(tls, qbe, bp+32, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
	cpy = x_emalloc(tls, qbe, libc.Uint64FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp)*uint64(4))
	*(*uintptr)(unsafe.Pointer(bp + 48)) = x_vnew(tls, qbe, uint64(10), uint64(8), int32(_PHeap))
	/* 1. build the copy-of map */
	n = uint32(0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			v3 = *(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*4))
			v4 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v3
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v4
			v5 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _6
		_6:
			if !(v5 != 0) {
				goto _2
			}
			eq = uint32(0)
			*(*_Ref)(unsafe.Pointer(bp + 56)) = _Ref{}
			a1 = uint32(0)
			for {
				if !(a1 < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
					break
				}
				if (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a1)*8)))).Fid < n {
					r11 = s_copyof(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4)), cpy)
					v8 = *(*_Ref)(unsafe.Pointer(bp + 56))
					v9 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp)) = v8
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
					v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _11
				_11:
					;
					if v16 = v10 != 0; !v16 {
						v12 = *(*_Ref)(unsafe.Pointer(bp + 56))
						v13 = _Ref{
							F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(0)&0x1fffffff<<3,
						}
						*(*_Ref)(unsafe.Pointer(bp)) = v12
						*(*_Ref)(unsafe.Pointer(bp + 4)) = v13
						v14 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
						goto _15
					_15:
					}
					if v16 || v14 != 0 {
						*(*_Ref)(unsafe.Pointer(bp + 56)) = r11
					}
					v17 = r11
					v18 = *(*_Ref)(unsafe.Pointer(bp + 56))
					*(*_Ref)(unsafe.Pointer(bp)) = v17
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v18
					v19 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _20
				_20:
					;
					if v25 = v19 != 0; !v25 {
						v21 = r11
						v22 = _Ref{
							F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(0)&0x1fffffff<<3,
						}
						*(*_Ref)(unsafe.Pointer(bp)) = v21
						*(*_Ref)(unsafe.Pointer(bp + 4)) = v22
						v23 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
						goto _24
					_24:
					}
					if v25 || v23 != 0 {
						eq++
					}
				}
				goto _7
			_7:
				;
				a1++
			}
			v26 = *(*_Ref)(unsafe.Pointer(bp + 56))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v26
			v27 = v26
			v28 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v27
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v28
			v29 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _30
		_30:
			if v29 != 0 {
				v31 = -int32(1)
				goto _32
			}
			v31 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _32
		_32:
			;
			if v31 == int32(_RTmp) && !(x_dom(tls, qbe, *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 56 + 0))&0xfffffff8>>3))*192))).Fbid)*8)), b1) != 0) {
				*(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*4)) = (*_Phi)(unsafe.Pointer(p)).Fto
			} else {
				if eq == (*_Phi)(unsafe.Pointer(p)).Fnarg {
					*(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*4)) = *(*_Ref)(unsafe.Pointer(bp + 56))
				} else {
					*(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*4)) = (*_Phi)(unsafe.Pointer(p)).Fto
					s_phisimpl(tls, qbe, p, *(*_Ref)(unsafe.Pointer(bp + 56)), cpy, bp+48, bp+16, bp+32, fn)
				}
			}
			goto _2
		_2:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			v34 = *(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*4))
			v35 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v34
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v35
			v36 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _37
		_37:
			if !(v36 != 0) {
				goto _33
			}
			*(*_Ref)(unsafe.Pointer(bp + 56)) = s_copyof(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), cpy)
			if s_iscopy(tls, qbe, i, *(*_Ref)(unsafe.Pointer(bp + 56)), fn) != 0 {
				*(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*4)) = *(*_Ref)(unsafe.Pointer(bp + 56))
			} else {
				*(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*4)) = (*_Ins)(unsafe.Pointer(i)).Fto
			}
			goto _33
		_33:
			;
			i += 16
		}
		goto _1
	_1:
		;
		n++
	}
	/* 2. remove redundant phis/copies
	 * and rewrite their uses */
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		pp = b1
		for {
			v40 = *(*uintptr)(unsafe.Pointer(pp))
			p = v40
			if !(v40 != 0) {
				break
			}
			*(*_Ref)(unsafe.Pointer(bp + 56)) = *(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*4))
			v41 = *(*_Ref)(unsafe.Pointer(bp + 56))
			v42 = (*_Phi)(unsafe.Pointer(p)).Fto
			*(*_Ref)(unsafe.Pointer(bp)) = v41
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v42
			v43 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _44
		_44:
			if !(v43 != 0) {
				*(*uintptr)(unsafe.Pointer(pp)) = (*_Phi)(unsafe.Pointer(p)).Flink
				goto _39
			}
			a1 = uint32(0)
			for {
				if !(a1 < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
					break
				}
				s_subst(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(a1)*4, cpy)
				goto _45
			_45:
				;
				a1++
			}
			pp = p + 32
			goto _39
		_39:
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			*(*_Ref)(unsafe.Pointer(bp + 56)) = *(*_Ref)(unsafe.Pointer(cpy + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*4))
			v47 = *(*_Ref)(unsafe.Pointer(bp + 56))
			v48 = (*_Ins)(unsafe.Pointer(i)).Fto
			*(*_Ref)(unsafe.Pointer(bp)) = v47
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v48
			v49 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _50
		_50:
			if !(v49 != 0) {
				*(*_Ins)(unsafe.Pointer(i)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
				goto _46
			}
			s_subst(tls, qbe, i+8, cpy)
			s_subst(tls, qbe, i+8+1*4, cpy)
			goto _46
		_46:
			;
			i += 16
		}
		s_subst(tls, qbe, b1+20+4, cpy)
		goto _38
	_38:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	if qbe.x_debug[int32('C')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3768, 0)
		t = int32(_Tmp0)
		for {
			if !(t < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
				break
			}
			v52 = *(*_Ref)(unsafe.Pointer(cpy + uintptr(t)*4))
			v53 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v52
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v53
			v54 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _55
		_55:
			if v54 != 0 {
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3789, libc.VaList(bp+72, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(t)*192))
			} else {
				v56 = *(*_Ref)(unsafe.Pointer(cpy + uintptr(t)*4))
				v57 = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
				}
				*(*_Ref)(unsafe.Pointer(bp)) = v56
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v57
				v58 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _59
			_59:
				if !(v58 != 0) {
					fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3805, libc.VaList(bp+72, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(t)*192))
					x_printref(tls, qbe, *(*_Ref)(unsafe.Pointer(cpy + uintptr(t)*4)), fn, libc.X__stderrp)
				}
			}
			goto _51
		_51:
			;
			t++
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3820, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
	x_vfree(tls, qbe, *(*uintptr)(unsafe.Pointer(bp + 48)))
	free(tls, qbe, cpy)
}

const _Bot = -1

const /* lattice bottom */
_Top = 0

type _Edge = struct {
	Fdest int32
	Fdead int32
	Fwork uintptr
}

func s_iscon1(tls *libc.TLS, qbe *_QBE, c uintptr, w int32, k _uint64_t) (r int32) {
	if (*_Con)(unsafe.Pointer(c)).Ftype1 != int32(_CBits) {
		return 0
	}
	if w != 0 {
		return libc.BoolInt32(libc.Uint64FromInt64(*(*_int64_t)(unsafe.Pointer(c + 16))) == k)
	} else {
		return libc.BoolInt32(libc.Uint32FromInt64(*(*_int64_t)(unsafe.Pointer(c + 16))) == uint32(k))
	}
	return r
}

func s_latval(tls *libc.TLS, qbe *_QBE, _r _Ref) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	switch v6 {
	case int32(_RTmp):
		return *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*4))
	case int32(_RCon):
		return int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0)) & 0xfffffff8 >> 3)
	default:
		_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
	}
	return r
}

func s_latmerge(tls *libc.TLS, qbe *_QBE, v int32, m int32) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if m == int32(_Top) {
		v1 = v
	} else {
		if v == int32(_Top) || v == m {
			v2 = m
		} else {
			v2 = int32(_Bot)
		}
		v1 = v2
	}
	return v1
}

func s_update(tls *libc.TLS, qbe *_QBE, t int32, m int32, fn uintptr) {
	var tmp uintptr
	var u, v2 _uint
	_, _, _ = tmp, u, v2
	m = s_latmerge(tls, qbe, *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)), m)
	if m != *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)) {
		tmp = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(t)*192
		u = uint32(0)
		for {
			if !(u < (*_Tmp)(unsafe.Pointer(tmp)).Fnuse) {
				break
			}
			qbe.s_nuse++
			v2 = qbe.s_nuse
			x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_usewrk)), uint64(v2))
			*(*uintptr)(unsafe.Pointer(qbe.s_usewrk + uintptr(qbe.s_nuse-uint32(1))*8)) = (*_Tmp)(unsafe.Pointer(tmp)).Fuse + uintptr(u)*16
			goto _1
		_1:
			;
			u++
		}
		*(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)) = m
	}
}

func s_deadedge(tls *libc.TLS, qbe *_QBE, s int32, d int32) (r int32) {
	var e uintptr
	_ = e
	e = qbe.s_edge + uintptr(s)*32
	if (*(*_Edge)(unsafe.Pointer(e))).Fdest == d && !((*(*_Edge)(unsafe.Pointer(e))).Fdead != 0) {
		return 0
	}
	if (*(*_Edge)(unsafe.Pointer(e + 1*16))).Fdest == d && !((*(*_Edge)(unsafe.Pointer(e + 1*16))).Fdead != 0) {
		return 0
	}
	return int32(1)
}

func s_visitphi(tls *libc.TLS, qbe *_QBE, p uintptr, n int32, fn uintptr) {
	var a _uint
	var v int32
	_, _ = a, v
	v = int32(_Top)
	a = uint32(0)
	for {
		if !(a < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
			break
		}
		if !(s_deadedge(tls, qbe, libc.Int32FromUint32((*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)))).Fid), n) != 0) {
			v = s_latmerge(tls, qbe, v, s_latval(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a)*4))))
		}
		goto _1
	_1:
		;
		a++
	}
	s_update(tls, qbe, int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3), v, fn)
}

func s_visitins(tls *libc.TLS, qbe *_QBE, i uintptr, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var l, r1, v, v10, v4, v6 int32
	var v1, v2, v3, v8, v9 _Ref
	_, _, _, _, _, _, _, _, _, _, _ = l, r1, v, v1, v10, v2, v3, v4, v6, v8, v9
	v1 = (*_Ins)(unsafe.Pointer(i)).Fto
	*(*_Ref)(unsafe.Pointer(bp + 12)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RTmp) {
		return
	}
	if int32(_uint(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_optab)) + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))*32 + 24))&0x1>>0)) != 0 {
		l = s_latval(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)))
		v8 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
		v9 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v8
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v9
		v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3))
		goto _11
	_11:
		if !(v10 != 0) {
			r1 = s_latval(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)))
		} else {
			*(*_Ref)(unsafe.Pointer(bp)) = _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
			}
			r1 = int32(*(*uint32)(unsafe.Pointer(bp + 0)) & 0xfffffff8 >> 3)
		}
		if l == int32(_Bot) || r1 == int32(_Bot) {
			v = int32(_Bot)
		} else {
			if l == int32(_Top) || r1 == int32(_Top) {
				v = int32(_Top)
			} else {
				v = s_opfold(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(l)*32, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(r1)*32, fn)
			}
		}
	} else {
		v = int32(_Bot)
	}
	/* fprintf(stderr, "\nvisiting %s (%p)", optab[i->op].name, (void *)i); */
	s_update(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3), v, fn)
}

func s_visitjmp(tls *libc.TLS, qbe *_QBE, b uintptr, n int32, fn uintptr) {
	var l int32
	_ = l
	switch int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) {
	case int32(_Jjnz):
		l = s_latval(tls, qbe, (*_Blk)(unsafe.Pointer(b)).Fjmp.Farg)
		if l == int32(_Bot) {
			(*(*_Edge)(unsafe.Pointer(qbe.s_edge + uintptr(n)*32 + 1*16))).Fwork = qbe.s_flowrk
			(*(*_Edge)(unsafe.Pointer(qbe.s_edge + uintptr(n)*32))).Fwork = qbe.s_edge + uintptr(n)*32 + 1*16
			qbe.s_flowrk = qbe.s_edge + uintptr(n)*32
		} else {
			if s_iscon1(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(l)*32, 0, uint64(0)) != 0 {
				(*(*_Edge)(unsafe.Pointer(qbe.s_edge + uintptr(n)*32 + 1*16))).Fwork = qbe.s_flowrk
				qbe.s_flowrk = qbe.s_edge + uintptr(n)*32 + 1*16
			} else {
				(*(*_Edge)(unsafe.Pointer(qbe.s_edge + uintptr(n)*32))).Fwork = qbe.s_flowrk
				qbe.s_flowrk = qbe.s_edge + uintptr(n)*32
			}
		}
	case int32(_Jjmp):
		(*(*_Edge)(unsafe.Pointer(qbe.s_edge + uintptr(n)*32))).Fwork = qbe.s_flowrk
		qbe.s_flowrk = qbe.s_edge + uintptr(n)*32
	case int32(_Jhlt):
	default:
		if libc.Uint32FromInt16((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)-uint32(_Jretw) <= libc.Uint32FromInt32(int32(_Jret0)-int32(_Jretw)) {
			break
		}
		_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
	}
}

func s_initedge(tls *libc.TLS, qbe *_QBE, e uintptr, s uintptr) {
	if s != 0 {
		(*_Edge)(unsafe.Pointer(e)).Fdest = libc.Int32FromUint32((*_Blk)(unsafe.Pointer(s)).Fid)
	} else {
		(*_Edge)(unsafe.Pointer(e)).Fdest = -int32(1)
	}
	(*_Edge)(unsafe.Pointer(e)).Fdead = int32(1)
	(*_Edge)(unsafe.Pointer(e)).Fwork = uintptr(0)
}

func s_renref(tls *libc.TLS, qbe *_QBE, r1 uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var l, v4, v6, v8 int32
	var v1, v2, v3 _Ref
	_, _, _, _, _, _, _ = l, v1, v2, v3, v4, v6, v8
	v1 = *(*_Ref)(unsafe.Pointer(r1))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		v8 = *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(int32(*(*uint32)(unsafe.Pointer(r1 + 0))&0xfffffff8>>3))*4))
		l = v8
		if v8 != int32(_Bot) {
			*(*_Ref)(unsafe.Pointer(r1)) = _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(l)&0x1fffffff<<3,
			}
			return int32(1)
		}
	}
	return 0
}

// C documentation
//
//	/* require rpo, use, pred */
func x_fold(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var a1, n, v6 _uint
	var b1, e, i, p, pb, pp, u, v11, v9 uintptr
	var d, t, v17, v22, v24 int32
	var v15, v16, v19, v20, v21 _Ref
	var v26 bool
	var _ /* start at bp+16 */ _Edge
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, b1, d, e, i, n, p, pb, pp, t, u, v11, v15, v16, v17, v19, v20, v21, v22, v24, v26, v6, v9
	qbe.s_val = x_emalloc(tls, qbe, libc.Uint64FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp)*uint64(4))
	qbe.s_edge = x_emalloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(32))
	qbe.s_usewrk = x_vnew(tls, qbe, uint64(0), uint64(8), int32(_PHeap))
	t = 0
	for {
		if !(t < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
			break
		}
		*(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)) = int32(_Top)
		goto _1
	_1:
		;
		t++
	}
	n = uint32(0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
		(*_Blk)(unsafe.Pointer(b1)).Fvisit = uint32(0)
		s_initedge(tls, qbe, qbe.s_edge+uintptr(n)*32, (*_Blk)(unsafe.Pointer(b1)).Fs1)
		s_initedge(tls, qbe, qbe.s_edge+uintptr(n)*32+1*16, (*_Blk)(unsafe.Pointer(b1)).Fs2)
		goto _2
	_2:
		;
		n++
	}
	s_initedge(tls, qbe, bp+16, (*_Fn)(unsafe.Pointer(fn)).Fstart)
	qbe.s_flowrk = bp + 16
	qbe.s_nuse = uint32(0)
	/* 1. find out constants and dead cfg edges */
	for {
		e = qbe.s_flowrk
		if e != 0 {
			qbe.s_flowrk = (*_Edge)(unsafe.Pointer(e)).Fwork
			(*_Edge)(unsafe.Pointer(e)).Fwork = uintptr(0)
			if (*_Edge)(unsafe.Pointer(e)).Fdest == -int32(1) || !((*_Edge)(unsafe.Pointer(e)).Fdead != 0) {
				goto _3
			}
			(*_Edge)(unsafe.Pointer(e)).Fdead = 0
			n = libc.Uint32FromInt32((*_Edge)(unsafe.Pointer(e)).Fdest)
			b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
			p = (*_Blk)(unsafe.Pointer(b1)).Fphi
			for {
				if !(p != 0) {
					break
				}
				s_visitphi(tls, qbe, p, libc.Int32FromUint32(n), fn)
				goto _4
			_4:
				;
				p = (*_Phi)(unsafe.Pointer(p)).Flink
			}
			if (*_Blk)(unsafe.Pointer(b1)).Fvisit == uint32(0) {
				i = (*_Blk)(unsafe.Pointer(b1)).Fins
				for {
					if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
						break
					}
					s_visitins(tls, qbe, i, fn)
					goto _5
				_5:
					;
					i += 16
				}
				s_visitjmp(tls, qbe, b1, libc.Int32FromUint32(n), fn)
			}
			(*_Blk)(unsafe.Pointer(b1)).Fvisit++
		} else {
			if qbe.s_nuse != 0 {
				qbe.s_nuse--
				v6 = qbe.s_nuse
				u = *(*uintptr)(unsafe.Pointer(qbe.s_usewrk + uintptr(v6)*8))
				n = (*_Use)(unsafe.Pointer(u)).Fbid
				b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
				if (*_Blk)(unsafe.Pointer(b1)).Fvisit == uint32(0) {
					goto _3
				}
				switch (*_Use)(unsafe.Pointer(u)).Ftype1 {
				case int32(_UPhi):
					s_visitphi(tls, qbe, *(*uintptr)(unsafe.Pointer(u + 8)), libc.Int32FromUint32((*_Use)(unsafe.Pointer(u)).Fbid), fn)
				case int32(_UIns):
					s_visitins(tls, qbe, *(*uintptr)(unsafe.Pointer(u + 8)), fn)
				case int32(_UJmp):
					s_visitjmp(tls, qbe, b1, libc.Int32FromUint32(n), fn)
				default:
					_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
				}
			} else {
				break
			}
		}
		goto _3
	_3:
	}
	if qbe.x_debug[int32('F')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3856, 0)
		t = int32(_Tmp0)
		for {
			if !(t < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
				break
			}
			if *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)) == int32(_Bot) {
				goto _7
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3874, libc.VaList(bp+40, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(t)*192))
			if *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)) == int32(_Top) {
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3882, 0)
			} else {
				x_printref(tls, qbe, _Ref{
					F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(qbe.s_val + uintptr(t)*4)))&0x1fffffff<<3,
				}, fn, libc.X__stderrp)
			}
			goto _7
		_7:
			;
			t++
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3886, 0)
	}
	/* 2. trim dead code, replace constants */
	d = 0
	pb = fn
	for {
		v9 = *(*uintptr)(unsafe.Pointer(pb))
		b1 = v9
		if !(v9 != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(b1)).Fvisit == uint32(0) {
			d = int32(1)
			if qbe.x_debug[int32('F')] != 0 {
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3900, libc.VaList(bp+40, b1+180))
			}
			x_edgedel(tls, qbe, b1, b1+32)
			x_edgedel(tls, qbe, b1, b1+40)
			*(*uintptr)(unsafe.Pointer(pb)) = (*_Blk)(unsafe.Pointer(b1)).Flink
			goto _8
		}
		pp = b1
		for {
			v11 = *(*uintptr)(unsafe.Pointer(pp))
			p = v11
			if !(v11 != 0) {
				break
			}
			if *(*int32)(unsafe.Pointer(qbe.s_val + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*4)) != int32(_Bot) {
				*(*uintptr)(unsafe.Pointer(pp)) = (*_Phi)(unsafe.Pointer(p)).Flink
			} else {
				a1 = uint32(0)
				for {
					if !(a1 < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
						break
					}
					if !(s_deadedge(tls, qbe, libc.Int32FromUint32((*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a1)*8)))).Fid), libc.Int32FromUint32((*_Blk)(unsafe.Pointer(b1)).Fid)) != 0) {
						s_renref(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(a1)*4)
					}
					goto _12
				_12:
					;
					a1++
				}
				pp = p + 32
			}
			goto _10
		_10:
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			if s_renref(tls, qbe, i+4) != 0 {
				*(*_Ins)(unsafe.Pointer(i)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
			} else {
				n = uint32(0)
				for {
					if !(n < uint32(2)) {
						break
					}
					s_renref(tls, qbe, i+8+uintptr(n)*4)
					goto _14
				_14:
					;
					n++
				}
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
					v15 = *(*_Ref)(unsafe.Pointer(i + 8))
					v16 = _Ref{
						F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(0)&0x1fffffff<<3,
					}
					*(*_Ref)(unsafe.Pointer(bp)) = v15
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v16
					v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _18
				_18:
					if v17 != 0 {
						*(*_Ins)(unsafe.Pointer(i)) = _Ins{
							F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
						}
					}
				}
			}
			goto _13
		_13:
			;
			i += 16
		}
		s_renref(tls, qbe, b1+20+4)
		if v26 = int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jjnz); v26 {
			v19 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v19
			v20 = v19
			v21 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v20
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v21
			v22 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _23
		_23:
			if v22 != 0 {
				v24 = -int32(1)
				goto _25
			}
			v24 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _25
		_25:
		}
		if v26 && v24 == int32(_RCon) {
			if s_iscon1(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(b1 + 20 + 4 + 0))&0xfffffff8>>3))*32, 0, uint64(0)) != 0 {
				x_edgedel(tls, qbe, b1, b1+32)
				(*_Blk)(unsafe.Pointer(b1)).Fs1 = (*_Blk)(unsafe.Pointer(b1)).Fs2
				(*_Blk)(unsafe.Pointer(b1)).Fs2 = uintptr(0)
			} else {
				x_edgedel(tls, qbe, b1, b1+40)
			}
			(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(_Jjmp)
			(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = _Ref{}
		}
		pb = b1 + 48
		goto _8
	_8:
	}
	if qbe.x_debug[int32('F')] != 0 {
		if !(d != 0) {
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3904, 0)
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3911, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
	free(tls, qbe, qbe.s_val)
	free(tls, qbe, qbe.s_edge)
	x_vfree(tls, qbe, qbe.s_usewrk)
}

/* boring folding code */

func s_foldint(tls *libc.TLS, qbe *_QBE, res uintptr, op int32, w int32, cl uintptr, cr uintptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var typ int32
	var x _uint64_t
	var v1, v10, v2, v3, v6, v8 int64
	var v11, v4, v5, v7, v9 uint64
	var _ /* l at bp+0 */ struct {
		Fu  [0]_uint64_t
		Ffs [0]float32
		Ffd [0]float64
		Fs  _int64_t
	}
	var _ /* r at bp+8 */ struct {
		Fu  [0]_uint64_t
		Ffs [0]float32
		Ffd [0]float64
		Fs  _int64_t
	}
	var _ /* sym at bp+16 */ _Sym
	_, _, _, _, _, _, _, _, _, _, _, _, _ = typ, x, v1, v10, v11, v2, v3, v4, v5, v6, v7, v8, v9
	libc.X__builtin___memset_chk(tls, bp+16, 0, uint64(8), ^___predefined_size_t(0))
	typ = int32(_CBits)
	*(*_int64_t)(unsafe.Pointer(bp)) = *(*_int64_t)(unsafe.Pointer(cl + 16))
	*(*_int64_t)(unsafe.Pointer(bp + 8)) = *(*_int64_t)(unsafe.Pointer(cr + 16))
	if op == int32(_Oadd) {
		if (*_Con)(unsafe.Pointer(cl)).Ftype1 == int32(_CAddr) {
			if (*_Con)(unsafe.Pointer(cr)).Ftype1 == int32(_CAddr) {
				return int32(1)
			}
			typ = int32(_CAddr)
			*(*_Sym)(unsafe.Pointer(bp + 16)) = (*_Con)(unsafe.Pointer(cl)).Fsym
		} else {
			if (*_Con)(unsafe.Pointer(cr)).Ftype1 == int32(_CAddr) {
				typ = int32(_CAddr)
				*(*_Sym)(unsafe.Pointer(bp + 16)) = (*_Con)(unsafe.Pointer(cr)).Fsym
			}
		}
	} else {
		if op == int32(_Osub) {
			if (*_Con)(unsafe.Pointer(cl)).Ftype1 == int32(_CAddr) {
				if (*_Con)(unsafe.Pointer(cr)).Ftype1 != int32(_CAddr) {
					typ = int32(_CAddr)
					*(*_Sym)(unsafe.Pointer(bp + 16)) = (*_Con)(unsafe.Pointer(cl)).Fsym
				} else {
					if !(x_symeq(tls, qbe, (*_Con)(unsafe.Pointer(cl)).Fsym, (*_Con)(unsafe.Pointer(cr)).Fsym) != 0) {
						return int32(1)
					}
				}
			} else {
				if (*_Con)(unsafe.Pointer(cr)).Ftype1 == int32(_CAddr) {
					return int32(1)
				}
			}
		} else {
			if (*_Con)(unsafe.Pointer(cl)).Ftype1 == int32(_CAddr) || (*_Con)(unsafe.Pointer(cr)).Ftype1 == int32(_CAddr) {
				return int32(1)
			}
		}
	}
	if op == int32(_Odiv) || op == int32(_Orem) || op == int32(_Oudiv) || op == int32(_Ourem) {
		if s_iscon1(tls, qbe, cr, w, uint64(0)) != 0 {
			return int32(1)
		}
		if op == int32(_Odiv) || op == int32(_Orem) {
			if w != 0 {
				v1 = -libc.Int64FromInt64(9223372036854775807) - libc.Int64FromInt32(1)
			} else {
				v1 = int64(-libc.Int32FromInt32(2147483647) - libc.Int32FromInt32(1))
			}
			x = libc.Uint64FromInt64(v1)
			if s_iscon1(tls, qbe, cr, w, libc.Uint64FromInt32(-libc.Int32FromInt32(1))) != 0 {
				if s_iscon1(tls, qbe, cl, w, x) != 0 {
					return int32(1)
				}
			}
		}
	}
	switch op {
	case int32(_Oadd):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) + *(*_uint64_t)(unsafe.Pointer(bp + 8))
	case int32(_Osub):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) - *(*_uint64_t)(unsafe.Pointer(bp + 8))
	case int32(_Oneg):
		x = -*(*_uint64_t)(unsafe.Pointer(bp))
	case int32(_Odiv):
		if w != 0 {
			v2 = *(*_int64_t)(unsafe.Pointer(bp)) / *(*_int64_t)(unsafe.Pointer(bp + 8))
		} else {
			v2 = int64(int32(*(*_int64_t)(unsafe.Pointer(bp))) / int32(*(*_int64_t)(unsafe.Pointer(bp + 8))))
		}
		x = libc.Uint64FromInt64(v2)
	case int32(_Orem):
		if w != 0 {
			v3 = *(*_int64_t)(unsafe.Pointer(bp)) % *(*_int64_t)(unsafe.Pointer(bp + 8))
		} else {
			v3 = int64(int32(*(*_int64_t)(unsafe.Pointer(bp))) % int32(*(*_int64_t)(unsafe.Pointer(bp + 8))))
		}
		x = libc.Uint64FromInt64(v3)
	case int32(_Oudiv):
		if w != 0 {
			v4 = *(*_uint64_t)(unsafe.Pointer(bp)) / *(*_uint64_t)(unsafe.Pointer(bp + 8))
		} else {
			v4 = uint64(uint32(*(*_uint64_t)(unsafe.Pointer(bp))) / uint32(*(*_uint64_t)(unsafe.Pointer(bp + 8))))
		}
		x = v4
	case int32(_Ourem):
		if w != 0 {
			v5 = *(*_uint64_t)(unsafe.Pointer(bp)) % *(*_uint64_t)(unsafe.Pointer(bp + 8))
		} else {
			v5 = uint64(uint32(*(*_uint64_t)(unsafe.Pointer(bp))) % uint32(*(*_uint64_t)(unsafe.Pointer(bp + 8))))
		}
		x = v5
	case int32(_Omul):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) * *(*_uint64_t)(unsafe.Pointer(bp + 8))
	case int32(_Oand):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) & *(*_uint64_t)(unsafe.Pointer(bp + 8))
	case int32(_Oor):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) | *(*_uint64_t)(unsafe.Pointer(bp + 8))
	case int32(_Oxor):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) ^ *(*_uint64_t)(unsafe.Pointer(bp + 8))
	case int32(_Osar):
		if w != 0 {
			v6 = *(*_int64_t)(unsafe.Pointer(bp))
		} else {
			v6 = int64(int32(*(*_int64_t)(unsafe.Pointer(bp))))
		}
		x = libc.Uint64FromInt64(v6 >> (*(*_uint64_t)(unsafe.Pointer(bp + 8)) & libc.Uint64FromInt32(libc.Int32FromInt32(31)|w<<libc.Int32FromInt32(5))))
	case int32(_Oshr):
		if w != 0 {
			v7 = *(*_uint64_t)(unsafe.Pointer(bp))
		} else {
			v7 = uint64(uint32(*(*_uint64_t)(unsafe.Pointer(bp))))
		}
		x = v7 >> (*(*_uint64_t)(unsafe.Pointer(bp + 8)) & libc.Uint64FromInt32(libc.Int32FromInt32(31)|w<<libc.Int32FromInt32(5)))
	case int32(_Oshl):
		x = *(*_uint64_t)(unsafe.Pointer(bp)) << (*(*_uint64_t)(unsafe.Pointer(bp + 8)) & libc.Uint64FromInt32(libc.Int32FromInt32(31)|w<<libc.Int32FromInt32(5)))
	case int32(_Oextsb):
		x = libc.Uint64FromInt8(libc.Int8FromUint64(*(*_uint64_t)(unsafe.Pointer(bp))))
	case int32(_Oextub):
		x = uint64(uint8(*(*_uint64_t)(unsafe.Pointer(bp))))
	case int32(_Oextsh):
		x = libc.Uint64FromInt16(libc.Int16FromUint64(*(*_uint64_t)(unsafe.Pointer(bp))))
	case int32(_Oextuh):
		x = uint64(uint16(*(*_uint64_t)(unsafe.Pointer(bp))))
	case int32(_Oextsw):
		x = libc.Uint64FromInt32(libc.Int32FromUint64(*(*_uint64_t)(unsafe.Pointer(bp))))
	case int32(_Oextuw):
		x = uint64(uint32(*(*_uint64_t)(unsafe.Pointer(bp))))
	case int32(_Ostosi):
		if w != 0 {
			v8 = int64(*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cl)).Fbits)))
		} else {
			v8 = int64(int32(*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cl)).Fbits))))
		}
		x = libc.Uint64FromInt64(v8)
	case int32(_Ostoui):
		if w != 0 {
			v9 = uint64(*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cl)).Fbits)))
		} else {
			v9 = uint64(uint32(*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cl)).Fbits))))
		}
		x = v9
	case int32(_Odtosi):
		if w != 0 {
			v10 = int64(*(*float64)(unsafe.Pointer(cl + 16)))
		} else {
			v10 = int64(int32(*(*float64)(unsafe.Pointer(cl + 16))))
		}
		x = libc.Uint64FromInt64(v10)
	case int32(_Odtoui):
		if w != 0 {
			v11 = uint64(*(*float64)(unsafe.Pointer(cl + 16)))
		} else {
			v11 = uint64(uint32(*(*float64)(unsafe.Pointer(cl + 16))))
		}
		x = v11
	case int32(_Ocast):
		x = *(*_uint64_t)(unsafe.Pointer(bp))
		if (*_Con)(unsafe.Pointer(cl)).Ftype1 == int32(_CAddr) {
			typ = int32(_CAddr)
			*(*_Sym)(unsafe.Pointer(bp + 16)) = (*_Con)(unsafe.Pointer(cl)).Fsym
		}
	default:
		if int32(_Ocmpw) <= op && op <= int32(_Ocmpl1) {
			if op <= int32(_Ocmpw1) {
				*(*_uint64_t)(unsafe.Pointer(bp)) = libc.Uint64FromInt32(libc.Int32FromUint64(*(*_uint64_t)(unsafe.Pointer(bp))))
				*(*_uint64_t)(unsafe.Pointer(bp + 8)) = libc.Uint64FromInt32(libc.Int32FromUint64(*(*_uint64_t)(unsafe.Pointer(bp + 8))))
			} else {
				op -= int32(_Ocmpl) - int32(_Ocmpw)
			}
			switch op - int32(_Ocmpw) {
			case int32(_Ciule):
				x = libc.BoolUint64(*(*_uint64_t)(unsafe.Pointer(bp)) <= *(*_uint64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Ciult):
				x = libc.BoolUint64(*(*_uint64_t)(unsafe.Pointer(bp)) < *(*_uint64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Cisle):
				x = libc.BoolUint64(*(*_int64_t)(unsafe.Pointer(bp)) <= *(*_int64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Cislt):
				x = libc.BoolUint64(*(*_int64_t)(unsafe.Pointer(bp)) < *(*_int64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Cisgt):
				x = libc.BoolUint64(*(*_int64_t)(unsafe.Pointer(bp)) > *(*_int64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Cisge):
				x = libc.BoolUint64(*(*_int64_t)(unsafe.Pointer(bp)) >= *(*_int64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Ciugt):
				x = libc.BoolUint64(*(*_uint64_t)(unsafe.Pointer(bp)) > *(*_uint64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Ciuge):
				x = libc.BoolUint64(*(*_uint64_t)(unsafe.Pointer(bp)) >= *(*_uint64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Cieq):
				x = libc.BoolUint64(*(*_uint64_t)(unsafe.Pointer(bp)) == *(*_uint64_t)(unsafe.Pointer(bp + 8)))
			case int32(_Cine):
				x = libc.BoolUint64(*(*_uint64_t)(unsafe.Pointer(bp)) != *(*_uint64_t)(unsafe.Pointer(bp + 8)))
			default:
				_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
			}
		} else {
			if int32(_Ocmps) <= op && op <= int32(_Ocmps1) {
				switch op - int32(_Ocmps) {
				case int32(_Cfle):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) <= *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cflt):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) < *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cfgt):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) > *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cfge):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) >= *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cfne):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) != *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cfeq):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) == *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cfo):
					x = libc.BoolUint64(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) < *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))) || *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) >= *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))))
				case int32(_Cfuo):
					x = libc.BoolUint64(!(*(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) < *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8)))) || *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp)))) >= *(*float32)(unsafe.Pointer(&*(*struct {
						Fu  [0]_uint64_t
						Ffs [0]float32
						Ffd [0]float64
						Fs  _int64_t
					})(unsafe.Pointer(bp + 8))))))
				default:
					_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
				}
			} else {
				if int32(_Ocmpd) <= op && op <= int32(_Ocmpd1) {
					switch op - int32(_Ocmpd) {
					case int32(_Cfle):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) <= *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cflt):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) < *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cfgt):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) > *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cfge):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) >= *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cfne):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) != *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cfeq):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) == *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cfo):
						x = libc.BoolUint64(*(*float64)(unsafe.Pointer(bp)) < *(*float64)(unsafe.Pointer(bp + 8)) || *(*float64)(unsafe.Pointer(bp)) >= *(*float64)(unsafe.Pointer(bp + 8)))
					case int32(_Cfuo):
						x = libc.BoolUint64(!(*(*float64)(unsafe.Pointer(bp)) < *(*float64)(unsafe.Pointer(bp + 8)) || *(*float64)(unsafe.Pointer(bp)) >= *(*float64)(unsafe.Pointer(bp + 8))))
					default:
						_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
					}
				} else {
					_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
				}
			}
		}
	}
	*(*_Con)(unsafe.Pointer(res)) = _Con{
		Ftype1: typ,
		Fsym:   *(*_Sym)(unsafe.Pointer(bp + 16)),
		Fbits: *(*struct {
			Fd [0]float64
			Fs [0]float32
			Fi _int64_t
		})(unsafe.Pointer(&struct{ f _int64_t }{f: libc.Int64FromUint64(x)})),
	}
	return 0
}

func s_foldflt(tls *libc.TLS, qbe *_QBE, res uintptr, op int32, w int32, cl uintptr, cr uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var ld, rd, xd float64
	var ls, rs, xs float32
	_, _, _, _, _, _ = ld, ls, rd, rs, xd, xs
	if (*_Con)(unsafe.Pointer(cl)).Ftype1 != int32(_CBits) || (*_Con)(unsafe.Pointer(cr)).Ftype1 != int32(_CBits) {
		_err(tls, qbe, __ccgo_ts+3940, libc.VaList(bp+8, qbe.x_optab[op].Fname))
	}
	*(*_Con)(unsafe.Pointer(res)) = _Con{
		Ftype1: int32(_CBits),
	}
	libc.X__builtin___memset_chk(tls, res+16, 0, uint64(8), ^___predefined_size_t(0))
	if w != 0 {
		ld = *(*float64)(unsafe.Pointer(cl + 16))
		rd = *(*float64)(unsafe.Pointer(cr + 16))
		switch op {
		case int32(_Oadd):
			xd = ld + rd
		case int32(_Osub):
			xd = ld - rd
		case int32(_Oneg):
			xd = -ld
		case int32(_Odiv):
			xd = ld / rd
		case int32(_Omul):
			xd = ld * rd
		case int32(_Oswtof):
			xd = float64(int32(*(*_int64_t)(unsafe.Pointer(cl + 16))))
		case int32(_Ouwtof):
			xd = float64(libc.Uint32FromInt64(*(*_int64_t)(unsafe.Pointer(cl + 16))))
		case int32(_Osltof):
			xd = float64(*(*_int64_t)(unsafe.Pointer(cl + 16)))
		case int32(_Oultof):
			xd = float64(libc.Uint64FromInt64(*(*_int64_t)(unsafe.Pointer(cl + 16))))
		case int32(_Oexts):
			xd = float64(*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cl)).Fbits)))
		case int32(_Ocast):
			xd = ld
		default:
			_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
		}
		*(*float64)(unsafe.Pointer(res + 16)) = xd
		(*_Con)(unsafe.Pointer(res)).Fflt = int8(2)
	} else {
		ls = *(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cl)).Fbits))
		rs = *(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(cr)).Fbits))
		switch op {
		case int32(_Oadd):
			xs = ls + rs
		case int32(_Osub):
			xs = ls - rs
		case int32(_Oneg):
			xs = -ls
		case int32(_Odiv):
			xs = ls / rs
		case int32(_Omul):
			xs = ls * rs
		case int32(_Oswtof):
			xs = float32(int32(*(*_int64_t)(unsafe.Pointer(cl + 16))))
		case int32(_Ouwtof):
			xs = float32(libc.Uint32FromInt64(*(*_int64_t)(unsafe.Pointer(cl + 16))))
		case int32(_Osltof):
			xs = float32(*(*_int64_t)(unsafe.Pointer(cl + 16)))
		case int32(_Oultof):
			xs = float32(libc.Uint64FromInt64(*(*_int64_t)(unsafe.Pointer(cl + 16))))
		case int32(_Otruncd):
			xs = float32(*(*float64)(unsafe.Pointer(cl + 16)))
		case int32(_Ocast):
			xs = ls
		default:
			_die_(tls, qbe, __ccgo_ts+3849, __ccgo_ts+3532, 0)
		}
		*(*float32)(unsafe.Pointer(&(*_Con)(unsafe.Pointer(res)).Fbits)) = xs
		(*_Con)(unsafe.Pointer(res)).Fflt = int8(1)
	}
}

func s_opfold(tls *libc.TLS, qbe *_QBE, op int32, cls int32, cl uintptr, cr uintptr, fn uintptr) (r int32) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var _ /* c at bp+8 */ _Con
	var _ /* r at bp+0 */ _Ref
	if cls == int32(_Kw) || cls == int32(_Kl) {
		if s_foldint(tls, qbe, bp+8, op, libc.BoolInt32(cls == int32(_Kl)), cl, cr) != 0 {
			return int32(_Bot)
		}
	} else {
		s_foldflt(tls, qbe, bp+8, op, libc.BoolInt32(cls == int32(_Kd)), cl, cr)
	}
	if !(cls&libc.Int32FromInt32(1) != 0) {
		*(*_int64_t)(unsafe.Pointer(bp + 8 + 16)) &= libc.Int64FromUint32(0xffffffff)
	}
	*(*_Ref)(unsafe.Pointer(bp)) = x_newcon(tls, qbe, bp+8, fn)
	return int32(*(*uint32)(unsafe.Pointer(bp + 0)) & 0xfffffff8 >> 3)
}

func s_blit(tls *libc.TLS, qbe *_QBE, sd uintptr, sz int32, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var fwd, n, off, v1, v4, v5 int32
	var p uintptr
	var r, r1, ro _Ref
	var _ /* tbl at bp+0 */ [4]struct {
		Fst   int32
		Fld   int32
		Fcls  int32
		Fsize int32
	}
	_, _, _, _, _, _, _, _, _, _ = fwd, n, off, p, r, r1, ro, v1, v4, v5
	*(*[4]struct {
		Fst   int32
		Fld   int32
		Fcls  int32
		Fsize int32
	})(unsafe.Pointer(bp)) = [4]struct {
		Fst   int32
		Fld   int32
		Fcls  int32
		Fsize int32
	}{
		0: {
			Fst:   int32(_Ostorel),
			Fld:   int32(_Oload),
			Fcls:  int32(_Kl),
			Fsize: int32(8),
		},
		1: {
			Fst:   int32(_Ostorew),
			Fld:   int32(_Oload),
			Fsize: int32(4),
		},
		2: {
			Fst:   int32(_Ostoreh),
			Fld:   int32(_Oloaduh),
			Fsize: int32(2),
		},
		3: {
			Fst:   int32(_Ostoreb),
			Fld:   int32(_Oloadub),
			Fsize: int32(1),
		},
	}
	fwd = libc.BoolInt32(sz >= 0)
	sz = libc.Xabs(tls, sz)
	if fwd != 0 {
		v1 = sz
	} else {
		v1 = 0
	}
	off = v1
	p = bp
	for {
		if !(sz != 0) {
			break
		}
		n = (*struct {
			Fst   int32
			Fld   int32
			Fcls  int32
			Fsize int32
		})(unsafe.Pointer(p)).Fsize
		for {
			if !(sz >= n) {
				break
			}
			if fwd != 0 {
				v4 = n
			} else {
				v4 = 0
			}
			off -= v4
			r = x_newtmp(tls, qbe, __ccgo_ts+3973, int32(_Kl), fn)
			r1 = x_newtmp(tls, qbe, __ccgo_ts+3973, int32(_Kl), fn)
			ro = x_getcon(tls, qbe, int64(off), fn)
			x_emit(tls, qbe, (*struct {
				Fst   int32
				Fld   int32
				Fcls  int32
				Fsize int32
			})(unsafe.Pointer(p)).Fst, 0, _Ref{}, r, r1)
			x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(sd + 1*4)), ro)
			r1 = x_newtmp(tls, qbe, __ccgo_ts+3973, int32(_Kl), fn)
			x_emit(tls, qbe, (*struct {
				Fst   int32
				Fld   int32
				Fcls  int32
				Fsize int32
			})(unsafe.Pointer(p)).Fld, (*struct {
				Fst   int32
				Fld   int32
				Fcls  int32
				Fsize int32
			})(unsafe.Pointer(p)).Fcls, r, r1, _Ref{})
			x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(sd)), ro)
			if fwd != 0 {
				v5 = 0
			} else {
				v5 = n
			}
			off += v5
			goto _3
		_3:
			;
			sz -= n
		}
		goto _2
	_2:
		;
		p += 16
	}
}

func s_ulog2(tls *libc.TLS, qbe *_QBE, pow2 _uint64_t) (r int32) {
	return qbe.s_ulog2_tab64[pow2*uint64(0x5b31ab928877a7e)>>int32(58)]
}

func s_ispow2(tls *libc.TLS, qbe *_QBE, v _uint64_t) (r int32) {
	return libc.BoolInt32(v != 0 && v&(v-uint64(1)) == uint64(0))
}

func s_ins(tls *libc.TLS, qbe *_QBE, pi uintptr, new1 uintptr, b1 uintptr, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c, i uintptr
	var n, v1, v6, v8 int32
	var ni _ulong
	var v3, v4, v5 _Ref
	var _ /* r at bp+16 */ _Ref
	_, _, _, _, _, _, _, _, _, _ = c, i, n, ni, v1, v3, v4, v5, v6, v8
	i = *(*uintptr)(unsafe.Pointer(pi))
	/* simplify more instructions here;
	 * copy 0 into xor, bit rotations,
	 * etc. */
	switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
	case int32(_Oblit1):
		if !(*(*int32)(unsafe.Pointer(new1)) != 0) {
			qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
			ni = libc.Uint64FromInt64((___predefined_ptrdiff_t((*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) - int64(i+libc.UintptrFromInt32(1)*16)) / 16)
			qbe.x_curi -= uintptr(ni) * 16
			x_icpy(tls, qbe, qbe.x_curi, i+uintptr(1)*16, ni)
			*(*int32)(unsafe.Pointer(new1)) = int32(1)
		}
		*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8))
		v1 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
		goto _2
	_2:
		s_blit(tls, qbe, i-libc.UintptrFromInt32(1)*16+8, v1, fn)
		*(*uintptr)(unsafe.Pointer(pi)) = i - uintptr(1)*16
		return
	case int32(_Oudiv):
		fallthrough
	case int32(_Ourem):
		*(*_Ref)(unsafe.Pointer(bp + 16)) = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
			v3 = *(*_Ref)(unsafe.Pointer(bp + 16))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v3
			v4 = v3
			v5 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v4
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v5
			v6 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _7
		_7:
			if v6 != 0 {
				v8 = -int32(1)
				goto _9
			}
			v8 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _9
		_9:
			if v8 == int32(_RCon) {
				c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*32
				if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CBits) {
					if s_ispow2(tls, qbe, libc.Uint64FromInt64(*(*_int64_t)(unsafe.Pointer(c + 16)))) != 0 {
						n = s_ulog2(tls, qbe, libc.Uint64FromInt64(*(*_int64_t)(unsafe.Pointer(c + 16))))
						if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ourem) {
							libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oand), 0, 0x3fffffff)
							*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = x_getcon(tls, qbe, libc.Int64FromUint64(uint64(1)<<n-uint64(1)), fn)
						} else {
							libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oshr), 0, 0x3fffffff)
							*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = x_getcon(tls, qbe, int64(n), fn)
						}
					}
				}
			}
		}
		break
	}
	if *(*int32)(unsafe.Pointer(new1)) != 0 {
		x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(i)))
	}
}

func x_simpl(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var b uintptr
	var _ /* i at bp+0 */ uintptr
	var _ /* new at bp+8 */ int32
	_ = b
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		*(*int32)(unsafe.Pointer(bp + 8)) = 0
		*(*uintptr)(unsafe.Pointer(bp)) = (*_Blk)(unsafe.Pointer(b)).Fins + uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16
		for {
			if !(*(*uintptr)(unsafe.Pointer(bp)) != (*_Blk)(unsafe.Pointer(b)).Fins) {
				break
			}
			*(*uintptr)(unsafe.Pointer(bp)) -= 16
			s_ins(tls, qbe, bp, bp+8, b, fn)
			goto _2
		_2:
		}
		if *(*int32)(unsafe.Pointer(bp + 8)) != 0 {
			(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
			x_idup(tls, qbe, b+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b)).Fnins))
		}
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
}

func x_liveon(tls *libc.TLS, qbe *_QBE, v uintptr, b1 uintptr, s uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var a1 _uint
	var p uintptr
	var v11, v12, v13, v2, v3, v4 _Ref
	var v14, v16, v5, v7 int32
	_, _, _, _, _, _, _, _, _, _, _, _ = a1, p, v11, v12, v13, v14, v16, v2, v3, v4, v5, v7
	x_bscopy(tls, qbe, v, s+120)
	p = (*_Blk)(unsafe.Pointer(s)).Fphi
	for {
		if !(p != 0) {
			break
		}
		v2 = (*_Phi)(unsafe.Pointer(p)).Fto
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v2
		v3 = v2
		v4 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v3
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v4
		v5 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _6
	_6:
		if v5 != 0 {
			v7 = -int32(1)
			goto _8
		}
		v7 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _8
	_8:
		if v7 == int32(_RTmp) {
			x_bsclr(tls, qbe, v, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3)))
		}
		goto _1
	_1:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
	p = (*_Blk)(unsafe.Pointer(s)).Fphi
	for {
		if !(p != 0) {
			break
		}
		a1 = uint32(0)
		for {
			if !(a1 < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
				break
			}
			if *(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a1)*8)) == b1 {
				v11 = *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4))
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v11
				v12 = v11
				v13 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v12
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v13
				v14 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _15
			_15:
				if v14 != 0 {
					v16 = -int32(1)
					goto _17
				}
				v16 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _17
			_17:
				if v16 == int32(_RTmp) {
					x_bsset(tls, qbe, v, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4 + 0))&0xfffffff8>>3)))
					x_bsset(tls, qbe, b1+152, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a1)*4 + 0))&0xfffffff8>>3)))
				}
			}
			goto _10
		_10:
			;
			a1++
		}
		goto _9
	_9:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
}

func s_bset(tls *libc.TLS, qbe *_QBE, _r _Ref, b1 uintptr, nlv uintptr, tmp uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6, v9 int32
	var v8 _uint
	_, _, _, _, _, _, _ = v1, v2, v3, v4, v6, v8, v9
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RTmp) {
		return
	}
	x_bsset(tls, qbe, b1+152, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3)))
	v8 = libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0)) & 0xfffffff8 >> 3))
	v9 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1+120)).Ft + uintptr(v8/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v8%uint32(_NBit))) != uint64(0))
	goto _10
_10:
	if !(v9 != 0) {
		*(*int32)(unsafe.Pointer(nlv + uintptr(int32((*(*_Tmp)(unsafe.Pointer(tmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))).Fcls)>>libc.Int32FromInt32(1))*4))++
		x_bsset(tls, qbe, b1+120, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3)))
	}
}

// C documentation
//
//	/* liveness analysis
//	 * requires rpo computation
//	 */
func x_filllive(tls *libc.TLS, qbe *_QBE, f uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var b1, i, ma, v13 uintptr
	var chg, k, n, v17, v19, v26, v29, v35, v37, v7, v9 int32
	var v14, v15, v16, v24, v25, v32, v33, v34, v4, v5, v6 _Ref
	var v21 bool
	var v28 _uint
	var _ /* m at bp+16 */ [2]int32
	var _ /* nlv at bp+24 */ [2]int32
	var _ /* t at bp+12 */ int32
	var _ /* u at bp+32 */ [1]_BSet
	var _ /* v at bp+48 */ [1]_BSet
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, chg, i, k, ma, n, v13, v14, v15, v16, v17, v19, v21, v24, v25, v26, v28, v29, v32, v33, v34, v35, v37, v4, v5, v6, v7, v9
	x_bsinit(tls, qbe, bp+32, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(f)).Fntmp))
	x_bsinit(tls, qbe, bp+48, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(f)).Fntmp))
	b1 = (*_Fn)(unsafe.Pointer(f)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		x_bsinit(tls, qbe, b1+120, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(f)).Fntmp))
		x_bsinit(tls, qbe, b1+136, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(f)).Fntmp))
		x_bsinit(tls, qbe, b1+152, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(f)).Fntmp))
		goto _1
	_1:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	chg = int32(1)
	goto Again
Again:
	;
	n = libc.Int32FromUint32((*_Fn)(unsafe.Pointer(f)).Fnblk - uint32(1))
	for {
		if !(n >= 0) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(f)).Frpo + uintptr(n)*8))
		x_bscopy(tls, qbe, bp+32, b1+136)
		if (*_Blk)(unsafe.Pointer(b1)).Fs1 != 0 {
			x_liveon(tls, qbe, bp+48, b1, (*_Blk)(unsafe.Pointer(b1)).Fs1)
			x_bsunion(tls, qbe, b1+136, bp+48)
		}
		if (*_Blk)(unsafe.Pointer(b1)).Fs2 != 0 {
			x_liveon(tls, qbe, bp+48, b1, (*_Blk)(unsafe.Pointer(b1)).Fs2)
			x_bsunion(tls, qbe, b1+136, bp+48)
		}
		chg |= libc.BoolInt32(!(x_bsequal(tls, qbe, b1+136, bp+32) != 0))
		libc.X__builtin___memset_chk(tls, bp+24, 0, uint64(8), ^___predefined_size_t(0))
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1 + 136)).Ft)) |= qbe.x_T.Frglob
		x_bscopy(tls, qbe, b1+120, b1+136)
		*(*int32)(unsafe.Pointer(bp + 12)) = 0
		for {
			if !(x_bsiter(tls, qbe, b1+120, bp+12) != 0) {
				break
			}
			(*(*[2]int32)(unsafe.Pointer(bp + 24)))[int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(f)).Ftmp + uintptr(*(*int32)(unsafe.Pointer(bp + 12)))*192))).Fcls)>>int32(1)]++
			goto _3
		_3:
			;
			*(*int32)(unsafe.Pointer(bp + 12))++
		}
		v4 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v4
		v5 = v4
		v6 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v5
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
		v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _8
	_8:
		if v7 != 0 {
			v9 = -int32(1)
			goto _10
		}
		v9 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _10
	_10:
		if v9 == int32(_RCall) {
			*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1 + 120)).Ft)) |= (*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fretregs})))(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, bp+24)
		} else {
			s_bset(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, b1, bp+24, (*_Fn)(unsafe.Pointer(f)).Ftmp)
		}
		k = 0
		for {
			if !(k < int32(2)) {
				break
			}
			*(*int32)(unsafe.Pointer(b1 + 168 + uintptr(k)*4)) = (*(*[2]int32)(unsafe.Pointer(bp + 24)))[k]
			goto _11
		_11:
			;
			k++
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b1)).Fins) {
				break
			}
			i -= 16
			v13 = i
			if v21 = int32(*(*uint32)(unsafe.Pointer(v13 + 0))&0x3fffffff>>0) == int32(_Ocall); v21 {
				v14 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v14
				v15 = v14
				v16 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v15
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v16
				v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _18
			_18:
				if v17 != 0 {
					v19 = -int32(1)
					goto _20
				}
				v19 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _20
			_20:
			}
			if v21 && v19 == int32(_RCall) {
				*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1 + 120)).Ft)) &= ^(*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fretregs})))(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), bp+16)
				k = 0
				for {
					if !(k < int32(2)) {
						break
					}
					*(*int32)(unsafe.Pointer(bp + 24 + uintptr(k)*4)) -= (*(*[2]int32)(unsafe.Pointer(bp + 16)))[k]
					/* caller-save registers are used
					 * by the callee, in that sense,
					 * right in the middle of the call,
					 * they are live: */
					*(*int32)(unsafe.Pointer(bp + 24 + uintptr(k)*4)) += *(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_T)) + 64 + uintptr(k)*4))
					if (*(*[2]int32)(unsafe.Pointer(bp + 24)))[k] > *(*int32)(unsafe.Pointer(b1 + 168 + uintptr(k)*4)) {
						*(*int32)(unsafe.Pointer(b1 + 168 + uintptr(k)*4)) = (*(*[2]int32)(unsafe.Pointer(bp + 24)))[k]
					}
					goto _22
				_22:
					;
					k++
				}
				*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1 + 120)).Ft)) |= (*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fargregs})))(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), bp+16)
				k = 0
				for {
					if !(k < int32(2)) {
						break
					}
					*(*int32)(unsafe.Pointer(bp + 24 + uintptr(k)*4)) -= *(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_T)) + 64 + uintptr(k)*4))
					*(*int32)(unsafe.Pointer(bp + 24 + uintptr(k)*4)) += (*(*[2]int32)(unsafe.Pointer(bp + 16)))[k]
					goto _23
				_23:
					;
					k++
				}
			}
			v24 = (*_Ins)(unsafe.Pointer(i)).Fto
			v25 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v24
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v25
			v26 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _27
		_27:
			if !(v26 != 0) {
				*(*int32)(unsafe.Pointer(bp + 12)) = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
				v28 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 12)))
				v29 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1+120)).Ft + uintptr(v28/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v28%uint32(_NBit))) != uint64(0))
				goto _30
			_30:
				if v29 != 0 {
					(*(*[2]int32)(unsafe.Pointer(bp + 24)))[int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(f)).Ftmp + uintptr(*(*int32)(unsafe.Pointer(bp + 12)))*192))).Fcls)>>int32(1)]--
				}
				x_bsset(tls, qbe, b1+152, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 12))))
				x_bsclr(tls, qbe, b1+120, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 12))))
			}
			k = 0
			for {
				if !(k < int32(2)) {
					break
				}
				v32 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(k)*4))
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v32
				v33 = v32
				v34 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v33
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v34
				v35 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _36
			_36:
				if v35 != 0 {
					v37 = -int32(1)
					goto _38
				}
				v37 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _38
			_38:
				switch v37 {
				case int32(_RMem):
					ma = (*_Fn)(unsafe.Pointer(f)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(k)*4 + 0))&0xfffffff8>>3))*48
					s_bset(tls, qbe, (*_Mem)(unsafe.Pointer(ma)).Fbase, b1, bp+24, (*_Fn)(unsafe.Pointer(f)).Ftmp)
					s_bset(tls, qbe, (*_Mem)(unsafe.Pointer(ma)).Findex, b1, bp+24, (*_Fn)(unsafe.Pointer(f)).Ftmp)
				default:
					s_bset(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(k)*4)), b1, bp+24, (*_Fn)(unsafe.Pointer(f)).Ftmp)
					break
				}
				goto _31
			_31:
				;
				k++
			}
			k = 0
			for {
				if !(k < int32(2)) {
					break
				}
				if (*(*[2]int32)(unsafe.Pointer(bp + 24)))[k] > *(*int32)(unsafe.Pointer(b1 + 168 + uintptr(k)*4)) {
					*(*int32)(unsafe.Pointer(b1 + 168 + uintptr(k)*4)) = (*(*[2]int32)(unsafe.Pointer(bp + 24)))[k]
				}
				goto _39
			_39:
				;
				k++
			}
			goto _12
		_12:
		}
		goto _2
	_2:
		;
		n--
	}
	if chg != 0 {
		chg = 0
		goto Again
	}
	if qbe.x_debug[int32('L')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+3977, 0)
		b1 = (*_Fn)(unsafe.Pointer(f)).Fstart
		for {
			if !(b1 != 0) {
				break
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4000, libc.VaList(bp+72, b1+180))
			x_dumpts(tls, qbe, b1+120, (*_Fn)(unsafe.Pointer(f)).Ftmp, libc.X__stderrp)
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4013, 0)
			x_dumpts(tls, qbe, b1+136, (*_Fn)(unsafe.Pointer(f)).Ftmp, libc.X__stderrp)
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4031, 0)
			x_dumpts(tls, qbe, b1+152, (*_Fn)(unsafe.Pointer(f)).Ftmp, libc.X__stderrp)
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4049, 0)
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4067, libc.VaList(bp+72, *(*int32)(unsafe.Pointer(b1 + 168)), *(*int32)(unsafe.Pointer(b1 + 168 + 1*4))))
			goto _40
		_40:
			;
			b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
		}
	}
}

func s_aggreg(tls *libc.TLS, qbe *_QBE, hd uintptr, b uintptr) {
	var k int32
	_ = k
	/* aggregate looping information at
	 * loop headers */
	x_bsunion(tls, qbe, hd+152, b+152)
	k = 0
	for {
		if !(k < int32(2)) {
			break
		}
		if *(*int32)(unsafe.Pointer(b + 168 + uintptr(k)*4)) > *(*int32)(unsafe.Pointer(hd + 168 + uintptr(k)*4)) {
			*(*int32)(unsafe.Pointer(hd + 168 + uintptr(k)*4)) = *(*int32)(unsafe.Pointer(b + 168 + uintptr(k)*4))
		}
		goto _1
	_1:
		;
		k++
	}
}

func s_tmpuse(tls *libc.TLS, qbe *_QBE, _r _Ref, use int32, loop int32, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var m, t uintptr
	var v1, v10, v2, v3, v8, v9 _Ref
	var v11, v13, v4, v6 int32
	_, _, _, _, _, _, _, _, _, _, _, _ = m, t, v1, v10, v11, v13, v2, v3, v4, v6, v8, v9
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RMem) {
		m = (*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*48
		s_tmpuse(tls, qbe, (*_Mem)(unsafe.Pointer(m)).Fbase, int32(1), loop, fn)
		s_tmpuse(tls, qbe, (*_Mem)(unsafe.Pointer(m)).Findex, int32(1), loop, fn)
	} else {
		v8 = *(*_Ref)(unsafe.Pointer(bp + 12))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v8
		v9 = v8
		v10 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v9
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
		v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _12
	_12:
		if v11 != 0 {
			v13 = -int32(1)
			goto _14
		}
		v13 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _14
	_14:
		;
		if v13 == int32(_RTmp) && int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3) >= int32(_Tmp0) {
			t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192
			*(*_uint)(unsafe.Pointer(t + 100)) += libc.Uint32FromInt32(use)
			*(*_uint)(unsafe.Pointer(t + 96)) += libc.BoolUint32(!(use != 0))
			*(*_uint)(unsafe.Pointer(t + 108)) += libc.Uint32FromInt32(loop)
		}
	}
}

// C documentation
//
//	/* evaluate spill costs of temporaries,
//	 * this also fills usage information
//	 * requires rpo, preds
//	 */
func x_fillcost(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var a _uint
	var b, i, p, t uintptr
	var n int32
	var v4 uint32
	_, _, _, _, _, _, _ = a, b, i, n, p, t, v4
	x_loopiter(tls, qbe, fn, __ccgo_fp(s_aggreg))
	if qbe.x_debug[int32('S')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4074, 0)
		b = (*_Fn)(unsafe.Pointer(fn)).Fstart
		for {
			if !(b != 0) {
				break
			}
			a = uint32(0)
			for {
				if !(a < (*_Blk)(unsafe.Pointer(b)).Fnpred) {
					break
				}
				if (*_Blk)(unsafe.Pointer(b)).Fid <= (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fpred + uintptr(a)*8)))).Fid {
					break
				}
				goto _2
			_2:
				;
				a++
			}
			if a != (*_Blk)(unsafe.Pointer(b)).Fnpred {
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4096, libc.VaList(bp+8, b+180))
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4103, libc.VaList(bp+8, *(*int32)(unsafe.Pointer(b + 168))))
				fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4111, libc.VaList(bp+8, *(*int32)(unsafe.Pointer(b + 168 + 1*4))))
				x_dumpts(tls, qbe, b+152, (*_Fn)(unsafe.Pointer(fn)).Ftmp, libc.X__stderrp)
			}
			goto _1
		_1:
			;
			b = (*_Blk)(unsafe.Pointer(b)).Flink
		}
	}
	t = (*_Fn)(unsafe.Pointer(fn)).Ftmp
	for {
		if !((int64(t)-int64((*_Fn)(unsafe.Pointer(fn)).Ftmp))/192 < int64((*_Fn)(unsafe.Pointer(fn)).Fntmp)) {
			break
		}
		if (int64(t)-int64((*_Fn)(unsafe.Pointer(fn)).Ftmp))/192 < int64(_Tmp0) {
			v4 = libc.Uint32FromInt32(m___INT_MAX__)*libc.Uint32FromUint32(2) + libc.Uint32FromUint32(1)
		} else {
			v4 = uint32(0)
		}
		(*_Tmp)(unsafe.Pointer(t)).Fcost = v4
		(*_Tmp)(unsafe.Pointer(t)).Fnuse = uint32(0)
		(*_Tmp)(unsafe.Pointer(t)).Fndef = uint32(0)
		goto _3
	_3:
		;
		t += 192
	}
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		p = (*_Blk)(unsafe.Pointer(b)).Fphi
		for {
			if !(p != 0) {
				break
			}
			t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))*192
			s_tmpuse(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Fto, 0, 0, fn)
			a = uint32(0)
			for {
				if !(a < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
					break
				}
				n = (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)))).Floop
				*(*_uint)(unsafe.Pointer(t + 108)) += libc.Uint32FromInt32(n)
				s_tmpuse(tls, qbe, *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(a)*4)), int32(1), n, fn)
				goto _7
			_7:
				;
				a++
			}
			goto _6
		_6:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		n = (*_Blk)(unsafe.Pointer(b)).Floop
		i = (*_Blk)(unsafe.Pointer(b)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
				break
			}
			s_tmpuse(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, 0, n, fn)
			s_tmpuse(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), int32(1), n, fn)
			s_tmpuse(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), int32(1), n, fn)
			goto _8
		_8:
			;
			i += 16
		}
		s_tmpuse(tls, qbe, (*_Blk)(unsafe.Pointer(b)).Fjmp.Farg, int32(1), n, fn)
		goto _5
	_5:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	if qbe.x_debug[int32('S')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4118, 0)
		n = int32(_Tmp0)
		for {
			if !(n < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
				break
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4135, libc.VaList(bp+8, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(n)*192, (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(n)*192))).Fcost))
			goto _9
		_9:
			;
			n++
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+82, 0)
	}
}

func s_tcmp0(tls *libc.TLS, qbe *_QBE, pa uintptr, pb uintptr) (r1 int32) {
	var ca, cb _uint
	var r, v1 int32
	_, _, _, _ = ca, cb, r, v1
	ca = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(pa)))*192))).Fcost
	cb = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(pb)))*192))).Fcost
	if cb < ca {
		v1 = -int32(1)
	} else {
		v1 = libc.BoolInt32(cb > ca)
	}
	//orig: return (cb < ca) ? -1 : (cb > ca);
	//(jnml) stabilize qsort 2072b73865f86b85843bd4514a8fc97cc3200809
	r = v1
	if r != 0 {
		return r
	}
	return *(*int32)(unsafe.Pointer(pa)) - *(*int32)(unsafe.Pointer(pb))
}

func s_tcmp1(tls *libc.TLS, qbe *_QBE, pa uintptr, pb uintptr) (r int32) {
	var c, v2, v5, v7 int32
	var v1, v4 _uint
	_, _, _, _, _, _ = c, v1, v2, v4, v5, v7
	v1 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(pb)))
	v2 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(qbe.s_fst)).Ft + uintptr(v1/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v1%uint32(_NBit))) != uint64(0))
	goto _3
_3:
	v4 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(pa)))
	v5 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(qbe.s_fst)).Ft + uintptr(v4/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v4%uint32(_NBit))) != uint64(0))
	goto _6
_6:
	c = v2 - v5
	if c != 0 {
		v7 = c
	} else {
		v7 = s_tcmp0(tls, qbe, pa, pb)
	}
	return v7
}

func s_slot1(tls *libc.TLS, qbe *_QBE, t int32) (r _Ref) {
	var s int32
	_ = s
	s = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(t)*192))).Fslot
	if s == -int32(1) {
		/* specific to NAlign == 3 */
		/* nice logic to pack stack slots
		 * on demand, there can be only
		 * one hole and slot4 points to it
		 *
		 * invariant: slot4 <= slot8
		 */
		if int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(t)*192))).Fcls)&int32(1) != 0 {
			s = qbe.s_slot8
			if qbe.s_slot4 == qbe.s_slot8 {
				qbe.s_slot4 += int32(2)
			}
			qbe.s_slot8 += int32(2)
		} else {
			s = qbe.s_slot4
			if qbe.s_slot4 == qbe.s_slot8 {
				qbe.s_slot8 += int32(2)
				qbe.s_slot4 += int32(1)
			} else {
				qbe.s_slot4 = qbe.s_slot8
			}
		}
		s += qbe.s_locs
		(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(t)*192))).Fslot = s
	}
	return _Ref{
		F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
	}
}

// C documentation
//
//	/* restricts b to hold at most k
//	 * temporaries, preferring those
//	 * present in f (if given), then
//	 * those with the largest spill
//	 * cost
//	 */
func s_limit(tls *libc.TLS, qbe *_QBE, b uintptr, k int32, f uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i, nt, v2 int32
	var _ /* t at bp+0 */ int32
	_, _, _ = i, nt, v2
	nt = libc.Int32FromUint32(x_bscount(tls, qbe, b))
	if nt <= k {
		return
	}
	if nt > qbe.s_maxt {
		free(tls, qbe, qbe.s_tarr)
		qbe.s_tarr = x_emalloc(tls, qbe, libc.Uint64FromInt32(nt)*uint64(4))
		qbe.s_maxt = nt
	}
	i = 0
	*(*int32)(unsafe.Pointer(bp)) = libc.Int32FromInt32(0)
	for {
		if !(x_bsiter(tls, qbe, b, bp) != 0) {
			break
		}
		x_bsclr(tls, qbe, b, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp))))
		v2 = i
		i++
		*(*int32)(unsafe.Pointer(qbe.s_tarr + uintptr(v2)*4)) = *(*int32)(unsafe.Pointer(bp))
		goto _1
	_1:
		;
		*(*int32)(unsafe.Pointer(bp))++
	}
	if nt > int32(1) {
		if !(f != 0) {
			qsort(tls, qbe, qbe.s_tarr, libc.Uint64FromInt32(nt), uint64(4), __ccgo_fp(s_tcmp0))
		} else {
			qbe.s_fst = f
			qsort(tls, qbe, qbe.s_tarr, libc.Uint64FromInt32(nt), uint64(4), __ccgo_fp(s_tcmp1))
		}
	}
	i = 0
	for {
		if !(i < k && i < nt) {
			break
		}
		x_bsset(tls, qbe, b, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(qbe.s_tarr + uintptr(i)*4))))
		goto _3
	_3:
		;
		i++
	}
	for {
		if !(i < nt) {
			break
		}
		s_slot1(tls, qbe, *(*int32)(unsafe.Pointer(qbe.s_tarr + uintptr(i)*4)))
		goto _4
	_4:
		;
		i++
	}
}

// C documentation
//
//	/* spills temporaries to fit the
//	 * target limits using the same
//	 * preferences as limit(); assumes
//	 * that k1 gprs and k2 fprs are
//	 * currently in use
//	 */
func s_limit2(tls *libc.TLS, qbe *_QBE, b1 uintptr, k1 int32, k2 int32, f uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* b2 at bp+0 */ [1]_BSet
	x_bsinit(tls, qbe, bp, libc.Uint32FromInt32(qbe.s_ntmp)) /* todo, free those */
	x_bscopy(tls, qbe, bp, b1)
	x_bsinter(tls, qbe, b1, uintptr(unsafe.Pointer(&qbe.s_mask1)))
	x_bsinter(tls, qbe, bp, uintptr(unsafe.Pointer(&qbe.s_mask1))+1*16)
	s_limit(tls, qbe, b1, qbe.x_T.Fngpr-k1, f)
	s_limit(tls, qbe, bp, qbe.x_T.Fnfpr-k2, f)
	x_bsunion(tls, qbe, b1, bp)
}

func s_sethint(tls *libc.TLS, qbe *_QBE, u uintptr, r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* t at bp+0 */ int32
	*(*int32)(unsafe.Pointer(bp)) = int32(_Tmp0)
	for {
		if !(x_bsiter(tls, qbe, u, bp) != 0) {
			break
		}
		(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(x_phicls(tls, qbe, *(*int32)(unsafe.Pointer(bp)), qbe.s_tmp))*192))).Fhint.Fm |= r
		goto _1
	_1:
		;
		*(*int32)(unsafe.Pointer(bp))++
	}
}

// C documentation
//
//	/* reloads temporaries in u that are
//	 * not in v from their slots
//	 */
func s_reloads(tls *libc.TLS, qbe *_QBE, u uintptr, v uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var v2 _uint
	var v3 int32
	var _ /* t at bp+0 */ int32
	_, _ = v2, v3
	*(*int32)(unsafe.Pointer(bp)) = int32(_Tmp0)
	for {
		if !(x_bsiter(tls, qbe, u, bp) != 0) {
			break
		}
		v2 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp)))
		v3 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(v)).Ft + uintptr(v2/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v2%uint32(_NBit))) != uint64(0))
		goto _4
	_4:
		if !(v3 != 0) {
			x_emit(tls, qbe, int32(_Oload), int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(bp)))*192))).Fcls), _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp)))&0x1fffffff<<3,
			}, s_slot1(tls, qbe, *(*int32)(unsafe.Pointer(bp))), _Ref{})
		}
		goto _1
	_1:
		;
		*(*int32)(unsafe.Pointer(bp))++
	}
}

func s_store2(tls *libc.TLS, qbe *_QBE, _r _Ref, s int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	if s != -int32(1) {
		x_emit(tls, qbe, int32(_Ostorew)+int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3))*192))).Fcls), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(bp)), _Ref{
			F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
		})
	}
}

func s_regcpy(tls *libc.TLS, qbe *_QBE, i uintptr) (r int32) {
	return libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ocopy) && x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8))) != 0)
}

func s_dopm(tls *libc.TLS, qbe *_QBE, b1 uintptr, i uintptr, v uintptr) (r1 uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i1, v1, v10 uintptr
	var n, t, v4, v7 int32
	var r _bits
	var v2, v3 _Ref
	var v6 _uint
	var _ /* u at bp+8 */ [1]_BSet
	_, _, _, _, _, _, _, _, _, _, _ = i1, n, r, t, v1, v10, v2, v3, v4, v6, v7
	x_bsinit(tls, qbe, bp+8, libc.Uint32FromInt32(qbe.s_ntmp)) /* todo, free those */
	/* consecutive copies from
	 * registers need to be handled
	 * as one large instruction
	 *
	 * fixme: there is an assumption
	 * that calls are always followed
	 * by copy instructions here, this
	 * might not be true if previous
	 * passes change
	 */
	i += 16
	v1 = i
	i1 = v1
	for cond := true; cond; cond = i != (*_Blk)(unsafe.Pointer(b1)).Fins && s_regcpy(tls, qbe, i-uintptr(1)*16) != 0 {
		i -= 16
		t = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
		v2 = (*_Ins)(unsafe.Pointer(i)).Fto
		v3 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v2
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
		v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _5
	_5:
		if !(v4 != 0) {
			v6 = libc.Uint32FromInt32(t)
			v7 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(v)).Ft + uintptr(v6/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v6%uint32(_NBit))) != uint64(0))
			goto _8
		_8:
			if v7 != 0 {
				x_bsclr(tls, qbe, v, libc.Uint32FromInt32(t))
				s_store2(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(t)*192))).Fslot)
			}
		}
		x_bsset(tls, qbe, v, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3)))
	}
	x_bscopy(tls, qbe, bp+8, v)
	if i != (*_Blk)(unsafe.Pointer(b1)).Fins && int32(*(*uint32)(unsafe.Pointer(i - libc.UintptrFromInt32(1)*16 + 0))&0x3fffffff>>0) == int32(_Ocall) {
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(v)).Ft)) &= ^(*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fretregs})))(tls, qbe, *(*_Ref)(unsafe.Pointer(i - libc.UintptrFromInt32(1)*16 + 8 + 1*4)), uintptr(0))
		s_limit2(tls, qbe, v, *(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_T)) + 64)), *(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.x_T)) + 64 + 1*4)), uintptr(0))
		n = 0
		r = libc.Uint64FromInt32(0)
		for {
			if !(*(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(n)*4)) >= 0) {
				break
			}
			r |= libc.Uint64FromInt32(1) << *(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(n)*4))
			goto _9
		_9:
			;
			n++
		}
		*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(v)).Ft)) |= (*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fargregs})))(tls, qbe, *(*_Ref)(unsafe.Pointer(i - libc.UintptrFromInt32(1)*16 + 8 + 1*4)), uintptr(0))
	} else {
		s_limit2(tls, qbe, v, 0, 0, uintptr(0))
		r = *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(v)).Ft))
	}
	s_sethint(tls, qbe, v, r)
	s_reloads(tls, qbe, bp+8, v)
	for cond := true; cond; cond = i1 != i {
		i1 -= 16
		v10 = i1
		x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(v10)))
	}
	return i
}

func s_merge(tls *libc.TLS, qbe *_QBE, u uintptr, bu uintptr, v uintptr, bv uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* t at bp+0 */ int32
	if (*_Blk)(unsafe.Pointer(bu)).Floop <= (*_Blk)(unsafe.Pointer(bv)).Floop {
		x_bsunion(tls, qbe, u, v)
	} else {
		*(*int32)(unsafe.Pointer(bp)) = 0
		for {
			if !(x_bsiter(tls, qbe, v, bp) != 0) {
				break
			}
			if (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(bp)))*192))).Fslot == -int32(1) {
				x_bsset(tls, qbe, u, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp))))
			}
			goto _1
		_1:
			;
			*(*int32)(unsafe.Pointer(bp))++
		}
	}
}

// C documentation
//
//	/* spill code insertion
//	 * requires spill costs, rpo, liveness
//	 *
//	 * Note: this will replace liveness
//	 * information (in, out) with temporaries
//	 * that must be in registers at block
//	 * borders
//	 *
//	 * Be careful with:
//	 * - Ocopy instructions to ensure register
//	 *   constraints
//	 */
func x_spill(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp1 := tls.Alloc(96)
	defer tls.Free(96)
	var b1, bp, hd, i, m, p, s1, s2, v3 uintptr
	var j, k, l, v11, v15, v20, v22, v25, v28, v33, v36, v42, v44, v5, v50, v52, v57, v59, v64, v66, v69, v71, v76, v78, v81, v85, v89, v9, v92 int32
	var lvarg [2]int32
	var n, v14, v24, v27, v35, v68, v80, v88, v91 _uint
	var r1 _bits
	var v17, v18, v19, v31, v32, v39, v40, v41, v47, v48, v49, v54, v55, v56, v6, v61, v62, v63, v7, v73, v74, v75, v8, v83, v84 _Ref
	var _ /* t at bp+12 */ int32
	var _ /* u at bp+16 */ [1]_BSet
	var _ /* v at bp+32 */ [1]_BSet
	var _ /* w at bp+48 */ [1]_BSet
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, bp, hd, i, j, k, l, lvarg, m, n, p, r1, s1, s2, v11, v14, v15, v17, v18, v19, v20, v22, v24, v25, v27, v28, v3, v31, v32, v33, v35, v36, v39, v40, v41, v42, v44, v47, v48, v49, v5, v50, v52, v54, v55, v56, v57, v59, v6, v61, v62, v63, v64, v66, v68, v69, v7, v71, v73, v74, v75, v76, v78, v8, v80, v81, v83, v84, v85, v88, v89, v9, v91, v92
	qbe.s_tmp = (*_Fn)(unsafe.Pointer(fn)).Ftmp
	qbe.s_ntmp = (*_Fn)(unsafe.Pointer(fn)).Fntmp
	x_bsinit(tls, qbe, bp1+16, libc.Uint32FromInt32(qbe.s_ntmp))
	x_bsinit(tls, qbe, bp1+32, libc.Uint32FromInt32(qbe.s_ntmp))
	x_bsinit(tls, qbe, bp1+48, libc.Uint32FromInt32(qbe.s_ntmp))
	x_bsinit(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_mask1)), libc.Uint32FromInt32(qbe.s_ntmp))
	x_bsinit(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_mask1))+1*16, libc.Uint32FromInt32(qbe.s_ntmp))
	qbe.s_locs = (*_Fn)(unsafe.Pointer(fn)).Fslot
	qbe.s_slot4 = 0
	qbe.s_slot8 = 0
	*(*int32)(unsafe.Pointer(bp1 + 12)) = 0
	for {
		if !(*(*int32)(unsafe.Pointer(bp1 + 12)) < qbe.s_ntmp) {
			break
		}
		k = 0
		if *(*int32)(unsafe.Pointer(bp1 + 12)) >= qbe.x_T.Ffpr0 && *(*int32)(unsafe.Pointer(bp1 + 12)) < qbe.x_T.Ffpr0+qbe.x_T.Fnfpr {
			k = int32(1)
		}
		if *(*int32)(unsafe.Pointer(bp1 + 12)) >= int32(_Tmp0) {
			k = int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(bp1 + 12)))*192))).Fcls) >> int32(1)
		}
		x_bsset(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_mask1))+uintptr(k)*16, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
		goto _1
	_1:
		;
		*(*int32)(unsafe.Pointer(bp1 + 12))++
	}
	bp = (*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr((*_Fn)(unsafe.Pointer(fn)).Fnblk)*8
	for {
		if !(bp != (*_Fn)(unsafe.Pointer(fn)).Frpo) {
			break
		}
		bp -= 8
		v3 = bp
		b1 = *(*uintptr)(unsafe.Pointer(v3))
		/* invariant: all blocks with bigger rpo got
		 * their in,out updated. */
		/* 1. find temporaries in registers at
		 * the end of the block (put them in v) */
		qbe.x_curi = uintptr(0)
		s1 = (*_Blk)(unsafe.Pointer(b1)).Fs1
		s2 = (*_Blk)(unsafe.Pointer(b1)).Fs2
		hd = uintptr(0)
		if s1 != 0 && (*_Blk)(unsafe.Pointer(s1)).Fid <= (*_Blk)(unsafe.Pointer(b1)).Fid {
			hd = s1
		}
		if s2 != 0 && (*_Blk)(unsafe.Pointer(s2)).Fid <= (*_Blk)(unsafe.Pointer(b1)).Fid {
			if !(hd != 0) || (*_Blk)(unsafe.Pointer(s2)).Fid >= (*_Blk)(unsafe.Pointer(hd)).Fid {
				hd = s2
			}
		}
		if hd != 0 {
			/* back-edge */
			x_bszero(tls, qbe, bp1+32)
			*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(hd + 152)).Ft)) |= qbe.x_T.Frglob /* don't spill registers */
			k = 0
			for {
				if !(k < int32(2)) {
					break
				}
				if k == 0 {
					v5 = qbe.x_T.Fngpr
				} else {
					v5 = qbe.x_T.Fnfpr
				}
				n = libc.Uint32FromInt32(v5)
				x_bscopy(tls, qbe, bp1+16, b1+136)
				x_bsinter(tls, qbe, bp1+16, uintptr(unsafe.Pointer(&qbe.s_mask1))+uintptr(k)*16)
				x_bscopy(tls, qbe, bp1+48, bp1+16)
				x_bsinter(tls, qbe, bp1+16, hd+152)
				x_bsdiff(tls, qbe, bp1+48, hd+152)
				if x_bscount(tls, qbe, bp1+16) < n {
					j = libc.Int32FromUint32(x_bscount(tls, qbe, bp1+48)) /* live through */
					l = *(*int32)(unsafe.Pointer(hd + 168 + uintptr(k)*4))
					s_limit(tls, qbe, bp1+48, libc.Int32FromUint32(n-libc.Uint32FromInt32(l-j)), uintptr(0))
					x_bsunion(tls, qbe, bp1+16, bp1+48)
				} else {
					s_limit(tls, qbe, bp1+16, libc.Int32FromUint32(n), uintptr(0))
				}
				x_bsunion(tls, qbe, bp1+32, bp1+16)
				goto _4
			_4:
				;
				k++
			}
		} else {
			if s1 != 0 {
				/* avoid reloading temporaries
				 * in the middle of loops */
				x_bszero(tls, qbe, bp1+32)
				x_liveon(tls, qbe, bp1+48, b1, s1)
				s_merge(tls, qbe, bp1+32, b1, bp1+48, s1)
				if s2 != 0 {
					x_liveon(tls, qbe, bp1+16, b1, s2)
					s_merge(tls, qbe, bp1+32, b1, bp1+16, s2)
					x_bsinter(tls, qbe, bp1+48, bp1+16)
				}
				s_limit2(tls, qbe, bp1+32, 0, 0, bp1+48)
			} else {
				x_bscopy(tls, qbe, bp1+32, b1+136)
				v6 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
				*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v6
				v7 = v6
				v8 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v7
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v8
				v9 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _10
			_10:
				if v9 != 0 {
					v11 = -int32(1)
					goto _12
				}
				v11 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
				goto _12
			_12:
				if v11 == int32(_RCall) {
					*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1 + 32)).Ft)) |= (*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fretregs})))(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg, uintptr(0))
				}
			}
		}
		*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(_Tmp0)
		for {
			if !(x_bsiter(tls, qbe, b1+136, bp1+12) != 0) {
				break
			}
			v14 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
			v15 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v14/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v14%uint32(_NBit))) != uint64(0))
			goto _16
		_16:
			if !(v15 != 0) {
				s_slot1(tls, qbe, *(*int32)(unsafe.Pointer(bp1 + 12)))
			}
			goto _13
		_13:
			;
			*(*int32)(unsafe.Pointer(bp1 + 12))++
		}
		x_bscopy(tls, qbe, b1+136, bp1+32)
		/* 2. process the block instructions */
		v17 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
		*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v17
		v18 = v17
		v19 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp1)) = v18
		*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v19
		v20 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
		goto _21
	_21:
		if v20 != 0 {
			v22 = -int32(1)
			goto _23
		}
		v22 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
		goto _23
	_23:
		if v22 == int32(_RTmp) {
			*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(b1 + 20 + 4 + 0)) & 0xfffffff8 >> 3)
			v24 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
			v25 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v24/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v24%uint32(_NBit))) != uint64(0))
			goto _26
		_26:
			lvarg[0] = v25
			x_bsset(tls, qbe, bp1+32, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
			x_bscopy(tls, qbe, bp1+16, bp1+32)
			s_limit2(tls, qbe, bp1+32, 0, 0, libc.UintptrFromInt32(0))
			v27 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
			v28 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v27/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v27%uint32(_NBit))) != uint64(0))
			goto _29
		_29:
			if !(v28 != 0) {
				if !(lvarg[0] != 0) {
					x_bsclr(tls, qbe, bp1+16, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
				}
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = s_slot1(tls, qbe, *(*int32)(unsafe.Pointer(bp1 + 12)))
			}
			s_reloads(tls, qbe, bp1+16, bp1+32)
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b1)).Fins) {
				break
			}
			i -= 16
			if s_regcpy(tls, qbe, i) != 0 {
				i = s_dopm(tls, qbe, b1, i, bp1+32)
				goto _30
			}
			x_bszero(tls, qbe, bp1+48)
			v31 = (*_Ins)(unsafe.Pointer(i)).Fto
			v32 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp1)) = v31
			*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v32
			v33 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
			goto _34
		_34:
			if !(v33 != 0) {
				*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
				v35 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
				v36 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v35/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v35%uint32(_NBit))) != uint64(0))
				goto _37
			_37:
				if v36 != 0 {
					x_bsclr(tls, qbe, bp1+32, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
				} else {
					/* make sure we have a reg
					 * for the result */
					x_bsset(tls, qbe, bp1+32, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
					x_bsset(tls, qbe, bp1+48, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
				}
			}
			j = (*(*func(*libc.TLS, *_QBE, int32) int32)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fmemargs})))(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))
			n = uint32(0)
			for {
				if !(n < uint32(2)) {
					break
				}
				v39 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(n)*4))
				*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v39
				v40 = v39
				v41 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v40
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v41
				v42 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _43
			_43:
				if v42 != 0 {
					v44 = -int32(1)
					goto _45
				}
				v44 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
				goto _45
			_45:
				if v44 == int32(_RMem) {
					j--
				}
				goto _38
			_38:
				;
				n++
			}
			n = uint32(0)
			for {
				if !(n < uint32(2)) {
					break
				}
				v47 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(n)*4))
				*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v47
				v48 = v47
				v49 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v48
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v49
				v50 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _51
			_51:
				if v50 != 0 {
					v52 = -int32(1)
					goto _53
				}
				v52 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
				goto _53
			_53:
				switch v52 {
				case int32(_RMem):
					*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(n)*4 + 0)) & 0xfffffff8 >> 3)
					m = (*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr(*(*int32)(unsafe.Pointer(bp1 + 12)))*48
					v54 = (*_Mem)(unsafe.Pointer(m)).Fbase
					*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v54
					v55 = v54
					v56 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp1)) = v55
					*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v56
					v57 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
					goto _58
				_58:
					if v57 != 0 {
						v59 = -int32(1)
						goto _60
					}
					v59 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
					goto _60
				_60:
					if v59 == int32(_RTmp) {
						x_bsset(tls, qbe, bp1+32, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(m + 32 + 0))&0xfffffff8>>3)))
						x_bsset(tls, qbe, bp1+48, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(m + 32 + 0))&0xfffffff8>>3)))
					}
					v61 = (*_Mem)(unsafe.Pointer(m)).Findex
					*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v61
					v62 = v61
					v63 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp1)) = v62
					*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v63
					v64 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
					goto _65
				_65:
					if v64 != 0 {
						v66 = -int32(1)
						goto _67
					}
					v66 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
					goto _67
				_67:
					if v66 == int32(_RTmp) {
						x_bsset(tls, qbe, bp1+32, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(m + 36 + 0))&0xfffffff8>>3)))
						x_bsset(tls, qbe, bp1+48, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(m + 36 + 0))&0xfffffff8>>3)))
					}
				case int32(_RTmp):
					*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(n)*4 + 0)) & 0xfffffff8 >> 3)
					v68 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
					v69 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v68/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v68%uint32(_NBit))) != uint64(0))
					goto _70
				_70:
					lvarg[n] = v69
					x_bsset(tls, qbe, bp1+32, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
					v71 = j
					j--
					if v71 <= 0 {
						x_bsset(tls, qbe, bp1+48, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
					}
					break
				}
				goto _46
			_46:
				;
				n++
			}
			x_bscopy(tls, qbe, bp1+16, bp1+32)
			s_limit2(tls, qbe, bp1+32, 0, 0, bp1+48)
			n = uint32(0)
			for {
				if !(n < uint32(2)) {
					break
				}
				v73 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(n)*4))
				*(*_Ref)(unsafe.Pointer(bp1 + 8)) = v73
				v74 = v73
				v75 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1)) = v74
				*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v75
				v76 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
				goto _77
			_77:
				if v76 != 0 {
					v78 = -int32(1)
					goto _79
				}
				v78 = int32(*(*uint32)(unsafe.Pointer(bp1 + 8 + 0)) & 0x7 >> 0)
				goto _79
			_79:
				if v78 == int32(_RTmp) {
					*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(n)*4 + 0)) & 0xfffffff8 >> 3)
					v80 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
					v81 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v80/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v80%uint32(_NBit))) != uint64(0))
					goto _82
				_82:
					if !(v81 != 0) {
						/* do not reload if the
						 * argument is dead
						 */
						if !(lvarg[n] != 0) {
							x_bsclr(tls, qbe, bp1+16, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
						}
						*(*_Ref)(unsafe.Pointer(i + 8 + uintptr(n)*4)) = s_slot1(tls, qbe, *(*int32)(unsafe.Pointer(bp1 + 12)))
					}
				}
				goto _72
			_72:
				;
				n++
			}
			s_reloads(tls, qbe, bp1+16, bp1+32)
			v83 = (*_Ins)(unsafe.Pointer(i)).Fto
			v84 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp1)) = v83
			*(*_Ref)(unsafe.Pointer(bp1 + 4)) = v84
			v85 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 4 + 0))&0xfffffff8>>3))
			goto _86
		_86:
			if !(v85 != 0) {
				*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
				s_store2(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(bp1 + 12)))*192))).Fslot)
				if *(*int32)(unsafe.Pointer(bp1 + 12)) >= int32(_Tmp0) {
					/* in case i->to was a
					 * dead temporary */
					x_bsclr(tls, qbe, bp1+32, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
				}
			}
			x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(i)))
			r1 = *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1 + 32)).Ft)) /* Tmp0 is NBit */
			if r1 != 0 {
				s_sethint(tls, qbe, bp1+32, r1)
			}
			goto _30
		_30:
		}
		if b1 == (*_Fn)(unsafe.Pointer(fn)).Fstart {
		} else {
		}
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			*(*int32)(unsafe.Pointer(bp1 + 12)) = int32(*(*uint32)(unsafe.Pointer(p + 0)) & 0xfffffff8 >> 3)
			v88 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
			v89 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(bp1+32)).Ft + uintptr(v88/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v88%uint32(_NBit))) != uint64(0))
			goto _90
		_90:
			if v89 != 0 {
				x_bsclr(tls, qbe, bp1+32, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12))))
				s_store2(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Fto, (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp + uintptr(*(*int32)(unsafe.Pointer(bp1 + 12)))*192))).Fslot)
			} else {
				v91 = libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 12)))
				v92 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(b1+120)).Ft + uintptr(v91/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v91%uint32(_NBit))) != uint64(0))
				goto _93
			_93:
				if v92 != 0 {
					/* only if the phi is live */
					(*_Phi)(unsafe.Pointer(p)).Fto = s_slot1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))
				}
			}
			goto _87
		_87:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		x_bscopy(tls, qbe, b1+120, bp1+32)
		(*_Blk)(unsafe.Pointer(b1)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b1+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b1)).Fnins))
		goto _2
	_2:
	}
	/* align the locals to a 16 byte boundary */
	/* specific to NAlign == 3 */
	qbe.s_slot8 += qbe.s_slot8 & int32(3)
	*(*int32)(unsafe.Pointer(fn + 72)) += qbe.s_slot8
	if qbe.x_debug[int32('S')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4146, 0)
		b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
		for {
			if !(b1 != 0) {
				break
			}
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4169, libc.VaList(bp1+72, b1+180, (*_Blk)(unsafe.Pointer(b1)).Floop))
			x_dumpts(tls, qbe, b1+136, (*_Fn)(unsafe.Pointer(fn)).Ftmp, libc.X__stderrp)
			goto _94
		_94:
			;
			b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4184, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

type _RMap = struct {
	Ft [64]int32
	Fr [64]int32
	Fw [64]int32
	Fb [1]_BSet
	Fn int32
}

func s_hint(tls *libc.TLS, qbe *_QBE, t int32) (r uintptr) {
	return qbe.s_tmp1 + uintptr(x_phicls(tls, qbe, t, qbe.s_tmp1))*192 + 120
}

func s_sethint1(tls *libc.TLS, qbe *_QBE, t int32, r int32) {
	var p uintptr
	_ = p
	p = qbe.s_tmp1 + uintptr(x_phicls(tls, qbe, t, qbe.s_tmp1))*192
	if (*_Tmp)(unsafe.Pointer(p)).Fhint.Fr == -int32(1) || (*_Tmp)(unsafe.Pointer(p)).Fhint.Fw > qbe.s_loop {
		(*_Tmp)(unsafe.Pointer(p)).Fhint.Fr = r
		(*_Tmp)(unsafe.Pointer(p)).Fhint.Fw = qbe.s_loop
		(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fvisit = -int32(1)
	}
}

func s_rcopy(tls *libc.TLS, qbe *_QBE, ma uintptr, mb uintptr) {
	libc.X__builtin___memcpy_chk(tls, ma, mb, uint64(256), ^___predefined_size_t(0))
	libc.X__builtin___memcpy_chk(tls, ma+256, mb+256, uint64(256), ^___predefined_size_t(0))
	libc.X__builtin___memcpy_chk(tls, ma+512, mb+512, uint64(256), ^___predefined_size_t(0))
	x_bscopy(tls, qbe, ma+768, mb+768)
	(*_RMap)(unsafe.Pointer(ma)).Fn = (*_RMap)(unsafe.Pointer(mb)).Fn
}

func s_rfind(tls *libc.TLS, qbe *_QBE, m uintptr, t int32) (r int32) {
	var i int32
	_ = i
	i = 0
	for {
		if !(i < (*_RMap)(unsafe.Pointer(m)).Fn) {
			break
		}
		if *(*int32)(unsafe.Pointer(m + uintptr(i)*4)) == t {
			return *(*int32)(unsafe.Pointer(m + 256 + uintptr(i)*4))
		}
		goto _1
	_1:
		;
		i++
	}
	return -int32(1)
}

func s_rref(tls *libc.TLS, qbe *_QBE, m uintptr, t int32) (r1 _Ref) {
	var r, s int32
	_, _ = r, s
	r = s_rfind(tls, qbe, m, t)
	if r == -int32(1) {
		s = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fslot
		return _Ref{
			F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
		}
	} else {
		return _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(r)&0x1fffffff<<3,
		}
	}
	return r1
}

func s_radd1(tls *libc.TLS, qbe *_QBE, m uintptr, t int32, r int32) {
	x_bsset(tls, qbe, m+768, libc.Uint32FromInt32(t))
	x_bsset(tls, qbe, m+768, libc.Uint32FromInt32(r))
	*(*int32)(unsafe.Pointer(m + uintptr((*_RMap)(unsafe.Pointer(m)).Fn)*4)) = t
	*(*int32)(unsafe.Pointer(m + 256 + uintptr((*_RMap)(unsafe.Pointer(m)).Fn)*4)) = r
	(*_RMap)(unsafe.Pointer(m)).Fn++
	qbe.s_regu |= libc.Uint64FromInt32(1) << r
}

func s_ralloctry(tls *libc.TLS, qbe *_QBE, m uintptr, t int32, try int32) (r2 _Ref) {
	var h, r, r0, r1, v15, v2, v5, v9 int32
	var regs _bits
	var v1, v14, v4, v8 _uint
	var v11, v7 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = h, r, r0, r1, regs, v1, v11, v14, v15, v2, v4, v5, v7, v8, v9
	if t < int32(_Tmp0) {
		return _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(t)&0x1fffffff<<3,
		}
	}
	v1 = libc.Uint32FromInt32(t)
	v2 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v1/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v1%uint32(_NBit))) != uint64(0))
	goto _3
_3:
	if v2 != 0 {
		r = s_rfind(tls, qbe, m, t)
		return _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(r)&0x1fffffff<<3,
		}
	}
	r = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fvisit
	if v7 = r == -int32(1); !v7 {
		v4 = libc.Uint32FromInt32(r)
		v5 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v4/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v4%uint32(_NBit))) != uint64(0))
		goto _6
	_6:
	}
	if v7 || v5 != 0 {
		r = *(*int32)(unsafe.Pointer(s_hint(tls, qbe, t)))
	}
	if v11 = r == -int32(1); !v11 {
		v8 = libc.Uint32FromInt32(r)
		v9 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v8/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v8%uint32(_NBit))) != uint64(0))
		goto _10
	_10:
	}
	if v11 || v9 != 0 {
		if try != 0 {
			return _Ref{}
		}
		regs = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(x_phicls(tls, qbe, t, qbe.s_tmp1))*192))).Fhint.Fm
		regs |= *(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m + 768)).Ft))
		if int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fcls)>>int32(1) == 0 {
			r0 = qbe.x_T.Fgpr0
			r1 = r0 + qbe.x_T.Fngpr
		} else {
			r0 = qbe.x_T.Ffpr0
			r1 = r0 + qbe.x_T.Fnfpr
		}
		r = r0
		for {
			if !(r < r1) {
				break
			}
			if !(regs&(libc.Uint64FromInt32(1)<<r) != 0) {
				goto Found
			}
			goto _12
		_12:
			;
			r++
		}
		r = r0
		for {
			if !(r < r1) {
				break
			}
			v14 = libc.Uint32FromInt32(r)
			v15 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v14/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v14%uint32(_NBit))) != uint64(0))
			goto _16
		_16:
			if !(v15 != 0) {
				goto Found
			}
			goto _13
		_13:
			;
			r++
		}
		_die_(tls, qbe, __ccgo_ts+4204, __ccgo_ts+4211, 0)
	}
	goto Found
Found:
	;
	s_radd1(tls, qbe, m, t, r)
	s_sethint1(tls, qbe, t, r)
	(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fvisit = r
	h = *(*int32)(unsafe.Pointer(s_hint(tls, qbe, t)))
	if h != -int32(1) && h != r {
		*(*int32)(unsafe.Pointer(m + 512 + uintptr(h)*4)) = t
	}
	return _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(r)&0x1fffffff<<3,
	}
}

func s_ralloc(tls *libc.TLS, qbe *_QBE, m uintptr, t int32) (r _Ref) {
	return s_ralloctry(tls, qbe, m, t, 0)
}

func s_rfree(tls *libc.TLS, qbe *_QBE, m uintptr, t int32) (r1 int32) {
	var i, r, v2 int32
	var v1 _uint
	_, _, _, _ = i, r, v1, v2
	v1 = libc.Uint32FromInt32(t)
	v2 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v1/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v1%uint32(_NBit))) != uint64(0))
	goto _3
_3:
	if !(v2 != 0) {
		return -int32(1)
	}
	i = 0
	for {
		if !(*(*int32)(unsafe.Pointer(m + uintptr(i)*4)) != t) {
			break
		}
		goto _4
	_4:
		;
		i++
	}
	r = *(*int32)(unsafe.Pointer(m + 256 + uintptr(i)*4))
	x_bsclr(tls, qbe, m+768, libc.Uint32FromInt32(t))
	x_bsclr(tls, qbe, m+768, libc.Uint32FromInt32(r))
	(*_RMap)(unsafe.Pointer(m)).Fn--
	libc.X__builtin___memmove_chk(tls, m+uintptr(i)*4, m+uintptr(i+int32(1))*4, libc.Uint64FromInt32((*_RMap)(unsafe.Pointer(m)).Fn-i)*uint64(4), ^___predefined_size_t(0))
	libc.X__builtin___memmove_chk(tls, m+256+uintptr(i)*4, m+256+uintptr(i+int32(1))*4, libc.Uint64FromInt32((*_RMap)(unsafe.Pointer(m)).Fn-i)*uint64(4), ^___predefined_size_t(0))
	return r
}

func s_mdump(tls *libc.TLS, qbe *_QBE, m uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	_ = i
	i = 0
	for {
		if !(i < (*_RMap)(unsafe.Pointer(m)).Fn) {
			break
		}
		if *(*int32)(unsafe.Pointer(m + uintptr(i)*4)) >= int32(_Tmp0) {
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4224, libc.VaList(bp+8, qbe.s_tmp1+uintptr(*(*int32)(unsafe.Pointer(m + uintptr(i)*4)))*192, *(*int32)(unsafe.Pointer(m + 256 + uintptr(i)*4))))
		}
		goto _1
	_1:
		;
		i++
	}
	fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+82, 0)
}

func s_pmadd(tls *libc.TLS, qbe *_QBE, src _Ref, dst _Ref, k int32) {
	if qbe.s_npm == int32(_Tmp0) {
		_die_(tls, qbe, __ccgo_ts+4204, __ccgo_ts+4235, 0)
	}
	qbe.s_pm[qbe.s_npm].Fsrc = src
	qbe.s_pm[qbe.s_npm].Fdst = dst
	qbe.s_pm[qbe.s_npm].Fcls = k
	qbe.s_npm++
}

type _PMStat = int32

const _ToMove = 0

const _Moving = 1

const _Moved = 2

func s_pmrec(tls *libc.TLS, qbe *_QBE, status uintptr, i int32, k uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var c, j, v10, v3, v8 int32
	var v1, v2, v6, v7 _Ref
	_, _, _, _, _, _, _, _, _ = c, j, v1, v10, v2, v3, v6, v7, v8
	/* note, this routine might emit
	 * too many large instructions
	 */
	v1 = qbe.s_pm[i].Fsrc
	v2 = qbe.s_pm[i].Fdst
	*(*_Ref)(unsafe.Pointer(bp)) = v1
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
	v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _4
_4:
	if v3 != 0 {
		*(*_PMStat)(unsafe.Pointer(status + uintptr(i)*4)) = int32(_Moved)
		return -int32(1)
	}
	*(*int32)(unsafe.Pointer(k)) |= qbe.s_pm[i].Fcls
	j = 0
	for {
		if !(j < qbe.s_npm) {
			break
		}
		v6 = qbe.s_pm[j].Fdst
		v7 = qbe.s_pm[i].Fsrc
		*(*_Ref)(unsafe.Pointer(bp)) = v6
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v7
		v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _9
	_9:
		if v8 != 0 {
			break
		}
		goto _5
	_5:
		;
		j++
	}
	if j == qbe.s_npm {
		v10 = int32(_Moved)
	} else {
		v10 = *(*_PMStat)(unsafe.Pointer(status + uintptr(j)*4))
	}
	switch v10 {
	case int32(_Moving):
		c = j /* start of cycle */
		x_emit(tls, qbe, int32(_Oswap), *(*int32)(unsafe.Pointer(k)), _Ref{}, qbe.s_pm[i].Fsrc, qbe.s_pm[i].Fdst)
	case int32(_ToMove):
		*(*_PMStat)(unsafe.Pointer(status + uintptr(i)*4)) = int32(_Moving)
		c = s_pmrec(tls, qbe, status, j, k)
		if c == i {
			c = -int32(1) /* end of cycle */
			break
		}
		if c != -int32(1) {
			x_emit(tls, qbe, int32(_Oswap), *(*int32)(unsafe.Pointer(k)), _Ref{}, qbe.s_pm[i].Fsrc, qbe.s_pm[i].Fdst)
			break
		}
		/* fall through */
		fallthrough
	case int32(_Moved):
		c = -int32(1)
		x_emit(tls, qbe, int32(_Ocopy), qbe.s_pm[i].Fcls, qbe.s_pm[i].Fdst, qbe.s_pm[i].Fsrc, _Ref{})
	default:
		_die_(tls, qbe, __ccgo_ts+4204, __ccgo_ts+3532, 0)
	}
	*(*_PMStat)(unsafe.Pointer(status + uintptr(i)*4)) = int32(_Moved)
	return c
}

func s_pmgen(tls *libc.TLS, qbe *_QBE) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var status uintptr
	_, _ = i, status
	status = x_alloc(tls, qbe, libc.Uint64FromInt32(qbe.s_npm)*uint64(4))
	i = 0
	for {
		if !(i < qbe.s_npm) {
			break
		}
		if *(*_PMStat)(unsafe.Pointer(status + uintptr(i)*4)) == int32(_ToMove) {
			*(*[1]int32)(unsafe.Pointer(bp)) = [1]int32{
				0: qbe.s_pm[i].Fcls,
			}
			s_pmrec(tls, qbe, status, i, bp)
		}
		goto _1
	_1:
		;
		i++
	}
}

func s_move(tls *libc.TLS, qbe *_QBE, r int32, _to _Ref, m uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 8)) = _to
	var n, r1, t, v1, v10, v13, v4, v7 int32
	var v11, v12, v2, v3 _Ref
	var v6 _uint
	_, _, _, _, _, _, _, _, _, _, _, _, _ = n, r1, t, v1, v10, v11, v12, v13, v2, v3, v4, v6, v7
	v2 = *(*_Ref)(unsafe.Pointer(bp + 8))
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v1 = -int32(1)
	} else {
		v1 = s_rfree(tls, qbe, m, int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3))
	}
	r1 = v1
	v6 = libc.Uint32FromInt32(r)
	v7 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v6/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v6%uint32(_NBit))) != uint64(0))
	goto _8
_8:
	if v7 != 0 {
		/* r is used and not by to */
		n = 0
		for {
			if !(*(*int32)(unsafe.Pointer(m + 256 + uintptr(n)*4)) != r) {
				break
			}
			goto _9
		_9:
			;
			n++
		}
		t = *(*int32)(unsafe.Pointer(m + uintptr(n)*4))
		s_rfree(tls, qbe, m, t)
		x_bsset(tls, qbe, m+768, libc.Uint32FromInt32(r))
		s_ralloc(tls, qbe, m, t)
		x_bsclr(tls, qbe, m+768, libc.Uint32FromInt32(r))
	}
	v11 = *(*_Ref)(unsafe.Pointer(bp + 8))
	v12 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v11
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v12
	v13 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _14
_14:
	if v13 != 0 {
		v10 = r
	} else {
		v10 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0xfffffff8 >> 3)
	}
	t = v10
	s_radd1(tls, qbe, m, t, r)
}

func s_regcpy1(tls *libc.TLS, qbe *_QBE, i uintptr) (r int32) {
	return libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ocopy) && x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8))) != 0)
}

func s_dopm1(tls *libc.TLS, qbe *_QBE, b1 uintptr, i uintptr, m uintptr) (r2 uintptr) {
	bp := tls.Alloc(800)
	defer tls.Free(800)
	var def _bits
	var i1, ip, v1 uintptr
	var n, r, r1, s, t, v7 int32
	var v5, v6 _Ref
	var _ /* m0 at bp+8 */ _RMap
	_, _, _, _, _, _, _, _, _, _, _, _ = def, i1, ip, n, r, r1, s, t, v1, v5, v6, v7
	*(*_RMap)(unsafe.Pointer(bp + 8)) = *(*_RMap)(unsafe.Pointer(m)) /* okay since we don't use m0.b */
	(*_BSet)(unsafe.Pointer(bp + 8 + 768)).Ft = uintptr(0)
	i += 16
	v1 = i
	i1 = v1
	for cond := true; cond; cond = i != (*_Blk)(unsafe.Pointer(b1)).Fins && s_regcpy1(tls, qbe, i-uintptr(1)*16) != 0 {
		i -= 16
		s_move(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3), (*_Ins)(unsafe.Pointer(i)).Fto, m)
	}
	if i != (*_Blk)(unsafe.Pointer(b1)).Fins && int32(*(*uint32)(unsafe.Pointer(i - libc.UintptrFromInt32(1)*16 + 0))&0x3fffffff>>0) == int32(_Ocall) {
		def = (*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fretregs})))(tls, qbe, *(*_Ref)(unsafe.Pointer(i - libc.UintptrFromInt32(1)*16 + 8 + 1*4)), uintptr(0)) | qbe.x_T.Frglob
		r = 0
		for {
			if !(*(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(r)*4)) >= 0) {
				break
			}
			if !(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(r)*4))&def != 0) {
				s_move(tls, qbe, *(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(r)*4)), _Ref{}, m)
			}
			goto _2
		_2:
			;
			r++
		}
	}
	qbe.s_npm = 0
	n = libc.Int32FromInt32(0)
	for {
		if !(n < (*_RMap)(unsafe.Pointer(m)).Fn) {
			break
		}
		t = *(*int32)(unsafe.Pointer(m + uintptr(n)*4))
		s = (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fslot
		r1 = *(*int32)(unsafe.Pointer(m + 256 + uintptr(n)*4))
		r = s_rfind(tls, qbe, bp+8, t)
		if r != -int32(1) {
			s_pmadd(tls, qbe, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(r1)&0x1fffffff<<3,
			}, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(r)&0x1fffffff<<3,
			}, int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fcls))
		} else {
			if s != -int32(1) {
				s_pmadd(tls, qbe, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(r1)&0x1fffffff<<3,
				}, _Ref{
					F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
				}, int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fcls))
			}
		}
		goto _3
	_3:
		;
		n++
	}
	ip = i
	for {
		if !(ip < i1) {
			break
		}
		v5 = (*_Ins)(unsafe.Pointer(ip)).Fto
		v6 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v5
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
		v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _8
	_8:
		if !(v7 != 0) {
			s_rfree(tls, qbe, m, int32(*(*uint32)(unsafe.Pointer(ip + 4 + 0))&0xfffffff8>>3))
		}
		r = int32(*(*uint32)(unsafe.Pointer(ip + 8 + 0)) & 0xfffffff8 >> 3)
		if s_rfind(tls, qbe, m, r) == -int32(1) {
			s_radd1(tls, qbe, m, r, r)
		}
		goto _4
	_4:
		;
		ip += 16
	}
	s_pmgen(tls, qbe)
	return i
}

func s_prio1(tls *libc.TLS, qbe *_QBE, _r1 _Ref, r2 _Ref) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r1
	/* trivial heuristic to begin with,
	 * later we can use the distance to
	 * the definition instruction
	 */
	_ = r2
	return libc.BoolInt32(*(*int32)(unsafe.Pointer(s_hint(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3)))) != -int32(1))
}

func s_insert(tls *libc.TLS, qbe *_QBE, r uintptr, rs uintptr, p int32) {
	var i, v1, v2 int32
	_, _, _ = i, v1, v2
	v1 = p
	i = v1
	*(*uintptr)(unsafe.Pointer(rs + uintptr(v1)*8)) = r
	for {
		v2 = i
		i--
		if !(v2 > 0 && s_prio1(tls, qbe, *(*_Ref)(unsafe.Pointer(r)), *(*_Ref)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(rs + uintptr(i)*8))))) != 0) {
			break
		}
		*(*uintptr)(unsafe.Pointer(rs + uintptr(i+int32(1))*8)) = *(*uintptr)(unsafe.Pointer(rs + uintptr(i)*8))
		*(*uintptr)(unsafe.Pointer(rs + uintptr(i)*8)) = r
	}
}

func s_doblk(tls *libc.TLS, qbe *_QBE, b1 uintptr, cur uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var i, i1, m, v9 uintptr
	var nr, r1, rf, rt, t, x, v14, v16, v20, v26, v28, v33, v35, v37, v4, v41, v43, v45, v46, v50, v53, v56, v58, v6, v63 int32
	var rs _bits
	var v1, v11, v12, v13, v18, v19, v2, v23, v24, v25, v3, v30, v31, v32, v38, v39, v40, v48, v49, v61, v62 _Ref
	var v52, v54, v59 bool
	var v55 _uint
	var _ /* ra at bp+16 */ [4]uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = i, i1, m, nr, r1, rf, rs, rt, t, x, v1, v11, v12, v13, v14, v16, v18, v19, v2, v20, v23, v24, v25, v26, v28, v3, v30, v31, v32, v33, v35, v37, v38, v39, v4, v40, v41, v43, v45, v46, v48, v49, v50, v52, v53, v54, v55, v56, v58, v59, v6, v61, v62, v63, v9
	v1 = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = s_ralloc(tls, qbe, cur, int32(*(*uint32)(unsafe.Pointer(b1 + 20 + 4 + 0))&0xfffffff8>>3))
	}
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
	i1 = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
	for {
		if !(i1 != (*_Blk)(unsafe.Pointer(b1)).Fins) {
			break
		}
		i1 -= 16
		v9 = i1
		x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(v9)))
		i = qbe.x_curi
		rf = -int32(1)
		switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
		case int32(_Ocall):
			rs = (*(*func(*libc.TLS, *_QBE, _Ref, uintptr) _bits)(unsafe.Pointer(&struct{ uintptr }{qbe.x_T.Fargregs})))(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), uintptr(0)) | qbe.x_T.Frglob
			r1 = 0
			for {
				if !(*(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(r1)*4)) >= 0) {
					break
				}
				if !(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(r1)*4))&rs != 0) {
					s_rfree(tls, qbe, cur, *(*int32)(unsafe.Pointer(qbe.x_T.Frsave + uintptr(r1)*4)))
				}
				goto _10
			_10:
				;
				r1++
			}
		case int32(_Ocopy):
			if s_regcpy1(tls, qbe, i) != 0 {
				qbe.x_curi += 16
				i1 = s_dopm1(tls, qbe, b1, i1, cur)
				qbe.s_stmov = _uint(int64(qbe.s_stmov) + (int64(i+libc.UintptrFromInt32(1)*16)-int64(qbe.x_curi))/16)
				goto _8
			}
			if x_isreg(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto) != 0 {
				v11 = *(*_Ref)(unsafe.Pointer(i + 8))
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v11
				v12 = v11
				v13 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v12
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v13
				v14 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _15
			_15:
				if v14 != 0 {
					v16 = -int32(1)
					goto _17
				}
				v16 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _17
			_17:
				if v16 == int32(_RTmp) {
					s_sethint1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3), int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))
				}
			}
			/* fall through */
			fallthrough
		default:
			v18 = (*_Ins)(unsafe.Pointer(i)).Fto
			v19 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v18
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v19
			v20 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _21
		_21:
			if !(v20 != 0) {
				r1 = int32(*(*uint32)(unsafe.Pointer(i + 4 + 0)) & 0xfffffff8 >> 3)
				if r1 < int32(_Tmp0) && libc.Uint64FromInt32(1)<<r1&qbe.x_T.Frglob != 0 {
					break
				}
				rf = s_rfree(tls, qbe, cur, r1)
				if rf == -int32(1) {
					qbe.x_curi += 16
					goto _8
				}
				(*_Ins)(unsafe.Pointer(i)).Fto = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(rf)&0x1fffffff<<3,
				}
			}
			break
		}
		x = 0
		nr = libc.Int32FromInt32(0)
		for {
			if !(x < int32(2)) {
				break
			}
			v23 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(x)*4))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v23
			v24 = v23
			v25 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v24
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v25
			v26 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _27
		_27:
			if v26 != 0 {
				v28 = -int32(1)
				goto _29
			}
			v28 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _29
		_29:
			switch v28 {
			case int32(_RMem):
				m = qbe.s_mem + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + uintptr(x)*4 + 0))&0xfffffff8>>3))*48
				v30 = (*_Mem)(unsafe.Pointer(m)).Fbase
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v30
				v31 = v30
				v32 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v31
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v32
				v33 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _34
			_34:
				if v33 != 0 {
					v35 = -int32(1)
					goto _36
				}
				v35 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _36
			_36:
				if v35 == int32(_RTmp) {
					v37 = nr
					nr++
					s_insert(tls, qbe, m+32, bp+16, v37)
				}
				v38 = (*_Mem)(unsafe.Pointer(m)).Findex
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v38
				v39 = v38
				v40 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v39
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v40
				v41 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _42
			_42:
				if v41 != 0 {
					v43 = -int32(1)
					goto _44
				}
				v43 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
				goto _44
			_44:
				if v43 == int32(_RTmp) {
					v45 = nr
					nr++
					s_insert(tls, qbe, m+36, bp+16, v45)
				}
			case int32(_RTmp):
				v46 = nr
				nr++
				s_insert(tls, qbe, i+8+uintptr(x)*4, bp+16, v46)
				break
			}
			goto _22
		_22:
			;
			x++
		}
		r1 = 0
		for {
			if !(r1 < nr) {
				break
			}
			*(*_Ref)(unsafe.Pointer((*(*[4]uintptr)(unsafe.Pointer(bp + 16)))[r1])) = s_ralloc(tls, qbe, cur, int32(*(*uint32)(unsafe.Pointer((*(*[4]uintptr)(unsafe.Pointer(bp + 16)))[r1] + 0))&0xfffffff8>>3))
			goto _47
		_47:
			;
			r1++
		}
		if v52 = int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Ocopy); v52 {
			v48 = (*_Ins)(unsafe.Pointer(i)).Fto
			v49 = *(*_Ref)(unsafe.Pointer(i + 8))
			*(*_Ref)(unsafe.Pointer(bp)) = v48
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v49
			v50 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _51
		_51:
		}
		if v52 && v50 != 0 {
			qbe.x_curi += 16
		}
		/* try to change the register of a hinted
		 * temporary if rf is available */
		if v54 = rf != -int32(1); v54 {
			v53 = *(*int32)(unsafe.Pointer(cur + 512 + uintptr(rf)*4))
			t = v53
		}
		if v54 && v53 != 0 {
			v55 = libc.Uint32FromInt32(rf)
			v56 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(cur+768)).Ft + uintptr(v55/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v55%uint32(_NBit))) != uint64(0))
			goto _57
		_57:
			;
			if v59 = !(v56 != 0) && *(*int32)(unsafe.Pointer(s_hint(tls, qbe, t))) == rf; v59 {
				v58 = s_rfree(tls, qbe, cur, t)
				rt = v58
			}
			if v59 && v58 != -int32(1) {
				(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fvisit = -int32(1)
				s_ralloc(tls, qbe, cur, t)
				x_emit(tls, qbe, int32(_Ocopy), int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t)*192))).Fcls), _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(rt)&0x1fffffff<<3,
				}, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(rf)&0x1fffffff<<3,
				}, _Ref{})
				qbe.s_stmov += uint32(1)
				*(*int32)(unsafe.Pointer(cur + 512 + uintptr(rf)*4)) = 0
				r1 = 0
				for {
					if !(r1 < nr) {
						break
					}
					v61 = *(*_Ref)(unsafe.Pointer((*(*[4]uintptr)(unsafe.Pointer(bp + 16)))[r1]))
					v62 = _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(rt)&0x1fffffff<<3,
					}
					*(*_Ref)(unsafe.Pointer(bp)) = v61
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v62
					v63 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _64
				_64:
					if v63 != 0 {
						*(*_Ref)(unsafe.Pointer((*(*[4]uintptr)(unsafe.Pointer(bp + 16)))[r1])) = _Ref{
							F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(rf)&0x1fffffff<<3,
						}
					}
					goto _60
				_60:
					;
					r1++
				}
				/* one could iterate this logic with
				 * the newly freed rt, but in this case
				 * the above loop must be changed */
			}
		}
		goto _8
	_8:
	}
	(*_Blk)(unsafe.Pointer(b1)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
	x_idup(tls, qbe, b1+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b1)).Fnins))
}

// C documentation
//
//	/* qsort() comparison function to peel
//	 * loop nests from inside out */
func s_carve(tls *libc.TLS, qbe *_QBE, a uintptr, b uintptr) (r int32) {
	var ba, bb uintptr
	var v1, v2 int32
	_, _, _, _ = ba, bb, v1, v2
	/* todo, evaluate if this order is really
	 * better than the simple postorder */
	ba = *(*uintptr)(unsafe.Pointer(a))
	bb = *(*uintptr)(unsafe.Pointer(b))
	if (*_Blk)(unsafe.Pointer(ba)).Floop == (*_Blk)(unsafe.Pointer(bb)).Floop {
		if (*_Blk)(unsafe.Pointer(ba)).Fid > (*_Blk)(unsafe.Pointer(bb)).Fid {
			v1 = -int32(1)
		} else {
			v1 = libc.BoolInt32((*_Blk)(unsafe.Pointer(ba)).Fid < (*_Blk)(unsafe.Pointer(bb)).Fid)
		}
		return v1
	}
	if (*_Blk)(unsafe.Pointer(ba)).Floop > (*_Blk)(unsafe.Pointer(bb)).Floop {
		v2 = -int32(1)
	} else {
		v2 = +libc.Int32FromInt32(1)
	}
	return v2
}

// C documentation
//
//	/* comparison function to order temporaries
//	 * for allocation at the end of blocks */
func s_prio2(tls *libc.TLS, qbe *_QBE, t1 int32, t2 int32) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t1)*192))).Fvisit^(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t2)*192))).Fvisit < 0 { /* != signs */
		if (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t1)*192))).Fvisit != -int32(1) {
			v1 = +libc.Int32FromInt32(1)
		} else {
			v1 = -int32(1)
		}
		return v1
	}
	if *(*int32)(unsafe.Pointer(s_hint(tls, qbe, t1)))^*(*int32)(unsafe.Pointer(s_hint(tls, qbe, t2))) < 0 {
		if *(*int32)(unsafe.Pointer(s_hint(tls, qbe, t1))) != -int32(1) {
			v2 = +libc.Int32FromInt32(1)
		} else {
			v2 = -int32(1)
		}
		return v2
	}
	return libc.Int32FromUint32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t1)*192))).Fcost - (*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(t2)*192))).Fcost)
}

// C documentation
//
//	/* register allocation
//	 * depends on rpo, phi, cost, (and obviously spill)
//	 */
func x_rega(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp1 := tls.Alloc(1936)
	defer tls.Free(1936)
	var b1, b11, beg, blist, blk, bp, end, i, m, p, ps, s, v5, v53 uintptr
	var j, x, v10, v18, v20, v27, v29, v3, v31, v37, v39, v41, v44, v47, v58, v60, v66, v68, v9 int32
	var n, u, v46 _uint
	var v15, v16, v17, v24, v25, v26, v34, v35, v36, v55, v56, v57, v63, v64, v65 _Ref
	var v32, v49 bool
	var _ /* cur at bp+312 */ _RMap
	var _ /* dst at bp+1900 */ _Ref
	var _ /* old at bp+1104 */ _RMap
	var _ /* r at bp+48 */ int32
	var _ /* rl at bp+52 */ [64]int32
	var _ /* src at bp+1896 */ _Ref
	var _ /* t at bp+44 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b1, b11, beg, blist, blk, bp, end, i, j, m, n, p, ps, s, u, x, v10, v15, v16, v17, v18, v20, v24, v25, v26, v27, v29, v3, v31, v32, v34, v35, v36, v37, v39, v41, v44, v46, v47, v49, v5, v53, v55, v56, v57, v58, v60, v63, v64, v65, v66, v68, v9
	/* 1. setup */
	qbe.s_stmov = uint32(0)
	qbe.s_stblk = uint32(0)
	qbe.s_regu = uint64(0)
	qbe.s_tmp1 = (*_Fn)(unsafe.Pointer(fn)).Ftmp
	qbe.s_mem = (*_Fn)(unsafe.Pointer(fn)).Fmem
	blk = x_alloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(8))
	end = x_alloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(792))
	beg = x_alloc(tls, qbe, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk)*uint64(792))
	n = uint32(0)
	for {
		if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
			break
		}
		x_bsinit(tls, qbe, end+uintptr(n)*792+768, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
		x_bsinit(tls, qbe, beg+uintptr(n)*792+768, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
		goto _1
	_1:
		;
		n++
	}
	x_bsinit(tls, qbe, bp1+312+768, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
	x_bsinit(tls, qbe, bp1+1104+768, libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fntmp))
	qbe.s_loop = int32(m___INT_MAX__)
	*(*int32)(unsafe.Pointer(bp1 + 44)) = 0
	for {
		if !(*(*int32)(unsafe.Pointer(bp1 + 44)) < (*_Fn)(unsafe.Pointer(fn)).Fntmp) {
			break
		}
		if *(*int32)(unsafe.Pointer(bp1 + 44)) < int32(_Tmp0) {
			v3 = *(*int32)(unsafe.Pointer(bp1 + 44))
		} else {
			v3 = -int32(1)
		}
		(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(*(*int32)(unsafe.Pointer(bp1 + 44)))*192))).Fhint.Fr = v3
		(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(*(*int32)(unsafe.Pointer(bp1 + 44)))*192))).Fhint.Fw = qbe.s_loop
		(*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(*(*int32)(unsafe.Pointer(bp1 + 44)))*192))).Fvisit = -int32(1)
		goto _2
	_2:
		;
		*(*int32)(unsafe.Pointer(bp1 + 44))++
	}
	bp = blk
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		v5 = bp
		bp += 8
		*(*uintptr)(unsafe.Pointer(v5)) = b1
		goto _4
	_4:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	qsort(tls, qbe, blk, uint64((*_Fn)(unsafe.Pointer(fn)).Fnblk), uint64(8), __ccgo_fp(s_carve))
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	i = (*_Blk)(unsafe.Pointer(b1)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Ocopy) || !(x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8))) != 0) {
			break
		} else {
			s_sethint1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3), int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))
		}
		goto _6
	_6:
		;
		i += 16
	}
	/* 2. assign registers */
	bp = blk
	for {
		if !(bp < blk+uintptr((*_Fn)(unsafe.Pointer(fn)).Fnblk)*8) {
			break
		}
		b1 = *(*uintptr)(unsafe.Pointer(bp))
		n = (*_Blk)(unsafe.Pointer(b1)).Fid
		qbe.s_loop = (*_Blk)(unsafe.Pointer(b1)).Floop
		(*(*_RMap)(unsafe.Pointer(bp1 + 312))).Fn = 0
		x_bszero(tls, qbe, bp1+312+768)
		libc.X__builtin___memset_chk(tls, bp1+312+512, 0, uint64(256), ^___predefined_size_t(0))
		x = 0
		*(*int32)(unsafe.Pointer(bp1 + 44)) = int32(_Tmp0)
		for {
			if !(x_bsiter(tls, qbe, b1+136, bp1+44) != 0) {
				break
			}
			v9 = x
			x++
			j = v9
			(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j] = *(*int32)(unsafe.Pointer(bp1 + 44))
			for {
				v10 = j
				j--
				if !(v10 > 0 && s_prio2(tls, qbe, *(*int32)(unsafe.Pointer(bp1 + 44)), (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j]) > 0) {
					break
				}
				(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j+int32(1)] = (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j]
				(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j] = *(*int32)(unsafe.Pointer(bp1 + 44))
			}
			goto _8
		_8:
			;
			*(*int32)(unsafe.Pointer(bp1 + 44))++
		}
		*(*int32)(unsafe.Pointer(bp1 + 48)) = 0
		for {
			if !(x_bsiter(tls, qbe, b1+136, bp1+48) != 0 && *(*int32)(unsafe.Pointer(bp1 + 48)) < int32(_Tmp0)) {
				break
			}
			s_radd1(tls, qbe, bp1+312, *(*int32)(unsafe.Pointer(bp1 + 48)), *(*int32)(unsafe.Pointer(bp1 + 48)))
			goto _11
		_11:
			;
			*(*int32)(unsafe.Pointer(bp1 + 48))++
		}
		j = 0
		for {
			if !(j < x) {
				break
			}
			s_ralloctry(tls, qbe, bp1+312, (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j], int32(1))
			goto _12
		_12:
			;
			j++
		}
		j = 0
		for {
			if !(j < x) {
				break
			}
			s_ralloc(tls, qbe, bp1+312, (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[j])
			goto _13
		_13:
			;
			j++
		}
		s_rcopy(tls, qbe, end+uintptr(n)*792, bp1+312)
		s_doblk(tls, qbe, b1, bp1+312)
		x_bscopy(tls, qbe, b1+120, bp1+312+768)
		p = (*_Blk)(unsafe.Pointer(b1)).Fphi
		for {
			if !(p != 0) {
				break
			}
			v15 = (*_Phi)(unsafe.Pointer(p)).Fto
			*(*_Ref)(unsafe.Pointer(bp1 + 40)) = v15
			v16 = v15
			v17 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp1 + 32)) = v16
			*(*_Ref)(unsafe.Pointer(bp1 + 36)) = v17
			v18 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0xfffffff8>>3))
			goto _19
		_19:
			if v18 != 0 {
				v20 = -int32(1)
				goto _21
			}
			v20 = int32(*(*uint32)(unsafe.Pointer(bp1 + 40 + 0)) & 0x7 >> 0)
			goto _21
		_21:
			if v20 == int32(_RTmp) {
				x_bsclr(tls, qbe, b1+120, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3)))
			}
			goto _14
		_14:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		s_rcopy(tls, qbe, beg+uintptr(n)*792, bp1+312)
		goto _7
	_7:
		;
		bp += 8
	}
	/* 3. emit copies shared by multiple edges
	 * to the same block */
	s = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(s != 0) {
			break
		}
		if (*_Blk)(unsafe.Pointer(s)).Fnpred <= uint32(1) {
			goto _22
		}
		m = beg + uintptr((*_Blk)(unsafe.Pointer(s)).Fid)*792
		/* rl maps a register that is live at the
		 * beginning of s to the one used in all
		 * predecessors (if any, -1 otherwise) */
		libc.X__builtin___memset_chk(tls, bp1+52, 0, uint64(256), ^___predefined_size_t(0))
		/* to find the register of a phi in a
		 * predecessor, we have to find the
		 * corresponding argument */
		p = (*_Blk)(unsafe.Pointer(s)).Fphi
		for {
			if !(p != 0) {
				break
			}
			v24 = (*_Phi)(unsafe.Pointer(p)).Fto
			*(*_Ref)(unsafe.Pointer(bp1 + 40)) = v24
			v25 = v24
			v26 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp1 + 32)) = v25
			*(*_Ref)(unsafe.Pointer(bp1 + 36)) = v26
			v27 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0xfffffff8>>3))
			goto _28
		_28:
			if v27 != 0 {
				v29 = -int32(1)
				goto _30
			}
			v29 = int32(*(*uint32)(unsafe.Pointer(bp1 + 40 + 0)) & 0x7 >> 0)
			goto _30
		_30:
			;
			if v32 = v29 != int32(_RTmp); !v32 {
				v31 = s_rfind(tls, qbe, m, int32(*(*uint32)(unsafe.Pointer(p + 0))&0xfffffff8>>3))
				*(*int32)(unsafe.Pointer(bp1 + 48)) = v31
			}
			if v32 || v31 == -int32(1) {
				goto _23
			}
			u = uint32(0)
			for {
				if !(u < (*_Phi)(unsafe.Pointer(p)).Fnarg) {
					break
				}
				b1 = *(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(u)*8))
				*(*_Ref)(unsafe.Pointer(bp1 + 1896)) = *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(u)*4))
				v34 = *(*_Ref)(unsafe.Pointer(bp1 + 1896))
				*(*_Ref)(unsafe.Pointer(bp1 + 40)) = v34
				v35 = v34
				v36 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1 + 32)) = v35
				*(*_Ref)(unsafe.Pointer(bp1 + 36)) = v36
				v37 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0xfffffff8>>3))
				goto _38
			_38:
				if v37 != 0 {
					v39 = -int32(1)
					goto _40
				}
				v39 = int32(*(*uint32)(unsafe.Pointer(bp1 + 40 + 0)) & 0x7 >> 0)
				goto _40
			_40:
				if v39 != int32(_RTmp) {
					goto _33
				}
				x = s_rfind(tls, qbe, end+uintptr((*_Blk)(unsafe.Pointer(b1)).Fid)*792, int32(*(*uint32)(unsafe.Pointer(bp1 + 1896 + 0))&0xfffffff8>>3))
				if x == -int32(1) { /* spilled */
					goto _33
				}
				if !((*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] != 0) || (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] == x {
					v41 = x
				} else {
					v41 = -int32(1)
				}
				(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] = v41
				goto _33
			_33:
				;
				u++
			}
			if (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] == 0 {
				(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] = -int32(1)
			}
			goto _23
		_23:
			;
			p = (*_Phi)(unsafe.Pointer(p)).Flink
		}
		/* process non-phis temporaries */
		j = 0
		for {
			if !(j < (*_RMap)(unsafe.Pointer(m)).Fn) {
				break
			}
			*(*int32)(unsafe.Pointer(bp1 + 44)) = *(*int32)(unsafe.Pointer(m + uintptr(j)*4))
			*(*int32)(unsafe.Pointer(bp1 + 48)) = *(*int32)(unsafe.Pointer(m + 256 + uintptr(j)*4))
			if (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] != 0 || *(*int32)(unsafe.Pointer(bp1 + 44)) < int32(_Tmp0) {
				goto _42
			}
			bp = (*_Blk)(unsafe.Pointer(s)).Fpred
			for {
				if !(bp < (*_Blk)(unsafe.Pointer(s)).Fpred+uintptr((*_Blk)(unsafe.Pointer(s)).Fnpred)*8) {
					break
				}
				x = s_rfind(tls, qbe, end+uintptr((*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Fid)*792, *(*int32)(unsafe.Pointer(bp1 + 44)))
				if x == -int32(1) { /* spilled */
					goto _43
				}
				if !((*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] != 0) || (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] == x {
					v44 = x
				} else {
					v44 = -int32(1)
				}
				(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] = v44
				goto _43
			_43:
				;
				bp += 8
			}
			if (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] == 0 {
				(*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))] = -int32(1)
			}
			goto _42
		_42:
			;
			j++
		}
		qbe.s_npm = 0
		j = 0
		for {
			if !(j < (*_RMap)(unsafe.Pointer(m)).Fn) {
				break
			}
			*(*int32)(unsafe.Pointer(bp1 + 44)) = *(*int32)(unsafe.Pointer(m + uintptr(j)*4))
			*(*int32)(unsafe.Pointer(bp1 + 48)) = *(*int32)(unsafe.Pointer(m + 256 + uintptr(j)*4))
			x = (*(*[64]int32)(unsafe.Pointer(bp1 + 52)))[*(*int32)(unsafe.Pointer(bp1 + 48))]
			if v49 = x > 0; v49 {
				v46 = libc.Uint32FromInt32(x)
				v47 = libc.BoolInt32(*(*_bits)(unsafe.Pointer((*_BSet)(unsafe.Pointer(m+768)).Ft + uintptr(v46/uint32(_NBit))*8))&(libc.Uint64FromInt32(1)<<(v46%uint32(_NBit))) != uint64(0))
				goto _48
			_48:
			}
			if v49 && !(v47 != 0) {
				s_pmadd(tls, qbe, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(x)&0x1fffffff<<3,
				}, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 48)))&0x1fffffff<<3,
				}, int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(*(*int32)(unsafe.Pointer(bp1 + 44)))*192))).Fcls))
				*(*int32)(unsafe.Pointer(m + 256 + uintptr(j)*4)) = x
				x_bsset(tls, qbe, m+768, libc.Uint32FromInt32(x))
			}
			goto _45
		_45:
			;
			j++
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		s_pmgen(tls, qbe)
		j = int32((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		if j == 0 {
			goto _22
		}
		qbe.s_stmov += libc.Uint32FromInt32(j)
		*(*_uint)(unsafe.Pointer(s + 16)) += libc.Uint32FromInt32(j)
		i = x_alloc(tls, qbe, uint64((*_Blk)(unsafe.Pointer(s)).Fnins)*uint64(16))
		x_icpy(tls, qbe, x_icpy(tls, qbe, i, qbe.x_curi, libc.Uint64FromInt32(j)), (*_Blk)(unsafe.Pointer(s)).Fins, uint64((*_Blk)(unsafe.Pointer(s)).Fnins-libc.Uint32FromInt32(j)))
		(*_Blk)(unsafe.Pointer(s)).Fins = i
		goto _22
	_22:
		;
		s = (*_Blk)(unsafe.Pointer(s)).Flink
	}
	if qbe.x_debug[int32('R')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4273, 0)
		n = uint32(0)
		for {
			if !(n < (*_Fn)(unsafe.Pointer(fn)).Fnblk) {
				break
			}
			b1 = *(*uintptr)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Frpo + uintptr(n)*8))
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4296, libc.VaList(bp1+1912, b1+180))
			s_mdump(tls, qbe, beg+uintptr(n)*792)
			fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4307, 0)
			s_mdump(tls, qbe, end+uintptr(n)*792)
			goto _50
		_50:
			;
			n++
		}
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+82, 0)
	}
	/* 4. emit remaining copies in new blocks */
	blist = uintptr(0)
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		*(*[1]uintptr)(unsafe.Pointer(bp1 + 24)) = [1]uintptr{}
		*(*[3]uintptr)(unsafe.Pointer(bp1)) = [3]uintptr{
			0: b1 + 32,
			1: b1 + 40,
			2: bp1 + 24,
		}
		ps = bp1
		for {
			v53 = *(*uintptr)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(ps))))
			s = v53
			if !(v53 != 0) {
				break
			}
			qbe.s_npm = 0
			p = (*_Blk)(unsafe.Pointer(s)).Fphi
			for {
				if !(p != 0) {
					break
				}
				*(*_Ref)(unsafe.Pointer(bp1 + 1900)) = (*_Phi)(unsafe.Pointer(p)).Fto
				v55 = *(*_Ref)(unsafe.Pointer(bp1 + 1900))
				*(*_Ref)(unsafe.Pointer(bp1 + 40)) = v55
				v56 = v55
				v57 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1 + 32)) = v56
				*(*_Ref)(unsafe.Pointer(bp1 + 36)) = v57
				v58 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0xfffffff8>>3))
				goto _59
			_59:
				if v58 != 0 {
					v60 = -int32(1)
					goto _61
				}
				v60 = int32(*(*uint32)(unsafe.Pointer(bp1 + 40 + 0)) & 0x7 >> 0)
				goto _61
			_61:
				if v60 == int32(_RTmp) {
					*(*int32)(unsafe.Pointer(bp1 + 48)) = s_rfind(tls, qbe, beg+uintptr((*_Blk)(unsafe.Pointer(s)).Fid)*792, int32(*(*uint32)(unsafe.Pointer(bp1 + 1900 + 0))&0xfffffff8>>3))
					if *(*int32)(unsafe.Pointer(bp1 + 48)) == -int32(1) {
						goto _54
					}
					*(*_Ref)(unsafe.Pointer(bp1 + 1900)) = _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp1 + 48)))&0x1fffffff<<3,
					}
				}
				u = uint32(0)
				for {
					if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(u)*8)) != b1) {
						break
					}
					goto _62
				_62:
					;
					u++
				}
				*(*_Ref)(unsafe.Pointer(bp1 + 1896)) = *(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Farg + uintptr(u)*4))
				v63 = *(*_Ref)(unsafe.Pointer(bp1 + 1896))
				*(*_Ref)(unsafe.Pointer(bp1 + 40)) = v63
				v64 = v63
				v65 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp1 + 32)) = v64
				*(*_Ref)(unsafe.Pointer(bp1 + 36)) = v65
				v66 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp1 + 32 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp1 + 36 + 0))&0xfffffff8>>3))
				goto _67
			_67:
				if v66 != 0 {
					v68 = -int32(1)
					goto _69
				}
				v68 = int32(*(*uint32)(unsafe.Pointer(bp1 + 40 + 0)) & 0x7 >> 0)
				goto _69
			_69:
				if v68 == int32(_RTmp) {
					*(*_Ref)(unsafe.Pointer(bp1 + 1896)) = s_rref(tls, qbe, end+uintptr((*_Blk)(unsafe.Pointer(b1)).Fid)*792, int32(*(*uint32)(unsafe.Pointer(bp1 + 1896 + 0))&0xfffffff8>>3))
				}
				s_pmadd(tls, qbe, *(*_Ref)(unsafe.Pointer(bp1 + 1896)), *(*_Ref)(unsafe.Pointer(bp1 + 1900)), (*_Phi)(unsafe.Pointer(p)).Fcls)
				goto _54
			_54:
				;
				p = (*_Phi)(unsafe.Pointer(p)).Flink
			}
			*(*int32)(unsafe.Pointer(bp1 + 44)) = int32(_Tmp0)
			for {
				if !(x_bsiter(tls, qbe, s+120, bp1+44) != 0) {
					break
				}
				*(*_Ref)(unsafe.Pointer(bp1 + 1896)) = s_rref(tls, qbe, end+uintptr((*_Blk)(unsafe.Pointer(b1)).Fid)*792, *(*int32)(unsafe.Pointer(bp1 + 44)))
				*(*_Ref)(unsafe.Pointer(bp1 + 1900)) = s_rref(tls, qbe, beg+uintptr((*_Blk)(unsafe.Pointer(s)).Fid)*792, *(*int32)(unsafe.Pointer(bp1 + 44)))
				s_pmadd(tls, qbe, *(*_Ref)(unsafe.Pointer(bp1 + 1896)), *(*_Ref)(unsafe.Pointer(bp1 + 1900)), int32((*(*_Tmp)(unsafe.Pointer(qbe.s_tmp1 + uintptr(*(*int32)(unsafe.Pointer(bp1 + 44)))*192))).Fcls))
				goto _70
			_70:
				;
				*(*int32)(unsafe.Pointer(bp1 + 44))++
			}
			qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
			s_pmgen(tls, qbe)
			if qbe.x_curi == uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16 {
				goto _52
			}
			b11 = x_newblk(tls, qbe)
			(*_Blk)(unsafe.Pointer(b11)).Floop = ((*_Blk)(unsafe.Pointer(b1)).Floop + (*_Blk)(unsafe.Pointer(s)).Floop) / int32(2)
			(*_Blk)(unsafe.Pointer(b11)).Flink = blist
			blist = b11
			(*_Fn)(unsafe.Pointer(fn)).Fnblk++
			x_strf(tls, qbe, b11+180, __ccgo_ts+4323, libc.VaList(bp1+1912, b1+180, s+180))
			(*_Blk)(unsafe.Pointer(b11)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
			qbe.s_stmov += (*_Blk)(unsafe.Pointer(b11)).Fnins
			qbe.s_stblk += uint32(1)
			x_idup(tls, qbe, b11+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b11)).Fnins))
			(*_Blk)(unsafe.Pointer(b11)).Fjmp.Ftype1 = int16(_Jjmp)
			(*_Blk)(unsafe.Pointer(b11)).Fs1 = s
			*(*uintptr)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(ps)))) = b11
			goto _52
		_52:
			;
			ps += 8
		}
		if !((*_Blk)(unsafe.Pointer(b1)).Flink != 0) {
			(*_Blk)(unsafe.Pointer(b1)).Flink = blist
			break
		}
		goto _51
	_51:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b1)).Fphi = uintptr(0)
		goto _71
	_71:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	(*_Fn)(unsafe.Pointer(fn)).Freg = qbe.s_regu
	if qbe.x_debug[int32('R')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4329, 0)
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4365, libc.VaList(bp1+1912, qbe.s_stmov))
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4382, libc.VaList(bp1+1912, qbe.s_stblk))
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+4399, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

const _SecText = 0

const _SecData = 1

const _SecBss = 2

func x_emitlnk(tls *libc.TLS, qbe *_QBE, n uintptr, l uintptr, s int32, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var pfx, sfx, v1 uintptr
	_, _, _ = pfx, sfx, v1
	if int32(*(*int8)(unsafe.Pointer(n))) == int32('"') {
		v1 = __ccgo_ts + 4511
	} else {
		v1 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
	}
	pfx = v1
	sfx = __ccgo_ts + 4511
	if qbe.x_T.Fapple != 0 && (*_Lnk)(unsafe.Pointer(l)).Fthread != 0 {
		(*_Lnk)(unsafe.Pointer(l)).Fsec = __ccgo_ts + 4512
		(*_Lnk)(unsafe.Pointer(l)).Fsecf = __ccgo_ts + 4519
		sfx = __ccgo_ts + 4554
		fputs(tls, qbe, __ccgo_ts+4564, f)
		fprintf(tls, qbe, f, __ccgo_ts+4618, libc.VaList(bp+8, pfx, n))
		fprintf(tls, qbe, f, __ccgo_ts+4625, libc.VaList(bp+8, pfx, n, sfx))
	}
	if (*_Lnk)(unsafe.Pointer(l)).Fsec != 0 {
		fprintf(tls, qbe, f, __ccgo_ts+4673, libc.VaList(bp+8, (*_Lnk)(unsafe.Pointer(l)).Fsec))
		if (*_Lnk)(unsafe.Pointer(l)).Fsecf != 0 {
			fprintf(tls, qbe, f, __ccgo_ts+4685, libc.VaList(bp+8, (*_Lnk)(unsafe.Pointer(l)).Fsecf))
		}
	} else {
		fputs(tls, qbe, *(*uintptr)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.s_sec)) + libc.BoolUintptr(int32((*_Lnk)(unsafe.Pointer(l)).Fthread) != 0)*24 + uintptr(s)*8)), f)
	}
	fputc(tls, qbe, int32('\n'), f)
	if (*_Lnk)(unsafe.Pointer(l)).Falign != 0 {
		fprintf(tls, qbe, f, __ccgo_ts+4689, libc.VaList(bp+8, int32((*_Lnk)(unsafe.Pointer(l)).Falign)))
	}
	if (*_Lnk)(unsafe.Pointer(l)).Fexport != 0 {
		fprintf(tls, qbe, f, __ccgo_ts+4701, libc.VaList(bp+8, pfx, n))
	}
	fprintf(tls, qbe, f, __ccgo_ts+4714, libc.VaList(bp+8, pfx, n, sfx))
}

func x_emitfnlnk(tls *libc.TLS, qbe *_QBE, n uintptr, l uintptr, f uintptr) {
	x_emitlnk(tls, qbe, n, l, int32(_SecText), f)
}

func x_emitdat(tls *libc.TLS, qbe *_QBE, d uintptr, f uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var p, v1, v2 uintptr
	_, _, _ = p, v1, v2
	switch (*_Dat)(unsafe.Pointer(d)).Ftype1 {
	case int32(_DStart):
		qbe.s_zero = 0
	case int32(_DEnd):
		if (*_Lnk)(unsafe.Pointer((*_Dat)(unsafe.Pointer(d)).Flnk)).Fcommon != 0 {
			if qbe.s_zero == int64(-int32(1)) {
				_die_(tls, qbe, __ccgo_ts+4751, __ccgo_ts+4758, 0)
			}
			if int32(*(*int8)(unsafe.Pointer((*_Dat)(unsafe.Pointer(d)).Fname))) == int32('"') {
				v1 = __ccgo_ts + 4511
			} else {
				v1 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
			}
			p = v1
			fprintf(tls, qbe, f, __ccgo_ts+4789, libc.VaList(bp+8, p, (*_Dat)(unsafe.Pointer(d)).Fname, qbe.s_zero))
			if (*_Lnk)(unsafe.Pointer((*_Dat)(unsafe.Pointer(d)).Flnk)).Falign != 0 {
				fprintf(tls, qbe, f, __ccgo_ts+4805, libc.VaList(bp+8, int32((*_Lnk)(unsafe.Pointer((*_Dat)(unsafe.Pointer(d)).Flnk)).Falign)))
			}
			fputc(tls, qbe, int32('\n'), f)
		} else {
			if qbe.s_zero != int64(-int32(1)) {
				x_emitlnk(tls, qbe, (*_Dat)(unsafe.Pointer(d)).Fname, (*_Dat)(unsafe.Pointer(d)).Flnk, int32(_SecBss), f)
				fprintf(tls, qbe, f, __ccgo_ts+4809, libc.VaList(bp+8, qbe.s_zero))
			}
		}
	case int32(_DZ):
		if qbe.s_zero != int64(-int32(1)) {
			qbe.s_zero += (*_Dat)(unsafe.Pointer(d)).Fu.Fnum
		} else {
			fprintf(tls, qbe, f, __ccgo_ts+4809, libc.VaList(bp+8, (*_Dat)(unsafe.Pointer(d)).Fu.Fnum))
		}
	default:
		if qbe.s_zero != int64(-int32(1)) {
			x_emitlnk(tls, qbe, (*_Dat)(unsafe.Pointer(d)).Fname, (*_Dat)(unsafe.Pointer(d)).Flnk, int32(_SecData), f)
			if qbe.s_zero > 0 {
				fprintf(tls, qbe, f, __ccgo_ts+4809, libc.VaList(bp+8, qbe.s_zero))
			}
			qbe.s_zero = int64(-int32(1))
		}
		if (*_Dat)(unsafe.Pointer(d)).Fisstr != 0 {
			if (*_Dat)(unsafe.Pointer(d)).Ftype1 != int32(_DB) {
				_err(tls, qbe, __ccgo_ts+4826, 0)
			}
			fprintf(tls, qbe, f, __ccgo_ts+4867, libc.VaList(bp+8, *(*uintptr)(unsafe.Pointer(&(*_Dat)(unsafe.Pointer(d)).Fu))))
		} else {
			if (*_Dat)(unsafe.Pointer(d)).Fisref != 0 {
				if int32(*(*int8)(unsafe.Pointer((*(*struct {
					Fname uintptr
					Foff  _int64_t
				})(unsafe.Pointer(d + 24))).Fname))) == int32('"') {
					v2 = __ccgo_ts + 4511
				} else {
					v2 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
				}
				p = v2
				fprintf(tls, qbe, f, __ccgo_ts+4879, libc.VaList(bp+8, qbe.s_dtoa[(*_Dat)(unsafe.Pointer(d)).Ftype1], p, (*(*struct {
					Fname uintptr
					Foff  _int64_t
				})(unsafe.Pointer(d + 24))).Fname, (*(*struct {
					Fname uintptr
					Foff  _int64_t
				})(unsafe.Pointer(d + 24))).Foff))
			} else {
				fprintf(tls, qbe, f, __ccgo_ts+4893, libc.VaList(bp+8, qbe.s_dtoa[(*_Dat)(unsafe.Pointer(d)).Ftype1], (*_Dat)(unsafe.Pointer(d)).Fu.Fnum))
			}
		}
		break
	}
}

type _Asmbits = struct {
	Fbits [16]int8
	Fsize int32
	Flink uintptr
}

func x_stashbits(tls *libc.TLS, qbe *_QBE, bits uintptr, size int32) (r int32) {
	var b, pb, v2 uintptr
	var i int32
	_, _, _, _ = b, i, pb, v2
	pb = uintptr(unsafe.Pointer(&qbe.s_stash))
	i = libc.Int32FromInt32(0)
	for {
		v2 = *(*uintptr)(unsafe.Pointer(pb))
		b = v2
		if !(v2 != 0) {
			break
		}
		if size <= (*_Asmbits)(unsafe.Pointer(b)).Fsize {
			if libc.Xmemcmp(tls, bits, b, libc.Uint64FromInt32(size)) == 0 {
				return i
			}
		}
		goto _1
	_1:
		;
		pb = b + 24
		i++
	}
	b = x_emalloc(tls, qbe, uint64(32))
	libc.X__builtin___memcpy_chk(tls, b, bits, libc.Uint64FromInt32(size), ^___predefined_size_t(0))
	(*_Asmbits)(unsafe.Pointer(b)).Fsize = size
	(*_Asmbits)(unsafe.Pointer(b)).Flink = uintptr(0)
	*(*uintptr)(unsafe.Pointer(pb)) = b
	return i
}

func s_emitfin(tls *libc.TLS, qbe *_QBE, f uintptr, sec uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var b, p, v4 uintptr
	var d float64
	var i, lg int32
	_, _, _, _, _, _ = b, d, i, lg, p, v4
	if !(qbe.s_stash != 0) {
		return
	}
	fprintf(tls, qbe, f, __ccgo_ts+4902, 0)
	lg = int32(4)
	for {
		if !(lg >= int32(2)) {
			break
		}
		b = qbe.s_stash
		i = libc.Int32FromInt32(0)
		for {
			if !(b != 0) {
				break
			}
			if (*_Asmbits)(unsafe.Pointer(b)).Fsize == int32(1)<<lg {
				fprintf(tls, qbe, f, __ccgo_ts+4934, libc.VaList(bp+8, *(*uintptr)(unsafe.Pointer(sec + uintptr(lg-int32(2))*8)), lg, uintptr(unsafe.Pointer(&qbe.x_T))+136, i))
				p = b
				for {
					if !(p < b+uintptr((*_Asmbits)(unsafe.Pointer(b)).Fsize)) {
						break
					}
					fprintf(tls, qbe, f, __ccgo_ts+4966, libc.VaList(bp+8, *(*_int32_t)(unsafe.Pointer(p))))
					goto _3
				_3:
					;
					p += uintptr(4)
				}
				if lg <= int32(3) {
					if lg == int32(2) {
						d = float64(*(*float32)(unsafe.Pointer(b)))
					} else {
						d = *(*float64)(unsafe.Pointer(b))
					}
					fprintf(tls, qbe, f, __ccgo_ts+4976, libc.VaList(bp+8, d))
				} else {
					fprintf(tls, qbe, f, __ccgo_ts+4988, 0)
				}
			}
			goto _2
		_2:
			;
			b = (*_Asmbits)(unsafe.Pointer(b)).Flink
			i++
		}
		goto _1
	_1:
		;
		lg--
	}
	for {
		v4 = qbe.s_stash
		b = v4
		if !(v4 != 0) {
			break
		}
		qbe.s_stash = (*_Asmbits)(unsafe.Pointer(b)).Flink
		free(tls, qbe, b)
	}
}

func x_elf_emitfin(tls *libc.TLS, qbe *_QBE, f uintptr) {
	s_emitfin(tls, qbe, f, uintptr(unsafe.Pointer(&qbe.s_sec1)))
	fprintf(tls, qbe, f, __ccgo_ts+4999, 0)
}

func x_elf_emitfnfin(tls *libc.TLS, qbe *_QBE, fn uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	fprintf(tls, qbe, f, __ccgo_ts+5038, libc.VaList(bp+8, fn))
	fprintf(tls, qbe, f, __ccgo_ts+5059, libc.VaList(bp+8, fn, fn))
}

func x_macho_emitfin(tls *libc.TLS, qbe *_QBE, f uintptr) {
	s_emitfin(tls, qbe, f, uintptr(unsafe.Pointer(&qbe.s_sec2)))
}

func x_emitdbgfile(tls *libc.TLS, qbe *_QBE, fn uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var id _uint32_t
	var n, v2 _uint
	_, _, _ = id, n, v2
	id = x_intern(tls, qbe, fn)
	n = uint32(0)
	for {
		if !(n < qbe.s_nfile) {
			break
		}
		if *(*_uint32_t)(unsafe.Pointer(qbe.s_file + uintptr(n)*4)) == id {
			/* gas requires positive
			 * file numbers */
			qbe.s_curfile = n + uint32(1)
			return
		}
		goto _1
	_1:
		;
		n++
	}
	if !(qbe.s_file != 0) {
		qbe.s_file = x_vnew(tls, qbe, uint64(0), uint64(4), int32(_PHeap))
	}
	qbe.s_nfile++
	v2 = qbe.s_nfile
	x_vgrow(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_file)), uint64(v2))
	*(*_uint32_t)(unsafe.Pointer(qbe.s_file + uintptr(qbe.s_nfile-uint32(1))*4)) = id
	qbe.s_curfile = qbe.s_nfile
	fprintf(tls, qbe, f, __ccgo_ts+5141, libc.VaList(bp+8, qbe.s_curfile, fn))
}

func x_emitdbgloc(tls *libc.TLS, qbe *_QBE, line _uint, col _uint, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	if col != uint32(0) {
		fprintf(tls, qbe, f, __ccgo_ts+5154, libc.VaList(bp+8, qbe.s_curfile, line, col))
	} else {
		fprintf(tls, qbe, f, __ccgo_ts+5170, libc.VaList(bp+8, qbe.s_curfile, line))
	}
}

type _Amd64Op = struct {
	Fnmem  int8
	Fzflag int8
	Flflag int8
}

const _RAX = 1

const _RCX = 2

const _RDX = 3

const _RSI = 4

const _RDI = 5

const _R8 = 6

const _R9 = 7

const _R10 = 8

const _R11 = 9

const _RBX = 10

const _R12 = 11

const _R13 = 12

const _R14 = 13

const _R15 = 14

const _RBP = 15

const _RSP = 16

const _XMM0 = 17

const _XMM1 = 18

const _XMM2 = 19

const _XMM3 = 20

const _XMM4 = 21

const _XMM5 = 22

const _XMM6 = 23

const _XMM7 = 24

const _XMM8 = 25

const _XMM9 = 26

const _XMM10 = 27

const _XMM11 = 28

const _XMM12 = 29

const _XMM13 = 30

const _XMM14 = 31

const _NFPR = 15

const _NGPR = 16

const _NGPS = 9

const _NFPS = 15

const _NCLR = 5

func s_amd64_memargs(tls *libc.TLS, qbe *_QBE, op int32) (r int32) {
	return int32(qbe.x_amd64_op[op].Fnmem)
}

func (qbe *_QBE) init1() {
	p := unsafe.Pointer(&qbe.x_T_amd64_sysv)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(x_amd64_sysv_retregs)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(x_amd64_sysv_argregs)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(s_amd64_memargs)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(x_elimsb)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(x_amd64_sysv_abi)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(x_amd64_isel)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(x_amd64_emitfn)
	*(*uintptr)(unsafe.Add(p, 128)) = __ccgo_fp(x_elf_emitfin)
}

func (qbe *_QBE) init2() {
	p := unsafe.Pointer(&qbe.x_T_amd64_apple)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(x_amd64_sysv_retregs)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(x_amd64_sysv_argregs)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(s_amd64_memargs)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(x_elimsb)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(x_amd64_sysv_abi)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(x_amd64_isel)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(x_amd64_emitfn)
	*(*uintptr)(unsafe.Add(p, 128)) = __ccgo_fp(x_macho_emitfin)
}

type _AClass = struct {
	Ftype1 uintptr
	Finmem int32
	Falign int32
	Fsize  _uint
	Fcls   [2]int32
	Fref   [2]_Ref
}

type _RAlloc = struct {
	Fi    _Ins
	Flink uintptr
}

func s_classify(tls *libc.TLS, qbe *_QBE, a uintptr, t uintptr, s _uint) {
	var cls, f uintptr
	var n, s1 _uint
	_, _, _, _ = cls, f, n, s1
	n = uint32(0)
	s1 = s
	for {
		if !(n < (*_Typ)(unsafe.Pointer(t)).Fnunion) {
			break
		}
		f = (*_Typ)(unsafe.Pointer(t)).Ffields + uintptr(n)*264
		for {
			if !((*_Field)(unsafe.Pointer(f)).Ftype1 != int32(_FEnd)) {
				break
			}
			cls = a + 20 + uintptr(s/uint32(8))*4
			switch (*_Field)(unsafe.Pointer(f)).Ftype1 {
			case int32(_FEnd):
				_die_(tls, qbe, __ccgo_ts+5183, __ccgo_ts+3532, 0)
				fallthrough
			case int32(_FPad):
				/* don't change anything */
				s += (*_Field)(unsafe.Pointer(f)).Flen1
			case int32(_Fs):
				fallthrough
			case int32(_Fd):
				if *(*int32)(unsafe.Pointer(cls)) == int32(_Kx) {
					*(*int32)(unsafe.Pointer(cls)) = int32(_Kd)
				}
				s += (*_Field)(unsafe.Pointer(f)).Flen1
			case int32(_Fb):
				fallthrough
			case int32(_Fh):
				fallthrough
			case int32(_Fw):
				fallthrough
			case int32(_Fl):
				*(*int32)(unsafe.Pointer(cls)) = int32(_Kl)
				s += (*_Field)(unsafe.Pointer(f)).Flen1
			case int32(_FTyp):
				s_classify(tls, qbe, a, qbe.x_typ+uintptr((*_Field)(unsafe.Pointer(f)).Flen1)*112, s)
				s = _uint(uint64(s) + (*(*_Typ)(unsafe.Pointer(qbe.x_typ + uintptr((*_Field)(unsafe.Pointer(f)).Flen1)*112))).Fsize)
				break
			}
			goto _2
		_2:
			;
			f += 8
		}
		goto _1
	_1:
		;
		n++
		s = s1
	}
}

func s_typclass(tls *libc.TLS, qbe *_QBE, a uintptr, t uintptr) {
	var al, sz _uint
	_, _ = al, sz
	sz = uint32((*_Typ)(unsafe.Pointer(t)).Fsize)
	al = uint32(1) << (*_Typ)(unsafe.Pointer(t)).Falign
	/* the ABI requires sizes to be rounded
	 * up to the nearest multiple of 8, moreover
	 * it makes it easy load and store structures
	 * in registers
	 */
	if al < uint32(8) {
		al = uint32(8)
	}
	sz = (sz + al - uint32(1)) & -al
	(*_AClass)(unsafe.Pointer(a)).Ftype1 = t
	(*_AClass)(unsafe.Pointer(a)).Fsize = sz
	(*_AClass)(unsafe.Pointer(a)).Falign = (*_Typ)(unsafe.Pointer(t)).Falign
	if (*_Typ)(unsafe.Pointer(t)).Fisdark != 0 || sz > uint32(16) || sz == uint32(0) {
		/* large or unaligned structures are
		 * required to be passed in memory
		 */
		(*_AClass)(unsafe.Pointer(a)).Finmem = int32(1)
		return
	}
	*(*int32)(unsafe.Pointer(a + 20)) = int32(_Kx)
	*(*int32)(unsafe.Pointer(a + 20 + 1*4)) = int32(_Kx)
	(*_AClass)(unsafe.Pointer(a)).Finmem = 0
	s_classify(tls, qbe, a, t, uint32(0))
}

func s_retr(tls *libc.TLS, qbe *_QBE, reg uintptr, aret uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var ca, k, n, v1, v3 int32
	var v4 uintptr
	var _ /* nr at bp+0 */ [2]int32
	_, _, _, _, _, _ = ca, k, n, v1, v3, v4
	v1 = libc.Int32FromInt32(0)
	(*(*[2]int32)(unsafe.Pointer(bp)))[int32(1)] = v1
	(*(*[2]int32)(unsafe.Pointer(bp)))[0] = v1
	ca = 0
	n = 0
	for {
		if !(libc.Uint32FromInt32(n)*uint32(8) < (*_AClass)(unsafe.Pointer(aret)).Fsize) {
			break
		}
		k = *(*int32)(unsafe.Pointer(aret + 20 + uintptr(n)*4)) >> int32(1)
		v4 = bp + uintptr(k)*4
		v3 = *(*int32)(unsafe.Pointer(v4))
		*(*int32)(unsafe.Pointer(v4))++
		*(*_Ref)(unsafe.Pointer(reg + uintptr(n)*4)) = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.s_retreg)) + uintptr(k)*8 + uintptr(v3)*4)))&0x1fffffff<<3,
		}
		ca += int32(1) << (int32(2) * k)
		goto _2
	_2:
		;
		n++
	}
	return ca
}

func s_selret(tls *libc.TLS, qbe *_QBE, b uintptr, fn uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var ca, j, k int32
	var r, r0 _Ref
	var _ /* aret at bp+8 */ _AClass
	var _ /* reg at bp+0 */ [2]_Ref
	_, _, _, _, _ = ca, j, k, r, r0
	j = int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)
	if !(libc.Uint32FromInt32(j)-uint32(_Jretw) <= libc.Uint32FromInt32(int32(_Jret0)-int32(_Jretw))) || j == int32(_Jret0) {
		return
	}
	r0 = (*_Blk)(unsafe.Pointer(b)).Fjmp.Farg
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jret0)
	if j == int32(_Jretc) {
		s_typclass(tls, qbe, bp+8, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112)
		if (*(*_AClass)(unsafe.Pointer(bp + 8))).Finmem != 0 {
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
			}, (*_Fn)(unsafe.Pointer(fn)).Fretr, _Ref{})
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | uint32((*_Typ)(unsafe.Pointer((*(*_AClass)(unsafe.Pointer(bp + 8))).Ftype1)).Fsize&libc.Uint64FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, r0, (*_Fn)(unsafe.Pointer(fn)).Fretr)
			ca = int32(1)
		} else {
			ca = s_retr(tls, qbe, bp, bp+8)
			if (*(*_AClass)(unsafe.Pointer(bp + 8))).Fsize > uint32(8) {
				r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Oload), int32(_Kl), (*(*[2]_Ref)(unsafe.Pointer(bp)))[int32(1)], r, _Ref{})
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, r0, x_getcon(tls, qbe, int64(8), fn))
			}
			x_emit(tls, qbe, int32(_Oload), int32(_Kl), (*(*[2]_Ref)(unsafe.Pointer(bp)))[0], r0, _Ref{})
		}
	} else {
		k = j - int32(_Jretw)
		if k>>int32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
			}, r0, _Ref{})
			ca = int32(1)
		} else {
			x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_XMM0)&0x1fffffff<<3,
			}, r0, _Ref{})
			ca = libc.Int32FromInt32(1) << libc.Int32FromInt32(2)
		}
	}
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Farg = _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(ca)&0x1fffffff<<3,
	}
}

func s_argsclass(tls *libc.TLS, qbe *_QBE, i0 uintptr, i1 uintptr, ac uintptr, op int32, aret uintptr, env uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var a, i, pn uintptr
	var envc, n, ni, ns, varc, v2 int32
	var _ /* nint at bp+0 */ int32
	var _ /* nsse at bp+4 */ int32
	_, _, _, _, _, _, _, _, _ = a, envc, i, n, ni, ns, pn, varc, v2
	if aret != 0 && (*_AClass)(unsafe.Pointer(aret)).Finmem != 0 {
		*(*int32)(unsafe.Pointer(bp)) = int32(5)
	} else {
		*(*int32)(unsafe.Pointer(bp)) = int32(6)
	}
	*(*int32)(unsafe.Pointer(bp + 4)) = int32(8)
	varc = 0
	envc = 0
	i = i0
	a = ac
	for {
		if !(i < i1) {
			break
		}
		switch int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - op + int32(_Oarg) {
		case int32(_Oarg):
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
				pn = bp
			} else {
				pn = bp + 4
			}
			if *(*int32)(unsafe.Pointer(pn)) > 0 {
				*(*int32)(unsafe.Pointer(pn))--
				(*_AClass)(unsafe.Pointer(a)).Finmem = 0
			} else {
				(*_AClass)(unsafe.Pointer(a)).Finmem = int32(2)
			}
			(*_AClass)(unsafe.Pointer(a)).Falign = int32(3)
			(*_AClass)(unsafe.Pointer(a)).Fsize = uint32(8)
			*(*int32)(unsafe.Pointer(a + 20)) = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30)
		case int32(_Oargc):
			n = int32(*(*uint32)(unsafe.Pointer(i + 8 + 0)) & 0xfffffff8 >> 3)
			s_typclass(tls, qbe, a, qbe.x_typ+uintptr(n)*112)
			if (*_AClass)(unsafe.Pointer(a)).Finmem != 0 {
				goto _1
			}
			v2 = libc.Int32FromInt32(0)
			ns = v2
			ni = v2
			n = 0
			for {
				if !(libc.Uint32FromInt32(n)*uint32(8) < (*_AClass)(unsafe.Pointer(a)).Fsize) {
					break
				}
				if *(*int32)(unsafe.Pointer(a + 20 + uintptr(n)*4))>>int32(1) == 0 {
					ni++
				} else {
					ns++
				}
				goto _3
			_3:
				;
				n++
			}
			if *(*int32)(unsafe.Pointer(bp)) >= ni && *(*int32)(unsafe.Pointer(bp + 4)) >= ns {
				*(*int32)(unsafe.Pointer(bp)) -= ni
				*(*int32)(unsafe.Pointer(bp + 4)) -= ns
			} else {
				(*_AClass)(unsafe.Pointer(a)).Finmem = int32(1)
			}
		case int32(_Oarge):
			envc = int32(1)
			if op == int32(_Opar) {
				*(*_Ref)(unsafe.Pointer(env)) = (*_Ins)(unsafe.Pointer(i)).Fto
			} else {
				*(*_Ref)(unsafe.Pointer(env)) = *(*_Ref)(unsafe.Pointer(i + 8))
			}
		case int32(_Oargv):
			varc = int32(1)
		default:
			_die_(tls, qbe, __ccgo_ts+5183, __ccgo_ts+3532, 0)
		}
		goto _1
	_1:
		;
		i += 16
		a += 40
	}
	if varc != 0 && envc != 0 {
		_err(tls, qbe, __ccgo_ts+5200, 0)
	}
	return (varc|envc)<<int32(12) | (int32(6)-*(*int32)(unsafe.Pointer(bp)))<<int32(4) | (int32(8)-*(*int32)(unsafe.Pointer(bp + 4)))<<int32(8)
}

/* layout of call's second argument (RCall)
 *
 *  29     12    8    4  3  0
 *  |0...00|x|xxxx|xxxx|xx|xx|                  range
 *          |    |    |  |  ` gp regs returned (0..2)
 *          |    |    |  ` sse regs returned   (0..2)
 *          |    |    ` gp regs passed         (0..6)
 *          |    ` sse regs passed             (0..8)
 *          ` 1 if rax is used to pass data    (0..1)
 */

func x_amd64_sysv_retregs(tls *libc.TLS, qbe *_QBE, _r _Ref, p uintptr) (r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	var b _bits
	var nf, ni int32
	_, _, _ = b, nf, ni
	b = uint64(0)
	ni = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) & int32(3)
	nf = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(2) & int32(3)
	if ni >= int32(1) {
		b |= libc.Uint64FromInt32(1) << int32(_RAX)
	}
	if ni >= int32(2) {
		b |= libc.Uint64FromInt32(1) << int32(_RDX)
	}
	if nf >= int32(1) {
		b |= libc.Uint64FromInt32(1) << int32(_XMM0)
	}
	if nf >= int32(2) {
		b |= libc.Uint64FromInt32(1) << int32(_XMM1)
	}
	if p != 0 {
		*(*int32)(unsafe.Pointer(p)) = ni
		*(*int32)(unsafe.Pointer(p + 1*4)) = nf
	}
	return b
}

func x_amd64_sysv_argregs(tls *libc.TLS, qbe *_QBE, _r _Ref, p uintptr) (r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	var b _bits
	var j, nf, ni, ra int32
	var v3 uint64
	_, _, _, _, _, _ = b, j, nf, ni, ra, v3
	b = uint64(0)
	ni = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(4) & int32(15)
	nf = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(8) & int32(15)
	ra = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(12) & int32(1)
	j = 0
	for {
		if !(j < ni) {
			break
		}
		b |= libc.Uint64FromInt32(1) << qbe.x_amd64_sysv_rsave[j]
		goto _1
	_1:
		;
		j++
	}
	j = 0
	for {
		if !(j < nf) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_XMM0) + j)
		goto _2
	_2:
		;
		j++
	}
	if p != 0 {
		*(*int32)(unsafe.Pointer(p)) = ni + ra
		*(*int32)(unsafe.Pointer(p + 1*4)) = nf
	}
	if ra != 0 {
		v3 = libc.Uint64FromInt32(1) << int32(_RAX)
	} else {
		v3 = uint64(0)
	}
	return b | v3
}

func s_rarg(tls *libc.TLS, qbe *_QBE, ty int32, ni uintptr, ns uintptr) (r _Ref) {
	var v1, v3 int32
	var v2, v4 uintptr
	_, _, _, _ = v1, v2, v3, v4
	if ty>>int32(1) == 0 {
		v2 = ni
		v1 = *(*int32)(unsafe.Pointer(v2))
		*(*int32)(unsafe.Pointer(v2))++
		return _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(qbe.x_amd64_sysv_rsave[v1])&0x1fffffff<<3,
		}
	} else {
		v4 = ns
		v3 = *(*int32)(unsafe.Pointer(v4))
		*(*int32)(unsafe.Pointer(v4))++
		return _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(int32(_XMM0)+v3)&0x1fffffff<<3,
		}
	}
	return r
}

func s_selcall(tls *libc.TLS, qbe *_QBE, fn uintptr, i0 uintptr, i1 uintptr, rap uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var a1, ac, i, ra, v6 uintptr
	var al, ca, v11, v14, v16, v3, v9 int32
	var off, stk _uint
	var r, r1, r2, v1, v12, v13, v2, v7, v8 _Ref
	var _ /* aret at bp+8 */ _AClass
	var _ /* env at bp+64 */ _Ref
	var _ /* ni at bp+48 */ int32
	var _ /* ns at bp+52 */ int32
	var _ /* reg at bp+56 */ [2]_Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, ac, al, ca, i, off, r, r1, r2, ra, stk, v1, v11, v12, v13, v14, v16, v2, v3, v6, v7, v8, v9
	*(*_Ref)(unsafe.Pointer(bp + 64)) = _Ref{}
	ac = x_alloc(tls, qbe, libc.Uint64FromInt64((int64(i1)-int64(i0))/16)*uint64(40))
	v1 = *(*_Ref)(unsafe.Pointer(i1 + 8 + 1*4))
	v2 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v1
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
	v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _4
_4:
	if !(v3 != 0) {
		s_typclass(tls, qbe, bp+8, qbe.x_typ+uintptr(int32(*(*uint32)(unsafe.Pointer(i1 + 8 + 1*4 + 0))&0xfffffff8>>3))*112)
		ca = s_argsclass(tls, qbe, i0, i1, ac, int32(_Oarg), bp+8, bp+64)
	} else {
		ca = s_argsclass(tls, qbe, i0, i1, ac, int32(_Oarg), uintptr(0), bp+64)
	}
	stk = uint32(0)
	a1 = ac + uintptr((int64(i1)-int64(i0))/16)*40
	for {
		if !(a1 > ac) {
			break
		}
		a1 -= 40
		v6 = a1
		if (*_AClass)(unsafe.Pointer(v6)).Finmem != 0 {
			if (*_AClass)(unsafe.Pointer(a1)).Falign > int32(4) {
				_err(tls, qbe, __ccgo_ts+5245, 0)
			}
			stk += (*_AClass)(unsafe.Pointer(a1)).Fsize
			if (*_AClass)(unsafe.Pointer(a1)).Falign == int32(4) {
				stk += stk & uint32(15)
			}
		}
		goto _5
	_5:
	}
	stk += stk & uint32(15)
	if stk != 0 {
		r = x_getcon(tls, qbe, -libc.Int64FromUint32(stk), fn)
		x_emit(tls, qbe, int32(_Osalloc), int32(_Kl), _Ref{}, r, _Ref{})
	}
	v7 = *(*_Ref)(unsafe.Pointer(i1 + 8 + 1*4))
	v8 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v7
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v8
	v9 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _10
_10:
	if !(v9 != 0) {
		if (*(*_AClass)(unsafe.Pointer(bp + 8))).Finmem != 0 {
			/* get the return location from eax
			 * it saves one callee-save reg */
			r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
			}, _Ref{})
			ca += int32(1)
		} else {
			/* todo, may read out of bounds.
			 * gcc did this up until 5.2, but
			 * this should still be fixed.
			 */
			if (*(*_AClass)(unsafe.Pointer(bp + 8))).Fsize > uint32(8) {
				r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				*(*_Ref)(unsafe.Pointer(bp + 8 + 28 + 1*4)) = x_newtmp(tls, qbe, __ccgo_ts+5196, *(*int32)(unsafe.Pointer(bp + 8 + 20 + 1*4)), fn)
				x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(bp + 8 + 28 + 1*4)), r)
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, (*_Ins)(unsafe.Pointer(i1)).Fto, x_getcon(tls, qbe, int64(8), fn))
			}
			*(*_Ref)(unsafe.Pointer(bp + 8 + 28)) = x_newtmp(tls, qbe, __ccgo_ts+5196, *(*int32)(unsafe.Pointer(bp + 8 + 20)), fn)
			x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(bp + 8 + 28)), (*_Ins)(unsafe.Pointer(i1)).Fto)
			ca += s_retr(tls, qbe, bp+56, bp+8)
			if (*(*_AClass)(unsafe.Pointer(bp + 8))).Fsize > uint32(8) {
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(bp + 8 + 20 + 1*4)), *(*_Ref)(unsafe.Pointer(bp + 8 + 28 + 1*4)), (*(*[2]_Ref)(unsafe.Pointer(bp + 56)))[int32(1)], _Ref{})
			}
			x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(bp + 8 + 20)), *(*_Ref)(unsafe.Pointer(bp + 8 + 28)), (*(*[2]_Ref)(unsafe.Pointer(bp + 56)))[0], _Ref{})
			r1 = (*_Ins)(unsafe.Pointer(i1)).Fto
		}
		/* allocate return pad */
		ra = x_alloc(tls, qbe, uint64(24))
		/* specific to NAlign == 3 */
		if (*(*_AClass)(unsafe.Pointer(bp + 8))).Falign >= int32(2) {
			v11 = (*(*_AClass)(unsafe.Pointer(bp + 8))).Falign - int32(2)
		} else {
			v11 = 0
		}
		al = v11
		(*_RAlloc)(unsafe.Pointer(ra)).Fi = _Ins{
			F__ccgo0: libc.Uint32FromInt32(int32(_Oalloc)+al)&0x3fffffff<<0 | uint32(_Kl)&0x3<<30,
			Fto:      r1,
			Farg: [2]_Ref{
				0: x_getcon(tls, qbe, libc.Int64FromUint32((*(*_AClass)(unsafe.Pointer(bp + 8))).Fsize), fn),
			},
		}
		(*_RAlloc)(unsafe.Pointer(ra)).Flink = *(*uintptr)(unsafe.Pointer(rap))
		*(*uintptr)(unsafe.Pointer(rap)) = ra
	} else {
		ra = uintptr(0)
		if int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
			}, _Ref{})
			ca += int32(1)
		} else {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_XMM0)&0x1fffffff<<3,
			}, _Ref{})
			ca += libc.Int32FromInt32(1) << libc.Int32FromInt32(2)
		}
	}
	x_emit(tls, qbe, int32(_Ocall), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), _Ref{}, *(*_Ref)(unsafe.Pointer(i1 + 8)), _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(ca)&0x1fffffff<<3,
	})
	v12 = _Ref{}
	v13 = *(*_Ref)(unsafe.Pointer(bp + 64))
	*(*_Ref)(unsafe.Pointer(bp)) = v12
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v13
	v14 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _15
_15:
	if !(v14 != 0) {
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
		}, *(*_Ref)(unsafe.Pointer(bp + 64)), _Ref{})
	} else {
		if ca>>int32(12)&int32(1) != 0 { /* vararg call */
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
			}, x_getcon(tls, qbe, int64(ca>>int32(8)&int32(15)), fn), _Ref{})
		}
	}
	v16 = libc.Int32FromInt32(0)
	*(*int32)(unsafe.Pointer(bp + 52)) = v16
	*(*int32)(unsafe.Pointer(bp + 48)) = v16
	if ra != 0 && (*(*_AClass)(unsafe.Pointer(bp + 8))).Finmem != 0 {
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), s_rarg(tls, qbe, int32(_Kl), bp+48, bp+52), (*_RAlloc)(unsafe.Pointer(ra)).Fi.Fto, _Ref{})
	} /* pass hidden argument */
	i = i0
	a1 = ac
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) >= int32(_Oarge) || (*_AClass)(unsafe.Pointer(a1)).Finmem != 0 {
			goto _17
		}
		r1 = s_rarg(tls, qbe, *(*int32)(unsafe.Pointer(a1 + 20)), bp+48, bp+52)
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargc) {
			if (*_AClass)(unsafe.Pointer(a1)).Fsize > uint32(8) {
				r2 = s_rarg(tls, qbe, *(*int32)(unsafe.Pointer(a1 + 20 + 1*4)), bp+48, bp+52)
				r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Oload), *(*int32)(unsafe.Pointer(a1 + 20 + 1*4)), r2, r, _Ref{})
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), x_getcon(tls, qbe, int64(8), fn))
			}
			x_emit(tls, qbe, int32(_Oload), *(*int32)(unsafe.Pointer(a1 + 20)), r1, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), _Ref{})
		} else {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), r1, *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
		}
		goto _17
	_17:
		;
		i += 16
		a1 += 40
	}
	if !(stk != 0) {
		return
	}
	r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	i = i0
	a1 = ac
	off = libc.Uint32FromInt32(0)
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) >= int32(_Oarge) || !((*_AClass)(unsafe.Pointer(a1)).Finmem != 0) {
			goto _18
		}
		r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargc) {
			if (*_AClass)(unsafe.Pointer(a1)).Falign == int32(4) {
				off += off & uint32(15)
			}
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | uint32((*_Typ)(unsafe.Pointer((*_AClass)(unsafe.Pointer(a1)).Ftype1)).Fsize&libc.Uint64FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), r1)
		} else {
			x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8)), r1)
		}
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, r, x_getcon(tls, qbe, libc.Int64FromUint32(off), fn))
		off += (*_AClass)(unsafe.Pointer(a1)).Fsize
		goto _18
	_18:
		;
		i += 16
		a1 += 40
	}
	x_emit(tls, qbe, int32(_Osalloc), int32(_Kl), r, x_getcon(tls, qbe, libc.Int64FromUint32(stk), fn), _Ref{})
}

func s_selpar(tls *libc.TLS, qbe *_QBE, fn uintptr, i0 uintptr, i1 uintptr) (r1 int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a1, ac, i uintptr
	var al, fa, s, v1, v3, v7 int32
	var r, v5, v6 _Ref
	var _ /* aret at bp+8 */ _AClass
	var _ /* env at bp+56 */ _Ref
	var _ /* ni at bp+48 */ int32
	var _ /* ns at bp+52 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _ = a1, ac, al, fa, i, r, s, v1, v3, v5, v6, v7
	*(*_Ref)(unsafe.Pointer(bp + 56)) = _Ref{}
	ac = x_alloc(tls, qbe, libc.Uint64FromInt64((int64(i1)-int64(i0))/16)*uint64(40))
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
	v1 = libc.Int32FromInt32(0)
	*(*int32)(unsafe.Pointer(bp + 52)) = v1
	*(*int32)(unsafe.Pointer(bp + 48)) = v1
	if (*_Fn)(unsafe.Pointer(fn)).Fretty >= 0 {
		s_typclass(tls, qbe, bp+8, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112)
		fa = s_argsclass(tls, qbe, i0, i1, ac, int32(_Opar), bp+8, bp+56)
	} else {
		fa = s_argsclass(tls, qbe, i0, i1, ac, int32(_Opar), uintptr(0), bp+56)
	}
	(*_Fn)(unsafe.Pointer(fn)).Freg = x_amd64_sysv_argregs(tls, qbe, _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(fa)&0x1fffffff<<3,
	}, uintptr(0))
	i = i0
	a1 = ac
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Oparc) || (*_AClass)(unsafe.Pointer(a1)).Finmem != 0 {
			goto _2
		}
		if (*_AClass)(unsafe.Pointer(a1)).Fsize > uint32(8) {
			r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			*(*_Ref)(unsafe.Pointer(a1 + 28 + 1*4)) = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(a1 + 28 + 1*4)), r)
			x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, (*_Ins)(unsafe.Pointer(i)).Fto, x_getcon(tls, qbe, int64(8), fn))
		}
		*(*_Ref)(unsafe.Pointer(a1 + 28)) = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(a1 + 28)), (*_Ins)(unsafe.Pointer(i)).Fto)
		/* specific to NAlign == 3 */
		if (*_AClass)(unsafe.Pointer(a1)).Falign >= int32(2) {
			v3 = (*_AClass)(unsafe.Pointer(a1)).Falign - int32(2)
		} else {
			v3 = 0
		}
		al = v3
		x_emit(tls, qbe, int32(_Oalloc)+al, int32(_Kl), (*_Ins)(unsafe.Pointer(i)).Fto, x_getcon(tls, qbe, libc.Int64FromUint32((*_AClass)(unsafe.Pointer(a1)).Fsize), fn), _Ref{})
		goto _2
	_2:
		;
		i += 16
		a1 += 40
	}
	if (*_Fn)(unsafe.Pointer(fn)).Fretty >= 0 && (*(*_AClass)(unsafe.Pointer(bp + 8))).Finmem != 0 {
		r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), r, s_rarg(tls, qbe, int32(_Kl), bp+48, bp+52), _Ref{})
		(*_Fn)(unsafe.Pointer(fn)).Fretr = r
	}
	i = i0
	a1 = ac
	s = libc.Int32FromInt32(4)
	for {
		if !(i < i1) {
			break
		}
		switch (*_AClass)(unsafe.Pointer(a1)).Finmem {
		case int32(1):
			if (*_AClass)(unsafe.Pointer(a1)).Falign > int32(4) {
				_err(tls, qbe, __ccgo_ts+5245, 0)
			}
			if (*_AClass)(unsafe.Pointer(a1)).Falign == int32(4) {
				s = (s + int32(3)) & -int32(4)
			}
			(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192))).Fslot = -s
			s = int32(uint32(s) + (*_AClass)(unsafe.Pointer(a1)).Fsize/libc.Uint32FromInt32(4))
			goto _4
		case int32(2):
			x_emit(tls, qbe, int32(_Oload), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i)).Fto, _Ref{
				F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(-s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			s += int32(2)
			goto _4
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Opare) {
			goto _4
		}
		r = s_rarg(tls, qbe, *(*int32)(unsafe.Pointer(a1 + 20)), bp+48, bp+52)
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oparc) {
			x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(a1 + 20)), *(*_Ref)(unsafe.Pointer(a1 + 28)), r, _Ref{})
			if (*_AClass)(unsafe.Pointer(a1)).Fsize > uint32(8) {
				r = s_rarg(tls, qbe, *(*int32)(unsafe.Pointer(a1 + 20 + 1*4)), bp+48, bp+52)
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(a1 + 20 + 1*4)), *(*_Ref)(unsafe.Pointer(a1 + 28 + 1*4)), r, _Ref{})
			}
		} else {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i)).Fto, r, _Ref{})
		}
		goto _4
	_4:
		;
		i += 16
		a1 += 40
	}
	v5 = _Ref{}
	v6 = *(*_Ref)(unsafe.Pointer(bp + 56))
	*(*_Ref)(unsafe.Pointer(bp)) = v5
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v6
	v7 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _8
_8:
	if !(v7 != 0) {
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), *(*_Ref)(unsafe.Pointer(bp + 56)), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
		}, _Ref{})
	}
	return fa | s*int32(4)<<int32(12)
}

func s_split(tls *libc.TLS, qbe *_QBE, fn uintptr, b uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var bn, v2 uintptr
	var v1 _uint
	_, _, _ = bn, v1, v2
	(*_Fn)(unsafe.Pointer(fn)).Fnblk++
	bn = x_newblk(tls, qbe)
	(*_Blk)(unsafe.Pointer(bn)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
	x_idup(tls, qbe, bn+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(bn)).Fnins))
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
	v2 = b + 60
	*(*_uint)(unsafe.Pointer(v2))++
	v1 = *(*_uint)(unsafe.Pointer(v2))
	(*_Blk)(unsafe.Pointer(bn)).Fvisit = v1
	x_strf(tls, qbe, bn+180, __ccgo_ts+188, libc.VaList(bp+8, b+180, (*_Blk)(unsafe.Pointer(b)).Fvisit))
	(*_Blk)(unsafe.Pointer(bn)).Floop = (*_Blk)(unsafe.Pointer(b)).Floop
	(*_Blk)(unsafe.Pointer(bn)).Flink = (*_Blk)(unsafe.Pointer(b)).Flink
	(*_Blk)(unsafe.Pointer(b)).Flink = bn
	return bn
}

func s_chpred(tls *libc.TLS, qbe *_QBE, b uintptr, bp uintptr, bp1 uintptr) {
	var a _uint
	var p uintptr
	_, _ = a, p
	p = (*_Blk)(unsafe.Pointer(b)).Fphi
	for {
		if !(p != 0) {
			break
		}
		a = uint32(0)
		for {
			if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)) != bp) {
				break
			}
			goto _2
		_2:
			;
			a++
		}
		*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)) = bp1
		goto _1
	_1:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
}

func s_selvaarg(tls *libc.TLS, qbe *_QBE, fn uintptr, b uintptr, i uintptr) {
	var ap, c, c16, c4, c8, loc, lreg, lstk, nr, r0, r1, v1, v2, v4 _Ref
	var b0, breg, bstk uintptr
	var isint, v3 int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = ap, b0, breg, bstk, c, c16, c4, c8, isint, loc, lreg, lstk, nr, r0, r1, v1, v2, v3, v4
	c4 = x_getcon(tls, qbe, int64(4), fn)
	c8 = x_getcon(tls, qbe, int64(8), fn)
	c16 = x_getcon(tls, qbe, int64(16), fn)
	ap = *(*_Ref)(unsafe.Pointer(i + 8))
	isint = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0)
	/* @b [...]
	       r0 =l add ap, (0 or 4)
	       nr =l loadsw r0
	       r1 =w cultw nr, (48 or 176)
	       jnz r1, @breg, @bstk
	   @breg
	       r0 =l add ap, 16
	       r1 =l loadl r0
	       lreg =l add r1, nr
	       r0 =w add nr, (8 or 16)
	       r1 =l add ap, (0 or 4)
	       storew r0, r1
	   @bstk
	       r0 =l add ap, 8
	       lstk =l loadl r0
	       r1 =l add lstk, 8
	       storel r1, r0
	   @b0
	       %loc =l phi @breg %lreg, @bstk %lstk
	       i->to =(i->cls) load %loc
	*/
	loc = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Oload), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i)).Fto, loc, _Ref{})
	b0 = s_split(tls, qbe, fn, b)
	(*_Blk)(unsafe.Pointer(b0)).Fjmp = (*_Blk)(unsafe.Pointer(b)).Fjmp
	(*_Blk)(unsafe.Pointer(b0)).Fs1 = (*_Blk)(unsafe.Pointer(b)).Fs1
	(*_Blk)(unsafe.Pointer(b0)).Fs2 = (*_Blk)(unsafe.Pointer(b)).Fs2
	if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 {
		s_chpred(tls, qbe, (*_Blk)(unsafe.Pointer(b)).Fs1, b, b0)
	}
	if (*_Blk)(unsafe.Pointer(b)).Fs2 != 0 && (*_Blk)(unsafe.Pointer(b)).Fs2 != (*_Blk)(unsafe.Pointer(b)).Fs1 {
		s_chpred(tls, qbe, (*_Blk)(unsafe.Pointer(b)).Fs2, b, b0)
	}
	lreg = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	nr = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kw), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorew), int32(_Kw), _Ref{}, r0, r1)
	if isint != 0 {
		v1 = _Ref{
			F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
		}
	} else {
		v1 = c4
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, ap, v1)
	if isint != 0 {
		v2 = c8
	} else {
		v2 = c16
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kw), r0, nr, v2)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), lreg, r1, nr)
	x_emit(tls, qbe, int32(_Oload), int32(_Kl), r1, r0, _Ref{})
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, c16)
	breg = s_split(tls, qbe, fn, b)
	(*_Blk)(unsafe.Pointer(breg)).Fjmp.Ftype1 = int16(_Jjmp)
	(*_Blk)(unsafe.Pointer(breg)).Fs1 = b0
	lstk = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r1, r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, lstk, c8)
	x_emit(tls, qbe, int32(_Oload), int32(_Kl), lstk, r0, _Ref{})
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, c8)
	bstk = s_split(tls, qbe, fn, b)
	(*_Blk)(unsafe.Pointer(bstk)).Fjmp.Ftype1 = int16(_Jjmp)
	(*_Blk)(unsafe.Pointer(bstk)).Fs1 = b0
	(*_Blk)(unsafe.Pointer(b0)).Fphi = x_alloc(tls, qbe, uint64(40))
	*(*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)) = _Phi{
		Fto:   loc,
		Farg:  x_vnew(tls, qbe, uint64(2), uint64(4), int32(_PFn)),
		Fblk:  x_vnew(tls, qbe, uint64(2), uint64(8), int32(_PFn)),
		Fnarg: uint32(2),
		Fcls:  int32(_Kl),
	}
	*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Fblk)) = bstk
	*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Fblk + 1*8)) = breg
	*(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Farg)) = lstk
	*(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Farg + 1*4)) = lreg
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kw), fn)
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jjnz)
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Farg = r1
	(*_Blk)(unsafe.Pointer(b)).Fs1 = breg
	(*_Blk)(unsafe.Pointer(b)).Fs2 = bstk
	if isint != 0 {
		v3 = int32(48)
	} else {
		v3 = int32(176)
	}
	c = x_getcon(tls, qbe, int64(v3), fn)
	x_emit(tls, qbe, int32(_Ocmpw)+int32(_Ciult), int32(_Kw), r1, nr, c)
	x_emit(tls, qbe, int32(_Oloadsw), int32(_Kl), nr, r0, _Ref{})
	if isint != 0 {
		v4 = _Ref{
			F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
		}
	} else {
		v4 = c4
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, v4)
}

func s_selvastart(tls *libc.TLS, qbe *_QBE, fn uintptr, fa int32, ap _Ref) {
	var fp, gp, sp int32
	var r0, r1 _Ref
	_, _, _, _, _ = fp, gp, r0, r1, sp
	gp = fa >> int32(4) & int32(15) * int32(8)
	fp = int32(48) + fa>>int32(8)&int32(15)*int32(16)
	sp = fa >> int32(12)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r1, r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RBP)&0x1fffffff<<3,
	}, x_getcon(tls, qbe, int64(-int32(176)), fn))
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(16), fn))
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r1, r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RBP)&0x1fffffff<<3,
	}, x_getcon(tls, qbe, int64(sp), fn))
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(8), fn))
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorew), int32(_Kw), _Ref{}, x_getcon(tls, qbe, int64(fp), fn), r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(4), fn))
	x_emit(tls, qbe, int32(_Ostorew), int32(_Kw), _Ref{}, x_getcon(tls, qbe, int64(gp), fn), ap)
}

func x_amd64_sysv_abi(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var b, i, i0, ip, v3, v4, v6 uintptr
	var fa, n int32
	var _ /* ral at bp+0 */ uintptr
	_, _, _, _, _, _, _, _, _ = b, fa, i, i0, ip, n, v3, v4, v6
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fvisit = uint32(0)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	/* lower parameters */
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	i = (*_Blk)(unsafe.Pointer(b)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
			break
		}
		if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Opar) <= libc.Uint32FromInt32(int32(_Opare)-int32(_Opar))) {
			break
		}
		goto _2
	_2:
		;
		i += 16
	}
	fa = s_selpar(tls, qbe, fn, (*_Blk)(unsafe.Pointer(b)).Fins, i)
	n = int32(libc.Int64FromUint32((*_Blk)(unsafe.Pointer(b)).Fnins) - (int64(i)-int64((*_Blk)(unsafe.Pointer(b)).Fins))/16 + (___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16)-int64(qbe.x_curi))/16)
	i0 = x_alloc(tls, qbe, libc.Uint64FromInt32(n)*uint64(16))
	v3 = i0
	ip = v3
	ip = x_icpy(tls, qbe, v3, qbe.x_curi, libc.Uint64FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16)-int64(qbe.x_curi))/16))
	ip = x_icpy(tls, qbe, ip, i, libc.Uint64FromInt64((___predefined_ptrdiff_t((*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16)-int64(i))/16))
	(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt32(n)
	(*_Blk)(unsafe.Pointer(b)).Fins = i0
	/* lower calls, returns, and vararg instructions */
	*(*uintptr)(unsafe.Pointer(bp)) = uintptr(0)
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for cond := true; cond; cond = b != (*_Fn)(unsafe.Pointer(fn)).Fstart {
		v4 = (*_Blk)(unsafe.Pointer(b)).Flink
		b = v4
		if !(v4 != 0) {
			b = (*_Fn)(unsafe.Pointer(fn)).Fstart
		} /* do it last */
		if (*_Blk)(unsafe.Pointer(b)).Fvisit != 0 {
			continue
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		s_selret(tls, qbe, b, fn)
		i = (*_Blk)(unsafe.Pointer(b)).Fins + uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b)).Fins) {
				break
			}
			i -= 16
			v6 = i
			switch int32(*(*uint32)(unsafe.Pointer(v6 + 0)) & 0x3fffffff >> 0) {
			default:
				goto _7
			case int32(_Ocall):
				goto _8
			case int32(_Ovastart):
				goto _9
			case int32(_Ovaarg):
				goto _10
			case int32(_Oargc):
				goto _11
			case int32(_Oarg):
				goto _12
			}
			goto _13
		_7:
			;
			x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(i)))
			goto _13
		_8:
			;
			i0 = i
		_16:
			;
			if !(i0 > (*_Blk)(unsafe.Pointer(b)).Fins) {
				goto _14
			}
			if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i0 - libc.UintptrFromInt32(1)*16 + 0))&0x3fffffff>>0))-uint32(_Oarg) <= libc.Uint32FromInt32(int32(_Oargv)-int32(_Oarg))) {
				goto _14
			}
			goto _15
		_15:
			;
			i0 -= 16
			goto _16
			goto _14
		_14:
			;
			s_selcall(tls, qbe, fn, i0, i, bp)
			i = i0
			goto _13
		_9:
			;
			s_selvastart(tls, qbe, fn, fa, *(*_Ref)(unsafe.Pointer(i + 8)))
			goto _13
		_10:
			;
			s_selvaarg(tls, qbe, fn, b, i)
			goto _13
		_12:
			;
		_11:
			;
			_die_(tls, qbe, __ccgo_ts+5183, __ccgo_ts+3532, 0)
		_13:
			;
			goto _5
		_5:
		}
		if b == (*_Fn)(unsafe.Pointer(fn)).Fstart {
			for {
				if !(*(*uintptr)(unsafe.Pointer(bp)) != 0) {
					break
				}
				x_emiti(tls, qbe, (*_RAlloc)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Fi)
				goto _17
			_17:
				;
				*(*uintptr)(unsafe.Pointer(bp)) = (*_RAlloc)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Flink
			}
		}
		(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b)).Fnins))
	}
	if qbe.x_debug[int32('A')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+5288, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

func s_noimm(tls *libc.TLS, qbe *_QBE, _r _Ref, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var val _int64_t
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _ = val, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RCon) {
		return 0
	}
	switch (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32))).Ftype1 {
	case int32(_CAddr):
		/* we only support the 'small'
		 * code model of the ABI, this
		 * means that we can always
		 * address data with 32bits
		 */
		return 0
	case int32(_CBits):
		val = *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32 + 16))
		return libc.BoolInt32(val < int64(-libc.Int32FromInt32(2147483647)-libc.Int32FromInt32(1)) || val > int64(2147483647))
	default:
		_die_(tls, qbe, __ccgo_ts+5312, __ccgo_ts+5325, 0)
	}
	return r
}

func s_rslot(tls *libc.TLS, qbe *_QBE, _r _Ref, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RTmp) {
		return -int32(1)
	}
	return (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*192))).Fslot
}

func s_hascon(tls *libc.TLS, qbe *_QBE, _r _Ref, pc uintptr, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	switch v6 {
	case int32(_RCon):
		*(*uintptr)(unsafe.Pointer(pc)) = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
		return int32(1)
	case int32(_RMem):
		*(*uintptr)(unsafe.Pointer(pc)) = (*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*48
		return int32(1)
	default:
		return 0
	}
	return r
}

func s_fixarg(tls *libc.TLS, qbe *_QBE, r1 uintptr, k int32, i uintptr, fn uintptr) {
	bp := tls.Alloc(176)
	defer tls.Free(176)
	var m, v12 uintptr
	var n, op, s, v11, v13, v17, v19, v2, v24, v26, v32, v34, v38, v6, v8 int32
	var r11, r2, r3, v1, v14, v15, v16, v21, v22, v23, v29, v3, v30, v31, v36, v37, v4, v5 _Ref
	var v10, v28 bool
	var _ /* a at bp+48 */ _Addr
	var _ /* buf at bp+12 */ [32]int8
	var _ /* c at bp+128 */ uintptr
	var _ /* cc at bp+96 */ _Con
	var _ /* r0 at bp+136 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = m, n, op, r11, r2, r3, s, v1, v10, v11, v12, v13, v14, v15, v16, v17, v19, v2, v21, v22, v23, v24, v26, v28, v29, v3, v30, v31, v32, v34, v36, v37, v38, v4, v5, v6, v8
	v1 = *(*_Ref)(unsafe.Pointer(r1))
	*(*_Ref)(unsafe.Pointer(bp + 136)) = v1
	r11 = v1
	s = s_rslot(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 136)), fn)
	if i != 0 {
		v2 = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0)
	} else {
		v2 = int32(_Ocopy)
	}
	op = v2
	if v10 = k>>int32(1) == int32(1); v10 {
		v3 = *(*_Ref)(unsafe.Pointer(bp + 136))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v3
		v4 = v3
		v5 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v4
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v5
		v6 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _7
	_7:
		if v6 != 0 {
			v8 = -int32(1)
			goto _9
		}
		v8 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _9
	_9:
	}
	if v10 && v8 == int32(_RCon) {
		/* load floating points from memory
		 * slots, they can't be used as
		 * immediates
		 */
		r11 = _Ref{
			F__ccgo0: uint32(_RMem)&0x7<<0 | libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fnmem)&0x1fffffff<<3,
		}
		v12 = fn + 40
		*(*int32)(unsafe.Pointer(v12))++
		v11 = *(*int32)(unsafe.Pointer(v12))
		x_vgrow(tls, qbe, fn+24, libc.Uint64FromInt32(v11))
		libc.X__builtin___memset_chk(tls, bp+48, 0, uint64(48), ^___predefined_size_t(0))
		(*(*_Addr)(unsafe.Pointer(bp + 48))).Foffset.Ftype1 = int32(_CAddr)
		if k&int32(1) != 0 {
			v13 = int32(8)
		} else {
			v13 = int32(4)
		}
		n = x_stashbits(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 136 + 0))&0xfffffff8>>3))*32+16, v13)
		/* quote the name so that we do not
		 * add symbol prefixes on the apple
		 * target variant
		 */
		libc.X__builtin___sprintf_chk(tls, bp+12, 0, ^___predefined_size_t(0), __ccgo_ts+5342, libc.VaList(bp+152, uintptr(unsafe.Pointer(&qbe.x_T))+136, n))
		(*(*_Addr)(unsafe.Pointer(bp + 48))).Foffset.Fsym.Fid = x_intern(tls, qbe, bp+12)
		*(*_Mem)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr((*_Fn)(unsafe.Pointer(fn)).Fnmem-int32(1))*48)) = *(*_Addr)(unsafe.Pointer(bp + 48))
	} else {
		if op != int32(_Ocopy) && k == int32(_Kl) && s_noimm(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 136)), fn) != 0 {
			/* load constants that do not fit in
			 * a 32bit signed integer into a
			 * long temporary
			 */
			r11 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), r11, *(*_Ref)(unsafe.Pointer(bp + 136)), _Ref{})
		} else {
			if s != -int32(1) {
				/* load fast locals' addresses into
				 * temporaries right before the
				 * instruction
				 */
				r11 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), r11, _Ref{
					F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
				}, _Ref{})
			} else {
				if qbe.x_T.Fapple != 0 && s_hascon(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 136)), bp+128, fn) != 0 && (*_Con)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 128)))).Ftype1 == int32(_CAddr) && (*_Con)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 128)))).Fsym.Ftype1 == int32(_SThr) {
					r11 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
					if *(*_int64_t)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 128)) + 16)) != 0 {
						r2 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
						*(*_Con)(unsafe.Pointer(bp + 96)) = _Con{
							Ftype1: int32(_CBits),
						}
						*(*_int64_t)(unsafe.Pointer(bp + 96 + 16)) = *(*_int64_t)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 128)) + 16))
						r3 = x_newcon(tls, qbe, bp+96, fn)
						x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r11, r2, r3)
					} else {
						r2 = r11
					}
					x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), r2, _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
					}, _Ref{})
					r2 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
					r3 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
					x_emit(tls, qbe, int32(_Ocall), 0, _Ref{}, r3, _Ref{
						F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(17)&0x1fffffff<<3,
					})
					x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RDI)&0x1fffffff<<3,
					}, r2, _Ref{})
					x_emit(tls, qbe, int32(_Oload), int32(_Kl), r3, r2, _Ref{})
					*(*_Con)(unsafe.Pointer(bp + 96)) = *(*_Con)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 128))))
					*(*_int64_t)(unsafe.Pointer(bp + 96 + 16)) = 0
					r3 = x_newcon(tls, qbe, bp+96, fn)
					x_emit(tls, qbe, int32(_Oload), int32(_Kl), r2, r3, _Ref{})
					v14 = *(*_Ref)(unsafe.Pointer(bp + 136))
					*(*_Ref)(unsafe.Pointer(bp + 8)) = v14
					v15 = v14
					v16 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp)) = v15
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v16
					v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _18
				_18:
					if v17 != 0 {
						v19 = -int32(1)
						goto _20
					}
					v19 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
					goto _20
				_20:
					if v19 == int32(_RMem) {
						m = (*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 136 + 0))&0xfffffff8>>3))*48
						(*_Addr)(unsafe.Pointer(m)).Foffset.Ftype1 = int32(_CUndef)
						(*_Addr)(unsafe.Pointer(m)).Fbase = r11
						r11 = *(*_Ref)(unsafe.Pointer(bp + 136))
					}
				} else {
					if v28 = !(libc.Uint32FromInt32(op)-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) && r1 == i+8+1*4) && !(libc.Uint32FromInt32(op)-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb))) && op != int32(_Ocall); v28 {
						v21 = *(*_Ref)(unsafe.Pointer(bp + 136))
						*(*_Ref)(unsafe.Pointer(bp + 8)) = v21
						v22 = v21
						v23 = _Ref{}
						*(*_Ref)(unsafe.Pointer(bp)) = v22
						*(*_Ref)(unsafe.Pointer(bp + 4)) = v23
						v24 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
						goto _25
					_25:
						if v24 != 0 {
							v26 = -int32(1)
							goto _27
						}
						v26 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
						goto _27
					_27:
					}
					if v28 && v26 == int32(_RCon) && (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 136 + 0))&0xfffffff8>>3))*32))).Ftype1 == int32(_CAddr) {
						/* apple as does not support 32-bit
						 * absolute addressing, use a rip-
						 * relative leaq instead
						 */
						r11 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
						x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), r11, *(*_Ref)(unsafe.Pointer(bp + 136)), _Ref{})
					} else {
						v29 = *(*_Ref)(unsafe.Pointer(bp + 136))
						*(*_Ref)(unsafe.Pointer(bp + 8)) = v29
						v30 = v29
						v31 = _Ref{}
						*(*_Ref)(unsafe.Pointer(bp)) = v30
						*(*_Ref)(unsafe.Pointer(bp + 4)) = v31
						v32 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
						goto _33
					_33:
						if v32 != 0 {
							v34 = -int32(1)
							goto _35
						}
						v34 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
						goto _35
					_35:
						if v34 == int32(_RMem) {
							/* eliminate memory operands of
							 * the form $foo(%rip, ...)
							 */
							m = (*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 136 + 0))&0xfffffff8>>3))*48
							v36 = (*_Addr)(unsafe.Pointer(m)).Fbase
							v37 = _Ref{}
							*(*_Ref)(unsafe.Pointer(bp)) = v36
							*(*_Ref)(unsafe.Pointer(bp + 4)) = v37
							v38 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
							goto _39
						_39:
							if v38 != 0 {
								if (*_Addr)(unsafe.Pointer(m)).Foffset.Ftype1 == int32(_CAddr) {
									*(*_Ref)(unsafe.Pointer(bp + 136)) = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
									x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), *(*_Ref)(unsafe.Pointer(bp + 136)), x_newcon(tls, qbe, m, fn), _Ref{})
									(*_Addr)(unsafe.Pointer(m)).Foffset.Ftype1 = int32(_CUndef)
									(*_Addr)(unsafe.Pointer(m)).Fbase = *(*_Ref)(unsafe.Pointer(bp + 136))
								}
							}
						}
					}
				}
			}
		}
	}
	*(*_Ref)(unsafe.Pointer(r1)) = r11
}

func s_seladdr(tls *libc.TLS, qbe *_QBE, r1 uintptr, tn uintptr, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var r0, v1, v12, v13, v16, v17, v18, v2, v3, v8, v9 _Ref
	var v10, v14, v19, v21, v24, v4, v6 int32
	var v23 bool
	var v25 uintptr
	var _ /* a at bp+16 */ _Addr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = r0, v1, v10, v12, v13, v14, v16, v17, v18, v19, v2, v21, v23, v24, v25, v3, v4, v6, v8, v9
	r0 = *(*_Ref)(unsafe.Pointer(r1))
	v1 = r0
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		libc.X__builtin___memset_chk(tls, bp+16, 0, uint64(48), ^___predefined_size_t(0))
		if !(s_amatch(tls, qbe, bp+16, tn, r0, fn) != 0) {
			return
		}
		v8 = (*(*_Addr)(unsafe.Pointer(bp + 16))).Fbase
		v9 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v8
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
		v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _11
	_11:
		if !(v10 != 0) {
			if (*(*_Addr)(unsafe.Pointer(bp + 16))).Foffset.Ftype1 == int32(_CAddr) {
				/* apple as does not support
				 * $foo(%r0, %r1, M); try to
				 * rewrite it or bail out if
				 * impossible
				 */
				v12 = (*(*_Addr)(unsafe.Pointer(bp + 16))).Findex
				v13 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp)) = v12
				*(*_Ref)(unsafe.Pointer(bp + 4)) = v13
				v14 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
				goto _15
			_15:
				;
				if v23 = !(v14 != 0); !v23 {
					v16 = (*(*_Addr)(unsafe.Pointer(bp + 16))).Fbase
					*(*_Ref)(unsafe.Pointer(bp + 8)) = v16
					v17 = v16
					v18 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp)) = v17
					*(*_Ref)(unsafe.Pointer(bp + 4)) = v18
					v19 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
					goto _20
				_20:
					if v19 != 0 {
						v21 = -int32(1)
						goto _22
					}
					v21 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
					goto _22
				_22:
				}
				if v23 || v21 != int32(_RTmp) {
					return
				} else {
					(*(*_Addr)(unsafe.Pointer(bp + 16))).Findex = (*(*_Addr)(unsafe.Pointer(bp + 16))).Fbase
					(*(*_Addr)(unsafe.Pointer(bp + 16))).Fscale = int32(1)
					(*(*_Addr)(unsafe.Pointer(bp + 16))).Fbase = _Ref{}
				}
			}
		}
		x_chuse(tls, qbe, r0, -int32(1), fn)
		v25 = fn + 40
		*(*int32)(unsafe.Pointer(v25))++
		v24 = *(*int32)(unsafe.Pointer(v25))
		x_vgrow(tls, qbe, fn+24, libc.Uint64FromInt32(v24))
		*(*_Mem)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fmem + uintptr((*_Fn)(unsafe.Pointer(fn)).Fnmem-int32(1))*48)) = *(*_Addr)(unsafe.Pointer(bp + 16))
		x_chuse(tls, qbe, (*(*_Addr)(unsafe.Pointer(bp + 16))).Fbase, +libc.Int32FromInt32(1), fn)
		x_chuse(tls, qbe, (*(*_Addr)(unsafe.Pointer(bp + 16))).Findex, +libc.Int32FromInt32(1), fn)
		*(*_Ref)(unsafe.Pointer(r1)) = _Ref{
			F__ccgo0: uint32(_RMem)&0x7<<0 | libc.Uint32FromInt32((*_Fn)(unsafe.Pointer(fn)).Fnmem-libc.Int32FromInt32(1))&0x1fffffff<<3,
		}
	}
}

func s_cmpswap(tls *libc.TLS, qbe *_QBE, arg uintptr, op int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _ = v1, v2, v3, v4, v6
	switch op {
	case int32(_NCmpI) + int32(_Cflt):
		fallthrough
	case int32(_NCmpI) + int32(_Cfle):
		return int32(1)
	case int32(_NCmpI) + int32(_Cfgt):
		fallthrough
	case int32(_NCmpI) + int32(_Cfge):
		return 0
	}
	v1 = *(*_Ref)(unsafe.Pointer(arg))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	return libc.BoolInt32(v6 == int32(_RCon))
}

func s_selcmp(tls *libc.TLS, qbe *_QBE, arg uintptr, k int32, swap int32, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var icmp uintptr
	var r1, v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _, _ = icmp, r1, v1, v2, v3, v4, v6
	if swap != 0 {
		r1 = *(*_Ref)(unsafe.Pointer(arg + 1*4))
		*(*_Ref)(unsafe.Pointer(arg + 1*4)) = *(*_Ref)(unsafe.Pointer(arg))
		*(*_Ref)(unsafe.Pointer(arg)) = r1
	}
	x_emit(tls, qbe, int32(_Oxcmp), k, _Ref{}, *(*_Ref)(unsafe.Pointer(arg + 1*4)), *(*_Ref)(unsafe.Pointer(arg)))
	icmp = qbe.x_curi
	v1 = *(*_Ref)(unsafe.Pointer(arg))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RCon) {
		*(*_Ref)(unsafe.Pointer(icmp + 8 + 1*4)) = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
		x_emit(tls, qbe, int32(_Ocopy), k, *(*_Ref)(unsafe.Pointer(icmp + 8 + 1*4)), *(*_Ref)(unsafe.Pointer(arg)), _Ref{})
		s_fixarg(tls, qbe, qbe.x_curi+8, k, qbe.x_curi, fn)
	}
	s_fixarg(tls, qbe, icmp+8, k, icmp, fn)
	s_fixarg(tls, qbe, icmp+8+1*4, k, icmp, fn)
}

func s_sel(tls *libc.TLS, qbe *_QBE, _i _Ins, tn uintptr, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	*(*_Ins)(unsafe.Pointer(bp + 12)) = _i
	var i0, i1, v73, v82 uintptr
	var j, k, sh, swap, v4, v53, v55, v6, v60, v62, v67, v69, v78, v80 int32
	var r1, v1, v2, v3, v50, v51, v52, v57, v58, v59, v64, v65, v66, v75, v76, v77 _Ref
	var tmp [7]_Ref
	var v83 bool
	var _ /* kc at bp+36 */ int32
	var _ /* r0 at bp+28 */ _Ref
	var _ /* x at bp+32 */ int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = i0, i1, j, k, r1, sh, swap, tmp, v1, v2, v3, v4, v50, v51, v52, v53, v55, v57, v58, v59, v6, v60, v62, v64, v65, v66, v67, v69, v73, v75, v76, v77, v78, v80, v82, v83
	v1 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		if !(x_isreg(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto) != 0) && !(x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12 + 8))) != 0) && !(x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))) != 0) {
			if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 4 + 0))&0xfffffff8>>3))*192))).Fnuse == uint32(0) {
				x_chuse(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), -int32(1), fn)
				x_chuse(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)), -int32(1), fn)
				return
			}
		}
	}
	i0 = qbe.x_curi
	k = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0)) & 0xc0000000 >> 30)
	switch int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0)) & 0x3fffffff >> 0) {
	case int32(_Ourem):
		goto _8
	case int32(_Oudiv):
		goto _9
	case int32(_Orem):
		goto _10
	case int32(_Odiv):
		goto _11
	case int32(_Oshl):
		goto _12
	case int32(_Oshr):
		goto _13
	case int32(_Osar):
		goto _14
	case int32(_Ouwtof):
		goto _15
	case int32(_Oultof):
		goto _16
	case int32(_Ostoui):
		goto _17
	case int32(_Odtoui):
		goto _18
	case int32(_Onop):
		goto _19
	case int32(_Ostoreb):
		goto _20
	case int32(_Ostoreh):
		goto _21
	case int32(_Ostorew):
		goto _22
	case int32(_Ostorel):
		goto _23
	case int32(_Ostores):
		goto _24
	case int32(_Ostored):
		goto _25
	case int32(_Ocast):
		goto _26
	case int32(_Otruncd):
		goto _27
	case int32(_Oexts):
		goto _28
	case int32(_Osltof):
		goto _29
	case int32(_Oswtof):
		goto _30
	case int32(_Odtosi):
		goto _31
	case int32(_Ostosi):
		goto _32
	case int32(_Oxtest):
		goto _33
	case int32(_Oxor):
		goto _34
	case int32(_Oor):
		goto _35
	case int32(_Oand):
		goto _36
	case int32(_Omul):
		goto _37
	case int32(_Oneg):
		goto _38
	case int32(_Osub):
		goto _39
	case int32(_Oadd):
		goto _40
	case int32(_Ocopy):
		goto _41
	case int32(_Osalloc):
		goto _42
	case int32(_Ocall):
		goto _43
	case int32(_Odbgloc):
		goto _44
	case int32(_Oalloc16):
		goto _45
	case int32(_Oalloc8):
		goto _46
	case int32(_Oalloc4):
		goto _47
	default:
		goto _48
	}
	goto _49
_11:
	;
_10:
	;
_9:
	;
_8:
	;
	if k>>int32(1) == int32(1) {
		goto Emit
	}
	if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) == int32(_Odiv) || int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) == int32(_Oudiv) {
		*(*_Ref)(unsafe.Pointer(bp + 28)) = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
		}
		r1 = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RDX)&0x1fffffff<<3,
		}
	} else {
		*(*_Ref)(unsafe.Pointer(bp + 28)) = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RDX)&0x1fffffff<<3,
		}
		r1 = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
		}
	}
	x_emit(tls, qbe, int32(_Ocopy), k, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
	x_emit(tls, qbe, int32(_Ocopy), k, _Ref{}, r1, _Ref{})
	v50 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v50
	v51 = v50
	v52 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v51
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v52
	v53 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _54
_54:
	if v53 != 0 {
		v55 = -int32(1)
		goto _56
	}
	v55 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _56
_56:
	if v55 == int32(_RCon) {
		/* immediates not allowed for
		 * divisions in x86
		 */
		*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
	} else {
		*(*_Ref)(unsafe.Pointer(bp + 28)) = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	}
	if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))*192))).Fslot != -int32(1) {
		_err(tls, qbe, __ccgo_ts+5351, libc.VaList(bp+48, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))*192, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0)].Fname))
	}
	if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) == int32(_Odiv) || int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) == int32(_Orem) {
		x_emit(tls, qbe, int32(_Oxidiv), k, _Ref{}, *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
		x_emit(tls, qbe, int32(_Osign), k, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RDX)&0x1fffffff<<3,
		}, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
		}, _Ref{})
	} else {
		x_emit(tls, qbe, int32(_Oxdiv), k, _Ref{}, *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
		x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RDX)&0x1fffffff<<3,
		}, _Ref{
			F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
		}, _Ref{})
	}
	x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RAX)&0x1fffffff<<3,
	}, *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), _Ref{})
	s_fixarg(tls, qbe, qbe.x_curi+8, k, qbe.x_curi, fn)
	v57 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v57
	v58 = v57
	v59 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v58
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v59
	v60 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _61
_61:
	if v60 != 0 {
		v62 = -int32(1)
		goto _63
	}
	v62 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _63
_63:
	if v62 == int32(_RCon) {
		x_emit(tls, qbe, int32(_Ocopy), k, *(*_Ref)(unsafe.Pointer(bp + 28)), *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)), _Ref{})
	}
	goto _49
_14:
	;
_13:
	;
_12:
	;
	*(*_Ref)(unsafe.Pointer(bp + 28)) = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	v64 = *(*_Ref)(unsafe.Pointer(bp + 28))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v64
	v65 = v64
	v66 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v65
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v66
	v67 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _68
_68:
	if v67 != 0 {
		v69 = -int32(1)
		goto _70
	}
	v69 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _70
_70:
	if v69 == int32(_RCon) {
		goto Emit
	}
	if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))*192))).Fslot != -int32(1) {
		_err(tls, qbe, __ccgo_ts+5351, libc.VaList(bp+48, (*_Fn)(unsafe.Pointer(fn)).Ftmp+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))*192, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0)].Fname))
	}
	*(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)) = _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RCX)&0x1fffffff<<3,
	}
	x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), _Ref{}, _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RCX)&0x1fffffff<<3,
	}, _Ref{})
	x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(bp + 12)))
	i1 = qbe.x_curi
	x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RCX)&0x1fffffff<<3,
	}, *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
	s_fixarg(tls, qbe, i1+8, x_argcls(tls, qbe, bp+12, 0), i1, fn)
	goto _49
_15:
	;
	*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+5380, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Osltof), k, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
	x_emit(tls, qbe, int32(_Oextuw), int32(_Kl), *(*_Ref)(unsafe.Pointer(bp + 28)), *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), _Ref{})
	s_fixarg(tls, qbe, qbe.x_curi+8, k, qbe.x_curi, fn)
	goto _49
_16:
	;
	/* %mask =l and %arg.0, 1
	 * %isbig =l shr %arg.0, 63
	 * %divided =l shr %arg.0, %isbig
	 * %or =l or %mask, %divided
	 * %float =d sltof %or
	 * %cast =l cast %float
	 * %addend =l shl %isbig, 52
	 * %sum =l add %cast, %addend
	 * %result =d cast %sum
	 */
	*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+5380, k, fn)
	if k == int32(_Ks) {
		*(*int32)(unsafe.Pointer(bp + 36)) = int32(_Kw)
		sh = libc.Int32FromInt32(23)
	} else {
		*(*int32)(unsafe.Pointer(bp + 36)) = int32(_Kl)
		sh = libc.Int32FromInt32(52)
	}
	j = 0
	for {
		if !(j < int32(4)) {
			break
		}
		tmp[j] = x_newtmp(tls, qbe, __ccgo_ts+5380, int32(_Kl), fn)
		goto _71
	_71:
		;
		j++
	}
	for {
		if !(j < int32(7)) {
			break
		}
		tmp[j] = x_newtmp(tls, qbe, __ccgo_ts+5380, *(*int32)(unsafe.Pointer(bp + 36)), fn)
		goto _72
	_72:
		;
		j++
	}
	x_emit(tls, qbe, int32(_Ocast), k, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, tmp[int32(6)], _Ref{})
	x_emit(tls, qbe, int32(_Oadd), *(*int32)(unsafe.Pointer(bp + 36)), tmp[int32(6)], tmp[int32(4)], tmp[int32(5)])
	x_emit(tls, qbe, int32(_Oshl), *(*int32)(unsafe.Pointer(bp + 36)), tmp[int32(5)], tmp[int32(1)], x_getcon(tls, qbe, int64(sh), fn))
	x_emit(tls, qbe, int32(_Ocast), *(*int32)(unsafe.Pointer(bp + 36)), tmp[int32(4)], *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
	x_emit(tls, qbe, int32(_Osltof), k, *(*_Ref)(unsafe.Pointer(bp + 28)), tmp[int32(3)], _Ref{})
	x_emit(tls, qbe, int32(_Oor), int32(_Kl), tmp[int32(3)], tmp[0], tmp[int32(2)])
	x_emit(tls, qbe, int32(_Oshr), int32(_Kl), tmp[int32(2)], *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), tmp[int32(1)])
	v73 = qbe.x_curi
	qbe.x_curi += 16
	s_sel(tls, qbe, *(*_Ins)(unsafe.Pointer(v73)), uintptr(0), fn)
	x_emit(tls, qbe, int32(_Oshr), int32(_Kl), tmp[int32(1)], *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), x_getcon(tls, qbe, int64(63), fn))
	s_fixarg(tls, qbe, qbe.x_curi+8, int32(_Kl), qbe.x_curi, fn)
	x_emit(tls, qbe, int32(_Oand), int32(_Kl), tmp[0], *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), x_getcon(tls, qbe, int64(1), fn))
	s_fixarg(tls, qbe, qbe.x_curi+8, int32(_Kl), qbe.x_curi, fn)
	goto _49
_17:
	;
	libc.SetBitFieldPtr32Uint32(bp+12+0, uint32(_Ostosi), 0, 0x3fffffff)
	*(*int32)(unsafe.Pointer(bp + 36)) = int32(_Ks)
	tmp[int32(4)] = x_getcon(tls, qbe, libc.Int64FromUint32(0xdf000000), fn)
	goto Oftoui
_18:
	;
	libc.SetBitFieldPtr32Uint32(bp+12+0, uint32(_Odtosi), 0, 0x3fffffff)
	*(*int32)(unsafe.Pointer(bp + 36)) = int32(_Kd)
	tmp[int32(4)] = x_getcon(tls, qbe, libc.Int64FromUint64(0xc3e0000000000000), fn)
	goto Oftoui
Oftoui:
	;
	if k == int32(_Kw) {
		*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+5385, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
		libc.SetBitFieldPtr32Uint32(bp+12+0, uint32(_Kl), 30, 0xc0000000)
		(*(*_Ins)(unsafe.Pointer(bp + 12))).Fto = *(*_Ref)(unsafe.Pointer(bp + 28))
		goto Emit
	}
	/* %try0 =l {s,d}tosi %fp
	 * %mask =l sar %try0, 63
	 *
	 *    mask is all ones if the first
	 *    try was oob, all zeroes o.w.
	 *
	 * %fps ={s,d} sub %fp, (1<<63)
	 * %try1 =l {s,d}tosi %fps
	 *
	 * %tmp =l and %mask, %try1
	 * %res =l or %tmp, %try0
	 */
	*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+5385, *(*int32)(unsafe.Pointer(bp + 36)), fn)
	j = 0
	for {
		if !(j < int32(4)) {
			break
		}
		tmp[j] = x_newtmp(tls, qbe, __ccgo_ts+5385, int32(_Kl), fn)
		goto _74
	_74:
		;
		j++
	}
	x_emit(tls, qbe, int32(_Oor), int32(_Kl), (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, tmp[0], tmp[int32(3)])
	x_emit(tls, qbe, int32(_Oand), int32(_Kl), tmp[int32(3)], tmp[int32(2)], tmp[int32(1)])
	x_emit(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0), int32(_Kl), tmp[int32(2)], *(*_Ref)(unsafe.Pointer(bp + 28)), _Ref{})
	x_emit(tls, qbe, int32(_Oadd), *(*int32)(unsafe.Pointer(bp + 36)), *(*_Ref)(unsafe.Pointer(bp + 28)), tmp[int32(4)], *(*_Ref)(unsafe.Pointer(bp + 12 + 8)))
	i1 = qbe.x_curi /* fixarg() can change curi */
	s_fixarg(tls, qbe, i1+8, *(*int32)(unsafe.Pointer(bp + 36)), i1, fn)
	s_fixarg(tls, qbe, i1+8+1*4, *(*int32)(unsafe.Pointer(bp + 36)), i1, fn)
	x_emit(tls, qbe, int32(_Osar), int32(_Kl), tmp[int32(1)], tmp[0], x_getcon(tls, qbe, int64(63), fn))
	x_emit(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0), int32(_Kl), tmp[0], *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), _Ref{})
	s_fixarg(tls, qbe, qbe.x_curi+8, int32(_Kl), qbe.x_curi, fn)
	goto _49
_19:
	;
	goto _49
_25:
	;
_24:
	;
_23:
	;
_22:
	;
_21:
	;
_20:
	;
	v75 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v75
	v76 = v75
	v77 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v76
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v77
	v78 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _79
_79:
	if v78 != 0 {
		v80 = -int32(1)
		goto _81
	}
	v80 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _81
_81:
	if v80 == int32(_RCon) {
		if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) == int32(_Ostored) {
			libc.SetBitFieldPtr32Uint32(bp+12+0, uint32(_Ostorel), 0, 0x3fffffff)
		}
		if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) == int32(_Ostores) {
			libc.SetBitFieldPtr32Uint32(bp+12+0, uint32(_Ostorew), 0, 0x3fffffff)
		}
	}
	s_seladdr(tls, qbe, bp+12+8+1*4, tn, fn)
	goto Emit
	goto case_Oload
case_Oload:
	;
	s_seladdr(tls, qbe, bp+12+8, tn, fn)
	goto Emit
_44:
	;
_43:
	;
_42:
	;
_41:
	;
_40:
	;
_39:
	;
_38:
	;
_37:
	;
_36:
	;
_35:
	;
_34:
	;
_33:
	;
_32:
	;
_31:
	;
_30:
	;
_29:
	;
_28:
	;
_27:
	;
_26:
	;
	goto case_OExt
case_OExt:
	;
	goto Emit
Emit:
	;
	x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(bp + 12)))
	i1 = qbe.x_curi /* fixarg() can change curi */
	s_fixarg(tls, qbe, i1+8, x_argcls(tls, qbe, bp+12, 0), i1, fn)
	s_fixarg(tls, qbe, i1+8+1*4, x_argcls(tls, qbe, bp+12, int32(1)), i1, fn)
	goto _49
_47:
	;
_46:
	;
_45:
	;
	x_salloc(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), fn)
	goto _49
_48:
	;
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0))-uint32(_Oextsb) <= libc.Uint32FromInt32(int32(_Oextuw)-int32(_Oextsb)) {
		goto case_OExt
	}
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) {
		goto case_Oload
	}
	if x_iscmp(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0), bp+36, bp+32) != 0 {
		switch *(*int32)(unsafe.Pointer(bp + 32)) {
		case int32(_NCmpI) + int32(_Cfeq):
			/* zf is set when operands are
			 * unordered, so we may have to
			 * check pf
			 */
			*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kw), fn)
			r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kw), fn)
			x_emit(tls, qbe, int32(_Oand), int32(_Kw), (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, *(*_Ref)(unsafe.Pointer(bp + 28)), r1)
			x_emit(tls, qbe, int32(_Oflagfo), k, r1, _Ref{}, _Ref{})
			(*(*_Ins)(unsafe.Pointer(bp + 12))).Fto = *(*_Ref)(unsafe.Pointer(bp + 28))
		case int32(_NCmpI) + int32(_Cfne):
			*(*_Ref)(unsafe.Pointer(bp + 28)) = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kw), fn)
			r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kw), fn)
			x_emit(tls, qbe, int32(_Oor), int32(_Kw), (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, *(*_Ref)(unsafe.Pointer(bp + 28)), r1)
			x_emit(tls, qbe, int32(_Oflagfuo), k, r1, _Ref{}, _Ref{})
			(*(*_Ins)(unsafe.Pointer(bp + 12))).Fto = *(*_Ref)(unsafe.Pointer(bp + 28))
			break
		}
		swap = s_cmpswap(tls, qbe, bp+12+8, *(*int32)(unsafe.Pointer(bp + 32)))
		if swap != 0 {
			*(*int32)(unsafe.Pointer(bp + 32)) = x_cmpop(tls, qbe, *(*int32)(unsafe.Pointer(bp + 32)))
		}
		x_emit(tls, qbe, int32(_Oflag)+*(*int32)(unsafe.Pointer(bp + 32)), k, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, _Ref{}, _Ref{})
		s_selcmp(tls, qbe, bp+12+8, *(*int32)(unsafe.Pointer(bp + 36)), swap, fn)
		goto _49
	}
	_die_(tls, qbe, __ccgo_ts+5312, __ccgo_ts+5390, libc.VaList(bp+48, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0)].Fname))
_49:
	;
	for {
		if v83 = i0 > qbe.x_curi; v83 {
			i0 -= 16
			v82 = i0
		}
		if !(v83 && v82 != 0) {
			break
		}
	}
}

func s_flagi(tls *libc.TLS, qbe *_QBE, i0 uintptr, i uintptr) (r uintptr) {
	for i > i0 {
		i -= 16
		if qbe.x_amd64_op[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fzflag != 0 {
			return i
		}
		if qbe.x_amd64_op[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Flflag != 0 {
			continue
		}
		return uintptr(0)
	}
	return uintptr(0)
}

func s_seljmp(tls *libc.TLS, qbe *_QBE, b1 uintptr, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var fi, t uintptr
	var swap, v11, v16, v18, v25, v27, v3, v9 int32
	var v1, v13, v14, v15, v2, v22, v23, v24, v6, v7, v8 _Ref
	var v20, v21, v5 bool
	var _ /* c at bp+24 */ int32
	var _ /* k at bp+28 */ int32
	var _ /* r at bp+20 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = fi, swap, t, v1, v11, v13, v14, v15, v16, v18, v2, v20, v21, v22, v23, v24, v25, v27, v3, v5, v6, v7, v8, v9
	if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jret0) || int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jjmp) || int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jhlt) {
		return
	}
	*(*_Ref)(unsafe.Pointer(bp + 20)) = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
	t = (*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 20 + 0))&0xfffffff8>>3))*192
	(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = _Ref{}
	if (*_Blk)(unsafe.Pointer(b1)).Fs1 == (*_Blk)(unsafe.Pointer(b1)).Fs2 {
		x_chuse(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 20)), -int32(1), fn)
		(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(_Jjmp)
		(*_Blk)(unsafe.Pointer(b1)).Fs2 = uintptr(0)
		return
	}
	fi = s_flagi(tls, qbe, (*_Blk)(unsafe.Pointer(b1)).Fins, (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16)
	if v5 = !(fi != 0); !v5 {
		v1 = (*_Ins)(unsafe.Pointer(fi)).Fto
		v2 = *(*_Ref)(unsafe.Pointer(bp + 20))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
		*(*_Ref)(unsafe.Pointer(bp + 12)) = v2
		v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))
		goto _4
	_4:
	}
	if v5 || !(v3 != 0) {
		*(*[2]_Ref)(unsafe.Pointer(bp)) = [2]_Ref{
			0: *(*_Ref)(unsafe.Pointer(bp + 20)),
			1: {
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
			},
		}
		s_selcmp(tls, qbe, bp, int32(_Kw), 0, fn)
		(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(int32(_Jjf) + int32(_Cine))
	} else {
		if x_iscmp(tls, qbe, int32(*(*uint32)(unsafe.Pointer(fi + 0))&0x3fffffff>>0), bp+28, bp+24) != 0 && *(*int32)(unsafe.Pointer(bp + 24)) != int32(_NCmpI)+int32(_Cfeq) && *(*int32)(unsafe.Pointer(bp + 24)) != int32(_NCmpI)+int32(_Cfne) {
			swap = s_cmpswap(tls, qbe, fi+8, *(*int32)(unsafe.Pointer(bp + 24)))
			if swap != 0 {
				*(*int32)(unsafe.Pointer(bp + 24)) = x_cmpop(tls, qbe, *(*int32)(unsafe.Pointer(bp + 24)))
			}
			if (*_Tmp)(unsafe.Pointer(t)).Fnuse == uint32(1) {
				s_selcmp(tls, qbe, fi+8, *(*int32)(unsafe.Pointer(bp + 28)), swap, fn)
				*(*_Ins)(unsafe.Pointer(fi)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
			}
			(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(int32(_Jjf) + *(*int32)(unsafe.Pointer(bp + 24)))
		} else {
			if v21 = int32(*(*uint32)(unsafe.Pointer(fi + 0))&0x3fffffff>>0) == int32(_Oand) && (*_Tmp)(unsafe.Pointer(t)).Fnuse == uint32(1); v21 {
				v6 = *(*_Ref)(unsafe.Pointer(fi + 8))
				*(*_Ref)(unsafe.Pointer(bp + 16)) = v6
				v7 = v6
				v8 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v7
				*(*_Ref)(unsafe.Pointer(bp + 12)) = v8
				v9 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))
				goto _10
			_10:
				if v9 != 0 {
					v11 = -int32(1)
					goto _12
				}
				v11 = int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0)) & 0x7 >> 0)
				goto _12
			_12:
				;
				if v20 = v11 == int32(_RTmp); !v20 {
					v13 = *(*_Ref)(unsafe.Pointer(fi + 8 + 1*4))
					*(*_Ref)(unsafe.Pointer(bp + 16)) = v13
					v14 = v13
					v15 = _Ref{}
					*(*_Ref)(unsafe.Pointer(bp + 8)) = v14
					*(*_Ref)(unsafe.Pointer(bp + 12)) = v15
					v16 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))
					goto _17
				_17:
					if v16 != 0 {
						v18 = -int32(1)
						goto _19
					}
					v18 = int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0)) & 0x7 >> 0)
					goto _19
				_19:
				}
			}
			if v21 && (v20 || v18 == int32(_RTmp)) {
				libc.SetBitFieldPtr32Uint32(fi+0, uint32(_Oxtest), 0, 0x3fffffff)
				(*_Ins)(unsafe.Pointer(fi)).Fto = _Ref{}
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(int32(_Jjf) + int32(_Cine))
				v22 = *(*_Ref)(unsafe.Pointer(fi + 8 + 1*4))
				*(*_Ref)(unsafe.Pointer(bp + 16)) = v22
				v23 = v22
				v24 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp + 8)) = v23
				*(*_Ref)(unsafe.Pointer(bp + 12)) = v24
				v25 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))
				goto _26
			_26:
				if v25 != 0 {
					v27 = -int32(1)
					goto _28
				}
				v27 = int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0)) & 0x7 >> 0)
				goto _28
			_28:
				if v27 == int32(_RCon) {
					*(*_Ref)(unsafe.Pointer(bp + 20)) = *(*_Ref)(unsafe.Pointer(fi + 8 + 1*4))
					*(*_Ref)(unsafe.Pointer(fi + 8 + 1*4)) = *(*_Ref)(unsafe.Pointer(fi + 8))
					*(*_Ref)(unsafe.Pointer(fi + 8)) = *(*_Ref)(unsafe.Pointer(bp + 20))
				}
			} else {
				/* since flags are not tracked in liveness,
				 * the result of the flag-setting instruction
				 * has to be marked as live
				 */
				if (*_Tmp)(unsafe.Pointer(t)).Fnuse == uint32(1) {
					x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), _Ref{}, *(*_Ref)(unsafe.Pointer(bp + 20)), _Ref{})
				}
				(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(int32(_Jjf) + int32(_Cine))
			}
		}
	}
}

const _Pob = 0

const _Pbis = 1

const _Pois = 2

const _Pobis = 3

const _Pbi1 = 4

const _Pobi1 = 5

/* mgen generated code
 *
 * (with-vars (o b i s)
 *   (patterns
 *     (ob   (add (con o) (tmp b)))
 *     (bis  (add (tmp b) (mul (tmp i) (con s 1 2 4 8))))
 *     (ois  (add (con o) (mul (tmp i) (con s 1 2 4 8))))
 *     (obis (add (con o) (tmp b) (mul (tmp i) (con s 1 2 4 8))))
 *     (bi1  (add (tmp b) (tmp i)))
 *     (obi1 (add (con o) (tmp b) (tmp i)))
 * ))
 */

func s_opn(tls *libc.TLS, qbe *_QBE, op int32, l int32, r int32) (r1 int32) {
	var t int32
	_ = t
	if l < r {
		t = l
		l = r
		r = t
	}
	switch op {
	case int32(_Omul):
		if int32(2) <= l {
			if r == 0 {
				return int32(3)
			}
		}
		return int32(2)
	case int32(_Oadd):
		return libc.Int32FromUint8(qbe.s_Oaddtbl[(l+l*l)/int32(2)+r])
	default:
		return int32(2)
	}
	return r1
}

func s_refn(tls *libc.TLS, qbe *_QBE, _r _Ref, tn uintptr, con uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var n _int64_t
	var v1, v2, v3 _Ref
	var v4, v6 int32
	_, _, _, _, _, _ = n, v1, v2, v3, v4, v6
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	switch v6 {
	case int32(_RTmp):
		if !((*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*12))).Fn != 0) {
			(*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*12))).Fn = uint8(2)
		}
		return libc.Int32FromUint8((*(*_Num)(unsafe.Pointer(tn + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*12))).Fn)
	case int32(_RCon):
		if (*(*_Con)(unsafe.Pointer(con + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32))).Ftype1 != int32(_CBits) {
			return int32(1)
		}
		n = *(*_int64_t)(unsafe.Pointer(con + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32 + 16))
		if n == int64(8) || n == int64(4) || n == int64(2) || n == int64(1) {
			return 0
		}
		return int32(1)
	default:
		return -libc.Int32FromInt32(m___INT_MAX__) - libc.Int32FromInt32(1)
	}
	return r
}

/* end of generated code */

func s_anumber(tls *libc.TLS, qbe *_QBE, tn uintptr, b1 uintptr, con uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i, n uintptr
	var v2, v3, v4 _Ref
	var v5, v7 int32
	_, _, _, _, _, _, _ = i, n, v2, v3, v4, v5, v7
	i = (*_Blk)(unsafe.Pointer(b1)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
			break
		}
		v2 = (*_Ins)(unsafe.Pointer(i)).Fto
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v2
		v3 = v2
		v4 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v3
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v4
		v5 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _6
	_6:
		if v5 != 0 {
			v7 = -int32(1)
			goto _8
		}
		v7 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _8
	_8:
		if v7 != int32(_RTmp) {
			goto _1
		}
		n = tn + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*12
		(*_Num)(unsafe.Pointer(n)).Fl = *(*_Ref)(unsafe.Pointer(i + 8))
		(*_Num)(unsafe.Pointer(n)).Fr = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
		(*_Num)(unsafe.Pointer(n)).Fnl = libc.Uint8FromInt32(s_refn(tls, qbe, (*_Num)(unsafe.Pointer(n)).Fl, tn, con))
		(*_Num)(unsafe.Pointer(n)).Fnr = libc.Uint8FromInt32(s_refn(tls, qbe, (*_Num)(unsafe.Pointer(n)).Fr, tn, con))
		(*_Num)(unsafe.Pointer(n)).Fn = libc.Uint8FromInt32(s_opn(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0), libc.Int32FromUint8((*_Num)(unsafe.Pointer(n)).Fnl), libc.Int32FromUint8((*_Num)(unsafe.Pointer(n)).Fnr)))
		goto _1
	_1:
		;
		i += 16
	}
}

func s_adisp(tls *libc.TLS, qbe *_QBE, c uintptr, tn uintptr, r _Ref, fn uintptr, s int32) (r1 _Ref) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var n, v3 int32
	var v1, v2 _Ref
	var _ /* v at bp+8 */ [2]_Ref
	_, _, _, _ = n, v1, v2, v3
	for {
		v1 = r
		v2 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v1
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
		v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _4
	_4:
		if !!(v3 != 0) {
			break
		}
		n = s_refn(tls, qbe, r, tn, (*_Fn)(unsafe.Pointer(fn)).Fcon)
		if !(qbe.s_match[n]&(libc.Uint64FromInt32(1)<<int32(_Pob)) != 0) {
			break
		}
		x_runmatch(tls, qbe, qbe.s_matcher[int32(_Pob)], tn, r, bp+8)
		x_addcon(tls, qbe, c, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3))*32, s)
		r = (*(*[2]_Ref)(unsafe.Pointer(bp + 8)))[int32(1)]
	}
	return r
}

func s_amatch(tls *libc.TLS, qbe *_QBE, a1 uintptr, tn uintptr, r1 _Ref, fn uintptr) (r int32) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var c, p uintptr
	var n, s, v11, v15, v20, v22, v26, v4, v6 int32
	var v1, v10, v13, v14, v17, v18, v19, v2, v24, v25, v3, v9 _Ref
	var _ /* co at bp+48 */ _Con
	var _ /* rb at bp+16 */ _Ref
	var _ /* ri at bp+20 */ _Ref
	var _ /* ro at bp+12 */ _Ref
	var _ /* rs at bp+24 */ _Ref
	var _ /* v at bp+28 */ [4]_Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, n, p, s, v1, v10, v11, v13, v14, v15, v17, v18, v19, v2, v20, v22, v24, v25, v26, v3, v4, v6, v9
	v1 = r1
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 != int32(_RTmp) {
		return 0
	}
	n = s_refn(tls, qbe, r1, tn, (*_Fn)(unsafe.Pointer(fn)).Fcon)
	libc.X__builtin___memset_chk(tls, bp+28, 0, uint64(16), ^___predefined_size_t(0))
	p = uintptr(unsafe.Pointer(&qbe.s_pat))
	for {
		if !(*(*int32)(unsafe.Pointer(p)) >= 0) {
			break
		}
		if qbe.s_match[n]&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(p))) != 0 {
			x_runmatch(tls, qbe, qbe.s_matcher[*(*int32)(unsafe.Pointer(p))], tn, r1, bp+28)
			break
		}
		goto _8
	_8:
		;
		p += 4
	}
	if *(*int32)(unsafe.Pointer(p)) < 0 {
		(*(*[4]_Ref)(unsafe.Pointer(bp + 28)))[int32(1)] = r1
	}
	libc.X__builtin___memset_chk(tls, bp+48, 0, uint64(32), ^___predefined_size_t(0))
	*(*_Ref)(unsafe.Pointer(bp + 12)) = (*(*[4]_Ref)(unsafe.Pointer(bp + 28)))[0]
	*(*_Ref)(unsafe.Pointer(bp + 16)) = s_adisp(tls, qbe, bp+48, tn, (*(*[4]_Ref)(unsafe.Pointer(bp + 28)))[int32(1)], fn, int32(1))
	*(*_Ref)(unsafe.Pointer(bp + 20)) = (*(*[4]_Ref)(unsafe.Pointer(bp + 28)))[int32(2)]
	*(*_Ref)(unsafe.Pointer(bp + 24)) = (*(*[4]_Ref)(unsafe.Pointer(bp + 28)))[int32(3)]
	s = int32(1)
	if *(*int32)(unsafe.Pointer(p)) < 0 && (*(*_Con)(unsafe.Pointer(bp + 48))).Ftype1 != int32(_CUndef) {
		if s_amatch(tls, qbe, a1, tn, *(*_Ref)(unsafe.Pointer(bp + 16)), fn) != 0 {
			return x_addcon(tls, qbe, a1, bp+48, int32(1))
		}
	}
	v9 = *(*_Ref)(unsafe.Pointer(bp + 12))
	v10 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v9
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
	v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _12
_12:
	if !(v11 != 0) {
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
		if !(x_addcon(tls, qbe, bp+48, c, int32(1)) != 0) {
			return 0
		}
	}
	v13 = *(*_Ref)(unsafe.Pointer(bp + 24))
	v14 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v13
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v14
	v15 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _16
_16:
	if !(v15 != 0) {
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0xfffffff8>>3))*32
		s = int32(*(*_int64_t)(unsafe.Pointer(c + 16)))
	}
	*(*_Ref)(unsafe.Pointer(bp + 20)) = s_adisp(tls, qbe, bp+48, tn, *(*_Ref)(unsafe.Pointer(bp + 20)), fn, s)
	*(*_Addr)(unsafe.Pointer(a1)) = _Addr{
		Foffset: *(*_Con)(unsafe.Pointer(bp + 48)),
		Fbase:   *(*_Ref)(unsafe.Pointer(bp + 16)),
		Findex:  *(*_Ref)(unsafe.Pointer(bp + 20)),
		Fscale:  s,
	}
	v17 = *(*_Ref)(unsafe.Pointer(bp + 20))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v17
	v18 = v17
	v19 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v18
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v19
	v20 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _21
_21:
	if v20 != 0 {
		v22 = -int32(1)
		goto _23
	}
	v22 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _23
_23:
	if v22 == int32(_RTmp) {
		if (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 20 + 0))&0xfffffff8>>3))*192))).Fslot != -int32(1) {
			if (*_Addr)(unsafe.Pointer(a1)).Fscale != int32(1) || (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*192))).Fslot != -int32(1) {
				return 0
			}
			(*_Addr)(unsafe.Pointer(a1)).Fbase = *(*_Ref)(unsafe.Pointer(bp + 20))
			(*_Addr)(unsafe.Pointer(a1)).Findex = *(*_Ref)(unsafe.Pointer(bp + 16))
		}
	}
	v24 = (*_Addr)(unsafe.Pointer(a1)).Fbase
	v25 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v24
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v25
	v26 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _27
_27:
	if !(v26 != 0) {
		s = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(a1 + 32 + 0))&0xfffffff8>>3))*192))).Fslot
		if s != -int32(1) {
			(*_Addr)(unsafe.Pointer(a1)).Fbase = _Ref{
				F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
			}
		}
	}
	return int32(1)
}

// C documentation
//
//	/* instruction selection
//	 * requires use counts (as given by parsing)
//	 */
func x_amd64_isel(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a1 _uint
	var al, n, v6, v8 int32
	var b1, i, num, p, sb, v16, p10 uintptr
	var sz _int64_t
	var v3, v4, v5 _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a1, al, b1, i, n, num, p, sb, sz, v16, v3, v4, v5, v6, v8, p10
	/* assign slots to fast allocs */
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	/* specific to NAlign == 3 */ /* or change n=4 and sz /= 4 below */
	al = int32(_Oalloc)
	n = libc.Int32FromInt32(4)
	for {
		if !(al <= int32(_Oalloc1)) {
			break
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == al {
				v3 = *(*_Ref)(unsafe.Pointer(i + 8))
				*(*_Ref)(unsafe.Pointer(bp + 32)) = v3
				v4 = v3
				v5 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp + 24)) = v4
				*(*_Ref)(unsafe.Pointer(bp + 28)) = v5
				v6 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))
				goto _7
			_7:
				if v6 != 0 {
					v8 = -int32(1)
					goto _9
				}
				v8 = int32(*(*uint32)(unsafe.Pointer(bp + 32 + 0)) & 0x7 >> 0)
				goto _9
			_9:
				if v8 != int32(_RCon) {
					break
				}
				sz = *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32 + 16))
				if sz < 0 || sz >= int64(libc.Int32FromInt32(m___INT_MAX__)-libc.Int32FromInt32(15)) {
					_err(tls, qbe, __ccgo_ts+194, libc.VaList(bp+48, sz))
				}
				sz = (sz + int64(n) - int64(1)) & int64(-n)
				sz /= int64(4)
				if sz > int64(int32(m___INT_MAX__)-(*_Fn)(unsafe.Pointer(fn)).Fslot) {
					_die_(tls, qbe, __ccgo_ts+5312, __ccgo_ts+5413, 0)
				}
				(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192))).Fslot = (*_Fn)(unsafe.Pointer(fn)).Fslot
				p10 = fn + 72
				*(*int32)(unsafe.Pointer(p10)) = int32(int64(*(*int32)(unsafe.Pointer(p10))) + sz)
				(*_Fn)(unsafe.Pointer(fn)).Fsalign = int32(2) + al - int32(_Oalloc)
				*(*_Ins)(unsafe.Pointer(i)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
			}
			goto _2
		_2:
			;
			i += 16
		}
		goto _1
	_1:
		;
		al++
		n *= int32(2)
	}
	/* process basic blocks */
	n = (*_Fn)(unsafe.Pointer(fn)).Fntmp
	num = x_emalloc(tls, qbe, libc.Uint64FromInt32(n)*uint64(12))
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		*(*[3]uintptr)(unsafe.Pointer(bp)) = [3]uintptr{
			0: (*_Blk)(unsafe.Pointer(b1)).Fs1,
			1: (*_Blk)(unsafe.Pointer(b1)).Fs2,
		}
		sb = bp
		for {
			if !(*(*uintptr)(unsafe.Pointer(sb)) != 0) {
				break
			}
			p = (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(sb)))).Fphi
			for {
				if !(p != 0) {
					break
				}
				a1 = uint32(0)
				for {
					if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a1)*8)) != b1) {
						break
					}
					goto _14
				_14:
					;
					a1++
				}
				s_fixarg(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(a1)*4, (*_Phi)(unsafe.Pointer(p)).Fcls, uintptr(0), fn)
				goto _13
			_13:
				;
				p = (*_Phi)(unsafe.Pointer(p)).Flink
			}
			goto _12
		_12:
			;
			sb += 8
		}
		libc.X__builtin___memset_chk(tls, num, 0, libc.Uint64FromInt32(n)*uint64(12), ^___predefined_size_t(0))
		s_anumber(tls, qbe, num, b1, (*_Fn)(unsafe.Pointer(fn)).Fcon)
		s_seljmp(tls, qbe, b1, fn)
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b1)).Fins) {
				break
			}
			i -= 16
			v16 = i
			s_sel(tls, qbe, *(*_Ins)(unsafe.Pointer(v16)), num, fn)
			goto _15
		_15:
		}
		(*_Blk)(unsafe.Pointer(b1)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b1+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b1)).Fnins))
		goto _11
	_11:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	free(tls, qbe, num)
	if qbe.x_debug[int32('I')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+5429, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

type _E = struct {
	Ff     uintptr
	Ffn    uintptr
	Ffp    int32
	Ffsz   _uint64_t
	Fnclob int32
}

const _SLong = 0

const _SWord = 1

const _SShort = 2

const _SByte = 3

const _Ki = -1

const /* matches Kw and Kl */
_Ka = -2

func s_slot2(tls *libc.TLS, qbe *_QBE, r1 _Ref, e uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var s, v1 int32
	_, _ = s, v1
	*(*_Ref)(unsafe.Pointer(bp)) = r1
	v1 = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
	goto _2
_2:
	s = v1
	/* specific to NAlign == 3 */
	if s < 0 {
		if (*_E)(unsafe.Pointer(e)).Ffp == int32(_RSP) {
			return libc.Int32FromUint64(libc.Uint64FromInt32(int32(4)*-s-int32(8)) + (*_E)(unsafe.Pointer(e)).Ffsz + libc.Uint64FromInt32((*_E)(unsafe.Pointer(e)).Fnclob*int32(8)))
		} else {
			return int32(4) * -s
		}
	} else {
		if (*_E)(unsafe.Pointer(e)).Ffp == int32(_RSP) {
			return int32(4)*s + (*_E)(unsafe.Pointer(e)).Fnclob*int32(8)
		} else {
			if (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fvararg != 0 {
				return -int32(176) + -int32(4)*((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fslot-s)
			} else {
				return -int32(4) * ((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fslot - s)
			}
		}
	}
	return r
}

func s_emitcon(tls *libc.TLS, qbe *_QBE, con uintptr, e uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var l, p, v1 uintptr
	_, _, _ = l, p, v1
	switch (*_Con)(unsafe.Pointer(con)).Ftype1 {
	case int32(_CAddr):
		l = x_str(tls, qbe, (*_Con)(unsafe.Pointer(con)).Fsym.Fid)
		if int32(*(*int8)(unsafe.Pointer(l))) == int32('"') {
			v1 = __ccgo_ts + 4511
		} else {
			v1 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
		}
		p = v1
		if (*_Con)(unsafe.Pointer(con)).Fsym.Ftype1 == int32(_SThr) {
			if qbe.x_T.Fapple != 0 {
				fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6744, libc.VaList(bp+8, p, l))
			} else {
				fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6754, libc.VaList(bp+8, p, l))
			}
		} else {
			fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6770, libc.VaList(bp+8, p, l))
		}
		if *(*_int64_t)(unsafe.Pointer(con + 16)) != 0 {
			fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6775, libc.VaList(bp+8, *(*_int64_t)(unsafe.Pointer(con + 16))))
		}
	case int32(_CBits):
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6781, libc.VaList(bp+8, *(*_int64_t)(unsafe.Pointer(con + 16))))
	default:
		_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+3532, 0)
	}
}

func s_regtoa(tls *libc.TLS, qbe *_QBE, reg int32, sz int32) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	if reg >= int32(_XMM0) {
		libc.X__builtin___sprintf_chk(tls, uintptr(unsafe.Pointer(&qbe.s_buf)), 0, ^___predefined_size_t(0), __ccgo_ts+6799, libc.VaList(bp+8, reg-int32(_XMM0)))
		return uintptr(unsafe.Pointer(&qbe.s_buf))
	} else {
		return *(*uintptr)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.s_rname)) + uintptr(reg)*32 + uintptr(sz)*8))
	}
	return r
}

func s_getarg(tls *libc.TLS, qbe *_QBE, c int8, i uintptr) (r _Ref) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	switch int32(c) {
	case int32('0'):
		return *(*_Ref)(unsafe.Pointer(i + 8))
	case int32('1'):
		return *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
	case int32('='):
		return (*_Ins)(unsafe.Pointer(i)).Fto
	default:
		_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+6805, libc.VaList(bp+8, int32(c)))
	}
	return r
}

func s_emitcopy(tls *libc.TLS, qbe *_QBE, r1 _Ref, r2 _Ref, k int32, e uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* icp at bp+0 */ _Ins
	libc.SetBitFieldPtr32Uint32(bp+0, uint32(_Ocopy), 0, 0x3fffffff)
	*(*_Ref)(unsafe.Pointer(bp + 8)) = r2
	(*(*_Ins)(unsafe.Pointer(bp))).Fto = r1
	libc.SetBitFieldPtr32Uint32(bp+0, libc.Uint32FromInt32(k), 30, 0xc0000000)
	s_emitins(tls, qbe, *(*_Ins)(unsafe.Pointer(bp)), e)
}

func s_emitf(tls *libc.TLS, qbe *_QBE, s uintptr, i uintptr, e uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var c, v5, v7 int8
	var m, v24, v53, v6, v8 uintptr
	var sz, v23, v28, v3, v30, v41, v43, v47, v51, v57, v59 int32
	var v1, v2, v25, v26, v27, v38, v39, v40, v45, v46, v49, v50, v54, v55, v56 _Ref
	var _ /* off at bp+16 */ _Con
	var _ /* ref at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, m, sz, v1, v2, v23, v24, v25, v26, v27, v28, v3, v30, v38, v39, v40, v41, v43, v45, v46, v47, v49, v5, v50, v51, v53, v54, v55, v56, v57, v59, v6, v7, v8
	switch int32(*(*int8)(unsafe.Pointer(s))) {
	case int32('+'):
		v1 = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
		v2 = (*_Ins)(unsafe.Pointer(i)).Fto
		*(*_Ref)(unsafe.Pointer(bp)) = v1
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
		v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _4
	_4:
		if v3 != 0 {
			*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8))
			*(*_Ref)(unsafe.Pointer(i + 8)) = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
			*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = *(*_Ref)(unsafe.Pointer(bp + 12))
		}
		/* fall through */
		fallthrough
	case int32('-'):
		s_emitcopy(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, *(*_Ref)(unsafe.Pointer(i + 8)), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), e)
		s++
		break
	}
	fputc(tls, qbe, int32('\t'), (*_E)(unsafe.Pointer(e)).Ff)
	goto Next
Next:
	;
	for {
		v6 = s
		s++
		v5 = *(*int8)(unsafe.Pointer(v6))
		c = v5
		if !(int32(v5) != int32('%')) {
			break
		}
		if !(c != 0) {
			fputc(tls, qbe, int32('\n'), (*_E)(unsafe.Pointer(e)).Ff)
			return
		} else {
			fputc(tls, qbe, int32(c), (*_E)(unsafe.Pointer(e)).Ff)
		}
	}
	v8 = s
	s++
	v7 = *(*int8)(unsafe.Pointer(v8))
	c = v7
	switch int32(v7) {
	case int32('%'):
		goto _9
	case int32('k'):
		goto _10
	case int32('='):
		goto _11
	case int32('1'):
		goto _12
	case int32('0'):
		goto _13
	case int32('S'):
		goto _14
	case int32('D'):
		goto _15
	case int32('L'):
		goto _16
	case int32('W'):
		goto _17
	case int32('H'):
		goto _18
	case int32('B'):
		goto _19
	case int32('M'):
		goto _20
	default:
		goto _21
	}
	goto _22
_9:
	;
	fputc(tls, qbe, int32('%'), (*_E)(unsafe.Pointer(e)).Ff)
	goto _22
_10:
	;
	fputs(tls, qbe, uintptr(unsafe.Pointer(&qbe.s_clstoa))+uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30))*3, (*_E)(unsafe.Pointer(e)).Ff)
	goto _22
_13:
	;
_12:
	;
_11:
	;
	if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)&int32(1) != 0 {
		v23 = int32(_SLong)
	} else {
		v23 = int32(_SWord)
	}
	sz = v23
	s--
	goto Ref
_15:
	;
_14:
	;
	sz = int32(_SLong) /* does not matter for floats */
	goto Ref
Ref:
	;
	v24 = s
	s++
	c = *(*int8)(unsafe.Pointer(v24))
	*(*_Ref)(unsafe.Pointer(bp + 12)) = s_getarg(tls, qbe, c, i)
	v25 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v25
	v26 = v25
	v27 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v26
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v27
	v28 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _29
_29:
	if v28 != 0 {
		v30 = -int32(1)
		goto _31
	}
	v30 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _31
_31:
	switch v30 {
	case int32(_RTmp):
		goto _32
	case int32(_RSlot):
		goto _33
	case int32(_RMem):
		goto _34
	case int32(_RCon):
		goto _35
	default:
		goto _36
	}
	goto _37
_32:
	;
	fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+3056, libc.VaList(bp+56, s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3), sz)))
	goto _37
_33:
	;
	fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6827, libc.VaList(bp+56, s_slot2(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), e), s_regtoa(tls, qbe, (*_E)(unsafe.Pointer(e)).Ffp, int32(_SLong))))
	goto _37
_34:
	;
	goto Mem
Mem:
	;
	m = (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fmem + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*48
	v38 = (*_Mem)(unsafe.Pointer(m)).Fbase
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v38
	v39 = v38
	v40 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v39
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v40
	v41 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _42
_42:
	if v41 != 0 {
		v43 = -int32(1)
		goto _44
	}
	v43 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _44
_44:
	if v43 == int32(_RSlot) {
		(*(*_Con)(unsafe.Pointer(bp + 16))).Ftype1 = int32(_CBits)
		*(*_int64_t)(unsafe.Pointer(bp + 16 + 16)) = int64(s_slot2(tls, qbe, (*_Mem)(unsafe.Pointer(m)).Fbase, e))
		x_addcon(tls, qbe, m, bp+16, int32(1))
		(*_Mem)(unsafe.Pointer(m)).Fbase = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32((*_E)(unsafe.Pointer(e)).Ffp)&0x1fffffff<<3,
		}
	}
	if (*_Mem)(unsafe.Pointer(m)).Foffset.Ftype1 != int32(_CUndef) {
		s_emitcon(tls, qbe, m, e)
	}
	fputc(tls, qbe, int32('('), (*_E)(unsafe.Pointer(e)).Ff)
	v45 = (*_Mem)(unsafe.Pointer(m)).Fbase
	v46 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v45
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v46
	v47 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _48
_48:
	if !(v47 != 0) {
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+3056, libc.VaList(bp+56, s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(m + 32 + 0))&0xfffffff8>>3), int32(_SLong))))
	} else {
		if (*_Mem)(unsafe.Pointer(m)).Foffset.Ftype1 == int32(_CAddr) {
			fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6836, 0)
		}
	}
	v49 = (*_Mem)(unsafe.Pointer(m)).Findex
	v50 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v49
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v50
	v51 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _52
_52:
	if !(v51 != 0) {
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6842, libc.VaList(bp+56, s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(m + 36 + 0))&0xfffffff8>>3), int32(_SLong)), (*_Mem)(unsafe.Pointer(m)).Fscale))
	}
	fputc(tls, qbe, int32(')'), (*_E)(unsafe.Pointer(e)).Ff)
	goto _37
_35:
	;
	fputc(tls, qbe, int32('$'), (*_E)(unsafe.Pointer(e)).Ff)
	s_emitcon(tls, qbe, (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32, e)
	goto _37
_36:
	;
	_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+3532, 0)
_37:
	;
	goto _22
_16:
	;
	sz = int32(_SLong)
	goto Ref
_17:
	;
	sz = int32(_SWord)
	goto Ref
_18:
	;
	sz = int32(_SShort)
	goto Ref
_19:
	;
	sz = int32(_SByte)
	goto Ref
_20:
	;
	v53 = s
	s++
	c = *(*int8)(unsafe.Pointer(v53))
	*(*_Ref)(unsafe.Pointer(bp + 12)) = s_getarg(tls, qbe, c, i)
	v54 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v54
	v55 = v54
	v56 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v55
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v56
	v57 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _58
_58:
	if v57 != 0 {
		v59 = -int32(1)
		goto _60
	}
	v59 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _60
_60:
	switch v59 {
	case int32(_RMem):
		goto Mem
	case int32(_RSlot):
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6827, libc.VaList(bp+56, s_slot2(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), e), s_regtoa(tls, qbe, (*_E)(unsafe.Pointer(e)).Ffp, int32(_SLong))))
	case int32(_RCon):
		*(*_Con)(unsafe.Pointer(bp + 16)) = *(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32))
		s_emitcon(tls, qbe, bp+16, e)
		if (*(*_Con)(unsafe.Pointer(bp + 16))).Ftype1 == int32(_CAddr) {
			if (*(*_Con)(unsafe.Pointer(bp + 16))).Fsym.Ftype1 != int32(_SThr) || qbe.x_T.Fapple != 0 {
				fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6853, 0)
			}
		}
	case int32(_RTmp):
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6861, libc.VaList(bp+56, s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3), int32(_SLong))))
	default:
		_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+3532, 0)
	}
	goto _22
_21:
	;
	_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+6868, libc.VaList(bp+56, int32(c)))
_22:
	;
	goto Next
}

func s_emitins(tls *libc.TLS, qbe *_QBE, _i _Ins, e uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	*(*_Ins)(unsafe.Pointer(bp + 12)) = _i
	var con, sym, v98 uintptr
	var ineg _Ins
	var o, t0, v102, v104, v108, v18, v20, v25, v27, v33, v35, v40, v44, v49, v53, v57, v61, v66, v71, v73, v78, v80, v85, v87, v89, v93, v95 int32
	var r1, v100, v101, v106, v107, v15, v16, v17, v22, v23, v24, v30, v31, v32, v38, v39, v42, v43, v47, v48, v51, v52, v55, v56, v59, v60, v64, v65, v68, v69, v70, v75, v76, v77, v82, v83, v84, v90, v91, v92, v99 _Ref
	var val _int64_t
	var v29, v37, v46, v63, v97 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = con, ineg, o, r1, sym, t0, val, v100, v101, v102, v104, v106, v107, v108, v15, v16, v17, v18, v20, v22, v23, v24, v25, v27, v29, v30, v31, v32, v33, v35, v37, v38, v39, v40, v42, v43, v44, v46, v47, v48, v49, v51, v52, v53, v55, v56, v57, v59, v60, v61, v63, v64, v65, v66, v68, v69, v70, v71, v73, v75, v76, v77, v78, v80, v82, v83, v84, v85, v87, v89, v90, v91, v92, v93, v95, v97, v98, v99
	switch int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0)) & 0x3fffffff >> 0) {
	default:
		goto _1
	case int32(_Onop):
		goto _2
	case int32(_Omul):
		goto _3
	case int32(_Osub):
		goto _4
	case int32(_Oneg):
		goto _5
	case int32(_Odiv):
		goto _6
	case int32(_Ocopy):
		goto _7
	case int32(_Oaddr):
		goto _8
	case int32(_Ocall):
		goto _9
	case int32(_Osalloc):
		goto _10
	case int32(_Oswap):
		goto _11
	case int32(_Odbgloc):
		goto _12
	}
	goto _13
_1:
	;
	goto Table
Table:
	;
	/* most instructions are just pulled out of
	 * the table omap[], some special cases are
	 * detailed below */
	o = 0
	for {
		/* this linear search should really be a binary
		 * search */
		if int32(qbe.s_omap[o].Fop) == int32(_NOp) {
			_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+6898, libc.VaList(bp+40, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0)].Fname, int32(*(*int8)(unsafe.Pointer(__ccgo_ts + 6918 + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)))))))
		}
		if int32(qbe.s_omap[o].Fop) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x3fffffff>>0) {
			if int32(qbe.s_omap[o].Fcls) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30) || int32(qbe.s_omap[o].Fcls) == int32(_Ki) && int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 || int32(qbe.s_omap[o].Fcls) == int32(_Ka) {
				break
			}
		}
		goto _14
	_14:
		;
		o++
	}
	s_emitf(tls, qbe, qbe.s_omap[o].Ffmt, bp+12, e)
	goto _13
_2:
	;
	/* just do nothing for nops, they are inserted
	 * by some passes */
	goto _13
_3:
	;
	/* here, we try to use the 3-addresss form
	 * of multiplication when possible */
	v15 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v15
	v16 = v15
	v17 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v16
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v17
	v18 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _19
_19:
	if v18 != 0 {
		v20 = -int32(1)
		goto _21
	}
	v20 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _21
_21:
	if v20 == int32(_RCon) {
		r1 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
		*(*_Ref)(unsafe.Pointer(bp + 12 + 8)) = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
		*(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)) = r1
	}
	if v29 = int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0; v29 {
		v22 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v22
		v23 = v22
		v24 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v23
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v24
		v25 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _26
	_26:
		if v25 != 0 {
			v27 = -int32(1)
			goto _28
		}
		v27 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _28
	_28:
	}
	if v37 = v29 && v27 == int32(_RCon); v37 {
		v30 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v30
		v31 = v30
		v32 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v31
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v32
		v33 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _34
	_34:
		if v33 != 0 {
			v35 = -int32(1)
			goto _36
		}
		v35 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _36
	_36:
	}
	if v37 && v35 == int32(_RTmp) {
		s_emitf(tls, qbe, __ccgo_ts+6923, bp+12, e)
		goto _13
	}
	goto Table
_4:
	;
	/* we have to use the negation trick to handle
	 * some 3-address subtractions */
	v38 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	v39 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	*(*_Ref)(unsafe.Pointer(bp)) = v38
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v39
	v40 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _41
_41:
	;
	if v46 = v40 != 0; v46 {
		v42 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
		v43 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
		*(*_Ref)(unsafe.Pointer(bp)) = v42
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v43
		v44 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _45
	_45:
	}
	if v46 && !(v44 != 0) {
		ineg = _Ins{
			F__ccgo0: uint32(_Oneg)&0x3fffffff<<0 | libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30))&0x3<<30,
			Fto:      (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto,
			Farg: [2]_Ref{
				0: (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto,
			},
		}
		s_emitins(tls, qbe, ineg, e)
		s_emitf(tls, qbe, __ccgo_ts+6941, bp+12, e)
		goto _13
	}
	goto Table
_5:
	;
	v47 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	v48 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
	*(*_Ref)(unsafe.Pointer(bp)) = v47
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v48
	v49 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _50
_50:
	if !(v49 != 0) {
		s_emitf(tls, qbe, __ccgo_ts+6954, bp+12, e)
	}
	if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
		s_emitf(tls, qbe, __ccgo_ts+6967, bp+12, e)
	} else {
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6976, libc.VaList(bp+40, int32(*(*int8)(unsafe.Pointer(__ccgo_ts + 7005 + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30))))), uintptr(unsafe.Pointer(&qbe.x_T))+136, x_stashbits(tls, qbe, qbe.s_negmask[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)], int32(16)), s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 4 + 0))&0xfffffff8>>3), int32(_SLong))))
	}
	goto _13
_6:
	;
	/* use xmm15 to adjust the instruction when the
	 * conversion to 2-address in emitf() would fail */
	v51 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	v52 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4))
	*(*_Ref)(unsafe.Pointer(bp)) = v51
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v52
	v53 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _54
_54:
	if v53 != 0 {
		*(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)) = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(int32(_XMM0)+libc.Int32FromInt32(15))&0x1fffffff<<3,
		}
		s_emitf(tls, qbe, __ccgo_ts+7010, bp+12, e)
		s_emitf(tls, qbe, __ccgo_ts+6954, bp+12, e)
		*(*_Ref)(unsafe.Pointer(bp + 12 + 8)) = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	}
	goto Table
_7:
	;
	/* copies are used for many things; see my note
	 * to understand how to load big constants:
	 * https://c9x.me/notes/2015-09-19.html */
	v55 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	v56 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v55
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v56
	v57 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _58
_58:
	;
	if v63 = v57 != 0; !v63 {
		v59 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
		v60 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v59
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v60
		v61 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _62
	_62:
	}
	if v63 || v61 != 0 {
		goto _13
	}
	v64 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	v65 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
	*(*_Ref)(unsafe.Pointer(bp)) = v64
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v65
	v66 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _67
_67:
	if v66 != 0 {
		goto _13
	}
	v68 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v68
	v69 = v68
	v70 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v69
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v70
	v71 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _72
_72:
	if v71 != 0 {
		v73 = -int32(1)
		goto _74
	}
	v73 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _74
_74:
	t0 = v73
	if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30) == int32(_Kl) && t0 == int32(_RCon) && (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3))*32))).Ftype1 == int32(_CBits) {
		val = *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3))*32 + 16))
		if x_isreg(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto) != 0 {
			if val >= 0 && val <= libc.Int64FromUint32(4294967295) {
				s_emitf(tls, qbe, __ccgo_ts+5843, bp+12, e)
				goto _13
			}
		}
		v75 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v75
		v76 = v75
		v77 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v76
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v77
		v78 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _79
	_79:
		if v78 != 0 {
			v80 = -int32(1)
			goto _81
		}
		v80 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _81
	_81:
		if v80 == int32(_RSlot) {
			if val < int64(-libc.Int32FromInt32(2147483647)-libc.Int32FromInt32(1)) || val > int64(2147483647) {
				s_emitf(tls, qbe, __ccgo_ts+7023, bp+12, e)
				s_emitf(tls, qbe, __ccgo_ts+7035, bp+12, e)
				goto _13
			}
		}
	}
	if x_isreg(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto) != 0 && t0 == int32(_RCon) && (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3))*32))).Ftype1 == int32(_CAddr) {
		s_emitf(tls, qbe, __ccgo_ts+6053, bp+12, e)
		goto _13
	}
	v82 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v82
	v83 = v82
	v84 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v83
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v84
	v85 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _86
_86:
	if v85 != 0 {
		v87 = -int32(1)
		goto _88
	}
	v87 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _88
_88:
	;
	if v87 == int32(_RSlot) && (t0 == int32(_RSlot) || t0 == int32(_RMem)) {
		if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)&int32(1) != 0 {
			v89 = int32(_Kd)
		} else {
			v89 = int32(_Ks)
		}
		libc.SetBitFieldPtr32Uint32(bp+12+0, libc.Uint32FromInt32(v89), 30, 0xc0000000)
		*(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)) = _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(int32(_XMM0)+libc.Int32FromInt32(15))&0x1fffffff<<3,
		}
		s_emitf(tls, qbe, __ccgo_ts+7053, bp+12, e)
		s_emitf(tls, qbe, __ccgo_ts+7066, bp+12, e)
		goto _13
	}
	/* conveniently, the assembler knows if it
	 * should use movabsq when reading movq */
	s_emitf(tls, qbe, __ccgo_ts+6954, bp+12, e)
	goto _13
_8:
	;
	if v97 = !(qbe.x_T.Fapple != 0); v97 {
		v90 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v90
		v91 = v90
		v92 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v91
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v92
		v93 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _94
	_94:
		if v93 != 0 {
			v95 = -int32(1)
			goto _96
		}
		v95 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _96
	_96:
	}
	if v97 && v95 == int32(_RCon) && (*(*_Con)(unsafe.Pointer((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3))*32))).Fsym.Ftype1 == int32(_SThr) {
		/* derive the symbol address from the TCB
		 * address at offset 0 of %fs */
		con = (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3))*32
		sym = x_str(tls, qbe, (*_Con)(unsafe.Pointer(con)).Fsym.Fid)
		s_emitf(tls, qbe, __ccgo_ts+7079, bp+12, e)
		if int32(*(*int8)(unsafe.Pointer(sym))) == int32('"') {
			v98 = __ccgo_ts + 4511
		} else {
			v98 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
		}
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+7096, libc.VaList(bp+40, v98, sym))
		if *(*_int64_t)(unsafe.Pointer(con + 16)) != 0 {
			fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+6775, libc.VaList(bp+40, *(*_int64_t)(unsafe.Pointer(con + 16))))
		}
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+7113, libc.VaList(bp+40, s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 4 + 0))&0xfffffff8>>3), int32(_SLong)), s_regtoa(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 4 + 0))&0xfffffff8>>3), int32(_SLong))))
		goto _13
	}
	goto Table
_9:
	;
	/* calls simply have a weird syntax in AT&T
	 * assembly... */
	v99 = *(*_Ref)(unsafe.Pointer(bp + 12 + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v99
	v100 = v99
	v101 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v100
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v101
	v102 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _103
_103:
	if v102 != 0 {
		v104 = -int32(1)
		goto _105
	}
	v104 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _105
_105:
	switch v104 {
	case int32(_RCon):
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+7127, 0)
		s_emitcon(tls, qbe, (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3))*32, e)
		fprintf(tls, qbe, (*_E)(unsafe.Pointer(e)).Ff, __ccgo_ts+82, 0)
	case int32(_RTmp):
		s_emitf(tls, qbe, __ccgo_ts+7135, bp+12, e)
	default:
		_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+7146, 0)
	}
	goto _13
_10:
	;
	/* there is no good reason why this is here
	 * maybe we should split Osalloc in 2 different
	 * instructions depending on the result
	 */
	s_emitf(tls, qbe, __ccgo_ts+7168, bp+12, e)
	v106 = (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto
	v107 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v106
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v107
	v108 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _109
_109:
	if !(v108 != 0) {
		s_emitcopy(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp + 12))).Fto, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_RSP)&0x1fffffff<<3,
		}, int32(_Kl), e)
	}
	goto _13
_11:
	;
	if int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
		goto Table
	}
	/* for floats, there is no swap instruction
	 * so we use xmm15 as a temporary
	 */
	s_emitcopy(tls, qbe, _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(int32(_XMM0)+libc.Int32FromInt32(15))&0x1fffffff<<3,
	}, *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30), e)
	s_emitcopy(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12 + 8)), *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)), int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30), e)
	s_emitcopy(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12 + 8 + 1*4)), _Ref{
		F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(int32(_XMM0)+libc.Int32FromInt32(15))&0x1fffffff<<3,
	}, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xc0000000>>30), e)
	goto _13
_12:
	;
	x_emitdbgloc(tls, qbe, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 0))&0xfffffff8>>3)), libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 8 + 1*4 + 0))&0xfffffff8>>3)), (*_E)(unsafe.Pointer(e)).Ff)
	goto _13
_13:
}

func s_framesz(tls *libc.TLS, qbe *_QBE, e uintptr) {
	var f, i, o _uint64_t
	_, _, _ = f, i, o
	/* specific to NAlign == 3 */
	o = uint64(0)
	if !((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fleaf != 0) {
		i = uint64(0)
		o = libc.Uint64FromInt32(0)
		for {
			if !(i < uint64(_NCLR)) {
				break
			}
			o ^= (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Freg >> qbe.x_amd64_sysv_rclob[i]
			goto _1
		_1:
			;
			i++
		}
		o &= uint64(1)
	}
	f = libc.Uint64FromInt32((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fslot)
	f = (f + uint64(3)) & libc.Uint64FromInt32(-libc.Int32FromInt32(4))
	if f > uint64(0) && (*_E)(unsafe.Pointer(e)).Ffp == int32(_RSP) && (*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fsalign == int32(4) {
		f += uint64(2)
	}
	(*_E)(unsafe.Pointer(e)).Ffsz = uint64(4)*f + uint64(8)*o + libc.Uint64FromInt32(int32(176)*int32((*_Fn)(unsafe.Pointer((*_E)(unsafe.Pointer(e)).Ffn)).Fvararg))
}

func x_amd64_emitfn(tls *libc.TLS, qbe *_QBE, fn uintptr, f uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var b, e, i, r, s, v12 uintptr
	var c, lbl, n, o int32
	var _ /* itmp at bp+40 */ _Ins
	_, _, _, _, _, _, _, _, _, _ = b, c, e, i, lbl, n, o, r, s, v12
	*(*_E)(unsafe.Pointer(bp)) = _E{
		Ff:  f,
		Ffn: fn,
	}
	e = bp
	x_emitfnlnk(tls, qbe, fn+83, fn+168, f)
	fputs(tls, qbe, __ccgo_ts+7208, f)
	if !((*_Fn)(unsafe.Pointer(fn)).Fleaf != 0) || (*_Fn)(unsafe.Pointer(fn)).Fvararg != 0 || (*_Fn)(unsafe.Pointer(fn)).Fdynalloc != 0 {
		(*_E)(unsafe.Pointer(e)).Ffp = int32(_RBP)
		fputs(tls, qbe, __ccgo_ts+7218, f)
	} else {
		(*_E)(unsafe.Pointer(e)).Ffp = int32(_RSP)
	}
	s_framesz(tls, qbe, e)
	if (*_E)(unsafe.Pointer(e)).Ffsz != 0 {
		fprintf(tls, qbe, f, __ccgo_ts+7248, libc.VaList(bp+64, (*_E)(unsafe.Pointer(e)).Ffsz))
	}
	if (*_Fn)(unsafe.Pointer(fn)).Fvararg != 0 {
		o = -int32(176)
		r = uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rsave))
		for {
			if !(r < uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rsave))+6*4) {
				break
			}
			fprintf(tls, qbe, f, __ccgo_ts+7268, libc.VaList(bp+64, *(*uintptr)(unsafe.Pointer(uintptr(unsafe.Pointer(&qbe.s_rname)) + uintptr(*(*int32)(unsafe.Pointer(r)))*32)), o))
			goto _1
		_1:
			;
			r += 4
			o += int32(8)
		}
		n = 0
		for {
			if !(n < int32(8)) {
				break
			}
			fprintf(tls, qbe, f, __ccgo_ts+7291, libc.VaList(bp+64, n, o))
			goto _2
		_2:
			;
			n++
			o += int32(16)
		}
	}
	r = uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rclob))
	for {
		if !(r < uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rclob))+uintptr(_NCLR)*4) {
			break
		}
		if (*_Fn)(unsafe.Pointer(fn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(r))) != 0 {
			*(*_Ref)(unsafe.Pointer(bp + 40 + 8)) = _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(r)))&0x1fffffff<<3,
			}
			s_emitf(tls, qbe, __ccgo_ts+7319, bp+40, e)
			(*_E)(unsafe.Pointer(e)).Fnclob++
		}
		goto _3
	_3:
		;
		r += 4
	}
	lbl = 0
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if lbl != 0 || (*_Blk)(unsafe.Pointer(b)).Fnpred > uint32(1) {
			fprintf(tls, qbe, f, __ccgo_ts+7329, libc.VaList(bp+64, uintptr(unsafe.Pointer(&qbe.x_T))+136, libc.Uint32FromInt32(qbe.s_id0)+(*_Blk)(unsafe.Pointer(b)).Fid))
		}
		i = (*_Blk)(unsafe.Pointer(b)).Fins
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
				break
			}
			s_emitins(tls, qbe, *(*_Ins)(unsafe.Pointer(i)), e)
			goto _5
		_5:
			;
			i += 16
		}
		lbl = int32(1)
		switch int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) {
		case int32(_Jhlt):
			goto _6
		case int32(_Jret0):
			goto _7
		case int32(_Jjmp):
			goto _8
		default:
			goto _9
		}
		goto _10
	_6:
		;
		fprintf(tls, qbe, f, __ccgo_ts+7338, 0)
		goto _10
	_7:
		;
		if (*_Fn)(unsafe.Pointer(fn)).Fdynalloc != 0 {
			fprintf(tls, qbe, f, __ccgo_ts+7344, libc.VaList(bp+64, (*_E)(unsafe.Pointer(e)).Ffsz+libc.Uint64FromInt32((*_E)(unsafe.Pointer(e)).Fnclob*int32(8))))
		}
		r = uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rclob)) + uintptr(_NCLR)*4
		for {
			if !(r > uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rclob))) {
				break
			}
			r -= 4
			v12 = r
			if (*_Fn)(unsafe.Pointer(fn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(v12))) != 0 {
				*(*_Ref)(unsafe.Pointer(bp + 40 + 8)) = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(r)))&0x1fffffff<<3,
				}
				s_emitf(tls, qbe, __ccgo_ts+7383, bp+40, e)
			}
			goto _11
		_11:
		}
		if (*_E)(unsafe.Pointer(e)).Ffp == int32(_RBP) {
			fputs(tls, qbe, __ccgo_ts+7392, f)
		} else {
			if (*_E)(unsafe.Pointer(e)).Ffsz != 0 {
				fprintf(tls, qbe, f, __ccgo_ts+7400, libc.VaList(bp+64, (*_E)(unsafe.Pointer(e)).Ffsz))
			}
		}
		fputs(tls, qbe, __ccgo_ts+7420, f)
		goto _10
	_8:
		;
		goto Jmp
	Jmp:
		;
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != (*_Blk)(unsafe.Pointer(b)).Flink {
			fprintf(tls, qbe, f, __ccgo_ts+7426, libc.VaList(bp+64, uintptr(unsafe.Pointer(&qbe.x_T))+136, libc.Uint32FromInt32(qbe.s_id0)+(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs1)).Fid))
		} else {
			lbl = 0
		}
		goto _10
	_9:
		;
		c = int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) - int32(_Jjf)
		if 0 <= c && c <= int32(_NCmp) {
			if (*_Blk)(unsafe.Pointer(b)).Flink == (*_Blk)(unsafe.Pointer(b)).Fs2 {
				s = (*_Blk)(unsafe.Pointer(b)).Fs1
				(*_Blk)(unsafe.Pointer(b)).Fs1 = (*_Blk)(unsafe.Pointer(b)).Fs2
				(*_Blk)(unsafe.Pointer(b)).Fs2 = s
			} else {
				c = x_cmpneg(tls, qbe, c)
			}
			fprintf(tls, qbe, f, __ccgo_ts+7439, libc.VaList(bp+64, qbe.s_ctoa[c], uintptr(unsafe.Pointer(&qbe.x_T))+136, libc.Uint32FromInt32(qbe.s_id0)+(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs2)).Fid))
			goto Jmp
		}
		_die_(tls, qbe, __ccgo_ts+6786, __ccgo_ts+7452, libc.VaList(bp+64, int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)))
	_10:
		;
		goto _4
	_4:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	qbe.s_id0 = int32(uint32(qbe.s_id0) + (*_Fn)(unsafe.Pointer(fn)).Fnblk)
	if !(qbe.x_T.Fapple != 0) {
		x_elf_emitfnfin(tls, qbe, fn+83, f)
	}
}

const _R0 = 1

const _R1 = 2

const _R2 = 3

const _R3 = 4

const _R4 = 5

const _R5 = 6

const _R6 = 7

const _R7 = 8

const _R81 = 9

const _R91 = 10

const _R101 = 11

const _R111 = 12

const _R121 = 13

const _R131 = 14

const _R141 = 15

const _R151 = 16

const _IP0 = 17

const _IP1 = 18

const _R18 = 19

const _R19 = 20

const _R20 = 21

const _R21 = 22

const _R22 = 23

const _R23 = 24

const _R24 = 25

const _R25 = 26

const _R26 = 27

const _R27 = 28

const _R28 = 29

const _FP = 30

const _LR = 31

const _SP = 32

const _V0 = 33

const _V1 = 34

const _V2 = 35

const _V3 = 36

const _V4 = 37

const _V5 = 38

const _V6 = 39

const _V7 = 40

const _V8 = 41

const _V9 = 42

const _V10 = 43

const _V11 = 44

const _V12 = 45

const _V13 = 46

const _V14 = 47

const _V15 = 48

const _V16 = 49

const _V17 = 50

const _V18 = 51

const _V19 = 52

const _V20 = 53

const _V21 = 54

const _V22 = 55

const _V23 = 56

const _V24 = 57

const _V25 = 58

const _V26 = 59

const _V27 = 60

const _V28 = 61

const _V29 = 62

const _V30 = 63

const _NFPR1 = 31

const _NGPR1 = 32

const _NGPS1 = 20

const _NFPS1 = 23

func s_arm64_memargs(tls *libc.TLS, qbe *_QBE, op int32) (r int32) {
	_ = op
	return 0
}

func (qbe *_QBE) init3() {
	p := unsafe.Pointer(&qbe.x_T_arm64)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(x_arm64_retregs)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(x_arm64_argregs)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(s_arm64_memargs)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(x_elimsb)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(x_arm64_abi)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(x_arm64_isel)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(x_arm64_emitfn)
	*(*uintptr)(unsafe.Add(p, 128)) = __ccgo_fp(x_elf_emitfin)
}

func (qbe *_QBE) init4() {
	p := unsafe.Pointer(&qbe.x_T_arm64_apple)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(x_arm64_retregs)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(x_arm64_argregs)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(s_arm64_memargs)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(x_apple_extsb)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(x_arm64_abi)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(x_arm64_isel)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(x_arm64_emitfn)
	*(*uintptr)(unsafe.Add(p, 128)) = __ccgo_fp(x_macho_emitfin)
}

type _Class = struct {
	Fclass int8
	Fishfa int8
	Fhfa   struct {
		Fbase int8
		Fsize _uchar
	}
	Fsize  _uint
	Falign _uint
	Ft     uintptr
	Fnreg  _uchar
	Fngp   _uchar
	Fnfp   _uchar
	Freg   [4]int32
	Fcls   [4]int32
}

type _Insl = struct {
	Fi    _Ins
	Flink uintptr
}

type _Params = struct {
	Fngp _uint
	Fnfp _uint
	Fstk _uint
}

const _Cstk = 1

const /* pass on the stack */
_Cptr = 2

/* layout of call's second argument (RCall)
 *
 *         13
 *  29   14 |    9    5   2  0
 *  |0.00|x|x|xxxx|xxxx|xxx|xx|                  range
 *        | |    |    |   |  ` gp regs returned (0..2)
 *        | |    |    |   ` fp regs returned    (0..4)
 *        | |    |    ` gp regs passed          (0..8)
 *        | |     ` fp regs passed              (0..8)
 *        | ` indirect result register x8 used  (0..1)
 *        ` env pointer passed in x9            (0..1)
 */

func s_isfloatv(tls *libc.TLS, qbe *_QBE, t uintptr, cls uintptr) (r int32) {
	var f uintptr
	var n _uint
	_, _ = f, n
	n = uint32(0)
	for {
		if !(n < (*_Typ)(unsafe.Pointer(t)).Fnunion) {
			break
		}
		f = (*_Typ)(unsafe.Pointer(t)).Ffields + uintptr(n)*264
		for {
			if !((*_Field)(unsafe.Pointer(f)).Ftype1 != int32(_FEnd)) {
				break
			}
			switch (*_Field)(unsafe.Pointer(f)).Ftype1 {
			case int32(_Fs):
				if int32(*(*int8)(unsafe.Pointer(cls))) == int32(_Kd) {
					return 0
				}
				*(*int8)(unsafe.Pointer(cls)) = int8(_Ks)
			case int32(_Fd):
				if int32(*(*int8)(unsafe.Pointer(cls))) == int32(_Ks) {
					return 0
				}
				*(*int8)(unsafe.Pointer(cls)) = int8(_Kd)
			case int32(_FTyp):
				if s_isfloatv(tls, qbe, qbe.x_typ+uintptr((*_Field)(unsafe.Pointer(f)).Flen1)*112, cls) != 0 {
					break
				}
				/* fall through */
				fallthrough
			default:
				return 0
			}
			goto _2
		_2:
			;
			f += 8
		}
		goto _1
	_1:
		;
		n++
	}
	return int32(1)
}

func s_typclass1(tls *libc.TLS, qbe *_QBE, c uintptr, t uintptr, gp uintptr, fp uintptr) {
	var hfasz, sz _uint64_t
	var n _uint
	var v1 int32
	var v4, v7, p2, p5 uintptr
	_, _, _, _, _, _, _, _ = hfasz, n, sz, v1, v4, v7, p2, p5
	sz = ((*_Typ)(unsafe.Pointer(t)).Fsize + uint64(7)) & libc.Uint64FromInt32(-libc.Int32FromInt32(8))
	(*_Class)(unsafe.Pointer(c)).Ft = t
	(*_Class)(unsafe.Pointer(c)).Fclass = 0
	(*_Class)(unsafe.Pointer(c)).Fngp = uint8(0)
	(*_Class)(unsafe.Pointer(c)).Fnfp = uint8(0)
	(*_Class)(unsafe.Pointer(c)).Falign = uint32(8)
	if (*_Typ)(unsafe.Pointer(t)).Falign > int32(3) {
		_err(tls, qbe, __ccgo_ts+7470, 0)
	}
	(*_Class)(unsafe.Pointer(c)).Fsize = uint32(sz)
	(*_Class)(unsafe.Pointer(c)).Fhfa.Fbase = int8(_Kx)
	(*_Class)(unsafe.Pointer(c)).Fishfa = int8(s_isfloatv(tls, qbe, t, c+2))
	if int32((*_Class)(unsafe.Pointer(c)).Fhfa.Fbase)&int32(1) != 0 {
		v1 = int32(8)
	} else {
		v1 = int32(4)
	}
	hfasz = (*_Typ)(unsafe.Pointer(t)).Fsize / libc.Uint64FromInt32(v1)
	p2 = c + 1
	*(*int8)(unsafe.Pointer(p2)) = int8(int32(*(*int8)(unsafe.Pointer(p2))) & libc.BoolInt32(!((*_Typ)(unsafe.Pointer(t)).Fisdark != 0) && hfasz <= uint64(4)))
	(*_Class)(unsafe.Pointer(c)).Fhfa.Fsize = uint8(hfasz)
	if (*_Class)(unsafe.Pointer(c)).Fishfa != 0 {
		n = uint32(0)
		for {
			if !(uint64(n) < hfasz) {
				break
			}
			v4 = fp
			fp += 4
			*(*int32)(unsafe.Pointer(c + 28 + uintptr(n)*4)) = *(*int32)(unsafe.Pointer(v4))
			*(*int32)(unsafe.Pointer(c + 44 + uintptr(n)*4)) = int32((*_Class)(unsafe.Pointer(c)).Fhfa.Fbase)
			goto _3
		_3:
			;
			n++
			(*_Class)(unsafe.Pointer(c)).Fnfp++
		}
		(*_Class)(unsafe.Pointer(c)).Fnreg = uint8(n)
	} else {
		if (*_Typ)(unsafe.Pointer(t)).Fisdark != 0 || sz > uint64(16) || sz == uint64(0) {
			/* large structs are replaced by a
			 * pointer to some caller-allocated
			 * memory */
			p5 = c
			*(*int8)(unsafe.Pointer(p5)) = int8(int32(*(*int8)(unsafe.Pointer(p5))) | int32(_Cptr))
			(*_Class)(unsafe.Pointer(c)).Fsize = uint32(8)
			(*_Class)(unsafe.Pointer(c)).Fngp = uint8(1)
			*(*int32)(unsafe.Pointer(c + 28)) = *(*int32)(unsafe.Pointer(gp))
			*(*int32)(unsafe.Pointer(c + 44)) = int32(_Kl)
		} else {
			n = uint32(0)
			for {
				if !(uint64(n) < sz/uint64(8)) {
					break
				}
				v7 = gp
				gp += 4
				*(*int32)(unsafe.Pointer(c + 28 + uintptr(n)*4)) = *(*int32)(unsafe.Pointer(v7))
				*(*int32)(unsafe.Pointer(c + 44 + uintptr(n)*4)) = int32(_Kl)
				goto _6
			_6:
				;
				n++
				(*_Class)(unsafe.Pointer(c)).Fngp++
			}
			(*_Class)(unsafe.Pointer(c)).Fnreg = uint8(n)
		}
	}
}

func s_sttmps(tls *libc.TLS, qbe *_QBE, tmp uintptr, cls uintptr, nreg _uint, mem _Ref, fn uintptr) {
	var n _uint
	var off _uint64_t
	var r _Ref
	var v2 int32
	_, _, _, _ = n, off, r, v2
	off = uint64(0)
	n = uint32(0)
	for {
		if !(n < nreg) {
			break
		}
		*(*_Ref)(unsafe.Pointer(tmp + uintptr(n)*4)) = x_newtmp(tls, qbe, __ccgo_ts+5196, *(*int32)(unsafe.Pointer(cls + uintptr(n)*4)), fn)
		r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		x_emit(tls, qbe, qbe.s_store3[*(*int32)(unsafe.Pointer(cls + uintptr(n)*4))], 0, _Ref{}, *(*_Ref)(unsafe.Pointer(tmp + uintptr(n)*4)), r)
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, mem, x_getcon(tls, qbe, libc.Int64FromUint64(off), fn))
		if *(*int32)(unsafe.Pointer(cls + uintptr(n)*4))&int32(1) != 0 {
			v2 = int32(8)
		} else {
			v2 = int32(4)
		}
		off += libc.Uint64FromInt32(v2)
		goto _1
	_1:
		;
		n++
	}
}

// C documentation
//
//	/* todo, may read out of bounds */
func s_ldregs(tls *libc.TLS, qbe *_QBE, reg uintptr, cls uintptr, n int32, mem _Ref, fn uintptr) {
	var i, v2 int32
	var off _uint64_t
	var r _Ref
	_, _, _, _ = i, off, r, v2
	off = uint64(0)
	i = 0
	for {
		if !(i < n) {
			break
		}
		r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Oload), *(*int32)(unsafe.Pointer(cls + uintptr(i)*4)), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(reg + uintptr(i)*4)))&0x1fffffff<<3,
		}, r, _Ref{})
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, mem, x_getcon(tls, qbe, libc.Int64FromUint64(off), fn))
		if *(*int32)(unsafe.Pointer(cls + uintptr(i)*4))&int32(1) != 0 {
			v2 = int32(8)
		} else {
			v2 = int32(4)
		}
		off += libc.Uint64FromInt32(v2)
		goto _1
	_1:
		;
		i++
	}
}

func s_selret1(tls *libc.TLS, qbe *_QBE, b uintptr, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var cty, j, k int32
	var r _Ref
	var _ /* cr at bp+0 */ _Class
	_, _, _, _ = cty, j, k, r
	j = int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)
	if !(libc.Uint32FromInt32(j)-uint32(_Jretw) <= libc.Uint32FromInt32(int32(_Jret0)-int32(_Jretw))) || j == int32(_Jret0) {
		return
	}
	r = (*_Blk)(unsafe.Pointer(b)).Fjmp.Farg
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jret0)
	if j == int32(_Jretc) {
		s_typclass1(tls, qbe, bp, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112, uintptr(unsafe.Pointer(&qbe.s_gpreg)), uintptr(unsafe.Pointer(&qbe.s_fpreg)))
		if int32((*(*_Class)(unsafe.Pointer(bp))).Fclass)&int32(_Cptr) != 0 {
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | uint32((*_Typ)(unsafe.Pointer((*(*_Class)(unsafe.Pointer(bp))).Ft)).Fsize&libc.Uint64FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, r, (*_Fn)(unsafe.Pointer(fn)).Fretr)
			cty = 0
		} else {
			s_ldregs(tls, qbe, bp+28, bp+44, libc.Int32FromUint8((*(*_Class)(unsafe.Pointer(bp))).Fnreg), r, fn)
			cty = libc.Int32FromUint8((*(*_Class)(unsafe.Pointer(bp))).Fnfp)<<int32(2) | libc.Int32FromUint8((*(*_Class)(unsafe.Pointer(bp))).Fngp)
		}
	} else {
		k = j - int32(_Jretw)
		if k>>int32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R0)&0x1fffffff<<3,
			}, r, _Ref{})
			cty = int32(1)
		} else {
			x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_V0)&0x1fffffff<<3,
			}, r, _Ref{})
			cty = libc.Int32FromInt32(1) << libc.Int32FromInt32(2)
		}
	}
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Farg = _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(cty)&0x1fffffff<<3,
	}
}

func s_argsclass1(tls *libc.TLS, qbe *_QBE, i0 uintptr, i1 uintptr, carg uintptr) (r int32) {
	var c, fp, gp, i, v20, v21, p19, p22, p23 uintptr
	var envc, nfp, ngp, va int32
	_, _, _, _, _, _, _, _, _, _, _, _, _ = c, envc, fp, gp, i, nfp, ngp, va, v20, v21, p19, p22, p23
	va = 0
	envc = 0
	gp = uintptr(unsafe.Pointer(&qbe.s_gpreg))
	fp = uintptr(unsafe.Pointer(&qbe.s_fpreg))
	ngp = int32(8)
	nfp = int32(8)
	i = i0
	c = carg
	for {
		if !(i < i1) {
			break
		}
		switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
		case int32(_Oparub):
			goto _2
		case int32(_Oparsb):
			goto _3
		case int32(_Oargub):
			goto _4
		case int32(_Oargsb):
			goto _5
		case int32(_Oparuh):
			goto _6
		case int32(_Oparsh):
			goto _7
		case int32(_Oarguh):
			goto _8
		case int32(_Oargsh):
			goto _9
		case int32(_Oarg):
			goto _10
		case int32(_Opar):
			goto _11
		case int32(_Oargc):
			goto _12
		case int32(_Oparc):
			goto _13
		case int32(_Oarge):
			goto _14
		case int32(_Opare):
			goto _15
		case int32(_Oargv):
			goto _16
		default:
			goto _17
		}
		goto _18
	_5:
		;
	_4:
		;
	_3:
		;
	_2:
		;
		(*_Class)(unsafe.Pointer(c)).Fsize = uint32(1)
		goto Scalar
	_9:
		;
	_8:
		;
	_7:
		;
	_6:
		;
		(*_Class)(unsafe.Pointer(c)).Fsize = uint32(2)
		goto Scalar
	_11:
		;
	_10:
		;
		(*_Class)(unsafe.Pointer(c)).Fsize = uint32(8)
		if qbe.x_T.Fapple != 0 && !(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)&libc.Int32FromInt32(1) != 0) {
			(*_Class)(unsafe.Pointer(c)).Fsize = uint32(4)
		}
		goto Scalar
	Scalar:
		;
		(*_Class)(unsafe.Pointer(c)).Falign = (*_Class)(unsafe.Pointer(c)).Fsize
		*(*int32)(unsafe.Pointer(c + 44)) = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30)
		if va != 0 {
			p19 = c
			*(*int8)(unsafe.Pointer(p19)) = int8(int32(*(*int8)(unsafe.Pointer(p19))) | int32(_Cstk))
			(*_Class)(unsafe.Pointer(c)).Fsize = uint32(8)
			(*_Class)(unsafe.Pointer(c)).Falign = uint32(8)
			goto _18
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 && ngp > 0 {
			ngp--
			v20 = gp
			gp += 4
			*(*int32)(unsafe.Pointer(c + 28)) = *(*int32)(unsafe.Pointer(v20))
			goto _18
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == int32(1) && nfp > 0 {
			nfp--
			v21 = fp
			fp += 4
			*(*int32)(unsafe.Pointer(c + 28)) = *(*int32)(unsafe.Pointer(v21))
			goto _18
		}
		p22 = c
		*(*int8)(unsafe.Pointer(p22)) = int8(int32(*(*int8)(unsafe.Pointer(p22))) | int32(_Cstk))
		goto _18
	_13:
		;
	_12:
		;
		s_typclass1(tls, qbe, c, qbe.x_typ+uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*112, gp, fp)
		if libc.Int32FromUint8((*_Class)(unsafe.Pointer(c)).Fngp) <= ngp {
			if libc.Int32FromUint8((*_Class)(unsafe.Pointer(c)).Fnfp) <= nfp {
				ngp -= libc.Int32FromUint8((*_Class)(unsafe.Pointer(c)).Fngp)
				nfp -= libc.Int32FromUint8((*_Class)(unsafe.Pointer(c)).Fnfp)
				gp += uintptr((*_Class)(unsafe.Pointer(c)).Fngp) * 4
				fp += uintptr((*_Class)(unsafe.Pointer(c)).Fnfp) * 4
				goto _18
			} else {
				nfp = 0
			}
		} else {
			ngp = 0
		}
		p23 = c
		*(*int8)(unsafe.Pointer(p23)) = int8(int32(*(*int8)(unsafe.Pointer(p23))) | int32(_Cstk))
		goto _18
	_15:
		;
	_14:
		;
		*(*int32)(unsafe.Pointer(c + 28)) = int32(_R91)
		*(*int32)(unsafe.Pointer(c + 44)) = int32(_Kl)
		envc = int32(1)
		goto _18
	_16:
		;
		va = libc.BoolInt32(int32(qbe.x_T.Fapple) != 0)
		goto _18
	_17:
		;
		_die_(tls, qbe, __ccgo_ts+7513, __ccgo_ts+3532, 0)
	_18:
		;
		goto _1
	_1:
		;
		i += 16
		c += 64
	}
	return int32(int64(envc<<int32(14)) | (int64(gp)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.s_gpreg))))/4<<int32(5) | (int64(fp)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.s_fpreg))))/4<<int32(9))
}

func x_arm64_retregs(tls *libc.TLS, qbe *_QBE, _r _Ref, p uintptr) (r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	var b _bits
	var nfp, ngp, v1, v2 int32
	_, _, _, _, _ = b, nfp, ngp, v1, v2
	ngp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) & int32(3)
	nfp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(2) & int32(7)
	if p != 0 {
		*(*int32)(unsafe.Pointer(p)) = ngp
		*(*int32)(unsafe.Pointer(p + 1*4)) = nfp
	}
	b = uint64(0)
	for {
		v1 = ngp
		ngp--
		if !(v1 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_R0) + ngp)
	}
	for {
		v2 = nfp
		nfp--
		if !(v2 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_V0) + nfp)
	}
	return b
}

func x_arm64_argregs(tls *libc.TLS, qbe *_QBE, _r _Ref, p uintptr) (r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	var b _bits
	var nfp, ngp, x8, x9, v1, v2 int32
	_, _, _, _, _, _, _ = b, nfp, ngp, x8, x9, v1, v2
	ngp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(5) & int32(15)
	nfp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(9) & int32(15)
	x8 = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(13) & int32(1)
	x9 = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(14) & int32(1)
	if p != 0 {
		*(*int32)(unsafe.Pointer(p)) = ngp + x8 + x9
		*(*int32)(unsafe.Pointer(p + 1*4)) = nfp
	}
	b = uint64(0)
	for {
		v1 = ngp
		ngp--
		if !(v1 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_R0) + ngp)
	}
	for {
		v2 = nfp
		nfp--
		if !(v2 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_V0) + nfp)
	}
	return b | libc.Uint64FromInt32(x8)<<int32(_R81) | libc.Uint64FromInt32(x9)<<int32(_R91)
}

func s_stkblob(tls *libc.TLS, qbe *_QBE, r _Ref, c uintptr, fn uintptr, ilp uintptr) {
	var al int32
	var il uintptr
	var sz _uint64_t
	var v1 uint64
	_, _, _, _ = al, il, sz, v1
	il = x_alloc(tls, qbe, uint64(24))
	al = (*_Typ)(unsafe.Pointer((*_Class)(unsafe.Pointer(c)).Ft)).Falign - int32(2) /* NAlign == 3 */
	if al < 0 {
		al = 0
	}
	if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cptr) != 0 {
		v1 = (*_Typ)(unsafe.Pointer((*_Class)(unsafe.Pointer(c)).Ft)).Fsize
	} else {
		v1 = uint64((*_Class)(unsafe.Pointer(c)).Fsize)
	}
	sz = v1
	(*_Insl)(unsafe.Pointer(il)).Fi = _Ins{
		F__ccgo0: libc.Uint32FromInt32(int32(_Oalloc)+al)&0x3fffffff<<0 | uint32(_Kl)&0x3<<30,
		Fto:      r,
		Farg: [2]_Ref{
			0: x_getcon(tls, qbe, libc.Int64FromUint64(sz), fn),
		},
	}
	(*_Insl)(unsafe.Pointer(il)).Flink = *(*uintptr)(unsafe.Pointer(ilp))
	*(*uintptr)(unsafe.Pointer(ilp)) = il
}

func s_align(tls *libc.TLS, qbe *_QBE, x _uint, al _uint) (r _uint) {
	return (x + al - uint32(1)) & -al
}

func s_selcall1(tls *libc.TLS, qbe *_QBE, fn uintptr, i0 uintptr, i1 uintptr, ilp uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var c, ca, i uintptr
	var cty, op, v4 int32
	var n, off, stk _uint
	var r, rstk, v2, v3 _Ref
	var _ /* cr at bp+8 */ _Class
	var _ /* tmp at bp+72 */ [4]_Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _ = c, ca, cty, i, n, off, op, r, rstk, stk, v2, v3, v4
	ca = x_alloc(tls, qbe, libc.Uint64FromInt64((int64(i1)-int64(i0))/16)*uint64(64))
	cty = s_argsclass1(tls, qbe, i0, i1, ca)
	stk = uint32(0)
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cptr) != 0 {
			*(*_Ref)(unsafe.Pointer(i + 8)) = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			s_stkblob(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), c, fn, ilp)
			libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oarg), 0, 0x3fffffff)
		}
		if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cstk) != 0 {
			stk = s_align(tls, qbe, stk, (*_Class)(unsafe.Pointer(c)).Falign)
			stk += (*_Class)(unsafe.Pointer(c)).Fsize
		}
		goto _1
	_1:
		;
		i += 16
		c += 64
	}
	stk = s_align(tls, qbe, stk, uint32(16))
	rstk = x_getcon(tls, qbe, libc.Int64FromUint32(stk), fn)
	if stk != 0 {
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_SP)&0x1fffffff<<3,
		}, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_SP)&0x1fffffff<<3,
		}, rstk)
	}
	v2 = *(*_Ref)(unsafe.Pointer(i1 + 8 + 1*4))
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if !(v4 != 0) {
		s_typclass1(tls, qbe, bp+8, qbe.x_typ+uintptr(int32(*(*uint32)(unsafe.Pointer(i1 + 8 + 1*4 + 0))&0xfffffff8>>3))*112, uintptr(unsafe.Pointer(&qbe.s_gpreg)), uintptr(unsafe.Pointer(&qbe.s_fpreg)))
		s_stkblob(tls, qbe, (*_Ins)(unsafe.Pointer(i1)).Fto, bp+8, fn, ilp)
		cty |= libc.Int32FromUint8((*(*_Class)(unsafe.Pointer(bp + 8))).Fnfp)<<int32(2) | libc.Int32FromUint8((*(*_Class)(unsafe.Pointer(bp + 8))).Fngp)
		if int32((*(*_Class)(unsafe.Pointer(bp + 8))).Fclass)&int32(_Cptr) != 0 {
			/* spill & rega expect calls to be
			 * followed by copies from regs,
			 * so we emit a dummy
			 */
			cty |= libc.Int32FromInt32(1)<<libc.Int32FromInt32(13) | libc.Int32FromInt32(1)
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), _Ref{}, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R0)&0x1fffffff<<3,
			}, _Ref{})
		} else {
			s_sttmps(tls, qbe, bp+72, bp+8+44, uint32((*(*_Class)(unsafe.Pointer(bp + 8))).Fnreg), (*_Ins)(unsafe.Pointer(i1)).Fto, fn)
			n = uint32(0)
			for {
				if !(n < uint32((*(*_Class)(unsafe.Pointer(bp + 8))).Fnreg)) {
					break
				}
				r = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 8 + 28 + uintptr(n)*4)))&0x1fffffff<<3,
				}
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(bp + 8 + 44 + uintptr(n)*4)), (*(*[4]_Ref)(unsafe.Pointer(bp + 72)))[n], r, _Ref{})
				goto _6
			_6:
				;
				n++
			}
		}
	} else {
		if int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R0)&0x1fffffff<<3,
			}, _Ref{})
			cty |= int32(1)
		} else {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_V0)&0x1fffffff<<3,
			}, _Ref{})
			cty |= libc.Int32FromInt32(1) << libc.Int32FromInt32(2)
		}
	}
	x_emit(tls, qbe, int32(_Ocall), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i1 + 8)), _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(cty)&0x1fffffff<<3,
	})
	if cty&(libc.Int32FromInt32(1)<<libc.Int32FromInt32(13)) != 0 {
		/* struct return argument */
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R81)&0x1fffffff<<3,
		}, (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{})
	}
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cstk) != 0 {
			goto _7
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oarg) || int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oarge) {
			x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(c + 44)), _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 28)))&0x1fffffff<<3,
			}, *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargc) {
			s_ldregs(tls, qbe, c+28, c+44, libc.Int32FromUint8((*_Class)(unsafe.Pointer(c)).Fnreg), *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), fn)
		}
		goto _7
	_7:
		;
		i += 16
		c += 64
	}
	/* populate the stack */
	off = uint32(0)
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cstk) == 0 {
			goto _8
		}
		off = s_align(tls, qbe, off, (*_Class)(unsafe.Pointer(c)).Falign)
		r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oarg) || libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oargsb) <= libc.Uint32FromInt32(int32(_Oarguh)-int32(_Oargsb)) {
			switch (*_Class)(unsafe.Pointer(c)).Fsize {
			case uint32(1):
				op = int32(_Ostoreb)
			case uint32(2):
				op = int32(_Ostoreh)
			case uint32(4):
				fallthrough
			case uint32(8):
				op = qbe.s_store3[*(*int32)(unsafe.Pointer(c + 44))]
			default:
				_die_(tls, qbe, __ccgo_ts+7513, __ccgo_ts+3532, 0)
			}
			x_emit(tls, qbe, op, 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8)), r)
		} else {
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | (*_Class)(unsafe.Pointer(c)).Fsize&libc.Uint32FromInt32(0x1fffffff)&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), r)
		}
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_SP)&0x1fffffff<<3,
		}, x_getcon(tls, qbe, libc.Int64FromUint32(off), fn))
		off += (*_Class)(unsafe.Pointer(c)).Fsize
		goto _8
	_8:
		;
		i += 16
		c += 64
	}
	if stk != 0 {
		x_emit(tls, qbe, int32(_Osub), int32(_Kl), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_SP)&0x1fffffff<<3,
		}, _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_SP)&0x1fffffff<<3,
		}, rstk)
	}
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cptr) != 0 {
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | uint32((*_Typ)(unsafe.Pointer((*_Class)(unsafe.Pointer(c)).Ft)).Fsize&libc.Uint64FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), *(*_Ref)(unsafe.Pointer(i + 8)))
		}
		goto _9
	_9:
		;
		i += 16
		c += 64
	}
}

func s_selpar1(tls *libc.TLS, qbe *_QBE, fn uintptr, i0 uintptr, i1 uintptr) (r1 _Params) {
	bp := tls.Alloc(144)
	defer tls.Free(144)
	var c, ca, i, t, v5 uintptr
	var cty, n, op int32
	var off _uint
	var r _Ref
	var _ /* cr at bp+0 */ _Class
	var _ /* il at bp+64 */ uintptr
	var _ /* tmp at bp+72 */ [16]_Ref
	_, _, _, _, _, _, _, _, _, _ = c, ca, cty, i, n, off, op, r, t, v5
	ca = x_alloc(tls, qbe, libc.Uint64FromInt64((int64(i1)-int64(i0))/16)*uint64(64))
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
	cty = s_argsclass1(tls, qbe, i0, i1, ca)
	(*_Fn)(unsafe.Pointer(fn)).Freg = x_arm64_argregs(tls, qbe, _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(cty)&0x1fffffff<<3,
	}, uintptr(0))
	*(*uintptr)(unsafe.Pointer(bp + 64)) = uintptr(0)
	t = bp + 72
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Oparc) || int32((*_Class)(unsafe.Pointer(c)).Fclass)&(int32(_Cptr)|int32(_Cstk)) != 0 {
			goto _1
		}
		s_sttmps(tls, qbe, t, c+44, uint32((*_Class)(unsafe.Pointer(c)).Fnreg), (*_Ins)(unsafe.Pointer(i)).Fto, fn)
		s_stkblob(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, c, fn, bp+64)
		t += uintptr((*_Class)(unsafe.Pointer(c)).Fnreg) * 4
		goto _1
	_1:
		;
		i += 16
		c += 64
	}
	for {
		if !(*(*uintptr)(unsafe.Pointer(bp + 64)) != 0) {
			break
		}
		x_emiti(tls, qbe, (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 64)))).Fi)
		goto _2
	_2:
		;
		*(*uintptr)(unsafe.Pointer(bp + 64)) = (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 64)))).Flink
	}
	if (*_Fn)(unsafe.Pointer(fn)).Fretty >= 0 {
		s_typclass1(tls, qbe, bp, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112, uintptr(unsafe.Pointer(&qbe.s_gpreg)), uintptr(unsafe.Pointer(&qbe.s_fpreg)))
		if int32((*(*_Class)(unsafe.Pointer(bp))).Fclass)&int32(_Cptr) != 0 {
			(*_Fn)(unsafe.Pointer(fn)).Fretr = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), (*_Fn)(unsafe.Pointer(fn)).Fretr, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R81)&0x1fffffff<<3,
			}, _Ref{})
			*(*_bits)(unsafe.Pointer(fn + 64)) |= libc.Uint64FromInt32(1) << int32(_R81)
		}
	}
	t = bp + 72
	off = uint32(0)
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oparc) && !(int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cptr) != 0) {
			if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cstk) != 0 {
				off = s_align(tls, qbe, off, (*_Class)(unsafe.Pointer(c)).Falign)
				(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192))).Fslot = libc.Int32FromUint32(-(off + libc.Uint32FromInt32(2)))
				off += (*_Class)(unsafe.Pointer(c)).Fsize
			} else {
				n = 0
				for {
					if !(n < libc.Int32FromUint8((*_Class)(unsafe.Pointer(c)).Fnreg)) {
						break
					}
					r = _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 28 + uintptr(n)*4)))&0x1fffffff<<3,
					}
					v5 = t
					t += 4
					x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(c + 44 + uintptr(n)*4)), *(*_Ref)(unsafe.Pointer(v5)), r, _Ref{})
					goto _4
				_4:
					;
					n++
				}
			}
		} else {
			if int32((*_Class)(unsafe.Pointer(c)).Fclass)&int32(_Cstk) != 0 {
				off = s_align(tls, qbe, off, (*_Class)(unsafe.Pointer(c)).Falign)
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oparsb) <= libc.Uint32FromInt32(int32(_Oparuh)-int32(_Oparsb)) {
					op = int32(_Oloadsb) + (int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - int32(_Oparsb))
				} else {
					op = int32(_Oload)
				}
				x_emit(tls, qbe, op, *(*int32)(unsafe.Pointer(c + 44)), (*_Ins)(unsafe.Pointer(i)).Fto, _Ref{
					F__ccgo0: uint32(_RSlot)&0x7<<0 | -(off+libc.Uint32FromInt32(2))&libc.Uint32FromInt32(0x1fffffff)&0x1fffffff<<3,
				}, _Ref{})
				off += (*_Class)(unsafe.Pointer(c)).Fsize
			} else {
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(c + 44)), (*_Ins)(unsafe.Pointer(i)).Fto, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 28)))&0x1fffffff<<3,
				}, _Ref{})
			}
		}
		goto _3
	_3:
		;
		i += 16
		c += 64
	}
	return _Params{
		Fngp: libc.Uint32FromInt32(cty >> int32(5) & int32(15)),
		Fnfp: libc.Uint32FromInt32(cty >> int32(9) & int32(15)),
		Fstk: s_align(tls, qbe, off, uint32(8)),
	}
}

func s_split1(tls *libc.TLS, qbe *_QBE, fn uintptr, b uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var bn, v2 uintptr
	var v1 _uint
	_, _, _ = bn, v1, v2
	(*_Fn)(unsafe.Pointer(fn)).Fnblk++
	bn = x_newblk(tls, qbe)
	(*_Blk)(unsafe.Pointer(bn)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
	x_idup(tls, qbe, bn+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(bn)).Fnins))
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
	v2 = b + 60
	*(*_uint)(unsafe.Pointer(v2))++
	v1 = *(*_uint)(unsafe.Pointer(v2))
	(*_Blk)(unsafe.Pointer(bn)).Fvisit = v1
	x_strf(tls, qbe, bn+180, __ccgo_ts+188, libc.VaList(bp+8, b+180, (*_Blk)(unsafe.Pointer(b)).Fvisit))
	(*_Blk)(unsafe.Pointer(bn)).Floop = (*_Blk)(unsafe.Pointer(b)).Floop
	(*_Blk)(unsafe.Pointer(bn)).Flink = (*_Blk)(unsafe.Pointer(b)).Flink
	(*_Blk)(unsafe.Pointer(b)).Flink = bn
	return bn
}

func s_chpred1(tls *libc.TLS, qbe *_QBE, b uintptr, bp uintptr, bp1 uintptr) {
	var a _uint
	var p uintptr
	_, _ = a, p
	p = (*_Blk)(unsafe.Pointer(b)).Fphi
	for {
		if !(p != 0) {
			break
		}
		a = uint32(0)
		for {
			if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)) != bp) {
				break
			}
			goto _2
		_2:
			;
			a++
		}
		*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(a)*8)) = bp1
		goto _1
	_1:
		;
		p = (*_Phi)(unsafe.Pointer(p)).Flink
	}
}

func s_apple_selvaarg(tls *libc.TLS, qbe *_QBE, fn uintptr, b uintptr, i uintptr) {
	var ap, c8, stk, stk8 _Ref
	_, _, _, _ = ap, c8, stk, stk8
	_ = b
	c8 = x_getcon(tls, qbe, int64(8), fn)
	ap = *(*_Ref)(unsafe.Pointer(i + 8))
	stk8 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	stk = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, stk8, ap)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), stk8, stk, c8)
	x_emit(tls, qbe, int32(_Oload), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i)).Fto, stk, _Ref{})
	x_emit(tls, qbe, int32(_Oload), int32(_Kl), stk, ap, _Ref{})
}

func s_arm64_selvaarg(tls *libc.TLS, qbe *_QBE, fn uintptr, b uintptr, i uintptr) {
	var ap, c16, c24, c28, c8, loc, lreg, lstk, nr, r0, r1, v1, v2, v3, v4 _Ref
	var b0, breg, bstk uintptr
	var isgp int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = ap, b0, breg, bstk, c16, c24, c28, c8, isgp, loc, lreg, lstk, nr, r0, r1, v1, v2, v3, v4
	c8 = x_getcon(tls, qbe, int64(8), fn)
	c16 = x_getcon(tls, qbe, int64(16), fn)
	c24 = x_getcon(tls, qbe, int64(24), fn)
	c28 = x_getcon(tls, qbe, int64(28), fn)
	ap = *(*_Ref)(unsafe.Pointer(i + 8))
	isgp = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0)
	/* @b [...]
	       r0 =l add ap, (24 or 28)
	       nr =l loadsw r0
	       r1 =w csltw nr, 0
	       jnz r1, @breg, @bstk
	   @breg
	       r0 =l add ap, (8 or 16)
	       r1 =l loadl r0
	       lreg =l add r1, nr
	       r0 =w add nr, (8 or 16)
	       r1 =l add ap, (24 or 28)
	       storew r0, r1
	   @bstk
	       lstk =l loadl ap
	       r0 =l add lstk, 8
	       storel r0, ap
	   @b0
	       %loc =l phi @breg %lreg, @bstk %lstk
	       i->to =(i->cls) load %loc
	*/
	loc = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Oload), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i)).Fto, loc, _Ref{})
	b0 = s_split1(tls, qbe, fn, b)
	(*_Blk)(unsafe.Pointer(b0)).Fjmp = (*_Blk)(unsafe.Pointer(b)).Fjmp
	(*_Blk)(unsafe.Pointer(b0)).Fs1 = (*_Blk)(unsafe.Pointer(b)).Fs1
	(*_Blk)(unsafe.Pointer(b0)).Fs2 = (*_Blk)(unsafe.Pointer(b)).Fs2
	if (*_Blk)(unsafe.Pointer(b)).Fs1 != 0 {
		s_chpred1(tls, qbe, (*_Blk)(unsafe.Pointer(b)).Fs1, b, b0)
	}
	if (*_Blk)(unsafe.Pointer(b)).Fs2 != 0 && (*_Blk)(unsafe.Pointer(b)).Fs2 != (*_Blk)(unsafe.Pointer(b)).Fs1 {
		s_chpred1(tls, qbe, (*_Blk)(unsafe.Pointer(b)).Fs2, b, b0)
	}
	lreg = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	nr = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kw), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorew), int32(_Kw), _Ref{}, r0, r1)
	if isgp != 0 {
		v1 = c24
	} else {
		v1 = c28
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, ap, v1)
	if isgp != 0 {
		v2 = c8
	} else {
		v2 = c16
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kw), r0, nr, v2)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), lreg, r1, nr)
	x_emit(tls, qbe, int32(_Oload), int32(_Kl), r1, r0, _Ref{})
	if isgp != 0 {
		v3 = c8
	} else {
		v3 = c16
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, v3)
	breg = s_split1(tls, qbe, fn, b)
	(*_Blk)(unsafe.Pointer(breg)).Fjmp.Ftype1 = int16(_Jjmp)
	(*_Blk)(unsafe.Pointer(breg)).Fs1 = b0
	lstk = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r0, ap)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, lstk, c8)
	x_emit(tls, qbe, int32(_Oload), int32(_Kl), lstk, ap, _Ref{})
	bstk = s_split1(tls, qbe, fn, b)
	(*_Blk)(unsafe.Pointer(bstk)).Fjmp.Ftype1 = int16(_Jjmp)
	(*_Blk)(unsafe.Pointer(bstk)).Fs1 = b0
	(*_Blk)(unsafe.Pointer(b0)).Fphi = x_alloc(tls, qbe, uint64(40))
	*(*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)) = _Phi{
		Fto:   loc,
		Farg:  x_vnew(tls, qbe, uint64(2), uint64(4), int32(_PFn)),
		Fblk:  x_vnew(tls, qbe, uint64(2), uint64(8), int32(_PFn)),
		Fnarg: uint32(2),
		Fcls:  int32(_Kl),
	}
	*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Fblk)) = bstk
	*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Fblk + 1*8)) = breg
	*(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Farg)) = lstk
	*(*_Ref)(unsafe.Pointer((*_Phi)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b0)).Fphi)).Farg + 1*4)) = lreg
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kw), fn)
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jjnz)
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Farg = r1
	(*_Blk)(unsafe.Pointer(b)).Fs1 = breg
	(*_Blk)(unsafe.Pointer(b)).Fs2 = bstk
	x_emit(tls, qbe, int32(_Ocmpw)+int32(_Cislt), int32(_Kw), r1, nr, _Ref{
		F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
	})
	x_emit(tls, qbe, int32(_Oloadsw), int32(_Kl), nr, r0, _Ref{})
	if isgp != 0 {
		v4 = c24
	} else {
		v4 = c28
	}
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, v4)
}

func s_apple_selvastart(tls *libc.TLS, qbe *_QBE, fn uintptr, p _Params, ap _Ref) {
	var arg, off, stk _Ref
	_, _, _ = arg, off, stk
	off = x_getcon(tls, qbe, libc.Int64FromUint32(p.Fstk), fn)
	stk = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	arg = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, arg, ap)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), arg, stk, off)
	x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), stk, _Ref{
		F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(-libc.Int32FromInt32(1)&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
	}, _Ref{})
}

func s_arm64_selvastart(tls *libc.TLS, qbe *_QBE, fn uintptr, p _Params, ap _Ref) {
	var r0, r1, rsave _Ref
	_, _, _ = r0, r1, rsave
	rsave = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r0, ap)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, rsave, x_getcon(tls, qbe, libc.Int64FromUint32(p.Fstk+uint32(192)), fn))
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r1, r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, rsave, x_getcon(tls, qbe, int64(64), fn))
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(8), fn))
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, r1, r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, rsave, x_getcon(tls, qbe, int64(192), fn))
	x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), rsave, _Ref{
		F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(-libc.Int32FromInt32(1)&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
	}, _Ref{})
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(16), fn))
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorew), int32(_Kw), _Ref{}, x_getcon(tls, qbe, libc.Int64FromUint32((p.Fngp-uint32(8))*uint32(8)), fn), r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(24), fn))
	r0 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorew), int32(_Kw), _Ref{}, x_getcon(tls, qbe, libc.Int64FromUint32((p.Fnfp-uint32(8))*uint32(16)), fn), r0)
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r0, ap, x_getcon(tls, qbe, int64(28), fn))
}

func x_arm64_abi(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var b, i, i0, ip, v3, v4, v6 uintptr
	var n int32
	var p _Params
	var _ /* il at bp+0 */ uintptr
	_, _, _, _, _, _, _, _, _ = b, i, i0, ip, n, p, v3, v4, v6
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fvisit = uint32(0)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	/* lower parameters */
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	i = (*_Blk)(unsafe.Pointer(b)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
			break
		}
		if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Opar) <= libc.Uint32FromInt32(int32(_Opare)-int32(_Opar))) {
			break
		}
		goto _2
	_2:
		;
		i += 16
	}
	p = s_selpar1(tls, qbe, fn, (*_Blk)(unsafe.Pointer(b)).Fins, i)
	n = int32(libc.Int64FromUint32((*_Blk)(unsafe.Pointer(b)).Fnins) - (int64(i)-int64((*_Blk)(unsafe.Pointer(b)).Fins))/16 + (___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16)-int64(qbe.x_curi))/16)
	i0 = x_alloc(tls, qbe, libc.Uint64FromInt32(n)*uint64(16))
	v3 = i0
	ip = v3
	ip = x_icpy(tls, qbe, v3, qbe.x_curi, libc.Uint64FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16)-int64(qbe.x_curi))/16))
	ip = x_icpy(tls, qbe, ip, i, libc.Uint64FromInt64((___predefined_ptrdiff_t((*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16)-int64(i))/16))
	(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt32(n)
	(*_Blk)(unsafe.Pointer(b)).Fins = i0
	/* lower calls, returns, and vararg instructions */
	*(*uintptr)(unsafe.Pointer(bp)) = uintptr(0)
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for cond := true; cond; cond = b != (*_Fn)(unsafe.Pointer(fn)).Fstart {
		v4 = (*_Blk)(unsafe.Pointer(b)).Flink
		b = v4
		if !(v4 != 0) {
			b = (*_Fn)(unsafe.Pointer(fn)).Fstart
		} /* do it last */
		if (*_Blk)(unsafe.Pointer(b)).Fvisit != 0 {
			continue
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		s_selret1(tls, qbe, b, fn)
		i = (*_Blk)(unsafe.Pointer(b)).Fins + uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b)).Fins) {
				break
			}
			i -= 16
			v6 = i
			switch int32(*(*uint32)(unsafe.Pointer(v6 + 0)) & 0x3fffffff >> 0) {
			default:
				goto _7
			case int32(_Ocall):
				goto _8
			case int32(_Ovastart):
				goto _9
			case int32(_Ovaarg):
				goto _10
			case int32(_Oargc):
				goto _11
			case int32(_Oarg):
				goto _12
			}
			goto _13
		_7:
			;
			x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(i)))
			goto _13
		_8:
			;
			i0 = i
		_16:
			;
			if !(i0 > (*_Blk)(unsafe.Pointer(b)).Fins) {
				goto _14
			}
			if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i0 - libc.UintptrFromInt32(1)*16 + 0))&0x3fffffff>>0))-uint32(_Oarg) <= libc.Uint32FromInt32(int32(_Oargv)-int32(_Oarg))) {
				goto _14
			}
			goto _15
		_15:
			;
			i0 -= 16
			goto _16
			goto _14
		_14:
			;
			s_selcall1(tls, qbe, fn, i0, i, bp)
			i = i0
			goto _13
		_9:
			;
			if qbe.x_T.Fapple != 0 {
				s_apple_selvastart(tls, qbe, fn, p, *(*_Ref)(unsafe.Pointer(i + 8)))
			} else {
				s_arm64_selvastart(tls, qbe, fn, p, *(*_Ref)(unsafe.Pointer(i + 8)))
			}
			goto _13
		_10:
			;
			if qbe.x_T.Fapple != 0 {
				s_apple_selvaarg(tls, qbe, fn, b, i)
			} else {
				s_arm64_selvaarg(tls, qbe, fn, b, i)
			}
			goto _13
		_12:
			;
		_11:
			;
			_die_(tls, qbe, __ccgo_ts+7513, __ccgo_ts+3532, 0)
		_13:
			;
			goto _5
		_5:
		}
		if b == (*_Fn)(unsafe.Pointer(fn)).Fstart {
			for {
				if !(*(*uintptr)(unsafe.Pointer(bp)) != 0) {
					break
				}
				x_emiti(tls, qbe, (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Fi)
				goto _17
			_17:
				;
				*(*uintptr)(unsafe.Pointer(bp)) = (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Flink
			}
		}
		(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b)).Fnins))
	}
	if qbe.x_debug[int32('A')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+5288, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

// C documentation
//
//	/* abi0 for apple target; introduces
//	 * necessary sign extensions in calls
//	 * and returns
//	 */
func x_apple_extsb(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	var b, i, i0, i1, v3, v5, v7, v9 uintptr
	var j, op int32
	var r _Ref
	_, _, _, _, _, _, _, _, _, _, _ = b, i, i0, i1, j, op, r, v3, v5, v7, v9
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		j = int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)
		if libc.Uint32FromInt32(j)-uint32(_Jretsb) <= libc.Uint32FromInt32(int32(_Jretuh)-int32(_Jretsb)) {
			r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kw), fn)
			op = int32(_Oextsb) + (j - int32(_Jretsb))
			x_emit(tls, qbe, op, int32(_Kw), r, (*_Blk)(unsafe.Pointer(b)).Fjmp.Farg, _Ref{})
			(*_Blk)(unsafe.Pointer(b)).Fjmp.Farg = r
			(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jretw)
		}
		i = (*_Blk)(unsafe.Pointer(b)).Fins + uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16
		for {
			if !(i > (*_Blk)(unsafe.Pointer(b)).Fins) {
				break
			}
			i -= 16
			v3 = i
			x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(v3)))
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) != int32(_Ocall) {
				goto _2
			}
			v5 = i
			i1 = v5
			i0 = v5
			for {
				if !(i0 > (*_Blk)(unsafe.Pointer(b)).Fins) {
					break
				}
				if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i0 - libc.UintptrFromInt32(1)*16 + 0))&0x3fffffff>>0))-uint32(_Oarg) <= libc.Uint32FromInt32(int32(_Oargv)-int32(_Oarg))) {
					break
				}
				goto _4
			_4:
				;
				i0 -= 16
			}
			i = i1
			for {
				if !(i > i0) {
					break
				}
				i -= 16
				v7 = i
				x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(v7)))
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oargsb) <= libc.Uint32FromInt32(int32(_Oarguh)-int32(_Oargsb)) {
					(*_Ins)(unsafe.Pointer(i)).Fto = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
					*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8)) = (*_Ins)(unsafe.Pointer(i)).Fto
				}
				goto _6
			_6:
			}
			i = i1
			for {
				if !(i > i0) {
					break
				}
				i -= 16
				v9 = i
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(v9 + 0))&0x3fffffff>>0))-uint32(_Oargsb) <= libc.Uint32FromInt32(int32(_Oarguh)-int32(_Oargsb)) {
					op = int32(_Oextsb) + (int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) - int32(_Oargsb))
					x_emit(tls, qbe, op, int32(_Kw), (*_Ins)(unsafe.Pointer(i)).Fto, *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
				}
				goto _8
			_8:
			}
			goto _2
		_2:
		}
		(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b)).Fnins))
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	if qbe.x_debug[int32('A')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+7525, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

type _Imm = int32

const _Iother = 0

const _Iplo12 = 1

const _Iphi12 = 2

const _Inlo12 = 4

const _Inhi12 = 5

func s_imm(tls *libc.TLS, qbe *_QBE, c uintptr, k int32, pn uintptr) (r _Imm) {
	var i int32
	var n _int64_t
	_, _ = i, n
	if (*_Con)(unsafe.Pointer(c)).Ftype1 != int32(_CBits) {
		return int32(_Iother)
	}
	n = *(*_int64_t)(unsafe.Pointer(c + 16))
	if k == int32(_Kw) {
		n = int64(int32(n))
	}
	i = int32(_Iplo12)
	if n < 0 {
		i = int32(_Inlo12)
		n = libc.Int64FromUint64(-libc.Uint64FromInt64(n))
	}
	*(*_int64_t)(unsafe.Pointer(pn)) = n
	if n&int64(0x000fff) == n {
		return i
	}
	if n&int64(0xfff000) == n {
		return i + int32(1)
	}
	if n&int64(0xffffff) == n {
		return i + int32(2)
	}
	return int32(_Iother)
}

func x_arm64_logimm(tls *libc.TLS, qbe *_QBE, x _uint64_t, k int32) (r int32) {
	var n _uint64_t
	_ = n
	if k == int32(_Kw) {
		x = x&uint64(0xffffffff) | x<<int32(32)
	}
	if x&uint64(1) != 0 {
		x = ^x
	}
	if x == uint64(0) {
		return 0
	}
	if x == uint64(0xaaaaaaaaaaaaaaaa) {
		return int32(1)
	}
	n = x & uint64(0xf)
	if uint64(0x1111111111111111)*n == x {
		goto Check
	}
	n = x & uint64(0xff)
	if uint64(0x0101010101010101)*n == x {
		goto Check
	}
	n = x & uint64(0xffff)
	if uint64(0x0001000100010001)*n == x {
		goto Check
	}
	n = x & uint64(0xffffffff)
	if uint64(0x0000000100000001)*n == x {
		goto Check
	}
	n = x
	goto Check
Check:
	;
	return libc.BoolInt32(n&(n+n&-n) == uint64(0))
	return r
}

func s_fixarg1(tls *libc.TLS, qbe *_QBE, pr uintptr, k int32, phi int32, fn uintptr) {
	bp := tls.Alloc(112)
	defer tls.Free(112)
	var c, v10 uintptr
	var n, s, v4, v6, v8, v9 int32
	var r1, r2, r3, v1, v2, v3 _Ref
	var _ /* buf at bp+12 */ [32]int8
	var _ /* cc at bp+48 */ _Con
	var _ /* r0 at bp+80 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, n, r1, r2, r3, s, v1, v10, v2, v3, v4, v6, v8, v9
	*(*_Ref)(unsafe.Pointer(bp + 80)) = *(*_Ref)(unsafe.Pointer(pr))
	v1 = *(*_Ref)(unsafe.Pointer(bp + 80))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	switch v6 {
	case int32(_RCon):
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 80 + 0))&0xfffffff8>>3))*32
		if qbe.x_T.Fapple != 0 && (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CAddr) && (*_Con)(unsafe.Pointer(c)).Fsym.Ftype1 == int32(_SThr) {
			r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
			*(*_Ref)(unsafe.Pointer(pr)) = r1
			if *(*_int64_t)(unsafe.Pointer(c + 16)) != 0 {
				r2 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
				*(*_Con)(unsafe.Pointer(bp + 48)) = _Con{
					Ftype1: int32(_CBits),
				}
				*(*_int64_t)(unsafe.Pointer(bp + 48 + 16)) = *(*_int64_t)(unsafe.Pointer(c + 16))
				r3 = x_newcon(tls, qbe, bp+48, fn)
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, r2, r3)
				r1 = r2
			}
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), r1, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R0)&0x1fffffff<<3,
			}, _Ref{})
			r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
			r2 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ocall), 0, _Ref{}, r1, _Ref{
				F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(33)&0x1fffffff<<3,
			})
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R0)&0x1fffffff<<3,
			}, r2, _Ref{})
			x_emit(tls, qbe, int32(_Oload), int32(_Kl), r1, r2, _Ref{})
			*(*_Con)(unsafe.Pointer(bp + 48)) = *(*_Con)(unsafe.Pointer(c))
			*(*_int64_t)(unsafe.Pointer(bp + 48 + 16)) = 0
			r3 = x_newcon(tls, qbe, bp+48, fn)
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), r2, r3, _Ref{})
			break
		}
		if k>>int32(1) == 0 && phi != 0 {
			return
		}
		r1 = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
		if k>>int32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), k, r1, *(*_Ref)(unsafe.Pointer(bp + 80)), _Ref{})
		} else {
			if k&int32(1) != 0 {
				v8 = int32(8)
			} else {
				v8 = int32(4)
			}
			n = x_stashbits(tls, qbe, c+16, v8)
			v10 = fn + 36
			*(*int32)(unsafe.Pointer(v10))++
			v9 = *(*int32)(unsafe.Pointer(v10))
			x_vgrow(tls, qbe, fn+16, libc.Uint64FromInt32(v9))
			c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr((*_Fn)(unsafe.Pointer(fn)).Fncon-int32(1))*32
			libc.X__builtin___sprintf_chk(tls, bp+12, 0, ^___predefined_size_t(0), __ccgo_ts+5342, libc.VaList(bp+96, uintptr(unsafe.Pointer(&qbe.x_T))+136, n))
			*(*_Con)(unsafe.Pointer(c)) = _Con{
				Ftype1: int32(_CAddr),
			}
			(*_Con)(unsafe.Pointer(c)).Fsym.Fid = x_intern(tls, qbe, bp+12)
			r2 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Oload), k, r1, r2, _Ref{})
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), r2, _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt64((int64(c)-int64((*_Fn)(unsafe.Pointer(fn)).Fcon))/32)&0x1fffffff<<3,
			}, _Ref{})
		}
		*(*_Ref)(unsafe.Pointer(pr)) = r1
	case int32(_RTmp):
		s = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 80 + 0))&0xfffffff8>>3))*192))).Fslot
		if s == -int32(1) {
			break
		}
		r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), r1, _Ref{
			F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
		}, _Ref{})
		*(*_Ref)(unsafe.Pointer(pr)) = r1
		break
	}
}

func s_selcmp1(tls *libc.TLS, qbe *_QBE, arg uintptr, k int32, fn uintptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c, iarg uintptr
	var cmp, fix, swap, v11, v13, v4, v6 int32
	var v1, v10, v2, v3, v8, v9 _Ref
	var _ /* n at bp+16 */ _int64_t
	var _ /* r at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, cmp, fix, iarg, swap, v1, v10, v11, v13, v2, v3, v4, v6, v8, v9
	if k>>int32(1) == int32(1) {
		x_emit(tls, qbe, int32(_Oafcmp), k, _Ref{}, *(*_Ref)(unsafe.Pointer(arg)), *(*_Ref)(unsafe.Pointer(arg + 1*4)))
		iarg = qbe.x_curi + 8
		s_fixarg1(tls, qbe, iarg, k, 0, fn)
		s_fixarg1(tls, qbe, iarg+1*4, k, 0, fn)
		return 0
	}
	v1 = *(*_Ref)(unsafe.Pointer(arg))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	swap = libc.BoolInt32(v6 == int32(_RCon))
	if swap != 0 {
		*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(arg + 1*4))
		*(*_Ref)(unsafe.Pointer(arg + 1*4)) = *(*_Ref)(unsafe.Pointer(arg))
		*(*_Ref)(unsafe.Pointer(arg)) = *(*_Ref)(unsafe.Pointer(bp + 12))
	}
	fix = int32(1)
	cmp = int32(_Oacmp)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(arg + 1*4))
	v8 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v8
	v9 = v8
	v10 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v9
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
	v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _12
_12:
	if v11 != 0 {
		v13 = -int32(1)
		goto _14
	}
	v13 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _14
_14:
	if v13 == int32(_RCon) {
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
		switch s_imm(tls, qbe, c, k, bp+16) {
		default:
		case int32(_Iplo12):
			fallthrough
		case int32(_Iphi12):
			fix = 0
		case int32(_Inlo12):
			fallthrough
		case int32(_Inhi12):
			cmp = int32(_Oacmn)
			*(*_Ref)(unsafe.Pointer(bp + 12)) = x_getcon(tls, qbe, *(*_int64_t)(unsafe.Pointer(bp + 16)), fn)
			fix = 0
			break
		}
	}
	x_emit(tls, qbe, cmp, k, _Ref{}, *(*_Ref)(unsafe.Pointer(arg)), *(*_Ref)(unsafe.Pointer(bp + 12)))
	iarg = qbe.x_curi + 8
	s_fixarg1(tls, qbe, iarg, k, 0, fn)
	if fix != 0 {
		s_fixarg1(tls, qbe, iarg+1*4, k, 0, fn)
	}
	return swap
}

func s_callable(tls *libc.TLS, qbe *_QBE, _r _Ref, fn uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp + 12)) = _r
	var c uintptr
	var v1, v10, v2, v3, v8, v9 _Ref
	var v11, v13, v4, v6 int32
	_, _, _, _, _, _, _, _, _, _, _ = c, v1, v10, v11, v13, v2, v3, v4, v6, v8, v9
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RTmp) {
		return int32(1)
	}
	v8 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v8
	v9 = v8
	v10 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v9
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
	v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _12
_12:
	if v11 != 0 {
		v13 = -int32(1)
		goto _14
	}
	v13 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _14
_14:
	if v13 == int32(_RCon) {
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
		if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CAddr) {
			if *(*_int64_t)(unsafe.Pointer(c + 16)) == 0 {
				return int32(1)
			}
		}
	}
	return 0
}

func s_sel1(tls *libc.TLS, qbe *_QBE, _i _Ins, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	*(*_Ins)(unsafe.Pointer(bp)) = _i
	var i0, iarg uintptr
	var _ /* cc at bp+20 */ int32
	var _ /* ck at bp+16 */ int32
	_, _ = i0, iarg
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0))-uint32(_Oalloc) <= libc.Uint32FromInt32(int32(_Oalloc1)-int32(_Oalloc)) {
		i0 = qbe.x_curi - uintptr(1)*16
		x_salloc(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp))).Fto, *(*_Ref)(unsafe.Pointer(bp + 8)), fn)
		s_fixarg1(tls, qbe, i0+8, int32(_Kl), 0, fn)
		return
	}
	if x_iscmp(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0), bp+16, bp+20) != 0 {
		x_emit(tls, qbe, int32(_Oflag), int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), (*(*_Ins)(unsafe.Pointer(bp))).Fto, _Ref{}, _Ref{})
		i0 = qbe.x_curi
		if s_selcmp1(tls, qbe, bp+8, *(*int32)(unsafe.Pointer(bp + 16)), fn) != 0 {
			libc.AssignBitFieldPtr32Int32(i0+0, int32(*(*uint32)(unsafe.Pointer(i0 + 0))&0x3fffffff>>0)+x_cmpop(tls, qbe, *(*int32)(unsafe.Pointer(bp + 20))), 30, 0, 0x3fffffff)
		} else {
			libc.AssignBitFieldPtr32Int32(i0+0, int32(*(*uint32)(unsafe.Pointer(i0 + 0))&0x3fffffff>>0)+*(*int32)(unsafe.Pointer(bp + 20)), 30, 0, 0x3fffffff)
		}
		return
	}
	if int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0) == int32(_Ocall) {
		if s_callable(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 8)), fn) != 0 {
			x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(bp)))
			return
		}
	}
	if int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0) != int32(_Onop) {
		x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(bp)))
		iarg = qbe.x_curi + 8 /* fixarg() can change curi */
		s_fixarg1(tls, qbe, iarg, x_argcls(tls, qbe, bp, 0), 0, fn)
		s_fixarg1(tls, qbe, iarg+1*4, x_argcls(tls, qbe, bp, int32(1)), 0, fn)
	}
}

func s_seljmp1(tls *libc.TLS, qbe *_QBE, b1 uintptr, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i, ir, v1 uintptr
	var use, v4 int32
	var v2, v3 _Ref
	var _ /* cc at bp+24 */ int32
	var _ /* ck at bp+20 */ int32
	var _ /* r at bp+16 */ _Ref
	_, _, _, _, _, _, _ = i, ir, use, v1, v2, v3, v4
	if int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jret0) || int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jjmp) || int32((*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1) == int32(_Jhlt) {
		return
	}
	*(*_Ref)(unsafe.Pointer(bp + 16)) = (*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg
	use = -int32(1)
	(*_Blk)(unsafe.Pointer(b1)).Fjmp.Farg = _Ref{}
	ir = uintptr(0)
	i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
	for i > (*_Blk)(unsafe.Pointer(b1)).Fins {
		i -= 16
		v1 = i
		v2 = (*_Ins)(unsafe.Pointer(v1)).Fto
		v3 = *(*_Ref)(unsafe.Pointer(bp + 16))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v2
		*(*_Ref)(unsafe.Pointer(bp + 12)) = v3
		v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))
		goto _5
	_5:
		if v4 != 0 {
			use = libc.Int32FromUint32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3))*192))).Fnuse)
			ir = i
			break
		}
	}
	if ir != 0 && use == int32(1) && x_iscmp(tls, qbe, int32(*(*uint32)(unsafe.Pointer(ir + 0))&0x3fffffff>>0), bp+20, bp+24) != 0 {
		if s_selcmp1(tls, qbe, ir+8, *(*int32)(unsafe.Pointer(bp + 20)), fn) != 0 {
			*(*int32)(unsafe.Pointer(bp + 24)) = x_cmpop(tls, qbe, *(*int32)(unsafe.Pointer(bp + 24)))
		}
		(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(int32(_Jjf) + *(*int32)(unsafe.Pointer(bp + 24)))
		*(*_Ins)(unsafe.Pointer(ir)) = _Ins{
			F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
		}
	} else {
		*(*[2]_Ref)(unsafe.Pointer(bp)) = [2]_Ref{
			0: *(*_Ref)(unsafe.Pointer(bp + 16)),
			1: {
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt32(1)&0x1fffffff<<3,
			},
		}
		s_selcmp1(tls, qbe, bp, int32(_Kw), fn)
		(*_Blk)(unsafe.Pointer(b1)).Fjmp.Ftype1 = int16(_Jjfine)
	}
}

func x_arm64_isel(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var al, n _uint
	var b1, i, p, sb, v16, p10 uintptr
	var sz _int64_t
	var v3, v4, v5 _Ref
	var v6, v8 int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = al, b1, i, n, p, sb, sz, v16, v3, v4, v5, v6, v8, p10
	/* assign slots to fast allocs */
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	/* specific to NAlign == 3 */ /* or change n=4 and sz /= 4 below */
	al = uint32(_Oalloc)
	n = libc.Uint32FromInt32(4)
	for {
		if !(al <= uint32(_Oalloc1)) {
			break
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)) == al {
				v3 = *(*_Ref)(unsafe.Pointer(i + 8))
				*(*_Ref)(unsafe.Pointer(bp + 32)) = v3
				v4 = v3
				v5 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp + 24)) = v4
				*(*_Ref)(unsafe.Pointer(bp + 28)) = v5
				v6 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))
				goto _7
			_7:
				if v6 != 0 {
					v8 = -int32(1)
					goto _9
				}
				v8 = int32(*(*uint32)(unsafe.Pointer(bp + 32 + 0)) & 0x7 >> 0)
				goto _9
			_9:
				if v8 != int32(_RCon) {
					break
				}
				sz = *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32 + 16))
				if sz < 0 || sz >= int64(libc.Int32FromInt32(m___INT_MAX__)-libc.Int32FromInt32(15)) {
					_err(tls, qbe, __ccgo_ts+194, libc.VaList(bp+48, sz))
				}
				sz = (sz + libc.Int64FromUint32(n) - int64(1)) & libc.Int64FromUint32(-n)
				sz /= int64(4)
				(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192))).Fslot = (*_Fn)(unsafe.Pointer(fn)).Fslot
				p10 = fn + 72
				*(*int32)(unsafe.Pointer(p10)) = int32(int64(*(*int32)(unsafe.Pointer(p10))) + sz)
				*(*_Ins)(unsafe.Pointer(i)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
			}
			goto _2
		_2:
			;
			i += 16
		}
		goto _1
	_1:
		;
		al++
		n *= uint32(2)
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		*(*[3]uintptr)(unsafe.Pointer(bp)) = [3]uintptr{
			0: (*_Blk)(unsafe.Pointer(b1)).Fs1,
			1: (*_Blk)(unsafe.Pointer(b1)).Fs2,
		}
		sb = bp
		for {
			if !(*(*uintptr)(unsafe.Pointer(sb)) != 0) {
				break
			}
			p = (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(sb)))).Fphi
			for {
				if !(p != 0) {
					break
				}
				n = uint32(0)
				for {
					if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8)) != b1) {
						break
					}
					goto _14
				_14:
					;
					n++
				}
				s_fixarg1(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(n)*4, (*_Phi)(unsafe.Pointer(p)).Fcls, int32(1), fn)
				goto _13
			_13:
				;
				p = (*_Phi)(unsafe.Pointer(p)).Flink
			}
			goto _12
		_12:
			;
			sb += 8
		}
		s_seljmp1(tls, qbe, b1, fn)
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b1)).Fins) {
				break
			}
			i -= 16
			v16 = i
			s_sel1(tls, qbe, *(*_Ins)(unsafe.Pointer(v16)), fn)
			goto _15
		_15:
		}
		(*_Blk)(unsafe.Pointer(b1)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b1+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b1)).Fnins))
		goto _11
	_11:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	if qbe.x_debug[int32('I')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+5429, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

type _E2 = struct {
	Ff       uintptr
	Ffn      uintptr
	Fframe   _uint64_t
	Fpadding _uint
}

func s_rname1(tls *libc.TLS, qbe *_QBE, r int32, k int32) (r1 uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	if r == int32(_SP) {
		libc.X__builtin___sprintf_chk(tls, uintptr(unsafe.Pointer(&qbe.s_buf1)), 0, ^___predefined_size_t(0), __ccgo_ts+6737, 0)
	} else {
		if int32(_R0) <= r && r <= int32(_LR) {
			switch k {
			default:
				_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+8636, 0)
				fallthrough
			case int32(_Kw):
				libc.X__builtin___sprintf_chk(tls, uintptr(unsafe.Pointer(&qbe.s_buf1)), 0, ^___predefined_size_t(0), __ccgo_ts+8650, libc.VaList(bp+8, r-int32(_R0)))
			case int32(_Kx):
				fallthrough
			case int32(_Kl):
				libc.X__builtin___sprintf_chk(tls, uintptr(unsafe.Pointer(&qbe.s_buf1)), 0, ^___predefined_size_t(0), __ccgo_ts+8654, libc.VaList(bp+8, r-int32(_R0)))
				break
			}
		} else {
			if int32(_V0) <= r && r <= int32(_V30) {
				switch k {
				default:
					_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+8636, 0)
					fallthrough
				case int32(_Ks):
					libc.X__builtin___sprintf_chk(tls, uintptr(unsafe.Pointer(&qbe.s_buf1)), 0, ^___predefined_size_t(0), __ccgo_ts+8658, libc.VaList(bp+8, r-int32(_V0)))
				case int32(_Kx):
					fallthrough
				case int32(_Kd):
					libc.X__builtin___sprintf_chk(tls, uintptr(unsafe.Pointer(&qbe.s_buf1)), 0, ^___predefined_size_t(0), __ccgo_ts+8662, libc.VaList(bp+8, r-int32(_V0)))
					break
				}
			} else {
				_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+8666, 0)
			}
		}
	}
	return uintptr(unsafe.Pointer(&qbe.s_buf1))
}

func s_slot3(tls *libc.TLS, qbe *_QBE, r1 _Ref, e uintptr) (r _uint64_t) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var s, v1 int32
	_, _ = s, v1
	*(*_Ref)(unsafe.Pointer(bp)) = r1
	v1 = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
	goto _2
_2:
	s = v1
	if s == -int32(1) {
		return uint64(16) + (*_E2)(unsafe.Pointer(e)).Fframe
	}
	if s < 0 {
		if (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fvararg != 0 && !(qbe.x_T.Fapple != 0) {
			return uint64(16) + (*_E2)(unsafe.Pointer(e)).Fframe + uint64(192) - libc.Uint64FromInt32(s+libc.Int32FromInt32(2))
		} else {
			return uint64(16) + (*_E2)(unsafe.Pointer(e)).Fframe - libc.Uint64FromInt32(s+libc.Int32FromInt32(2))
		}
	} else {
		return uint64(uint32(16) + (*_E2)(unsafe.Pointer(e)).Fpadding + libc.Uint32FromInt32(int32(4)*s))
	}
	return r
}

func s_emitf1(tls *libc.TLS, qbe *_QBE, s uintptr, i uintptr, e uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c, k, v11, v13, v2, v20, v22, v4 int32
	var n _uint64_t
	var pc, v15, v3, v5, v6 uintptr
	var sp _uint
	var v10, v16, v17, v18, v19, v7, v8, v9 _Ref
	var _ /* r at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, k, n, pc, sp, v10, v11, v13, v15, v16, v17, v18, v19, v2, v20, v22, v3, v4, v5, v6, v7, v8, v9
	fputc(tls, qbe, int32('\t'), (*_E2)(unsafe.Pointer(e)).Ff)
	sp = uint32(0)
	for {
		k = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30)
		for {
			v3 = s
			s++
			v2 = int32(*(*int8)(unsafe.Pointer(v3)))
			c = v2
			if !(v2 != int32('%')) {
				break
			}
			if c == int32(' ') && !(sp != 0) {
				fputc(tls, qbe, int32('\t'), (*_E2)(unsafe.Pointer(e)).Ff)
				sp = uint32(1)
			} else {
				if !(c != 0) {
					fputc(tls, qbe, int32('\n'), (*_E2)(unsafe.Pointer(e)).Ff)
					return
				} else {
					fputc(tls, qbe, c, (*_E2)(unsafe.Pointer(e)).Ff)
				}
			}
		}
		goto Switch
	Switch:
		;
		v5 = s
		s++
		v4 = int32(*(*int8)(unsafe.Pointer(v5)))
		c = v4
		switch v4 {
		default:
			_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+8683, 0)
			fallthrough
		case int32('W'):
			k = int32(_Kw)
			goto Switch
		case int32('L'):
			k = int32(_Kl)
			goto Switch
		case int32('S'):
			k = int32(_Ks)
			goto Switch
		case int32('D'):
			k = int32(_Kd)
			goto Switch
		case int32('?'):
			if k>>int32(1) == 0 {
				fputs(tls, qbe, s_rname1(tls, qbe, int32(_R18), k), (*_E2)(unsafe.Pointer(e)).Ff)
			} else {
				if k == int32(_Ks) {
					v6 = __ccgo_ts + 8698
				} else {
					v6 = __ccgo_ts + 8702
				}
				fputs(tls, qbe, v6, (*_E2)(unsafe.Pointer(e)).Ff)
			}
		case int32('='):
			fallthrough
		case int32('0'):
			if c == int32('=') {
				v7 = (*_Ins)(unsafe.Pointer(i)).Fto
			} else {
				v7 = *(*_Ref)(unsafe.Pointer(i + 8))
			}
			*(*_Ref)(unsafe.Pointer(bp + 12)) = v7
			fputs(tls, qbe, s_rname1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3), k), (*_E2)(unsafe.Pointer(e)).Ff)
		case int32('1'):
			*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
			v8 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v8
			v9 = v8
			v10 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v9
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
			v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _12
		_12:
			if v11 != 0 {
				v13 = -int32(1)
				goto _14
			}
			v13 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _14
		_14:
			switch v13 {
			default:
				_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+8706, 0)
				fallthrough
			case int32(_RTmp):
				fputs(tls, qbe, s_rname1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3), k), (*_E2)(unsafe.Pointer(e)).Ff)
			case int32(_RCon):
				pc = (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
				n = libc.Uint64FromInt64(*(*_int64_t)(unsafe.Pointer(pc + 16)))
				if n>>int32(24) != 0 {
					fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+8730, libc.VaList(bp+24, n))
				} else {
					if n&uint64(0xfff000) != 0 {
						fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+8736, libc.VaList(bp+24, n>>int32(12)))
					} else {
						fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+8730, libc.VaList(bp+24, n))
					}
				}
				break
			}
		case int32('M'):
			v15 = s
			s++
			c = int32(*(*int8)(unsafe.Pointer(v15)))
			if c == int32('=') {
				v16 = (*_Ins)(unsafe.Pointer(i)).Fto
			} else {
				v16 = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(c-int32('0'))*4))
			}
			*(*_Ref)(unsafe.Pointer(bp + 12)) = v16
			v17 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v17
			v18 = v17
			v19 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v18
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v19
			v20 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _21
		_21:
			if v20 != 0 {
				v22 = -int32(1)
				goto _23
			}
			v22 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _23
		_23:
			switch v22 {
			default:
				_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+8751, 0)
				fallthrough
			case int32(_RTmp):
				fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+8782, libc.VaList(bp+24, s_rname1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3), int32(_Kl))))
			case int32(_RSlot):
				fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+8787, libc.VaList(bp+24, s_slot3(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), e)))
				break
			}
			break
		}
		goto _1
	_1:
	}
}

func s_loadaddr(tls *libc.TLS, qbe *_QBE, c uintptr, rn uintptr, e uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var l, p, s, v1 uintptr
	_, _, _, _ = l, p, s, v1
	switch (*_Con)(unsafe.Pointer(c)).Fsym.Ftype1 {
	default:
		_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+3532, 0)
		fallthrough
	case int32(_SGlo):
		if qbe.x_T.Fapple != 0 {
			s = __ccgo_ts + 8799
		} else {
			s = __ccgo_ts + 8839
		}
	case int32(_SThr):
		if qbe.x_T.Fapple != 0 {
			s = __ccgo_ts + 8873
		} else {
			s = __ccgo_ts + 8921
		}
		break
	}
	l = x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid)
	if int32(*(*int8)(unsafe.Pointer(l))) == int32('"') {
		v1 = __ccgo_ts + 4511
	} else {
		v1 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
	}
	p = v1
	for {
		if !(*(*int8)(unsafe.Pointer(s)) != 0) {
			break
		}
		switch int32(*(*int8)(unsafe.Pointer(s))) {
		default:
			fputc(tls, qbe, int32(*(*int8)(unsafe.Pointer(s))), (*_E2)(unsafe.Pointer(e)).Ff)
		case int32('R'):
			fputs(tls, qbe, rn, (*_E2)(unsafe.Pointer(e)).Ff)
		case int32('S'):
			fputs(tls, qbe, p, (*_E2)(unsafe.Pointer(e)).Ff)
			fputs(tls, qbe, l, (*_E2)(unsafe.Pointer(e)).Ff)
		case int32('O'):
			if *(*_int64_t)(unsafe.Pointer(c + 16)) != 0 {
				/* todo, handle large offsets */
				fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9006, libc.VaList(bp+8, *(*_int64_t)(unsafe.Pointer(c + 16))))
			}
			break
		}
		goto _2
	_2:
		;
		s++
	}
}

func s_loadcon(tls *libc.TLS, qbe *_QBE, c uintptr, r int32, k int32, e uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var n _int64_t
	var rn uintptr
	var sh, w int32
	_, _, _, _ = n, rn, sh, w
	w = k & int32(1)
	rn = s_rname1(tls, qbe, r, k)
	n = *(*_int64_t)(unsafe.Pointer(c + 16))
	if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CAddr) {
		rn = s_rname1(tls, qbe, r, int32(_Kl))
		s_loadaddr(tls, qbe, c, rn, e)
		return
	}
	if !(w != 0) {
		n = int64(int32(n))
	}
	if n|int64(0xffff) == int64(-int32(1)) || x_arm64_logimm(tls, qbe, libc.Uint64FromInt64(n), k) != 0 {
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9012, libc.VaList(bp+8, rn, n))
	} else {
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9028, libc.VaList(bp+8, rn, int32(n&libc.Int64FromInt32(0xffff))))
		sh = int32(16)
		for {
			n >>= int64(16)
			if !(n != 0) {
				break
			}
			if !(w != 0) && sh == int32(32) || sh == int32(64) {
				break
			}
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9042, libc.VaList(bp+8, rn, libc.Uint32FromInt64(n&libc.Int64FromInt32(0xffff)), sh))
			goto _1
		_1:
			;
			sh += int32(16)
		}
	}
}

func s_fixarg2(tls *libc.TLS, qbe *_QBE, pr uintptr, sz int32, e uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i uintptr
	var r1, v1, v2, v3 _Ref
	var s _uint64_t
	var v4, v6 int32
	_, _, _, _, _, _, _, _ = i, r1, s, v1, v2, v3, v4, v6
	r1 = *(*_Ref)(unsafe.Pointer(pr))
	v1 = r1
	*(*_Ref)(unsafe.Pointer(bp + 24)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp + 16)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 20)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 20 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 16 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 20 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RSlot) {
		s = s_slot3(tls, qbe, r1, e)
		if s > uint64(libc.Uint32FromInt32(sz)*uint32(4095)) {
			*(*_Ins)(unsafe.Pointer(bp)) = _Ins{
				F__ccgo0: uint32(_Oaddr)&0x3fffffff<<0 | uint32(_Kl)&0x3<<30,
				Fto: _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_IP0)&0x1fffffff<<3,
				},
				Farg: [2]_Ref{
					0: r1,
				},
			}
			i = bp
			s_emitins1(tls, qbe, i, e)
			*(*_Ref)(unsafe.Pointer(pr)) = _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_IP0)&0x1fffffff<<3,
			}
		}
	}
}

func s_emitins1(tls *libc.TLS, qbe *_QBE, i uintptr, e uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var c, l, p, rn, v35 uintptr
	var o, v12, v17, v19, v24, v26, v31, v33, v38 int32
	var r1, v10, v11, v14, v15, v16, v21, v22, v23, v28, v29, v30, v36, v37 _Ref
	var s _uint64_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, l, o, p, r1, rn, s, v10, v11, v12, v14, v15, v16, v17, v19, v21, v22, v23, v24, v26, v28, v29, v30, v31, v33, v35, v36, v37, v38
	switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
	default:
		goto _1
	case int32(_Onop):
		goto _2
	case int32(_Ocopy):
		goto _3
	case int32(_Oaddr):
		goto _4
	case int32(_Ocall):
		goto _5
	case int32(_Osalloc):
		goto _6
	case int32(_Odbgloc):
		goto _7
	}
	goto _8
_1:
	;
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) {
		s_fixarg2(tls, qbe, i+8, x_loadsz(tls, qbe, i), e)
	}
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
		s_fixarg2(tls, qbe, i+8+1*4, x_storesz(tls, qbe, i), e)
	}
	goto Table
Table:
	;
	/* most instructions are just pulled out of
	 * the table omap[], some special cases are
	 * detailed below */
	o = 0
	for {
		/* this linear search should really be a binary
		 * search */
		if int32(qbe.s_omap1[o].Fop) == int32(_NOp) {
			_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+6898, libc.VaList(bp+24, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname, int32(*(*int8)(unsafe.Pointer(__ccgo_ts + 6918 + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)))))))
		}
		if int32(qbe.s_omap1[o].Fop) == int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) {
			if int32(qbe.s_omap1[o].Fcls) == int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) || int32(qbe.s_omap1[o].Fcls) == int32(_Ka) || int32(qbe.s_omap1[o].Fcls) == int32(_Ki) && int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
				break
			}
		}
		goto _9
	_9:
		;
		o++
	}
	s_emitf1(tls, qbe, qbe.s_omap1[o].Ffmt, i, e)
	goto _8
_2:
	;
	goto _8
_3:
	;
	v10 = (*_Ins)(unsafe.Pointer(i)).Fto
	v11 = *(*_Ref)(unsafe.Pointer(i + 8))
	*(*_Ref)(unsafe.Pointer(bp)) = v10
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v11
	v12 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _13
_13:
	if v12 != 0 {
		goto _8
	}
	v14 = (*_Ins)(unsafe.Pointer(i)).Fto
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v14
	v15 = v14
	v16 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v15
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v16
	v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _18
_18:
	if v17 != 0 {
		v19 = -int32(1)
		goto _20
	}
	v19 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _20
_20:
	if v19 == int32(_RSlot) {
		r1 = (*_Ins)(unsafe.Pointer(i)).Fto
		if !(x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8))) != 0) {
			(*_Ins)(unsafe.Pointer(i)).Fto = _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_R18)&0x1fffffff<<3,
			}
			s_emitins1(tls, qbe, i, e)
			*(*_Ref)(unsafe.Pointer(i + 8)) = (*_Ins)(unsafe.Pointer(i)).Fto
		}
		libc.SetBitFieldPtr32Uint32(i+0, libc.Uint32FromInt32(int32(_Ostorew)+int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)), 0, 0x3fffffff)
		libc.SetBitFieldPtr32Uint32(i+0, uint32(_Kw), 30, 0xc0000000)
		*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = r1
		s_emitins1(tls, qbe, i, e)
		goto _8
	}
	v21 = *(*_Ref)(unsafe.Pointer(i + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v21
	v22 = v21
	v23 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v22
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v23
	v24 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _25
_25:
	if v24 != 0 {
		v26 = -int32(1)
		goto _27
	}
	v26 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _27
_27:
	switch v26 {
	case int32(_RCon):
		c = (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32
		s_loadcon(tls, qbe, c, int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), e)
	case int32(_RSlot):
		libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oload), 0, 0x3fffffff)
		s_emitins1(tls, qbe, i, e)
	default:
		goto Table
	}
	goto _8
_4:
	;
	rn = s_rname1(tls, qbe, int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3), int32(_Kl))
	s = s_slot3(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), e)
	if s <= uint64(4095) {
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9068, libc.VaList(bp+24, rn, s))
	} else {
		if s <= uint64(65535) {
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9089, libc.VaList(bp+24, rn, s, rn, rn))
		} else {
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9122, libc.VaList(bp+24, rn, s&uint64(0xFFFF), rn, s>>int32(16), rn, rn))
		}
	}
	goto _8
_5:
	;
	v28 = *(*_Ref)(unsafe.Pointer(i + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v28
	v29 = v28
	v30 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v29
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v30
	v31 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _32
_32:
	if v31 != 0 {
		v33 = -int32(1)
		goto _34
	}
	v33 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _34
_34:
	if v33 != int32(_RCon) {
		goto Table
	}
	c = (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32
	if (*_Con)(unsafe.Pointer(c)).Ftype1 != int32(_CAddr) || (*_Con)(unsafe.Pointer(c)).Fsym.Ftype1 != int32(_SGlo) || *(*_int64_t)(unsafe.Pointer(c + 16)) != 0 {
		_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+7146, 0)
	}
	l = x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid)
	if int32(*(*int8)(unsafe.Pointer(l))) == int32('"') {
		v35 = __ccgo_ts + 4511
	} else {
		v35 = uintptr(unsafe.Pointer(&qbe.x_T)) + 140
	}
	p = v35
	fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9180, libc.VaList(bp+24, p, l))
	goto _8
_6:
	;
	s_emitf1(tls, qbe, __ccgo_ts+9190, i, e)
	v36 = (*_Ins)(unsafe.Pointer(i)).Fto
	v37 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v36
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v37
	v38 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _39
_39:
	if !(v38 != 0) {
		s_emitf1(tls, qbe, __ccgo_ts+9205, i, e)
	}
	goto _8
_7:
	;
	x_emitdbgloc(tls, qbe, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3)), libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 8 + 1*4 + 0))&0xfffffff8>>3)), (*_E2)(unsafe.Pointer(e)).Ff)
	goto _8
_8:
}

func s_framelayout(tls *libc.TLS, qbe *_QBE, e uintptr) {
	var f _uint64_t
	var o _uint
	var r uintptr
	_, _, _ = f, o, r
	o = uint32(0)
	r = uintptr(unsafe.Pointer(&qbe.x_arm64_rclob))
	for {
		if !(*(*int32)(unsafe.Pointer(r)) >= 0) {
			break
		}
		o = _uint(uint64(o) + libc.Uint64FromInt32(1)&((*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Freg>>*(*int32)(unsafe.Pointer(r))))
		goto _1
	_1:
		;
		r += 4
	}
	f = libc.Uint64FromInt32((*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fslot)
	f = (f + uint64(3)) & libc.Uint64FromInt32(-libc.Int32FromInt32(4))
	o += o & uint32(1)
	(*_E2)(unsafe.Pointer(e)).Fpadding = uint32(uint64(4) * (f - libc.Uint64FromInt32((*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fslot)))
	(*_E2)(unsafe.Pointer(e)).Fframe = uint64(4)*f + uint64(uint32(8)*o)
}

/*

  Stack-frame layout:

  +=============+
  | varargs     |
  |  save area  |
  +-------------+
  | callee-save |  ^
  |  registers  |  |
  +-------------+  |
  |    ...      |  |
  | spill slots |  |
  |    ...      |  | e->frame
  +-------------+  |
  |    ...      |  |
  |   locals    |  |
  |    ...      |  |
  +-------------+  |
  | e->padding  |  v
  +-------------+
  |  saved x29  |
  |  saved x30  |
  +=============+ <- x29

*/

func x_arm64_emitfn(tls *libc.TLS, qbe *_QBE, fn uintptr, out uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var b, e, i, r, t uintptr
	var c, lbl, n, s, v13, v4 int32
	var o _uint64_t
	_, _, _, _, _, _, _, _, _, _, _, _ = b, c, e, i, lbl, n, o, r, s, t, v13, v4
	*(*_E2)(unsafe.Pointer(bp)) = _E2{
		Ff:  out,
		Ffn: fn,
	}
	e = bp
	if qbe.x_T.Fapple != 0 {
		(*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Flnk.Falign = int8(4)
	}
	x_emitfnlnk(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ffn+83, (*_E2)(unsafe.Pointer(e)).Ffn+168, (*_E2)(unsafe.Pointer(e)).Ff)
	fputs(tls, qbe, __ccgo_ts+9249, (*_E2)(unsafe.Pointer(e)).Ff)
	s_framelayout(tls, qbe, e)
	if (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fvararg != 0 && !(qbe.x_T.Fapple != 0) {
		n = int32(7)
		for {
			if !(n >= 0) {
				break
			}
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9260, libc.VaList(bp+72, n))
			goto _1
		_1:
			;
			n--
		}
		n = int32(7)
		for {
			if !(n >= 0) {
				break
			}
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9282, libc.VaList(bp+72, n-int32(1), n))
			goto _2
		_2:
			;
			n -= int32(2)
		}
	}
	if (*_E2)(unsafe.Pointer(e)).Fframe+uint64(16) <= uint64(512) {
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9309, libc.VaList(bp+72, (*_E2)(unsafe.Pointer(e)).Fframe+uint64(16)))
	} else {
		if (*_E2)(unsafe.Pointer(e)).Fframe <= uint64(4095) {
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9338, libc.VaList(bp+72, (*_E2)(unsafe.Pointer(e)).Fframe))
		} else {
			if (*_E2)(unsafe.Pointer(e)).Fframe <= uint64(65535) {
				fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9384, libc.VaList(bp+72, (*_E2)(unsafe.Pointer(e)).Fframe))
			} else {
				fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9444, libc.VaList(bp+72, (*_E2)(unsafe.Pointer(e)).Fframe&uint64(0xFFFF), (*_E2)(unsafe.Pointer(e)).Fframe>>int32(16)))
			}
		}
	}
	fputs(tls, qbe, __ccgo_ts+9530, (*_E2)(unsafe.Pointer(e)).Ff)
	s = libc.Int32FromUint64(((*_E2)(unsafe.Pointer(e)).Fframe - uint64((*_E2)(unsafe.Pointer(e)).Fpadding)) / uint64(4))
	r = uintptr(unsafe.Pointer(&qbe.x_arm64_rclob))
	for {
		if !(*(*int32)(unsafe.Pointer(r)) >= 0) {
			break
		}
		if (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(r))) != 0 {
			s -= int32(2)
			*(*_Ins)(unsafe.Pointer(bp + 32)) = _Ins{
				Farg: [2]_Ref{
					0: {
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(r)))&0x1fffffff<<3,
					},
					1: {
						F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
					},
				},
			}
			i = bp + 32
			if *(*int32)(unsafe.Pointer(r)) >= int32(_V0) {
				v4 = int32(_Ostored)
			} else {
				v4 = int32(_Ostorel)
			}
			libc.SetBitFieldPtr32Uint32(i+0, libc.Uint32FromInt32(v4), 0, 0x3fffffff)
			s_emitins1(tls, qbe, i, e)
		}
		goto _3
	_3:
		;
		r += 4
	}
	lbl = 0
	b = (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if lbl != 0 || (*_Blk)(unsafe.Pointer(b)).Fnpred > uint32(1) {
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9544, libc.VaList(bp+72, uintptr(unsafe.Pointer(&qbe.x_T))+136, libc.Uint32FromInt32(qbe.s_id01)+(*_Blk)(unsafe.Pointer(b)).Fid))
		}
		i = (*_Blk)(unsafe.Pointer(b)).Fins
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
				break
			}
			s_emitins1(tls, qbe, i, e)
			goto _6
		_6:
			;
			i += 16
		}
		lbl = int32(1)
		switch int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) {
		case int32(_Jhlt):
			goto _7
		case int32(_Jret0):
			goto _8
		case int32(_Jjmp):
			goto _9
		default:
			goto _10
		}
		goto _11
	_7:
		;
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9551, 0)
		goto _11
	_8:
		;
		s = libc.Int32FromUint64(((*_E2)(unsafe.Pointer(e)).Fframe - uint64((*_E2)(unsafe.Pointer(e)).Fpadding)) / uint64(4))
		r = uintptr(unsafe.Pointer(&qbe.x_arm64_rclob))
		for {
			if !(*(*int32)(unsafe.Pointer(r)) >= 0) {
				break
			}
			if (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(r))) != 0 {
				s -= int32(2)
				*(*_Ins)(unsafe.Pointer(bp + 48)) = _Ins{
					F__ccgo0: uint32(_Oload)&0x3fffffff<<0 | libc.Uint32FromInt32(0)&0x3<<30,
					Fto: _Ref{
						F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(r)))&0x1fffffff<<3,
					},
					Farg: [2]_Ref{
						0: {
							F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
						},
					},
				}
				i = bp + 48
				if *(*int32)(unsafe.Pointer(r)) >= int32(_V0) {
					v13 = int32(_Kd)
				} else {
					v13 = int32(_Kl)
				}
				libc.SetBitFieldPtr32Uint32(i+0, libc.Uint32FromInt32(v13), 30, 0xc0000000)
				s_emitins1(tls, qbe, i, e)
			}
			goto _12
		_12:
			;
			r += 4
		}
		if (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fdynalloc != 0 {
			fputs(tls, qbe, __ccgo_ts+9563, (*_E2)(unsafe.Pointer(e)).Ff)
		}
		o = (*_E2)(unsafe.Pointer(e)).Fframe + uint64(16)
		if (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fvararg != 0 && !(qbe.x_T.Fapple != 0) {
			o += uint64(192)
		}
		if o <= uint64(504) {
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9577, libc.VaList(bp+72, o))
		} else {
			if o-uint64(16) <= uint64(4095) {
				fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9604, libc.VaList(bp+72, o-uint64(16)))
			} else {
				if o-uint64(16) <= uint64(65535) {
					fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9648, libc.VaList(bp+72, o-uint64(16)))
				} else {
					fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9706, libc.VaList(bp+72, (o-uint64(16))&uint64(0xFFFF), (o-uint64(16))>>int32(16)))
				}
			}
		}
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+7420, 0)
		goto _11
	_9:
		;
		goto Jmp
	Jmp:
		;
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != (*_Blk)(unsafe.Pointer(b)).Flink {
			fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9790, libc.VaList(bp+72, uintptr(unsafe.Pointer(&qbe.x_T))+136, libc.Uint32FromInt32(qbe.s_id01)+(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs1)).Fid))
		} else {
			lbl = 0
		}
		goto _11
	_10:
		;
		c = int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) - int32(_Jjf)
		if c < 0 || c > int32(_NCmp) {
			_die_(tls, qbe, __ccgo_ts+8623, __ccgo_ts+7452, libc.VaList(bp+72, int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)))
		}
		if (*_Blk)(unsafe.Pointer(b)).Flink == (*_Blk)(unsafe.Pointer(b)).Fs2 {
			t = (*_Blk)(unsafe.Pointer(b)).Fs1
			(*_Blk)(unsafe.Pointer(b)).Fs1 = (*_Blk)(unsafe.Pointer(b)).Fs2
			(*_Blk)(unsafe.Pointer(b)).Fs2 = t
		} else {
			c = x_cmpneg(tls, qbe, c)
		}
		fprintf(tls, qbe, (*_E2)(unsafe.Pointer(e)).Ff, __ccgo_ts+9799, libc.VaList(bp+72, qbe.s_ctoa1[c], uintptr(unsafe.Pointer(&qbe.x_T))+136, libc.Uint32FromInt32(qbe.s_id01)+(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs2)).Fid))
		goto Jmp
	_11:
		;
		goto _5
	_5:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	qbe.s_id01 = int32(uint32(qbe.s_id01) + (*_Fn)(unsafe.Pointer((*_E2)(unsafe.Pointer(e)).Ffn)).Fnblk)
	if !(qbe.x_T.Fapple != 0) {
		x_elf_emitfnfin(tls, qbe, fn+83, out)
	}
}

type _Rv64Op = struct {
	Fimm int8
}

const _T0 = 1

const _T1 = 2

const _T2 = 3

const _T3 = 4

const _T4 = 5

const _T5 = 6

const _A0 = 7

const _A1 = 8

const _A2 = 9

const _A3 = 10

const _A4 = 11

const _A5 = 12

const _A6 = 13

const _A7 = 14

const _S1 = 15

const _S2 = 16

const _S3 = 17

const _S4 = 18

const _S5 = 19

const _S6 = 20

const _S7 = 21

const _S8 = 22

const _S9 = 23

const _S10 = 24

const _S11 = 25

const _FP1 = 26

const _SP1 = 27

const _GP = 28

const _TP = 29

const _RA = 30

const _FT0 = 31

const _FT1 = 32

const _FT2 = 33

const _FT3 = 34

const _FT4 = 35

const _FT5 = 36

const _FT6 = 37

const _FT7 = 38

const _FT8 = 39

const _FT9 = 40

const _FT10 = 41

const _FA0 = 42

const _FA1 = 43

const _FA2 = 44

const _FA3 = 45

const _FA4 = 46

const _FA5 = 47

const _FA6 = 48

const _FA7 = 49

const _FS0 = 50

const _FS1 = 51

const _FS2 = 52

const _FS3 = 53

const _FS4 = 54

const _FS5 = 55

const _FS6 = 56

const _FS7 = 57

const _FS8 = 58

const _FS9 = 59

const _FS10 = 60

const _FS11 = 61

const _T6 = 62

const _NGPR2 = 30

const _NGPS2 = 14

const _NFPS2 = 19

func s_rv64_memargs(tls *libc.TLS, qbe *_QBE, op int32) (r int32) {
	_ = op
	return 0
}

func (qbe *_QBE) init5() {
	p := unsafe.Pointer(&qbe.x_T_rv64)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(x_rv64_retregs)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(x_rv64_argregs)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(s_rv64_memargs)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(x_elimsb)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(x_rv64_abi)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(x_rv64_isel)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(x_rv64_emitfn)
	*(*uintptr)(unsafe.Add(p, 128)) = __ccgo_fp(x_elf_emitfin)
}

/* the risc-v lp64d abi */

type _Class2 = struct {
	Fclass int8
	Ftype1 uintptr
	Freg   [2]int32
	Fcls   [2]int32
	Foff   [2]int32
	Fngp   int8
	Fnfp   int8
	Fnreg  int8
}

type _Params2 = struct {
	Fngp int32
	Fnfp int32
	Fstk int32
}

const _Cptr1 = 1

const /* replaced by a pointer */
_Cstk11 = 2

const /* pass first XLEN on the stack */
_Cstk2 = 4

const /* pass second XLEN on the stack */
_Cstk1 = 6

const _Cfpint = 8

/* layout of call's second argument (RCall)
 *
 *  29   12    8    4  2  0
 *  |0.00|x|xxxx|xxxx|xx|xx|                  range
 *        |   |    |  |  ` gp regs returned (0..2)
 *        |   |    |  ` fp regs returned    (0..2)
 *        |   |    ` gp regs passed         (0..8)
 *        |    ` fp regs passed             (0..8)
 *        ` env pointer passed in t5        (0..1)
 */

func x_rv64_retregs(tls *libc.TLS, qbe *_QBE, _r _Ref, p uintptr) (r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	var b _bits
	var nfp, ngp, v1, v2 int32
	_, _, _, _, _ = b, nfp, ngp, v1, v2
	ngp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) & int32(3)
	nfp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(2) & int32(3)
	if p != 0 {
		*(*int32)(unsafe.Pointer(p)) = ngp
		*(*int32)(unsafe.Pointer(p + 1*4)) = nfp
	}
	b = uint64(0)
	for {
		v1 = ngp
		ngp--
		if !(v1 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_A0) + ngp)
	}
	for {
		v2 = nfp
		nfp--
		if !(v2 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_FA0) + nfp)
	}
	return b
}

func x_rv64_argregs(tls *libc.TLS, qbe *_QBE, _r _Ref, p uintptr) (r _bits) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ref)(unsafe.Pointer(bp)) = _r
	var b _bits
	var nfp, ngp, t5, v1, v2 int32
	_, _, _, _, _, _ = b, nfp, ngp, t5, v1, v2
	ngp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(4) & int32(15)
	nfp = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(8) & int32(15)
	t5 = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) >> libc.Int32FromInt32(12) & int32(1)
	if p != 0 {
		*(*int32)(unsafe.Pointer(p)) = ngp + t5
		*(*int32)(unsafe.Pointer(p + 1*4)) = nfp
	}
	b = uint64(0)
	for {
		v1 = ngp
		ngp--
		if !(v1 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_A0) + ngp)
	}
	for {
		v2 = nfp
		nfp--
		if !(v2 != 0) {
			break
		}
		b |= libc.Uint64FromInt32(1) << (int32(_FA0) + nfp)
	}
	return b | libc.Uint64FromInt32(t5)<<int32(_T5)
}

func s_fpstruct(tls *libc.TLS, qbe *_QBE, t uintptr, off int32, c uintptr) (r int32) {
	var f uintptr
	var n int32
	_, _ = f, n
	if (*_Typ)(unsafe.Pointer(t)).Fisunion != 0 {
		return -int32(1)
	}
	f = (*_Typ)(unsafe.Pointer(t)).Ffields
	for {
		if !((*_Field)(unsafe.Pointer(f)).Ftype1 != int32(_FEnd)) {
			break
		}
		if (*_Field)(unsafe.Pointer(f)).Ftype1 == int32(_FPad) {
			off = int32(uint32(off) + (*_Field)(unsafe.Pointer(f)).Flen1)
		} else {
			if (*_Field)(unsafe.Pointer(f)).Ftype1 == int32(_FTyp) {
				if s_fpstruct(tls, qbe, qbe.x_typ+uintptr((*_Field)(unsafe.Pointer(f)).Flen1)*112, off, c) == -int32(1) {
					return -int32(1)
				}
			} else {
				n = int32((*_Class2)(unsafe.Pointer(c)).Fnfp) + int32((*_Class2)(unsafe.Pointer(c)).Fngp)
				if n == int32(2) {
					return -int32(1)
				}
				switch (*_Field)(unsafe.Pointer(f)).Ftype1 {
				default:
					_die_(tls, qbe, __ccgo_ts+9810, __ccgo_ts+3532, 0)
					fallthrough
				case int32(_Fb):
					fallthrough
				case int32(_Fh):
					fallthrough
				case int32(_Fw):
					*(*int32)(unsafe.Pointer(c + 24 + uintptr(n)*4)) = int32(_Kw)
					(*_Class2)(unsafe.Pointer(c)).Fngp++
				case int32(_Fl):
					*(*int32)(unsafe.Pointer(c + 24 + uintptr(n)*4)) = int32(_Kl)
					(*_Class2)(unsafe.Pointer(c)).Fngp++
				case int32(_Fs):
					*(*int32)(unsafe.Pointer(c + 24 + uintptr(n)*4)) = int32(_Ks)
					(*_Class2)(unsafe.Pointer(c)).Fnfp++
				case int32(_Fd):
					*(*int32)(unsafe.Pointer(c + 24 + uintptr(n)*4)) = int32(_Kd)
					(*_Class2)(unsafe.Pointer(c)).Fnfp++
					break
				}
				*(*int32)(unsafe.Pointer(c + 32 + uintptr(n)*4)) = off
				off = int32(uint32(off) + (*_Field)(unsafe.Pointer(f)).Flen1)
			}
		}
		goto _1
	_1:
		;
		f += 8
	}
	return int32((*_Class2)(unsafe.Pointer(c)).Fnfp)
}

func s_typclass2(tls *libc.TLS, qbe *_QBE, c uintptr, t uintptr, fpabi int32, gp uintptr, fp uintptr) {
	var i int32
	var n _uint
	var v4, v5, p1 uintptr
	_, _, _, _, _ = i, n, v4, v5, p1
	(*_Class2)(unsafe.Pointer(c)).Ftype1 = t
	(*_Class2)(unsafe.Pointer(c)).Fclass = 0
	(*_Class2)(unsafe.Pointer(c)).Fngp = 0
	(*_Class2)(unsafe.Pointer(c)).Fnfp = 0
	if (*_Typ)(unsafe.Pointer(t)).Falign > int32(4) {
		_err(tls, qbe, __ccgo_ts+9821, 0)
	}
	if (*_Typ)(unsafe.Pointer(t)).Fisdark != 0 || (*_Typ)(unsafe.Pointer(t)).Fsize > uint64(16) || (*_Typ)(unsafe.Pointer(t)).Fsize == uint64(0) {
		/* large structs are replaced by a
		 * pointer to some caller-allocated
		 * memory
		 */
		p1 = c
		*(*int8)(unsafe.Pointer(p1)) = int8(int32(*(*int8)(unsafe.Pointer(p1))) | int32(_Cptr1))
		*(*int32)(unsafe.Pointer(c + 24)) = int32(_Kl)
		*(*int32)(unsafe.Pointer(c + 32)) = 0
		(*_Class2)(unsafe.Pointer(c)).Fngp = int8(1)
	} else {
		if !(fpabi != 0) || s_fpstruct(tls, qbe, t, 0, c) <= 0 {
			n = uint32(0)
			for {
				if !(uint64(uint32(8)*n) < (*_Typ)(unsafe.Pointer(t)).Fsize) {
					break
				}
				*(*int32)(unsafe.Pointer(c + 24 + uintptr(n)*4)) = int32(_Kl)
				*(*int32)(unsafe.Pointer(c + 32 + uintptr(n)*4)) = libc.Int32FromUint32(uint32(8) * n)
				goto _2
			_2:
				;
				n++
			}
			(*_Class2)(unsafe.Pointer(c)).Fnfp = 0
			(*_Class2)(unsafe.Pointer(c)).Fngp = libc.Int8FromUint32(n)
		}
	}
	(*_Class2)(unsafe.Pointer(c)).Fnreg = int8(int32((*_Class2)(unsafe.Pointer(c)).Fnfp) + int32((*_Class2)(unsafe.Pointer(c)).Fngp))
	i = 0
	for {
		if !(i < int32((*_Class2)(unsafe.Pointer(c)).Fnreg)) {
			break
		}
		if *(*int32)(unsafe.Pointer(c + 24 + uintptr(i)*4))>>int32(1) == 0 {
			v4 = gp
			gp += 4
			*(*int32)(unsafe.Pointer(c + 16 + uintptr(i)*4)) = *(*int32)(unsafe.Pointer(v4))
		} else {
			v5 = fp
			fp += 4
			*(*int32)(unsafe.Pointer(c + 16 + uintptr(i)*4)) = *(*int32)(unsafe.Pointer(v5))
		}
		goto _3
	_3:
		;
		i++
	}
}

func s_sttmps1(tls *libc.TLS, qbe *_QBE, tmp uintptr, ntmp int32, c uintptr, mem _Ref, fn uintptr) {
	var i int32
	var r _Ref
	_, _ = i, r
	i = 0
	for {
		if !(i < ntmp) {
			break
		}
		*(*_Ref)(unsafe.Pointer(tmp + uintptr(i)*4)) = x_newtmp(tls, qbe, __ccgo_ts+5196, *(*int32)(unsafe.Pointer(c + 24 + uintptr(i)*4)), fn)
		r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		x_emit(tls, qbe, qbe.s_st[*(*int32)(unsafe.Pointer(c + 24 + uintptr(i)*4))], 0, _Ref{}, *(*_Ref)(unsafe.Pointer(tmp + uintptr(i)*4)), r)
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, mem, x_getcon(tls, qbe, int64(*(*int32)(unsafe.Pointer(c + 32 + uintptr(i)*4))), fn))
		goto _1
	_1:
		;
		i++
	}
}

func s_ldregs1(tls *libc.TLS, qbe *_QBE, c uintptr, mem _Ref, fn uintptr) {
	var i int32
	var r _Ref
	_, _ = i, r
	i = 0
	for {
		if !(i < int32((*_Class2)(unsafe.Pointer(c)).Fnreg)) {
			break
		}
		r = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
		x_emit(tls, qbe, int32(_Oload), *(*int32)(unsafe.Pointer(c + 24 + uintptr(i)*4)), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 16 + uintptr(i)*4)))&0x1fffffff<<3,
		}, r, _Ref{})
		x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r, mem, x_getcon(tls, qbe, int64(*(*int32)(unsafe.Pointer(c + 32 + uintptr(i)*4))), fn))
		goto _1
	_1:
		;
		i++
	}
}

func s_selret2(tls *libc.TLS, qbe *_QBE, b uintptr, fn uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var cty, j, k int32
	var r _Ref
	var _ /* cr at bp+0 */ _Class2
	_, _, _, _ = cty, j, k, r
	j = int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1)
	if !(libc.Uint32FromInt32(j)-uint32(_Jretw) <= libc.Uint32FromInt32(int32(_Jret0)-int32(_Jretw))) || j == int32(_Jret0) {
		return
	}
	r = (*_Blk)(unsafe.Pointer(b)).Fjmp.Farg
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1 = int16(_Jret0)
	if j == int32(_Jretc) {
		s_typclass2(tls, qbe, bp, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112, int32(1), uintptr(unsafe.Pointer(&qbe.s_gpreg1)), uintptr(unsafe.Pointer(&qbe.s_fpreg1)))
		if int32((*(*_Class2)(unsafe.Pointer(bp))).Fclass)&int32(_Cptr1) != 0 {
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | uint32((*_Typ)(unsafe.Pointer((*(*_Class2)(unsafe.Pointer(bp))).Ftype1)).Fsize&libc.Uint64FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, r, (*_Fn)(unsafe.Pointer(fn)).Fretr)
			cty = 0
		} else {
			s_ldregs1(tls, qbe, bp, r, fn)
			cty = int32((*(*_Class2)(unsafe.Pointer(bp))).Fnfp)<<int32(2) | int32((*(*_Class2)(unsafe.Pointer(bp))).Fngp)
		}
	} else {
		k = j - int32(_Jretw)
		if k>>int32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_A0)&0x1fffffff<<3,
			}, r, _Ref{})
			cty = int32(1)
		} else {
			x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_FA0)&0x1fffffff<<3,
			}, r, _Ref{})
			cty = libc.Int32FromInt32(1) << libc.Int32FromInt32(2)
		}
	}
	(*_Blk)(unsafe.Pointer(b)).Fjmp.Farg = _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(cty)&0x1fffffff<<3,
	}
}

func s_argsclass2(tls *libc.TLS, qbe *_QBE, i0 uintptr, i1 uintptr, carg uintptr, retptr int32) (r int32) {
	var c, fp, gp, i, t, v2, v4, p3, p5, p6, p7, p8 uintptr
	var envc, nfp, ngp, vararg int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, envc, fp, gp, i, nfp, ngp, t, vararg, v2, v4, p3, p5, p6, p7, p8
	gp = uintptr(unsafe.Pointer(&qbe.s_gpreg1))
	fp = uintptr(unsafe.Pointer(&qbe.s_fpreg1))
	ngp = int32(8)
	nfp = int32(8)
	vararg = 0
	envc = 0
	if retptr != 0 {
		gp += 4
		ngp--
	}
	i = i0
	c = carg
	for {
		if !(i < i1) {
			break
		}
		switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
		case int32(_Opar):
			fallthrough
		case int32(_Oarg):
			*(*int32)(unsafe.Pointer(c + 24)) = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30)
			if !(vararg != 0) && int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == int32(1) && nfp > 0 {
				nfp--
				v2 = fp
				fp += 4
				*(*int32)(unsafe.Pointer(c + 16)) = *(*int32)(unsafe.Pointer(v2))
			} else {
				if ngp > 0 {
					if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == int32(1) {
						p3 = c
						*(*int8)(unsafe.Pointer(p3)) = int8(int32(*(*int8)(unsafe.Pointer(p3))) | int32(_Cfpint))
					}
					ngp--
					v4 = gp
					gp += 4
					*(*int32)(unsafe.Pointer(c + 16)) = *(*int32)(unsafe.Pointer(v4))
				} else {
					p5 = c
					*(*int8)(unsafe.Pointer(p5)) = int8(int32(*(*int8)(unsafe.Pointer(p5))) | int32(_Cstk11))
				}
			}
		case int32(_Oargv):
			vararg = int32(1)
		case int32(_Oparc):
			fallthrough
		case int32(_Oargc):
			t = qbe.x_typ + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*112
			s_typclass2(tls, qbe, c, t, int32(1), gp, fp)
			if int32((*_Class2)(unsafe.Pointer(c)).Fnfp) > 0 {
				if int32((*_Class2)(unsafe.Pointer(c)).Fnfp) >= nfp || int32((*_Class2)(unsafe.Pointer(c)).Fngp) >= ngp {
					s_typclass2(tls, qbe, c, t, 0, gp, fp)
				}
			}
			if int32((*_Class2)(unsafe.Pointer(c)).Fngp) <= ngp {
				ngp -= int32((*_Class2)(unsafe.Pointer(c)).Fngp)
				nfp -= int32((*_Class2)(unsafe.Pointer(c)).Fnfp)
				gp += uintptr((*_Class2)(unsafe.Pointer(c)).Fngp) * 4
				fp += uintptr((*_Class2)(unsafe.Pointer(c)).Fnfp) * 4
			} else {
				if ngp > 0 {
					p6 = c
					*(*int8)(unsafe.Pointer(p6)) = int8(int32(*(*int8)(unsafe.Pointer(p6))) | int32(_Cstk2))
					(*_Class2)(unsafe.Pointer(c)).Fnreg = int8(1)
					ngp--
					gp += 4
				} else {
					p7 = c
					*(*int8)(unsafe.Pointer(p7)) = int8(int32(*(*int8)(unsafe.Pointer(p7))) | int32(_Cstk11))
					if int32((*_Class2)(unsafe.Pointer(c)).Fnreg) > int32(1) {
						p8 = c
						*(*int8)(unsafe.Pointer(p8)) = int8(int32(*(*int8)(unsafe.Pointer(p8))) | int32(_Cstk2))
					}
					(*_Class2)(unsafe.Pointer(c)).Fnreg = 0
				}
			}
		case int32(_Opare):
			fallthrough
		case int32(_Oarge):
			*(*int32)(unsafe.Pointer(c + 16)) = int32(_T5)
			*(*int32)(unsafe.Pointer(c + 24)) = int32(_Kl)
			envc = int32(1)
			break
		}
		goto _1
	_1:
		;
		i += 16
		c += 48
	}
	return int32(int64(envc<<int32(12)) | (int64(gp)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.s_gpreg1))))/4<<int32(4) | (int64(fp)-___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.s_fpreg1))))/4<<int32(8))
}

func s_stkblob1(tls *libc.TLS, qbe *_QBE, r _Ref, t uintptr, fn uintptr, ilp uintptr) {
	var al int32
	var il uintptr
	var sz _uint64_t
	_, _, _ = al, il, sz
	il = x_alloc(tls, qbe, uint64(24))
	al = (*_Typ)(unsafe.Pointer(t)).Falign - int32(2) /* specific to NAlign == 3 */
	if al < 0 {
		al = 0
	}
	sz = ((*_Typ)(unsafe.Pointer(t)).Fsize + uint64(7)) & libc.Uint64FromInt32(^libc.Int32FromInt32(7))
	(*_Insl)(unsafe.Pointer(il)).Fi = _Ins{
		F__ccgo0: libc.Uint32FromInt32(int32(_Oalloc)+al)&0x3fffffff<<0 | uint32(_Kl)&0x3<<30,
		Fto:      r,
		Farg: [2]_Ref{
			0: x_getcon(tls, qbe, libc.Int64FromUint64(sz), fn),
		},
	}
	(*_Insl)(unsafe.Pointer(il)).Flink = *(*uintptr)(unsafe.Pointer(ilp))
	*(*uintptr)(unsafe.Pointer(ilp)) = il
}

func s_selcall2(tls *libc.TLS, qbe *_QBE, fn uintptr, i0 uintptr, i1 uintptr, ilp uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var c, ca, i uintptr
	var cty, j, k, v12, v14, v3, v8 int32
	var off, stk _uint64_t
	var r1, r2, v1, v2, v6, v7 _Ref
	var _ /* cr at bp+8 */ _Class2
	var _ /* r at bp+56 */ _Ref
	var _ /* tmp at bp+64 */ [2]_Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, ca, cty, i, j, k, off, r1, r2, stk, v1, v12, v14, v2, v3, v6, v7, v8
	ca = x_alloc(tls, qbe, libc.Uint64FromInt64((int64(i1)-int64(i0))/16)*uint64(48))
	(*(*_Class2)(unsafe.Pointer(bp + 8))).Fclass = 0
	v1 = *(*_Ref)(unsafe.Pointer(i1 + 8 + 1*4))
	v2 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v1
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v2
	v3 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _4
_4:
	if !(v3 != 0) {
		s_typclass2(tls, qbe, bp+8, qbe.x_typ+uintptr(int32(*(*uint32)(unsafe.Pointer(i1 + 8 + 1*4 + 0))&0xfffffff8>>3))*112, int32(1), uintptr(unsafe.Pointer(&qbe.s_gpreg1)), uintptr(unsafe.Pointer(&qbe.s_fpreg1)))
	}
	cty = s_argsclass2(tls, qbe, i0, i1, ca, int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fclass)&int32(_Cptr1))
	stk = uint64(0)
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargv) {
			goto _5
		}
		if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cptr1) != 0 {
			*(*_Ref)(unsafe.Pointer(i + 8)) = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			s_stkblob1(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), (*_Class2)(unsafe.Pointer(c)).Ftype1, fn, ilp)
			libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oarg), 0, 0x3fffffff)
		}
		if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk11) != 0 {
			stk += uint64(8)
		}
		if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk2) != 0 {
			stk += uint64(8)
		}
		goto _5
	_5:
		;
		i += 16
		c += 48
	}
	stk += stk & uint64(15)
	if stk != 0 {
		x_emit(tls, qbe, int32(_Osalloc), int32(_Kl), _Ref{}, x_getcon(tls, qbe, libc.Int64FromUint64(-stk), fn), _Ref{})
	}
	v6 = *(*_Ref)(unsafe.Pointer(i1 + 8 + 1*4))
	v7 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v6
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v7
	v8 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _9
_9:
	if !(v8 != 0) {
		s_stkblob1(tls, qbe, (*_Ins)(unsafe.Pointer(i1)).Fto, (*(*_Class2)(unsafe.Pointer(bp + 8))).Ftype1, fn, ilp)
		cty |= int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fnfp)<<int32(2) | int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fngp)
		if int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fclass)&int32(_Cptr1) != 0 {
			/* spill & rega expect calls to be
			 * followed by copies from regs,
			 * so we emit a dummy
			 */
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kw), _Ref{}, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_A0)&0x1fffffff<<3,
			}, _Ref{})
		} else {
			s_sttmps1(tls, qbe, bp+64, int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fnreg), bp+8, (*_Ins)(unsafe.Pointer(i1)).Fto, fn)
			j = 0
			for {
				if !(j < int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fnreg)) {
					break
				}
				*(*_Ref)(unsafe.Pointer(bp + 56)) = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 8 + 16 + uintptr(j)*4)))&0x1fffffff<<3,
				}
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(bp + 8 + 24 + uintptr(j)*4)), (*(*[2]_Ref)(unsafe.Pointer(bp + 64)))[j], *(*_Ref)(unsafe.Pointer(bp + 56)), _Ref{})
				goto _10
			_10:
				;
				j++
			}
		}
	} else {
		if int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_A0)&0x1fffffff<<3,
			}, _Ref{})
			cty |= int32(1)
		} else {
			x_emit(tls, qbe, int32(_Ocopy), int32(*(*uint32)(unsafe.Pointer(i1 + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_FA0)&0x1fffffff<<3,
			}, _Ref{})
			cty |= libc.Int32FromInt32(1) << libc.Int32FromInt32(2)
		}
	}
	x_emit(tls, qbe, int32(_Ocall), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i1 + 8)), _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(cty)&0x1fffffff<<3,
	})
	if int32((*(*_Class2)(unsafe.Pointer(bp + 8))).Fclass)&int32(_Cptr1) != 0 {
		/* struct return argument */
		x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), _Ref{
			F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_A0)&0x1fffffff<<3,
		}, (*_Ins)(unsafe.Pointer(i1)).Fto, _Ref{})
	}
	/* move arguments into registers */
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargv) || int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk11) != 0 {
			goto _11
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargc) {
			s_ldregs1(tls, qbe, c, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), fn)
		} else {
			if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cfpint) != 0 {
				if *(*int32)(unsafe.Pointer(c + 24))&int32(1) != 0 {
					v12 = int32(_Kl)
				} else {
					v12 = int32(_Kw)
				}
				k = v12
				*(*_Ref)(unsafe.Pointer(bp + 56)) = x_newtmp(tls, qbe, __ccgo_ts+5196, k, fn)
				x_emit(tls, qbe, int32(_Ocopy), k, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 16)))&0x1fffffff<<3,
				}, *(*_Ref)(unsafe.Pointer(bp + 56)), _Ref{})
				*(*int32)(unsafe.Pointer(c + 16)) = int32(*(*uint32)(unsafe.Pointer(bp + 56 + 0)) & 0xfffffff8 >> 3)
			} else {
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(c + 24)), _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 16)))&0x1fffffff<<3,
				}, *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
			}
		}
		goto _11
	_11:
		;
		i += 16
		c += 48
	}
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cfpint) != 0 {
			if *(*int32)(unsafe.Pointer(c + 24))&int32(1) != 0 {
				v14 = int32(_Kl)
			} else {
				v14 = int32(_Kw)
			}
			k = v14
			x_emit(tls, qbe, int32(_Ocast), k, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 16)))&0x1fffffff<<3,
			}, *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
		}
		if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cptr1) != 0 {
			x_emit(tls, qbe, int32(_Oblit1), 0, _Ref{}, _Ref{
				F__ccgo0: uint32(_RInt)&0x7<<0 | uint32((*_Typ)(unsafe.Pointer((*_Class2)(unsafe.Pointer(c)).Ftype1)).Fsize&libc.Uint64FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			x_emit(tls, qbe, int32(_Oblit0), 0, _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), *(*_Ref)(unsafe.Pointer(i + 8)))
		}
		goto _13
	_13:
		;
		i += 16
		c += 48
	}
	if !(stk != 0) {
		return
	}
	/* populate the stack */
	off = uint64(0)
	*(*_Ref)(unsafe.Pointer(bp + 56)) = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargv) || !(int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk1) != 0) {
			goto _15
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oarg) {
			r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ostorew)+int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), int32(_Kw), _Ref{}, *(*_Ref)(unsafe.Pointer(i + 8)), r1)
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) == int32(_Kw) {
				/* TODO: we only need this sign
				 * extension for l temps passed
				 * as w arguments
				 * (see rv64/isel.c:fixarg)
				 */
				libc.SetBitFieldPtr32Uint32(qbe.x_curi+0, uint32(_Ostorel), 0, 0x3fffffff)
				*(*_Ref)(unsafe.Pointer(qbe.x_curi + 8)) = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Oextsw), int32(_Kl), *(*_Ref)(unsafe.Pointer(qbe.x_curi + 8)), *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
			}
			x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(bp + 56)), x_getcon(tls, qbe, libc.Int64FromUint64(off), fn))
			off += uint64(8)
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oargc) {
			if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk11) != 0 {
				r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				r2 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, r2, r1)
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(bp + 56)), x_getcon(tls, qbe, libc.Int64FromUint64(off), fn))
				x_emit(tls, qbe, int32(_Oload), int32(_Kl), r2, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), _Ref{})
				off += uint64(8)
			}
			if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk2) != 0 {
				r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				r2 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Ostorel), 0, _Ref{}, r2, r1)
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(bp + 56)), x_getcon(tls, qbe, libc.Int64FromUint64(off), fn))
				r1 = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
				x_emit(tls, qbe, int32(_Oload), int32(_Kl), r2, r1, _Ref{})
				x_emit(tls, qbe, int32(_Oadd), int32(_Kl), r1, *(*_Ref)(unsafe.Pointer(i + 8 + 1*4)), x_getcon(tls, qbe, int64(8), fn))
				off += uint64(8)
			}
		}
		goto _15
	_15:
		;
		i += 16
		c += 48
	}
	x_emit(tls, qbe, int32(_Osalloc), int32(_Kl), *(*_Ref)(unsafe.Pointer(bp + 56)), x_getcon(tls, qbe, libc.Int64FromUint64(stk), fn), _Ref{})
}

func s_selpar2(tls *libc.TLS, qbe *_QBE, fn uintptr, i0 uintptr, i1 uintptr) (r1 _Params2) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var c, ca, i, t, v7 uintptr
	var cty, j, k, nt, s, v2, v5 int32
	var r _Ref
	var _ /* cr at bp+0 */ _Class2
	var _ /* il at bp+48 */ uintptr
	var _ /* tmp at bp+56 */ [17]_Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _ = c, ca, cty, i, j, k, nt, r, s, t, v2, v5, v7
	ca = x_alloc(tls, qbe, libc.Uint64FromInt64((int64(i1)-int64(i0))/16)*uint64(48))
	(*(*_Class2)(unsafe.Pointer(bp))).Fclass = 0
	qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
	if (*_Fn)(unsafe.Pointer(fn)).Fretty >= 0 {
		s_typclass2(tls, qbe, bp, qbe.x_typ+uintptr((*_Fn)(unsafe.Pointer(fn)).Fretty)*112, int32(1), uintptr(unsafe.Pointer(&qbe.s_gpreg1)), uintptr(unsafe.Pointer(&qbe.s_fpreg1)))
		if int32((*(*_Class2)(unsafe.Pointer(bp))).Fclass)&int32(_Cptr1) != 0 {
			(*_Fn)(unsafe.Pointer(fn)).Fretr = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
			x_emit(tls, qbe, int32(_Ocopy), int32(_Kl), (*_Fn)(unsafe.Pointer(fn)).Fretr, _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_A0)&0x1fffffff<<3,
			}, _Ref{})
		}
	}
	cty = s_argsclass2(tls, qbe, i0, i1, ca, int32((*(*_Class2)(unsafe.Pointer(bp))).Fclass)&int32(_Cptr1))
	(*_Fn)(unsafe.Pointer(fn)).Freg = x_rv64_argregs(tls, qbe, _Ref{
		F__ccgo0: uint32(_RCall)&0x7<<0 | libc.Uint32FromInt32(cty)&0x1fffffff<<3,
	}, uintptr(0))
	*(*uintptr)(unsafe.Pointer(bp + 48)) = uintptr(0)
	t = bp + 56
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cfpint) != 0 {
			r = (*_Ins)(unsafe.Pointer(i)).Fto
			k = *(*int32)(unsafe.Pointer(c + 24))
			if k&int32(1) != 0 {
				v2 = int32(_Kl)
			} else {
				v2 = int32(_Kw)
			}
			*(*int32)(unsafe.Pointer(c + 24)) = v2
			(*_Ins)(unsafe.Pointer(i)).Fto = x_newtmp(tls, qbe, __ccgo_ts+5196, k, fn)
			x_emit(tls, qbe, int32(_Ocast), k, r, (*_Ins)(unsafe.Pointer(i)).Fto, _Ref{})
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oparc) {
			if !(int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cptr1) != 0) {
				if int32((*_Class2)(unsafe.Pointer(c)).Fnreg) != 0 {
					nt = int32((*_Class2)(unsafe.Pointer(c)).Fnreg)
					if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk2) != 0 {
						*(*int32)(unsafe.Pointer(c + 24 + 1*4)) = int32(_Kl)
						*(*int32)(unsafe.Pointer(c + 32 + 1*4)) = int32(8)
						nt = int32(2)
					}
					s_sttmps1(tls, qbe, t, nt, c, (*_Ins)(unsafe.Pointer(i)).Fto, fn)
					s_stkblob1(tls, qbe, (*_Ins)(unsafe.Pointer(i)).Fto, (*_Class2)(unsafe.Pointer(c)).Ftype1, fn, bp+48)
					t += uintptr(nt) * 4
				}
			}
		}
		goto _1
	_1:
		;
		i += 16
		c += 48
	}
	for {
		if !(*(*uintptr)(unsafe.Pointer(bp + 48)) != 0) {
			break
		}
		x_emiti(tls, qbe, (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 48)))).Fi)
		goto _3
	_3:
		;
		*(*uintptr)(unsafe.Pointer(bp + 48)) = (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 48)))).Flink
	}
	t = bp + 56
	s = int32(2) + int32(8)*int32((*_Fn)(unsafe.Pointer(fn)).Fvararg)
	i = i0
	c = ca
	for {
		if !(i < i1) {
			break
		}
		if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == int32(_Oparc) && !(int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cptr1) != 0) {
			if int32((*_Class2)(unsafe.Pointer(c)).Fnreg) == 0 {
				(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192))).Fslot = -s
				if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk2) != 0 {
					v5 = int32(2)
				} else {
					v5 = int32(1)
				}
				s += v5
				goto _4
			}
			j = 0
			for {
				if !(j < int32((*_Class2)(unsafe.Pointer(c)).Fnreg)) {
					break
				}
				r = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 16 + uintptr(j)*4)))&0x1fffffff<<3,
				}
				v7 = t
				t += 4
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(c + 24 + uintptr(j)*4)), *(*_Ref)(unsafe.Pointer(v7)), r, _Ref{})
				goto _6
			_6:
				;
				j++
			}
			if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk2) != 0 {
				x_emit(tls, qbe, int32(_Oload), int32(_Kl), *(*_Ref)(unsafe.Pointer(t)), _Ref{
					F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(-s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
				}, _Ref{})
				t += 4
				s++
			}
		} else {
			if int32((*_Class2)(unsafe.Pointer(c)).Fclass)&int32(_Cstk11) != 0 {
				x_emit(tls, qbe, int32(_Oload), *(*int32)(unsafe.Pointer(c + 24)), (*_Ins)(unsafe.Pointer(i)).Fto, _Ref{
					F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(-s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
				}, _Ref{})
				s++
			} else {
				x_emit(tls, qbe, int32(_Ocopy), *(*int32)(unsafe.Pointer(c + 24)), (*_Ins)(unsafe.Pointer(i)).Fto, _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(c + 16)))&0x1fffffff<<3,
				}, _Ref{})
			}
		}
		goto _4
	_4:
		;
		i += 16
		c += 48
	}
	return _Params2{
		Fngp: cty >> int32(4) & int32(15),
		Fnfp: cty >> int32(8) & int32(15),
		Fstk: s,
	}
}

func s_selvaarg1(tls *libc.TLS, qbe *_QBE, fn uintptr, i uintptr) {
	var loc, newloc _Ref
	_, _ = loc, newloc
	loc = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	newloc = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, newloc, *(*_Ref)(unsafe.Pointer(i + 8)))
	x_emit(tls, qbe, int32(_Oadd), int32(_Kl), newloc, loc, x_getcon(tls, qbe, int64(8), fn))
	x_emit(tls, qbe, int32(_Oload), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), (*_Ins)(unsafe.Pointer(i)).Fto, loc, _Ref{})
	x_emit(tls, qbe, int32(_Oload), int32(_Kl), loc, *(*_Ref)(unsafe.Pointer(i + 8)), _Ref{})
}

func s_selvastart1(tls *libc.TLS, qbe *_QBE, fn uintptr, p _Params2, ap _Ref) {
	var rsave _Ref
	var s, v1 int32
	_, _, _ = rsave, s, v1
	rsave = x_newtmp(tls, qbe, __ccgo_ts+5196, int32(_Kl), fn)
	x_emit(tls, qbe, int32(_Ostorel), int32(_Kw), _Ref{}, rsave, ap)
	if p.Fstk > int32(2)+int32(8)*int32((*_Fn)(unsafe.Pointer(fn)).Fvararg) {
		v1 = p.Fstk
	} else {
		v1 = int32(2) + p.Fngp
	}
	s = v1
	x_emit(tls, qbe, int32(_Oaddr), int32(_Kl), rsave, _Ref{
		F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(-s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
	}, _Ref{})
}

func x_rv64_abi(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var b, i, i0, ip, v3, v4, v6 uintptr
	var n int32
	var p _Params2
	var _ /* il at bp+0 */ uintptr
	_, _, _, _, _, _, _, _, _ = b, i, i0, ip, n, p, v3, v4, v6
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		(*_Blk)(unsafe.Pointer(b)).Fvisit = uint32(0)
		goto _1
	_1:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	/* lower parameters */
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	i = (*_Blk)(unsafe.Pointer(b)).Fins
	for {
		if !(i < (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
			break
		}
		if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Opar) <= libc.Uint32FromInt32(int32(_Opare)-int32(_Opar))) {
			break
		}
		goto _2
	_2:
		;
		i += 16
	}
	p = s_selpar2(tls, qbe, fn, (*_Blk)(unsafe.Pointer(b)).Fins, i)
	n = int32(libc.Int64FromUint32((*_Blk)(unsafe.Pointer(b)).Fnins) - (int64(i)-int64((*_Blk)(unsafe.Pointer(b)).Fins))/16 + (___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16)-int64(qbe.x_curi))/16)
	i0 = x_alloc(tls, qbe, libc.Uint64FromInt32(n)*uint64(16))
	v3 = i0
	ip = v3
	ip = x_icpy(tls, qbe, v3, qbe.x_curi, libc.Uint64FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16)-int64(qbe.x_curi))/16))
	ip = x_icpy(tls, qbe, ip, i, libc.Uint64FromInt64((___predefined_ptrdiff_t((*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16)-int64(i))/16))
	(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt32(n)
	(*_Blk)(unsafe.Pointer(b)).Fins = i0
	/* lower calls, returns, and vararg instructions */
	*(*uintptr)(unsafe.Pointer(bp)) = uintptr(0)
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for cond := true; cond; cond = b != (*_Fn)(unsafe.Pointer(fn)).Fstart {
		v4 = (*_Blk)(unsafe.Pointer(b)).Flink
		b = v4
		if !(v4 != 0) {
			b = (*_Fn)(unsafe.Pointer(fn)).Fstart
		} /* do it last */
		if (*_Blk)(unsafe.Pointer(b)).Fvisit != 0 {
			continue
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		s_selret2(tls, qbe, b, fn)
		i = (*_Blk)(unsafe.Pointer(b)).Fins + uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b)).Fins) {
				break
			}
			i -= 16
			v6 = i
			switch int32(*(*uint32)(unsafe.Pointer(v6 + 0)) & 0x3fffffff >> 0) {
			default:
				goto _7
			case int32(_Ocall):
				goto _8
			case int32(_Ovastart):
				goto _9
			case int32(_Ovaarg):
				goto _10
			case int32(_Oargc):
				goto _11
			case int32(_Oarg):
				goto _12
			}
			goto _13
		_7:
			;
			x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(i)))
			goto _13
		_8:
			;
			i0 = i
		_16:
			;
			if !(i0 > (*_Blk)(unsafe.Pointer(b)).Fins) {
				goto _14
			}
			if !(libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i0 - libc.UintptrFromInt32(1)*16 + 0))&0x3fffffff>>0))-uint32(_Oarg) <= libc.Uint32FromInt32(int32(_Oargv)-int32(_Oarg))) {
				goto _14
			}
			goto _15
		_15:
			;
			i0 -= 16
			goto _16
			goto _14
		_14:
			;
			s_selcall2(tls, qbe, fn, i0, i, bp)
			i = i0
			goto _13
		_9:
			;
			s_selvastart1(tls, qbe, fn, p, *(*_Ref)(unsafe.Pointer(i + 8)))
			goto _13
		_10:
			;
			s_selvaarg1(tls, qbe, fn, i)
			goto _13
		_12:
			;
		_11:
			;
			_die_(tls, qbe, __ccgo_ts+9810, __ccgo_ts+3532, 0)
		_13:
			;
			goto _5
		_5:
		}
		if b == (*_Fn)(unsafe.Pointer(fn)).Fstart {
			for {
				if !(*(*uintptr)(unsafe.Pointer(bp)) != 0) {
					break
				}
				x_emiti(tls, qbe, (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Fi)
				goto _17
			_17:
				;
				*(*uintptr)(unsafe.Pointer(bp)) = (*_Insl)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))).Flink
			}
		}
		(*_Blk)(unsafe.Pointer(b)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b)).Fnins))
	}
	if qbe.x_debug[int32('A')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+5288, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

func s_memarg(tls *libc.TLS, qbe *_QBE, r uintptr, op int32, i uintptr) (r1 int32) {
	if libc.Uint32FromInt32(op)-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) || op == int32(_Ocall) {
		return libc.BoolInt32(r == i+8)
	}
	if libc.Uint32FromInt32(op)-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
		return libc.BoolInt32(r == i+8+1*4)
	}
	return 0
}

func s_immarg(tls *libc.TLS, qbe *_QBE, r uintptr, op int32, i uintptr) (r1 int32) {
	return libc.BoolInt32(qbe.x_rv64_op[op].Fimm != 0 && r == i+8+1*4)
}

func s_fixarg3(tls *libc.TLS, qbe *_QBE, r1 uintptr, k int32, i uintptr, fn uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var c, v12 uintptr
	var n, op, s, v10, v11, v2, v6, v8 int32
	var r11, v1, v3, v4, v5 _Ref
	var _ /* buf at bp+12 */ [32]int8
	var _ /* r0 at bp+44 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, n, op, r11, s, v1, v10, v11, v12, v2, v3, v4, v5, v6, v8
	v1 = *(*_Ref)(unsafe.Pointer(r1))
	r11 = v1
	*(*_Ref)(unsafe.Pointer(bp + 44)) = v1
	if i != 0 {
		v2 = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0)
	} else {
		v2 = int32(_Ocopy)
	}
	op = v2
	v3 = *(*_Ref)(unsafe.Pointer(bp + 44))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v3
	v4 = v3
	v5 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v4
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v5
	v6 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _7
_7:
	if v6 != 0 {
		v8 = -int32(1)
		goto _9
	}
	v8 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _9
_9:
	switch v8 {
	case int32(_RCon):
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 44 + 0))&0xfffffff8>>3))*32
		if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CAddr) && s_memarg(tls, qbe, r1, op, i) != 0 {
			break
		}
		if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CBits) && s_immarg(tls, qbe, r1, op, i) != 0 {
			if int64(-int32(2048)) <= *(*_int64_t)(unsafe.Pointer(c + 16)) && *(*_int64_t)(unsafe.Pointer(c + 16)) < int64(2048) {
				break
			}
		}
		r11 = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
		if k>>int32(1) == int32(1) {
			/* load floating points from memory
			 * slots, they can't be used as
			 * immediates
			 */
			if k&int32(1) != 0 {
				v10 = int32(8)
			} else {
				v10 = int32(4)
			}
			n = x_stashbits(tls, qbe, c+16, v10)
			v12 = fn + 36
			*(*int32)(unsafe.Pointer(v12))++
			v11 = *(*int32)(unsafe.Pointer(v12))
			x_vgrow(tls, qbe, fn+16, libc.Uint64FromInt32(v11))
			c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr((*_Fn)(unsafe.Pointer(fn)).Fncon-int32(1))*32
			libc.X__builtin___sprintf_chk(tls, bp+12, 0, ^___predefined_size_t(0), __ccgo_ts+5342, libc.VaList(bp+56, uintptr(unsafe.Pointer(&qbe.x_T))+136, n))
			*(*_Con)(unsafe.Pointer(c)) = _Con{
				Ftype1: int32(_CAddr),
			}
			(*_Con)(unsafe.Pointer(c)).Fsym.Fid = x_intern(tls, qbe, bp+12)
			x_emit(tls, qbe, int32(_Oload), k, r11, _Ref{
				F__ccgo0: uint32(_RCon)&0x7<<0 | libc.Uint32FromInt64((int64(c)-int64((*_Fn)(unsafe.Pointer(fn)).Fcon))/32)&0x1fffffff<<3,
			}, _Ref{})
			break
		}
		x_emit(tls, qbe, int32(_Ocopy), k, r11, *(*_Ref)(unsafe.Pointer(bp + 44)), _Ref{})
	case int32(_RTmp):
		if x_isreg(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 44))) != 0 {
			break
		}
		s = (*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 44 + 0))&0xfffffff8>>3))*192))).Fslot
		if s != -int32(1) {
			/* aggregate passed by value on
			 * stack, or fast local address,
			 * replace with slot if we can
			 */
			if s_memarg(tls, qbe, r1, op, i) != 0 {
				r11 = _Ref{
					F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
				}
				break
			}
			r11 = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
			x_emit(tls, qbe, int32(_Oaddr), k, r11, _Ref{
				F__ccgo0: uint32(_RSlot)&0x7<<0 | libc.Uint32FromInt32(s&libc.Int32FromInt32(0x1fffffff))&0x1fffffff<<3,
			}, _Ref{})
			break
		}
		if k == int32(_Kw) && int32((*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 44 + 0))&0xfffffff8>>3))*192))).Fcls) == int32(_Kl) {
			/* TODO: this sign extension isn't needed
			 * for 32-bit arithmetic instructions
			 */
			r11 = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
			x_emit(tls, qbe, int32(_Oextsw), int32(_Kl), r11, *(*_Ref)(unsafe.Pointer(bp + 44)), _Ref{})
		} else {
		}
		break
	}
	*(*_Ref)(unsafe.Pointer(r1)) = r11
}

func s_negate(tls *libc.TLS, qbe *_QBE, pr uintptr, fn uintptr) {
	var r _Ref
	_ = r
	r = x_newtmp(tls, qbe, __ccgo_ts+218, int32(_Kw), fn)
	x_emit(tls, qbe, int32(_Oxor), int32(_Kw), *(*_Ref)(unsafe.Pointer(pr)), r, x_getcon(tls, qbe, int64(1), fn))
	*(*_Ref)(unsafe.Pointer(pr)) = r
}

func s_selcmp2(tls *libc.TLS, qbe *_QBE, _i _Ins, k int32, op int32, fn uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*_Ins)(unsafe.Pointer(bp)) = _i
	var icmp uintptr
	var neg, sign, swap, v1, v2, v3 int32
	var r, r0, r1 _Ref
	_, _, _, _, _, _, _, _, _, _ = icmp, neg, r, r0, r1, sign, swap, v1, v2, v3
	switch op {
	case int32(_Cieq):
		r = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
		x_emit(tls, qbe, int32(_Oreqz), int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), (*(*_Ins)(unsafe.Pointer(bp))).Fto, r, _Ref{})
		x_emit(tls, qbe, int32(_Oxor), k, r, *(*_Ref)(unsafe.Pointer(bp + 8)), *(*_Ref)(unsafe.Pointer(bp + 8 + 1*4)))
		icmp = qbe.x_curi
		s_fixarg3(tls, qbe, icmp+8, k, icmp, fn)
		s_fixarg3(tls, qbe, icmp+8+1*4, k, icmp, fn)
		return
	case int32(_Cine):
		r = x_newtmp(tls, qbe, __ccgo_ts+218, k, fn)
		x_emit(tls, qbe, int32(_Ornez), int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), (*(*_Ins)(unsafe.Pointer(bp))).Fto, r, _Ref{})
		x_emit(tls, qbe, int32(_Oxor), k, r, *(*_Ref)(unsafe.Pointer(bp + 8)), *(*_Ref)(unsafe.Pointer(bp + 8 + 1*4)))
		icmp = qbe.x_curi
		s_fixarg3(tls, qbe, icmp+8, k, icmp, fn)
		s_fixarg3(tls, qbe, icmp+8+1*4, k, icmp, fn)
		return
	case int32(_Cisge):
		sign = int32(1)
		swap = 0
		neg = libc.Int32FromInt32(1)
	case int32(_Cisgt):
		sign = int32(1)
		swap = int32(1)
		neg = libc.Int32FromInt32(0)
	case int32(_Cisle):
		sign = int32(1)
		swap = int32(1)
		neg = libc.Int32FromInt32(1)
	case int32(_Cislt):
		sign = int32(1)
		swap = 0
		neg = libc.Int32FromInt32(0)
	case int32(_Ciuge):
		sign = 0
		swap = 0
		neg = libc.Int32FromInt32(1)
	case int32(_Ciugt):
		sign = 0
		swap = int32(1)
		neg = libc.Int32FromInt32(0)
	case int32(_Ciule):
		sign = 0
		swap = int32(1)
		neg = libc.Int32FromInt32(1)
	case int32(_Ciult):
		sign = 0
		swap = 0
		neg = libc.Int32FromInt32(0)
	case int32(_NCmpI) + int32(_Cfeq):
		fallthrough
	case int32(_NCmpI) + int32(_Cfge):
		fallthrough
	case int32(_NCmpI) + int32(_Cfgt):
		fallthrough
	case int32(_NCmpI) + int32(_Cfle):
		fallthrough
	case int32(_NCmpI) + int32(_Cflt):
		swap = 0
		neg = libc.Int32FromInt32(0)
	case int32(_NCmpI) + int32(_Cfuo):
		s_negate(tls, qbe, bp+4, fn)
		/* fall through */
		fallthrough
	case int32(_NCmpI) + int32(_Cfo):
		r0 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), fn)
		r1 = x_newtmp(tls, qbe, __ccgo_ts+218, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), fn)
		x_emit(tls, qbe, int32(_Oand), int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), (*(*_Ins)(unsafe.Pointer(bp))).Fto, r0, r1)
		if k&int32(1) != 0 {
			v1 = int32(_Oceqd)
		} else {
			v1 = int32(_Oceqs)
		}
		op = v1
		x_emit(tls, qbe, op, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), r0, *(*_Ref)(unsafe.Pointer(bp + 8)), *(*_Ref)(unsafe.Pointer(bp + 8)))
		icmp = qbe.x_curi
		s_fixarg3(tls, qbe, icmp+8, k, icmp, fn)
		s_fixarg3(tls, qbe, icmp+8+1*4, k, icmp, fn)
		x_emit(tls, qbe, op, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xc0000000>>30), r1, *(*_Ref)(unsafe.Pointer(bp + 8 + 1*4)), *(*_Ref)(unsafe.Pointer(bp + 8 + 1*4)))
		icmp = qbe.x_curi
		s_fixarg3(tls, qbe, icmp+8, k, icmp, fn)
		s_fixarg3(tls, qbe, icmp+8+1*4, k, icmp, fn)
		return
	case int32(_NCmpI) + int32(_Cfne):
		swap = 0
		neg = libc.Int32FromInt32(1)
		if k&int32(1) != 0 {
			v2 = int32(_Oceqd)
		} else {
			v2 = int32(_Oceqs)
		}
		libc.SetBitFieldPtr32Uint32(bp+0, libc.Uint32FromInt32(v2), 0, 0x3fffffff)
	default:
	}
	if op < int32(_NCmpI) {
		if sign != 0 {
			v3 = int32(_Ocsltl)
		} else {
			v3 = int32(_Ocultl)
		}
		libc.SetBitFieldPtr32Uint32(bp+0, libc.Uint32FromInt32(v3), 0, 0x3fffffff)
	}
	if swap != 0 {
		r = *(*_Ref)(unsafe.Pointer(bp + 8))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = *(*_Ref)(unsafe.Pointer(bp + 8 + 1*4))
		*(*_Ref)(unsafe.Pointer(bp + 8 + 1*4)) = r
	}
	if neg != 0 {
		s_negate(tls, qbe, bp+4, fn)
	}
	x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(bp)))
	icmp = qbe.x_curi
	s_fixarg3(tls, qbe, icmp+8, k, icmp, fn)
	s_fixarg3(tls, qbe, icmp+8+1*4, k, icmp, fn)
}

func s_sel2(tls *libc.TLS, qbe *_QBE, _i _Ins, fn uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	*(*_Ins)(unsafe.Pointer(bp)) = _i
	var i0 uintptr
	var _ /* cc at bp+20 */ int32
	var _ /* ck at bp+16 */ int32
	_ = i0
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0))-uint32(_Oalloc) <= libc.Uint32FromInt32(int32(_Oalloc1)-int32(_Oalloc)) {
		i0 = qbe.x_curi - uintptr(1)*16
		x_salloc(tls, qbe, (*(*_Ins)(unsafe.Pointer(bp))).Fto, *(*_Ref)(unsafe.Pointer(bp + 8)), fn)
		s_fixarg3(tls, qbe, i0+8, int32(_Kl), i0, fn)
		return
	}
	if x_iscmp(tls, qbe, int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0), bp+16, bp+20) != 0 {
		s_selcmp2(tls, qbe, *(*_Ins)(unsafe.Pointer(bp)), *(*int32)(unsafe.Pointer(bp + 16)), *(*int32)(unsafe.Pointer(bp + 20)), fn)
		return
	}
	if int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x3fffffff>>0) != int32(_Onop) {
		x_emiti(tls, qbe, *(*_Ins)(unsafe.Pointer(bp)))
		i0 = qbe.x_curi /* fixarg() can change curi */
		s_fixarg3(tls, qbe, i0+8, x_argcls(tls, qbe, bp, 0), i0, fn)
		s_fixarg3(tls, qbe, i0+8+1*4, x_argcls(tls, qbe, bp, int32(1)), i0, fn)
	}
}

func s_seljmp2(tls *libc.TLS, qbe *_QBE, b uintptr, fn uintptr) {
	/* TODO: replace cmp+jnz with beq/bne/blt[u]/bge[u] */
	if int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) == int32(_Jjnz) {
		s_fixarg3(tls, qbe, b+20+4, int32(_Kw), uintptr(0), fn)
	}
}

func x_rv64_isel(tls *libc.TLS, qbe *_QBE, fn uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var al, v6, v8 int32
	var b1, i, p, sb, v16, p10 uintptr
	var n _uint
	var sz _int64_t
	var v3, v4, v5 _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = al, b1, i, n, p, sb, sz, v16, v3, v4, v5, v6, v8, p10
	/* assign slots to fast allocs */
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	/* specific to NAlign == 3 */ /* or change n=4 and sz /= 4 below */
	al = int32(_Oalloc)
	n = libc.Uint32FromInt32(4)
	for {
		if !(al <= int32(_Oalloc1)) {
			break
		}
		i = (*_Blk)(unsafe.Pointer(b1)).Fins
		for {
			if !(i < (*_Blk)(unsafe.Pointer(b1)).Fins+uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16) {
				break
			}
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) == al {
				v3 = *(*_Ref)(unsafe.Pointer(i + 8))
				*(*_Ref)(unsafe.Pointer(bp + 32)) = v3
				v4 = v3
				v5 = _Ref{}
				*(*_Ref)(unsafe.Pointer(bp + 24)) = v4
				*(*_Ref)(unsafe.Pointer(bp + 28)) = v5
				v6 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 24 + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 28 + 0))&0xfffffff8>>3))
				goto _7
			_7:
				if v6 != 0 {
					v8 = -int32(1)
					goto _9
				}
				v8 = int32(*(*uint32)(unsafe.Pointer(bp + 32 + 0)) & 0x7 >> 0)
				goto _9
			_9:
				if v8 != int32(_RCon) {
					break
				}
				sz = *(*_int64_t)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32 + 16))
				if sz < 0 || sz >= int64(libc.Int32FromInt32(m___INT_MAX__)-libc.Int32FromInt32(15)) {
					_err(tls, qbe, __ccgo_ts+194, libc.VaList(bp+48, sz))
				}
				sz = (sz + libc.Int64FromUint32(n) - int64(1)) & libc.Int64FromUint32(-n)
				sz /= int64(4)
				if sz > int64(int32(m___INT_MAX__)-(*_Fn)(unsafe.Pointer(fn)).Fslot) {
					_die_(tls, qbe, __ccgo_ts+9865, __ccgo_ts+5413, 0)
				}
				(*(*_Tmp)(unsafe.Pointer((*_Fn)(unsafe.Pointer(fn)).Ftmp + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3))*192))).Fslot = (*_Fn)(unsafe.Pointer(fn)).Fslot
				p10 = fn + 72
				*(*int32)(unsafe.Pointer(p10)) = int32(int64(*(*int32)(unsafe.Pointer(p10))) + sz)
				*(*_Ins)(unsafe.Pointer(i)) = _Ins{
					F__ccgo0: uint32(_Onop) & 0x3fffffff << 0,
				}
			}
			goto _2
		_2:
			;
			i += 16
		}
		goto _1
	_1:
		;
		al++
		n *= uint32(2)
	}
	b1 = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b1 != 0) {
			break
		}
		qbe.x_curi = uintptr(unsafe.Pointer(&qbe.x_insb)) + uintptr(_NIns)*16
		*(*[3]uintptr)(unsafe.Pointer(bp)) = [3]uintptr{
			0: (*_Blk)(unsafe.Pointer(b1)).Fs1,
			1: (*_Blk)(unsafe.Pointer(b1)).Fs2,
		}
		sb = bp
		for {
			if !(*(*uintptr)(unsafe.Pointer(sb)) != 0) {
				break
			}
			p = (*_Blk)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(sb)))).Fphi
			for {
				if !(p != 0) {
					break
				}
				n = uint32(0)
				for {
					if !(*(*uintptr)(unsafe.Pointer((*_Phi)(unsafe.Pointer(p)).Fblk + uintptr(n)*8)) != b1) {
						break
					}
					goto _14
				_14:
					;
					n++
				}
				s_fixarg3(tls, qbe, (*_Phi)(unsafe.Pointer(p)).Farg+uintptr(n)*4, (*_Phi)(unsafe.Pointer(p)).Fcls, uintptr(0), fn)
				goto _13
			_13:
				;
				p = (*_Phi)(unsafe.Pointer(p)).Flink
			}
			goto _12
		_12:
			;
			sb += 8
		}
		s_seljmp2(tls, qbe, b1, fn)
		i = (*_Blk)(unsafe.Pointer(b1)).Fins + uintptr((*_Blk)(unsafe.Pointer(b1)).Fnins)*16
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b1)).Fins) {
				break
			}
			i -= 16
			v16 = i
			s_sel2(tls, qbe, *(*_Ins)(unsafe.Pointer(v16)), fn)
			goto _15
		_15:
		}
		(*_Blk)(unsafe.Pointer(b1)).Fnins = libc.Uint32FromInt64((___predefined_ptrdiff_t(uintptr(unsafe.Pointer(&qbe.x_insb))+uintptr(_NIns)*16) - int64(qbe.x_curi)) / 16)
		x_idup(tls, qbe, b1+8, qbe.x_curi, uint64((*_Blk)(unsafe.Pointer(b1)).Fnins))
		goto _11
	_11:
		;
		b1 = (*_Blk)(unsafe.Pointer(b1)).Flink
	}
	if qbe.x_debug[int32('I')] != 0 {
		fprintf(tls, qbe, libc.X__stderrp, __ccgo_ts+5429, 0)
		x_printfn(tls, qbe, fn, libc.X__stderrp)
	}
}

func s_slot5(tls *libc.TLS, qbe *_QBE, r1 _Ref, fn uintptr) (r _int64_t) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var s, v1 int32
	_, _ = s, v1
	*(*_Ref)(unsafe.Pointer(bp)) = r1
	v1 = int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) ^ int32(0x10000000) - int32(0x10000000)
	goto _2
_2:
	s = v1
	if s < 0 {
		return int64(int32(8) * -s)
	} else {
		return int64(-int32(4) * ((*_Fn)(unsafe.Pointer(fn)).Fslot - s))
	}
	return r
}

func s_emitaddr(tls *libc.TLS, qbe *_QBE, c uintptr, f uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	fputs(tls, qbe, x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid), f)
	if *(*_int64_t)(unsafe.Pointer(c + 16)) != 0 {
		fprintf(tls, qbe, f, __ccgo_ts+9006, libc.VaList(bp+8, *(*_int64_t)(unsafe.Pointer(c + 16))))
	}
}

func s_emitf2(tls *libc.TLS, qbe *_QBE, s uintptr, i uintptr, fn uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c, k, v10, v12, v18, v2, v20, v4 int32
	var offset _int64_t
	var pc, v14, v3, v5 uintptr
	var v15, v16, v17, v6, v7, v8, v9 _Ref
	var _ /* r at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, k, offset, pc, v10, v12, v14, v15, v16, v17, v18, v2, v20, v3, v4, v5, v6, v7, v8, v9
	fputc(tls, qbe, int32('\t'), f)
	for {
		k = int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30)
		for {
			v3 = s
			s++
			v2 = int32(*(*int8)(unsafe.Pointer(v3)))
			c = v2
			if !(v2 != int32('%')) {
				break
			}
			if !(c != 0) {
				fputc(tls, qbe, int32('\n'), f)
				return
			} else {
				fputc(tls, qbe, c, f)
			}
		}
		v5 = s
		s++
		v4 = int32(*(*int8)(unsafe.Pointer(v5)))
		c = v4
		switch v4 {
		default:
			_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+8683, 0)
			fallthrough
		case int32('?'):
			if k>>int32(1) == 0 {
				fputs(tls, qbe, __ccgo_ts+11336, f)
			} else {
				fputs(tls, qbe, __ccgo_ts+11339, f)
			}
		case int32('k'):
			if int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) != int32(_Kl) {
				fputc(tls, qbe, int32(qbe.s_clschr[int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)]), f)
			}
		case int32('='):
			fallthrough
		case int32('0'):
			if c == int32('=') {
				v6 = (*_Ins)(unsafe.Pointer(i)).Fto
			} else {
				v6 = *(*_Ref)(unsafe.Pointer(i + 8))
			}
			*(*_Ref)(unsafe.Pointer(bp + 12)) = v6
			fputs(tls, qbe, qbe.s_rname2[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3)], f)
		case int32('1'):
			*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8 + 1*4))
			v7 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v7
			v8 = v7
			v9 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v8
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v9
			v10 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _11
		_11:
			if v10 != 0 {
				v12 = -int32(1)
				goto _13
			}
			v12 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _13
		_13:
			switch v12 {
			default:
				_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+8706, 0)
				fallthrough
			case int32(_RTmp):
				fputs(tls, qbe, qbe.s_rname2[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3)], f)
			case int32(_RCon):
				pc = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
				fprintf(tls, qbe, f, __ccgo_ts+3090, libc.VaList(bp+24, int32(*(*_int64_t)(unsafe.Pointer(pc + 16)))))
				break
			}
		case int32('M'):
			v14 = s
			s++
			c = int32(*(*int8)(unsafe.Pointer(v14)))
			*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(i + 8 + uintptr(c-int32('0'))*4))
			v15 = *(*_Ref)(unsafe.Pointer(bp + 12))
			*(*_Ref)(unsafe.Pointer(bp + 8)) = v15
			v16 = v15
			v17 = _Ref{}
			*(*_Ref)(unsafe.Pointer(bp)) = v16
			*(*_Ref)(unsafe.Pointer(bp + 4)) = v17
			v18 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
			goto _19
		_19:
			if v18 != 0 {
				v20 = -int32(1)
				goto _21
			}
			v20 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
			goto _21
		_21:
			switch v20 {
			default:
				_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+11356, 0)
				fallthrough
			case int32(_RTmp):
				fprintf(tls, qbe, f, __ccgo_ts+11381, libc.VaList(bp+24, qbe.s_rname2[int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3)]))
			case int32(_RCon):
				pc = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
				s_emitaddr(tls, qbe, pc, f)
				if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) || libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) && int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == int32(1) {
					/* store (and float load)
					 * pseudo-instructions need a
					 * temporary register in which to
					 * load the address
					 */
					fprintf(tls, qbe, f, __ccgo_ts+11387, 0)
				}
			case int32(_RSlot):
				offset = s_slot5(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), fn)
				fprintf(tls, qbe, f, __ccgo_ts+11392, libc.VaList(bp+24, int32(offset)))
				break
			}
			break
		}
		goto _1
	_1:
	}
}

func s_loadaddr1(tls *libc.TLS, qbe *_QBE, c uintptr, rn uintptr, f uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var _ /* off at bp+0 */ [32]int8
	if (*_Con)(unsafe.Pointer(c)).Fsym.Ftype1 == int32(_SThr) {
		if *(*_int64_t)(unsafe.Pointer(c + 16)) != 0 {
			libc.X__builtin___sprintf_chk(tls, bp, 0, ^___predefined_size_t(0), __ccgo_ts+9006, libc.VaList(bp+40, *(*_int64_t)(unsafe.Pointer(c + 16))))
		} else {
			(*(*[32]int8)(unsafe.Pointer(bp)))[0] = 0
		}
		fprintf(tls, qbe, f, __ccgo_ts+11399, libc.VaList(bp+40, rn, x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid), bp))
		fprintf(tls, qbe, f, __ccgo_ts+11426, libc.VaList(bp+40, rn, rn, x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid), bp))
		fprintf(tls, qbe, f, __ccgo_ts+11462, libc.VaList(bp+40, rn, rn, x_str(tls, qbe, (*_Con)(unsafe.Pointer(c)).Fsym.Fid), bp))
	} else {
		fprintf(tls, qbe, f, __ccgo_ts+11494, libc.VaList(bp+40, rn))
		s_emitaddr(tls, qbe, c, f)
		fputc(tls, qbe, int32('\n'), f)
	}
}

func s_loadcon1(tls *libc.TLS, qbe *_QBE, c uintptr, r int32, k int32, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var n _int64_t
	var rn uintptr
	_, _ = n, rn
	rn = qbe.s_rname2[r]
	switch (*_Con)(unsafe.Pointer(c)).Ftype1 {
	case int32(_CAddr):
		s_loadaddr1(tls, qbe, c, rn, f)
	case int32(_CBits):
		n = *(*_int64_t)(unsafe.Pointer(c + 16))
		if !(k&libc.Int32FromInt32(1) != 0) {
			n = int64(int32(n))
		}
		fprintf(tls, qbe, f, __ccgo_ts+11503, libc.VaList(bp+8, rn, n))
	default:
		_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+5325, 0)
	}
}

func s_fixmem(tls *libc.TLS, qbe *_QBE, pr uintptr, fn uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c uintptr
	var s _int64_t
	var v1, v10, v2, v3, v8, v9 _Ref
	var v11, v13, v4, v6 int32
	var _ /* r at bp+12 */ _Ref
	_, _, _, _, _, _, _, _, _, _, _, _ = c, s, v1, v10, v11, v13, v2, v3, v4, v6, v8, v9
	*(*_Ref)(unsafe.Pointer(bp + 12)) = *(*_Ref)(unsafe.Pointer(pr))
	v1 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v1
	v2 = v1
	v3 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v2
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v3
	v4 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _5
_5:
	if v4 != 0 {
		v6 = -int32(1)
		goto _7
	}
	v6 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _7
_7:
	if v6 == int32(_RCon) {
		c = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(bp + 12 + 0))&0xfffffff8>>3))*32
		if (*_Con)(unsafe.Pointer(c)).Ftype1 == int32(_CAddr) {
			if (*_Con)(unsafe.Pointer(c)).Fsym.Ftype1 == int32(_SThr) {
				s_loadcon1(tls, qbe, c, int32(_T6), int32(_Kl), f)
				*(*_Ref)(unsafe.Pointer(pr)) = _Ref{
					F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_T6)&0x1fffffff<<3,
				}
			}
		}
	}
	v8 = *(*_Ref)(unsafe.Pointer(bp + 12))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v8
	v9 = v8
	v10 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v9
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v10
	v11 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _12
_12:
	if v11 != 0 {
		v13 = -int32(1)
		goto _14
	}
	v13 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _14
_14:
	if v13 == int32(_RSlot) {
		s = s_slot5(tls, qbe, *(*_Ref)(unsafe.Pointer(bp + 12)), fn)
		if s < int64(-int32(2048)) || s > int64(2047) {
			fprintf(tls, qbe, f, __ccgo_ts+11517, libc.VaList(bp+24, s))
			fprintf(tls, qbe, f, __ccgo_ts+11531, 0)
			*(*_Ref)(unsafe.Pointer(pr)) = _Ref{
				F__ccgo0: uint32(_RTmp)&0x7<<0 | uint32(_T6)&0x1fffffff<<3,
			}
		}
	}
}

func s_emitins2(tls *libc.TLS, qbe *_QBE, i uintptr, fn uintptr, f uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var con, rn uintptr
	var o, v12, v17, v19, v24, v26, v31, v33, v38, v40, v48 int32
	var s _int64_t
	var v10, v11, v14, v15, v16, v21, v22, v23, v28, v29, v30, v35, v36, v37, v46, v47 _Ref
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = con, o, rn, s, v10, v11, v12, v14, v15, v16, v17, v19, v21, v22, v23, v24, v26, v28, v29, v30, v31, v33, v35, v36, v37, v38, v40, v46, v47, v48
	switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0x3fffffff >> 0) {
	default:
		goto _1
	case int32(_Ocopy):
		goto _2
	case int32(_Onop):
		goto _3
	case int32(_Oaddr):
		goto _4
	case int32(_Ocall):
		goto _5
	case int32(_Osalloc):
		goto _6
	case int32(_Odbgloc):
		goto _7
	}
	goto _8
_1:
	;
	if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Oloadsb) <= libc.Uint32FromInt32(int32(_Oload)-int32(_Oloadsb)) {
		s_fixmem(tls, qbe, i+8, fn, f)
	} else {
		if libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0))-uint32(_Ostoreb) <= libc.Uint32FromInt32(int32(_Ostored)-int32(_Ostoreb)) {
			s_fixmem(tls, qbe, i+8+1*4, fn, f)
		}
	}
	goto Table
Table:
	;
	/* most instructions are just pulled out of
	 * the table omap[], some special cases are
	 * detailed below */
	o = 0
	for {
		/* this linear search should really be a binary
		 * search */
		if int32(qbe.s_omap2[o].Fop) == int32(_NOp) {
			_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+6898, libc.VaList(bp+24, qbe.x_optab[int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0)].Fname, int32(*(*int8)(unsafe.Pointer(__ccgo_ts + 6918 + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)))))))
		}
		if int32(qbe.s_omap2[o].Fop) == int32(*(*uint32)(unsafe.Pointer(i + 0))&0x3fffffff>>0) {
			if int32(qbe.s_omap2[o].Fcls) == int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30) || int32(qbe.s_omap2[o].Fcls) == int32(_Ka) || int32(qbe.s_omap2[o].Fcls) == int32(_Ki) && int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30)>>libc.Int32FromInt32(1) == 0 {
				break
			}
		}
		goto _9
	_9:
		;
		o++
	}
	s_emitf2(tls, qbe, qbe.s_omap2[o].Ffmt, i, fn, f)
	goto _8
_2:
	;
	v10 = (*_Ins)(unsafe.Pointer(i)).Fto
	v11 = *(*_Ref)(unsafe.Pointer(i + 8))
	*(*_Ref)(unsafe.Pointer(bp)) = v10
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v11
	v12 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _13
_13:
	if v12 != 0 {
		goto _8
	}
	v14 = (*_Ins)(unsafe.Pointer(i)).Fto
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v14
	v15 = v14
	v16 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v15
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v16
	v17 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _18
_18:
	if v17 != 0 {
		v19 = -int32(1)
		goto _20
	}
	v19 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _20
_20:
	if v19 == int32(_RSlot) {
		v21 = *(*_Ref)(unsafe.Pointer(i + 8))
		*(*_Ref)(unsafe.Pointer(bp + 8)) = v21
		v22 = v21
		v23 = _Ref{}
		*(*_Ref)(unsafe.Pointer(bp)) = v22
		*(*_Ref)(unsafe.Pointer(bp + 4)) = v23
		v24 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
		goto _25
	_25:
		if v24 != 0 {
			v26 = -int32(1)
			goto _27
		}
		v26 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
		goto _27
	_27:
		switch v26 {
		case int32(_RSlot):
			fallthrough
		case int32(_RCon):
			_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+11548, 0)
		default:
			*(*_Ref)(unsafe.Pointer(i + 8 + 1*4)) = (*_Ins)(unsafe.Pointer(i)).Fto
			(*_Ins)(unsafe.Pointer(i)).Fto = _Ref{}
			switch int32(*(*uint32)(unsafe.Pointer(i + 0)) & 0xc0000000 >> 30) {
			case int32(_Kw):
				libc.SetBitFieldPtr32Uint32(i+0, uint32(_Ostorew), 0, 0x3fffffff)
			case int32(_Kl):
				libc.SetBitFieldPtr32Uint32(i+0, uint32(_Ostorel), 0, 0x3fffffff)
			case int32(_Ks):
				libc.SetBitFieldPtr32Uint32(i+0, uint32(_Ostores), 0, 0x3fffffff)
			case int32(_Kd):
				libc.SetBitFieldPtr32Uint32(i+0, uint32(_Ostored), 0, 0x3fffffff)
				break
			}
			s_fixmem(tls, qbe, i+8+1*4, fn, f)
			goto Table
		}
		goto _8
	}
	v28 = *(*_Ref)(unsafe.Pointer(i + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v28
	v29 = v28
	v30 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v29
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v30
	v31 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _32
_32:
	if v31 != 0 {
		v33 = -int32(1)
		goto _34
	}
	v33 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _34
_34:
	switch v33 {
	case int32(_RCon):
		s_loadcon1(tls, qbe, (*_Fn)(unsafe.Pointer(fn)).Fcon+uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32, int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3), int32(*(*uint32)(unsafe.Pointer(i + 0))&0xc0000000>>30), f)
	case int32(_RSlot):
		libc.SetBitFieldPtr32Uint32(i+0, uint32(_Oload), 0, 0x3fffffff)
		s_fixmem(tls, qbe, i+8, fn, f)
		goto Table
	default:
		goto Table
	}
	goto _8
_3:
	;
	goto _8
_4:
	;
	rn = qbe.s_rname2[int32(*(*uint32)(unsafe.Pointer(i + 4 + 0))&0xfffffff8>>3)]
	s = s_slot5(tls, qbe, *(*_Ref)(unsafe.Pointer(i + 8)), fn)
	if -s < int64(2048) {
		fprintf(tls, qbe, f, __ccgo_ts+11562, libc.VaList(bp+24, rn, s))
	} else {
		fprintf(tls, qbe, f, __ccgo_ts+11581, libc.VaList(bp+24, rn, s, rn, rn))
	}
	goto _8
_5:
	;
	v35 = *(*_Ref)(unsafe.Pointer(i + 8))
	*(*_Ref)(unsafe.Pointer(bp + 8)) = v35
	v36 = v35
	v37 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v36
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v37
	v38 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _39
_39:
	if v38 != 0 {
		v40 = -int32(1)
		goto _41
	}
	v40 = int32(*(*uint32)(unsafe.Pointer(bp + 8 + 0)) & 0x7 >> 0)
	goto _41
_41:
	switch v40 {
	case int32(_RCon):
		goto _42
	case int32(_RTmp):
		goto _43
	default:
		goto _44
	}
	goto _45
_42:
	;
	con = (*_Fn)(unsafe.Pointer(fn)).Fcon + uintptr(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3))*32
	if (*_Con)(unsafe.Pointer(con)).Ftype1 != int32(_CAddr) || (*_Con)(unsafe.Pointer(con)).Fsym.Ftype1 != int32(_SGlo) || *(*_int64_t)(unsafe.Pointer(con + 16)) != 0 {
		goto Invalid
	}
	fprintf(tls, qbe, f, __ccgo_ts+11611, libc.VaList(bp+24, x_str(tls, qbe, (*_Con)(unsafe.Pointer(con)).Fsym.Fid)))
	goto _45
_43:
	;
	s_emitf2(tls, qbe, __ccgo_ts+11112, i, fn, f)
	goto _45
_44:
	;
	goto Invalid
Invalid:
	;
	_die_(tls, qbe, __ccgo_ts+11344, __ccgo_ts+7146, 0)
_45:
	;
	goto _8
_6:
	;
	s_emitf2(tls, qbe, __ccgo_ts+9190, i, fn, f)
	v46 = (*_Ins)(unsafe.Pointer(i)).Fto
	v47 = _Ref{}
	*(*_Ref)(unsafe.Pointer(bp)) = v46
	*(*_Ref)(unsafe.Pointer(bp + 4)) = v47
	v48 = libc.BoolInt32(int32(*(*uint32)(unsafe.Pointer(bp + 0))&0x7>>0) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0x7>>0) && int32(*(*uint32)(unsafe.Pointer(bp + 0))&0xfffffff8>>3) == int32(*(*uint32)(unsafe.Pointer(bp + 4 + 0))&0xfffffff8>>3))
	goto _49
_49:
	if !(v48 != 0) {
		s_emitf2(tls, qbe, __ccgo_ts+11621, i, fn, f)
	}
	goto _8
_7:
	;
	x_emitdbgloc(tls, qbe, libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 8 + 0))&0xfffffff8>>3)), libc.Uint32FromInt32(int32(*(*uint32)(unsafe.Pointer(i + 8 + 1*4 + 0))&0xfffffff8>>3)), f)
	goto _8
_8:
}

/*

  Stack-frame layout:

  +=============+
  | varargs     |
  |  save area  |
  +-------------+
  |  saved ra   |
  |  saved fp   |
  +-------------+ <- fp
  |    ...      |
  | spill slots |
  |    ...      |
  +-------------+
  |    ...      |
  |   locals    |
  |    ...      |
  +-------------+
  |   padding   |
  +-------------+
  | callee-save |
  |  registers  |
  +=============+

*/

func x_rv64_emitfn(tls *libc.TLS, qbe *_QBE, fn uintptr, f uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var b, i, pr, s, v13, v14, v4 uintptr
	var frame, lbl, neg, off, r int32
	_, _, _, _, _, _, _, _, _, _, _, _ = b, frame, i, lbl, neg, off, pr, r, s, v13, v14, v4
	x_emitfnlnk(tls, qbe, fn+83, fn+168, f)
	if (*_Fn)(unsafe.Pointer(fn)).Fvararg != 0 {
		/* TODO: only need space for registers
		 * unused by named arguments
		 */
		fprintf(tls, qbe, f, __ccgo_ts+11631, 0)
		r = int32(_A0)
		for {
			if !(r <= int32(_A7)) {
				break
			}
			fprintf(tls, qbe, f, __ccgo_ts+11649, libc.VaList(bp+8, qbe.s_rname2[r], int32(8)*(r-int32(_A0))))
			goto _1
		_1:
			;
			r++
		}
	}
	fprintf(tls, qbe, f, __ccgo_ts+11665, 0)
	fprintf(tls, qbe, f, __ccgo_ts+11682, 0)
	fprintf(tls, qbe, f, __ccgo_ts+11698, 0)
	frame = (int32(16) + int32(4)*(*_Fn)(unsafe.Pointer(fn)).Fslot + int32(15)) & ^libc.Int32FromInt32(15)
	pr = uintptr(unsafe.Pointer(&qbe.x_rv64_rclob))
	for {
		if !(*(*int32)(unsafe.Pointer(pr)) >= 0) {
			break
		}
		if (*_Fn)(unsafe.Pointer(fn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(pr))) != 0 {
			frame += int32(8)
		}
		goto _2
	_2:
		;
		pr += 4
	}
	frame = (frame + int32(15)) & ^libc.Int32FromInt32(15)
	if frame <= int32(2048) {
		fprintf(tls, qbe, f, __ccgo_ts+11716, libc.VaList(bp+8, frame))
	} else {
		fprintf(tls, qbe, f, __ccgo_ts+11734, libc.VaList(bp+8, frame))
	}
	pr = uintptr(unsafe.Pointer(&qbe.x_rv64_rclob))
	off = libc.Int32FromInt32(0)
	for {
		if !(*(*int32)(unsafe.Pointer(pr)) >= 0) {
			break
		}
		if (*_Fn)(unsafe.Pointer(fn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(pr))) != 0 {
			if *(*int32)(unsafe.Pointer(pr)) < int32(_FT0) {
				v4 = __ccgo_ts + 11762
			} else {
				v4 = __ccgo_ts + 11765
			}
			fprintf(tls, qbe, f, __ccgo_ts+11769, libc.VaList(bp+8, v4, qbe.s_rname2[*(*int32)(unsafe.Pointer(pr))], off))
			off += int32(8)
		}
		goto _3
	_3:
		;
		pr += 4
	}
	lbl = 0
	b = (*_Fn)(unsafe.Pointer(fn)).Fstart
	for {
		if !(b != 0) {
			break
		}
		if lbl != 0 || (*_Blk)(unsafe.Pointer(b)).Fnpred > uint32(1) {
			fprintf(tls, qbe, f, __ccgo_ts+11785, libc.VaList(bp+8, libc.Uint32FromInt32(qbe.s_id02)+(*_Blk)(unsafe.Pointer(b)).Fid))
		}
		i = (*_Blk)(unsafe.Pointer(b)).Fins
		for {
			if !(i != (*_Blk)(unsafe.Pointer(b)).Fins+uintptr((*_Blk)(unsafe.Pointer(b)).Fnins)*16) {
				break
			}
			s_emitins2(tls, qbe, i, fn, f)
			goto _6
		_6:
			;
			i += 16
		}
		lbl = int32(1)
		switch int32((*_Blk)(unsafe.Pointer(b)).Fjmp.Ftype1) {
		case int32(_Jhlt):
			goto _7
		case int32(_Jret0):
			goto _8
		case int32(_Jjmp):
			goto _9
		case int32(_Jjnz):
			goto _10
		}
		goto _11
	_7:
		;
		fprintf(tls, qbe, f, __ccgo_ts+11792, 0)
		goto _11
	_8:
		;
		if (*_Fn)(unsafe.Pointer(fn)).Fdynalloc != 0 {
			if frame-int32(16) <= int32(2048) {
				fprintf(tls, qbe, f, __ccgo_ts+11801, libc.VaList(bp+8, frame-int32(16)))
			} else {
				fprintf(tls, qbe, f, __ccgo_ts+11819, libc.VaList(bp+8, frame-int32(16)))
			}
		}
		pr = uintptr(unsafe.Pointer(&qbe.x_rv64_rclob))
		off = libc.Int32FromInt32(0)
		for {
			if !(*(*int32)(unsafe.Pointer(pr)) >= 0) {
				break
			}
			if (*_Fn)(unsafe.Pointer(fn)).Freg&(libc.Uint64FromInt32(1)<<*(*int32)(unsafe.Pointer(pr))) != 0 {
				if *(*int32)(unsafe.Pointer(pr)) < int32(_FT0) {
					v13 = __ccgo_ts + 3737
				} else {
					v13 = __ccgo_ts + 11847
				}
				fprintf(tls, qbe, f, __ccgo_ts+11769, libc.VaList(bp+8, v13, qbe.s_rname2[*(*int32)(unsafe.Pointer(pr))], off))
				off += int32(8)
			}
			goto _12
		_12:
			;
			pr += 4
		}
		fprintf(tls, qbe, f, __ccgo_ts+11851, libc.VaList(bp+8, int32(16)+int32((*_Fn)(unsafe.Pointer(fn)).Fvararg)*int32(64)))
		goto _11
	_9:
		;
		goto Jmp
	Jmp:
		;
		if (*_Blk)(unsafe.Pointer(b)).Fs1 != (*_Blk)(unsafe.Pointer(b)).Flink {
			fprintf(tls, qbe, f, __ccgo_ts+11901, libc.VaList(bp+8, libc.Uint32FromInt32(qbe.s_id02)+(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs1)).Fid))
		} else {
			lbl = 0
		}
		goto _11
	_10:
		;
		neg = 0
		if (*_Blk)(unsafe.Pointer(b)).Flink == (*_Blk)(unsafe.Pointer(b)).Fs2 {
			s = (*_Blk)(unsafe.Pointer(b)).Fs1
			(*_Blk)(unsafe.Pointer(b)).Fs1 = (*_Blk)(unsafe.Pointer(b)).Fs2
			(*_Blk)(unsafe.Pointer(b)).Fs2 = s
			neg = int32(1)
		}
		if neg != 0 {
			v14 = __ccgo_ts + 9219
		} else {
			v14 = __ccgo_ts + 9216
		}
		fprintf(tls, qbe, f, __ccgo_ts+11910, libc.VaList(bp+8, v14, qbe.s_rname2[int32(*(*uint32)(unsafe.Pointer(b + 20 + 4 + 0))&0xfffffff8>>3)], libc.Uint32FromInt32(qbe.s_id02)+(*_Blk)(unsafe.Pointer((*_Blk)(unsafe.Pointer(b)).Fs2)).Fid))
		goto Jmp
	_11:
		;
		goto _5
	_5:
		;
		b = (*_Blk)(unsafe.Pointer(b)).Flink
	}
	qbe.s_id02 = int32(uint32(qbe.s_id02) + (*_Fn)(unsafe.Pointer(fn)).Fnblk)
	x_elf_emitfnfin(tls, qbe, fn+83, f)
}

func __ccgo_fp(f interface{}) uintptr {
	type iface [2]uintptr
	return (*iface)(unsafe.Pointer(&f))[1]
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "/* end data */\n\n\x00**** Function %s ****\x00\n> After parsing:\n\x00/* end function %s */\n\n\x00\n\x00unknown target '%s'\n\x00util.c\x00emalloc, out of memory\x00interning table overflow\x00emit, too many instructions\x00%s.%d\x00invalid alloc size %lld\x00isel\x00unlikely alloc argument %%%s for %%%s\x00[\x00 %s\x00 ]\n\x00add\x00sub\x00neg\x00div\x00rem\x00udiv\x00urem\x00mul\x00and\x00or\x00xor\x00sar\x00shr\x00shl\x00ceqw\x00cnew\x00csgew\x00csgtw\x00cslew\x00csltw\x00cugew\x00cugtw\x00culew\x00cultw\x00ceql\x00cnel\x00csgel\x00csgtl\x00cslel\x00csltl\x00cugel\x00cugtl\x00culel\x00cultl\x00ceqs\x00cges\x00cgts\x00cles\x00clts\x00cnes\x00cos\x00cuos\x00ceqd\x00cged\x00cgtd\x00cled\x00cltd\x00cned\x00cod\x00cuod\x00storeb\x00storeh\x00storew\x00storel\x00stores\x00stored\x00loadsb\x00loadub\x00loadsh\x00loaduh\x00loadsw\x00loaduw\x00load\x00extsb\x00extub\x00extsh\x00extuh\x00extsw\x00extuw\x00exts\x00truncd\x00stosi\x00stoui\x00dtosi\x00dtoui\x00swtof\x00uwtof\x00sltof\x00ultof\x00cast\x00alloc4\x00alloc8\x00alloc16\x00vaarg\x00vastart\x00copy\x00dbgloc\x00nop\x00addr\x00blit0\x00blit1\x00swap\x00sign\x00salloc\x00xidiv\x00xdiv\x00xcmp\x00xtest\x00acmp\x00acmn\x00afcmp\x00reqz\x00rnez\x00par\x00parsb\x00parub\x00parsh\x00paruh\x00parc\x00pare\x00arg\x00argsb\x00argub\x00argsh\x00arguh\x00argc\x00arge\x00argv\x00call\x00flagieq\x00flagine\x00flagisge\x00flagisgt\x00flagisle\x00flagislt\x00flagiuge\x00flagiugt\x00flagiule\x00flagiult\x00flagfeq\x00flagfge\x00flagfgt\x00flagfle\x00flagflt\x00flagfne\x00flagfo\x00flagfuo\x00loadw\x00loadl\x00loads\x00loadd\x00alloc1\x00alloc2\x00blit\x00env\x00phi\x00jmp\x00jnz\x00ret\x00hlt\x00export\x00thread\x00common\x00function\x00type\x00data\x00section\x00align\x00dbgfile\x00l\x00w\x00sh\x00uh\x00h\x00sb\x00ub\x00b\x00d\x00s\x00z\x00...\x00_%f\x00_%lf\x00unterminated string\x00invalid character %c (%d)\x00identifier too long\x00unknown keyword %s\x00label\x00=\x00,\x00(\x00)\x00{\x00}\x00newline\x00??\x00%s expected, got %s instead\x00undefined type :%s\x00invalid class specifier\x00too many instructions\x00no parameters allowed after '...'\x00only one '...' allowed\x00only one environment allowed\x00invalid argument\x00invalid function parameter\x00label or } expected\x00label, instruction or jump expected\x00multiple definitions of block @%s\x00invalid return value\x00invalid argument for jnz jump\x00invalid jump to the start block\x00line number too big\x00column number too big\x00cannot use vastart in non-variadic function\x00size class must be w, l, s, or d\x00too many arguments\x00invalid instruction argument\x00, or end of line expected\x00unexpected phi instruction\x00blit size must be constant\x00invalid blit size\x00invalid instruction\x00temporary %%%s is assigned with multiple types\x00multiple entries for @%s in phi %%%s\x00invalid type for operand %%%s in phi %%%s\x00predecessors not matched in phi %%%s\x00invalid instruction type in %s\x00second\x00first\x00no %s operand expected in %s\x00missing %s operand in %s\x00invalid type for %s operand %%%s in %s\x00invalid type for jump argument %%%s in block @%s\x00block @%s is used undefined\x00function name expected\x00function body must start with {\x00empty function\x00last block misses jump\x00invalid type member specifier\x00, or } expected\x00type name and then = expected\x00alignment expected\x00type body must start with {\x00dark types need alignment\x00} expected\x00invalid union member\x00invalid token after offset in ref\x00data name, then = expected\x00invalid alignment\x00expected data contents in { .. }\x00invalid size specifier %c in data\x00constant literal expected\x00only one section allowed\x00section \"name\" expected\x00only data may have thread linkage\x00only data and function have linkage\x00top-level definition expected\x00thread \x00$%s\x00%+lli\x00s_%f\x00d_%lf\x00%lli\x00R%d\x00%%%s\x00UNDEF\x00S%d\x00%04x\x00:%s\x00 + \x00%d * \x00%d\x00R\x00retw\x00retl\x00rets\x00retd\x00retsb\x00retub\x00retsh\x00retuh\x00retc\x00ret0\x00jfieq\x00jfine\x00jfisge\x00jfisgt\x00jfisle\x00jfislt\x00jfiuge\x00jfiugt\x00jfiule\x00jfiult\x00jffeq\x00jffge\x00jffgt\x00jffle\x00jfflt\x00jffne\x00jffo\x00jffuo\x00function $%s() {\n\x00@%s\n\x00\t\x00 =%c phi \x00@%s \x00, \x00 =%c \x00%s\x00 \x00\t%s\x00, :%s\x00\thlt\n\x00\tjmp @%s\n\x00\t%s \x00@%s, @%s\n\x00}\n\x00slot %%%s is read but never stored to\x00\n> After slot promotion:\n\x00\n> Slot coalescing:\n\x00\tkill [\x00 %%%s\x00\tfuse (% 3db) [\x00[%d,%d)\x00{}\x00\tsums %u/%u/%u (killed/fused/total)\n\n\x00ssa.c\x00unreachable\x00invalid input\x00\n> Dominators:\n\x00%10s:\x00\n> After SSA construction:\n\x00ssa temporary %%%s defined more than once\x00%%%s violates ssa invariant\x00ssa temporary %%%s is used undefined in @%s\x00alias.c\x00load.c\x00ld\x00\n> After load elimination:\n\x00\n> Copy information:\x00\n%10s not seen!\x00\n%10s copy of \x00\n\n> After copy elimination:\n\x00fold.c\x00\n> SCCP findings:\x00\n%10s: \x00Top\x00\n dead code: \x00%s \x00(none)\x00\n\n> After constant folding:\n\x00invalid address operand for '%s'\x00blt\x00\n> Liveness analysis:\n\x00\t%-10sin:   \x00\t          out:  \x00\t          gen:  \x00\t          live: \x00%d %d\n\x00\n> Loop information:\n\x00\t%-10s\x00 (% 3d \x00% 3d) \x00\n> Spill costs:\n\x00\t%-10s %d\n\x00\n> Block information:\n\x00\t%-10s (% 5d) \x00\n> After spilling:\n\x00rega.c\x00no more regs\x00 (%s, R%d)\x00cannot have more moves than registers\x00\n> Register mappings:\n\x00\t%-10s beg\x00\t           end\x00%s_%s\x00\n> Register allocation statistics:\n\x00\tnew moves:  %d\n\x00\tnew blocks: %d\n\x00\n> After register allocation:\n\x00.text\x00.data\x00.bss\x00.abort \"unreachable\"\x00.section .tdata,\"awT\"\x00.section .tbss,\"awT\"\x00\x00__DATA\x00__thread_data,thread_local_regular\x00$tlv$init\x00.section __DATA,__thread_vars,thread_local_variables\n\x00%s%s:\n\x00\t.quad __tlv_bootstrap\n\t.quad 0\n\t.quad %s%s%s\n\n\x00.section %s\x00,%s\x00.balign %d\n\x00.globl %s%s\n\x00%s%s%s:\n\x00\t.byte\x00\t.short\x00\t.int\x00\t.quad\x00emit.c\x00invalid common data definition\x00.comm %s%s,%lld\x00,%d\x00\t.fill %lld,1,0\n\x00strings only supported for 'b' currently\x00\t.ascii %s\n\x00%s %s%s%+lld\n\x00%s %lld\n\x00/* floating point constants */\n\x00.section %s\n.p2align %d\n%sfp%d:\x00\n\t.int %d\x00 /* %f */\n\n\x00\n\n\x00.rodata\x00.section .note.GNU-stack,\"\",@progbits\n\x00.type %s, @function\n\x00.size %s, .-%s\n\x00__TEXT,__literal4,4byte_literals\x00__TEXT,__literal8,8byte_literals\x00.file %u %s\n\x00\t.loc %u %u %u\n\x00\t.loc %u %u\n\x00amd64/sysv.c\x00abi\x00sysv abi does not support variadic env calls\x00sysv abi requires alignments of 16 or less\x00\n> After ABI lowering:\n\x00amd64/isel.c\x00invalid constant\x00\"%sfp%d\"\x00unlikely argument %%%s in %s\x00utof\x00ftou\x00unknown instruction %s\x00alloc too large\x00\n> After instruction selection:\n\x00+add%k %1, %=\x00-sub%k %1, %=\x00+and%k %1, %=\x00+or%k %1, %=\x00+xor%k %1, %=\x00-sar%k %B1, %=\x00-shr%k %B1, %=\x00-shl%k %B1, %=\x00+imul%k %1, %=\x00+mulss %1, %=\x00+mulsd %1, %=\x00-div%k %1, %=\x00movq %L0, %M1\x00movl %W0, %M1\x00movw %H0, %M1\x00movb %B0, %M1\x00movss %S0, %M1\x00movsd %D0, %M1\x00mov%k %M0, %=\x00movslq %M0, %L=\x00movl %M0, %W=\x00movsw%k %M0, %=\x00movzw%k %M0, %=\x00movsb%k %M0, %=\x00movzb%k %M0, %=\x00movslq %W0, %L=\x00movl %W0, %W=\x00movsw%k %H0, %=\x00movzw%k %H0, %=\x00movsb%k %B0, %=\x00movzb%k %B0, %=\x00cvtss2sd %0, %=\x00cvtsd2ss %0, %=\x00cvttss2si%k %0, %=\x00cvttsd2si%k %0, %=\x00cvtsi2%k %W0, %=\x00cvtsi2%k %L0, %=\x00movq %D0, %L=\x00movq %L0, %D=\x00lea%k %M0, %=\x00xchg%k %0, %1\x00cqto\x00div%k %0\x00idiv%k %0\x00ucomiss %S0, %S1\x00ucomisd %D0, %D1\x00cmp%k %0, %1\x00test%k %0, %1\x00setbe %B=\n\tmovzb%k %B=, %=\x00setb %B=\n\tmovzb%k %B=, %=\x00setle %B=\n\tmovzb%k %B=, %=\x00setl %B=\n\tmovzb%k %B=, %=\x00setg %B=\n\tmovzb%k %B=, %=\x00setge %B=\n\tmovzb%k %B=, %=\x00seta %B=\n\tmovzb%k %B=, %=\x00setae %B=\n\tmovzb%k %B=, %=\x00setz %B=\n\tmovzb%k %B=, %=\x00setnz %B=\n\tmovzb%k %B=, %=\x00setnp %B=\n\tmovzb%k %B=, %=\x00setp %B=\n\tmovzb%k %B=, %=\x00rax\x00eax\x00ax\x00al\x00rcx\x00ecx\x00cx\x00cl\x00rdx\x00edx\x00dx\x00dl\x00rsi\x00esi\x00si\x00sil\x00rdi\x00edi\x00di\x00dil\x00r8\x00r8d\x00r8w\x00r8b\x00r9\x00r9d\x00r9w\x00r9b\x00r10\x00r10d\x00r10w\x00r10b\x00r11\x00r11d\x00r11w\x00r11b\x00rbx\x00ebx\x00bx\x00bl\x00r12\x00r12d\x00r12w\x00r12b\x00r13\x00r13d\x00r13w\x00r13b\x00r14\x00r14d\x00r14w\x00r14b\x00r15\x00r15d\x00r15w\x00r15b\x00rbp\x00ebp\x00bp\x00bpl\x00rsp\x00esp\x00sp\x00spl\x00%s%s@TLVP\x00%%fs:%s%s@tpoff\x00%s%s\x00%+lld\x00%lld\x00amd64/emit.c\x00xmm%d\x00invalid arg letter %c\x00%d(%%%s)\x00%%rip\x00, %%%s, %d\x00(%%rip)\x00(%%%s)\x00invalid format specifier %%%c\x00no match for %s(%c)\x00wlsd\x00imul%k %0, %1, %=\x00add%k %0, %=\x00mov%k %0, %=\x00neg%k %=\x00\txorp%c %sfp%d(%%rip), %%%s\n\x00xxsd\x00mov%k %=, %1\x00movl %0, %=\x00movl %0>>32, 4+%=\x00mov%k %0, %1\x00mov%k %1, %=\x00movq %%fs:0, %L=\x00\tleaq %s%s@tpoff\x00(%%%s), %%%s\n\x00\tcallq \x00callq *%L0\x00invalid call argument\x00subq %L0, %%rsp\x00nz\x00ge\x00g\x00le\x00ae\x00a\x00be\x00np\x00p\x00\tendbr64\n\x00\tpushq %rbp\n\tmovq %rsp, %rbp\n\x00\tsubq $%llu, %%rsp\n\x00\tmovq %%%s, %d(%%rbp)\n\x00\tmovaps %%xmm%d, %d(%%rbp)\n\x00pushq %L0\x00%sbb%d:\n\x00\tud2\n\x00\tmovq %%rbp, %%rsp\n\tsubq $%llu, %%rsp\n\x00popq %L0\x00\tleave\n\x00\taddq $%llu, %%rsp\n\x00\tret\n\x00\tjmp %sbb%d\n\x00\tj%s %sbb%d\n\x00unhandled jump %d\x00alignments larger than 8 are not supported\x00arm64/abi.c\x00\n> After Apple pre-ABI:\n\x00add %=, %0, %1\x00fadd %=, %0, %1\x00sub %=, %0, %1\x00fsub %=, %0, %1\x00neg %=, %0\x00fneg %=, %0\x00and %=, %0, %1\x00orr %=, %0, %1\x00eor %=, %0, %1\x00asr %=, %0, %1\x00lsr %=, %0, %1\x00lsl %=, %0, %1\x00mul %=, %0, %1\x00fmul %=, %0, %1\x00sdiv %=, %0, %1\x00fdiv %=, %0, %1\x00udiv %=, %0, %1\x00sdiv %?, %0, %1\n\tmsub\t%=, %?, %1, %0\x00udiv %?, %0, %1\n\tmsub\t%=, %?, %1, %0\x00mov %=, %0\x00fmov %=, %0\x00mov %?, %0\n\tmov\t%0, %1\n\tmov\t%1, %?\x00fmov %?, %0\n\tfmov\t%0, %1\n\tfmov\t%1, %?\x00strb %W0, %M1\x00strh %W0, %M1\x00str %W0, %M1\x00str %L0, %M1\x00str %S0, %M1\x00str %D0, %M1\x00ldrsb %=, %M0\x00ldrb %W=, %M0\x00ldrsh %=, %M0\x00ldrh %W=, %M0\x00ldr %=, %M0\x00ldrsw %=, %M0\x00ldr %W=, %M0\x00sxtb %=, %W0\x00uxtb %W=, %W0\x00sxth %=, %W0\x00uxth %W=, %W0\x00sxtw %L=, %W0\x00mov %W=, %W0\x00fcvt %=, %S0\x00fcvt %=, %D0\x00fmov %=, %S0\x00fmov %=, %D0\x00fmov %=, %W0\x00fmov %=, %L0\x00fcvtzs %=, %S0\x00fcvtzu %=, %S0\x00fcvtzs %=, %D0\x00fcvtzu %=, %D0\x00scvtf %=, %W0\x00ucvtf %=, %W0\x00scvtf %=, %L0\x00ucvtf %=, %L0\x00blr %L0\x00cmp %0, %1\x00cmn %0, %1\x00fcmpe %0, %1\x00cset %=, eq\x00cset %=, ne\x00cset %=, ge\x00cset %=, gt\x00cset %=, le\x00cset %=, lt\x00cset %=, cs\x00cset %=, hi\x00cset %=, ls\x00cset %=, cc\x00cset %=, mi\x00cset %=, vc\x00cset %=, vs\x00arm64/emit.c\x00invalid class\x00w%d\x00x%d\x00s%d\x00d%d\x00invalid register\x00invalid escape\x00s31\x00d31\x00invalid second argument\x00#%llu\x00#%llu, lsl #12\x00todo (arm emit): unhandled ref\x00[%s]\x00[x29, %llu]\x00\tadrp\tR, S@pageO\n\tadd\tR, R, S@pageoffO\n\x00\tadrp\tR, SO\n\tadd\tR, R, #:lo12:SO\n\x00\tadrp\tR, S@tlvppage\n\tldr\tR, [R, S@tlvppageoff]\n\x00\tmrs\tR, tpidr_el0\n\tadd\tR, R, #:tprel_hi12:SO, lsl #12\n\tadd\tR, R, #:tprel_lo12_nc:SO\n\x00+%lli\x00\tmov\t%s, #%lli\n\x00\tmov\t%s, #%d\n\x00\tmovk\t%s, #0x%x, lsl #%d\n\x00\tadd\t%s, x29, #%llu\n\x00\tmov\t%s, #%llu\n\tadd\t%s, x29, %s\n\x00\tmov\t%s, #%llu\n\tmovk\t%s, #%llu, lsl #16\n\tadd\t%s, x29, %s\n\x00\tbl\t%s%s\n\x00sub sp, sp, %0\x00mov %=, sp\x00eq\x00ne\x00gt\x00lt\x00cs\x00hi\x00ls\x00cc\x00mi\x00vc\x00vs\x00\thint\t#34\n\x00\tstr\tq%d, [sp, -16]!\n\x00\tstp\tx%d, x%d, [sp, -16]!\n\x00\tstp\tx29, x30, [sp, -%llu]!\n\x00\tsub\tsp, sp, #%llu\n\tstp\tx29, x30, [sp, -16]!\n\x00\tmov\tx16, #%llu\n\tsub\tsp, sp, x16\n\tstp\tx29, x30, [sp, -16]!\n\x00\tmov\tx16, #%llu\n\tmovk\tx16, #%llu, lsl #16\n\tsub\tsp, sp, x16\n\tstp\tx29, x30, [sp, -16]!\n\x00\tmov\tx29, sp\n\x00%s%d:\n\x00\tbrk\t#1000\n\x00\tmov sp, x29\n\x00\tldp\tx29, x30, [sp], %llu\n\x00\tldp\tx29, x30, [sp], 16\n\tadd\tsp, sp, #%llu\n\x00\tldp\tx29, x30, [sp], 16\n\tmov\tx16, #%llu\n\tadd\tsp, sp, x16\n\x00\tldp\tx29, x30, [sp], 16\n\tmov\tx16, #%llu\n\tmovk\tx16, #%llu, lsl #16\n\tadd\tsp, sp, x16\n\x00\tb\t%s%d\n\x00\tb%s\t%s%d\n\x00rv64/abi.c\x00alignments larger than 16 are not supported\x00rv64/isel.c\x00add%k %=, %0, %1\x00fadd.%k %=, %0, %1\x00sub%k %=, %0, %1\x00fsub.%k %=, %0, %1\x00neg%k %=, %0\x00fneg.%k %=, %0\x00div%k %=, %0, %1\x00fdiv.%k %=, %0, %1\x00rem%k %=, %0, %1\x00rem %=, %0, %1\x00divu%k %=, %0, %1\x00remu%k %=, %0, %1\x00mul%k %=, %0, %1\x00fmul.%k %=, %0, %1\x00or %=, %0, %1\x00xor %=, %0, %1\x00sra%k %=, %0, %1\x00srl%k %=, %0, %1\x00sll%k %=, %0, %1\x00slt %=, %0, %1\x00sltu %=, %0, %1\x00feq.s %=, %0, %1\x00fge.s %=, %0, %1\x00fgt.s %=, %0, %1\x00fle.s %=, %0, %1\x00flt.s %=, %0, %1\x00feq.d %=, %0, %1\x00fge.d %=, %0, %1\x00fgt.d %=, %0, %1\x00fle.d %=, %0, %1\x00flt.d %=, %0, %1\x00sb %0, %M1\x00sh %0, %M1\x00sw %0, %M1\x00sd %0, %M1\x00fsw %0, %M1\x00fsd %0, %M1\x00lb %=, %M0\x00lbu %=, %M0\x00lh %=, %M0\x00lhu %=, %M0\x00lw %=, %M0\x00lwu %=, %M0\x00ld %=, %M0\x00flw %=, %M0\x00fld %=, %M0\x00sext.b %=, %0\x00zext.b %=, %0\x00sext.h %=, %0\x00zext.h %=, %0\x00sext.w %=, %0\x00zext.w %=, %0\x00fcvt.s.d %=, %0\x00fcvt.d.s %=, %0\x00fcvt.w.s %=, %0, rtz\x00fcvt.l.s %=, %0, rtz\x00fcvt.wu.s %=, %0, rtz\x00fcvt.lu.s %=, %0, rtz\x00fcvt.w.d %=, %0, rtz\x00fcvt.l.d %=, %0, rtz\x00fcvt.wu.d %=, %0, rtz\x00fcvt.lu.d %=, %0, rtz\x00fcvt.%k.w %=, %0\x00fcvt.%k.wu %=, %0\x00fcvt.%k.l %=, %0\x00fcvt.%k.lu %=, %0\x00fmv.x.w %=, %0\x00fmv.x.d %=, %0\x00fmv.w.x %=, %0\x00fmv.d.x %=, %0\x00mv %=, %0\x00fmv.%k %=, %0\x00mv %?, %0\n\tmv %0, %1\n\tmv %1, %?\x00fmv.%k %?, %0\n\tfmv.%k %0, %1\n\tfmv.%k %1, %?\x00seqz %=, %0\x00snez %=, %0\x00jalr %0\x00t0\x00t1\x00t2\x00t3\x00t4\x00t5\x00a0\x00a1\x00a2\x00a3\x00a4\x00a5\x00a6\x00a7\x00s1\x00s2\x00s3\x00s4\x00s5\x00s6\x00s7\x00s8\x00s9\x00s10\x00s11\x00fp\x00gp\x00tp\x00ra\x00ft0\x00ft1\x00ft2\x00ft3\x00ft4\x00ft5\x00ft6\x00ft7\x00ft8\x00ft9\x00ft10\x00fa0\x00fa1\x00fa2\x00fa3\x00fa4\x00fa5\x00fa6\x00fa7\x00fs0\x00fs1\x00fs2\x00fs3\x00fs4\x00fs5\x00fs6\x00fs7\x00fs8\x00fs9\x00fs10\x00fs11\x00t6\x00ft11\x00rv64/emit.c\x00invalid address argument\x000(%s)\x00, t6\x00%d(fp)\x00\tlui %s, %%tprel_hi(%s)%s\n\x00\tadd %s, %s, tp, %%tprel_add(%s)%s\n\x00\taddi %s, %s, %%tprel_lo(%s)%s\n\x00\tla %s, \x00\tli %s, %lli\n\x00\tli t6, %lld\n\x00\tadd t6, fp, t6\n\x00unimplemented\x00\tadd %s, fp, %lld\n\x00\tli %s, %lld\n\tadd %s, fp, %s\n\x00\tcall %s\n\x00mv %=, sp\x00\tadd sp, sp, -64\n\x00\tsd %s, %d(sp)\n\x00\tsd fp, -16(sp)\n\x00\tsd ra, -8(sp)\n\x00\tadd fp, sp, -16\n\x00\tadd sp, sp, -%d\n\x00\tli t6, %d\n\tsub sp, sp, t6\n\x00sd\x00fsd\x00\t%s %s, %d(sp)\n\x00.L%d:\n\x00\tebreak\n\x00\tadd sp, fp, -%d\n\x00\tli t6, %d\n\tsub sp, fp, t6\n\x00fld\x00\tadd sp, fp, %d\n\tld ra, 8(fp)\n\tld fp, 0(fp)\n\tret\n\x00\tj .L%d\n\x00\tb%sz %s, .L%d\n\x00"

type _QBE struct {
	mallocs          map[uintptr]struct{}
	stderr           io.Writer
	r                io.ByteScanner
	w                io.Writer
	s_Oaddtbl        [91]_uchar
	s___ccgo_init_1  [6]_uchar
	s___ccgo_init_11 [4]_uint32_t
	s___ccgo_init_2  [29]_uchar
	s___ccgo_init_21 [2]_uint64_t
	s___ccgo_init_3  [9]_uchar
	s___ccgo_init_4  [65]_uchar
	s___ccgo_init_5  [6]_uchar
	s___ccgo_init_6  [50]_uchar
	s_blink          uintptr
	s_blkh           [8192]uintptr
	s_buf            [6]int8
	s_buf1           [4]int8
	s_clschr         [4]int8
	s_clstoa         [4][3]int8
	s_cmptab         [18][2]int32
	s_ctoa           [18]uintptr
	s_ctoa1          [18]uintptr
	s_curb           uintptr
	s_curf           uintptr
	s_curf1          uintptr
	s_curfile        _uint
	s_dbg            int32
	s_done           int32
	s_dtoa           [6]uintptr
	s_edge           uintptr
	s_extcpy         [7]_bits
	s_file           uintptr
	s_flowrk         uintptr
	s_fpreg          [12]int32
	s_fpreg1         [10]int32
	s_fst            uintptr
	s_gpreg          [12]int32
	s_gpreg1         [10]int32
	s_id0            int32
	s_id01           int32
	s_id02           int32
	s_ilog           uintptr
	s_inf            uintptr
	s_inpath         uintptr
	s_inum           _uint
	s_itbl           [4096]_Bucket
	s_jtoa           [32]uintptr
	s_ktoc           [5]int8
	s_kwmap          [140]uintptr
	s_lexh           [512]_uchar
	s_lnum           int32
	s_locs           int32
	s_loop           int32
	s_mask1          [2][1]_BSet
	s_match          [13]_bits
	s_matcher        [6]uintptr
	s_maxt           int32
	s_mem            uintptr
	s_n              int32
	s_namel          uintptr
	s_nblk           int32
	s_negmask        [4]uintptr
	s_nfile          _uint
	s_nlog           _uint
	s_npm            int32
	s_nptr           int32
	s_ntmp           int32
	s_ntyp           _uint
	s_nuse           _uint
	s_omap           [69]struct {
		Fop  int16
		Fcls int16
		Ffmt uintptr
	}
	s_omap1 [80]struct {
		Fop  int16
		Fcls int16
		Ffmt uintptr
	}
	s_omap2 [81]struct {
		Fop  int16
		Fcls int16
		Ffmt uintptr
	}
	s_outf  uintptr
	s_pat   [6]int32
	s_plink uintptr
	s_pm    [64]struct {
		Fsrc _Ref
		Fdst _Ref
		Fcls int32
	}
	s_pool    uintptr
	s_ptr     [256]uintptr
	s_rcls    int32
	s_regu    _bits
	s_retreg  [2][2]int32
	s_rname   [17][4]uintptr
	s_rname2  [64]uintptr
	s_sec     [2][3]uintptr
	s_sec1    [3]uintptr
	s_sec2    [3]uintptr
	s_slot4   int32
	s_slot8   int32
	s_st      [4]int32
	s_stash   uintptr
	s_stblk   _uint
	s_stmov   _uint
	s_store3  [4]int32
	s_tarr    uintptr
	s_thead   int32
	s_tlist   [6]uintptr
	s_tmp     uintptr
	s_tmp1    uintptr
	s_tmph    uintptr
	s_tmphcap int32
	s_tok     [80]int8
	s_tokval  struct {
		Fchr  int8
		Ffltd float64
		Fflts float32
		Fnum  _int64_t
		Fstr  uintptr
	}
	s_ttoa             [140]uintptr
	s_ulog2_tab64      [64]int32
	s_usewrk           uintptr
	s_val              uintptr
	s_z                _Blk
	s_zero             _int64_t
	x_T                _Target
	x_T_amd64_apple    _Target
	x_T_amd64_sysv     _Target
	x_T_arm64          _Target
	x_T_arm64_apple    _Target
	x_T_rv64           _Target
	x_amd64_op         [138]_Amd64Op
	x_amd64_sysv_rclob [6]int32
	x_amd64_sysv_rsave [25]int32
	x_arm64_rclob      [19]int32
	x_arm64_rsave      [44]int32
	x_curi             uintptr
	x_debug            [91]int8
	x_insb             [1048576]_Ins
	x_optab            [138]_Op
	x_rv64_op          [138]_Rv64Op
	x_rv64_rclob       [24]int32
	x_rv64_rsave       [34]int32
	x_typ              uintptr
}

func newQBE(pinner *runtime.Pinner) (qbe *_QBE) {
	qbe = &_QBE{mallocs: map[uintptr]struct{}{}}
	pinner.Pin(qbe)
	qbe.s_Oaddtbl = [91]_uchar{
		0:  uint8(2),
		1:  uint8(2),
		2:  uint8(2),
		3:  uint8(4),
		4:  uint8(4),
		5:  uint8(5),
		6:  uint8(6),
		7:  uint8(6),
		8:  uint8(8),
		9:  uint8(8),
		10: uint8(4),
		11: uint8(4),
		12: uint8(9),
		13: uint8(10),
		14: uint8(9),
		15: uint8(7),
		16: uint8(7),
		17: uint8(5),
		18: uint8(8),
		19: uint8(9),
		20: uint8(5),
		21: uint8(4),
		22: uint8(4),
		23: uint8(12),
		24: uint8(10),
		25: uint8(12),
		26: uint8(12),
		27: uint8(12),
		28: uint8(4),
		29: uint8(4),
		30: uint8(9),
		31: uint8(10),
		32: uint8(9),
		33: uint8(9),
		34: uint8(12),
		35: uint8(9),
		36: uint8(11),
		37: uint8(11),
		38: uint8(5),
		39: uint8(8),
		40: uint8(9),
		41: uint8(5),
		42: uint8(12),
		43: uint8(9),
		44: uint8(5),
		45: uint8(7),
		46: uint8(7),
		47: uint8(5),
		48: uint8(8),
		49: uint8(9),
		50: uint8(5),
		51: uint8(12),
		52: uint8(9),
		53: uint8(5),
		54: uint8(5),
		55: uint8(11),
		56: uint8(11),
		57: uint8(5),
		58: uint8(8),
		59: uint8(9),
		60: uint8(5),
		61: uint8(12),
		62: uint8(9),
		63: uint8(5),
		64: uint8(5),
		65: uint8(5),
		66: uint8(4),
		67: uint8(4),
		68: uint8(9),
		69: uint8(10),
		70: uint8(9),
		71: uint8(9),
		72: uint8(12),
		73: uint8(9),
		74: uint8(9),
		75: uint8(9),
		76: uint8(9),
		77: uint8(9),
		78: uint8(7),
		79: uint8(7),
		80: uint8(5),
		81: uint8(8),
		82: uint8(9),
		83: uint8(5),
		84: uint8(12),
		85: uint8(9),
		86: uint8(5),
		87: uint8(5),
		88: uint8(5),
		89: uint8(9),
		90: uint8(5),
	}
	qbe.s___ccgo_init_1 = [6]_uchar{
		0: uint8(1),
		1: uint8(3),
		3: uint8(3),
		4: uint8(1),
	}
	qbe.s___ccgo_init_11 = [4]_uint32_t{
		0: uint32(0x80000000),
	}
	qbe.s___ccgo_init_2 = [29]_uchar{
		0:  uint8(5),
		1:  uint8(1),
		2:  uint8(8),
		3:  uint8(5),
		4:  uint8(27),
		5:  uint8(1),
		6:  uint8(5),
		7:  uint8(1),
		8:  uint8(2),
		9:  uint8(5),
		10: uint8(13),
		11: uint8(3),
		12: uint8(1),
		13: uint8(1),
		14: uint8(3),
		15: uint8(3),
		16: uint8(3),
		17: uint8(2),
		19: uint8(1),
		20: uint8(3),
		21: uint8(3),
		22: uint8(3),
		23: uint8(2),
		24: uint8(3),
		25: uint8(1),
		27: uint8(1),
		28: uint8(29),
	}
	qbe.s___ccgo_init_21 = [2]_uint64_t{
		0: uint64(0x8000000000000000),
	}
	qbe.s___ccgo_init_3 = [9]_uchar{
		0: uint8(1),
		1: uint8(3),
		3: uint8(1),
		4: uint8(3),
		5: uint8(3),
		6: uint8(3),
		7: uint8(2),
	}
	qbe.s___ccgo_init_4 = [65]_uchar{
		0:  uint8(5),
		1:  uint8(2),
		2:  uint8(10),
		3:  uint8(7),
		4:  uint8(11),
		5:  uint8(19),
		6:  uint8(49),
		7:  uint8(1),
		8:  uint8(1),
		9:  uint8(3),
		10: uint8(3),
		11: uint8(3),
		12: uint8(2),
		13: uint8(1),
		14: uint8(3),
		16: uint8(3),
		17: uint8(1),
		19: uint8(1),
		20: uint8(3),
		22: uint8(5),
		23: uint8(1),
		24: uint8(8),
		25: uint8(5),
		26: uint8(25),
		27: uint8(1),
		28: uint8(5),
		29: uint8(1),
		30: uint8(2),
		31: uint8(5),
		32: uint8(13),
		33: uint8(3),
		34: uint8(1),
		35: uint8(1),
		36: uint8(3),
		37: uint8(3),
		38: uint8(3),
		39: uint8(2),
		41: uint8(1),
		42: uint8(3),
		43: uint8(3),
		44: uint8(3),
		45: uint8(2),
		46: uint8(26),
		47: uint8(1),
		48: uint8(51),
		49: uint8(1),
		50: uint8(5),
		51: uint8(1),
		52: uint8(6),
		53: uint8(5),
		54: uint8(9),
		55: uint8(1),
		56: uint8(3),
		58: uint8(51),
		59: uint8(3),
		60: uint8(1),
		61: uint8(1),
		62: uint8(3),
		64: uint8(45),
	}
	qbe.s___ccgo_init_5 = [6]_uchar{
		0: uint8(1),
		1: uint8(3),
		2: uint8(1),
		3: uint8(3),
		4: uint8(2),
	}
	qbe.s___ccgo_init_6 = [50]_uchar{
		0:  uint8(5),
		1:  uint8(3),
		2:  uint8(9),
		3:  uint8(9),
		4:  uint8(10),
		5:  uint8(33),
		6:  uint8(12),
		7:  uint8(35),
		8:  uint8(45),
		9:  uint8(1),
		10: uint8(5),
		11: uint8(3),
		12: uint8(11),
		13: uint8(9),
		14: uint8(7),
		15: uint8(9),
		16: uint8(4),
		17: uint8(9),
		18: uint8(17),
		19: uint8(1),
		20: uint8(3),
		22: uint8(3),
		23: uint8(1),
		24: uint8(3),
		25: uint8(2),
		27: uint8(3),
		28: uint8(1),
		29: uint8(1),
		30: uint8(3),
		32: uint8(34),
		33: uint8(1),
		34: uint8(37),
		35: uint8(1),
		36: uint8(5),
		37: uint8(2),
		38: uint8(5),
		39: uint8(7),
		40: uint8(2),
		41: uint8(7),
		42: uint8(8),
		43: uint8(37),
		44: uint8(29),
		45: uint8(1),
		46: uint8(3),
		48: uint8(1),
		49: uint8(32),
	}
	qbe.s_clschr = [4]int8{
		0: int8('w'),
		1: int8('l'),
		2: int8('s'),
		3: int8('d'),
	}
	qbe.s_clstoa = [4][3]int8{
		0: {'l'},
		1: {'q'},
		2: {'s', 's'},
		3: {'s', 'd'},
	}
	qbe.s_cmptab = [18][2]int32{
		0: {
			0: int32(_Cine),
		},
		1: {
			1: int32(_Cine),
		},
		2: {
			0: int32(_Cislt),
			1: int32(_Cisle),
		},
		3: {
			0: int32(_Cisle),
			1: int32(_Cislt),
		},
		4: {
			0: int32(_Cisgt),
			1: int32(_Cisge),
		},
		5: {
			0: int32(_Cisge),
			1: int32(_Cisgt),
		},
		6: {
			0: int32(_Ciult),
			1: int32(_Ciule),
		},
		7: {
			0: int32(_Ciule),
			1: int32(_Ciult),
		},
		8: {
			0: int32(_Ciugt),
			1: int32(_Ciuge),
		},
		9: {
			0: int32(_Ciuge),
			1: int32(_Ciugt),
		},
		10: {
			0: int32(_NCmpI) + int32(_Cfne),
			1: int32(_NCmpI) + int32(_Cfeq),
		},
		11: {
			0: int32(_NCmpI) + int32(_Cflt),
			1: int32(_NCmpI) + int32(_Cfle),
		},
		12: {
			0: int32(_NCmpI) + int32(_Cfle),
			1: int32(_NCmpI) + int32(_Cflt),
		},
		13: {
			0: int32(_NCmpI) + int32(_Cfgt),
			1: int32(_NCmpI) + int32(_Cfge),
		},
		14: {
			0: int32(_NCmpI) + int32(_Cfge),
			1: int32(_NCmpI) + int32(_Cfgt),
		},
		15: {
			0: int32(_NCmpI) + int32(_Cfeq),
			1: int32(_NCmpI) + int32(_Cfne),
		},
		16: {
			0: int32(_NCmpI) + int32(_Cfuo),
			1: int32(_NCmpI) + int32(_Cfo),
		},
		17: {
			0: int32(_NCmpI) + int32(_Cfo),
			1: int32(_NCmpI) + int32(_Cfuo),
		},
	}
	qbe.s_ctoa = [18]uintptr{
		0:  __ccgo_ts + 1236,
		1:  __ccgo_ts + 7184,
		2:  __ccgo_ts + 7187,
		3:  __ccgo_ts + 7190,
		4:  __ccgo_ts + 7192,
		5:  __ccgo_ts + 1212,
		6:  __ccgo_ts + 7195,
		7:  __ccgo_ts + 7198,
		8:  __ccgo_ts + 7200,
		9:  __ccgo_ts + 1230,
		10: __ccgo_ts + 1236,
		11: __ccgo_ts + 7195,
		12: __ccgo_ts + 7198,
		13: __ccgo_ts + 7200,
		14: __ccgo_ts + 1230,
		15: __ccgo_ts + 7184,
		16: __ccgo_ts + 7203,
		17: __ccgo_ts + 7206,
	}
	qbe.s_ctoa1 = [18]uintptr{
		0:  __ccgo_ts + 9216,
		1:  __ccgo_ts + 9219,
		2:  __ccgo_ts + 7187,
		3:  __ccgo_ts + 9222,
		4:  __ccgo_ts + 7192,
		5:  __ccgo_ts + 9225,
		6:  __ccgo_ts + 9228,
		7:  __ccgo_ts + 9231,
		8:  __ccgo_ts + 9234,
		9:  __ccgo_ts + 9237,
		10: __ccgo_ts + 9216,
		11: __ccgo_ts + 7187,
		12: __ccgo_ts + 9222,
		13: __ccgo_ts + 9234,
		14: __ccgo_ts + 9240,
		15: __ccgo_ts + 9219,
		16: __ccgo_ts + 9243,
		17: __ccgo_ts + 9246,
	}
	qbe.s_dtoa = [6]uintptr{
		2: __ccgo_ts + 4723,
		3: __ccgo_ts + 4730,
		4: __ccgo_ts + 4738,
		5: __ccgo_ts + 4744,
	}
	qbe.s_extcpy = [7]_bits{
		1: libc.Uint64FromInt32(1)<<int32(_Wsb) | libc.Uint64FromInt32(1)<<int32(_Wsh) | libc.Uint64FromInt32(1)<<int32(_Wsw),
		2: libc.Uint64FromInt32(1)<<int32(_Wub) | libc.Uint64FromInt32(1)<<int32(_Wuh) | libc.Uint64FromInt32(1)<<int32(_Wuw),
		3: libc.Uint64FromInt32(1)<<int32(_Wsh) | libc.Uint64FromInt32(1)<<int32(_Wsw),
		4: libc.Uint64FromInt32(1)<<int32(_Wuh) | libc.Uint64FromInt32(1)<<int32(_Wuw),
		5: libc.Uint64FromInt32(1) << int32(_Wsw),
		6: libc.Uint64FromInt32(1) << int32(_Wuw),
	}
	qbe.s_fpreg = [12]int32{
		0: int32(_V0),
		1: int32(_V1),
		2: int32(_V2),
		3: int32(_V3),
		4: int32(_V4),
		5: int32(_V5),
		6: int32(_V6),
		7: int32(_V7),
	}
	qbe.s_fpreg1 = [10]int32{
		0: int32(_FA0),
		1: int32(_FA1),
		2: int32(_FA2),
		3: int32(_FA3),
		4: int32(_FA4),
		5: int32(_FA5),
		6: int32(_FA6),
		7: int32(_FA7),
	}
	qbe.s_gpreg = [12]int32{
		0: int32(_R0),
		1: int32(_R1),
		2: int32(_R2),
		3: int32(_R3),
		4: int32(_R4),
		5: int32(_R5),
		6: int32(_R6),
		7: int32(_R7),
	}
	qbe.s_gpreg1 = [10]int32{
		0: int32(_A0),
		1: int32(_A1),
		2: int32(_A2),
		3: int32(_A3),
		4: int32(_A4),
		5: int32(_A5),
		6: int32(_A6),
		7: int32(_A7),
	}
	qbe.s_jtoa = [32]uintptr{
		1:  __ccgo_ts + 3095,
		2:  __ccgo_ts + 3100,
		3:  __ccgo_ts + 3105,
		4:  __ccgo_ts + 3110,
		5:  __ccgo_ts + 3115,
		6:  __ccgo_ts + 3121,
		7:  __ccgo_ts + 3127,
		8:  __ccgo_ts + 3133,
		9:  __ccgo_ts + 3139,
		10: __ccgo_ts + 3144,
		11: __ccgo_ts + 1134,
		12: __ccgo_ts + 1138,
		13: __ccgo_ts + 3149,
		14: __ccgo_ts + 3155,
		15: __ccgo_ts + 3161,
		16: __ccgo_ts + 3168,
		17: __ccgo_ts + 3175,
		18: __ccgo_ts + 3182,
		19: __ccgo_ts + 3189,
		20: __ccgo_ts + 3196,
		21: __ccgo_ts + 3203,
		22: __ccgo_ts + 3210,
		23: __ccgo_ts + 3217,
		24: __ccgo_ts + 3223,
		25: __ccgo_ts + 3229,
		26: __ccgo_ts + 3235,
		27: __ccgo_ts + 3241,
		28: __ccgo_ts + 3247,
		29: __ccgo_ts + 3253,
		30: __ccgo_ts + 3258,
		31: __ccgo_ts + 1146,
	}
	qbe.s_ktoc = [5]int8{'w', 'l', 's', 'd'}
	qbe.s_kwmap = [140]uintptr{
		88:  __ccgo_ts + 1083,
		89:  __ccgo_ts + 1089,
		90:  __ccgo_ts + 1095,
		91:  __ccgo_ts + 1101,
		92:  __ccgo_ts + 1107,
		93:  __ccgo_ts + 1114,
		94:  __ccgo_ts + 1121,
		95:  __ccgo_ts + 927,
		96:  __ccgo_ts + 1126,
		97:  __ccgo_ts + 1130,
		98:  __ccgo_ts + 1134,
		99:  __ccgo_ts + 1138,
		100: __ccgo_ts + 1142,
		101: __ccgo_ts + 1146,
		102: __ccgo_ts + 1150,
		103: __ccgo_ts + 1157,
		104: __ccgo_ts + 1164,
		105: __ccgo_ts + 1171,
		106: __ccgo_ts + 1180,
		107: __ccgo_ts + 1185,
		108: __ccgo_ts + 1190,
		109: __ccgo_ts + 1198,
		110: __ccgo_ts + 1204,
		111: __ccgo_ts + 1212,
		112: __ccgo_ts + 1214,
		113: __ccgo_ts + 1216,
		114: __ccgo_ts + 1219,
		115: __ccgo_ts + 1222,
		116: __ccgo_ts + 1224,
		117: __ccgo_ts + 1227,
		118: __ccgo_ts + 1230,
		119: __ccgo_ts + 1232,
		120: __ccgo_ts + 1234,
		121: __ccgo_ts + 1236,
		138: __ccgo_ts + 1238,
	}
	qbe.s_match = [13]_bits{
		4:  libc.Uint64FromInt32(1) << int32(_Pob),
		5:  libc.Uint64FromInt32(1) << int32(_Pbi1),
		6:  libc.Uint64FromInt32(1)<<int32(_Pob) | libc.Uint64FromInt32(1)<<int32(_Pois),
		7:  libc.Uint64FromInt32(1)<<int32(_Pob) | libc.Uint64FromInt32(1)<<int32(_Pobi1),
		8:  libc.Uint64FromInt32(1)<<int32(_Pbi1) | libc.Uint64FromInt32(1)<<int32(_Pbis),
		9:  libc.Uint64FromInt32(1)<<int32(_Pbi1) | libc.Uint64FromInt32(1)<<int32(_Pobi1),
		10: libc.Uint64FromInt32(1)<<int32(_Pbi1) | libc.Uint64FromInt32(1)<<int32(_Pbis) | libc.Uint64FromInt32(1)<<int32(_Pobi1) | libc.Uint64FromInt32(1)<<int32(_Pobis),
		11: libc.Uint64FromInt32(1)<<int32(_Pob) | libc.Uint64FromInt32(1)<<int32(_Pobi1) | libc.Uint64FromInt32(1)<<int32(_Pobis),
		12: libc.Uint64FromInt32(1)<<int32(_Pbi1) | libc.Uint64FromInt32(1)<<int32(_Pobi1) | libc.Uint64FromInt32(1)<<int32(_Pobis),
	}
	qbe.s_matcher = [6]uintptr{
		0: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_1)),
		1: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_2)),
		2: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_3)),
		3: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_4)),
		4: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_5)),
		5: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_6)),
	}
	qbe.s_negmask = [4]uintptr{
		2: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_11)),
		3: uintptr(unsafe.Pointer(&qbe.s___ccgo_init_21)),
	}
	qbe.s_nptr = int32(1)
	qbe.s_omap = [69]struct {
		Fop  int16
		Fcls int16
		Ffmt uintptr
	}{
		0: {
			Fop:  int16(_Oadd),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5462,
		},
		1: {
			Fop:  int16(_Osub),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5476,
		},
		2: {
			Fop:  int16(_Oand),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5490,
		},
		3: {
			Fop:  int16(_Oor),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5504,
		},
		4: {
			Fop:  int16(_Oxor),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5517,
		},
		5: {
			Fop:  int16(_Osar),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5531,
		},
		6: {
			Fop:  int16(_Oshr),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5546,
		},
		7: {
			Fop:  int16(_Oshl),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5561,
		},
		8: {
			Fop:  int16(_Omul),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5576,
		},
		9: {
			Fop:  int16(_Omul),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 5591,
		},
		10: {
			Fop:  int16(_Omul),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 5605,
		},
		11: {
			Fop:  int16(_Odiv),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5619,
		},
		12: {
			Fop:  int16(_Ostorel),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5633,
		},
		13: {
			Fop:  int16(_Ostorew),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5647,
		},
		14: {
			Fop:  int16(_Ostoreh),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5661,
		},
		15: {
			Fop:  int16(_Ostoreb),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5675,
		},
		16: {
			Fop:  int16(_Ostores),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5689,
		},
		17: {
			Fop:  int16(_Ostored),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5704,
		},
		18: {
			Fop:  int16(_Oload),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5719,
		},
		19: {
			Fop:  int16(_Oloadsw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 5733,
		},
		20: {
			Fop:  int16(_Oloadsw),
			Ffmt: __ccgo_ts + 5749,
		},
		21: {
			Fop:  int16(_Oloaduw),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5749,
		},
		22: {
			Fop:  int16(_Oloadsh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5763,
		},
		23: {
			Fop:  int16(_Oloaduh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5779,
		},
		24: {
			Fop:  int16(_Oloadsb),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5795,
		},
		25: {
			Fop:  int16(_Oloadub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5811,
		},
		26: {
			Fop:  int16(_Oextsw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 5827,
		},
		27: {
			Fop:  int16(_Oextuw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 5843,
		},
		28: {
			Fop:  int16(_Oextsh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5857,
		},
		29: {
			Fop:  int16(_Oextuh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5873,
		},
		30: {
			Fop:  int16(_Oextsb),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5889,
		},
		31: {
			Fop:  int16(_Oextub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5905,
		},
		32: {
			Fop:  int16(_Oexts),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 5921,
		},
		33: {
			Fop:  int16(_Otruncd),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 5937,
		},
		34: {
			Fop:  int16(_Ostosi),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5953,
		},
		35: {
			Fop:  int16(_Odtosi),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 5972,
		},
		36: {
			Fop:  int16(_Oswtof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 5991,
		},
		37: {
			Fop:  int16(_Osltof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 6008,
		},
		38: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6025,
		},
		39: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 6039,
		},
		40: {
			Fop:  int16(_Oaddr),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6053,
		},
		41: {
			Fop:  int16(_Oswap),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6067,
		},
		42: {
			Fop:  int16(_Osign),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 6081,
		},
		43: {
			Fop:  int16(_Osign),
			Ffmt: __ccgo_ts + 503,
		},
		44: {
			Fop:  int16(_Oxdiv),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6086,
		},
		45: {
			Fop:  int16(_Oxidiv),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6095,
		},
		46: {
			Fop:  int16(_Oxcmp),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 6105,
		},
		47: {
			Fop:  int16(_Oxcmp),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 6122,
		},
		48: {
			Fop:  int16(_Oxcmp),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6139,
		},
		49: {
			Fop:  int16(_Oxtest),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6152,
		},
		50: {
			Fop:  int16(int32(_Oflag) + int32(_Ciule)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6166,
		},
		51: {
			Fop:  int16(int32(_Oflag) + int32(_Ciult)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6193,
		},
		52: {
			Fop:  int16(int32(_Oflag) + int32(_Cisle)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6219,
		},
		53: {
			Fop:  int16(int32(_Oflag) + int32(_Cislt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6246,
		},
		54: {
			Fop:  int16(int32(_Oflag) + int32(_Cisgt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6272,
		},
		55: {
			Fop:  int16(int32(_Oflag) + int32(_Cisge)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6298,
		},
		56: {
			Fop:  int16(int32(_Oflag) + int32(_Ciugt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6325,
		},
		57: {
			Fop:  int16(int32(_Oflag) + int32(_Ciuge)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6351,
		},
		58: {
			Fop:  int16(int32(_Oflag) + int32(_Cieq)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6378,
		},
		59: {
			Fop:  int16(int32(_Oflag) + int32(_Cine)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6404,
		},
		60: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfle)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6166,
		},
		61: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cflt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6193,
		},
		62: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfgt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6325,
		},
		63: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfge)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6351,
		},
		64: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfeq)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6378,
		},
		65: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfne)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6404,
		},
		66: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfo)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6431,
		},
		67: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfuo)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 6458,
		},
		68: {
			Fop: int16(_NOp),
		},
	}
	qbe.s_omap1 = [80]struct {
		Fop  int16
		Fcls int16
		Ffmt uintptr
	}{
		0: {
			Fop:  int16(_Oadd),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7550,
		},
		1: {
			Fop:  int16(_Oadd),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7565,
		},
		2: {
			Fop:  int16(_Osub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7581,
		},
		3: {
			Fop:  int16(_Osub),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7596,
		},
		4: {
			Fop:  int16(_Oneg),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7612,
		},
		5: {
			Fop:  int16(_Oneg),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7623,
		},
		6: {
			Fop:  int16(_Oand),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7635,
		},
		7: {
			Fop:  int16(_Oor),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7650,
		},
		8: {
			Fop:  int16(_Oxor),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7665,
		},
		9: {
			Fop:  int16(_Osar),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7680,
		},
		10: {
			Fop:  int16(_Oshr),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7695,
		},
		11: {
			Fop:  int16(_Oshl),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7710,
		},
		12: {
			Fop:  int16(_Omul),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7725,
		},
		13: {
			Fop:  int16(_Omul),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7740,
		},
		14: {
			Fop:  int16(_Odiv),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7756,
		},
		15: {
			Fop:  int16(_Odiv),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7772,
		},
		16: {
			Fop:  int16(_Oudiv),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7788,
		},
		17: {
			Fop:  int16(_Orem),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7804,
		},
		18: {
			Fop:  int16(_Ourem),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7841,
		},
		19: {
			Fop:  int16(_Ocopy),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7878,
		},
		20: {
			Fop:  int16(_Ocopy),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7889,
		},
		21: {
			Fop:  int16(_Oswap),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7901,
		},
		22: {
			Fop:  int16(_Oswap),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 7936,
		},
		23: {
			Fop:  int16(_Ostoreb),
			Ffmt: __ccgo_ts + 7974,
		},
		24: {
			Fop:  int16(_Ostoreh),
			Ffmt: __ccgo_ts + 7988,
		},
		25: {
			Fop:  int16(_Ostorew),
			Ffmt: __ccgo_ts + 8002,
		},
		26: {
			Fop:  int16(_Ostorel),
			Ffmt: __ccgo_ts + 8015,
		},
		27: {
			Fop:  int16(_Ostores),
			Ffmt: __ccgo_ts + 8028,
		},
		28: {
			Fop:  int16(_Ostored),
			Ffmt: __ccgo_ts + 8041,
		},
		29: {
			Fop:  int16(_Oloadsb),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8054,
		},
		30: {
			Fop:  int16(_Oloadub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8068,
		},
		31: {
			Fop:  int16(_Oloadsh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8082,
		},
		32: {
			Fop:  int16(_Oloaduh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8096,
		},
		33: {
			Fop:  int16(_Oloadsw),
			Ffmt: __ccgo_ts + 8110,
		},
		34: {
			Fop:  int16(_Oloadsw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 8122,
		},
		35: {
			Fop:  int16(_Oloaduw),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8136,
		},
		36: {
			Fop:  int16(_Oload),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8110,
		},
		37: {
			Fop:  int16(_Oextsb),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8149,
		},
		38: {
			Fop:  int16(_Oextub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8162,
		},
		39: {
			Fop:  int16(_Oextsh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8176,
		},
		40: {
			Fop:  int16(_Oextuh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8189,
		},
		41: {
			Fop:  int16(_Oextsw),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8203,
		},
		42: {
			Fop:  int16(_Oextuw),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8217,
		},
		43: {
			Fop:  int16(_Oexts),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 8230,
		},
		44: {
			Fop:  int16(_Otruncd),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 8243,
		},
		45: {
			Fop:  int16(_Ocast),
			Ffmt: __ccgo_ts + 8256,
		},
		46: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 8269,
		},
		47: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 8282,
		},
		48: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 8295,
		},
		49: {
			Fop:  int16(_Ostosi),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8308,
		},
		50: {
			Fop:  int16(_Ostoui),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8323,
		},
		51: {
			Fop:  int16(_Odtosi),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8338,
		},
		52: {
			Fop:  int16(_Odtoui),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8353,
		},
		53: {
			Fop:  int16(_Oswtof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8368,
		},
		54: {
			Fop:  int16(_Ouwtof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8382,
		},
		55: {
			Fop:  int16(_Osltof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8396,
		},
		56: {
			Fop:  int16(_Oultof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8410,
		},
		57: {
			Fop:  int16(_Ocall),
			Ffmt: __ccgo_ts + 8424,
		},
		58: {
			Fop:  int16(_Oacmp),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8432,
		},
		59: {
			Fop:  int16(_Oacmn),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8443,
		},
		60: {
			Fop:  int16(_Oafcmp),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 8454,
		},
		61: {
			Fop:  int16(int32(_Oflag) + int32(_Cieq)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8467,
		},
		62: {
			Fop:  int16(int32(_Oflag) + int32(_Cine)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8479,
		},
		63: {
			Fop:  int16(int32(_Oflag) + int32(_Cisge)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8491,
		},
		64: {
			Fop:  int16(int32(_Oflag) + int32(_Cisgt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8503,
		},
		65: {
			Fop:  int16(int32(_Oflag) + int32(_Cisle)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8515,
		},
		66: {
			Fop:  int16(int32(_Oflag) + int32(_Cislt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8527,
		},
		67: {
			Fop:  int16(int32(_Oflag) + int32(_Ciuge)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8539,
		},
		68: {
			Fop:  int16(int32(_Oflag) + int32(_Ciugt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8551,
		},
		69: {
			Fop:  int16(int32(_Oflag) + int32(_Ciule)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8563,
		},
		70: {
			Fop:  int16(int32(_Oflag) + int32(_Ciult)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8575,
		},
		71: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfeq)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8467,
		},
		72: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfge)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8491,
		},
		73: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfgt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8503,
		},
		74: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfle)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8563,
		},
		75: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cflt)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8587,
		},
		76: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfne)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8479,
		},
		77: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfo)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8599,
		},
		78: {
			Fop:  int16(int32(_Oflag) + int32(_NCmpI) + int32(_Cfuo)),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 8611,
		},
		79: {
			Fop: int16(_NOp),
		},
	}
	qbe.s_omap2 = [81]struct {
		Fop  int16
		Fcls int16
		Ffmt uintptr
	}{
		0: {
			Fop:  int16(_Oadd),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 9877,
		},
		1: {
			Fop:  int16(_Oadd),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 9894,
		},
		2: {
			Fop:  int16(_Osub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 9913,
		},
		3: {
			Fop:  int16(_Osub),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 9930,
		},
		4: {
			Fop:  int16(_Oneg),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 9949,
		},
		5: {
			Fop:  int16(_Oneg),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 9962,
		},
		6: {
			Fop:  int16(_Odiv),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 9977,
		},
		7: {
			Fop:  int16(_Odiv),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 9994,
		},
		8: {
			Fop:  int16(_Orem),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10013,
		},
		9: {
			Fop:  int16(_Orem),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10030,
		},
		10: {
			Fop:  int16(_Oudiv),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10045,
		},
		11: {
			Fop:  int16(_Ourem),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10063,
		},
		12: {
			Fop:  int16(_Omul),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10081,
		},
		13: {
			Fop:  int16(_Omul),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 10098,
		},
		14: {
			Fop:  int16(_Oand),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 7635,
		},
		15: {
			Fop:  int16(_Oor),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10117,
		},
		16: {
			Fop:  int16(_Oxor),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10131,
		},
		17: {
			Fop:  int16(_Osar),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10146,
		},
		18: {
			Fop:  int16(_Oshr),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10163,
		},
		19: {
			Fop:  int16(_Oshl),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10180,
		},
		20: {
			Fop:  int16(_Ocsltl),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10197,
		},
		21: {
			Fop:  int16(_Ocultl),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10212,
		},
		22: {
			Fop:  int16(_Oceqs),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10228,
		},
		23: {
			Fop:  int16(_Ocges),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10245,
		},
		24: {
			Fop:  int16(_Ocgts),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10262,
		},
		25: {
			Fop:  int16(_Ocles),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10279,
		},
		26: {
			Fop:  int16(_Oclts),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10296,
		},
		27: {
			Fop:  int16(_Oceqd),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10313,
		},
		28: {
			Fop:  int16(_Ocged),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10330,
		},
		29: {
			Fop:  int16(_Ocgtd),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10347,
		},
		30: {
			Fop:  int16(_Ocled),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10364,
		},
		31: {
			Fop:  int16(_Ocltd),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10381,
		},
		32: {
			Fop:  int16(_Ostoreb),
			Ffmt: __ccgo_ts + 10398,
		},
		33: {
			Fop:  int16(_Ostoreh),
			Ffmt: __ccgo_ts + 10409,
		},
		34: {
			Fop:  int16(_Ostorew),
			Ffmt: __ccgo_ts + 10420,
		},
		35: {
			Fop:  int16(_Ostorel),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10431,
		},
		36: {
			Fop:  int16(_Ostores),
			Ffmt: __ccgo_ts + 10442,
		},
		37: {
			Fop:  int16(_Ostored),
			Ffmt: __ccgo_ts + 10454,
		},
		38: {
			Fop:  int16(_Oloadsb),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10466,
		},
		39: {
			Fop:  int16(_Oloadub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10477,
		},
		40: {
			Fop:  int16(_Oloadsh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10489,
		},
		41: {
			Fop:  int16(_Oloaduh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10500,
		},
		42: {
			Fop:  int16(_Oloadsw),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10512,
		},
		43: {
			Fop:  int16(_Oloaduw),
			Ffmt: __ccgo_ts + 10512,
		},
		44: {
			Fop:  int16(_Oloaduw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10523,
		},
		45: {
			Fop:  int16(_Oload),
			Ffmt: __ccgo_ts + 10512,
		},
		46: {
			Fop:  int16(_Oload),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10535,
		},
		47: {
			Fop:  int16(_Oload),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 10546,
		},
		48: {
			Fop:  int16(_Oload),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 10558,
		},
		49: {
			Fop:  int16(_Oextsb),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10570,
		},
		50: {
			Fop:  int16(_Oextub),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10584,
		},
		51: {
			Fop:  int16(_Oextsh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10598,
		},
		52: {
			Fop:  int16(_Oextuh),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10612,
		},
		53: {
			Fop:  int16(_Oextsw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10626,
		},
		54: {
			Fop:  int16(_Oextuw),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10640,
		},
		55: {
			Fop:  int16(_Otruncd),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 10654,
		},
		56: {
			Fop:  int16(_Oexts),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 10670,
		},
		57: {
			Fop:  int16(_Ostosi),
			Ffmt: __ccgo_ts + 10686,
		},
		58: {
			Fop:  int16(_Ostosi),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10707,
		},
		59: {
			Fop:  int16(_Ostoui),
			Ffmt: __ccgo_ts + 10728,
		},
		60: {
			Fop:  int16(_Ostoui),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10750,
		},
		61: {
			Fop:  int16(_Odtosi),
			Ffmt: __ccgo_ts + 10772,
		},
		62: {
			Fop:  int16(_Odtosi),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10793,
		},
		63: {
			Fop:  int16(_Odtoui),
			Ffmt: __ccgo_ts + 10814,
		},
		64: {
			Fop:  int16(_Odtoui),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10836,
		},
		65: {
			Fop:  int16(_Oswtof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 10858,
		},
		66: {
			Fop:  int16(_Ouwtof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 10875,
		},
		67: {
			Fop:  int16(_Osltof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 10893,
		},
		68: {
			Fop:  int16(_Oultof),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 10910,
		},
		69: {
			Fop:  int16(_Ocast),
			Ffmt: __ccgo_ts + 10928,
		},
		70: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Kl),
			Ffmt: __ccgo_ts + 10943,
		},
		71: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Ks),
			Ffmt: __ccgo_ts + 10958,
		},
		72: {
			Fop:  int16(_Ocast),
			Fcls: int16(_Kd),
			Ffmt: __ccgo_ts + 10973,
		},
		73: {
			Fop:  int16(_Ocopy),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 10988,
		},
		74: {
			Fop:  int16(_Ocopy),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 10998,
		},
		75: {
			Fop:  int16(_Oswap),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 11012,
		},
		76: {
			Fop:  int16(_Oswap),
			Fcls: int16(_Ka),
			Ffmt: __ccgo_ts + 11044,
		},
		77: {
			Fop:  int16(_Oreqz),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 11088,
		},
		78: {
			Fop:  int16(_Ornez),
			Fcls: int16(_Ki),
			Ffmt: __ccgo_ts + 11100,
		},
		79: {
			Fop:  int16(_Ocall),
			Ffmt: __ccgo_ts + 11112,
		},
		80: {
			Fop: int16(_NOp),
		},
	}
	qbe.s_pat = [6]int32{
		0: int32(_Pobis),
		1: int32(_Pobi1),
		2: int32(_Pbis),
		3: int32(_Pois),
		4: int32(_Pbi1),
		5: -int32(1),
	}
	qbe.s_pool = uintptr(unsafe.Pointer(&qbe.s_ptr))
	qbe.s_retreg = [2][2]int32{
		0: {
			0: int32(_RAX),
			1: int32(_RDX),
		},
		1: {
			0: int32(_XMM0),
			1: int32(_XMM0) + libc.Int32FromInt32(1),
		},
	}
	qbe.s_rname = [17][4]uintptr{
		1: {
			0: __ccgo_ts + 6484,
			1: __ccgo_ts + 6488,
			2: __ccgo_ts + 6492,
			3: __ccgo_ts + 6495,
		},
		2: {
			0: __ccgo_ts + 6498,
			1: __ccgo_ts + 6502,
			2: __ccgo_ts + 6506,
			3: __ccgo_ts + 6509,
		},
		3: {
			0: __ccgo_ts + 6512,
			1: __ccgo_ts + 6516,
			2: __ccgo_ts + 6520,
			3: __ccgo_ts + 6523,
		},
		4: {
			0: __ccgo_ts + 6526,
			1: __ccgo_ts + 6530,
			2: __ccgo_ts + 6534,
			3: __ccgo_ts + 6537,
		},
		5: {
			0: __ccgo_ts + 6541,
			1: __ccgo_ts + 6545,
			2: __ccgo_ts + 6549,
			3: __ccgo_ts + 6552,
		},
		6: {
			0: __ccgo_ts + 6556,
			1: __ccgo_ts + 6559,
			2: __ccgo_ts + 6563,
			3: __ccgo_ts + 6567,
		},
		7: {
			0: __ccgo_ts + 6571,
			1: __ccgo_ts + 6574,
			2: __ccgo_ts + 6578,
			3: __ccgo_ts + 6582,
		},
		8: {
			0: __ccgo_ts + 6586,
			1: __ccgo_ts + 6590,
			2: __ccgo_ts + 6595,
			3: __ccgo_ts + 6600,
		},
		9: {
			0: __ccgo_ts + 6605,
			1: __ccgo_ts + 6609,
			2: __ccgo_ts + 6614,
			3: __ccgo_ts + 6619,
		},
		10: {
			0: __ccgo_ts + 6624,
			1: __ccgo_ts + 6628,
			2: __ccgo_ts + 6632,
			3: __ccgo_ts + 6635,
		},
		11: {
			0: __ccgo_ts + 6638,
			1: __ccgo_ts + 6642,
			2: __ccgo_ts + 6647,
			3: __ccgo_ts + 6652,
		},
		12: {
			0: __ccgo_ts + 6657,
			1: __ccgo_ts + 6661,
			2: __ccgo_ts + 6666,
			3: __ccgo_ts + 6671,
		},
		13: {
			0: __ccgo_ts + 6676,
			1: __ccgo_ts + 6680,
			2: __ccgo_ts + 6685,
			3: __ccgo_ts + 6690,
		},
		14: {
			0: __ccgo_ts + 6695,
			1: __ccgo_ts + 6699,
			2: __ccgo_ts + 6704,
			3: __ccgo_ts + 6709,
		},
		15: {
			0: __ccgo_ts + 6714,
			1: __ccgo_ts + 6718,
			2: __ccgo_ts + 6722,
			3: __ccgo_ts + 6725,
		},
		16: {
			0: __ccgo_ts + 6729,
			1: __ccgo_ts + 6733,
			2: __ccgo_ts + 6737,
			3: __ccgo_ts + 6740,
		},
	}
	qbe.s_rname2 = [64]uintptr{
		1:  __ccgo_ts + 11120,
		2:  __ccgo_ts + 11123,
		3:  __ccgo_ts + 11126,
		4:  __ccgo_ts + 11129,
		5:  __ccgo_ts + 11132,
		6:  __ccgo_ts + 11135,
		7:  __ccgo_ts + 11138,
		8:  __ccgo_ts + 11141,
		9:  __ccgo_ts + 11144,
		10: __ccgo_ts + 11147,
		11: __ccgo_ts + 11150,
		12: __ccgo_ts + 11153,
		13: __ccgo_ts + 11156,
		14: __ccgo_ts + 11159,
		15: __ccgo_ts + 11162,
		16: __ccgo_ts + 11165,
		17: __ccgo_ts + 11168,
		18: __ccgo_ts + 11171,
		19: __ccgo_ts + 11174,
		20: __ccgo_ts + 11177,
		21: __ccgo_ts + 11180,
		22: __ccgo_ts + 11183,
		23: __ccgo_ts + 11186,
		24: __ccgo_ts + 11189,
		25: __ccgo_ts + 11193,
		26: __ccgo_ts + 11197,
		27: __ccgo_ts + 6737,
		28: __ccgo_ts + 11200,
		29: __ccgo_ts + 11203,
		30: __ccgo_ts + 11206,
		31: __ccgo_ts + 11209,
		32: __ccgo_ts + 11213,
		33: __ccgo_ts + 11217,
		34: __ccgo_ts + 11221,
		35: __ccgo_ts + 11225,
		36: __ccgo_ts + 11229,
		37: __ccgo_ts + 11233,
		38: __ccgo_ts + 11237,
		39: __ccgo_ts + 11241,
		40: __ccgo_ts + 11245,
		41: __ccgo_ts + 11249,
		42: __ccgo_ts + 11254,
		43: __ccgo_ts + 11258,
		44: __ccgo_ts + 11262,
		45: __ccgo_ts + 11266,
		46: __ccgo_ts + 11270,
		47: __ccgo_ts + 11274,
		48: __ccgo_ts + 11278,
		49: __ccgo_ts + 11282,
		50: __ccgo_ts + 11286,
		51: __ccgo_ts + 11290,
		52: __ccgo_ts + 11294,
		53: __ccgo_ts + 11298,
		54: __ccgo_ts + 11302,
		55: __ccgo_ts + 11306,
		56: __ccgo_ts + 11310,
		57: __ccgo_ts + 11314,
		58: __ccgo_ts + 11318,
		59: __ccgo_ts + 11322,
		60: __ccgo_ts + 11326,
		61: __ccgo_ts + 11331,
		62: __ccgo_ts + 11336,
		63: __ccgo_ts + 11339,
	}
	qbe.s_sec = [2][3]uintptr{
		0: {
			0: __ccgo_ts + 4430,
			1: __ccgo_ts + 4436,
			2: __ccgo_ts + 4442,
		},
		1: {
			0: __ccgo_ts + 4447,
			1: __ccgo_ts + 4468,
			2: __ccgo_ts + 4490,
		},
	}
	qbe.s_sec1 = [3]uintptr{
		0: __ccgo_ts + 4991,
		1: __ccgo_ts + 4991,
		2: __ccgo_ts + 4991,
	}
	qbe.s_sec2 = [3]uintptr{
		0: __ccgo_ts + 5075,
		1: __ccgo_ts + 5108,
		2: __ccgo_ts + 4447,
	}
	qbe.s_st = [4]int32{
		0: int32(_Ostorew),
		1: int32(_Ostorel),
		2: int32(_Ostores),
		3: int32(_Ostored),
	}
	qbe.s_store3 = [4]int32{
		0: int32(_Ostorew),
		1: int32(_Ostorel),
		2: int32(_Ostores),
		3: int32(_Ostored),
	}
	qbe.s_tlist = [6]uintptr{
		0: uintptr(unsafe.Pointer(&qbe.x_T_amd64_sysv)),
		1: uintptr(unsafe.Pointer(&qbe.x_T_amd64_apple)),
		2: uintptr(unsafe.Pointer(&qbe.x_T_arm64)),
		3: uintptr(unsafe.Pointer(&qbe.x_T_arm64_apple)),
		4: uintptr(unsafe.Pointer(&qbe.x_T_rv64)),
	}
	qbe.s_ttoa = [140]uintptr{
		126: __ccgo_ts + 1336,
		131: __ccgo_ts + 1342,
		132: __ccgo_ts + 1344,
		133: __ccgo_ts + 1346,
		134: __ccgo_ts + 1348,
		135: __ccgo_ts + 1350,
		136: __ccgo_ts + 1352,
		137: __ccgo_ts + 1354,
	}
	qbe.s_ulog2_tab64 = [64]int32{
		0:  int32(63),
		2:  int32(1),
		3:  int32(41),
		4:  int32(37),
		5:  int32(2),
		6:  int32(16),
		7:  int32(42),
		8:  int32(38),
		9:  int32(29),
		10: int32(32),
		11: int32(3),
		12: int32(12),
		13: int32(17),
		14: int32(43),
		15: int32(55),
		16: int32(39),
		17: int32(35),
		18: int32(30),
		19: int32(53),
		20: int32(33),
		21: int32(21),
		22: int32(4),
		23: int32(23),
		24: int32(13),
		25: int32(9),
		26: int32(18),
		27: int32(6),
		28: int32(25),
		29: int32(44),
		30: int32(48),
		31: int32(56),
		32: int32(62),
		33: int32(40),
		34: int32(36),
		35: int32(15),
		36: int32(28),
		37: int32(31),
		38: int32(11),
		39: int32(54),
		40: int32(34),
		41: int32(52),
		42: int32(20),
		43: int32(22),
		44: int32(8),
		45: int32(5),
		46: int32(24),
		47: int32(47),
		48: int32(61),
		49: int32(14),
		50: int32(27),
		51: int32(10),
		52: int32(51),
		53: int32(19),
		54: int32(7),
		55: int32(46),
		56: int32(60),
		57: int32(26),
		58: int32(50),
		59: int32(45),
		60: int32(59),
		61: int32(49),
		62: int32(58),
		63: int32(57),
	}
	qbe.x_T_amd64_apple = _Target{
		Fname:   [16]int8{'a', 'm', 'd', '6', '4', '_', 'a', 'p', 'p', 'l', 'e'},
		Fapple:  int8(1),
		Fgpr0:   int32(_RAX),
		Fngpr:   int32(_NGPR),
		Ffpr0:   int32(_XMM0),
		Fnfpr:   int32(_NFPR),
		Frglob:  libc.Uint64FromInt32(1)<<int32(_RBP) | libc.Uint64FromInt32(1)<<int32(_RSP),
		Fnrglob: int32(2),
		Frsave:  uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rsave)),
		Fnrsave: [2]int32{
			0: int32(_NGPS),
			1: int32(_NFPS),
		},
		Fasloc: [4]int8{'L'},
		Fassym: [4]int8{'_'},
	}
	qbe.x_T_amd64_sysv = _Target{
		Fname:   [16]int8{'a', 'm', 'd', '6', '4', '_', 's', 'y', 's', 'v'},
		Fgpr0:   int32(_RAX),
		Fngpr:   int32(_NGPR),
		Ffpr0:   int32(_XMM0),
		Fnfpr:   int32(_NFPR),
		Frglob:  libc.Uint64FromInt32(1)<<int32(_RBP) | libc.Uint64FromInt32(1)<<int32(_RSP),
		Fnrglob: int32(2),
		Frsave:  uintptr(unsafe.Pointer(&qbe.x_amd64_sysv_rsave)),
		Fnrsave: [2]int32{
			0: int32(_NGPS),
			1: int32(_NFPS),
		},
		Fasloc: [4]int8{'.', 'L'},
	}
	qbe.x_T_arm64 = _Target{
		Fname:   [16]int8{'a', 'r', 'm', '6', '4'},
		Fgpr0:   int32(_R0),
		Fngpr:   int32(_NGPR1),
		Ffpr0:   int32(_V0),
		Fnfpr:   int32(_NFPR1),
		Frglob:  libc.Uint64FromInt32(1)<<int32(_FP) | libc.Uint64FromInt32(1)<<int32(_SP) | libc.Uint64FromInt32(1)<<int32(_R18),
		Fnrglob: int32(3),
		Frsave:  uintptr(unsafe.Pointer(&qbe.x_arm64_rsave)),
		Fnrsave: [2]int32{
			0: int32(_NGPS1),
			1: int32(_NFPS1),
		},
		Fasloc: [4]int8{'.', 'L'},
	}
	qbe.x_T_arm64_apple = _Target{
		Fname:   [16]int8{'a', 'r', 'm', '6', '4', '_', 'a', 'p', 'p', 'l', 'e'},
		Fapple:  int8(1),
		Fgpr0:   int32(_R0),
		Fngpr:   int32(_NGPR1),
		Ffpr0:   int32(_V0),
		Fnfpr:   int32(_NFPR1),
		Frglob:  libc.Uint64FromInt32(1)<<int32(_FP) | libc.Uint64FromInt32(1)<<int32(_SP) | libc.Uint64FromInt32(1)<<int32(_R18),
		Fnrglob: int32(3),
		Frsave:  uintptr(unsafe.Pointer(&qbe.x_arm64_rsave)),
		Fnrsave: [2]int32{
			0: int32(_NGPS1),
			1: int32(_NFPS1),
		},
		Fasloc: [4]int8{'L'},
		Fassym: [4]int8{'_'},
	}
	qbe.x_T_rv64 = _Target{
		Fname:   [16]int8{'r', 'v', '6', '4'},
		Fgpr0:   int32(_T0),
		Fngpr:   int32(_NGPR2),
		Ffpr0:   int32(_FT0),
		Fnfpr:   int32(_NFPR1),
		Frglob:  libc.Uint64FromInt32(1)<<int32(_FP1) | libc.Uint64FromInt32(1)<<int32(_SP1) | libc.Uint64FromInt32(1)<<int32(_GP) | libc.Uint64FromInt32(1)<<int32(_TP) | libc.Uint64FromInt32(1)<<int32(_RA),
		Fnrglob: int32(5),
		Frsave:  uintptr(unsafe.Pointer(&qbe.x_rv64_rsave)),
		Fnrsave: [2]int32{
			0: int32(_NGPS2),
			1: int32(_NFPS2),
		},
		Fasloc: [4]int8{'.', 'L'},
	}
	qbe.x_amd64_op = [138]_Amd64Op{
		1: {
			Fnmem:  int8(2),
			Fzflag: int8(1),
		},
		2: {
			Fnmem:  int8(2),
			Fzflag: int8(1),
		},
		3: {
			Fnmem:  int8(1),
			Fzflag: int8(1),
		},
		4: {},
		5: {},
		6: {},
		7: {},
		8: {
			Fnmem: int8(2),
		},
		9: {
			Fnmem:  int8(2),
			Fzflag: int8(1),
		},
		10: {
			Fnmem:  int8(2),
			Fzflag: int8(1),
		},
		11: {
			Fnmem:  int8(2),
			Fzflag: int8(1),
		},
		12: {
			Fnmem:  int8(1),
			Fzflag: int8(1),
		},
		13: {
			Fnmem:  int8(1),
			Fzflag: int8(1),
		},
		14: {
			Fnmem:  int8(1),
			Fzflag: int8(1),
		},
		15: {
			Fzflag: int8(1),
		},
		16: {
			Fzflag: int8(1),
		},
		17: {
			Fzflag: int8(1),
		},
		18: {
			Fzflag: int8(1),
		},
		19: {
			Fzflag: int8(1),
		},
		20: {
			Fzflag: int8(1),
		},
		21: {
			Fzflag: int8(1),
		},
		22: {
			Fzflag: int8(1),
		},
		23: {
			Fzflag: int8(1),
		},
		24: {
			Fzflag: int8(1),
		},
		25: {
			Fzflag: int8(1),
		},
		26: {
			Fzflag: int8(1),
		},
		27: {
			Fzflag: int8(1),
		},
		28: {
			Fzflag: int8(1),
		},
		29: {
			Fzflag: int8(1),
		},
		30: {
			Fzflag: int8(1),
		},
		31: {
			Fzflag: int8(1),
		},
		32: {
			Fzflag: int8(1),
		},
		33: {
			Fzflag: int8(1),
		},
		34: {
			Fzflag: int8(1),
		},
		35: {
			Fzflag: int8(1),
		},
		36: {
			Fzflag: int8(1),
		},
		37: {
			Fzflag: int8(1),
		},
		38: {
			Fzflag: int8(1),
		},
		39: {
			Fzflag: int8(1),
		},
		40: {
			Fzflag: int8(1),
		},
		41: {
			Fzflag: int8(1),
		},
		42: {
			Fzflag: int8(1),
		},
		43: {
			Fzflag: int8(1),
		},
		44: {
			Fzflag: int8(1),
		},
		45: {
			Fzflag: int8(1),
		},
		46: {
			Fzflag: int8(1),
		},
		47: {
			Fzflag: int8(1),
		},
		48: {
			Fzflag: int8(1),
		},
		49: {
			Fzflag: int8(1),
		},
		50: {
			Fzflag: int8(1),
		},
		51: {
			Flflag: int8(1),
		},
		52: {
			Flflag: int8(1),
		},
		53: {
			Flflag: int8(1),
		},
		54: {
			Flflag: int8(1),
		},
		55: {
			Flflag: int8(1),
		},
		56: {
			Flflag: int8(1),
		},
		57: {
			Flflag: int8(1),
		},
		58: {
			Flflag: int8(1),
		},
		59: {
			Flflag: int8(1),
		},
		60: {
			Flflag: int8(1),
		},
		61: {
			Flflag: int8(1),
		},
		62: {
			Flflag: int8(1),
		},
		63: {
			Flflag: int8(1),
		},
		64: {
			Flflag: int8(1),
		},
		65: {
			Flflag: int8(1),
		},
		66: {
			Flflag: int8(1),
		},
		67: {
			Flflag: int8(1),
		},
		68: {
			Flflag: int8(1),
		},
		69: {
			Flflag: int8(1),
		},
		70: {
			Flflag: int8(1),
		},
		71: {
			Flflag: int8(1),
		},
		72: {
			Flflag: int8(1),
		},
		73: {
			Flflag: int8(1),
		},
		74: {
			Flflag: int8(1),
		},
		75: {
			Flflag: int8(1),
		},
		76: {
			Flflag: int8(1),
		},
		77: {
			Flflag: int8(1),
		},
		78: {
			Flflag: int8(1),
		},
		79: {
			Flflag: int8(1),
		},
		80: {
			Flflag: int8(1),
		},
		81: {},
		82: {},
		83: {},
		84: {},
		85: {},
		86: {
			Flflag: int8(1),
		},
		87: {
			Flflag: int8(1),
		},
		88: {
			Flflag: int8(1),
		},
		89: {
			Flflag: int8(1),
		},
		90: {
			Fzflag: int8(1),
		},
		91: {
			Fzflag: int8(1),
		},
		92: {
			Fnmem: int8(1),
		},
		93: {},
		94: {},
		95: {
			Fnmem: int8(1),
		},
		96: {
			Fnmem: int8(1),
		},
		97: {
			Fnmem:  int8(1),
			Fzflag: int8(1),
		},
		98: {
			Fnmem:  int8(1),
			Fzflag: int8(1),
		},
		99:  {},
		100: {},
		101: {},
		102: {},
		103: {},
		104: {},
		105: {},
		106: {},
		107: {},
		108: {},
		109: {},
		110: {},
		111: {},
		112: {},
		113: {},
		114: {},
		115: {},
		116: {},
		117: {},
		118: {},
		119: {},
		120: {
			Flflag: int8(1),
		},
		121: {
			Flflag: int8(1),
		},
		122: {
			Flflag: int8(1),
		},
		123: {
			Flflag: int8(1),
		},
		124: {
			Flflag: int8(1),
		},
		125: {
			Flflag: int8(1),
		},
		126: {
			Flflag: int8(1),
		},
		127: {
			Flflag: int8(1),
		},
		128: {
			Flflag: int8(1),
		},
		129: {
			Flflag: int8(1),
		},
		130: {
			Flflag: int8(1),
		},
		131: {
			Flflag: int8(1),
		},
		132: {
			Flflag: int8(1),
		},
		133: {
			Flflag: int8(1),
		},
		134: {
			Flflag: int8(1),
		},
		135: {
			Flflag: int8(1),
		},
		136: {
			Flflag: int8(1),
		},
		137: {
			Flflag: int8(1),
		},
	}
	qbe.x_amd64_sysv_rclob = [6]int32{
		0: int32(_RBX),
		1: int32(_R12),
		2: int32(_R13),
		3: int32(_R14),
		4: int32(_R15),
		5: -int32(1),
	}
	qbe.x_amd64_sysv_rsave = [25]int32{
		0:  int32(_RDI),
		1:  int32(_RSI),
		2:  int32(_RDX),
		3:  int32(_RCX),
		4:  int32(_R8),
		5:  int32(_R9),
		6:  int32(_R10),
		7:  int32(_R11),
		8:  int32(_RAX),
		9:  int32(_XMM0),
		10: int32(_XMM1),
		11: int32(_XMM2),
		12: int32(_XMM3),
		13: int32(_XMM4),
		14: int32(_XMM5),
		15: int32(_XMM6),
		16: int32(_XMM7),
		17: int32(_XMM8),
		18: int32(_XMM9),
		19: int32(_XMM10),
		20: int32(_XMM11),
		21: int32(_XMM12),
		22: int32(_XMM13),
		23: int32(_XMM14),
		24: -int32(1),
	}
	qbe.x_arm64_rclob = [19]int32{
		0:  int32(_R19),
		1:  int32(_R20),
		2:  int32(_R21),
		3:  int32(_R22),
		4:  int32(_R23),
		5:  int32(_R24),
		6:  int32(_R25),
		7:  int32(_R26),
		8:  int32(_R27),
		9:  int32(_R28),
		10: int32(_V8),
		11: int32(_V9),
		12: int32(_V10),
		13: int32(_V11),
		14: int32(_V12),
		15: int32(_V13),
		16: int32(_V14),
		17: int32(_V15),
		18: -int32(1),
	}
	qbe.x_arm64_rsave = [44]int32{
		0:  int32(_R0),
		1:  int32(_R1),
		2:  int32(_R2),
		3:  int32(_R3),
		4:  int32(_R4),
		5:  int32(_R5),
		6:  int32(_R6),
		7:  int32(_R7),
		8:  int32(_R81),
		9:  int32(_R91),
		10: int32(_R101),
		11: int32(_R111),
		12: int32(_R121),
		13: int32(_R131),
		14: int32(_R141),
		15: int32(_R151),
		16: int32(_IP0),
		17: int32(_IP1),
		18: int32(_R18),
		19: int32(_LR),
		20: int32(_V0),
		21: int32(_V1),
		22: int32(_V2),
		23: int32(_V3),
		24: int32(_V4),
		25: int32(_V5),
		26: int32(_V6),
		27: int32(_V7),
		28: int32(_V16),
		29: int32(_V17),
		30: int32(_V18),
		31: int32(_V19),
		32: int32(_V20),
		33: int32(_V21),
		34: int32(_V22),
		35: int32(_V23),
		36: int32(_V24),
		37: int32(_V25),
		38: int32(_V26),
		39: int32(_V27),
		40: int32(_V28),
		41: int32(_V29),
		42: int32(_V30),
		43: -int32(1),
	}
	qbe.x_debug = [91]int8{}
	qbe.x_optab = [138]_Op{
		1: {
			Fname: __ccgo_ts + 271,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		2: {
			Fname: __ccgo_ts + 275,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		3: {
			Fname: __ccgo_ts + 279,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		4: {
			Fname: __ccgo_ts + 283,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(1)&0x1<<2,
		},
		5: {
			Fname: __ccgo_ts + 287,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		6: {
			Fname: __ccgo_ts + 291,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(1)&0x1<<2,
		},
		7: {
			Fname: __ccgo_ts + 296,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		8: {
			Fname: __ccgo_ts + 301,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(1)&0x1<<2,
		},
		9: {
			Fname: __ccgo_ts + 305,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		10: {
			Fname: __ccgo_ts + 309,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		11: {
			Fname: __ccgo_ts + 312,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		12: {
			Fname: __ccgo_ts + 316,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		13: {
			Fname: __ccgo_ts + 320,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		14: {
			Fname: __ccgo_ts + 324,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(1)&0x1<<1 | uint8(0)&0x1<<2,
		},
		15: {
			Fname: __ccgo_ts + 328,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		16: {
			Fname: __ccgo_ts + 333,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		17: {
			Fname: __ccgo_ts + 338,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		18: {
			Fname: __ccgo_ts + 344,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		19: {
			Fname: __ccgo_ts + 350,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		20: {
			Fname: __ccgo_ts + 356,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		21: {
			Fname: __ccgo_ts + 362,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		22: {
			Fname: __ccgo_ts + 368,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		23: {
			Fname: __ccgo_ts + 374,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		24: {
			Fname: __ccgo_ts + 380,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		25: {
			Fname: __ccgo_ts + 386,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		26: {
			Fname: __ccgo_ts + 391,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		27: {
			Fname: __ccgo_ts + 396,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		28: {
			Fname: __ccgo_ts + 402,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		29: {
			Fname: __ccgo_ts + 408,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		30: {
			Fname: __ccgo_ts + 414,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		31: {
			Fname: __ccgo_ts + 420,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		32: {
			Fname: __ccgo_ts + 426,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		33: {
			Fname: __ccgo_ts + 432,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		34: {
			Fname: __ccgo_ts + 438,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kl),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		35: {
			Fname: __ccgo_ts + 444,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		36: {
			Fname: __ccgo_ts + 449,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		37: {
			Fname: __ccgo_ts + 454,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		38: {
			Fname: __ccgo_ts + 459,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		39: {
			Fname: __ccgo_ts + 464,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		40: {
			Fname: __ccgo_ts + 469,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		41: {
			Fname: __ccgo_ts + 474,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		42: {
			Fname: __ccgo_ts + 478,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		43: {
			Fname: __ccgo_ts + 483,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		44: {
			Fname: __ccgo_ts + 488,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		45: {
			Fname: __ccgo_ts + 493,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		46: {
			Fname: __ccgo_ts + 498,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		47: {
			Fname: __ccgo_ts + 503,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		48: {
			Fname: __ccgo_ts + 508,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		49: {
			Fname: __ccgo_ts + 513,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		50: {
			Fname: __ccgo_ts + 517,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		51: {
			Fname: __ccgo_ts + 522,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		52: {
			Fname: __ccgo_ts + 529,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		53: {
			Fname: __ccgo_ts + 536,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		54: {
			Fname: __ccgo_ts + 543,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kl),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		55: {
			Fname: __ccgo_ts + 550,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		56: {
			Fname: __ccgo_ts + 557,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		57: {
			Fname: __ccgo_ts + 564,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		58: {
			Fname: __ccgo_ts + 571,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		59: {
			Fname: __ccgo_ts + 578,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		60: {
			Fname: __ccgo_ts + 585,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		61: {
			Fname: __ccgo_ts + 592,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		62: {
			Fname: __ccgo_ts + 599,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		63: {
			Fname: __ccgo_ts + 606,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Km),
					3: int16(_Km),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		64: {
			Fname: __ccgo_ts + 611,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		65: {
			Fname: __ccgo_ts + 617,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		66: {
			Fname: __ccgo_ts + 623,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		67: {
			Fname: __ccgo_ts + 629,
			Fargcls: [2][4]int16{
				0: {
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		68: {
			Fname: __ccgo_ts + 635,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		69: {
			Fname: __ccgo_ts + 641,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		70: {
			Fname: __ccgo_ts + 647,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ks),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		71: {
			Fname: __ccgo_ts + 652,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kd),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kx),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		72: {
			Fname: __ccgo_ts + 659,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		73: {
			Fname: __ccgo_ts + 665,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Ks),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		74: {
			Fname: __ccgo_ts + 671,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		75: {
			Fname: __ccgo_ts + 677,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kd),
					1: int16(_Kd),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		76: {
			Fname: __ccgo_ts + 683,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		77: {
			Fname: __ccgo_ts + 689,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		78: {
			Fname: __ccgo_ts + 695,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kl),
					3: int16(_Kl),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		79: {
			Fname: __ccgo_ts + 701,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kl),
					3: int16(_Kl),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		80: {
			Fname: __ccgo_ts + 707,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ks),
					1: int16(_Kd),
					3: int16(_Kl),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(1)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		81: {
			Fname: __ccgo_ts + 712,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		82: {
			Fname: __ccgo_ts + 719,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		83: {
			Fname: __ccgo_ts + 726,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		84: {
			Fname: __ccgo_ts + 734,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Km),
					3: int16(_Km),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		85: {
			Fname: __ccgo_ts + 740,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		86: {
			Fname: __ccgo_ts + 748,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		87: {
			Fname: __ccgo_ts + 753,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		88: {
			Fname: __ccgo_ts + 760,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		89: {
			Fname: __ccgo_ts + 764,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		90: {
			Fname: __ccgo_ts + 769,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Km),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		91: {
			Fname: __ccgo_ts + 775,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		92: {
			Fname: __ccgo_ts + 781,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		93: {
			Fname: __ccgo_ts + 786,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		94: {
			Fname: __ccgo_ts + 791,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		95: {
			Fname: __ccgo_ts + 798,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		96: {
			Fname: __ccgo_ts + 804,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		97: {
			Fname: __ccgo_ts + 809,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		98: {
			Fname: __ccgo_ts + 814,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		99: {
			Fname: __ccgo_ts + 820,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		100: {
			Fname: __ccgo_ts + 825,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		101: {
			Fname: __ccgo_ts + 830,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Ke),
					2: int16(_Ks),
					3: int16(_Kd),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		102: {
			Fname: __ccgo_ts + 836,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		103: {
			Fname: __ccgo_ts + 841,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		104: {
			Fname: __ccgo_ts + 846,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		105: {
			Fname: __ccgo_ts + 850,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		106: {
			Fname: __ccgo_ts + 856,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		107: {
			Fname: __ccgo_ts + 862,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		108: {
			Fname: __ccgo_ts + 868,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		109: {
			Fname: __ccgo_ts + 874,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		110: {
			Fname: __ccgo_ts + 879,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		111: {
			Fname: __ccgo_ts + 884,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Kl),
					2: int16(_Ks),
					3: int16(_Kd),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		112: {
			Fname: __ccgo_ts + 888,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		113: {
			Fname: __ccgo_ts + 894,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		114: {
			Fname: __ccgo_ts + 900,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		115: {
			Fname: __ccgo_ts + 906,
			Fargcls: [2][4]int16{
				0: {
					1: int16(_Ke),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		116: {
			Fname: __ccgo_ts + 912,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		117: {
			Fname: __ccgo_ts + 917,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Ke),
					1: int16(_Kl),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Ke),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		118: {
			Fname: __ccgo_ts + 922,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		119: {
			Fname: __ccgo_ts + 927,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Km),
					1: int16(_Km),
					2: int16(_Km),
					3: int16(_Km),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Kx),
					3: int16(_Kx),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		120: {
			Fname: __ccgo_ts + 932,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		121: {
			Fname: __ccgo_ts + 940,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		122: {
			Fname: __ccgo_ts + 948,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		123: {
			Fname: __ccgo_ts + 957,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		124: {
			Fname: __ccgo_ts + 966,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		125: {
			Fname: __ccgo_ts + 975,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		126: {
			Fname: __ccgo_ts + 984,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		127: {
			Fname: __ccgo_ts + 993,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		128: {
			Fname: __ccgo_ts + 1002,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		129: {
			Fname: __ccgo_ts + 1011,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		130: {
			Fname: __ccgo_ts + 1020,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		131: {
			Fname: __ccgo_ts + 1028,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		132: {
			Fname: __ccgo_ts + 1036,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		133: {
			Fname: __ccgo_ts + 1044,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		134: {
			Fname: __ccgo_ts + 1052,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		135: {
			Fname: __ccgo_ts + 1060,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		136: {
			Fname: __ccgo_ts + 1068,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
		137: {
			Fname: __ccgo_ts + 1075,
			Fargcls: [2][4]int16{
				0: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
				1: {
					0: int16(_Kx),
					1: int16(_Kx),
					2: int16(_Ke),
					3: int16(_Ke),
				},
			},
			F__ccgo24: uint8(0)&0x1<<0 | uint8(0)&0x1<<1 | uint8(0)&0x1<<2,
		},
	}
	qbe.x_rv64_op = [138]_Rv64Op{
		1: {
			Fimm: int8(1),
		},
		2: {},
		3: {},
		4: {},
		5: {},
		6: {},
		7: {},
		8: {},
		9: {
			Fimm: int8(1),
		},
		10: {
			Fimm: int8(1),
		},
		11: {
			Fimm: int8(1),
		},
		12: {
			Fimm: int8(1),
		},
		13: {
			Fimm: int8(1),
		},
		14: {
			Fimm: int8(1),
		},
		15: {},
		16: {},
		17: {},
		18: {},
		19: {},
		20: {
			Fimm: int8(1),
		},
		21: {},
		22: {},
		23: {},
		24: {
			Fimm: int8(1),
		},
		25: {},
		26: {},
		27: {},
		28: {},
		29: {},
		30: {
			Fimm: int8(1),
		},
		31: {},
		32: {},
		33: {},
		34: {
			Fimm: int8(1),
		},
		35:  {},
		36:  {},
		37:  {},
		38:  {},
		39:  {},
		40:  {},
		41:  {},
		42:  {},
		43:  {},
		44:  {},
		45:  {},
		46:  {},
		47:  {},
		48:  {},
		49:  {},
		50:  {},
		51:  {},
		52:  {},
		53:  {},
		54:  {},
		55:  {},
		56:  {},
		57:  {},
		58:  {},
		59:  {},
		60:  {},
		61:  {},
		62:  {},
		63:  {},
		64:  {},
		65:  {},
		66:  {},
		67:  {},
		68:  {},
		69:  {},
		70:  {},
		71:  {},
		72:  {},
		73:  {},
		74:  {},
		75:  {},
		76:  {},
		77:  {},
		78:  {},
		79:  {},
		80:  {},
		81:  {},
		82:  {},
		83:  {},
		84:  {},
		85:  {},
		86:  {},
		87:  {},
		88:  {},
		89:  {},
		90:  {},
		91:  {},
		92:  {},
		93:  {},
		94:  {},
		95:  {},
		96:  {},
		97:  {},
		98:  {},
		99:  {},
		100: {},
		101: {},
		102: {},
		103: {},
		104: {},
		105: {},
		106: {},
		107: {},
		108: {},
		109: {},
		110: {},
		111: {},
		112: {},
		113: {},
		114: {},
		115: {},
		116: {},
		117: {},
		118: {},
		119: {},
		120: {},
		121: {},
		122: {},
		123: {},
		124: {},
		125: {},
		126: {},
		127: {},
		128: {},
		129: {},
		130: {},
		131: {},
		132: {},
		133: {},
		134: {},
		135: {},
		136: {},
		137: {},
	}
	qbe.x_rv64_rclob = [24]int32{
		0:  int32(_S1),
		1:  int32(_S2),
		2:  int32(_S3),
		3:  int32(_S4),
		4:  int32(_S5),
		5:  int32(_S6),
		6:  int32(_S7),
		7:  int32(_S8),
		8:  int32(_S9),
		9:  int32(_S10),
		10: int32(_S11),
		11: int32(_FS0),
		12: int32(_FS1),
		13: int32(_FS2),
		14: int32(_FS3),
		15: int32(_FS4),
		16: int32(_FS5),
		17: int32(_FS6),
		18: int32(_FS7),
		19: int32(_FS8),
		20: int32(_FS9),
		21: int32(_FS10),
		22: int32(_FS11),
		23: -int32(1),
	}
	qbe.x_rv64_rsave = [34]int32{
		0:  int32(_T0),
		1:  int32(_T1),
		2:  int32(_T2),
		3:  int32(_T3),
		4:  int32(_T4),
		5:  int32(_T5),
		6:  int32(_A0),
		7:  int32(_A1),
		8:  int32(_A2),
		9:  int32(_A3),
		10: int32(_A4),
		11: int32(_A5),
		12: int32(_A6),
		13: int32(_A7),
		14: int32(_FA0),
		15: int32(_FA1),
		16: int32(_FA2),
		17: int32(_FA3),
		18: int32(_FA4),
		19: int32(_FA5),
		20: int32(_FA6),
		21: int32(_FA7),
		22: int32(_FT0),
		23: int32(_FT1),
		24: int32(_FT2),
		25: int32(_FT3),
		26: int32(_FT4),
		27: int32(_FT5),
		28: int32(_FT6),
		29: int32(_FT7),
		30: int32(_FT8),
		31: int32(_FT9),
		32: int32(_FT10),
		33: -int32(1),
	}
	qbe.init1()
	qbe.init2()
	qbe.init3()
	qbe.init4()
	qbe.init5()
	return qbe
}
